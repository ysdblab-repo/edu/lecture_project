function arrayDiff(a, b) {
  const tmp = {};
  const res = [];
  a.forEach((value) => {
    tmp[value] = 1;
  });
  b.forEach((value) => {
    if (tmp[value]) delete tmp[value];
  });
  Object.keys(tmp).forEach((key) => {
    res.push(key);
  });
  return res;
}