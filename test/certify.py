import numpy as np
import argparse
np.set_printoptions(precision=12)
parser = argparse.ArgumentParser()
parser.add_argument('vecstr', type=str, nargs='+', help = "Input vector string")
args = parser.parse_args()

para1 = args.vecstr[0]
para2 = args.vecstr[1]

para1f=[float(x) for x in para1.split(",")]
para2f=[float(x) for x in para2.split(",")]
avg_vector = np.array(para1f[0:128])
deviation = para1f[128]
#print(len(para1f))
new_vector = np.array(para2f)
d= avg_vector-new_vector
if np.dot(d,d)<deviation * 2:
    print(1)
else:
    print(0)
