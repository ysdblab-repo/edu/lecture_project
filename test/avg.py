import numpy as np
import argparse
np.set_printoptions(precision=12)
parser = argparse.ArgumentParser()
parser.add_argument('vecstr', type=str, nargs='+', help = "Input vector string")
args = parser.parse_args()
para = args.vecstr[0]

paraf=[float(x) for x in para.split(",")]
arrlen= len(paraf)/128
result = np.array(paraf[0:128])
for i in range(1,arrlen):
    result = np.vstack([result,np.array(paraf[i*128:(i+1)*128])])
avg = np.average(result, axis=0)
t=0
for i in range(1,arrlen):
    d=avg-np.array(paraf[i*128:(i+1)*128])
    #t+=np.dot(d,d)
    if t<np.dot(d,d):
        t=np.dot(d,d)
#t/=arrlen

resultString = ""
for i in range(0,128):
    resultString+=str(avg[i])+","
resultString+=str(t)
print(resultString)
