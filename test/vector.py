import time
import cv2
import os
import numpy as np
import openface
import argparse
import math
total_start = time.time()
np.set_printoptions(precision=12)
imgDim = 96

dirPath = os.path.dirname( os.path.abspath( __file__ ) )
align = openface.AlignDlib(dirPath + "/dlib/shape_predictor_68_face_landmarks.dat")
net = openface.TorchNeuralNet(dirPath + "/openface/nn4.small2.v1.t7", imgDim)
def getRep(imgPath):
    Img = cv2.imread(imgPath)
    if Img is None:
        return 0
    hist, bins = np.histogram(Img.flatten(), 256,[0,256])
    cdf = hist.cumsum()
    cdf_m = np.ma.masked_equal(cdf,0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
    cdf = np.ma.filled(cdf_m,0).astype('uint8')
    bgrImg = cdf[Img]    


    rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)
    bb = align.getLargestFaceBoundingBox(rgbImg)
    if bb is None:
        return 0
    alignedFace = align.align(imgDim, rgbImg, bb, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
    if alignedFace is None:
        return 0

    rep = net.forward(alignedFace)
    return rep

parser = argparse.ArgumentParser()
parser.add_argument('img', type=str, nargs='+', help = "Input images.")
args = parser.parse_args()

imgName = args.img[0]
dirSplit = dirPath.split('/')
v = getRep(imgName)
if type(v) == int:
	print 0
else:
	print('%.10f' % v[0])
	for i in range(1,128):
		print(' %.10f' % v[i])






