
module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  extends: 'airbnb-base',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  /*
  // check if imports actually resolve
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'build/webpack.base.conf.js'
      }
    }
  },
  */
  // add your custom rules here
  'rules': {
    // don't require .vue extension when importing
    'import/extensions': ['error', 'always', {
      'js': 'never',
      'vue': 'never'
    }],
    // allow optionalDependencies
    'import/no-extraneous-dependencies': ['error', {
      'optionalDependencies': ['test/unit/index.js']
    }],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,

    // 'global-require': 0,
    'import/no-unresolved': 0,
    // 'no-param-reassign': 0,
    // 'no-shadow': 0,
    // 'import/extensions': 0,
    'import/newline-after-import': 0,
    // 'no-multi-assign': 0,
    // allow debugger during development
    'no-underscore-dangle': ["error", {
      "allowAfterThis": true
    }],
    'no-console': 0,
    'no-continue': 0,
    'max-len': ['error', 130],
    'linebreak-style': 0
  }
};
