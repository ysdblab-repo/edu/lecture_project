'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return [
        queryInterface.addColumn('lectures', 'group_move', {
          type: Sequelize.INTEGER,
        }, { transaction: t }),
        queryInterface.addColumn('lectures', 'live_type', {
          type: Sequelize.INTEGER,
        }, { transaction: t }),
        queryInterface.addColumn('lectures', 'programming', {
          type: Sequelize.INTEGER,
        }, { transaction: t })
      ];
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return [
        queryInterface.removeColumn('lectures', 'group_move'),
        queryInterface.removeColumn('lectures', 'live_type'),
        queryInterface.removeColumn('lectures', 'programming')
      ];
    });
  }
};
