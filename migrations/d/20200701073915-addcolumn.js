'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn('bank_questions', 'question_media_type', {
          type: Sequelize.TEXT,
          allowNull: true,
        }, { transaction: t }),
        queryInterface.addColumn('bank_questions', 'question_media', {
          type: Sequelize.TEXT,
          allowNull: true,
        }, { transaction: t }),
        queryInterface.addColumn('bank_questions', 'answer_media_type', {
          type: Sequelize.TEXT,
          allowNull: true,
        }, { transaction: t }),
        queryInterface.addColumn('bank_questions', 'choice2', {
          type: Sequelize.TEXT,
          allowNull: true,
        }, { transaction: t }),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('bank_questions', 'question_media_type'),
        queryInterface.removeColumn('bank_questions', 'question_media'),
        queryInterface.removeColumn('bank_questions', 'answer_media_type'),
        queryInterface.removeColumn('bank_questions', 'choice2'),
      ]);
    });
  }
};
