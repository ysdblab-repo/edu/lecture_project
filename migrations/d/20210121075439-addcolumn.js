'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.addColumn('oj_solution', 'errorMsg', {
        type: Sequelize.TEXT,
        defaultValue: '',
      }, { transaction: t });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.removeColumn('oj_solution', 'errorMsg');
    });
  }
};
