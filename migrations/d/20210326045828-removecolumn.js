'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return [
        queryInterface.addColumn('oj_analyses', 'num_assignment', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_comparision_operator', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_class', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.removeColumn('oj_analyses', 'num_loop_upper'),
        queryInterface.removeColumn('oj_analyses', 'num_loop_lower'),
        queryInterface.removeColumn('oj_analyses', 'num_branch_upper'),
        queryInterface.removeColumn('oj_analyses', 'num_branch_lower'),
        queryInterface.removeColumn('oj_analyses', 'num_array_declared'),
        queryInterface.removeColumn('oj_analyses', 'num_int_declared'),
        queryInterface.removeColumn('oj_analyses', 'num_int_used'),
        queryInterface.removeColumn('oj_analyses', 'num_double_declared'),
        queryInterface.removeColumn('oj_analyses', 'num_double_used'),
        queryInterface.removeColumn('oj_analyses', 'num_string_declared'),
        queryInterface.removeColumn('oj_analyses', 'num_string_used'),
        queryInterface.removeColumn('oj_analyses', 'line_per_loop_avg'),
        queryInterface.removeColumn('oj_analyses', 'line_per_branch_avg'),
        queryInterface.removeColumn('oj_analyses', 'line_per_function_avg'),
        queryInterface.removeColumn('oj_analyses', 'ast_fanout_min'),
        queryInterface.removeColumn('oj_analyses', 'ast_fanout_max'),
        queryInterface.removeColumn('oj_analyses', 'ast_fanout_avg'),
        queryInterface.removeColumn('oj_analyses', 'ast_field_min'),
        queryInterface.removeColumn('oj_analyses', 'ast_field_max'),
        queryInterface.removeColumn('oj_analyses', 'ast_field_avg'),
      ];
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return [
        queryInterface.removeColumn('oj_analyses', 'num_assignment'),
        queryInterface.removeColumn('oj_analyses', 'num_comparision_operator'),
        queryInterface.removeColumn('oj_analyses', 'num_class'),
        queryInterface.addColumn('oj_analyses', 'num_loop_upper', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_loop_lower', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_branch_upper', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_branch_lower', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_array_declared', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_int_declared', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_int_used', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_double_declared', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_double_used', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_string_declared', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'num_string_used', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'line_per_loop_avg', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'line_per_branch_avg', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'line_per_function_avg', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'ast_fanout_min', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'ast_fanout_max', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'ast_fanout_avg', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'ast_field_min', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'ast_field_max', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('oj_analyses', 'ast_field_avg', {
          type: Sequelize.FLOAT,
          defaultValue: 0,
        }, { transaction: t }),
      ];
    });
  }
};
