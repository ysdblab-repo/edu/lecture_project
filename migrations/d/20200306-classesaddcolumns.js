'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn('classes', 'view_type', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        }, { transaction: t }),
        queryInterface.addColumn('classes', 'sign_up_start_time', {
          type: Sequelize.DATE,
        }, { transaction: t }),
        queryInterface.addColumn('classes', 'sign_up_end_time', {
          type: Sequelize.DATE,
        }, { transaction: t }),
        queryInterface.addColumn('classes', 'num', {
          type: Sequelize.STRING,
        }, { transaction: t }),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('classes', 'view_type'),
        queryInterface.removeColumn('classes', 'sign_up_start_time'),
        queryInterface.removeColumn('classes', 'sign_up_end_time'),
        queryInterface.removeColumn('classes', 'num'),
      ]);
    });
  },
};
