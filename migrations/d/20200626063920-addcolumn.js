'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn('questions', 'info_item_id', {
          type: Sequelize.INTEGER,
          allowNull: true,
        }, { transaction: t }),
        queryInterface.addColumn('questions', 'info_id', {
          type: Sequelize.INTEGER,
          allowNull: true,
        }, { transaction: t }),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('questions', 'info_item_id'),
        queryInterface.removeColumn('questions', 'info_id'),
      ]);
    });
  }
};
