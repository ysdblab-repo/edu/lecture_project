'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.addColumn('files', 'room_id', {
        type: Sequelize.INTEGER,
        allowNull: true,
        comment: '소그룹방 방번호',
      }, { transaction: t });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.removeColumn('files', 'room_id');
    });
  }
};
