'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addConstraint('questions', ['info_id'], {
        type: 'foreign key',
        name: 'info_id_fk',
        references: {
          table: 'individual_question_infos',
          field: 'info_id',
        },
        onDelete: 'cascade',
        onUpdate: 'cascade',
      }),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeConstraint('questions', 'info_id_fk'),
      ]);
    });
  }
};
