'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.addColumn('student_answer_logs', 'student_local_time', {
        type: Sequelize.DATE,
        defaultValue: null,
      }, { transaction: t });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.removeColumn('student_answer_logs', 'student_local_time');
    });
  }
};
