'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn('student_answer_logs', 'concentration', {
          type: Sequelize.FLOAT,
          allowNull: true,
        }, { transaction: t }),
        queryInterface.addColumn('student_answer_logs', 'transScore', {
          type: Sequelize.FLOAT,
          allowNull: true,
        }, { transaction: t }),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('student_answer_logs', 'concentration'),
        queryInterface.removeColumn('student_answer_logs', 'transScore'),
      ]);
    });
  }
};
