'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.addColumn('student_answer_logs', 'absolute_reactivity', {
        type: Sequelize.FLOAT,
      }, { transaction: t });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.removeColumn('student_answer_logs', 'absolute_reactivity');
    });
  }
};
