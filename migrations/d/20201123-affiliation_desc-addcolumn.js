'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.addColumn('affiliation_descriptions', 'isOpen', {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
      }, { transaction: t });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.removeColumn('affiliation_descriptions', 'isOpen');
    });
  }
};
