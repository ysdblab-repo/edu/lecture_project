'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.addColumn('lecture_homeworks', 'comment', {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      }, { transaction: t });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.removeColumn('lecture_homeworks', 'comment');
    });
  }
};
