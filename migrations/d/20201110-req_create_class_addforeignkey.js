'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.addColumn('req_create_classes', 'id_2nd', {
        type: Sequelize.INTEGER,
        references: {
          model: 'open_space_2nds',
          key: 'id',
        },
        onDelete: 'cascade',
        defaultValue: null
      }, { transaction: t });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.removeColumn('req_create_classes', 'id_2nd');
    });
  }
};
