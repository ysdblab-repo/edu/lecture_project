'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    
    return [
      queryInterface.addColumn('questions', 'multiChoiceMediaType', {
        type: Sequelize.TEXT,
      }),
      queryInterface.addColumn('questions', 'multiChoiceMedia', {
        type: Sequelize.TEXT,
      }),
    ];
    */

    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn('lectures', 'media_type', {
          type: Sequelize.TEXT,
        }, { transaction: t }),
        queryInterface.addColumn('lectures', 'audio_guid', {
          type: Sequelize.TEXT,
        }, { transaction: t }),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    
    return [
      queryInterface.removeColumn('questions', 'multiChoiceMediaType'),
      queryInterface.removeColumn('questions', 'multiChoiceMedia'),
    ];
    */

    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('lectures', 'media_type'),
        queryInterface.removeColumn('lectures', 'audio_guid'),
      ]);
    });
  },
};
