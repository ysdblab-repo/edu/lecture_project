'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    
    return [
      queryInterface.addColumn('questions', 'multiChoiceMediaType', {
        type: Sequelize.TEXT,
      }),
      queryInterface.addColumn('questions', 'multiChoiceMedia', {
        type: Sequelize.TEXT,
      }),
    ];
    */

    // return queryInterface.sequelize.transaction((t) => {
    //   return Promise.all([
    //     queryInterface.addColumn('files', 'question_multichoice', {
    //       type: Sequelize.INTEGER,
    //     }, { transaction: t }),
    //   ]);
    // });

    // raw 쿼리
    // 외래키 제한자를 설정한다.
    var sql = "ALTER TABLE `questions`" +
        "  ADD COLUMN `answer_media_type` TEXT DEFAULT NULL";

    // 쿼리 실행
    return queryInterface.sequelize.query(sql, {
      type: Sequelize.QueryTypes.RAW
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    
    return [
      queryInterface.removeColumn('questions', 'multiChoiceMediaType'),
      queryInterface.removeColumn('questions', 'multiChoiceMedia'),
    ];
    */

    // return queryInterface.sequelize.transaction((t) => {
    //   return Promise.all([
    //     queryInterface.removeColumn('files', 'question_multichoice'),
    //   ]);
    // });

    var sql = "ALTER TABLE `questions`" +
        " DROP COLUMN `answer_media_type`";

    return queryInterface.sequelize.query(sql, {
      type: Sequelize.QueryTypes.RAW
    });
  },
};
