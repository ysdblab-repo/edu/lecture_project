'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.addColumn('req_create_classes', 'subject_type', {
        type: Sequelize.INTEGER,
        allowNull: true,
        comment: '정규과목(폐쇄 과목: 1), 맛보기(시연, 개방 과목: 2)',
      }, { transaction: t });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.removeColumn('req_create_classes', 'subject_type');
    });
  }
};
