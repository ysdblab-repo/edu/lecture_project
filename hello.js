const cluster = require('cluster');
cluster.schedulingPolicy = cluster.SCHED_RR;
exports.forkCount = require('./config').index_core || Math.max(require('os').cpus().length - 1, 1);

if (cluster.isMaster && this.forkCount === 1) {
  const index = require('./index');
  index();
} else if (cluster.isMaster) {
  const { workerHandler } = require('./scripts/node/masterMessageHandler');
  const { scoreingQueue } = require('./modules/score-update');
  const workers = [];
  //워커 생성 함수 정의
  let forkCounter = 0;
  const forkWorker = () => {
    return new Promise((resolve) => {
      const worker = cluster.fork();
      workers.push({
        worker,
        currentScoreingSolution: 0,
      });
      worker.on('message', async message => {
        // 처음 생성시에
        if (message._done) {
          resolve();
        } else {
          workerHandler(message, workers);
        }
      });
    });
  };

  // 종료시 워커 다시 생성
  cluster.on('exit', async (worker, code, signal) => {
    console.log(`Worker ${worker.id} died. Code: ${code}, Signal: ${signal}`);
    await forkWorker();
  });

  (async () => {
    const config = require('./config');
    for (let i = 0; i < this.forkCount; i++) {
      console.log(`forking cluster... ${forkCounter++} / ${this.forkCount}`);
      await forkWorker();
    }
    console.log(`Express is running on port ${config.port} with ${this.forkCount} workers`);

    const sendJob = () => {
      const freeWorkers = workers.filter(workerInfo => workerInfo.currentScoreingSolution === 0);
      freeWorkers.forEach(workerInfo => {
        const solution_id = scoreingQueue.dequeue();
        if (solution_id) {
          workerInfo.currentScoreingSolution = solution_id;
          workerInfo.worker.send({
            job: 'oj_solution_judge',
            solution_id 
          });
        }
      });
      return freeWorkers.length / workers.length;
    };
    // 채점
    const sendJobInterval = () => {
      const workPercent = sendJob();
      setTimeout(() => {
        sendJobInterval();
      }, 1000 * (2 - workPercent));
    }

    sendJobInterval();
  })();

} else {
  const index = require('./index');
  index();
}