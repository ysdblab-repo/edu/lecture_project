const express = require('express');
const cors = require('cors');
const http = require('http');
const https = require('https');
const bodyParser = require('body-parser');
const fs = require('fs');
const config = require('./config');
const app = express();
const modules = require('./modules/frequent-modules');
const { Op } = require("sequelize");

app.use(bodyParser.urlencoded({ extended: false, limit: '1024mb' }));
app.use(bodyParser.json({ limit: '1024mb' }));
app.use(cors());

let options;
if (config.protocol === 'HTTPS' || config.protocol === 'https') {
  options = {
    key: fs.readFileSync('credential/wiseswot.private.key'),
    cert: fs.readFileSync('credential/www.wiseswot.com.crt'),
  };
}

if (config.protocol === 'HTTPS' || config.protocol === 'https') {
  https.createServer(options, app).listen(config.ping_port);
} else {
  http.createServer(app).listen(config.ping_port);
}

app.post('/ping/:number', modules.auth.tokenCheckNotError, async (req, res) => {
  const { number } = req.params;
  
  if (req.decoded) {
    const { authId } = req.decoded;
    const activeLectures = currentActiveLectures.filter(al => al.student_ids.find(si => si === authId));
    const { href } = req.body;

    await Promise.all(activeLectures.map(al => modules.models.lecture_ping_log.create({
      lecture_id: al.lecture_id,
      user_id: authId,
      href,
    })));
  }

  res.json({ res: number });
});

const now = () => new Date(Date.now() + 1000 * 60 * 60 * 9);
let currentActiveLectures = [];

setInterval(async() => {
  currentActiveLectures = await Promise.all((await modules.models.lecture.findAll({
    where: {
      start_time: {
        [Op.lt]: now(),
      },
      end_time: {
        [Op.gt]: now()
      },
      type: 1,
    },
    includes: [{
      model: modules.models.class,
    }]
  })).map(async res => {
    const classData = await modules.models.class.findOne({
      where: {
        class_id: res.dataValues.class_id,
      },
      include: {
        model: modules.models.user_class,
        where: {
          role: 'student'
        }
      }
    });
    return {
      lecture_id: res.lecture_id,
      start_time: res.start_time,
      end_time: res.end_time,
      student_ids: classData.user_classes.map(uc => uc.dataValues.user_id),
    };
  }));
}, 1000);

console.log(`ping_server is running on port ${config.ping_port}`);
