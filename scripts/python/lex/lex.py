import sys, json, tokenize, keyword

tokens = []

for token in tokenize.generate_tokens(sys.stdin.readline):
    tokenType = tokenize.tok_name[token.exact_type]
    tokenString = token.string

    if tokenType == 'NAME':
        if keyword.iskeyword(token.string):
            tokenType = 'KEYWORD'
        else:
            tokenType = 'ID'

    tokens.append({ 'type': tokenType, 'string': tokenString, 'start': str(token.start), 'end': str(token.end) })

print(json.dumps(tokens))