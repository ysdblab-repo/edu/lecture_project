import ast
import subprocess
import sys
import io
import tokenize
import token
import re


def isHangul(text):
    # Check the Python Version
    pyVer3 = sys.version_info >= (3, 0)

    if pyVer3:  # for Ver 3 or later
        encText = text
    else:  # for Ver 2.x
        if type(text) is not unicode:
            encText = text.decode('utf-8')
        else:
            encText = text

    hanCount = len(re.findall(u'[\u3130-\u318F\uAC00-\uD7A3]+', encText))
    return hanCount > 0


def remove_comments(src):
    """ Run on just one file.
    """
    source = io.StringIO(src)
    toks = []

    prev_toktype = token.INDENT
    first_line = None

    tokgen = tokenize.generate_tokens(source.readline)
    for toktype, ttext, (slineno, scol), (elineno, ecol), ltext in tokgen:
        if not (toktype == token.STRING and prev_toktype == token.INDENT) and toktype != tokenize.COMMENT and not isHangul(ttext):
            toks.append([toktype, ttext, (slineno, scol),
                         (elineno, ecol), ltext])

        prev_toktype = toktype

    return tokenize.untokenize(toks)


try:
    import astpretty
except:
    subprocess.check_call(
        [sys.executable, "-m", "pip", "install", "astpretty"])
    import astpretty


class WhileForCombination(ast.NodeVisitor):
    def __init__(self):
        self.combination = ""

    def visit_For(self, node):
        self.combination += "(f"
        self.generic_visit(node)
        self.combination += ")"

    def visit_While(self, node):
        self.combination += "(w"
        self.generic_visit(node)
        self.combination += ")"


class NestedForCounter(ast.NodeVisitor):
    def __init__(self, limit):
        self.limit = limit
        self.current_nested = 0
        self.cnt = 0

    def visit_For(self, node):
        self.current_nested += 1

        if self.limit == self.current_nested:
            self.cnt += 1

        self.generic_visit(node)
        self.current_nested -= 1


class NestedWhileCounter(ast.NodeVisitor):
    def __init__(self, limit):
        self.limit = limit
        self.current_nested = 0
        self.cnt = 0

    def visit_While(self, node):
        self.current_nested += 1

        if self.limit == self.current_nested:
            self.cnt += 1

        self.generic_visit(node)
        self.current_nested -= 1


# abandoned due to structure of ast module
'''
class NestedIfCounter(ast.NodeVisitor):
    def __init__(self, limit):
        self.limit = limit
        self.current_nested = 0
        self.cnt = 0

    def visit_If(self, node):
        self.current_nested += 1

        if self.limit == self.current_nested:
            self.cnt += 1

        self.generic_visit(node)
        self.current_nested -= 1
'''


def count_node_num(tree):
    # count the number of nodes in the tree
    node_cnt = 0
    for node in ast.walk(tree):
        node_cnt += 1

    return node_cnt


def calc_fan_out(tree):
    # calculate the minimum, maximum, and average
    min_fan_out = 1000
    max_fan_out = -1
    total_fan_out = 0

    for node in ast.walk(tree):
        fan_out = len(list(ast.iter_child_nodes(node)))
        # print(list(ast.iter_child_nodes(node)))
        total_fan_out += fan_out
        max_fan_out = max(max_fan_out, fan_out)
        min_fan_out = min(min_fan_out, fan_out)

    return min_fan_out, max_fan_out, total_fan_out / len(list(ast.walk(tree)))


def calc_field(tree):
    # calculate the minimum, maximum, and average
    min_field = 1000
    max_field = -1
    total_field = 0

    for node in ast.walk(tree):
        field = len(list(ast.iter_fields(node)))
        # print(list(ast.iter_fields(node)))
        total_field += field
        max_field = max(max_field, field)
        min_field = min(min_field, field)

    return min_field, max_field, total_field / len(list(ast.walk(tree)))


class DepthCounter(ast.NodeVisitor):
    def __init__(self):
        self.depth = 0
        self.max_depth = 0

    def visit(self, node):
        self.depth += 1
        self.max_depth = max(self.depth, self.max_depth)
        self.generic_visit(node)
        self.depth -= 1


# only counts the number of if statement, not including else.
class IfCounter(ast.NodeVisitor):
    def __init__(self):
        self.cnt = 0

    def visit_If(self, node):
        self.cnt += 1
        self.generic_visit(node)


# main function
# with open("testcase/simple3.py", "r") as f:
#     # Requirement 1. read the source file as a string
#     source_code = f.read()
src_code = []
while True:
    try:
        line = input()
        src_code.append(line)
    except EOFError:
        break

source_code = "\n".join(src_code)
# Requirement 2. parse the source code with ast library
code = remove_comments(source_code)
tree = ast.parse(code)

# Usage 1. You can print the ast.
# print(ast.dump(tree))   # standard library
# astpretty.pprint(tree)  # install astpretty module from pip, it gives better readability

# Usage 2: You can count the usage of nested for loops.
# nested_counter = NestedForCounter(2)    # 2 means only search for inner loop, excluding outer loop
# nested_counter.visit(tree)
# print(nested_counter.cnt)

# Usage 3: You can count the usage of if statement
# It only counts the number of if statement, not including else.
# nested_if_counter = IfCounter()
# nested_if_counter.visit(tree)
# print(nested_if_counter.cnt)

# Usage 4: You can get the loop combination information.
# loop_comb = WhileForCombination()
# loop_comb.visit(tree)
# print(loop_comb.combination)

# Usage 5: You can get the number of nodes in AST.
# print(count_node_num(tree))

# Usage 6. You can get the fan-out information of the AST.
# The function returns (max, min, average) fan-out.
# print(calc_fan_out(tree))

# Usage 7. You can get field information of the AST.
# The function returns (max, min, average) field of the AST.
# For example, "If" node has condition field.
# print(calc_field(tree))

# Usage 8. You can get the depth(height) of the AST.
depth_counter = DepthCounter()
depth_counter.visit(tree)
min_fan_out, max_fan_out, avg_fan_out = calc_fan_out(tree)
min_field, max_field, avg_field = calc_field(tree)
print(count_node_num(tree), min_fan_out, max_fan_out, avg_fan_out,
      min_field, max_field, avg_field, depth_counter.max_depth, sep=",", end="")
# print(depth_counter.max_depth, end="")
