const { doneQueue } = require('../../modules/score-update');

exports.workerHandler = async (message, workers) => {
  switch(message.job) {
    case 'oj_solution_judge': {
      if (message.state === 'done') {
        workers.find(workerInfo => workerInfo.currentScoreingSolution === message.solution_id).currentScoreingSolution = 0;
        doneQueue.enqueue(message.solution_id);
      }
      break;
    }
  }
};