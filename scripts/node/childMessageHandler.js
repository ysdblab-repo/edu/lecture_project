const { oj_solution_judge } = require('../../modules/scoreing');
const modules = require('../../modules/frequent-modules');

exports.workerHandler = async message => {
  switch(message.job) {
    case 'oj_solution_judge': {
      let error;
      try {
        await oj_solution_judge(message.solution_id);
      } catch (err) {
        error = err;
      }

      if (error) {
        await modules.models.oj_solution.update({
          errorMsg: error.toString(),
          judged: 1,
        }, {
          where: {
            solution_id: message.solution_id
          }
        });
      }
      process.send({
        job: 'oj_solution_judge',
        state: 'done',
        solution_id: message.solution_id
      });
      break;
    }
  }
};