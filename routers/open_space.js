const express = require('express');
const { json } = require('sequelize');
const router = express.Router();
const sequelize = require('sequelize');
const { Sequelize } = require('../models');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const { Op } = sequelize;

// 1st 카테고리
router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  const category = await modules.models.open_space_1st.findAll();

  res.json(category);
}));

// 2nd 카테고리 전체 분야
router.get('/second', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  const sql = `SELECT distinct id_1st, name FROM open_space_2nds`;
  const result = await db.getQueryResult(sql);

  res.json(result);
}));

router.get('/second/list', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
    
  const id_1st = parseInt(req.query.id_1st, 10);
  const name = req.query.name;

  const category = await modules.models.open_space_2nd.findAll({
    where: {
      id_1st,
      name
    }
  });
  
  const promises = category.map(x => modules.models.req_create_class.findAll({
    include: [{
      model: modules.models.affiliation_description,
      required: true,
      where: {
        isOpen: {
          [Op.not]: 0,
        },
        affiliation_id: x.dataValues.affiliation_id,
      }
    }, {
      model: modules.models.class,
      include: {
        model: modules.models.user,
        attributes: modules.models.user.selects.userJoined,
        as: 'master',
      },
      where: {
        isHide: 0
      },
      required: true,
    }],
    where: {
      [Op.or]: [{
        id_2nd: x.dataValues.id
      }, {
        id_2nd: null
      }],
    }
  }));
  const result = await Promise.all(promises);

  res.json({ result });
}));

// 검색한 기관 가져오기
router.get('/find/:affiliation_name', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  const name = req.params.affiliation_name;
  const category = await modules.models.affiliation_description.findAll({
    where: {
      description: name
    }
  });

  const promises = category.map(x => modules.models.req_create_class.findAll({
    include: [{
      model: modules.models.affiliation_description,
      required: true,
      where: {
        isOpen: 1,
        affiliation_id: x.dataValues.affiliation_id,
      }
    }, {
      model: modules.models.class,
      include: {
        model: modules.models.user,
        attributes: modules.models.user.selects.userJoined,
        as: 'master',
      },
      required: true,
    }],
  }));
  const result = await Promise.all(promises);

  res.json({ category, result });
}));

// preswot 공지사항 가져오기
router.get('/preswotNotify', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  const category = await modules.models.affiliation_description.findAll({
    where: {
      isOpen: 2
    }
  });

  const promises = category.map(x => modules.models.req_create_class.findAll({
    include: [{
      model: modules.models.affiliation_description,
      required: true,
      where: {
        isOpen: 2,
        affiliation_id: x.dataValues.affiliation_id,
      }
    }, {
      model: modules.models.class,
      include: {
        model: modules.models.user,
        attributes: modules.models.user.selects.userJoined,
        as: 'master',
      },
      required: true,
    }],
  }));
  const result = await Promise.all(promises);

  res.json({ result });
}));

module.exports = router;