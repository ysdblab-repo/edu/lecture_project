const express = require('express');
const router = express.Router();
const getmac = require('getmac');
const modules = require('../modules/frequent-modules');
const date = require('date-and-time');
const db = require('../modules/db');
const coverage = require('../modules/coverage-calculator');
const crypto = require('crypto');
router.get('/location',modules.asyncWrapper( async (req, res) => {
  const sql = `select * from locations`;
  const result = await db.getQueryResult(sql);
  res.json(result);
}))
router.post('/export-excel', modules.asyncWrapper( async (req, res) => {

  let { query,attributes } = req.body;
  let result = await db.getQueryResult(query);
  attributes = attributs.split(' ');
  let resultlists = []
  for ( let i = 0 ; i < result.length ; i += 1 ) {
    for ( let j = 0 ; j < attributes.length ; j += 1 ) {
      let res = {};
      res[attributes[j]] = result[i][attributes[j]];
      resultlists.push(res);
    }
  }
  
}));
router.get('/:id', modules.asyncWrapper(async( req, res , next) => {

  const key1 = 'databaseLab';
  const key2 = 'databaselab';

  var text = 'node js is javascript'
  var cipher = crypto.createCipher('aes192',key1);
  cipher.update(text, 'utf8','base64');
  var cipheredOutput = cipher.final('base64');

  var decipher = crypto.createDecipher('aes192',key1);
  decipher.update(cipheredOutput, 'base64','utf8');
  var decipheredOutput = decipher.final('utf8');

  
  // var decipher2 = crypto.createDecipher('aes192',key2);
  // decipher2.update(cipheredOutput, 'base64','utf8');
  // var decipheredOutput2 = decipher2.final('utf8');

  console.log('기존문자열 '+input);
  console.log('암호화된 문자열: '+cipheredOutput);
  console.log('복호화된 문자열' + decipheredOutput);

  // console.log('복호화된 문자열' + decipheredOutput2);


}));

router.get('/:id', modules.asyncWrapper(async(req, res,next) => {
  const lectureId = parseInt(req.params.id,10);
  const sql = `select lecture_item_id from lecture_items where lecture_id = ${lectureId}`;
  let result = await db.getQueryResult(sql);
  let itemIdList = [];
  for ( let i = 0; i < result.length; i += 1) {
    itemIdList.push(result[i].lecture_item_id);
  }
  const surveyTotal = `select count(*) as c from surveys where lecture_item_id in (${itemIdList})`;
  const surveyStudents = `select student_id, count(*) from student_surveys where survey_id in (select survey_id from lecture_items as i,
    surveys as s where i.lecture_item_id = s.lecture_item_id and  i.lecture_id = ${lectureId}) group by student_id `;

  const questionTotal = `select count(*) as c from questions where lecture_item_id in (${itemIdList})`;
  const questionStudents = `select student_id, count(*) from student_answer_logs where question_id in (select question_id from lecture_items as i,
    questions as s where i.lecture_item_id = s.lecture_item_id and  i.lecture_id = ${lectureId}) group by student_id `;

  // const discussionTotal = `select count(*) as c from surveys where lecture_item_id in (${itemIdList})`;
  // const duscussionStudents = `select student_id, count(*) from student_surveys where survey_id in (select survey_id from lecture_items as i,
  //   surveys as s where i.lecture_item_id = s.lecture_item_id and  i.lecture_id = 135) group by student_id `;

  // const discussionTotal = `select count(*) as c from surveys where lecture_item_id in (${itemIdList})`;
  // const duscussionStudents = `select student_id, count(*) from student_surveys where survey_id in (select survey_id from lecture_items as i,
  //   surveys as s where i.lecture_item_id = s.lecture_item_id and  i.lecture_id = 135) group by student_id `;

  let qt = await db.getQueryResult(questionTotal);

  res.json(qt);
}));
// router.get('/:id', modules.asyncWrapper(async (req, res, next) => {

//   const lectureId = parseInt(req.params.id,10);

//   coverage.newlecture(lectureId)
//   res.json(lectureId);
// }));

router.post('/', modules.asyncWrapper(async (req, res, next) => {
 
  const { user_id, lecture_id } = req.body;

  // lecture에 대한 user 접속 로그가 있을 경우 해당 offset 을 더해줘서 중간부터 시작하게.
  // const sql = `SELECT * FROM table WHERE user_id = ${user_id} AND lecture_id = ${lecture_id}`;
  // const result = await db.getQueryResult(sql);
  // if ( result.length > 0 ) offset = result[0].offset;
  // if ( result.length == 0 ) 
  // const sql = `INSERT IGNORE INTO table VALUES(${user_id}, ${lecture_id});
  // db.getQueryResult(sql);

  const lec = await modules.models.lecture.findOne({
    where: { lecture_id: lecture_id },
    include: { model: modules.models.lecture_item, required: true },
  });
  const curtime = Math.floor(new Date().getTime()/1000);
  const starttime = lec.start_time/1000;
  const endtime = lec.end_time/1000;
  // console.log(endtime-starttime);
  // console.log(endtime);
  // console.log(curtime);

  let lecItems = [];
  let resttime = 0; // 지난 시간들
  if( curtime < starttime ) {
    sendSignalToLecture(io, 'not yet', 'RELOAD_LECTURE_ITEMS', {'reload': true});
  }
   else if ( curtime > endtime ) {
    sendSignalToLecture(io, 'the end', 'RELOAD_LECTURE_ITEMS', {'reload': true});
  } else {
    for ( let i = 0; i < lec.lecture_items.length; i += 1 ) {
      let t = starttime + lec.lecture_items[i].offset * 1000;
      console.log(`${i} curtime - ${curtime/1000} ${t/1000} , starttime - ${starttime/1000}`);
      if ( starttime + lec.lecture_items[i].offset < curtime ) continue;
      lecItems.push(lec.lecture_items[i]);
    }
  }
  for ( let i = 0; i < lecItems.length; i+= 1 ) {
    lecItems[i].offset = Math.floor(( starttime + lecItems[i].offset * 1000 - curtime ) / 1000);
  }
  // console.log(lecItems.length);
  res.json(lecItems);

}));

module.exports = router;
