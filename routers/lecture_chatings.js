const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
var multer  = require('multer');

// 실시간 채팅 등록
router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lectureId = req.body.lecture_id;
  const content = req.body.content;

  let is_teacher = 0;
  const lecture = await modules.models.lecture.findByPk(lectureId);
  if (!lectureId) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class_id);
  if ((authType === 3 || teacherCheck)) {
	  is_teacher = 1;
  }

  const lectureChating = await modules.models.lecture_chating.create({
    lecture_id: lectureId,
    user_id: authId,
    is_teacher : is_teacher,
    content : content,
  });

  res.json({ success: true, lecture_chating_id: lectureChating.lecture_chating_id });
}));

// 실시간 채팅 가져오기
router.get('/:lecture_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.lecture_id, 10);

  const chatData = await modules.models.lecture_chating.findAll({
    where: {
      lecture_id: lectureId,
    },
    include: [
      { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
    ],
  });
  res.json({ chatData });
}));

// 해당 강의 채팅 내용 삭제 200511
router.delete('/:lecture_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.lecture_id, 10);

  // DB에서는 삭제하지 않고 connection 끊고 남김
  await modules.models.lecture_chating.update({
    lecture_id: null,
    origin_lecture_id: lectureId,
  }, {
    where: { lecture_id: lectureId },
  });
  
  res.json({ success: true });
}));

module.exports = router;
