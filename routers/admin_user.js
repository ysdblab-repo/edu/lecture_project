const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const modules = require('../modules/frequent-modules');

// 인증 (공통)
router.use(modules.auth.tokenCheck);
router.use(modules.auth.checkTokenIsAdmin);


// 유저 등록
router.post('/', modules.asyncWrapper(async (req, res) => {
  const {
    belongs,
    email_id,
    password,
    name,
    type,
    birth,
    sex,
    address,
    phone,
    major,
    career,
    account_bank,
    account_number,
  } = req.body;

  // 유효성 검사
  if (email_id === undefined) throw new Error('email_id은(는) 반드시 입력해야 합니다.');
  if (name === undefined) throw new Error('name은(는) 반드시 입력해야 합니다.');
  if (password === undefined) throw new Error('password은(는) 반드시 입력해야 합니다.');
  if (type === undefined) throw new Error('type은(는) 반드시 입력해야 합니다.');
  if (type !== '0' && type !== '1') throw new Error('type은(는) 0또는 1을 입력해야 합니다.');

  // belongs의 대학, 학과가 유효한 값인지 확인하고, 유효하다면 학과 id를 추출하여 배열에 저장
  const department_id_list = [];
  if (Array.isArray(belongs) === true
    && belongs.length > 0) {
    for (let i = 0; i < belongs.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      const res2 = await modules.models.department.findOne({
        attributes: ['department_id'],
        where: {
          '$university.name$': belongs[i].university_name,
          name: belongs[i].department_name,
        },
        include: {
          model: modules.models.university,
        },
      });
      if (res2 === null) {
        throw new Error(` 등록되지 않은 대학/학과 쌍 ${belongs[i].university_name} / ${belongs[i].department_name} 을 입력하였습니다.`);
      }
      if (department_id_list.includes(res2.department_id) !== false) {
        throw new Error(` 중복된 대학/학과 쌍 ${belongs[i].university_name} / ${belongs[i].department_name} 이 포함되었습니다.`);
      }
      department_id_list.push(res2.department_id);
    }
  }

  // 패스워드 암호화
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');

  // 유저 등록
  const res3 = await modules.models.user.create({
    email_id,
    password: encrypted,
    name,
    type,
    birth,
    sex,
    address,
    phone,
    major,
    career,
    account_bank,
    account_number,
  });

  // 소속 관계 등록
  for (let i = 0; i < department_id_list.length; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    await res3.addBelongs([department_id_list[i]]);
  }

  // 강사인 경우, student. 계정 추가 생성
  if (type === 1) {
    // 유저 등록
    const res4 = await modules.models.user.create({
      email_id: `student.${email_id}`,
      password: encrypted,
      name,
      type: 0,
      birth,
      sex,
    });

    // 소속 관계 등록
    for (let i = 0; i < department_id_list.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      await res4.addBelongs([department_id_list[i]]);
    }
  }

  // 응답
  res.json({ success: true });
}));

// 목록,건수 조회
router.get('/list', modules.asyncWrapper(async (req, res) => {
  const { type } = req.query; // eslint-disable-line
  let { university_name, department_name, category, search_word, page, limit } = req.query;

  // 유효성 검사
  if (type === undefined) throw new Error('type 은(는) 반드시 입력해야 합니다.');
  if (type !== '0' && type !== '1') throw new Error('조회 가능한 type값이 아닙니다.');
  if (category !== undefined
    && category !== 'name'
    && category !== 'email_id') throw new Error('category는 name 또는 email_id 또는 (undefined)를 입력해야 합니다.');
  if (page === undefined
    || page < 1) page = 1;
  if (search_word === undefined) search_word = '';
  if (limit === undefined) limit = 15;
  limit = Number(limit);
  if (limit > 100) limit = 100;

  const where = { type: Number(type) };
  if (category === 'name') where.name = { like: `%${search_word}%` };
  if (category === 'email_id') where.email_id = { like: `%${search_word}%` };

  let department_id_list;
  // 대학이름 있는 경우 전처리
  if (university_name !== undefined) {
    const where = { '$university.name$': university_name };
    // 학과이름 있는 경우 where 조건에 포함
    if (department_name !== undefined) where.name = department_name;

    const res2 = await modules.models.department.findAll({
      attributes: ['department_id'],
      where,
      include: {
        model: modules.models.university,
      },
    });
    department_id_list = res2.map(x => x.department_id);
  }

  // 본 쿼리
  let rows;
  let count;
  if (department_id_list !== undefined) {
    rows = await modules.models.sequelize.query(
      'SELECT email_id, name'
      + ' FROM users'
      + ' WHERE user_id IN'
      + ' ('
        + ' SELECT DISTINCT user_id'
        + ' FROM user_belongs '
        + ' WHERE department_id IN (:department_id_list)'
      + ' )'
      + ' LIMIT :offset, :limit',
      {
        replacements: {
          department_id_list, offset: (page - 1) * limit, limit,
        },
        type: modules.models.sequelize.QueryTypes.SELECT,
      },
    );
    count = await modules.models.sequelize.query(
      'SELECT COUNT(*)'
      + ' FROM users'
      + ' WHERE user_id IN'
      + ' ('
        + ' SELECT DISTINCT user_id'
        + ' FROM user_belongs '
        + ' WHERE department_id IN (:department_id_list)'
      + ' )'
      + ' LIMIT :offset, :limit',
      {
        replacements: {
          department_id_list, offset: (page - 1) * limit, limit,
        },
        type: modules.models.sequelize.QueryTypes.SELECT,
      },
    );
    res.json({ count: count[0]['COUNT(*)'], rows });
  } else {
    // 조회
    const userList = await modules.models.user.findAndCountAll({
      attributes: ['user_id', 'email_id', 'name'],
      where,
      offset: (page - 1) * limit,
      limit: limit,
    });
    res.json(userList);
  }
}));

// 수정
router.put('/', modules.asyncWrapper(async (req, res) => {
  const {
    belongs,
    email_id,
    new_email_id,
    password,
    name,
    type,
    birth,
    sex,
    address,
    phone,
    major,
    career,
    account_bank,
    account_number,
  } = req.body;

  // 유효성 검사
  if (email_id === undefined) throw new Error('email_id은(는) 반드시 입력해야 합니다.');

  // belongs의 대학, 학과가 유효한 값인지 확인하고, 유효하다면 학과 id를 추출하여 배열에 저장
  const department_id_list = [];
  if (Array.isArray(belongs) === true
    && belongs.length > 0) {
    for (let i = 0; i < belongs.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      const res2 = await modules.models.department.findOne({
        attributes: ['department_id'],
        where: {
          '$university.name$': belongs[i].university_name,
          name: belongs[i].department_name,
        },
        include: {
          model: modules.models.university,
        },
      });
      if (res2 === null) {
        throw new Error(` 등록되지 않은 대학/학과 쌍 ${belongs[i].university_name} / ${belongs[i].department_name} 을(를) 입력하였습니다.`);
      }
      if (department_id_list.includes(res2.department_id) !== false) {
        throw new Error(` 중복된 대학/학과 쌍 ${belongs[i].university_name} / ${belongs[i].department_name} 이 포함되었습니다.`);
      }
      department_id_list.push(res2.department_id);
    }
  }

  // email_id가 유효하다면 user_id를 추출
  const res3 = await modules.models.user.findOne({
    attributes: ['user_id'],
    where: {
      email_id,
    },
  });
  if (res3 === null) {
    throw new Error(` 등록되지 않은 이메일 ${email_id} 을(를) 입력하였습니다.`);
  }
  const { user_id } = res3;

  let encrypted;
  if (password !== undefined) {
    // 패스워드 암호화
    encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
      .update(password)
      .digest('hex');
  }

  // 유저 정보 수정
  const count = await modules.models.user.count({
    where: { email_id },
  });
  if (count !== 1) throw new Error('update 대상이 1열이 아닙니다.');
  await modules.models.user.update({
    password: encrypted,
    email_id: new_email_id,
    name,
    type,
    birth,
    sex,
    address,
    phone,
    major,
    career,
    account_bank,
    account_number,
  }, {
    where: { email_id },
  });

  // 관계 수정
  await modules.models.user_belong.destroy({
    where: { user_id },
  });
  for (let i = 0; i < department_id_list.length; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    await modules.models.user_belong.create({
      user_id,
      department_id: department_id_list[i],
    });
  }

  res.json({ success: true });
}));

// 수정할 유저 정보 조회
router.get('/', modules.asyncWrapper(async (req, res) => {
  const { email_id } = req.query;

  // 유효성 검사
  if (email_id === undefined) throw new Error('email_id 은(는) 반드시 입력해야 합니다.');

  // 조회
  const user = await modules.models.user.findOne({
    attributes: modules.models.user.selects.set1,
    where: { email_id },
    include: {
      attributes: [['name', 'department_name']],
      as: 'belongs',
      model: modules.models.department,
      include: {
        attributes: [['name', 'university_name']],
        model: modules.models.university,
      },
    },
  });

  res.json(user);
}));

router.delete('/', modules.asyncWrapper(async (req, res) => {
  const { email_id } = req.query;

  // 유효성 검사
  if (email_id === undefined) throw new Error('email_id 은(는) 반드시 입력해야 합니다.');

  // 조회
  const user = await modules.models.user.findOne({
    attributes: ['type'],
    where: { email_id },
  });

  if (user === null) {
    throw new Error(`${email_id} 은(는) 존재하지 않는 유저입니다.`);
  }

  if (user.type !== 0
    && user.type !== 1) {
    throw new Error('관리자 계정은 삭제할 수 없습니다.');
  }

  // 삭제
  const res2 = await modules.models.user.destroy({
    where: { email_id },
  });

  res.json({ success: true });
}));

// 에러 처리
router.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({ success: false, message: err.message });
});

module.exports = router;
