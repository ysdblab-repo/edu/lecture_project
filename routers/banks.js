const express = require('express');
const router = express.Router();
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const child_process = require("child_process");
const execCmd = require("child_process").exec;
const execTimeout10 = require('../src/utility');
const path = require('path');
const fs = require('fs');
const { Op } = require("sequelize");


// 현재 로그인한 강사가 속한 group 목록 반환
router.get('/groups', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  if (authType === 0) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  const groupList = await modules.models.bank_group.findAll({
    include: {
      model: modules.models.teacher_group,
      where: { user_id: authId },
      required: true,
    },
  });

  res.json({ groupList });
}));

router.get('/list', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const { authId } = req.decoded;
  const groups = await modules.models.teacher_group.findAll({ where: { user_id: authId }});
  let group_ids = [];
  if(groups.length === 0) {
    res.json({ classes: [], lectures: [], lecture_items: []});
  }
  for (let i = 0; i < groups.length ; i += 1) {
    group_ids.push(groups[i].group_id);
  }
  let ids_str = group_ids.join(',');
  const sql1 = `SELECT * FROM bank_classes WHERE group_id in (${ids_str})`;
  const sql2 = `SELECT * FROM bank_lectures WHERE group_id in (${ids_str})`;
  const sql3 = `SELECT * FROM bank_lecture_items WHERE group_id in (${ids_str})`;

  const result1 = await db.getQueryResult(sql1);
  const result2 = await db.getQueryResult(sql2);
  const result3 = await db.getQueryResult(sql3);
  res.json({ classes: result1, lectures: result2, lecture_items: result3 });

}));

/*
  편의상 BANK와 ACTIVE로 나누도록 하겠습니다.
  origin 으로 시작하는게 ACTIVE에 속한 과목/강의/강의아이템 혹은 기타 하위 항목(keyword, coverage 등)입니다.

*/
// 은행에 과목 저장하기
router.post('/class', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  // class  ( borad , plist 는 추가 안함 )
  // lecture(keyword (relation))
  // lecture-item , material(keyword), file
  // question(keyword), note(keyword), survey(keyword), homework(keyword), lecture_code_practice, discussion_info
  // oj_problem, oj_problem_testcase
  // ------------------------------------------------------
  // 복사할 file 종류 - material, note, question 3종류
  const { authId, authType } = req.decoded;
  const { class_id, group_id } = req.body;

  const classId = class_id;
  const groupId = group_id;

  const teacherCheck = await modules.teacherInGroup(authId, groupId); // 강사가 groupd에 속해있는지 판단하는 모듈

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const originClass = await modules.models.class.findByPk(classId, {
    include: {
      model: modules.models.lecture,
      include: {
        model: modules.models.lecture_item,
      }
    },
  });

  if (!originClass) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }


  const originLectures = originClass.lectures;
  // make bank history - type 0
  const history = await modules.models.bank_history.create({
    creator_id: authId,
    type: 0
  });

  const newBankClass = await modules.models.bank_class.create({
    name: originClass.name,
    summary: originClass.summary,
    description: originClass.description,
    capacity: originClass.capacity,
    start_time: originClass.start_time,
    end_time: originClass.end_time,
    opened: originClass.opened,
    lecturer_description: originClass.lecturer_description,
    origin_created_at: originClass.created_at,
    origin_updated_at: originClass.updated_at,
    latest_store_teacher_id: authId,
    group_id: groupId,
    bank_history_id: history.bank_history_id
  });

  const newLectures = originLectures.map(item => {
    return {
      class_id: newBankClass.class_id,
      name: item.name,
      location: item.location,
      type: item.type,
      start_time: item.start_time,
      end_time: item.end_time,
      is_auto: item.is_auto,
      video_link: item.video_link,
      keyword_state: item.keyword_state,
      teacher_id: item.teacher_id,
      latest_store_teacher_id: authId,
      bank_history_id: history.bank_history_id,
      group_id: groupId,
    }
  });
// class coverage 안쓰는 항목
  // let newBankClassCoverage = null;
  // if (originClassCoverage) { // coverage가 없는 경우가 있을 수도 있어서 넣어놧습니다. 
  //   newBankClassCoverage = await modules.models.bank_class_coverage.create({
  //     class_id: newBankClass.class_id,
  //     question_error: originClassCoverage.question_error,
  //     material_error: originClassCoverage.material_error,
  //   });
  // }

  // const newClassKeywords = originClassKeywords.map(item => {
  //   return {
  //     class_id: newBankClass.class_id,
  //     keyword: item.keyword,
  //     weight: item.weight,
  //     weight_ratio: item.weight_ratio,
  //     question_score_ratio: item.question_score_ratio,
  //     material_score_ratio: item.material_score_ratio,
  //     difference_question: item.difference_question,
  //     difference_material: item.difference_material,
  //   };
  // });
  // const newBankClassKeywords = await modules.models.bank_class_keyword.bulkCreate(newClassKeywords);
  
  // 새로운 강의를 만들기 위함
 
  const newBankLectures = await Promise.all(originLectures.map(lc => (async () => {

    const lectureId = lc.lecture_id;
    console.log(`lectureid - ${lectureId}`);

    const originLectureKeywords = await modules.models.lecture_keyword.findAll({
      where: { lecture_id: lectureId, },
    });
    const originLectureKeywordRelations = await modules.models.lecture_keyword_relation.findAll({
      where: { lecture_id: lectureId, },
    });
    const originLectureMaterials = await modules.models.material.findAll({
      where: { lecture_id: lectureId, },
      include: { model: modules.models.file },
    });
    // const originPlist = await modules.models.lecture_accept_plist.findAll({
    //   where: { lecture_id: lectureId, },
    // });

    const newLecture = {
      class_id: newBankClass.class_id,
      name: lc.name,
      location: lc.location,
      type: lc.type,
      start_time: lc.start_time,
      end_time: lc.end_time,
      is_auto: lc.is_auto,
      video_link: lc.video_link,
      keyword_state: lc.keyword_state,
      bank_history_id: history.bank_history_id,
      group_id: groupId
    };
    const newBankLecture = await modules.models.bank_lecture.create(newLecture);

    const newLectureKeywords = originLectureKeywords.map(item => {
      return {
        lecture_id: newBankLecture.lecture_id,
        keyword: item.keyword,
        weight: item.weight,
        weight_ratio: item.weight_ratio,
        question_score_ratio: item.question_score_ratio,
        material_score_ratio: item.material_score_ratio,
        difference_question: item.difference_question,
        difference_material: item.difference_material,
        bank_history_id: history.bank_history_id
      };
    });
    let newBankLectureKeywords = null;
    if (newLectureKeywords) {
      newBankLectureKeywords = await modules.models.bank_lecture_keyword.bulkCreate(newLectureKeywords);
    }

    const newLectureKeywordRelations = originLectureKeywordRelations.map(item => {
      return {
        lecture_id: newBankLecture.lecture_id,
        node1: item.node1,
        node2: item.node2,
        weight: item.weight,
        bank_history_id: history.bank_history_id
      };
    });
    let newBankLectureKeywordRelations = null;
    if (newLectureKeywordRelations) {
      newBankLectureKeywordRelations = await modules.models.bank_lecture_keyword_relation.bulkCreate(newLectureKeywordRelations);
    }
    const newBankMaterials = await Promise.all(originLectureMaterials.map(material => (async () => {
      const newBankMaterial = await modules.models.bank_material.create({
        comment: material.comment,
        score_sum: material.score_sum,
        lecture_id: newBankLecture.lecture_id,
      //  creator_id: material.creator_id, 가져올때 바뀔거라서 필요없음
        material_type: material.material_type,
        bank_history_id: history.bank_history_id
      });
      const file = material.file;
      const originLectureMaterialKeywords = await modules.models.material_keyword.findAll({ 
        where: { material_id: material.material_id, },
      });
      const newMaterialKeywords = originLectureMaterialKeywords.map((item) => {
        return {
          lecture_id: newBankLecture.lecture_id,
          material_id: newBankMaterial.material_id,
          keyword: item.keyword,
          score: item.score,
          bank_history_id: history.bank_history_id
        }
      });
      const newBankMaterialKeywords = await modules.models.bank_material_keyword.bulkCreate(newMaterialKeywords);
      const newBankMaterialFile = await modules.upload.copy(file.static_path, file.name, 'material_id', newBankMaterial.material_id, authId, null, 0);
      return {
        newBankMaterial,
        newBankMaterialKeywords,
        newBankMaterialFile
      };
    })()));

    // const newBankLectureAcceptPlist = await Promise.all(originPlist.map(program => (async () => {
    //   modules.models.bank_lecture_accept_plist.create({
    //     plist_id: program.plist_id,
    //     lecture_id: newBankLecture.lecture_id,
    //   });
    // })()));

    const originLectureItems = await modules.models.lecture_item.findAll({
      where: { lecture_id: lectureId },
    });

    let item_id_dic = {};

    const newBankLectureItems = await Promise.all(originLectureItems.map(lcItem => (async () => {
      const newBankLectureItem = await modules.models.bank_lecture_item.create({
        name: lcItem.name,
        start_time: lcItem.start_time,
        end_time: lcItem.end_time,
        offset: lcItem.offset,
        type: lcItem.type,
        past_opend: 0,
        order: lcItem.order,
        sequence: lcItem.sequence,
        result: lcItem.result,
        opend: 0,
        scoring_finish: 0,
        lecture_id: newBankLecture.lecture_id,
        bank_history_id: history.bank_history_id,
        latest_store_teacher_id: authId,
        group_id: groupId,
      });
      const newBankLectureItemId = newBankLectureItem.lecture_item_id;
      
      item_id_dic[lcItem.lecture_item_id] = newBankLectureItemId;

    switch(newBankLectureItem.type){
        // 0: 문제(question), 1: 설문(survey), 2: 실습(practice), 3: 토론(discussion) 4: 자료(note)
      
      case 0:
        const originQuestions = await modules.models.question.findOne({
        where: { lecture_item_id: lcItem.lecture_item_id },
        include: [
          { model: modules.models.oj_problem, },
          { model: modules.models.problem_testcase },
          { model: modules.models.file, as: 'question_material' },
          { model: modules.models.file },
        ],
        });
        const newBankQuestion = await modules.models.bank_question.create({
          lecture_item_id: newBankLectureItemId,
          type: originQuestions.type,
          question: originQuestions.question,
          choice: originQuestions.choice,
          answer: originQuestions.answer,
          is_ordering_answer: originQuestions.is_ordering_answer,
          accept_language: originQuestions.accept_language,
          order: originQuestions.order,
          showing_order: originQuestions.showing_order,
          difficulty: originQuestions.difficulty,
          timer: originQuestions.timer,
          creator_id: originQuestions.creator_id,
          //sqlite_file_guid: item.sqlite_file_guid, // 나중에 복사한 guid 의 값을 넣어준다.
          bank_history_id: history.bank_history_id,
          choice2: originQuestions.choice2,
          question_media: originQuestions.question_media,
          question_media_type: originQuestions.question_media_type,
          answer_media_type: originQuestions.answer_media_type,
        });
        // create question_keyword
        // question keyword -> foreign key로 묶이지 않았기에 별도로 호출해서 넣어준다.
        const originQuestionKeywords = await modules.models.question_keyword.findAll({
          where: { lecture_item_id: lcItem.lecture_item_id  }
        });
        await Promise.all(originQuestionKeywords.map(keyword => (async () =>{
          await modules.models.bank_question_keyword.create({
            question_id: newBankQuestion.question_id,
            keyword: keyword.keyword,
            lecture_id: newBankLecture.lecture_id,
            lecture_item_id: newBankLectureItemId,
            score_portion: keyword.score_portion,
            bank_history_id: history.bank_history_id,
          })
        })()));


        const ojProblem = originQuestions.oj_problem;
        const problemTestCases = originQuestions.problem_testcases;
        let bankOjProblem = null;
        if (ojProblem !== null) {
          const bankOjProblem = await modules.models.bank_oj_problem.create({
            problem_id: newBankQuestion.question_id,
            title: ojProblem.title,
            description: ojProblem.description,
            input: ojProblem.input,
            output: ojProblem.output,
            sample_input: ojProblem.sample_input,
            sample_output: ojProblem.sample_output,
            spj: ojProblem.spj,
            hint: ojProblem.hint,
            source: ojProblem.source,
            in_date: ojProblem.in_date,
            time_limit: ojProblem.time_limit,
            memory_limit: ojProblem.memory_limit,
            defunct: ojProblem.defunct,
            accepted: ojProblem.accepted,
            submit: 0, // 제출자, 정답자 0으로 초기화
            solved: 0, // 
            bank_history_id: history.bank_history_id,
          });
        }

        let bankProblemTestCases = [];
        // testcase create 시 home/judged 에 문항이 생성되는지 확인할 것 - 지건호
        if (problemTestCases.length > 0) {

          bankProblemTestCases = await Promise.all(problemTestCases.map(testcase => (async () => {
            const bankProblemTestCase = await modules.models.bank_problem_testcase.create({
              question_id: newBankQuestion.question_id,
              num: testcase.num,
              input: testcase.input,
              output: testcase.output,
              input_path: testcase.input_path,
              output_path: testcase.output_path,
              bank_history_id: history.bank_history_id,
            });
            return bankProblemTestCase;
          })()));

        }
      
      
        ///////////////////////////////////////////////////////////////////////
        const materialFiles = originQuestions.question_material;
        if ( materialFiles.length > 0) {
          materialFiles.forEach(async function(element){
              modules.upload.copy(element.static_path, element.name, 'question_id_material', newBankQuestion.question_id, authId, newBankLectureItemId, 0);
          });
        }
        const files = originQuestions.files;
        if(files.length > 0){
          files.forEach(async function(element){
            modules.upload.copy(file.static_path, file.name, 'question_id', newBankQuestion.question_id, authId, newBankLectureItemId, 0);
        })
        }
      
        break; // question case break

      case 1: // survey
        const originSurveys = await modules.models.survey.findOne({
          where: { lecture_item_id: lcItem.lecture_item_id },
          // include: { model: modules.models.file },  // 서비스 프론트에 survey에는 file이 없음.
        });
        const newBankSurvey = await modules.models.bank_survey.create({
          lecture_item_id: newBankLectureItemId,
          type: originSurveys.type,
          comment: originSurveys.comment,
          choice: originSurveys.choice,
          creator_id: originSurveys.creator_id,
          bank_history_id: history.bank_history_id,
        });
        const originSurveyKeywords = await modules.models.survey_keyword.findAll({
          where: { lecture_item_id: lcItem.lecture_item_id  }
        });
        await Promise.all(originSurveyKeywords.map(keyword => (async () =>{
          await modules.models.bank_survey_keyword.create({
            survey_id: newBankSurvey.survey_id,
            keyword: keyword.keyword,
            lecture_id: newBankLecture.lecture_id,
            lecture_item_id: newBankLectureItemId,
            score_portion: keyword.score_portion,
            bank_history_id: history.bank_history_id,
          })
        })()));
        break; // survey case break
      
      case 2: // case practice
        console.log('practice');
        const originCodePractice = await modules.models.lecture_code_practice.findOne({
        where: { lecture_item_id: lcItem.lecture_item_id },
        });
        const newBankCodePractice = await modules.models.bank_lecture_code_practice.create({
          lecture_item_id: newBankLectureItemId,
          score: originCodePractice.score,
          code: originCodePractice.code,
          is_jupyter: originCodePractice.is_jupyter,
          activated_time: originCodePractice.activated_time,
          deactivated_time: originCodePractice.deactivated_time,
          language_type: originCodePractice.language_type,
          difficulty: originCodePractice.difficulty,
          user_id: originCodePractice.user_id,
          bank_history_id: history.bank_history_id,
        });

        const originCodePracticeKeywords = await modules.models.lecture_code_practice_keyword.findAll({
          where: { lecture_item_id: lcItem.lecture_item_id  }
        })
        await Promise.all(originCodePracticeKeywords.map(keyword => (async () =>{
          await modules.models.bank_lecture_code_practice_keyword.create({
            lecture_code_practice_id: newBankCodePractice.lecture_code_practice_id,
            keyword: keyword.keyword,
            lecture_id: newBankLecture.lecture_id,
            lecture_item_id: newBankLectureItemId,
            score_portion: keyword.score_portion,
            bank_history_id: history.bank_history_id,
          })
        })()));

        break; // practice case break
      
      case 3: // case discussion
        console.log('discussion');
        const originDiscussion = await modules.models.discussion_info.findOne({
        where: { lecture_item_id: lcItem.lecture_item_id },
        });
        const newBankDiscussion = await modules.models.bank_discussion_info.create({
          lecture_item_id: newBankLectureItemId,
          score: originDiscussion.score,
          topic: originDiscussion.topic,
          difficulty: originDiscussion.difficulty,
          student_share: 0,
          bank_history_id: history.bank_history_id,
        });
        // discussion은 아직 keyword가 없음
        break; // discussion case break

      case 4: // case note
        console.log('note');
        const originNote = await modules.models.note.findOne({
          where: { lecture_item_id: lcItem.lecture_item_id },
          include: [
            { model: modules.models.file },
          ],
        });

        const newBankNote = await modules.models.bank_note.create({
          lecture_item_id: newBankLectureItemId,
          note_type: originNote.note_type,
          url: originNote.url,
          youtube_interval: originNote.youtube_interval,
          score: originNote.score,
          // note_file: 안쓰는 columns
          bank_history_id: history.bank_history_id,
        });

        const originNoteKeywords = await modules.models.note_keyword.findAll({
          where: { lecture_item_id: lcItem.lecture_item_id  }
        })
        await Promise.all(originNoteKeywords.map(keyword => (async () =>{
          await modules.models.bank_note_keyword.create({
            note_id: newBankNote.note_id,
            keyword: keyword.keyword,
            lecture_id: newBankLecture.lecture_id,
            lecture_item_id: newBankLectureItemId,
            score_portion: keyword.score_portion,
            bank_history_id: history.bank_history_id,
          })
        })()));

        const noteFiles = originNote.files
        if ( noteFiles.length > 0 ) {
          noteFiles.forEach(function(element){
            modules.upload.copy(element.static_path, element.name, 'note_id', newBankNote.note_id, authId, newBankLectureItemId, 0);
          })
        }
        break; // note case break

      default:
        console.log('default');
      }
    }
    )()));

    // lecture의 모든 아이템들을 저장
    let list_dic = {};
    const lists = await modules.models.lecture_item_list.findAll({ where: { lecture_id: lectureId }});
    for (let i = 0 ; i < lists.length; i +=1) {
      let list = lists[i].linked_list;
      let ids_ary = list.split(modules.config.separator);
      let newItemIds = [];
      for (let j = 0 ; j < ids_ary.length ; j += 1) {
        newItemIds.push(item_id_dic[ids_ary[j]]);
      }
      let joinedNewItemIds = newItemIds.join(modules.config.separator);
      let newList = await modules.models.bank_lecture_item_list.create({ 
        lecture_id: newBankLecture.lecture_id,
        item_id: newItemIds[0],
        linked_list: joinedNewItemIds,
        bank_history_id: history.bank_history_id,
      });
      list_dic[lists[i].lecture_item_list_id] = newList.lecture_item_list_id;
    }
    
    // group 생성
    const groups = await modules.models.lecture_item_group.findAll({ where: { lecture_id: lectureId }});
    for (let i = 0 ; i < groups.length ; i += 1) {
      let group = groups[i].group_list;
      let lists_ary = group.split(modules.config.separator);
      let newListIds = [];
      for (let j = 0 ; j < lists_ary.length ; j += 1) {
        newListIds.push(list_dic[lists_ary[j]]);
      }
      let joinedNewListIds = newListIds.join(modules.config.separator);
      const newGroup = await modules.models.bank_lecture_item_group.create({
        lecture_id: newBankLecture.lecture_id,
        group_list: newListIds[0],
        opened: groups[i].opened,
        start: groups[i].start,
        end: groups[i].end,
        bank_history_id: history.bank_history_id,
      });
    };
    

  })()));

  res.json({ success: true, newBankClass });
}));

// 은행에 강의 저장하기
router.post('/lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lecture_id, group_id } = req.body;

  const lectureId = lecture_id;
  const groupId = group_id;

  //console.log("authId: ", authId);
  //console.log("authType: ", authType);
  //console.log("lecture_id: ", lectureId);
  //console.log("group_id: ", group_id);

  const teacherCheck = await modules.teacherInGroup(authId, groupId);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const originLecture = await modules.models.lecture.findByPk(lectureId, {
    include: [{
      model: modules.models.lecture_item,
    }, {
      model: modules.models.lecture_keyword,
    }, {
      model: modules.models.lecture_keyword_relation,
    }, {
      as: 'material',
      model: modules.models.material,
      include: { model: modules.models.file }
    }, {
      model: modules.models.lecture_accept_plist,
      include: { model: modules.models.plist },
    }],
  });

  originLecture.lecture_items.forEach((x) => {
    if (x.type === 2 || x.type === 5) {
      const filePathCheck = path.join(modules.config.appRoot, `public/materials/docker_item_${x.dataValues.lecture_item_id}`);
      const itemName = x.dataValues.name;
      const lectureName = originLecture.name;
      if (!(fs.existsSync(filePathCheck))) {
        // throw modules.errorBuilder.default('Docker File Error', 500, true);
        res.json ({success:false, itemName, lectureName});
      }
    }
  });

  const history = await modules.models.bank_history.create({
    creator_id: authId,
    type: 1,
  });

  const newBankLecture = await modules.models.bank_lecture.create({
    name: originLecture.name,
    location: originLecture.location,
    type: originLecture.type,
    start_time: originLecture.start_time,
    end_time: originLecture.end_time,
    is_auto: originLecture.is_auto,
    video_link: originLecture.video_link,
    keyword_state: originLecture.keyword_state,
    bank_history_id: history.bank_history_id,
    latest_store_teacher_id: authId,
    group_id: groupId,
  });

  const originLectureKeywords = await modules.models.lecture_keyword.findAll({
    where: { lecture_id: lectureId, },
  });
  const originLectureKeywordRelations = await modules.models.lecture_keyword_relation.findAll({
    where: { lecture_id: lectureId, },
  });
  const originLectureMaterials = await modules.models.material.findAll({
    where: { lecture_id: lectureId, },
    include: { model: modules.models.file },
  });

  const newLectureKeywords = originLectureKeywords.map(item => {
    return {
      lecture_id: newBankLecture.lecture_id,
      keyword: item.keyword,
      weight: item.weight,
      weight_ratio: item.weight_ratio,
      question_score_ratio: item.question_score_ratio,
      material_score_ratio: item.material_score_ratio,
      difference_question: item.difference_question,
      difference_material: item.difference_material,
      bank_history_id: history.bank_history_id
    };
  });
  let newBankLectureKeywords = null;
  if (newLectureKeywords) {
    newBankLectureKeywords = await modules.models.bank_lecture_keyword.bulkCreate(newLectureKeywords);
  }


  const newLectureKeywordRelations = originLectureKeywordRelations.map(item => {
    return {
      lecture_id: newBankLecture.lecture_id,
      node1: item.node1,
      node2: item.node2,
      weight: item.weight,
      bank_history_id: history.bank_history_id
    };
  });
  let newBankLectureKeywordRelations = null;
  if (newLectureKeywordRelations) {
    newBankLectureKeywordRelations = await modules.models.bank_lecture_keyword_relation.bulkCreate(newLectureKeywordRelations);
  }
  const newBankMaterials = await Promise.all(originLectureMaterials.map(material => (async () => {
    const newBankMaterial = await modules.models.bank_material.create({
      comment: material.comment,
      score_sum: material.score_sum,
      lecture_id: newBankLecture.lecture_id,
    //  creator_id: material.creator_id, 가져올때 바뀔거라서 필요없음
      material_type: material.material_type,
      bank_history_id: history.bank_history_id
    });
    const file = material.file;
    const originLectureMaterialKeywords = await modules.models.material_keyword.findAll({ 
      where: { material_id: material.material_id, },
    });
    const newMaterialKeywords = originLectureMaterialKeywords.map((item) => {
      return {
        lecture_id: newBankLecture.lecture_id,
        material_id: newBankMaterial.material_id,
        keyword: item.keyword,
        score: item.score,
        bank_history_id: history.bank_history_id
      }
    });
    const newBankMaterialKeywords = await modules.models.bank_material_keyword.bulkCreate(newMaterialKeywords);
    const newBankMaterialFile = await modules.upload.copy(file.static_path, file.name, 'material_id', newBankMaterial.material_id, authId, null, 0);
    return {
      newBankMaterial,
      newBankMaterialKeywords,
      newBankMaterialFile
    };
  })()));
  // const originPlist = originLecture.lecture_accept_plists;
  // const newBankLectureAcceptPlist = await Promise.all(originPlist.map(program => (async () => {
  //   modules.models.bank_lecture_accept_plist.create({
  //     plist_id: program.plist_id,
  //     lecture_id: newBankLecture.lecture_id,
  //   });
  // })()));

  let item_id_dic = {};

  const originLectureItems = await modules.models.lecture_item.findAll({
    where: { lecture_id: lectureId },
  });
  const newBankLectureItems = await Promise.all(originLectureItems.map(lcItem => (async () => {
    const newBankLectureItem = await modules.models.bank_lecture_item.create({
      name: lcItem.name,
      start_time: lcItem.start_time,
      end_time: lcItem.end_time,
      offset: lcItem.offset,
      type: lcItem.type,
      past_opend: 0,
      order: lcItem.order,
      sequence: lcItem.sequence,
      result: lcItem.result,
      opend: 0,
      scoring_finish: 0,
      lecture_id: newBankLecture.lecture_id,
      bank_history_id: history.bank_history_id,
      latest_store_teacher_id: authId,
      group_id: groupId,
    });
    const newBankLectureItemId = newBankLectureItem.lecture_item_id;
    
    item_id_dic[lcItem.lecture_item_id] = newBankLectureItemId;

  switch(newBankLectureItem.type){
      // 0: 문제(question), 1: 설문(survey), 2: 실습(practice), 3: 토론(discussion) 4: 자료(note)
    
    case 0:
      console.log('question');
      const originQuestions = await modules.models.question.findOne({
      where: { lecture_item_id: lcItem.lecture_item_id },
      include: [
        { model: modules.models.oj_problem, },
        { model: modules.models.problem_testcase },
        { model: modules.models.file },
        { model: modules.models.file, as: 'question_material' },
      ],
      });
      const newBankQuestion = await modules.models.bank_question.create({
        lecture_item_id: newBankLectureItemId,
        type: originQuestions.type,
        question: originQuestions.question,
        choice: originQuestions.choice,
        answer: originQuestions.answer,
        is_ordering_answer: originQuestions.is_ordering_answer,
        accept_language: originQuestions.accept_language,
        order: originQuestions.order,
        showing_order: originQuestions.showing_order,
        difficulty: originQuestions.difficulty,
        timer: originQuestions.timer,
        creator_id: originQuestions.creator_id,
        //sqlite_file_guid: item.sqlite_file_guid, // 나중에 복사한 guid 의 값을 넣어준다.
        bank_history_id: history.bank_history_id,
        choice2: originQuestions.choice2,
        question_media: originQuestions.question_media,
        question_media_type: originQuestions.question_media_type,
        answer_media_type: originQuestions.answer_media_type,
      });
      // create question_keyword
      // question keyword -> foreign key로 묶이지 않았기에 별도로 호출해서 넣어준다.
      const originQuestionKeywords = await modules.models.question_keyword.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id  }
      });
      await Promise.all(originQuestionKeywords.map(keyword => (async () =>{
        await modules.models.bank_question_keyword.create({
          question_id: newBankQuestion.question_id,
          keyword: keyword.keyword,
          lecture_id: newBankLecture.lecture_id,
          lecture_item_id: newBankLectureItemId,
          score_portion: keyword.score_portion,
          bank_history_id: history.bank_history_id,
        })
      })()));


      const ojProblem = originQuestions.oj_problem;
      const problemTestCases = originQuestions.problem_testcases;
      let bankOjProblem = null;
      if (ojProblem !== null) {
        const bankOjProblem = await modules.models.bank_oj_problem.create({
          problem_id: newBankQuestion.question_id,
          title: ojProblem.title,
          description: ojProblem.description,
          input: ojProblem.input,
          output: ojProblem.output,
          sample_input: ojProblem.sample_input,
          sample_output: ojProblem.sample_output,
          spj: ojProblem.spj,
          hint: ojProblem.hint,
          source: ojProblem.source,
          in_date: ojProblem.in_date,
          time_limit: ojProblem.time_limit,
          memory_limit: ojProblem.memory_limit,
          defunct: ojProblem.defunct,
          accepted: ojProblem.accepted,
          submit: 0, // 제출자, 정답자 0으로 초기화
          solved: 0, // 
          bank_history_id: history.bank_history_id,
        });
      }

      let bankProblemTestCases = [];
      // testcase create 시 home/judged 에 문항이 생성되는지 확인할 것 - 지건호
      if (problemTestCases.length > 0) {

        bankProblemTestCases = await Promise.all(problemTestCases.map(testcase => (async () => {
          const bankProblemTestCase = await modules.models.bank_problem_testcase.create({
            question_id: newBankQuestion.question_id,
            num: testcase.num,
            input: testcase.input,
            output: testcase.output,
            input_path: testcase.input_path,
            output_path: testcase.output_path,
            bank_history_id: history.bank_history_id,
          });
          return bankProblemTestCase;
        })()));

      }
      ///////////////////////////////////////////////////////////////////////
      const files = originQuestions.files;
      if(files.length > 0){
        const newBankQuestionFiles = await Promise.all(files.map(file => (async () => {
          const newFile = await modules.upload.copy(file.static_path, file.name, 'question_id', newBankQuestion.question_id, authId, newBankLectureItemId, 0);
          return newFile;
        })()));
      }
      const material_files = originQuestions.question_material;
      if ( material_files.length > 0) {
        material_files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'question_id_material', newBankQuestion.question_id, authId, newBankLectureItemId, 0);
        })
      }

      break; // question case break

    case 1: // survey
      console.log('survey');
      const originSurveys = await modules.models.survey.findOne({
        where: { lecture_item_id: lcItem.lecture_item_id },
        // include: { model: modules.models.file },  // 서비스 프론트에 survey에는 file이 없음.
      });
      const newBankSurvey = await modules.models.bank_survey.create({
        lecture_item_id: newBankLectureItemId,
        type: originSurveys.type,
        comment: originSurveys.comment,
        choice: originSurveys.choice,
        creator_id: originSurveys.creator_id,
        bank_history_id: history.bank_history_id,
      });
      const originSurveyKeywords = await modules.models.survey_keyword.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id  }
      });
      await Promise.all(originSurveyKeywords.map(keyword => (async () =>{
        await modules.models.bank_survey_keyword.create({
          survey_id: newBankSurvey.survey_id,
          keyword: keyword.keyword,
          lecture_id: newBankLecture.lecture_id,
          lecture_item_id: newBankLectureItemId,
          score_portion: keyword.score_portion,
          bank_history_id: history.bank_history_id,
        })
      })()));
      break; // survey case break
    
    case 2: // case practice
      console.log('practice');
      const originCodePractice = await modules.models.lecture_code_practice.findOne({
      where: { lecture_item_id: lcItem.lecture_item_id },
      });
      const newBankCodePractice = await modules.models.bank_lecture_code_practice.create({
        lecture_item_id: newBankLectureItemId,
        score: originCodePractice.score,
        code: originCodePractice.code,
        is_jupyter: originCodePractice.is_jupyter,
        activated_time: originCodePractice.activated_time,
        deactivated_time: originCodePractice.deactivated_time,
        language_type: originCodePractice.language_type,
        difficulty: originCodePractice.difficulty,
        user_id: originCodePractice.user_id,
        bank_history_id: history.bank_history_id,
      });


      const oriPracticeFilePath = path.join(modules.config.appRoot, `public/materials/docker_item_${lcItem.lecture_item_id}`);
      const newPracticeFilePath = path.join(modules.config.appRoot, `public/materials/bank_item_${newBankLectureItemId}`);
      
      if (fs.existsSync(newPracticeFilePath)) {
        console.log('exists');
      } else {
        await fs.mkdirSync(newPracticeFilePath);
      }
      await execTimeout10("sudo cp " + oriPracticeFilePath + "/Untitled.ipynb " + newPracticeFilePath + "/Untitled.ipynb");

/*      const cmdPractice = `sudo cp ${oriPracticeFilePath}/Untitled.ipynb ${newPracticeFilePath}/Untitled.ipynb`;
      await execCmd(cmdPractice, function (error, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
      }); */
      const wherecmdPractice = {
        file_guid: `docker_practice_item${newBankLectureItemId}`
      };
      await modules.updateOrCreate(modules.models.bank_file, wherecmdPractice, {
        file_guid: `docker_practice_item${newBankLectureItemId}`,
        static_path: `${oriPracticeFilePath}/Untitled.ipynb`,
        client_path: `${newPracticeFilePath}/Untitled.ipynb`,
        file_type: '.ipynb',
        name: 'Untitled.ipynb',
        uploader_id: authId,
        jupyter_id:newBankLectureItemId
      })


      const originCodePracticeKeywords = await modules.models.lecture_code_practice_keyword.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id  }
      })
      await Promise.all(originCodePracticeKeywords.map(keyword => (async () =>{
        await modules.models.bank_lecture_code_practice_keyword.create({
          lecture_code_practice_id: newBankCodePractice.lecture_code_practice_id,
          keyword: keyword.keyword,
          lecture_id: newBankLecture.lecture_id,
          lecture_item_id: newBankLectureItemId,
          score_portion: keyword.score_portion,
          bank_history_id: history.bank_history_id,
        })
      })()));

      break; // practice case break
    
    case 3: // case discussion
      console.log('discussion');
      const originDiscussion = await modules.models.discussion_info.findOne({
      where: { lecture_item_id: lcItem.lecture_item_id },
      });
      const newBankDiscussion = await modules.models.bank_discussion_info.create({
        lecture_item_id: newBankLectureItemId,
        score: originDiscussion.score,
        topic: originDiscussion.topic,
        difficulty: originDiscussion.difficulty,
        student_share: 0,
        bank_history_id: history.bank_history_id,
      });
      // discussion은 아직 keyword가 없음
      break; // discussion case break

    case 4: // case note
      console.log('note');
      const originNote = await modules.models.note.findOne({
        where: { lecture_item_id: lcItem.lecture_item_id },
        include: [
          { model: modules.models.file },
        ],
      });

      const newBankNote = await modules.models.bank_note.create({
        lecture_item_id: newBankLectureItemId,
        note_type: originNote.note_type,
        url: originNote.url,
        youtube_interval: originNote.youtube_interval,
        score: originNote.score,
        // note_file: 안쓰는 columns
        bank_history_id: history.bank_history_id,
      });

      const originNoteKeywords = await modules.models.note_keyword.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id  }
      })
      await Promise.all(originNoteKeywords.map(keyword => (async () =>{
        await modules.models.bank_note_keyword.create({
          note_id: newBankNote.note_id,
          keyword: keyword.keyword,
          lecture_id: newBankLecture.lecture_id,
          lecture_item_id: newBankLectureItemId,
          score_portion: keyword.score_portion,
          bank_history_id: history.bank_history_id,
        })
      })()));

      const noteFiles = originNote.files
      if ( noteFiles.length > 0 ) {
        noteFiles.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'note_id', newBankNote.note_id, authId, newBankLectureItemId, 0);
        })
      }
      break; // note case break

    case 5: // case coding
      console.log('coding');
      const originCoding = await modules.models.lecture_code_practice.findOne({
        where: { lecture_item_id: lcItem.lecture_item_id },
      });
      const newBankCoding = await modules.models.bank_lecture_code_practice.create({
        lecture_item_id: newBankLectureItemId,
        score: originCoding.score,
        code: originCoding.code,
        is_jupyter: originCoding.is_jupyter,
        activated_time: originCoding.activated_time,
        deactivated_time: originCoding.deactivated_time,
        language_type: originCoding.language_type,
        difficulty: originCoding.difficulty,
        user_id: originCoding.user_id,
        bank_history_id: history.bank_history_id,
      });

      const oriCodingFilePath = path.join(modules.config.appRoot, `public/materials/docker_item_${lcItem.lecture_item_id}`);
      const newCodingFilePath = path.join(modules.config.appRoot, `public/materials/bank_item_${newBankLectureItemId}`);

      if (fs.existsSync(newCodingFilePath)) {
        console.log('exists');
      } else {
        await fs.mkdirSync(newCodingFilePath);
      }
      /*
      const cmdCoding = `sudo cp ${oriCodingFilePath}/Untitled.ipynb ${newCodingFilePath}/Untitled.ipynb`;
      await execCmd(cmdCoding, function (error, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
      }); */

      await execTimeout10("sudo cp " + oriCodingFilePath + "/Untitled.ipynb " + newCodingFilePath + "/Untitled.ipynb");

      const whereCoding = {
        file_guid: `docker_practice_item${newBankLectureItemId}`
      };
      await modules.updateOrCreate(modules.models.bank_file, whereCoding, {
        file_guid: `docker_practice_item${newBankLectureItemId}`,
        static_path: `${oriCodingFilePath}/Untitled.ipynb`,
        client_path: `${newCodingFilePath}/Untitled.ipynb`,
        file_type: '.ipynb',
        name: 'Untitled.ipynb',
        uploader_id: authId,
        jupyter_id:newBankLectureItemId 
      })

      const originCodingKeywords = await modules.models.lecture_code_practice_keyword.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id }
      })
      await Promise.all(originCodingKeywords.map(keyword => (async () =>{
        await modules.models.bank_lecture_code_practice_keyword.create({
          lecture_code_practice_id: newBankCoding.lecture_code_practice_id,
          keyword: keyword.keyword,
          lecture_id: newBankLecture.lecture_id,
          lecture_item_id: newBankLectureItemId,
          score_portion: keyword.score_portion,
          bank_history_id: history.bank_history_id,
        })
      })()));
  
      break; // coding case break

    default:
      console.log('default');
    }
  }
  
  )()));

  // lecture의 모든 아이템들을 저장
  let list_dic = {};
  const lists = await modules.models.lecture_item_list.findAll({ where: { lecture_id: lectureId }});
  for (let i = 0 ; i < lists.length; i +=1) {
    let list = lists[i].linked_list;
    let ids_ary = list.split(modules.config.separator);
    let newItemIds = [];
    for (let j = 0 ; j < ids_ary.length ; j += 1) {
      newItemIds.push(item_id_dic[ids_ary[j]]);
    }
    let joinedNewItemIds = newItemIds.join(modules.config.separator);
    let newList = await modules.models.bank_lecture_item_list.create({ 
      lecture_id: newBankLecture.lecture_id,
      item_id: newItemIds[0],
      linked_list: joinedNewItemIds,
      bank_history_id: history.bank_history_id,
    });
    list_dic[lists[i].lecture_item_list_id] = newList.lecture_item_list_id;
  }
  
  // group 생성
  const groups = await modules.models.lecture_item_group.findAll({ where: { lecture_id: lectureId }});
  for (let i = 0 ; i < groups.length ; i += 1) {
    let group = groups[i].group_list;
    let lists_ary = group.split(modules.config.separator);
    let newListIds = [];
    for (let j = 0 ; j < lists_ary.length ; j += 1) {
      newListIds.push(list_dic[lists_ary[j]]);
    }
    let joinedNewListIds = newListIds.join(modules.config.separator);
    const newGroup = await modules.models.bank_lecture_item_group.create({
      lecture_id: newBankLecture.lecture_id,
      group_list: newListIds[0],
      opened: groups[i].opened,
      start: groups[i].start,
      end: groups[i].end,
      bank_history_id: history.bank_history_id,
    });
  };

  res.json({ success: true, newBankLecture });
}));

// 은행에 강의 아이템 저장하기
router.post('/lecture-item', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lecture_item_id, group_id } = req.body;

  const lectureItemId = lecture_item_id;
  const groupId = group_id;

  const teacherCheck = await modules.teacherInGroup(authId, groupId);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const history = await modules.models.bank_history.create({
    creator_id: authId,
    type: 2,
  });

  const originLectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: [{
      model: modules.models.lecture_item_keyword
    }, {
      model: modules.models.lecture_code_practice
    }, {
      model: modules.models.homework,
      include: { model: modules.models.file },
    }, {
      model: modules.models.question,
      include: [
        { model: modules.models.problem_testcase},
        { model: modules.models.oj_problem },
        { model: modules.models.file},
        { model: modules.models.file, as: 'question_material' },
      ],
    }, {
      model: modules.models.survey,
      include: { model: modules.models.file },
    }, {
      model: modules.models.discussion_info,
    }, {
      model: modules.models.note,
      include: { model: modules.models.file },
    }]
  });
  if (originLectureItem.dataValues.type === 2 || originLectureItem.dataValues.type === 5) {
    const filePathCheck = path.join(modules.config.appRoot, `public/materials/docker_item_${originLectureItem.dataValues.lecture_item_id}`);
    const itemName = originLectureItem.dataValues.name;
    if (!(fs.existsSync(filePathCheck))) {
      // throw modules.errorBuilder.default('Docker File Error', 500, true);
      res.json ({ success: false, itemName });
    }
  }

  const newBankLectureItem = await modules.models.bank_lecture_item.create({
    name: originLectureItem.name,
    start_time: originLectureItem.start_time,
    end_time: originLectureItem.end_time,
    type: originLectureItem.type,
    past_opend: 0,
    order: originLectureItem.order,
    sequence: originLectureItem.sequence,
    result: originLectureItem.result,
    opend: 0,
    scoring_finish: 0,
    group_id: groupId,
    latest_store_teacher_id: authId,
    offset: originLectureItem.offset,
    bank_history_id: history.bank_history_id,
  });
  const newBankLectureItemId = newBankLectureItem.lecture_item_id;

  switch(newBankLectureItem.type){
    // 0: 문제(question), 1: 설문(survey), 2: 실습(practice), 3: 토론(discussion), 4: 자료(note), 5: 코딩(coding)
  
  case 0:
    console.log('question');
    const originQuestions = await modules.models.question.findOne({
    where: { lecture_item_id: lectureItemId },
    include: [
      { model: modules.models.oj_problem, },
      { model: modules.models.problem_testcase },
      { model: modules.models.file },
      { model: modules.models.file, as: 'question_material' },
    ],
    });
    const newBankQuestion = await modules.models.bank_question.create({
      lecture_item_id: newBankLectureItemId,
      type: originQuestions.type,
      question: originQuestions.question,
      choice: originQuestions.choice,
      answer: originQuestions.answer,
      is_ordering_answer: originQuestions.is_ordering_answer,
      accept_language: originQuestions.accept_language,
      order: originQuestions.order,
      showing_order: originQuestions.showing_order,
      difficulty: originQuestions.difficulty,
      timer: originQuestions.timer,
      creator_id: originQuestions.creator_id,
      //sqlite_file_guid: item.sqlite_file_guid, // 나중에 복사한 guid 의 값을 넣어준다.
      bank_history_id: history.bank_history_id,
      choice2: originQuestions.choice2,
      question_media: originQuestions.question_media,
      question_media_type: originQuestions.question_media_type,
      answer_media_type: originQuestions.answer_media_type,
    });
    // create question_keyword
    // question keyword -> foreign key로 묶이지 않았기에 별도로 호출해서 넣어준다.
    const originQuestionKeywords = await modules.models.question_keyword.findAll({
      where: { lecture_item_id: lectureItemId  }
    });
    await Promise.all(originQuestionKeywords.map(keyword => (async () =>{
      await modules.models.bank_question_keyword.create({
        question_id: newBankQuestion.question_id,
        keyword: keyword.keyword,
        // lecture_id: newBankLecture.lecture_id,
        lecture_item_id: newBankLectureItemId,
        score_portion: keyword.score_portion,
        bank_history_id: history.bank_history_id,
      })
    })()));


    const ojProblem = originQuestions.oj_problem;
    const problemTestCases = originQuestions.problem_testcases;
    let bankOjProblem = null;
    if (ojProblem !== null) {
      const bankOjProblem = await modules.models.bank_oj_problem.create({
        problem_id: newBankQuestion.question_id,
        title: ojProblem.title,
        description: ojProblem.description,
        input: ojProblem.input,
        output: ojProblem.output,
        sample_input: ojProblem.sample_input,
        sample_output: ojProblem.sample_output,
        spj: ojProblem.spj,
        hint: ojProblem.hint,
        source: ojProblem.source,
        in_date: ojProblem.in_date,
        time_limit: ojProblem.time_limit,
        memory_limit: ojProblem.memory_limit,
        defunct: ojProblem.defunct,
        accepted: ojProblem.accepted,
        submit: 0, // 제출자, 정답자 0으로 초기화
        solved: 0, // 
        bank_history_id: history.bank_history_id,
      });
    }

    let bankProblemTestCases = [];
    // testcase create 시 home/judged 에 문항이 생성되는지 확인할 것 - 지건호
    if (problemTestCases.length > 0) {

      bankProblemTestCases = await Promise.all(problemTestCases.map(testcase => (async () => {
        const bankProblemTestCase = await modules.models.bank_problem_testcase.create({
          question_id: newBankQuestion.question_id,
          num: testcase.num,
          input: testcase.input,
          output: testcase.output,
          input_path: testcase.input_path,
          output_path: testcase.output_path,
          bank_history_id: history.bank_history_id,
        });
        return bankProblemTestCase;
      })()));

    }
    ///////////////////////////////////////////////////////////////////////
    const files = originQuestions.files;
    if(files.length > 0){
      const newBankQuestionFiles = await Promise.all(files.map(file => (async () => {
        const newFile = await modules.upload.copy(file.static_path, file.name, 'question_id', newBankQuestion.question_id, authId, newBankLectureItemId, 0);
        return newFile;
      })()));
    }
    const material_files = originQuestions.question_material;
    if ( material_files.length > 0) {
      material_files.forEach(function(element){
        modules.upload.copy(element.static_path, element.name, 'question_id_material', newBankQuestion.question_id, authId, newBankLectureItemId, 0);
      })
    }
    
    break; // question case break

  case 1: // survey
    console.log('survey');
    const originSurveys = await modules.models.survey.findOne({
      where: { lecture_item_id: lectureItemId },
      // include: { model: modules.models.file },  // 서비스 프론트에 survey에는 file이 없음.
    });
    const newBankSurvey = await modules.models.bank_survey.create({
      lecture_item_id: newBankLectureItemId,
      type: originSurveys.type,
      comment: originSurveys.comment,
      choice: originSurveys.choice,
      creator_id: originSurveys.creator_id,
      bank_history_id: history.bank_history_id,
    });
    const originSurveyKeywords = await modules.models.survey_keyword.findAll({
      where: { lecture_item_id: lectureItemId  }
    });
    await Promise.all(originSurveyKeywords.map(keyword => (async () =>{
      await modules.models.bank_survey_keyword.create({
        survey_id: newBankSurvey.survey_id,
        keyword: keyword.keyword,
        lecture_id: newBankLecture.lecture_id,
        lecture_item_id: newBankLectureItemId,
        score_portion: keyword.score_portion,
        bank_history_id: history.bank_history_id,
      })
    })()));
    break; // survey case break
  
  case 2: // case practice
    console.log('practice');
    const originCodePractice = await modules.models.lecture_code_practice.findOne({
    where: { lecture_item_id: lectureItemId },
    });
    const newBankCodePractice = await modules.models.bank_lecture_code_practice.create({
      lecture_item_id: newBankLectureItemId,
      score: originCodePractice.score,
      code: originCodePractice.code,
      is_jupyter: originCodePractice.is_jupyter,
      activated_time: originCodePractice.activated_time,
      deactivated_time: originCodePractice.deactivated_time,
      language_type: originCodePractice.language_type,
      difficulty: originCodePractice.difficulty,
      user_id: originCodePractice.user_id,
      bank_history_id: history.bank_history_id,
    });

      const oriPracticeFilePath = path.join(modules.config.appRoot, `public/materials/docker_item_${lectureItemId}`);
      const newPracticeFilePath = path.join(modules.config.appRoot, `public/materials/bank_item_${newBankLectureItemId}`);

      if (fs.existsSync(newPracticeFilePath)) {
        console.log('exists');
      } else {
        await fs.mkdirSync(newPracticeFilePath);
      }

      /*
      const cmdPractice = `sudo cp ${oriPracticeFilePath}/Untitled.ipynb ${newPracticeFilePath}/Untitled.ipynb`;
      await execCmd(cmdPractice, function (error, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
      }); */
      await execTimeout10("sudo cp " + oriPracticeFilePath + "/Untitled.ipynb " + newPracticeFilePath + "/Untitled.ipynb");

      const wherecmdPractice = {
        file_guid: `docker_practice_item${newBankLectureItemId}`
      };
      await modules.updateOrCreate(modules.models.bank_file, wherecmdPractice, {
        file_guid: `docker_practice_item${newBankLectureItemId}`,
        static_path: `${oriPracticeFilePath}/Untitled.ipynb`,
        client_path: `${newPracticeFilePath}/Untitled.ipynb`,
        file_type: '.ipynb',
        name: 'Untitled.ipynb',
        uploader_id: authId,
        jupyter_id:newBankLectureItemId
      })


    const originCodePracticeKeywords = await modules.models.lecture_code_practice_keyword.findAll({
      where: { lecture_item_id: lectureItemId  }
    })
    await Promise.all(originCodePracticeKeywords.map(keyword => (async () =>{
      await modules.models.bank_lecture_code_practice_keyword.create({
        lecture_code_practice_id: newBankCodePractice.lecture_code_practice_id,
        keyword: keyword.keyword,
        // lecture_id: newBankLecture.lecture_id,
        lecture_item_id: newBankLectureItemId,
        score_portion: keyword.score_portion,
        bank_history_id: history.bank_history_id,
      })
    })()));

    break; // practice case break
  
  case 3: // case discussion
    console.log('discussion');
    const originDiscussion = await modules.models.discussion_info.findOne({
    where: { lecture_item_id: lectureItemId },
    });
    const newBankDiscussion = await modules.models.bank_discussion_info.create({
      lecture_item_id: newBankLectureItemId,
      score: originDiscussion.score,
      topic: originDiscussion.topic,
      difficulty: originDiscussion.difficulty,
      student_share: 0,
      bank_history_id: history.bank_history_id,
    });
    // discussion은 아직 keyword가 없음
    break; // discussion case break

  case 4: // case note
    console.log('note');
    const originNote = await modules.models.note.findOne({
      where: { lecture_item_id: lectureItemId },
      include: [
        { model: modules.models.file },
      ],
    });

    const newBankNote = await modules.models.bank_note.create({
      lecture_item_id: newBankLectureItemId,
      note_type: originNote.note_type,
      url: originNote.url,
      youtube_interval: originNote.youtube_interval,
      score: originNote.score,
      // note_file: 안쓰는 columns
      bank_history_id: history.bank_history_id,
    });

    const originNoteKeywords = await modules.models.note_keyword.findAll({
      where: { lecture_item_id: lectureItemId  }
    })
    await Promise.all(originNoteKeywords.map(keyword => (async () =>{
      await modules.models.bank_note_keyword.create({
        note_id: newBankNote.note_id,
        keyword: keyword.keyword,
        // lecture_id: newBankLecture.lecture_id,
        lecture_item_id: newBankLectureItemId,
        score_portion: keyword.score_portion,
        bank_history_id: history.bank_history_id,
      })
    })()));

    const noteFiles = originNote.files
    if ( noteFiles.length > 0 ) {
      noteFiles.forEach(function(element){
        modules.upload.copy(element.static_path, element.name, 'note_id', newBankNote.note_id, authId, newBankLectureItemId, 0);
      })
    }
    break; // note case break

  case 5: // case coding
    console.log('coding');
    const originCoding = await modules.models.lecture_code_practice.findOne({
      where: { lecture_item_id: lectureItemId },
    });
    const newBankCoding = await modules.models.bank_lecture_code_practice.create({
      lecture_item_id: newBankLectureItemId,
      score: originCoding.score,
      code: originCoding.code,
      is_jupyter: originCoding.is_jupyter,
      activated_time: originCoding.activated_time,
      deactivated_time: originCoding.deactivated_time,
      language_type: originCoding.language_type,
      difficulty: originCoding.difficulty,
      user_id: originCoding.user_id,
      bank_history_id: history.bank_history_id,
    });

      const oriCodingFilePath = path.join(modules.config.appRoot, `public/materials/docker_item_${lectureItemId}`);
      const newCodingFilePath = path.join(modules.config.appRoot, `public/materials/bank_item_${newBankLectureItemId}`);

      if (fs.existsSync(newCodingFilePath)) {
        console.log('exists');
      } else {
        await fs.mkdirSync(newCodingFilePath);
      }
      /*
      const cmdCoding = `sudo cp ${oriCodingFilePath}/Untitled.ipynb ${newCodingFilePath}/Untitled.ipynb`;
      await execCmd(cmdCoding, function (error, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
      }); */

      await execTimeout10("sudo cp " + oriCodingFilePath + "/Untitled.ipynb " + newCodingFilePath + "/Untitled.ipynb");

      const whereCoding = {
        file_guid: `docker_practice_item${newBankLectureItemId}`
      };
      await modules.updateOrCreate(modules.models.bank_file, whereCoding, {
        file_guid: `docker_practice_item${newBankLectureItemId}`,
        static_path: `${oriCodingFilePath}/Untitled.ipynb`,
        client_path: `${newCodingFilePath}/Untitled.ipynb`,
        file_type: '.ipynb',
        name: 'Untitled.ipynb',
        uploader_id: authId,
        jupyter_id:newBankLectureItemId
      })


    const originCodingKeywords = await modules.models.lecture_code_practice_keyword.findAll({
      where: { lecture_item_id: lectureItemId }
    })
    await Promise.all(originCodingKeywords.map(keyword => (async () =>{
      await modules.models.bank_lecture_code_practice_keyword.create({
        lecture_code_practice_id: newBankCoding.lecture_code_practice_id,
        keyword: keyword.keyword,
        // lecture_id: newBankLecture.lecture_id,
        lecture_item_id: newBankLectureItemId,
        score_portion: keyword.score_portion,
        bank_history_id: history.bank_history_id,
      })
    })()));

    break; // coding case break

  default:
    console.log('default');
  }
  res.json({ success: true, newBankLectureItem });
}));

// 은행 강의 삭제하기 190904
router.delete('/lecture/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const lecture = await modules.models.bank_lecture.findByPk(id);
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.models.teacher_group.findOne({
    where: {
      group_id: lecture.group_id,
      user_id: authId,
    },
  });
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  await modules.delete.bank_lecture(id);
  res.json({ success: true });
}));

// 은행 강의 아이템 삭제하기 190905
router.delete('/lecture-item/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const lectureItem = await modules.models.bank_lecture_item.findByPk(id);
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.models.teacher_group.findOne({
    where: {
      group_id: lectureItem.group_id,
      user_id: authId,
    },
  });
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  await modules.delete.bankLectureItems(id);
  res.json({ success: true });
}));

// 은행 과목 목록 전체
router.get('/class', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classList = await modules.models.bank_class.findAll({
    attributes: modules.models.bank_class.selects.classInfo,
    include: {
      attributes: modules.models.bank_group.selects.groupInfo,
      model: modules.models.bank_group,
      include: {
        model: modules.models.teacher_group,
        attributes: modules.models.user_serial.selects.nothing,
        where: { user_id: authId },
        required: true,
      },
      required: true,
    },
  });

  res.json({ classList });
}));

// 은행의 과목 검색
router.get('/class/search', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const keywords = req.query.keywords;
  const groupId = req.query.groupId;
  const name = req.query.name;

  let classList = [];

  if (!keywords && !groupId && !name) { // 쿼리 없을 때 : 강사가 볼 수 있는 과목 전체 보여주기
    classList = await modules.models.bank_class.findAll({
      attributes: modules.models.bank_class.selects.classInfo,
      include: {
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },
    });
  } else if (!keywords && groupId && !name){ // groupId만 있을 때: group으로만 검색
    classList = await modules.models.bank_class.findAll({
      attributes: modules.models.bank_class.selects.classInfo,
      include: {
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        where: { group_id: groupId },
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },
    });
  } else if (keywords && !groupId && !name) { // keyword만 있을 때: keyword로만 검색
    const keywordArr = keywords.split(',');
    const num = keywordArr.length;
    const tempClassList = await modules.models.bank_class.findAll({
      attributes: modules.models.bank_class.selects.classInfo,
      include: [{
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },{
        model: modules.models.bank_lecture,
        include: {
          model: modules.models.bank_lecture_keyword,
          where: { keyword: keywordArr },
          required: true,
        },
        required: true,
      }]
    });
    classList = tempClassList;

  } else if (!keywords && !groupId && name) { // name만 있을 때: name으로만 검색
    classList = await modules.models.bank_class.findAll({
      where: { name: { like: `%${name}%` } },
      attributes: modules.models.bank_class.selects.classInfo,
      include: {
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },
    });
  } else {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }

  res.json({ classList });
}));

// 은행 강의 목록 전체
router.get('/lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lectureList = await modules.models.bank_lecture.findAll({
    attributes: modules.models.bank_lecture.selects.lectureInfo,
    include: [{
      attributes: modules.models.bank_group.selects.groupInfo,
      model: modules.models.bank_group,
      include: {
        model: modules.models.teacher_group,
        attributes: modules.models.user_serial.selects.nothing,
        where: { user_id: authId },
        required: true,
      },
      required: true,
    },{
      attributes: modules.models.bank_history.selects.nothing,
      model: modules.models.bank_history,
      where: { type: 1 },
      required: true,
    },]
  });

  const master_department = [];
  for(let i=0; i<lectureList.length; i++) {
    const bank_lectures = await modules.models.bank_lecture.findAll({
      attributes: ['class_id'],
      where: { lecture_id: lectureList[i].lecture_id },
    });
    //console.log("class_id: ", bank_lectures[0].dataValues.class_id);
    if(bank_lectures[0].dataValues.class_id !== null) {
      const master = await modules.models.classes.findAll({
        attributes: ['master_id'],
        where: { class_id: bank_lectures[0].dataValues.class_id },
      });
      //console.log("master_id: ", master[0].dataValues.master_id);
      const affiliation_user = await modules.models.user_belonging.findAll({
        attributes: ['affiliation_id', 'user_id'],
        where: { user_id: master[0].dataValues.master_id },
      });
      //console.log("affiliation_id: ", affiliation[0].dataValues.affiliation_id);
      const description = await modules.models.affiliation_description.findAll({
        attributes: ['description'],
        where: { affiliation_id: affiliation_user[0].dataValues.affiliation_id },
      });
      //console.log("description: ", description[0].dataValues.description);
      const user = await modules.models.user.findAll({
        attributes: ['name'],
        where: { user_id: affiliation_user[0].user_id },
      });
      const mas_dep = {
        master_name: user[0].dataValues.name,
        affiliation: description[0].dataValues.description };
      master_department.push(mas_dep);
      //console.log(master_department);
    } else {
      master_department.push({ master_name: '', affiliation: '' });
    }
  }
  //console.log(lectureList);

  res.json({ lectureList, master_department });
}));

// 은행 강의 검색
router.get('/lecture/search', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  const keywords = req.query.keywords;
  const groupId = req.query.groupId;
  const name = req.query.name;

  let lectureList = [];

  if (!keywords && !groupId && !name) { // 쿼리 없을 때 : 강사가 볼 수 있는 과목 전체 보여주기
    lectureList = await modules.models.bank_lecture.findAll({
      attributes: modules.models.bank_lecture.selects.lectureInfo,
      include: [{
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },{
        attributes: modules.models.bank_history.selects.nothing,
        model: modules.models.bank_history,
        where: { type: 1 },
        required: true,
      }]
    });
  } else if (!keywords && groupId && !name){ // groupId만 있을 때: group으로만 검색
    lectureList = await modules.models.bank_lecture.findAll({
      attributes: modules.models.bank_lecture.selects.lectureInfo,
      include: [{
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        where: { group_id: groupId },
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },{
        attributes: modules.models.bank_history.selects.nothing,
        model: modules.models.bank_history,
        where: { type: 1 },
        required: true,
      }]
    });
  } else if (keywords && !groupId && !name) { // keyword만 있을 때: keyword로만 검색
    const keywordArr = keywords.split(',');
    const num = keywordArr.length;
    const tempLectureList = await modules.models.bank_lecture.findAll({
      attributes: modules.models.bank_lecture.selects.lectureInfo,
      include: [{
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },{
        model: modules.models.bank_lecture_keyword,
        where: { keyword: keywordArr },
        required: true,
      },{
        attributes: modules.models.bank_history.selects.nothing,
        model: modules.models.bank_history,
        where: { type: 1 },
        required: true,
      }]
    });
    lectureList = tempLectureList.filter(lc => lc.bank_lecture_keywords.length === num);

  } else if (!keywords && !groupId && name) { // name만 있을 때: name으로만 검색
    lectureList = await modules.models.bank_lecture.findAll({
      where: { name: { like: `%${name}%` } },
      attributes: modules.models.bank_lecture.selects.lectureInfo,
      include: [{
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },{
        attributes: modules.models.bank_history.selects.nothing,
        model: modules.models.bank_history,
        where: { type: 1 },
        required: true,
      }]
    });
  } else {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }

  res.json({ lectureList });
}));

// 은행 강의아이템 전체 (종류 별로) type을 쿼리로 보낸다.
router.get('/lecture-item', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  let type = 9;
  switch(req.query.type) {
    case 'question':
      type = 0;
      break;
    case 'survey':
      type = 1;
      break;
    case 'codepractice':
      type = 2;
      break;
    case 'discussion':
      type = 3;
      break;
    case 'note':
      type = 4;
      break;
    case 'homework':
      type = 5;
      break;
  }
  if (type === 9) {
    throw modules.errorBuilder.default('Query Error', 404, true);
  }

  const lectureItemList = await modules.models.bank_lecture_item.findAll({
    attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
    where: { type: type },
    include: [{
      attributes: modules.models.bank_group.selects.groupInfo,
      model: modules.models.bank_group,
      include: {
        model: modules.models.teacher_group,
        attributes: modules.models.user_serial.selects.nothing,
        where: { user_id: authId },
        required: true,
      },
      required: true,
    },{
      attributes: modules.models.bank_history.selects.nothing,
      model: modules.models.bank_history,
      where: { type: 2 },
      required: true,
    }]
  });

  res.json({ lectureItemList });
}));

// 은행 강의아이템 검색
router.get('/lecture-item/search', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  const keywords = req.query.keywords;
  const groupId = req.query.groupId;
  const name = req.query.name;
  let type = 9;
  switch(req.query.type) {
    case 'question':
      type = 0;
      break;
    case 'survey':
      type = 1;
      break;
    case 'codepractice':
      type = 2;
      break;
    case 'discussion':
      type = 3;
      break;
    case 'note':
      type = 4;
      break;
    case 'homework':
      type = 5;
      break;
  }
  if (type === 9) {
    throw modules.errorBuilder.default('Query Error', 404, true);
  }
  let lectureItemList = [];

  if (!keywords && !groupId && !name) { // 쿼리 없을 때 : 강사가 볼 수 있는 과목 전체 보여주기
    lectureItemList = await modules.models.bank_lecture_item.findAll({
      attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
      where: { type: type },
      include: [{
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },{
        attributes: modules.models.bank_history.selects.nothing,
        model: modules.models.bank_history,
        where: { type: 2 },
        required: true,
      }]
    });
  } else if (!keywords && groupId && !name){ // groupId만 있을 때: group으로만 검색
    lectureItemList = await modules.models.bank_lecture_item.findAll({
      attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
      where: { type: type },
      include: [{
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        where: { group_id: groupId },
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },{
        attributes: modules.models.bank_history.selects.nothing,
        model: modules.models.bank_history,
        where: { type: 2 },
        required: true,
      }]
    });
  } else if (keywords && !groupId && !name) { // keyword만 있을 때: keyword로만 검색
    const keywordArr = keywords.split(',');
    const num = keywordArr.length;
    let templectureItemList = [];

    switch(type) {
      case 0:
        templectureItemList = await modules.models.bank_lecture_item.findAll({
          attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
          where: { type: type },
          include: [{
            attributes: modules.models.bank_group.selects.groupInfo,
            model: modules.models.bank_group,
            include: {
              model: modules.models.teacher_group,
              attributes: modules.models.user_serial.selects.nothing,
              where: { user_id: authId },
              required: true,
            },
            required: true,
          },{
            model: modules.models.bank_question,
            include: {
              model: modules.models.bank_question_keyword,
              where: { keyword: keywordArr },
              required: true,
            },
            required: true,
          },{
            attributes: modules.models.bank_history.selects.nothing,
            model: modules.models.bank_history,
            where: { type: 2 },
            required: true,
          }]
        });
        // lectureItemList = templectureItemList.filter(lcItem => lcItem.bank_questions.length === num);
        lectureItemList = templectureItemList;
        break;
      case 1:
        templectureItemList = await modules.models.bank_lecture_item.findAll({
          attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
          where: { type: type },
          include: [{
            attributes: modules.models.bank_group.selects.groupInfo,
            model: modules.models.bank_group,
            include: {
              model: modules.models.teacher_group,
              attributes: modules.models.user_serial.selects.nothing,
              where: { user_id: authId },
              required: true,
            },
            required: true,
          },{
            model: modules.models.bank_survey,
            include: {
              model: modules.models.bank_survey_keyword,
              where: { keyword: keywordArr },
              required: true,
            },
            required: true,
          },{
            attributes: modules.models.bank_history.selects.nothing,
            model: modules.models.bank_history,
            where: { type: 2 },
            required: true,
          }]
        });
        lectureItemList = templectureItemList;
        break;
      case 2:
        templectureItemList = await modules.models.bank_lecture_item.findAll({
          attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
          where: { type: type },
          include: [{
            attributes: modules.models.bank_group.selects.groupInfo,
            model: modules.models.bank_group,
            include: {
              model: modules.models.teacher_group,
              attributes: modules.models.user_serial.selects.nothing,
              where: { user_id: authId },
              required: true,
            },
            required: true,
          },{
            model: modules.models.bank_lecture_code_practice,
            include: {
              model: modules.models.bank_lecture_code_practice_keyword,
              where: { keyword: keywordArr },
              required: true,
            },
            required: true,
          },{
            attributes: modules.models.bank_history.selects.nothing,
            model: modules.models.bank_history,
            where: { type: 2 },
            required: true,
          }]
        });
        lectureItemList = templectureItemList;
        break;
      case 4:
        templectureItemList = await modules.models.bank_lecture_item.findAll({
          attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
          where: { type: type },
          include: [{
            attributes: modules.models.bank_group.selects.groupInfo,
            model: modules.models.bank_group,
            include: {
              model: modules.models.teacher_group,
              attributes: modules.models.user_serial.selects.nothing,
              where: { user_id: authId },
              required: true,
            },
            required: true,
          },{
            model: modules.models.bank_note,
            include: {
              model: modules.models.bank_note_keyword,
              where: { keyword: keywordArr },
              required: true,
            },
            required: true,
          },{
            attributes: modules.models.bank_history.selects.nothing,
            model: modules.models.bank_history,
            where: { type: 2 },
            required: true,
          }]
        });
        lectureItemList = templectureItemList;
        break;
      case 5:
        templectureItemList = await modules.models.bank_lecture_item.findAll({
          attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
          where: { type: type },
          include: [{
            attributes: modules.models.bank_group.selects.groupInfo,
            model: modules.models.bank_group,
            include: {
              model: modules.models.teacher_group,
              attributes: modules.models.user_serial.selects.nothing,
              where: { user_id: authId },
              required: true,
            },
            required: true,
          },{
            model: modules.models.bank_homework,
            include: {
              model: modules.models.bank_homework_keyword,
              where: { keyword: keywordArr },
              required: true,
            },
            required: true,
          },{
            attributes: modules.models.bank_history.selects.nothing,
            model: modules.models.bank_history,
            where: { type: 2 },
            required: true,
          }]
        });
        lectureItemList = templectureItemList;
        break;
      default:
        break;
    }
  } else if (!keywords && !groupId && name) { // name만 있을 때: name으로만 검색
    lectureItemList = await modules.models.bank_lecture_item.findAll({
      where: { name: { like: `%${name}%` }, type: type },
      attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
      include: [{
        attributes: modules.models.bank_group.selects.groupInfo,
        model: modules.models.bank_group,
        include: {
          model: modules.models.teacher_group,
          attributes: modules.models.user_serial.selects.nothing,
          where: { user_id: authId },
          required: true,
        },
        required: true,
      },{
        attributes: modules.models.bank_history.selects.nothing,
        model: modules.models.bank_history,
        where: { type: 2 },
        required: true,
      }]
    });
  } else {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }

  res.json({ lectureItemList });
}));

// classId 라는 id를 가진 과목의 정보
router.get('/class/:classId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId = parseInt(req.params.classId, 10);

  const groupId = (await modules.models.bank_class.findByPk(classId)).group_id;

  const teacherCheck = await modules.teacherInGroup(authId, groupId);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const bankClass = await modules.models.bank_class.findByPk(classId, {
    attributes: modules.models.bank_class.selects.classInfo,
    include: {
      model: modules.models.bank_lecture,
      attributes: modules.models.bank_lecture.selects.lectureInfo,
      include: {
        attributes: modules.models.bank_lecture_item.selects.lcItemInfo,
        model: modules.models.bank_lecture_item,
      },
    }
  });

  const bankClassLectureList = await Promise.all(bankClass.bank_lectures.map(lecture => (async () => {
    let questions = [];
    let surveys = [];
    let codepractices = [];
    let discussions = [];
    let notes = [];
    let homeworks = [];
    await Promise.all(lecture.bank_lecture_items.map(item => (async () => {
      switch (item.type) {
        case 0:
          const question = await modules.models.bank_question.findOne({
            where: { lecture_item_id: item.lecture_item_id },
          });
          return questions.push({ 
            lecture_item_id: item.lecture_item_id,
            name: item.name,
            start_time: item.start_time,
            end_time: item.end_time,
            order: item.order,
            type: item.type,
            sequence: item.sequence,
            result: item.result,
            latest_store_teacher_id: item.latest_store_teacher_id,
            question,
          });
        case 1:
          const survey = await modules.models.bank_survey.findOne({
            where: { lecture_item_id: item.lecture_item_id },
          });
          return surveys.push({ 
            lecture_item_id: item.lecture_item_id,
            name: item.name,
            start_time: item.start_time,
            end_time: item.end_time,
            order: item.order,
            type: item.type,
            sequence: item.sequence,
            result: item.result,
            latest_store_teacher_id: item.latest_store_teacher_id,
            survey 
          });
        case 2:
          const codepractice = await modules.models.bank_lecture_code_practice.findOne({
            where: { lecture_item_id: item.lecture_item_id },
          });
          return codepractices.push({ 
            lecture_item_id: item.lecture_item_id,
            name: item.name,
            start_time: item.start_time,
            end_time: item.end_time,
            order: item.order,
            type: item.type,
            sequence: item.sequence,
            result: item.result,
            latest_store_teacher_id: item.latest_store_teacher_id,
            codepractice
          });
        case 3:
          const discussion = await modules.models.bank_discussion_info.findOne({
            where: { lecture_item_id: item.lecture_item_id },
          });
          return discussions.push({ 
            lecture_item_id: item.lecture_item_id,
            name: item.name,
            start_time: item.start_time,
            end_time: item.end_time,
            order: item.order,
            type: item.type,
            sequence: item.sequence,
            result: item.result,
            latest_store_teacher_id: item.latest_store_teacher_id,
            discussion
          });
        case 4:
          const note = await modules.models.bank_note.findOne({
            where: { lecture_item_id: item.lecture_item_id },
          });
          return notes.push({ 
            lecture_item_id: item.lecture_item_id,
            name: item.name,
            start_time: item.start_time,
            end_time: item.end_time,
            order: item.order,
            type: item.type,
            sequence: item.sequence,
            result: item.result,
            latest_store_teacher_id: item.latest_store_teacher_id,
            note
          });
        case 5:
          const homework = await modules.models.bank_homework.findOne({
            where: { lecture_item_id: item.lecture_item_id },
          })
          return homeworks.push({ 
            lecture_item_id: item.lecture_item_id,
            name: item.name,
            start_time: item.start_time,
            end_time: item.end_time,
            order: item.order,
            type: item.type,
            sequence: item.sequence,
            result: item.result,
            latest_store_teacher_id: item.latest_store_teacher_id,
            homework
          });
      }
    })()));

    return {
      lecture_id: lecture.lecture_id,
      name: lecture.name,
      location: lecture.location,
      type: lecture.type,
      start_time: lecture.start_time,
      end_time: lecture.end_time,
      latest_store_teacher_id: lecture.latest_store_teacher_id,
      is_auto: lecture.is_auto,
      video_link: lecture.video_link,
      lecture_serial_no: lecture.lecture_serial_no,
      keyword_state: lecture.keyword_state,
      lecture_item: {
        questions,
        surveys,
        codepractices,
        discussions,
        notes,
        homeworks,
      },
    };
  })()));

  const bankClassInfo = {
    class_id: bankClass.class_id,
    name: bankClass.name,
    summary: bankClass.summary,
    description: bankClass.description,
    capacity: bankClass.capacity,
    start_time: bankClass.start_time,
    end_time: bankClass.end_time,
    lecturer_description: bankClass.lecturer_description,
    created_at: bankClass.created_at,
    updated_at: bankClass.updated_at,
    lectureList: bankClassLectureList,
  };
  res.json({ bankClassInfo });
}));

// 개인화 문항 router
router.get('/lecture/:lectureId/search', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lectureId = parseInt(req.params.lectureId, 10);
  const keywords = req.query.keywords.split(',');
  const questionType = req.query.questionType;
  const diffs = req.query.difficulties.split(',');
  // const realDiffs = req.query.realDifficulties.split(','); 실질 난이도 미구현
  // const scores = req.query.scores.split(',');
  // 문항 유형별 추출 요청 갯수
  // const multipleChoiceQuestionNumber = req.query.multipleChoiceQuestionNumber;
  // const shortAnswerQuestionNumber = req.query.shortAnswerQuestionNumber;
  // const narrativeQuestionNumber = req.query.narrativeQuestionNumber;
  // const swQuestionNumber = req.query.swQuestionNumber;
  // const sqlQuestionNumber = req.query.sqlQuestionNumber;
  // const multiMediaQuestionNumber = req.query.multiMediaQuestionNumber;

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });

  // 강의 존재 여부 판단
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }

  // 강의 접근 권한 check 관리자 or 해당 강의 강사
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // 개인화 문항 추출 강의 이름
  const lectureName = lecture.name;

  // 강사 소속 문제은행 그룹 아이디 추출
  const groupList = await modules.models.bank_group.findAll({
    include: {
      model: modules.models.teacher_group,
      where: { user_id: authId },
      required: true,
    },
  });
  let groupIds = [];
  groupList.forEach(groupData => {
    groupIds.push(groupData.dataValues.group_id);
  });

  // 조건(강의 이름, 키워드, 배점, 난이도)에 따른 문제은행 강의 모두 추출
  const bankLectureDatas = await modules.models.bank_lecture.findAll({
    where: {
      name: lectureName,
      group_id: {
        [Op.or]: groupIds,
      },
    },
    include: [
      {
        model: modules.models.bank_lecture_item,
        include: [
          {
            model: modules.models.bank_question,
            where: {
              /* score: {
                [Op.between]: scores,
              }, */
              difficulty: {
                [Op.between]: diffs,
              },
              /* real_difficulty: {
                [Op.between]: realDiffs,
              }, */
              type: questionType,
            },
            include: [
              {
                model: modules.models.bank_question_keyword,
                where: {
                  keyword: {
                    [Op.or]: keywords,
                  },
                },
              },
            ],
          },
        ],
      },
    ],
  });

  const bankLectureNoConditionDatas = await modules.models.bank_lecture.findAll({ // 입력 조건(키워드, 난이도 등등) 없는 최대의 결과를 추출하기 위함
    where: {
      name: lectureName,
      group_id: {
        [Op.or]: groupIds,
      },
    },
    include: [
      {
        model: modules.models.bank_lecture_item,
        include: [
          {
            model: modules.models.bank_question,
            where: {
              type: questionType,
            },
            include: [
              {
                model: modules.models.bank_question_keyword,
              },
            ],
          },
        ],
      },
    ],
  });

   // 추출 결과 아이템 array
  let bankLectureItems = [];

  // 추출 유형별 array
  /* let multipleChoiceQuestionArray = [];
  let shortAnswerQuestionArray = [];
  let narrativeQuestionArray = [];
  // let swQuestionArray = [];
  // let sqlQuestionArray = [];
  let multiMediaQuestionArray = []; */

  /* const bankToLecture = async (lecture_item_id, target_lecture_id, individual_item_id) => { // lecture_item_id는 array
    const lectureItemId = lecture_item_id;
    const targetLectureId = target_lecture_id;
    const items = await modules.models.lecture_item.findAll({ where: { lecture_id: targetLectureId }});
  
    const bankLectureItem = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
      include: [{
        model: modules.models.bank_question,
        include: [
          { model: modules.models.bank_problem_testcase}, 
          { model: modules.models.bank_oj_problem },
          { model: modules.models.bank_file}, 
          { model: modules.models.bank_file, as: 'question_material' },
          { model: modules.models.bank_question_keyword} 
        ],
      }]
    });
  
    if (!bankLectureItem) {
      throw modules.errorBuilder.default('Lecture Item Not Found', 404, true);
    }
    
    const newActiveLectureItem = await modules.models.lecture_item.create({
      name: bankLectureItem.name,
      start_time: bankLectureItem.start_time,
      end_time: bankLectureItem.end_time,
      type: 7, // 개별 개인화 문항
      past_opend: 0,
      order: bankLectureItem.order,
      sequence: items.length+1,    // 기존에 있던 item len + 1
      result: bankLectureItem.result,
      offset: bankLectureItem.offset,
      opend: 0,
      scoring_finish: 0,
      lecture_id: targetLectureId,
      individual_item_id: individual_item_id,
    });
  
    const newActiveLectureItemId = newActiveLectureItem.lecture_item_id;
  
    const newList = await modules.models.lecture_item_list.create({
      lecture_id: newActiveLectureItem.lecture_id,
      item_id: newActiveLectureItemId,
      linked_list: newActiveLectureItemId,
    });

    const newGroup = await modules.models.lecture_item_group.create({
      lecture_id: newActiveLectureItem.lecture_id,
      group_list: newList.lecture_item_list_id,
      opened: 0,
      start: 0,
      end: 0
    });
  
    const bankQuestions = bankLectureItem.bank_questions;
    const newActiveQuestions = await Promise.all(bankQuestions.map(item => (async () => {
      const newActiveQuestion = await modules.models.question.create({
        lecture_item_id: newActiveLectureItemId,
        type: item.type,
        question: item.question,
        choice: item.choice,
        answer: item.answer,
        is_ordering_answer: item.is_ordering_answer,
        accept_language: item.accept_language,
        order: item.order,
        showing_order: item.showing_order,
        difficulty: item.difficulty,
        timer: item.timer,
        creator_id: item.creator_id,
        sqlite_file_guid: item.sqlite_file_guid,
      });
  
      const bankQuestionKeywords = item.bank_question_keywords;
      const newQuestionKeywords = await Promise.all(bankQuestionKeywords.map(key => (async () => {
        const newQuestionKeyword = await modules.models.question_keyword.create({
          lecture_item_id: newActiveLectureItemId,
          score_portion: key.score_portion,
          keyword: key.keyword,
          question_id: newActiveQuestion.question_id,
          lecture_id: targetLectureId,
        });
        return newQuestionKeyword;
      })()));
  
      const ojProblem = item.bank_oj_problem;
      const problemTestCases = item.bank_problem_testcases;
      let newActiveOjProblem = null;
      if (ojProblem !== null) {
        const bankOjProblem = await modules.models.oj_problem.create({
          problem_id: newActiveQuestion.question_id,
          title: ojProblem.title,
          description: ojProblem.description,
          input: ojProblem.input,
          output: ojProblem.output,
          sample_input: ojProblem.sample_input,
          sample_output: ojProblem.sample_output,
          spj: ojProblem.spj,
          hint: ojProblem.hint,
          source: ojProblem.source,
          in_date: ojProblem.in_date,
          time_limit: ojProblem.time_limit,
          memory_limit: ojProblem.memory_limit,
          defunct: ojProblem.defunct,
          accepted: ojProblem.accepted,
          submit: 0,
          solved: 0, // 생각 해보자 여기 ... 0 으로 초기화 되는게 맞는거 같은데
        });
      }
  
      let newActiveProblemTestCases = [];
      if (problemTestCases.length > 0) {
        newActiveProblemTestCases = await Promise.all(problemTestCases.map(testcase => (async () => {
          const bankProblemTestCase = await modules.models.problem_testcase.create({
            question_id: newActiveQuestion.question_id,
            num: testcase.num,
            input: testcase.input,
            output: testcase.output,
            input_path: testcase.input_path,
            output_path: testcase.output_path,
          });
          return newActiveProblemTestCases;
        })()));
      }
      const files = item.bank_files;
      if ( files.length > 0 ) {
        files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'question_id', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
        })
      }
  
      const material_files = item.question_material;
      if ( material_files.length > 0) {
        material_files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'question_id_material', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
        })
      }
  
      return {
        newActiveQuestion,
        newActiveOjProblem,
        newActiveProblemTestCases
      };
    })()));
  }; */

  let questionArray = [];

  /* const newIndividualQuestionInfoLectureItem = await modules.models.lecture_item.create({
    name: bankLectureItem.name,
    start_time: bankLectureItem.start_time,
    end_time: bankLectureItem.end_time,
    type: 6, // 개별 개인화 문항 정보
    past_opend: 0,
    order: bankLectureItem.order,
    sequence: 1,    // 기존에 있던 item len + 1
    result: bankLectureItem.result,
    offset: bankLectureItem.offset,
    opend: 0,
    scoring_finish: 0,
    lecture_id: targetLectureId,
  }); */

  bankLectureDatas.forEach(async bankLectureData => { // 검색된 문제은행에 저장된 강의 마다
    bankLectureData.bank_lecture_items.forEach(bankLectureItem => { // 그 안의 강의 아이템 마다
      /* switch (bankLectureItem.dataValues.bank_questions[0].type) { // 문항 유형 별로 분류하여 array에 넣음
        case 0: // 객관식
          multipleChoiceQuestionArray.push(bankLectureItem.dataValues);
          break;
        case 1: // 단답형
          shortAnswerQuestionArray.push(bankLectureItem.dataValues);
          break;
        case 2: // 서술형
          narrativeQuestionArray.push(bankLectureItem.dataValues);
          break;
        /* case 3: // SW
          swQuestionArray.push(bankLectureItem.dataValues);
          break;
        case 4: // SQL
          sqlQuestionArray.push(bankLectureItem.dataValues);
          break;
        case 6: // 멀티미디어 5번 지금 안씀
          multiMediaQuestionArray.push(bankLectureItem.dataValues);
          break;
      } */
      if (bankLectureItem.dataValues.bank_questions[0].choice) {
        bankLectureItem.dataValues.bank_questions[0].choice = bankLectureItem.dataValues.bank_questions[0].choice.split(modules.config.separator);
      }
      if (bankLectureItem.dataValues.bank_questions[0].answer) {
        bankLectureItem.dataValues.bank_questions[0].answer = bankLectureItem.dataValues.bank_questions[0].answer.split(modules.config.separator);
      }
      questionArray.push(bankLectureItem.dataValues);
      /* bankToLecture(bankLectureItem.dataValues.lecture_item_id, lectureId); */
    });
  });

  // 조건이 없을 때의(전체의) 추출 유형별 문항 수를 측정 하기 위한 것
  /* let multipleTotalNumber = 0;
  let shortTotalNumber = 0;
  let narrativeTotalNumber = 0;
  // let swTotalNumber = 0;
  // let sqlTotalNumber = 0;
  let multiTotalNumber = 0; */
  questionTotalNumber = 0;

  bankLectureNoConditionDatas.forEach(async bankLectureNoConditionData => { // 검색된 문제은행에 저장된 강의 마다
    bankLectureNoConditionData.bank_lecture_items.forEach(bankLectureItem => { // 그 안의 강의 아이템 마다
      /* switch (bankLectureItem.dataValues.bank_questions[0].type) { // 문항 유형 별로 분류하여 문항 수를 증가시킴 array에 push 안함
        case 0: // 객관식
          multipleTotalNumber++;
          break;
        case 1: // 단답형
          shortTotalNumber++;
          break;
        case 2: // 서술형
          narrativeTotalNumber++;
          break;
        /* case 3: // SW
          swTotalNumber++;
          break;
        case 4: // SQL
          sqlTotalNumber++;
          break; 
        case 6: // 멀티미디어 5번 지금 안씀
          multiTotalNumber++;
          break;
      } */
      questionTotalNumber++;
    });
  });

  /* const randGen = (min, max) => { // min, max 사이의 random 정수 생성 min, max는 정수여야하고 min <= max 여야 함 아니면 null return
    if (isNaN(min) || isNaN(max) || Math.floor(min) !== min || Math.floor(max) !== max || min > max) {
      return null;
    } else {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
  };

  const sortFunc = (itemArray, extNumber) => { // 랜덤하게 집어넣는 함수
    let forEachCounter = 0; // forEach문 용 카운터
    let itemPushNum = 0;
    let randSelectIdxs = new Set();
    let randSelectNumber = itemArray.length - extNumber;
    if (randSelectNumber <= 0) { // 주어진 array의 길이가 추출 요청 수 보다 작거나 같을 경우 모두 추출
      itemArray.forEach(item => {
        bankLectureItems.push(item);
        itemPushNum++;
      });
    } else { // 클 경우
      // set으로 중복 제거 됨 randGen으로 제외할 idx 선택
      for (; randSelectIdxs.size < randSelectNumber;) { // eslint-disable-line
        randSelectIdxs.add(randGen(0, itemArray.length - 1));
      }
      // randSelectIdxs를 제외한 나머지 것을 집어넣음
      itemArray.forEach(item => {
        if (randSelectIdxs.has(forEachCounter) === false) {
          bankLectureItems.push(item);
          itemPushNum++;
        }
        forEachCounter++;
      });
    }
    return itemPushNum;
  };
  // 문항 유형별 random하게 결과 array에 집어넣기
  let multipleSelectNumber= sortFunc(multipleChoiceQuestionArray, multipleChoiceQuestionNumber);
  let shortSelectNumber = sortFunc(shortAnswerQuestionArray, shortAnswerQuestionNumber);
  let narrativeSelectNumber = sortFunc(narrativeQuestionArray, narrativeQuestionNumber);
  // sortFunc(swQuestionArray, swQuestionNumber);
  // sortFunc(sqlQuestionArray, sqlQuestionNumber);
  let multiSelectNumber = sortFunc(multiMediaQuestionArray, multiMediaQuestionNumber);
  res.json({ bankLectureItems, extractionInfo: { multipleSelectNumber, multipleTotalNumber, shortSelectNumber, shortTotalNumber,
    narrativeSelectNumber, narrativeTotalNumber, multiSelectNumber, multiTotalNumber } }); */
  res.json({ questionArray, questionTotalNumber, bankLectureExist: bankLectureDatas.length });
}));

// lectureId라는 id를 가진 강의의 정보
router.get('/lecture/:lectureId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lectureId = parseInt(req.params.lectureId, 10);

  const classId = (await modules.models.bank_lecture.findByPk(lectureId)).class_id;
  let groupId;

  if (classId) {
    groupId = (await modules.models.bank_class.findByPk(classId)).group_id;
  } else {
    groupId = (await modules.models.bank_lecture.findByPk(lectureId)).group_id;
  }

  const teacherCheck = await modules.teacherInGroup(authId, groupId);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const bankLecture = await modules.models.bank_lecture.findByPk(lectureId, {
    attributes: modules.models.bank_lecture.selects.lectureInfo,
    include: [
      {
        model: modules.models.bank_lecture_item,
        attributes: modules.models.bank_class.selects.lcItemInfo,
      },
    ]
  });
  let questions = [];
  let surveys = [];
  let codepractices = [];
  let discussions = [];
  let notes = [];
  let homeworks = [];
  await Promise.all(bankLecture.bank_lecture_items.map(item => (async () => {
    switch (item.type) {
      case 0:
        const question = await modules.models.bank_question.findOne({
          where: { lecture_item_id: item.lecture_item_id },
        });
        return questions.push({ 
          lecture_item_id: item.lecture_item_id,
          name: item.name,
          start_time: item.start_time,
          end_time: item.end_time,
          order: item.order,
          type: item.type,
          sequence: item.sequence,
          result: item.result,
          latest_store_teacher_id: item.latest_store_teacher_id,
          question,
        });
      case 1:
        const survey = await modules.models.bank_survey.findOne({
          where: { lecture_item_id: item.lecture_item_id },
        });
        return surveys.push({ 
          lecture_item_id: item.lecture_item_id,
          name: item.name,
          start_time: item.start_time,
          end_time: item.end_time,
          order: item.order,
          type: item.type,
          sequence: item.sequence,
          result: item.result,
          latest_store_teacher_id: item.latest_store_teacher_id,
          survey 
        });
      case 2:
        const codepractice = await modules.models.bank_lecture_code_practice.findOne({
          where: { lecture_item_id: item.lecture_item_id },
        });
        return codepractices.push({ 
          lecture_item_id: item.lecture_item_id,
          name: item.name,
          start_time: item.start_time,
          end_time: item.end_time,
          order: item.order,
          type: item.type,
          sequence: item.sequence,
          result: item.result,
          latest_store_teacher_id: item.latest_store_teacher_id,
          codepractice
        });
      case 3:
        const discussion = await modules.models.bank_discussion_info.findOne({
          where: { lecture_item_id: item.lecture_item_id },
        });
        return discussions.push({ 
          lecture_item_id: item.lecture_item_id,
          name: item.name,
          start_time: item.start_time,
          end_time: item.end_time,
          order: item.order,
          type: item.type,
          sequence: item.sequence,
          result: item.result,
          latest_store_teacher_id: item.latest_store_teacher_id,
          discussion
        });
      case 4:
        const note = await modules.models.bank_note.findOne({
          where: { lecture_item_id: item.lecture_item_id },
        });
        return notes.push({ 
          lecture_item_id: item.lecture_item_id,
          name: item.name,
          start_time: item.start_time,
          end_time: item.end_time,
          order: item.order,
          type: item.type,
          sequence: item.sequence,
          result: item.result,
          latest_store_teacher_id: item.latest_store_teacher_id,
          note
        });
      case 5:
        const homework = await modules.models.bank_homework.findOne({
          where: { lecture_item_id: item.lecture_item_id },
        })
        return homeworks.push({ 
          lecture_item_id: item.lecture_item_id,
          name: item.name,
          start_time: item.start_time,
          end_time: item.end_time,
          order: item.order,
          type: item.type,
          sequence: item.sequence,
          result: item.result,
          latest_store_teacher_id: item.latest_store_teacher_id,
          homework
        });
      }
  })()));  
  const lectureInfo = { 
    lecture_id: bankLecture.lecture_id, 
    name: bankLecture.name, 
    location: bankLecture.location,
    type: bankLecture.type,
    start_time: bankLecture.start_time,
    end_time: bankLecture.end_time,
    latest_store_teacher_id: bankLecture.latest_store_teacher_id,
    is_auto: bankLecture.is_auto,
    video_link: bankLecture.video_link,
    keyword_state: bankLecture.keyword_state,
    lecture_serial_no: bankLecture.lecture_serial_no,
  };
  const bankLectureInfo = {
    lectureInfo,
    questions,
    surveys,
    codepractices,
    discussions,
    notes,
    homeworks,
  };
  res.json({ bankLectureInfo });
}));

// lectureItemId라는 id를 가진 아이템의 정보
router.get('/lecture-item/:lectureItemId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureItemId = parseInt(req.params.lectureItemId, 10);
  const { authId, authType } = req.decoded;

  const lectureId = (await modules.models.bank_lecture_item.findByPk(lectureItemId)).lecture_id;
  let groupId;

  if (lectureId) {
    const classId = (await modules.models.bank_lecture.findByPk(lectureId)).class_id;
    if (classId) {
      groupId = (await modules.models.bank_class.findByPk(classId)).group_id;
    } else {
      groupId = (await modules.models.bank_lecture.findByPk(lectureId)).group_id;
    }
  } else {
    groupId = (await modules.models.bank_lecture_item.findByPk(lectureItemId)).group_id;
  }

  const teacherCheck = await modules.teacherInGroup(authId, groupId);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const bankLectureItemType = (await modules.models.bank_lecture_item.findByPk(lectureItemId)).type;
  let bankLectureItemInfo;
  
  switch (bankLectureItemType) {
      case 0:
        bankLectureItemInfo = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
          include: {
            model: modules.models.bank_question,
          },
        });
        break;
      case 1:
        bankLectureItemInfo = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
          include: {
            model: modules.models.bank_survey,
          },
        });
        break;
      case 2:
        bankLectureItemInfo = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
          include: {
            model: modules.models.bank_lecture_code_practice,
          },
        });
        break;
      case 3:
        bankLectureItemInfo = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
          include: {
            model: modules.models.bank_discussion_info,
          },
        });
        break;
      case 4:
        bankLectureItemInfo = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
          include: {
            model: modules.models.bank_note,
          },
        });
        break;
      case 5:
        bankLectureItemInfo = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
          include: {
            model: modules.models.bank_homework,
          },
        });
        break;
      }
  res.json({ bankLectureItemInfo });
}));

// 과목 가져오기
router.post('/to/class', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { class_id } = req.body;
  const classId = class_id;

  const bankClass = await modules.models.bank_class.findByPk(classId, {
    include: {
      model: modules.models.bank_lecture,
      include: {
        model: modules.models.bank_lecture_item,
      }
    },
  });

  const teacherCheck = await modules.teacherInGroup(authId, bankClass.group_id);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  if (!bankClass) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }

  /* coverage 안 쓰기로 함
  const bankClassCoverage = await modules.models.bank_class_coverage.findOne({
    where: { class_id: classId }, 
  });
  */

  /* class keyword도 따로 없음
  const bankClassKeywords = await modules.models.bank_class_keyword.findAll({
    where: { class_id: classId },
  });
  */
  const bankLectures = bankClass.bank_lectures;

  const newActiveClass = await modules.models.class.create({
    name: bankClass.name,
    summary: bankClass.summary,
    description: bankClass.description,
    capacity: bankClass.capacity,
    start_time: bankClass.start_time,
    end_time: bankClass.end_time,
    opened: bankClass.opened,
    lecturer_description: bankClass.lecturer_description,
    master_id: authId,
  });

  const newLectures = bankLectures.map(item => {
    return {
      class_id: newActiveClass.class_id,
      name: item.name,
      location: item.location,
      type: item.type,
      start_time: item.start_time,
      end_time: item.end_time,
      is_auto: item.is_auto,
      video_link: item.video_link,
      keyword_state: item.keyword_state,
      teacher_id: authId,
    }
  });

  /* coverage와 keyword 삭제
  let newActiveClassCoverage = null;
  if (bankClassCoverage) { // 이 부분 오류로 빼야 할 것 같음
    newActiveClassCoverage = await modules.models.class_coverage.create({
      class_id: newActiveClass.class_id,
      question_error: bankClassCoverage.question_error,
      material_error: bankClassCoverage.material_error,
    });
  }

  const newClassKeywords = bankClassKeywords.map(item => {
    return {
      class_id: newActiveClass.class_id,
      keyword: item.keyword,
      weight: item.weight,
      weight_ratio: item.weight_ratio,
      question_score_ratio: item.question_score_ratio,
      material_score_ratio: item.material_score_ratio,
      difference_question: item.difference_question,
      difference_material: item.difference_material,
    };
  });
  const newActiveClassKeywords = await modules.models.class_keyword.bulkCreate(newClassKeywords);
  */
  
  const newActiveLectures = await Promise.all(bankLectures.map(lc => (async () => {

    const lectureId = lc.lecture_id;

    const bankLectureKeywords = await modules.models.bank_lecture_keyword.findAll({
      where: { lecture_id: lectureId, },
    });
    const bankLectureKeywordRelations = await modules.models.bank_lecture_keyword_relation.findAll({
      where: { lecture_id: lectureId, },
    });
    const bankLectureMaterials = await modules.models.bank_material.findAll({
      where: { lecture_id: lectureId, },
      include: { model: modules.models.bank_file },
    });
    /* 은행에 해당 테이블 없음
    const bankPlist = await modules.models.bank_lecture_accept_plist.findAll({
      where: { lecture_id: lectureId, },
    });
    */

    const newActiveLecture = await modules.models.lecture.create({
      class_id: newActiveClass.class_id,
      name: lc.name,
      location: lc.location,
      type: lc.type,
      start_time: lc.start_time,
      end_time: lc.end_time,
      is_auto: lc.is_auto,
      video_link: lc.video_link,
      keyword_state: lc.keyword_state,
      teacher_id: authId,
    });

    const newLectureKeyword = bankLectureKeywords.map(item => {
      return {
        lecture_id: newActiveLecture.lecture_id,
        keyword: item.keyword,
        weight: item.weight,
        weight_ratio: item.weight_ratio,
        question_score_ratio: item.question_score_ratio,
        material_score_ratio: item.material_score_ratio,
        difference_question: item.difference_question,
        difference_material: item.difference_material,
      };
    });
    let newActiveLectureKeywords = [];
    if (newLectureKeyword) {
      newActiveLectureKeywords = await modules.models.lecture_keyword.bulkCreate(newLectureKeyword);
    }

    const newLectureKeywordRelations = bankLectureKeywordRelations.map(item => {
      return {
        lecture_id: newActiveLecture.lecture_id,
        node1: item.node1,
        node2: item.node2,
        weight: item.weight,
      };
    });
    let newActiveLectureKeywordRelations = [];
    if (newLectureKeywordRelations) {
      newActiveLectureKeywordRelations = await modules.models.lecture_keyword_relation.bulkCreate(newLectureKeywordRelations);
    }

    const newActiveMaterials = await Promise.all(bankLectureMaterials.map(material => (async () => {
      const newActiveMaterial = await modules.models.material.create({
        comment: material.comment,
        score_sum: material.score_sum,
        lecture_id: newActiveLecture.lecture_id,
        creator_id: material.creator_id,
        material_type: material.material_type,
      });
      const file = material.bank_file;
      const bankLectureMaterialKeywords = await modules.models.bank_material_keyword.findAll({ 
        where: { material_id: material.material_id, },
      });
      const newMaterialKeywords = bankLectureMaterialKeywords.map((item) => {
        return {
          lecture_id: newActiveLecture.lecture_id,
          material_id: newActiveMaterial.material_id,
          keyword: item.keyword,
          score: item.score,
        }
      });
      const newActiveMaterialKeywords = await modules.models.material_keyword.bulkCreate(newMaterialKeywords);
      const newActiveMaterialFile = await modules.upload.copy(file.static_path, file.name, 'material_id', newActiveMaterial.material_id, authId, null, 1);

      return {
        newActiveMaterial,
        newActiveMaterialKeywords,
        newActiveMaterialFile
      };
    })()));

    /* PList 무시
    const newActiveLectureAcceptPlist = await Promise.all(bankPlist.map(program => (async () => {
      const newActiveLectureProgram = modules.models.lecture_accept_plist.create({
        plist_id: program.plist_id,
        lecture_id: newActiveLecture.lecture_id,
      });

      return newActiveLectureProgram;
    })()));
    */


    const bankLectureItems = await modules.models.bank_lecture_item.findAll({
      where: { lecture_id: lectureId },
    });

    let item_id_dic = {};

    const newActiveLectureItems = await Promise.all(bankLectureItems.map(lcItem => (async () => {
      const newActiveLectureItem = await modules.models.lecture_item.create({
        name: lcItem.name,
        start_time: lcItem.start_time,
        end_time: lcItem.end_time,
        type: lcItem.type,
        past_opend: 0,
        order: lcItem.order,
        sequence: lcItem.sequence,
        result: lcItem.result,
        opend: 0,
        scoring_finish: 0,
        lecture_id: newActiveLecture.lecture_id,
        offset: lcItem.offset,
      });

      const newActiveLectureItemId = newActiveLectureItem.lecture_item_id;

      // key는 bank_item , value는 lecture_item
      item_id_dic[lcItem.lecture_item_id] = newActiveLectureItemId

      const bankHomeworks = await modules.models.bank_homework.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id },
        include: [{
          model: modules.models.bank_file
        },{
          model: modules.models.bank_homework_keyword
        }]
      });
      const newActiveHomeworks = await Promise.all(bankHomeworks.map(item => (async () => {
        const newActiveHomework = await modules.models.homework.create({
          lecture_item_id: newActiveLectureItemId,
          comment: item.comment,
        });
        const bankHomeworkKeywords = item.bank_homework_keywords;
        const newHomeworkKeywords = await Promise.all(bankHomeworkKeywords.map(key => (async () => {
          const newHomeworkKeyword = await modules.models.homework_keyword.create({
            lecture_item_id: newActiveLectureItemId,
            score_portion: key.score_portion,
            keyword: key.keyword,
            homework_id: newActiveHomework.homework_id,
          });
          return newHomeworkKeyword;
        })()));
        const files = item.bank_files;
        const newActiveHomeworkFiles = await Promise.all(files.map(file => (async () => {
          const newFile = await modules.upload.copy(file.static_path, file.name, 'homework_id', newActiveHomework.homework_id, authId, newActiveLectureItemId, 1);
          return newFile;
        })()));
        return { newActiveHomework, newActiveHomeworkFiles };
      })()));

      const bankNotes = await modules.models.bank_note.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id },
        include: [{
          model: modules.models.bank_file
        },{
          model: modules.models.bank_note_keyword
        }]
      });
      const newActiveNotes = await Promise.all(bankNotes.map(item => (async () => {
        const newActiveNote = await modules.models.note.create({
          lecture_item_id: newActiveLectureItemId,
          note_type: item.note_type,
          url: item.url,
          note_file: item.note_file,
          creator_id: item.creator_id,
          youtube_interval: item.youtube_interval,
        });
        const bankNoteKeywords = item.bank_note_keywords;
        const newNoteKeywords = await Promise.all(bankNoteKeywords.map(key => (async () => {
          const newNoteKeyword = await modules.models.note_keyword.create({
            lecture_id: newActiveLecture.lecture_id,
            lecture_item_id: newActiveLectureItemId,
            score_portion: key.score_portion,
            keyword: key.keyword,
            note_id: newActiveNote.note_id,
          });
          return newNoteKeyword;
        })()));
        const files = item.bank_files;
      if ( files.length > 0 ) {
        files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'note_id', newActiveNote.note_id, authId, newActiveLectureItemId, 1);
        })
      }

        return { newActiveNote  };
      })()));

      const bankSurveys = await modules.models.bank_survey.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id },
        include: [{
          model: modules.models.bank_file
        },{
          model: modules.models.bank_survey_keyword
        }]
      });
      const newActiveSurveys = await Promise.all(bankSurveys.map(item => (async () => {
        const newActiveSurvey = await modules.models.survey.create({
          lecture_item_id: newActiveLectureItemId,
          type: item.type,
          comment: item.comment,
          choice: item.choice,
          creator_id: item.creator_id,
        });
        const bankSurveyKeywords = item.bank_survey_keywords;
        const newSurveyKeywords = await Promise.all(bankSurveyKeywords.map(key => (async () => {
          const newSurveyKeyword = await modules.models.survey_keyword.create({
            lecture_id: newActiveLecture.lecture_id,
            lecture_item_id: newActiveLectureItemId,
            score_portion: key.score_portion,
            keyword: key.keyword,
            survey_id: newActiveSurvey.survey_id,
          });
          return newSurveyKeyword;
        })()));
        const files = item.bank_files;
        const newActiveSurveyFiles = await Promise.all(files.map(file => (async () => {
          const newFile = await modules.upload.copy(file.static_path, file.name, 'survey_id', newActiveSurvey.survey_id, authId, newActiveLectureItemId, 1);
          return newFile;
        })()));
        return { newActiveSurvey, newActiveSurveyFiles };
      })()));

      const bankCodePractices = await modules.models.bank_lecture_code_practice.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id },
        include: { model: modules.models.bank_lecture_code_practice_keyword }
      });
      const newActiveCodePractices = await Promise.all(bankCodePractices.map(item => (async () => {
        const newActiveCodePractice = await modules.models.lecture_code_practice.create({
          lecture_item_id: newActiveLectureItemId,
          score: item.score,
          code: item.code,
          is_jupyter: item.is_jupyter,
          activated_time: item.activated_time,
          deactivated_time: item.deactivated_time,
          language_type: item.language_type,
          difficulty: item.difficulty,
          user_id: item.user_id,
        });
        const bankCodePracticeKeywords = item.bank_lecture_code_practice_keywords;
        const newCodePracticeKeywords = await Promise.all(bankCodePracticeKeywords.map(key => (async () => {
          const newCodePracticeKeyword = await modules.models.lecture_code_practice_keyword.create({
            lecture_item_id: newActiveLectureItemId,
            score_portion: key.score_portion,
            keyword: key.keyword,
            lecture_code_practice_id: newActiveCodePractice.lecture_code_practice_id,
          });
          return newCodePracticeKeyword;
        })()));
        return newActiveCodePractice;
      })()));

      const bankQuestions = await modules.models.bank_question.findAll({
        where: { lecture_item_id: lcItem.lecture_item_id },
        include: [
          { model: modules.models.bank_oj_problem, },
          { model: modules.models.bank_problem_testcase },
          { model: modules.models.bank_file },
          { model: modules.models.bank_file, as: 'question_material' },
          { model: modules.models.bank_question_keyword },
        ],
      });
      const newActiveQuestions = await Promise.all(bankQuestions.map(item => (async () => {
        const newActiveQuestion = await modules.models.question.create({
          lecture_item_id: newActiveLectureItemId,
          type: item.type,
          question: item.question,
          choice: item.choice,
          choice2: item.choice2,
          answer: item.answer,
          is_ordering_answer: item.is_ordering_answer,
          accept_language: item.accept_language,
          order: item.order,
          showing_order: item.showing_order,
          difficulty: item.difficulty,
          timer: item.timer,
          creator_id: item.creator_id,
          sqlite_file_guid: item.sqlite_file_guid,
          answer_media_type: item.answer_media_type,
          question_media_type: item.question_media_type,
          question_media: item.question_media,
        });
        const bankQuestionKeywords = item.bank_question_keywords;
        const newQuestionKeywords = await Promise.all(bankQuestionKeywords.map(key => (async () => {
          const newQuestionKeyword = await modules.models.question_keyword.create({
            lecture_id: newActiveLecture.lecture_id,
            lecture_item_id: newActiveLectureItemId,
            score_portion: key.score_portion,
            keyword: key.keyword,
            question_id: newActiveQuestion.question_id,
          });
          return newQuestionKeyword;
        })()));
        const ojProblem = item.bank_oj_problem;
        let newActiveOjProblem = null;
        if (ojProblem !== null) {
          newActiveOjProblem = await modules.models.oj_problem.create({
            problem_id: newActiveQuestion.question_id,
            title: ojProblem.title,
            description: ojProblem.description,
            input: ojProblem.input,
            output: ojProblem.output,
            sample_input: ojProblem.sample_input,
            sample_output: ojProblem.sample_output,
            spj: ojProblem.spj,
            hint: ojProblem.hint,
            source: ojProblem.source,
            in_date: ojProblem.in_date,
            time_limit: ojProblem.time_limit,
            memory_limit: ojProblem.memory_limit,
            defunct: ojProblem.defunct,
            accepted: ojProblem.accepted,
            submit: ojProblem.submit,
            solved: 0, // 생각 해보자 여기 ... 0 으로 초기화 되는게 맞는거 같은데
          });
        }

        const problemTestCases = item.bank_problem_testcases;
        let newActiveProblemTestCases = [];
        if (problemTestCases.length > 0) {
          newActiveProblemTestCases = await Promise.all(problemTestCases.map(testcase => (async () => {
            const newActiveProblemTestCase = await modules.models.problem_testcase.create({
              question_id: newActiveQuestion.question_id,
              num: testcase.num,
              input: testcase.input,
              output: testcase.output,
              input_path: testcase.input_path,
              output_path: testcase.output_path,
            });
            return newActiveProblemTestCase;
          })()));
        }

        
        /////////////////////// file 복사 ////////////////////////////
        const files = item.bank_files;
        if ( files.length > 0 ) {
          files.forEach(function(element){
            modules.upload.copy(element.static_path, element.name, 'question_id', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
          })
        }
        const material_files = item.question_material;
        if ( material_files.length > 0) {
          material_files.forEach(function(element){
            modules.upload.copy(element.static_path, element.name, 'question_id_material', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
          })
        }

        return {
          newActiveQuestion,
          newActiveOjProblem,
          newActiveProblemTestCases
        };
      })()));

      const bankDiscussion = await modules.models.bank_discussion_info.findOne({
        where: { lecture_item_id: lcItem.lecture_item_id },
      });
      let newActiveDiscussion = null;
      if (bankDiscussion !== null) {
        newActiveDiscussion = await modules.models.discussion_info.create({
          lecture_item_id: newActiveLectureItemId,
          topic: bankDiscussion.topic,
          difficulty: bankDiscussion.difficulty,
          student_share: bankDiscussion.student_share,
        });
      }

      return {
        newActiveLectureItem,
        newActiveHomeworks,
        newActiveQuestions,
        newActiveDiscussion,
        newActiveNotes,
        newActiveSurveys,
        newActiveCodePractices
      };
    })()));

    let list_dic = {};
    const lists = await modules.models.bank_lecture_item_list.findAll({ where: { lecture_id: lectureId }});
    for (let i = 0 ; i < lists.length; i +=1) {
      let list = lists[i].linked_list;
      let ids_ary = list.split(modules.config.separator);
      let newItemIds = [];
      for (let j = 0 ; j < ids_ary.length ; j += 1) {
        newItemIds.push(item_id_dic[ids_ary[j]]);
      }
      let joinedNewItemIds = newItemIds.join(modules.config.separator);
      let newList = await modules.models.lecture_item_list.create({ 
        lecture_id: newActiveLecture.lecture_id,
        item_id: newItemIds[0],
        linked_list: joinedNewItemIds,
      });
      list_dic[lists[i].lecture_item_list_id] = newList.lecture_item_list_id;
    }
    
    // group 생성
    const groups = await modules.models.bank_lecture_item_group.findAll({ where: { lecture_id: lectureId }});
    for (let i = 0 ; i < groups.length ; i += 1) {
      let group = groups[i].group_list;
      let lists_ary = group.split(modules.config.separator);
      let newListIds = [];
      for (let j = 0 ; j < lists_ary.length ; j += 1) {
        newListIds.push(list_dic[lists_ary[j]]);
      }
      let joinedNewListIds = newListIds.join(modules.config.separator);
      const newGroup = await modules.models.lecture_item_group.create({
        lecture_id: newActiveLecture.lecture_id,
        group_list: newListIds[0],
        opened: groups[i].opened,
        start: groups[i].start,
        end: groups[i].end,
      });
    };
  

    return {
      newActiveLecture,
      newActiveLectureKeywords,
      newActiveLectureKeywordRelations,
      // newActiveLectureAcceptPlist,
      newActiveMaterials,
      newActiveLectureItems,
    };
  })()));


 
  

  await modules.models.user_class.create({
    role: 'teacher',
    class_id: newActiveClass.class_id,
    user_id: authId,
  });

  res.json({ success: true, newActiveClass });
}));

// 강의 가져오기
router.post('/to/lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lecture_id, target_class_id } = req.body;
  const lectureId = lecture_id;
  const targetClassId = target_class_id;

  const targetClass = await modules.models.class.findByPk(targetClassId);

  if (!targetClass) {
    throw modules.errorBuilder.default('Class Not Found', 404, true);
  }
  const bankLecture = await modules.models.bank_lecture.findByPk(lectureId, {
    include: [{
      model: modules.models.bank_lecture_item,
    }, {
      model: modules.models.bank_lecture_keyword,
    }, {
      model: modules.models.bank_lecture_keyword_relation,
    }, {
      as: "bank_material",
      model: modules.models.bank_material,
      include: { model: modules.models.bank_file }
    }, 
    /* 없어진 것 같음. 이거땜에 오류 발생 {
      model: modules.models.bank_lecture_accept_plist,
    }*/
    ],
  });

  if (!bankLecture) {
    throw modules.errorBuilder.default('lecture Not Found in Bank', 404, true);
  }
  const teacherCheck = await modules.teacherInGroup(authId, bankLecture.group_id);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const newActiveLecture = await modules.models.lecture.create({
    name: bankLecture.name,
    location: bankLecture.location,
    type: bankLecture.type,
    start_time: bankLecture.start_time,
    end_time: bankLecture.end_time,
    is_auto: bankLecture.is_auto,
    video_link: bankLecture.video_link,
    keyword_state: bankLecture.keyword_state,
    class_id: targetClassId,
    teacher_id: authId,
  });

  const bankLectureKeywords = bankLecture.bank_lecture_keywords;
  const newLectureKeywords = bankLectureKeywords.map(item => {
    return {
      lecture_id: newActiveLecture.lecture_id,
      keyword: item.keyword,
      weight: item.weight,
      weight_ratio: item.weight_ratio,
      question_score_ratio: item.question_score_ratio,
      material_score_ratio: item.material_score_ratio,
      difference_question: item.difference_question,
      difference_material: item.difference_material,
    };
  });
  if (newLectureKeywords) {
    const newActiveLectureKeywords = await modules.models.lecture_keyword.bulkCreate(newLectureKeywords);
  }
  const bankLectureKeywordRelations = bankLecture.bank_lecture_keyword_relations;
  const newLectureKeywordRelations = bankLectureKeywordRelations.map(item => {
    return {
      lecture_id: newActiveLecture.lecture_id,
      node1: item.node1,
      node2: item.node2,
      weight: item.weight,
    };
  });
  if (newLectureKeywordRelations) {
    const newActiveLectureKeywordRelations = await modules.models.lecture_keyword_relation.bulkCreate(newLectureKeywordRelations);
  }
  
  const bankLectureMaterials = bankLecture.bank_material;

  const newActiveMaterials = await Promise.all(bankLectureMaterials.map(material => (async () => {
    const newActiveMaterial = await modules.models.material.create({
      comment: material.comment,
      score_sum: material.score_sum,
      lecture_id: newActiveLecture.lecture_id,
      creator_id: material.creator_id,
      material_type: material.material_type,
    });
    const file = material.bank_file;
    const bankLectureMaterialKeywords = await modules.models.bank_material_keyword.findAll({ 
      where: { material_id: material.material_id, },
    });
    const newMaterialKeywords = bankLectureMaterialKeywords.map((item) => {
      return {
        lecture_id: newActiveLecture.lecture_id,
        material_id: newActiveMaterial.material_id,
        keyword: item.keyword,
        score: item.score,
      }
    });
    const newActiveMaterialKeywords = await modules.models.material_keyword.bulkCreate(newMaterialKeywords);
    const newActiveMaterialFile = await modules.upload.copy(file.static_path, file.name, 'material_id', newActiveMaterial.material_id, authId, null, 1);

    return {
      newActiveMaterial,
      newActiveMaterialKeywords,
      newActiveMaterialFile
    };
  })()));
  /* 여기서 오류 발생
  const bankPlist = bankLecture.bank_lecture_accept_plists;
  const newActiveLectureAcceptPlist = await Promise.all(bankPlist.map(program => (async () => {
    modules.models.lecture_accept_plist.create({
      plist_id: program.plist_id,
      lecture_id: newActiveLecture.lecture_id,
    });
  })()));
  */
  const bankLectureItems = bankLecture.bank_lecture_items;

  let item_id_dic = {};

  const newActiveLectureItems = await Promise.all(bankLectureItems.map(lcItem => (async () => {
    const newActiveLectureItem = await modules.models.lecture_item.create({
      name: lcItem.name,
      start_time: lcItem.start_time,
      end_time: lcItem.end_time,
      type: lcItem.type,
      past_opend: 0,
      order: lcItem.order,
      sequence: lcItem.sequence,
      result: lcItem.result,
      opend: 0,
      scoring_finish: 0,
      lecture_id: newActiveLecture.lecture_id,
      offset: lcItem.offset,
    });

    const newActiveLectureItemId = newActiveLectureItem.lecture_item_id;

    item_id_dic[lcItem.lecture_item_id] = newActiveLectureItemId;

    const bankHomeworks = await modules.models.bank_homework.findAll({
      where: { lecture_item_id: lcItem.lecture_item_id },
      include: [{
        model: modules.models.bank_file
      },{
        model: modules.models.bank_homework_keyword
      }]
    });
    const newActiveHomeworks = await Promise.all(bankHomeworks.map(item => (async () => {
      const newActiveHomework = await modules.models.homework.create({
        lecture_item_id: newActiveLectureItemId,
        comment: item.comment,
      });
      const bankHomeworkKeywords = item.bank_homework_keywords;
      const newHomeworkKeywords = await Promise.all(bankHomeworkKeywords.map(key => (async () => {
        const newHomeworkKeyword = await modules.models.homework_keyword.create({
          lecture_item_id: newActiveLectureItemId,
          score_portion: key.score_portion,
          keyword: key.keyword,
          homework_id: newActiveHomework.homework_id,
        });
        return newHomeworkKeyword;
      })()));
      const files = item.bank_files;
      const newActiveHomeworkFiles = await Promise.all(files.map(file => (async () => {
        const newFile = await modules.upload.copy(file.static_path, file.name, 'homework_id', newActiveHomework.homework_id, authId, newActiveLectureItemId, 1);
        return newFile;
      })()));
      return { newActiveHomework, newActiveHomeworkFiles };
    })()));

    const bankNotes = await modules.models.bank_note.findAll({
      where: { lecture_item_id: lcItem.lecture_item_id },
      include: [{
        model: modules.models.bank_file,
      },{
        model: modules.models.bank_note_keyword,
      }]
    });
    const newActiveNotes = await Promise.all(bankNotes.map(item => (async () => {
      const newActiveNote = await modules.models.note.create({
        lecture_item_id: newActiveLectureItemId,
        note_type: item.note_type,
        url: item.url,
        note_file: item.note_file,
        creator_id: item.creator_id,
        youtube_interval: item.youtube_interval,
      });
      const bankNoteKeywords = item.bank_note_keywords;
      const newNoteKeywords = await Promise.all(bankNoteKeywords.map(key => (async () => {
        const newNoteKeyword = await modules.models.note_keyword.create({
          lecture_item_id: newActiveLectureItemId,
          score_portion: key.score_portion,
          keyword: key.keyword,
          note_id: newActiveNote.note_id,
        });
        return newNoteKeyword;
      })()));
      const files = item.bank_files;
      if ( files.length > 0 ) {
        files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'note_id', newActiveNote.note_id, authId, newActiveLectureItemId, 1);
        })
      }
      return { newActiveNote, newNoteKeywords };
    })()));

    const bankSurveys = await modules.models.bank_survey.findAll({
      where: { lecture_item_id: lcItem.lecture_item_id },
      include: [{
        model: modules.models.bank_file
      },{
        model: modules.models.bank_survey_keyword
      }]
    });
    const newActiveSurveys = await Promise.all(bankSurveys.map(item => (async () => {
      const newActiveSurvey = await modules.models.survey.create({
        lecture_item_id: newActiveLectureItemId,
        type: item.type,
        comment: item.comment,
        choice: item.choice,
        creator_id: item.creator_id,
      });
      const bankSurveyKeywords = item.bank_survey_keywords;
      const newSurveyKeywords = await Promise.all(bankSurveyKeywords.map(key => (async () => {
        const newSurveyKeyword = await modules.models.survey_keyword.create({
          lecture_item_id: newActiveLectureItemId,
          score_portion: key.score_portion,
          keyword: key.keyword,
          survey_id: newActiveSurvey.survey_id,
        });
        return newSurveyKeyword;
      })()));
      const files = item.bank_files;
      const newActiveSurveyFiles = await Promise.all(files.map(file => (async () => {
        const newFile = await modules.upload.copy(file.static_path, file.name, 'survey_id', newActiveSurvey.survey_id, authId, newActiveLectureItemId, 1);
        return newFile;
      })()));
      return { newActiveSurvey, newActiveSurveyFiles };
    })()));

    const bankCodePractices = await modules.models.bank_lecture_code_practice.findAll({
      where: { lecture_item_id: lcItem.lecture_item_id },
      include: { model: modules.models.bank_lecture_code_practice_keyword }
    });
    const newActiveCodePractices = await Promise.all(bankCodePractices.map(item => (async () => {
      const newActiveCodePractice = await modules.models.lecture_code_practice.create({
        lecture_item_id: newActiveLectureItemId,
        score: item.score,
        code: item.code,
        is_jupyter: item.is_jupyter,
        activated_time: item.activated_time,
        deactivated_time: item.deactivated_time,
        language_type: item.language_type,
        difficulty: item.difficulty,
        user_id: item.user_id,
      });
      const usersInClass = await modules.models.user_class.findAll({
        where: {
          class_id: targetClassId,
        },
        include: {
          model: modules.models.user,
        },
      });
      // create directory educator & student
      await execTimeout10("sudo docker exec class"+ targetClassId  + " su educator -c 'mkdir " + "/home/educator/practice_item_" + newActiveLectureItemId+ "'");
      for (const user of usersInClass) {
        await execTimeout10("sudo docker exec class" + targetClassId +" su " + user.user.email_id + " -c 'mkdir " + "/home/" + user.user.email_id + "/practice_item_" + newActiveLectureItemId + "'");
      }
      
      const classname = `class${targetClassId}`;

      const oriBankFilePath = path.join(modules.config.appRoot, `public/materials/bank_item_${lcItem.lecture_item_id}`);
      const newDockerFilePath = `${classname}:/home/educator/practice_item_${newActiveLectureItemId}`;

      // copy server to docker ipynb file
      /*
      const cmdFile = `sudo docker cp ${oriBankFilePath}/Untitled.ipynb ${newDockerFilePath}/Untitled.ipynb`;
      await execCmd(cmdFile, function (error, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
      }); */
      await execTimeout10("sudo docker cp " + oriBankFilePath + "/Untitled.ipynb " + newDockerFilePath + "/Untitled.ipynb");

      // file permission
      await execTimeout10("sudo docker exec class" + targetClassId + " bash -c 'chown educator.educator /home/educator/practice_item_" + newActiveLectureItemId+ "/Untitled.ipynb'");

      const bankCodePracticeKeywords = item.bank_lecture_code_practice_keywords;
      const newCodePracticeKeywords = await Promise.all(bankCodePracticeKeywords.map(key => (async () => {
        const newCodePracticeKeyword = await modules.models.lecture_code_practice_keyword.create({
          lecture_item_id: newActiveLectureItemId,
          score_portion: key.score_portion,
          keyword: key.keyword,
          lecture_code_practice_id: newActiveCodePractice.lecture_code_practice_id,
        });
        return newCodePracticeKeyword;
      })()));
      return newActiveCodePractice;
    })()));

    const bankQuestions = await modules.models.bank_question.findAll({
      where: { lecture_item_id: lcItem.lecture_item_id },
      include: [
        { model: modules.models.bank_oj_problem, },
        { model: modules.models.bank_problem_testcase },
        { model: modules.models.bank_file },
        { model: modules.models.bank_file, as: 'question_material' },
        { model: modules.models.bank_question_keyword },
      ],
    });
    const newActiveQuestions = await Promise.all(bankQuestions.map(item => (async () => {
      const newActiveQuestion = await modules.models.question.create({
        lecture_item_id: newActiveLectureItemId,
        type: item.type,
        question: item.question,
        choice: item.choice,
        choice2: item.choice2,
        answer: item.answer,
        is_ordering_answer: item.is_ordering_answer,
        accept_language: item.accept_language,
        order: item.order,
        showing_order: item.showing_order,
        difficulty: item.difficulty,
        timer: item.timer,
        creator_id: item.creator_id,
        sqlite_file_guid: item.sqlite_file_guid,
        answer_media_type: item.answer_media_type,
        question_media_type: item.question_media_type,
        question_media: item.question_media,
      });
      const bankQuestionKeywords = item.bank_question_keywords;
      const newQuestionKeywords = await Promise.all(bankQuestionKeywords.map(key => (async () => {
        const newQuestionKeyword = await modules.models.question_keyword.create({
          lecture_item_id: newActiveLectureItemId,
          score_portion: key.score_portion,
          keyword: key.keyword,
          question_id: newActiveQuestion.question_id,
        });
        return newQuestionKeyword;
      })()));
      const ojProblem = item.bank_oj_problem;
      const problemTestCases = item.bank_problem_testcases;
      let newActiveOjProblem = null;
      if (ojProblem !== null) {
        const newActiveOjProblem = await modules.models.oj_problem.create({
          problem_id: newActiveQuestion.question_id,
          title: ojProblem.title,
          description: ojProblem.description,
          input: ojProblem.input,
          output: ojProblem.output,
          sample_input: ojProblem.sample_input,
          sample_output: ojProblem.sample_output,
          spj: ojProblem.spj,
          hint: ojProblem.hint,
          source: ojProblem.source,
          in_date: ojProblem.in_date,
          time_limit: ojProblem.time_limit,
          memory_limit: ojProblem.memory_limit,
          defunct: ojProblem.defunct,
          accepted: ojProblem.accepted,
          submit: ojProblem.submit,
          solved: 0, // 생각 해보자 여기 ... 0 으로 초기화 되는게 맞는거 같은데
        });
      }

      let newActiveProblemTestCases = [];
      if (problemTestCases.length > 0) {
        newActiveProblemTestCases = await Promise.all(problemTestCases.map(testcase => (async () => {
          const bankProblemTestCase = await modules.models.problem_testcase.create({
            question_id: newActiveQuestion.question_id,
            num: testcase.num,
            input: testcase.input,
            output: testcase.output,
            input_path: testcase.input_path,
            output_path: testcase.output_path,
          });
          return bankProblemTestCase;
        })()));
      }

      const files = item.bank_files;
      if ( files.length > 0 ) {
        files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'question_id', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
        })
      }

      const material_files = item.question_material;
      if ( material_files.length > 0) {
        material_files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'question_id_material', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
        })
      }
      

      return {
        newActiveQuestion,
        newActiveOjProblem,
        newActiveProblemTestCases
      };
    })()));

    const bankDiscussion = await modules.models.bank_discussion_info.findOne({
      where: { lecture_item_id: lcItem.lecture_item_id },
    });

    let newActiveDiscussion = null;
    if (bankDiscussion !== null) {
      newActiveDiscussion = await modules.models.discussion_info.create({
        lecture_item_id: newActiveLectureItemId,
        topic: bankDiscussion.topic,
        difficulty: bankDiscussion.difficulty,
        student_share: bankDiscussion.student_share,
      });
    }

    return {
      newActiveLectureItem,
      newActiveHomeworks,
      newActiveQuestions,
      newActiveDiscussion,
      newActiveNotes,
      newActiveSurveys,
      newActiveCodePractices,
    };
  })()));


  let list_dic = {};
  const lists = await modules.models.bank_lecture_item_list.findAll({ where: { lecture_id: lectureId }});
  for (let i = 0 ; i < lists.length; i +=1) {
    let list = lists[i].linked_list;
    let ids_ary = list.split(modules.config.separator);
    let newItemIds = [];
    for (let j = 0 ; j < ids_ary.length ; j += 1) {
      newItemIds.push(item_id_dic[ids_ary[j]]);
    }
    let joinedNewItemIds = newItemIds.join(modules.config.separator);
    let newList = await modules.models.lecture_item_list.create({ 
      lecture_id: newActiveLecture.lecture_id,
      item_id: newItemIds[0],
      linked_list: joinedNewItemIds,
    });
    list_dic[lists[i].lecture_item_list_id] = newList.lecture_item_list_id;
  }
  
  // group 생성
  const groups = await modules.models.bank_lecture_item_group.findAll({ where: { lecture_id: lectureId }});
  for (let i = 0 ; i < groups.length ; i += 1) {
    let group = groups[i].group_list;
    let lists_ary = group.split(modules.config.separator);
    let newListIds = [];
    for (let j = 0 ; j < lists_ary.length ; j += 1) {
      newListIds.push(list_dic[lists_ary[j]]);
    }
    let joinedNewListIds = newListIds.join(modules.config.separator);
    const newGroup = await modules.models.lecture_item_group.create({
      lecture_id: newActiveLecture.lecture_id,
      group_list: newListIds[0],
      opened: groups[i].opened,
      start: groups[i].start,
      end: groups[i].end,
    });
  };

  res.json({ newActiveLecture });
}));

// 아이템 가져오기
router.post('/to/lecture-item', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lecture_item_id, target_lecture_id, class_id } = req.body;

  const lectureItemId = lecture_item_id;
  const targetLectureId = target_lecture_id;
  const classId = class_id;

  const items = await modules.models.lecture_item.findAll({ where: { lecture_id: targetLectureId }});
  

  const bankLectureItem = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
    include: [{
      model: modules.models.bank_lecture_code_practice,
      include: { model: modules.models.bank_lecture_code_practice_keyword },
    }, {
      model: modules.models.bank_homework,
      include: [{
        model: modules.models.bank_file
      },{
        model: modules.models.bank_homework_keyword
      }],
    }, {
      model: modules.models.bank_question,
      include: [
        { model: modules.models.bank_problem_testcase}, 
        { model: modules.models.bank_oj_problem },
        { model: modules.models.bank_file}, 
        { model: modules.models.bank_file, as: 'question_material' },
        { model: modules.models.bank_question_keyword} 
      ],
    }, {
      model: modules.models.bank_survey,
      include: [{
        model: modules.models.bank_file
      },{
        model: modules.models.bank_survey_keyword
      }],
    }, {
      model: modules.models.bank_discussion_info,
    }, {
      model: modules.models.bank_note,
      include: [{
        model: modules.models.bank_file
      },{
        model: modules.models.bank_note_keyword
      }]
    }]
  });

  if (!bankLectureItem) {
    throw modules.errorBuilder.default('Lecture Item Not Found', 404, true);
  }
  
  const newActiveLectureItem = await modules.models.lecture_item.create({
    name: bankLectureItem.name,
    start_time: bankLectureItem.start_time,
    end_time: bankLectureItem.end_time,
    type: bankLectureItem.type,
    past_opend: 0,
    order: bankLectureItem.order,
    sequence: items.length+1,    // 기존에 있던 item len + 1
    result: bankLectureItem.result,
    offset: bankLectureItem.offset,
    opend: 0,
    scoring_finish: 0,
    lecture_id: targetLectureId,
  });


  const newActiveLectureItemId = newActiveLectureItem.lecture_item_id;

  const newList = await modules.models.lecture_item_list.create({
    lecture_id: newActiveLectureItem.lecture_id,
    item_id: newActiveLectureItemId,
    linked_list: newActiveLectureItemId,
  });
  const newGroup = await modules.models.lecture_item_group.create({
    lecture_id: newActiveLectureItem.lecture_id,
    group_list: newList.lecture_item_list_id,
    opened: 0,
    start: 0,
    end: 0
  });

  const bankHomeworks = bankLectureItem.bank_homeworks;
  const newActiveHomeworks = await Promise.all(bankHomeworks.map(item => (async () => {
    const newActiveHomework = await modules.models.homework.create({
      lecture_item_id: newActiveLectureItemId,
      comment: item.comment,
    });
    const bankHomeworkKeywords = item.bank_homework_keywords;
    const newHomeworkKeywords = await Promise.all(bankHomeworkKeywords.map(key => (async () => {
      const newHomeworkKeyword = await modules.models.homework_keyword.create({
        lecture_item_id: newActiveLectureItemId,
        score_portion: key.score_portion,
        keyword: key.keyword,
        homework_id: newActiveHomework.homework_id,
      });
      return newHomeworkKeyword;
    })()));
    const files = item.bank_files;
    const newActiveHomeworkFiles = await Promise.all(files.map(file => (async () => {
      const newFile = await modules.upload.copy(file.static_path, file.name, 'homework_id', newActiveHomework.homework_id, authId, newActiveLectureItemId, 1);
      return newFile;
    })()));
      return { newActiveHomework, newActiveHomeworkFiles };
  })()));

  const bankNotes = bankLectureItem.bank_notes;
  const newActiveNotes = await Promise.all(bankNotes.map(item => (async () => {
    const newActiveNote = await modules.models.note.create({
      lecture_item_id: newActiveLectureItemId,
      note_type: item.note_type,
      url: item.url,
      note_file: item.note_file,
      creator_id: item.creator_id,
      youtube_interval: item.youtube_interval,
    });
    const bankNoteKeywords = item.bank_note_keywords;
    const newNoteKeywords = await Promise.all(bankNoteKeywords.map(key => (async () => {
      const newNoteKeyword = await modules.models.note_keyword.create({
        lecture_item_id: newActiveLectureItemId,
        score_portion: key.score_portion,
        keyword: key.keyword,
        note_id: newActiveNote.note_id,
      });
      return newNoteKeyword;
    })()));
    const files = item.bank_files;
      if ( files.length > 0 ) {
        files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'note_id', newActiveNote.note_id, authId, newActiveLectureItemId, 1);
        })
      }
    return { newActiveNote };
  })()));

  const bankSurveys = bankLectureItem.bank_surveys;
  const newActiveSurveys = await Promise.all(bankSurveys.map(item => (async () => {
    const newActiveSurvey = await modules.models.survey.create({
      lecture_item_id: newActiveLectureItemId,
      type: item.type,
      comment: item.comment,
      choice: item.choice,
      creator_id: item.creator_id,
    });
    const bankSurveyKeywords = item.bank_survey_keywords;
    const newSurveyKeywords = await Promise.all(bankSurveyKeywords.map(key => (async () => {
      const newSurveyKeyword = await modules.models.survey_keyword.create({
        lecture_item_id: newActiveLectureItemId,
        score_portion: key.score_portion,
        keyword: key.keyword,
        survey_id: newActiveSurvey.survey_id,
      });
      return newSurveyKeyword;
    })()));

    const files = item.bank_files;
    const newActiveSurveyFiles = await Promise.all(files.map(file => (async () => {
    const newFile = await modules.upload.copy(file.static_path, file.name, 'survey_id', newActiveSurvey.survey_id, authId, newActiveLectureItemId, 1);
      return newFile;
    })()));
    return { newActiveSurvey, newActiveSurveyFiles };
  })()));

  const bankCodePractices = bankLectureItem.bank_lecture_code_practices; // 실습과 코딩 과제가 공유
  const newActiveCodePractices = await Promise.all(bankCodePractices.map(item => (async () => {
    const newActiveCodePractice = await modules.models.lecture_code_practice.create({
      lecture_item_id: newActiveLectureItemId,
      score: item.score,
      code: item.code,
      is_jupyter: item.is_jupyter,
      activated_time: item.activated_time,
      deactivated_time: item.deactivated_time,
      language_type: item.language_type,
      difficulty: item.difficulty,
      user_id: item.user_id,
    });
    const bankCodePracticeKeywords = item.bank_lecture_code_practice_keywords;
    const newCodePracticeKeywords = await Promise.all(bankCodePracticeKeywords.map(key => (async () => {
      const newCodePracticeKeyword = await modules.models.lecture_code_practice_keyword.create({
        lecture_item_id: newActiveLectureItemId,
        score_portion: key.score_portion,
        keyword: key.keyword,
        lecture_code_practice_id: newActiveCodePractice.lecture_code_practice_id,
      });
      return newCodePracticeKeyword;
    })()));
    return newActiveCodePractice;
  })()));

  const bankQuestions = bankLectureItem.bank_questions;
  const newActiveQuestions = await Promise.all(bankQuestions.map(item => (async () => {
    const newActiveQuestion = await modules.models.question.create({
      lecture_item_id: newActiveLectureItemId,
      type: item.type,
      question: item.question,
      choice: item.choice,
      choice2: item.choice2,
      answer: item.answer,
      is_ordering_answer: item.is_ordering_answer,
      accept_language: item.accept_language,
      order: item.order,
      showing_order: item.showing_order,
      difficulty: item.difficulty,
      timer: item.timer,
      creator_id: item.creator_id,
      sqlite_file_guid: item.sqlite_file_guid,
      answer_media_type: item.answer_media_type,
      question_media_type: item.question_media_type,
      question_media: item.question_media,
    });

    const bankQuestionKeywords = item.bank_question_keywords;
    const newQuestionKeywords = await Promise.all(bankQuestionKeywords.map(key => (async () => {
      const newQuestionKeyword = await modules.models.question_keyword.create({
        lecture_item_id: newActiveLectureItemId,
        score_portion: key.score_portion,
        keyword: key.keyword,
        question_id: newActiveQuestion.question_id,
      });
      return newQuestionKeyword;
    })()));

    const ojProblem = item.bank_oj_problem;
    const problemTestCases = item.bank_problem_testcases;
    let newActiveOjProblem = null;
    if (ojProblem !== null) {
      const bankOjProblem = await modules.models.oj_problem.create({
        problem_id: newActiveQuestion.question_id,
        title: ojProblem.title,
        description: ojProblem.description,
        input: ojProblem.input,
        output: ojProblem.output,
        sample_input: ojProblem.sample_input,
        sample_output: ojProblem.sample_output,
        spj: ojProblem.spj,
        hint: ojProblem.hint,
        source: ojProblem.source,
        in_date: ojProblem.in_date,
        time_limit: ojProblem.time_limit,
        memory_limit: ojProblem.memory_limit,
        defunct: ojProblem.defunct,
        accepted: ojProblem.accepted,
        submit: 0,
        solved: 0, // 생각 해보자 여기 ... 0 으로 초기화 되는게 맞는거 같은데
      });
    }

    let newActiveProblemTestCases = [];
    if (problemTestCases.length > 0) {
      newActiveProblemTestCases = await Promise.all(problemTestCases.map(testcase => (async () => {
        const bankProblemTestCase = await modules.models.problem_testcase.create({
          question_id: newActiveQuestion.question_id,
          num: testcase.num,
          input: testcase.input,
          output: testcase.output,
          input_path: testcase.input_path,
          output_path: testcase.output_path,
        });
        return newActiveProblemTestCases;
      })()));
    }
    const files = item.bank_files;
    if ( files.length > 0 ) {
      files.forEach(function(element){
        modules.upload.copy(element.static_path, element.name, 'question_id', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
      })
    }

    const material_files = item.question_material;
    if ( material_files.length > 0) {
      material_files.forEach(function(element){
        modules.upload.copy(element.static_path, element.name, 'question_id_material', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
      })
    }

    return {
      newActiveQuestion,
      newActiveOjProblem,
      newActiveProblemTestCases
    };
  })()));

  const bankDiscussion = bankLectureItem.bank_discussion_info;
  let newActiveDiscussion = null;
  if (bankDiscussion !== null) {
    newActiveDiscussion = await modules.models.discussion_info.create({
      lecture_item_id: newActiveLectureItemId,
      topic: bankDiscussion.topic,
      difficulty: bankDiscussion.difficulty,
      student_share: bankDiscussion.student_share,
    });
  }

  // 실습이나 코딩 과제 가져올 경우 주피터 docker 컨테이너 생성 필요
  if (bankLectureItem.type === 2 || bankLectureItem.type === 5) {
    const practice = await modules.models.lecture_code_practice.create({
      lecture_item_id: newActiveLectureItemId,
      user_id: authId,
    });
  
    const usersInClass = await modules.models.user_class.findAll({
      where: {
        class_id: classId,
      },
      include: {
        model: modules.models.user,
      },
    });
    // child_process.execSync("sudo docker exec class"+ classId + " su educator -c 'mkdir " + "/home/educator/practice_item_" + newActiveLectureItemId + "'");
    // usersInClass.map((user) => {
    //   // lectureItemId가 구별하고자하는 아이템ID가 맞는지 확인.
    //   // (user의 이메일ID)/practice_item_아이템ID 로 생성되게 구현했습니다.
    //   return child_process.execSync("sudo docker exec class" + classId+" su " + user.user.email_id + " -c 'mkdir " + "/home/" + user.user.email_id + "/practice_item_" + newActiveLectureItemId + "'");
    // });

     await execTimeout10("sudo docker exec class"+ classId + " su educator -c 'mkdir " + "/home/educator/practice_item_" + newActiveLectureItemId + "'");

    for (const user of usersInClass) {
      // lectureItemId가 구별하고자하는 아이템ID가 맞는지 확인.
      // (user의 이메일ID)/practice_item_아이템ID 로 생성되게 구현했습니다.
      await execTimeout10("sudo docker exec class" + classId +" su " + user.user.email_id + " -c 'mkdir " + "/home/" + user.user.email_id + "/practice_item_" + newActiveLectureItemId + "'");
    }
    
    const classname = `class${classId}`;

    const oriBankFilePath = path.join(modules.config.appRoot, `public/materials/bank_item_${lectureItemId}`);
    const newDockerFilePath = `${classname}:/home/educator/practice_item_${newActiveLectureItemId}`;

    // copy server to docker ipynb file
    /*
    const cmdFile = `sudo docker cp ${oriBankFilePath}/Untitled.ipynb ${newDockerFilePath}/Untitled.ipynb`;
    await execCmd(cmdFile, function (error, stdout, stderr) {
      console.log(stdout);
      console.log(stderr);
    }); */
    await execTimeout10("sudo docker cp " + oriBankFilePath + "/Untitled.ipynb " + newDockerFilePath + "/Untitled.ipynb");

    // file permission
    await execTimeout10("sudo docker exec " + classname + " bash -c 'chown educator.educator /home/educator/practice_item_" + newActiveLectureItemId+ "/Untitled.ipynb'");

  }

  res.json({ success: true, newActiveLectureItem });
}));

// 학생이 만든 문항 아이템 은행에 저장
router.post('/student-item', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lecture_item_id, group_id } = req.body;

  const lectureItemId = lecture_item_id;
  const groupId = group_id;

  const teacherCheck = await modules.teacherInGroup(authId, groupId);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const history = await modules.models.bank_history.create({
    creator_id: authId,
    type: 2,
  });

  const originStudentQuestion = await modules.models.student_question.findByPk(lectureItemId, {
    include: [{
      model: modules.models.student_question_keyword
    }, {
      model: modules.models.student_question_file
    }]
  });

  const newBankLectureItem = await modules.models.bank_lecture_item.create({
    name: originStudentQuestion.name,
    type: 0,
    past_opend: 0,
    order: 1,
    sequence: 1,
    result: 0,
    opend: 0,
    scoring_finish: 0,
    group_id: groupId,
    latest_store_teacher_id: authId,
    offset: 0,
    bank_history_id: history.bank_history_id,
  });
  const newBankLectureItemId = newBankLectureItem.lecture_item_id;

  const newBankQuestion = await modules.models.bank_question.create({
    lecture_item_id: newBankLectureItemId,
    type: originStudentQuestion.type,
    question: originStudentQuestion.question,
    choice: originStudentQuestion.choice,
    answer: originStudentQuestion.answer,
    is_ordering_answer: 0,
    difficulty: originStudentQuestion.difficulty,
    creator_id: originStudentQuestion.student_id,
    //sqlite_file_guid: item.sqlite_file_guid, // 나중에 복사한 guid 의 값을 넣어준다.
    bank_history_id: history.bank_history_id,
    choice2: originStudentQuestion.choice2,
    question_media: originStudentQuestion.question_media,
    question_media_type: originStudentQuestion.question_media_type,
    answer_media_type: originStudentQuestion.answer_media_type,
  });
  // create question_keyword
  // question keyword -> foreign key로 묶이지 않았기에 별도로 호출해서 넣어준다.
  const originQuestionKeywords = await modules.models.student_question_keyword.findAll({
    where: { student_question_id: lectureItemId  }
  });
  await Promise.all(originQuestionKeywords.map(keyword => (async () =>{
    await modules.models.bank_question_keyword.create({
      question_id: newBankQuestion.question_id,
      keyword: keyword.keyword,
      // lecture_id: newBankLecture.lecture_id,
      lecture_item_id: newBankLectureItemId,
      score_portion: keyword.score_portion,
      bank_history_id: history.bank_history_id,
    })
  })()));

  res.json({ success: true, newBankLectureItem });
}));

// 아이템 키워드 가져오기
router.get('/lecture-item-keyword', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
// router.get('/lecture-item/keyword', modules.asyncWrapper(async (req, res, next) => {

  const { authId, authType } = req.decoded;
  const { lecture_item_id } = req.query;
  const lectureItemId = lecture_item_id;

  // 유효성 검사
  if (!(lecture_item_id)) {
    throw modules.errorBuilder.default('lecture_item_id 는 반드시 입력해야 합니다. ex) IP/bank/lecture-item-keyword?lecture_item_id=1', 403, true);
  }
  
  // TODO: 권한 체크 세분화 - 이사람이 이 강의의 강사인가?

  // 타입이 뭔가?
  const res1 = await modules.models.bank_lecture_item.findByPk(lectureItemId, {
    attributes: ['type'],
  })
  const itemType = res1.type;
  
  // 타입에 따른 키워드 가져오기
  let keywords, res2;

  switch(itemType) {
    case 0: // 문항
      res2 = await modules.models.bank_question_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    case 1: // 설문
      res2 = await modules.models.bank_survey_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    case 2: // 실습
      res2 = await modules.models.bank_lecture_code_practice_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    case 3: // 토론
      res2 = [];
      break;
    case 4: // 자료
      res2 = await modules.models.bank_note_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    case 5: // 실습과제
      res2 = await modules.models.bank_lecture_code_practice_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    default:
      throw modules.errorBuilder.default('키워드를 구하는중에 문제가 발생하였습니다.', 404, true);
  }
  keywords = res2.map(element => element.keyword);

  res.json({ success: true, keywords });
}));
module.exports = router;
