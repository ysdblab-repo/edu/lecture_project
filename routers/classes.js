const express = require('express');
const router = express.Router();
const db = require('../modules/db');
const fs = require('fs');
const modules = require('../modules/frequent-modules');
const _ = require('underscore');
const sequelize = require('sequelize');
const { response } = require('express');
const { cat } = require('shelljs');
const { Op } = sequelize;
const crypto = require('crypto');

// 2021-03-09 exec 실행을 위한 변수 추가
const execTimeout10 = require('../src/utility');

// 2021-03-09 도커 생성, 실행 코드 복사 (from admin_class.js)
async function practiceAutomation({class_id, docker_port}) {
  const class_name = "class" + class_id;
  const docker_image_name = "junkyu/jupyterhub:latest";
  // 명령어 생성 명령어
  const docker_run_cmd = "docker run -itd --memory=\"4g\" -p " + docker_port + ":8000 --name " + class_name + " " +  docker_image_name + " jupyterhub -f jupyter_config.py";
  
  // 도커 생성 후 실행
  try{
   await execTimeout10(docker_run_cmd);
   await execTimeout10("docker exec " + class_name + " /bin/sh -c \"sed -i '5c\\        self.CLASS_ID = " + class_id + "' /root/preswot_server_info.py\""); 
   await execTimeout10("sudo docker exec -d " + class_name + " python3 /srv/CAPSTONE1_19_spring/module.py > /dev/null");
  }
  catch(e)
  {
   console.error(e);
  }
}

router.get('/', modules.asyncWrapper(async (req, res, next) => {
  // const { authType } = req.decoded;

  const openedClasses = await modules.models.class.findAll({
    where: {
      [Op.or]: {
        opened: [0, 1],
      },
    },
    include: {
      model: modules.models.user,
      as: 'master',
      attributes: modules.models.user.selects.userJoined,
      include: {
        model: modules.models.file,
        as: 'user_profile',
        attributes: modules.models.file.selects.fileClient,
      },
    },
  });
  const finishedClasses = await modules.models.class.findAll({
    where: { opened: 2 },
    include: {
      model: modules.models.user,
      as: 'master',
      attributes: modules.models.user.selects.userJoined,
      include: {
        model: modules.models.file,
        as: 'user_profile',
        attributes: modules.models.file.selects.fileClient,
      },
    },
  });

  res.json({ openedClasses, finishedClasses });
}));
router.get('/my', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  let teachingClasses = [];
  let studyingClasses = [];
  let previewClasses = [];

  if (authType === 1 || authType === 4) { // 강사 or 기관 관리자
    const a = await modules.models.user.find({
      include: {
        model: modules.models.class,
        through: { where: { role: 'teacher' } },
        include: [{
          model: modules.models.user,
          as: 'master',
          attributes: modules.models.user.selects.userJoined,
          include: {
            model: modules.models.file,
            as: 'user_profile',
            attributes: modules.models.file.selects.fileClient,
            required: false,
          },
          required: false,
        }, {
          model: modules.models.user,
          through: { where: { role: 'student' } },
        }, {
          model: modules.models.connect_session,
        }, {
          model: modules.models.req_create_class,
          include: {
            model: modules.models.affiliation_description,
          }
        }],
        where: { isHide: 0 },
        required: false,
      },
      order: [
        [modules.models.class, 'class_id', 'desc'],
      ],
      where: {
        user_id: authId
      }
    });
    teachingClasses = a.classes;
    // teachingClasses = (await modules.models.user.findByPk(authId, {
    //   include: {
    //     model: modules.models.class,
    //     through: { where: { role: 'teacher' } },
    //     include: [{
    //       model: modules.models.user,
    //       as: 'master',
    //       attributes: modules.models.user.selects.userJoined,
    //       include: {
    //         model: modules.models.file,
    //         as: 'user_profile',
    //         attributes: modules.models.file.selects.fileClient,
    //         required: false,
    //       },
    //       required: false,
    //     }, {
    //       model: modules.models.user,
    //       through: { where: { role: 'student' } },
    //     }, {
    //       model: modules.models.connect_session,
    //     }, {
    //       model: modules.models.req_create_class,
    //       include: {
    //         model: modules.models.affiliation_description,
    //       }
    //     }],
    //     where: { isHide: 0 },
    //     required: false,
    //   },
    //   order: [
    //     [modules.models.class, 'class_id', 'desc'],
    //   ],
    // })).classes;
  }
  if (authType === 0) { // 학생
    studyingClasses = (await modules.models.user.findByPk(authId, {
      include: {
        model: modules.models.class,
        through: { where: { role: 'student' } },
        include: [{
          model: modules.models.user,
          as: 'master',
          attributes: modules.models.user.selects.userJoined,
          include: {
            model: modules.models.file,
            as: 'user_profile',
            attributes: modules.models.file.selects.fileClient,
            required: false,
          },
          required: false,
        }, {
          model: modules.models.user,
          through: { where: { role: 'student' } },
        }, {
          model: modules.models.req_create_class,
          include: {
            model: modules.models.affiliation_description,
            where: {
              isOpen: 0
            }
          }
        }],
        where: {
          isHide: 0,
          type: {
            [Op.not]: 4,
          }
        },
        required: false,
      },
      order: [
        [modules.models.class, 'class_id', 'desc'],
      ],
    })).classes;

    previewClasses = (await modules.models.user.findByPk(authId, {
      include: {
        model: modules.models.class,
        through: { where: { role: 'student' } },
        include: [{
          model: modules.models.user,
          as: 'master',
          attributes: modules.models.user.selects.userJoined,
          include: {
            model: modules.models.file,
            as: 'user_profile',
            attributes: modules.models.file.selects.fileClient,
            required: false,
          },
          required: false,
        }, {
          model: modules.models.user,
          through: { where: { role: 'student' } },
        }, {
          model: modules.models.req_create_class,
          include: {
            model: modules.models.affiliation_description,
          }
        }],
        where: {
          isHide: 0,
          type: 4,
        },
        required: false,
      },
      order: [
        [modules.models.class, 'class_id', 'desc'],
      ],
    })).classes;
  }

  res.json({ teachingClasses, studyingClasses, previewClasses });
}));

router.get('/affiliation', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  // 요청자 id로 기관 id 확보
  const temp = await modules.models.user_belonging.findOne({
    attributes: ['affiliation_id'],
    where: {
      user_id: authId,
    },
  });

  // 기관 id로 기관에 속한 과목 id 확보
  const temp2 = await modules.models.req_create_class.findAll({
    attributes: ['class_id', 'user_id'],
    where: {
      affiliation_id: temp.affiliation_id,
    },
  });

  const temp3 = [];
  const temp4 = [];
  temp2.forEach((element) => {
    if (element.class_id !== null) {
      temp3.push(element.class_id);
      temp4.push(element.user_id);
    }
  });

  // 과목 id에 속하는 과목 검색
  const affiliateClasses = await modules.models.class.findAll({
    include: {
      attributes: ['name'],
      model: modules.models.user,
      where: {
        user_id: { [Op.in]: temp4 },
      },
    },
    where: {
      class_id: { [Op.in]: temp3 },
      isHide: 0
    },
    order: [
      ['class_id', 'desc'],
    ],
  });

  res.json({ affiliateClasses });
}));
/* 20200308 이전 코드
router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const {
    name,
    description,
    summary,
    opened,
    capacity,
    start_time,
    end_time,
    lecturer_description,
  } = req.body;

  const newClass = await modules.models.class.create({
    name,
    description,
    summary,
    opened,
    capacity,
    start_time,
    end_time,
    lecturer_description,
    master_id: authId,
  });

  //2021-03-05 Type이 3일 경우 practice를 1로하고 portNum을 7000 + classId로 설정
  if(type === 3)
  {
    await modules.models.class.update(
      {
        portNum: 7000 + newClass.class_id,
        practice: 1
      },
      {
        where: {
          class_id: newClass.class_id,
        }
      }
    );
    //2021-03-08 Docker 생성 및 실행
    await practiceAutomation({
      class_id: newClass.class_id,
      docker_port: Number(7000 + newClass.class_id),
    });
  }

  await modules.models.user_class.create({
    role: 'teacher',
    class_id: newClass.class_id,
    user_id: authId,
  });

  res.json({ success: true, class: newClass });
})); */

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const {
    name,
    summary,
    start_time,
    end_time,
    view_type,
    sign_up_start_time,
    sign_up_end_time,
    password,
    num,
    type,
    req_id,
    affiliationId,
    isOpenSpace,
  } = req.body;

  console.log("affiliationId: ", affiliationId);
  const newClass = await modules.models.class.create({
    name,
    summary,
    start_time,
    end_time,
    view_type,
    sign_up_start_time,
    sign_up_end_time,
    num,
    type,
    master_id: authId,
    isOpenSpace,
  });

  console.log("name: ", name);

  await modules.models.user_class.create({
    role: 'teacher',
    class_id: newClass.class_id,
    user_id: authId,
  });
  /* 20200507 안쓰는 부분 (class_password 관련)
  const test = await modules.models.class_password.findOne({ where: { password }});
  test.create_check = 1;
  test.class_id = newClass.class_id;
  await test.save();
  */

  //2021-03-05 Type이 3일 경우 practice를 1로하고 portNum을 7000 + classId로 설정
  if(type === 3)
  {
    await modules.models.class.update(
      {
        portNum: 7000 + newClass.class_id,
        practice: 1
      },
      {
        where: {
          class_id: newClass.class_id,
        }
      }
    );
    //2021-03-08 Docker 생성 및 실행
    await practiceAutomation({
      class_id: newClass.class_id,
      docker_port: Number(7000 + newClass.class_id),
    });
  }

  await modules.models.req_create_class.update(
    {
      status: 3,
      user_id: authId,
      class_id: newClass.class_id,
    },
    { where: { req_id } },
  );

  await modules.models.class_passwords_by_admin.update(
    {
      create_check: 1,
    },
    { where: { req_id } },
  );

  // TODO.2021-05-12 Follow 중인 학생에게 알람 Insert
  try{
  const sql = `SELECT A.user_id, B.description FROM affiliation_follows A, affiliation_descriptions B WHERE B.affiliation_id = ${affiliationId} GROUP BY user_id`;
  const result = await db.getQueryResult(sql);

  result.forEach(async (ret) => {
    console.log(ret.user_id);
    console.log(ret.description);
    console.log("강의명: ", name);
    
    //const msg = "개방 공간에 신규 강의가 개설되었습니다.";    
    const msg = "[" + name + "]이(가) 개설되었습니다.";

    await modules.models.event_notification.create({
      user_id: ret.user_id,
      isMaster: 0,
      affiliation_name: ret.description,
      class_name: "",
      lecture_name: "",
      event_type: 0,
      importance: 0,
      discription: msg,
      isRead: 0, 
    });
  });
  }catch(e){
    console.log(e);
  }
  res.json({ success: true, class: newClass });
}));

router.get('/:id/password', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const id = parseInt(req.params.id, 10);

  const class_password = await modules.models.class_password.findOne({
    where: {
      class_id: id,
    },
  });

  res.json({ data: class_password });
}));

router.post('/create/enroll/password', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const class_id = parseInt(req.body.classId, 10);
  const password = req.body.password;
  const teacherCheck = await modules.teacherInClass(authId, class_id);

  if (!(teacherCheck || authType === 3 || authType === 4)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // 비밀번호 찾음
  const exist = await modules.models.class_enroll_password.findOne({
    where: {
      class_id: class_id,
    }
  });

  let result;
  // 새로운 비밀번호 생성
  if (exist === null) {
    result = await modules.models.class_enroll_password.create({
      class_id: class_id,
      password: password,
    });
  } else { // 기존 비밀번호 업데이트
    result = await modules.models.class_enroll_password.update({
      password: password,
    }, {
      where: {
        class_id: class_id
      },
    });
  }

  // 새로운 비밀번호 생성

  res.json({ data: result });
}));
/*
// 자신의 강의 아이디를 통해 과목 소속 코드를 return
router.get('/belonging/id/:classId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const class_id = parseInt(req.params.classId, 10);
  const teacherCheck = await modules.teacherInClass(authId, class_id);

  if (!(teacherCheck || authType === 3)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const result = await modules.models.class_password.findOne({
    where: {
      class_id: class_id,
    },
    attributes: [],
    include: {
      model: modules.models.class_belonging,
      required: true,
      attributes: [['belonging_pwd', 'password']],
      include: {
        model: modules.models.affiliation_description,
        attributes: [['description', 'name']],
        required: true,
      },
    },
  });

  const data = { 
    name: result.dataValues.class_belonging.dataValues.affiliation_description.dataValues.name,
    code: result.dataValues.class_belonging.dataValues.password,
  };

  res.json({ data });
})); */

// 자신의 강의 아이디를 통해 과목 소속 코드를 return 20200507 version
router.get('/belonging/id/:classId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const class_id = parseInt(req.params.classId, 10);
  const teacherCheck = await modules.teacherInClass(authId, class_id);

  if (!(teacherCheck || authType === 3 || authType === 4)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const sql = `SELECT * FROM req_create_classes AS a INNER JOIN class_belongings AS b ON a.affiliation_id = b.affiliation_id WHERE a.class_id = ${class_id}`;
  const result = await db.getQueryResult(sql);

  // const result = await modules.models.req_create_class.findOne({
  //   where: {
  //     class_id: class_id,
  //   },
  //   include: {
  //     model: modules.models.class_belonging,
  //     required: true,
  //     attributes: [['belonging_pwd']],
  //   },
  // });

  res.json({ result });
}));

router.get('/belonging/:code', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const code = req.params.code;

  const class_belonging = await modules.models.class_belonging.findOne({
    where: {
      belonging_pwd: code,
    },
  });

  res.json({ result: class_belonging });
}));


// 과목 수강 신청 password에 해당하는 view_type(1 = 공개)인 sign_up_end_time이 지나지 않은 강의 가져오기
router.get('/belonging_pwd/:pwd', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const pwd = req.params.pwd;

  const classListData = await modules.models.class_belonging.findAll({
    where: { belonging_pwd: pwd },
    include: {
      model: modules.models.class_password,
    },
  });

  let classIdList = [];

  // 코드가 유효
  if (classListData[0] !== undefined) {
    classListData[0].class_passwords.forEach((class_password) => {
      classIdList.push(class_password.class_id);
    });
  }

  const tmpClassList = await modules.models.class.findAll({
    where: {
      class_id: {
        [Op.in]: classIdList,
      },
      sign_up_end_time: {
        [Op.gt]: Date.now()
      },
      view_type: 1,
    },
    include: {
      model: modules.models.user,
      as: 'master',
      attributes: modules.models.user.selects.userJoined,
      include: {
        model: modules.models.file,
        as: 'user_profile',
        attributes: modules.models.file.selects.fileClient,
      },
    },
  });

  // return 할 classList
  let classList = [];
  // promise all 용
  let promiseToDo = [];
  // db에서 가져온 clasList에 수강 신청 상태(신청중: 0, 수락: 1, 거절: 2, 없음: -1 로 등록하는중)
  tmpClassList.forEach(async (classData) => {
    // enrollment테이블에서 뒤지기
    promiseToDo.push(modules.models.enrollment.findOne({
      where: {
        class_id: classData.class_id,
        user_id: authId,
      },
    }));
    // 기본값을 -1로 설정
    classData.dataValues.enrollment = -1;
    classList.push(classData);
  });
  // 변환 테이블용
  let enrollStatusTable = {};
  // promise all로 한번에 promise 실행
  Promise.all(promiseToDo).then((result) => {
    // 결과들
    result.filter(item => item !== null).forEach((data) => {
      const class_id = data.class_id;
      const status = data.status;
      enrollStatusTable[class_id] = status;
    });
    classList.forEach((classData) => {
      classData.dataValues.enrollment = (enrollStatusTable[classData.class_id] === undefined ? -1 : enrollStatusTable[classData.class_id]);
    });
    res.json({ data: classList });
  });
}));

router.get('/creating_info', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;

  const acceptInfo = await modules.models.req_create_class.findAll({
    where: { user_id: authId },
    include: {
      model: modules.models.user,
    },
  });

  res.json({ acceptInfo });
}));

router.get('/lectureResult/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const classId = parseInt(req.params.id, 10);

  const lectures = await modules.models.lecture.findAll({
    where: {
      class_id: classId,
      result_opened: 1
    },
    include: [{
      model: modules.models.student_answer_log,
      where: {
        student_id: authId,
      },
      required: false
    }, {
      model: modules.models.lecture_item,
      where: {
        type: 0,
      },
      required: false,
    }, {
      model: modules.models.lecture_grade,
      where: {
        user_id: authId
      },
      required: false,
    }]
  });

  const lecture_grades = lectures.map(lecture => {
    const items = lecture.lecture_items.map(lecture_item => {
      const student_answer_log = lecture.student_answer_logs.find(student_answer_log => student_answer_log.item_id === lecture_item.lecture_item_id);
      if (student_answer_log === undefined) {
        return {
          name: lecture_item.name,
          sequence: lecture_item.sequence,
          type: lecture_item.type,
          lectureItemId: lecture_item.lecture_item_id,
          underStanding: null,
          reliability: null,
          absolute_reactivity: null,
          finalScore: null,
          answers: null,
        };
      } else {
        return {
          name: lecture_item.name,
          sequence: lecture_item.sequence,
          type: lecture_item.type,
          lectureItemId: lecture_item.lecture_item_id,
          underStanding: student_answer_log.score,
          reliability: modules.computeReliability(student_answer_log.count),
          absolute_reactivity: student_answer_log.absolute_reactivity,
          finalScore: student_answer_log.finalScore,
          answers: student_answer_log.answer ? student_answer_log.answer.split(modules.config.separator) : '',
        };
      }
    });
    if (lecture.lecture_grade !== null || lecture.grade_use === false) {
      return {
        lecture_name: lecture.name,
        lecture_id: lecture.lecture_id,
        score: lecture.lecture_grade && lecture.lecture_grade.score,
        percentage: lecture.lecture_grade && lecture.lecture_grade.percentage,
        grade: lecture.lecture_grade && lecture.lecture_grade.grade,
        items
      };
    }
  }).filter(lecture => lecture);

  res.json({ lecture_grades });
}));

// router.get('/classLectureResult/:id/:id2', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
//   const { authId } = req.decoded;
//   const classId = parseInt(req.params.id, 10);
//   const lectureId = parseInt(req.params.id2, 10);

//   const student_ids = (await modules.models.user_class.findAll({
//     where: {
//       class_id: classId,
//       role: 'student'
//     },
//   })).map(uc => uc.dataValues.user_id);

//   const lectures = await modules.models.lecture.findOne({
//     where: {
//       lecture_id: lectureId,
//     },
//     include: [{
//       model: modules.models.student_answer_log,
//       required: false,
//     }]
//   });

//   function percentRank(myScore, scores) {
//     let s = 0;
//     let b = 0;
//     scores.forEach(score => {
//       if (score <= myScore) {
//         s += 1;
//       } else {
//         b += 1;
//       }
//     });
//     return Math.round((s - 1) / (s + b) * 10000) / 100;
//   };

//   // function percentRank(myScore, scores) {
//   //   let s = 0;
//   //   let b = 0;
//   //   scores.forEach(score => {
//   //     if (score < myScore) {
//   //       s += 1;
//   //     } else {
//   //       b += 1;
//   //     }
//   //   });
//   //   return Math.round(s / (s + b) * 10000) / 100;
//   // };

//   function grade2(percentage) {
//     let grade;
//     if (percentage >= 90) {
//       grade = 'A+';
//     } else if (percentage >= 78.75) {
//       grade = 'A';
//     } else if (percentage >= 67.5) {
//       grade = 'A-';
//     } else if (percentage >= 56.25) {
//       grade = 'B+';
//     } else if (percentage >= 45) {
//       grade = 'B'
//     } else if (percentage >= 33.75) {
//       grade = 'B-'
//     } else if (percentage >= 22.5) {
//       grade = 'C+'
//     } else if (percentage >= 11.25) {
//       grade = 'C'
//     } else {
//       grade = 'C-'
//     }

//     return grade;
//   }

  // const student_ids = (await modules.models.user_class.findAll({
  //   where: {
  //     class_id: classId,
  //     role: 'student'
  //   },
  // })).map(uc => uc.dataValues.user_id);

router.get('/classLectureResult/:id/:id2', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const classId = parseInt(req.params.id, 10);
  const lectureId = parseInt(req.params.id2, 10);

  const student_ids = (await modules.models.user_class.findAll({
    where: {
      class_id: classId,
      role: 'student'
    },
  })).map(uc => uc.dataValues.user_id);

  // const student_ids = [
  //   1837,
  //   2066,
  //   3598,
  //   3599,
  //   3601,
  //   3608,
  //   3609,
  //   3614,
  //   3618,
  //   3620,
  //   3621,
  //   3622,
  //   3627,
  //   3628,
  //   3629,
  //   3631,
  //   3633,
  //   3634,
  //   3635,
  //   3637,
  //   3638,
  //   3640,
  //   3642,
  //   3644,
  //   3645,
  //   3648,
  //   3650,
  //   3653,
  //   3654,
  //   3655,
  //   3656,
  //   3657,
  //   3658,
  //   3661,
  //   3666,
  //   3673,
  //   3675,
  //   ];

  const lectures = await modules.models.lecture.findOne({
    where: {
      lecture_id: lectureId,
    },
    include: [{
      model: modules.models.student_answer_log,
      required: false,
    }]
  });


  // 중위값
  function percentRank(myScore, scores) {
    let s = 0;
    let b = 0;
    let t = 0;
    scores.forEach(score => {
      if  (score === myScore) {
        t += 1;
      } else if (score < myScore) {
        s += 1;
      } else {
        b += 1;
      }
    });
    return Math.round((s +((t - 1) / 2)) / (s + t + b) * 10000) / 100;
  };

  // 상위값
  // function percentRank(myScore, scores) {
  //   let s = 0;
  //   let b = 0;
  //   scores.forEach(score => {
  //     if (score <= myScore) {
  //       s += 1;
  //     } else {
  //       b += 1;
  //     }
  //   });
  //   return Math.round((s - 1) / (s + b) * 10000) / 100;
  // };

  function grade(percentage) {
    let grade;
    if (percentage >= 92) {
      grade = 'A+';
    } else if (percentage >= 83) {
      grade = 'A';
    } else if (percentage >= 74) {
      grade = 'A-';
    } else if (percentage >= 65) {
      grade = 'B+';
    } else if (percentage >= 56) {
      grade = 'B'
    } else if (percentage >= 47) {
      grade = 'B-'
    } else if (percentage >= 38) {
      grade = 'C+'
    } else if (percentage >= 29) {
      grade = 'C'
    } else if (percentage >= 20) {
      grade = 'C-'
    } else if (percentage >= 11) {
      grade = 'D'
    } else {
      grade = 'F'
    }

    return grade;
  }

    // function grade(percentage) {
    //   let grade;
    //   if (percentage >= 90) {
    //     grade = 'A+';
    //   } else if (percentage >= 78.75) {
    //     grade = 'A';
    //   } else if (percentage >= 67.5) {
    //     grade = 'A-';
    //   } else if (percentage >= 56.25) {
    //     grade = 'B+';
    //   } else if (percentage >= 45) {
    //     grade = 'B'
    //   } else if (percentage >= 33.75) {
    //     grade = 'B-'
    //   } else if (percentage >= 22.5) {
    //     grade = 'C+'
    //   } else if (percentage >= 11.25) {
    //     grade = 'C'
    //   } else {
    //     grade = 'C-'
    //   }

    //   return grade;
    // }

  const scores = student_ids.map(student_id => {
    const myScores = lectures.student_answer_logs.filter(student_answer_log => student_answer_log.lecture_id === lectureId && student_answer_log.student_id === student_id)
      .map(student_answer_log => student_answer_log.finalScore);
    
    let scoreSum = 0;
    if (myScores.length > 0) {
      scoreSum = myScores.reduce((ac, cu) => ac + cu);
    }
    return {
      student_id,
      score: Math.floor(scoreSum * 100) / 100
    };
  });

  scores.forEach(score => {
    score.percentage = percentRank(score.score, scores.map(s => s.score));
    score.grade = grade(score.percentage);
  });

  scores.sort((a, b) => {
    return a.percentage - b.percentage;
  });


  await Promise.all(scores.map(async score => {
    const lecture_grade = await modules.models.lecture_grade.findOne({
      where: {
        lecture_id: lectureId,
        user_id: score.student_id
      }
    });
    if (!lecture_grade) {
      return modules.models.lecture_grade.create({
        lecture_id: lectureId,
        user_id: score.student_id,
        score: score.score,
        percentage: score.percentage,
        grade: score.grade
      });
    } else {
      return lecture_grade.update({
        score: score.score,
        percentage: score.percentage,
        grade: score.grade
      });
    }
  }));


  res.json({
    hello: true,
  });
}));

router.get('/classLectureItemResult/:classId/:lectureId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const classId = parseInt(req.params.classId, 10);
  const lectureId = parseInt(req.params.lectureId, 10);

  const answerList = await modules.models.student_answer_log.findAll({
    where: { student_id: authId, class_id: classId, lecture_id: lectureId },
    include: [
      {
        model: modules.models.lecture,
        include: {
          model: modules.models.lecture_student_login_log,
          limit: 1,
          required: false,
          where: {
            student_id: authId,
          },
        },
      },
    ],
  });

  const resultOnLecture = await modules.models.lecture.findAll({
    where: { class_id: classId, result_opened: 1 },
  });

  res.json({ answerList, resultOnLecture });
}));

router.get('/enroll/password/:password', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const password = req.params.password;
  const { authId } = req.decoded;

  const class_enroll_password = await modules.models.class_enroll_password.findOne({
    where: {
      password: password,
    },
  });

  res.json({ class_enroll_password });
}));

router.get('/:id/only', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  theClass = await modules.models.class.findByPk(id);
  res.json(theClass);
}));

router.get('/:id/enroll/password', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, id);

  if (!(authType === 3 || authType === 4 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const password = await modules.models.class_enroll_password.findOne({
    where: {
      class_id: id,
    }
  });

  res.json({ data: password });
}));

router.post('/enroll/password', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const passwordToCheck = req.body.password;

  const password = await modules.models.class_enroll_password.findOne({
    where: {
      password: passwordToCheck,
    }
  });

  let class_id = null;
  //class_password에 있을때만
  if (password !== null) {
    const classData = await modules.models.class.findOne({
      where: {
        class_id: password.dataValues.class_id,
      },
    });
    // 비공개 강의 일때만 비밀번호 입력시 되야 하니까
    if (classData.view_type === 0) {
      class_id = password.class_id
    }
  }
  let resultType = -1;

  if (class_id !== null) {
    const exist = await modules.models.user_class.findAll({
      where: {
        role: 'student',
        class_id: class_id,
        user_id: authId,
      },
    });
    if (exist.length > 0) {
      resultType = 1;
    } else {
      await modules.models.user_class.create({
        role: 'student',
        class_id: class_id,
        user_id: authId,
      });

      await modules.models.enrollment.create({
        class_id,
        user_id: authId,
        status: 1,
      });

      // 비공개 강의 등록시 학생 소속 추가(공개 강의용) 용도
      const class_password = await modules.models.class_password.findOne({
        where: {
          class_id: class_id,
        },
      });
      const class_affiliation = await modules.models.req_create_class.findOne({
        where: {
          class_id: class_id,
        },
      });
      const class_belonging = await modules.models.class_belonging.findOne({
        where: {
          affiliation_id: class_affiliation.affiliation_id,
        },
      });
      const user_belonging_exist = await modules.models.user_belonging.findOne({
        where: {
          user_id: authId,
          affiliation_id: class_belonging.affiliation_id,
          class_belonging_id: class_belonging.class_belonging_id,
        },
      });
      // 비공개 강의에 해당하는 소속에 학생이 속해있지 않으면 추가함
      if (user_belonging_exist === null) {
        await modules.models.user_belonging.create({
          user_id: authId,
          affiliation_id: class_belonging.affiliation_id,
          class_belonging_id: class_belonging.class_belonging_id,
        });
      }
      resultType = 2;
    }
  } else {
    resultType = 0;
  }

  res.json({ result: resultType });
}));

router.put('/notice', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const { noticeId, name, content, fileGuid } = req.body;

  const notice = await modules.models.notice.findOne({
    where: {
      notice_id: noticeId,
    }
  });

  if (!(notice.user_id === authId || authType === 3)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const rollback = {
    name: notice.name,
    content: notice.content
  };

  notice.name = name;
  notice.content = content;

  let error;
  try {
    await notice.save();
  } catch (err) {
    error = err;
  }

  if (!error) {
    try {
      await modules.addFiles(fileGuid, 'notice_id', notice.notice_id);
    } catch (err) {
      error = err;
    }
  }
  if (error) {
    notice.name = rollback.name;
    notice.content = rollback.content;
    await notice.save();
  }
  
  res.json({
    success: !error,
    error
  });
}));

router.get('/:id/notice', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const id = Number(req.params.id);
  const pageNum = Number(req.query.pageNum);
  const {
    type,
    query
  } = req.query;

  const teacherCheck = await modules.teacherInClass(authId, id);
  const studentCheck = await modules.studentInClass(authId, id);

  if (!(authType === 3 || teacherCheck || studentCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const result = (await modules.models.notice.findAll({
    where: {
      class_id: id,
    },
    include: [{
      model: modules.models.notice_reply
    },{
      model: modules.models.user,
    }]
  })).filter(notice => {
    if (type === 'user') {
      return notice.user.name === query;
    } else if (type === 'name') {
      return notice.name.includes(query);
    } else {
      return true;
    }
  }).sort((a, b) => {
    return new Date(a.created_at) - new Date(b.created_at)
  }).map((data, index) => ({
    ...data.dataValues,
    num: index + 1,
  }));

  res.json({
    total: result.length,
    result: result.slice((pageNum - 1) * 10, pageNum * 10),
  });
}));

router.delete('/notice_reply/:notice_reply_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;

  const notice_reply_id = Number(req.params.notice_reply_id);

  const noticeReply = await modules.models.notice_reply.findOne({
    where: {
      notice_reply_id,
    }
  });

  if (!(noticeReply.user_id === authId || authType === 3)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  let error;

  try {
    await noticeReply.destroy();
  } catch (err) {
    error = err;
  }

  res.json({
    success: !error,
    error,
  });
}));

router.post('/notice_reply', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;

  const { classId, noticeId, comment } = req.body;
  const teacherCheck = await modules.teacherInClass(authId, classId);
  const studentCheck = await modules.studentInClass(authId, classId);

  if (!(authType === 3 || teacherCheck || studentCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.notice_reply.create({
    user_id: authId,
    notice_id: noticeId,
    content: comment,
    isTeacher: teacherCheck,
  });

  res.json({
    success: true
  });
}));

router.delete('/notice/:notice_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const notice_id = Number(req.params.notice_id);

  const notice = await modules.models.notice.findOne({
    where: {
      notice_id,
    }
  });

  if (!(notice.user_id === authId || authType === 3)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  let error;
  try {
    await notice.destroy();
  } catch (err) {
    error = err;
  }

  res.json({
    success: !error,
    error,
  })
}));

router.get('/:classId/notice/:noticeId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;

  const classId = Number(req.params.classId);
  const noticeId = Number(req.params.noticeId);
  const teacherCheck = await modules.teacherInClass(authId, classId);
  const studentCheck = await modules.studentInClass(authId, classId);

  if (!(authType === 3 || teacherCheck || studentCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const data = await modules.models.notice.findOne({
    where: {
      notice_id: noticeId
    },
    include: [
      { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
      { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
      {
        model: modules.models.notice_reply,
        include: {
          model: modules.models.user,
          attributes: modules.models.user.selects.userJoined,
        },
      },
      { model: modules.models.class },
    ],
  });

  data.num_views += 1;
  await data.save();

  res.json(data);
}));

router.post('/notice', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const { classId, name, content, fileGuid } = req.body;
  const teacherCheck = await modules.teacherInClass(authId, classId);

  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const notice = await modules.models.notice.create({
    name,
    content,
    class_id: classId,
    user_id: authId,
  });

  await modules.addFiles(fileGuid, 'notice_id', notice.notice_id);
  res.json({ success: true });
}));

router.get('/myNotices', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  
  const notices = await modules.models.notice.findAll({
    include: [{
      model: modules.models.class,
      include: [{
        model: modules.models.user_class,
        where: {
          user_id: authId,
        },
        required: true,
      }],
      required: true,
    }, {
      model: modules.models.user,
    }]
  });

  res.json({
    notices
  });
}));

router.get('/myBoards', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;

  const boards = await modules.models.board.findAll({
    include: [{
      model: modules.models.class,
      include: [{
        model: modules.models.user_class,
        where: {
          user_id: authId,
        },
        required: true,
      }],
      required: true,
    }, {
      model: modules.models.user,
    }]
  });

  res.json({
    boards
  });
}));

router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, id);

  let theClass;
  if (authType === 3 || teacherCheck) { // admin or the teacher
    theClass = await modules.models.class.findByPk(id, {
      include: [
        {
          model: modules.models.lecture,
          include: [
            { model: modules.models.lecture_keyword },
            {
              model: modules.models.lecture_item,
              include: [{
                model: modules.models.question,
                include: {
                  model: modules.models.question_keyword,
                },
              }, {
                model: modules.models.survey,
                include: {
                  model: modules.models.survey_keyword,
                },
              }, {
                model: modules.models.note,
                include: {
                  model: modules.models.note_keyword,
                },
              }, {
                model: modules.models.lecture_code_practice,
                include: {
                  model: modules.models.lecture_code_practice_keyword,
                },
              }],
            },
          ],
        },
        {
          model: modules.models.user,
          as: 'master',
          attributes: modules.models.user.selects.userJoined,
          include: {
            model: modules.models.file,
            as: 'user_profile',
            attributes: modules.models.file.selects.fileClient,
          },
        },
        {
          model: modules.models.class_password,
          attributes: modules.models.class_password.password,
        },
      ],
      order: [
        [modules.models.lecture, 'sequence', 'asc'],
      ],
    });
  } else {
    theClass = await modules.models.class.findByPk(id, {
      include: [
        {
          model: modules.models.lecture,
          include: [{
            model: modules.models.lecture_student_login_log,
            limit: 1,
            required: false,
            where: {
              student_id: authId,
            },
          }, {
              model: modules.models.connect_session,
              required: false,
              where: {
                class_id: id,
              },
              include: {
                model: modules.models.connect_session_permission,
              }
            },
          ]
        }, {
          model: modules.models.user,
          as: 'master',
          attributes: modules.models.user.selects.userJoined,
          include: {
            model: modules.models.file,
            as: 'user_profile',
            attributes: modules.models.file.selects.fileClient,
          },
        },
      ],
      order: [
        [modules.models.lecture, 'sequence', 'asc'],
      ],
    });
  }

  res.json(theClass);
}));
/* 20200309 이전 코드
router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  const teacherCheck = await modules.teacherInClass(authId, id);

  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const theClass = await modules.models.class.findByPk(id);
  for (let i = 0; i < keyLen; i += 1) {
    const key = keys[i];
    if (key === 'name' || key === 'description' || key === 'capacity' ||
      key === 'start_time' || key === 'end_time' || key === 'opened' ||
      key === 'summary' || key === 'lecturer_description') {
      theClass[key] = req.body[key];
    }
  }
  await theClass.save();

  res.json({ success: true });
}));*/

router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  const teacherCheck = await modules.teacherInClass(authId, id);
  const affiliationAdminCheck = await modules.affAdminInClass(authId, authType, id);

  if (!(authType === 3 || teacherCheck || affiliationAdminCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const theClass = await modules.models.class.findByPk(id);
  for (let i = 0; i < keyLen; i += 1) {
    const key = keys[i];
    if (key === 'name' || key === 'summary' || key === 'start_time' ||
      key === 'end_time' || key === 'view_type' || key === 'sign_up_start_time' ||
      key === 'sign_up_end_time' || key === 'num' || key === 'isOpenSpace') {
      theClass[key] = req.body[key];
    }
  }
  await theClass.save();

  res.json({ success: true });
}));

router.post('/:id/teacher/:email', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const { email } = req.params;
  const theUser = await modules.models.user.findOne({ where: { email_id: email } });
  if (!theUser) {
    throw modules.errorBuilder.default('User Not Found', 404, true);
  }

  const isExist = await modules.teacherInClass(theUser.user_id, id);
  if (isExist) {
    throw modules.errorBuilder.default('Already Exist', 409, true);
  }

  const theClass = await modules.models.class.findByPk(id);
  theClass.addUser(theUser.user_id, {
    through: {
      role: 'teacher',
    },
  });

  res.json({ success: true });
}));

router.delete('/:id/teacher/:email', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const { email } = req.params;
  const theUser = await modules.models.user.findOne({ where: { email_id: email } });
  if (!theUser) {
    throw modules.errorBuilder.default('User Not Found', 409, true);
  }

  await modules.models.user_class.destroy({
    where: {
      user_id: theUser.user_id,
      class_id: id,
    },
  });

  res.json({ success: true });
}));

router.delete('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.delete.class(id);
  res.json({ success: true });
}));

// start ----------------- board -----------------
router.post('/:id/boards', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const { name, content, fileGuid } = req.body;

  const board = await modules.models.board.create({
    name,
    content,
    class_id: id,
    writer_id: authId,
  });

  await modules.addFiles(fileGuid, 'board_id', board.board_id);
  res.json({ success: true, board_id: board.board_id });
}));

router.put('/:id/boards/:bid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const bid = parseInt(req.params.bid, 10);
  const { authId, authType } = req.decoded;

  const board = await modules.models.board.findByPk(bid);
  if (!board) {
    throw modules.errorBuilder.default('Board Not Found', 404, true);
  }
  const isWriter = board.writer_id === authId;

  const teacherCheck = await modules.teacherInClass(authId, id);
  if (!(authType === 3 || teacherCheck || isWriter)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  const { name, content } = req.body;

  board.name = name;
  board.content = content;

  await board.save();
  res.json({ success: true });
}));

router.post('/:id/boards/:bid/file', modules.auth.tokenCheck,
  modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
    const bid = parseInt(req.params.bid, 10);
    const board = await modules.models.board.findByPk(bid);
    if (!board) {
      throw modules.errorBuilder.default('Board Not Found', 404, true);
    }

    const file = await modules.upload.fileAdd(req, 'board_id', bid);
    res.json({ success: true, file });
  }));

router.delete('/:id/boards/:bid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const bid = parseInt(req.params.bid, 10);
  const { authId, authType } = req.decoded;

  const board = await modules.models.board.findByPk(bid);
  if (!board) {
    throw modules.errorBuilder.default('Board Not Found', 404, true);
  }
  const isWriter = board.writer_id === authId;

  const teacherCheck = await modules.teacherInClass(authId, id);
  if (!(authType === 3 || teacherCheck || isWriter)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.board.destroy({
    where: { board_id: bid },
  });

  res.json({ success: true });
}));

const PAGE_MAX = 10;

router.get('/:id/boards', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { type, query } = req.query;

  const nameWhere = {};
  const userWhere = {};

  // if (type === 'name') {
  //   const queryList = query.split(' ');
  //   console.log(queryList);
  //   nameWhere.name = {};
  //   nameWhere.name.$or = [];
  //   for (let i = 0; i < queryList.length; i += 1) {
  //     nameWhere.name.$or.push({ $like: `%${queryList[i]}%` });
  //   }
  // }
  if (type === 'name') {
    nameWhere.name = {};
    nameWhere.name.$like = `%${query}%`;
  }
  if (type === 'user') {
    userWhere.name = {};
    userWhere.name.$like = `%${query}%`;
  }
  nameWhere.class_id = id;

  const pageIndex = parseInt(req.query.page || '0', 10);

  const boards = await modules.models.board.findAll({
    offset: pageIndex * PAGE_MAX,
    limit: PAGE_MAX,
    order: [['created_at', 'DESC']],
    where: nameWhere,
    include: [
      { model: modules.models.user, attributes: modules.models.user.selects.userJoined, where: userWhere },
    ],
  });
  res.json(boards);
}));

router.get('/:id/NNboards', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { type, query } = req.query;

  const nameWhere = {};
  const userWhere = {};

  // if (type === 'name') {
  //   const queryList = query.split(' ');
  //   console.log(queryList);
  //   nameWhere.name = {};
  //   nameWhere.name.$or = [];
  //   for (let i = 0; i < queryList.length; i += 1) {
  //     nameWhere.name.$or.push({ $like: `%${queryList[i]}%` });
  //   }
  // }
  if (type === 'name') {
    nameWhere.name = {};
    nameWhere.name.$like = `%${query}%`;
  }
  if (type === 'user') {
    userWhere.name = {};
    userWhere.name.$like = `%${query}%`;
  }
  nameWhere.class_id = id;

  const pageIndex = parseInt(req.query.page || '0', 10);

  const result = await modules.models.board.findAndCountAll({
    offset: pageIndex * PAGE_MAX,
    limit: PAGE_MAX,
    order: [['created_at', 'DESC']],
    where: nameWhere,
    include: [
      { model: modules.models.user, attributes: modules.models.user.selects.userJoined, where: userWhere },
    ],
  });

  // TODO: 원인을 찾으려고 라이브러리를 까보았으나 결국 찾지 못하고 현실과 타협했습니다..
  // TODO: 혹시 누가 이 문제를 푸신다면, 이 코드를 지워주세요.
  result.rows.forEach((element) => {
    element.dataValues.created_at.setHours((element.dataValues.created_at.getHours()) + 18);
  });

  res.json({ result: result.rows, count: result.count });
}));

router.get('/:id/boards/total-number', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const id = parseInt(req.params.id, 10);
  const { type, query } = req.query;

  const nameWhere = {};
  const userWhere = {};
  if (type === 'name') {
    nameWhere.name = {};
    nameWhere.name.$like = `%${query}%`;
  }
  if (type === 'user') {
    userWhere.name = {};
    userWhere.name.$like = `%${query}%`;
  }
  nameWhere.class_id = id;

  const boards = await modules.models.board.findAll({
    where: nameWhere,
    include: [
      { model: modules.models.user, attributes: modules.models.user.selects.userJoined, where: userWhere },
    ],
  });

  res.json({ totalNumber: boards.length });
}));

router.get('/:id/boards/:bid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const bid = parseInt(req.params.bid, 10);

  const board = await modules.models.board.findByPk(bid, {
    include: [
      { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
      { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
      {
        model: modules.models.board_reply,
        include: {
          model: modules.models.user,
          attributes: modules.models.user.selects.userJoined,
        },
      },
    ],
  });
  if (!board) {
    throw modules.errorBuilder.default('NOT FOUND', 404, true);
  }
  board.num_views += 1;
  await board.save();
  res.json(board);
}));
// --------------------- board ----------------- end

router.get('/:id/coverage', modules.auth.class, modules.asyncWrapper(async (req, res, next) => {
  const classId = parseInt(req.params.id, 10);

  const avgCoverage = await modules.models.class_coverage.findByPk(classId);
  const keywordCoverages = await modules.models.class_keyword.findAll({
    where: { class_id: classId },
    attributes: modules.models.class_keyword.selects.classCoverage,
    raw: true,
  });

  const keywordQuestions = await modules.db.getQueryResult(`select qk.keyword as keyword, qk.question_id as question_id,
l.name as lecture_name, i.name as item_name, i.lecture_item_id as lecture_item_id, l.lecture_id as lecture_id
from question_keywords as qk join lectures as l 
on l.lecture_id=qk.lecture_id and l.class_id=${classId}
join questions as q on qk.question_id=q.question_id
join lecture_items as i on i.lecture_item_id=q.lecture_item_id
order by keyword`);
  const keywordMaterial = await modules.db.getQueryResult(`select mk.keyword as keyword, mk.material_id as material_id,
l.name as lecture_name, i.name as item_name, i.lecture_item_id as lecture_item_id, l.lecture_id as lecture_id
from material_keywords as mk join lectures as l 
on l.lecture_id=mk.lecture_id and l.class_id=${classId}
join materials as m on mk.material_id=m.material_id
join lecture_items as i on i.lecture_item_id=m.lecture_item_id
order by keyword`);

  for (let i = 0; i < keywordCoverages.length; i += 1) {
    keywordCoverages[i].questions = [];
    keywordCoverages[i].materials = [];
  }
  let pre = '^&%&@(*$^';
  let preIndex = 0;
  for (let i = 0; i < keywordQuestions.length; i += 1) {
    const cur = keywordQuestions[i].keyword;
    if (pre === cur) {
      keywordCoverages[preIndex].questions.push(keywordQuestions[i]);
    } else {
      for (let j = 0; j < keywordCoverages.length; j += 1) {
        if (keywordCoverages[j].keyword === keywordQuestions[i].keyword) {
          pre = cur;
          preIndex = j;
          keywordCoverages[preIndex].questions.push(keywordQuestions[i]);
        }
      }
    }
  }
  pre = '^&%&@(*$^';
  preIndex = 0;
  for (let i = 0; i < keywordMaterial.length; i += 1) {
    const cur = keywordMaterial[i].keyword;
    if (pre === cur) {
      keywordCoverages[preIndex].materials.push(keywordMaterial[i]);
    } else {
      for (let j = 0; j < keywordCoverages.length; j += 1) {
        if (keywordCoverages[j].keyword === keywordMaterial[i].keyword) {
          pre = cur;
          preIndex = j;
          keywordCoverages[preIndex].materials.push(keywordMaterial[i]);
        }
      }
    }
  }

  res.json({ avg_coverage: avgCoverage, keyword_coverages: keywordCoverages });
}));
router.get('/:id/total-result1', modules.auth.class, modules.asyncWrapper(async (req, res) => {

  const classId = parseInt(req.params.id, 10);

  const sql0 = `select lecture_id, name from lectures where class_id = ${classId}`;
  const sql1 = `select * from 
  (select l.lecture_id, li.lecture_item_id, l.name as l_name, li.name as li_name  from lectures l, lecture_items li where l.lecture_id = li.lecture_id  and l.class_id = ${classId} and li.\`type\` = 0) as res1
  left join
  (select lecture_id, item_id, q.score as q_score, avg(sal.score) as average, count(sal.score) as scored,count(ifnull(sal.score,1)) as unscored from student_answer_logs sal, questions q where sal.question_id = q.question_id and  sal.class_id = ${classId} group by sal.lecture_id, sal.item_id, q.score) as res2
  on res1.lecture_item_id = res2.item_id`
  const sql2 = `select * from 
  (select l.lecture_id, li.lecture_item_id, l.name as l_name, li.name as li_name  from lectures l, lecture_items li where l.lecture_id = li.lecture_id  and l.class_id = ${classId} and li.\`type\` = 1) as res1
  left join
   (select lecture_id, item_id, avg(ss.ratio) as average, count(ss.ratio) as cnt from student_surveys as ss where lecture_id in (select lecture_id from lectures where class_id = ${classId})
    group by lecture_id, item_id) as res2
  on res1.lecture_item_id = res2.item_id `
  const result0 = await db.getQueryResult(sql0);
  const result1 = await db.getQueryResult(sql1);
  const result2 = await db.getQueryResult(sql2);
  const result1_1 = _.groupBy(result1, 'lecture_id');
  const result2_2 = _.groupBy(result2, 'lecture_id');
  let result3 = []
  for (let i = 0; i < result0.length; i += 1) {
    result3.push(result0[i]);
    result3[i]['result'] = result1_1[result0[i].lecture_id];
    result3[i]['result2'] = result2_2[result0[i].lecture_id];
  }
  res.json(result3);

}))
router.get('/:id/total-result', modules.auth.class, modules.asyncWrapper(async (req, res) => {
  const classId = parseInt(req.params.id, 10);


  const cresult = await modules.models.class.findByPk(classId, {
    include: {
      model: modules.models.lecture,
      include: {
        model: modules.models.lecture_item,
        include: [{
          model: modules.models.survey,
          include: {
            model: modules.models.student_survey,
            include: {
              model: modules.models.user, attributes: modules.models.user.selects.userJoined,
            },
          },
        }, {
          model: modules.models.question,
          include: [
            { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
            { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
            { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
            {
              model: modules.models.student_answer_log,
              where: { valid: true },
              required: false,
              include: [
                { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
                {
                  model: modules.models.oj_solution,
                  required: false,
                  include: {
                    model: modules.models.oj_analysis,
                    required: false,
                  }
                },
                { model: modules.models.oj_compileinfo, required: false },
                { model: modules.models.oj_runtimeinfo, required: false },
                { model: modules.models.feedback, required: false },
                { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
              ],
            }, {
              model: modules.models.question_keyword,
            }],
        }],
      },
    },
  });
  if (!cresult) {
    res.json({ result: null });
    return;
  }

  const result = cresult.toJSON();
  for (let ll = 0; ll < result.lectures.length; ll += 1) {
    for (let kk = 0; kk < result.lectures[ll].lecture_items.length; kk += 1) {
      const lec = result.lectures[ll].lecture_items[kk];
      for (let i = 0; i < lec.questions.length; i += 1) {
        if (lec.questions[i].choice) {
          lec.questions[i].choice = lec.questions[i].choice.split(modules.config.separator);
        }
        if (lec.questions[i].answer) {
          lec.questions[i].answer = lec.questions[i].answer.split(modules.config.separator);
        }
        if (lec.questions[i].choice2) {
          lec.questions[i].choice2 = lec.questions[i].choice2.split(modules.config.separator);
        }
        if (lec.questions[i].accept_language) {
          lec.questions[i].accept_language = lec.questions[i].accept_language.split(modules.config.separator);
        }
        if (lec.questions[i].type === 3) {
          const problem = await modules.models.oj_problem.findOne({
            where: { problem_id: lec.questions[i].question_id }
          });
          if (problem) {
            lec.questions[i].input_description = problem.input;
            lec.questions[i].output_description = problem.output;
            lec.questions[i].sample_input = problem.sample_input;
            lec.questions[i].sample_output = problem.sample_output;
            lec.questions[i].time_limit = problem.time_limit;
            lec.questions[i].memory_limit = problem.memory_limit;
          }
        }
        for (let j = 0; j < lec.questions[i].student_answer_logs.length; j += 1) {
          if (lec.questions[i].student_answer_logs[j].answer) {
            lec.questions[i].student_answer_logs[j].answer =
              lec.questions[i].student_answer_logs[j].answer.split(modules.config.separator);
          } else {
            lec.questions[i].student_answer_logs[j].answer = [];
          }
        }
        if (!lec.questions[i].sqlite_file_guid) {
          lec.questions[i].sql_lite_file = [];
        } else {
          lec.questions[i].sql_lite_file = await modules.models.file.findAll({
            where: { file_guid: lec.questions[i].sqlite_file_guid }
          });
        }
      }
      for (let i = 0; i < lec.surveys.length; i += 1) {
        if (lec.surveys[i].choice) {
          lec.surveys[i].choice = lec.surveys[i].choice.split(modules.config.separator);
        }
        if (lec.surveys[i].student_surveys[0]) {
          lec.surveys[i].student_surveys[0].answer = lec.surveys[i].student_surveys[0].answer.split(modules.config.separator);
        }
      }
    }
  }

  res.json({ result });
}));

router.get('/:id/student-result', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId } = req.decoded;
  const classId = parseInt(req.params.id, 10);
  const cresult = await modules.models.class.findByPk(classId, {
    include: {
      model: modules.models.lecture,
      where: { result_opened: true },
      include: {
        model: modules.models.lecture_item,
        include: [{
          model: modules.models.survey,
          include: {
            model: modules.models.student_survey,
            required: false,
            where: { student_id: authId },
            include: {
              model: modules.models.user,
              attributes: modules.models.user.selects.userJoined,
            },
          },
        }, {
          model: modules.models.question,
          include: [
            { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
            { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
            {
              model: modules.models.student_answer_log,
              where: { valid: true, student_id: authId },
              required: false,
              include: [
                { model: modules.models.oj_solution, required: false },
                { model: modules.models.oj_compileinfo, required: false },
                { model: modules.models.oj_runtimeinfo, required: false },
                { model: modules.models.feedback, required: false },
                { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
              ],
            }, {
              model: modules.models.question_keyword,
            }],
        }],
      },
    },
  });
  if (!cresult) {
    res.json({ result: null });
    return;
  }

  const result = cresult.toJSON();
  for (let ll = 0; ll < result.lectures.length; ll += 1) {
    for (let kk = 0; kk < result.lectures[ll].lecture_items.length; kk += 1) {
      const lec = result.lectures[ll].lecture_items[kk];
      for (let i = 0; i < lec.questions.length; i += 1) {
        if (lec.questions[i].choice) {
          lec.questions[i].choice = lec.questions[i].choice.split(modules.config.separator);
        }
        if (lec.questions[i].answer) {
          lec.questions[i].answer = lec.questions[i].answer.split(modules.config.separator);
        }
        if (lec.questions[i].choice2) {
          lec.questions[i].choice2 = lec.questions[i].choice2.split(modules.config.separator);
        }
        if (lec.questions[i].accept_language) {
          lec.questions[i].accept_language = lec.questions[i].accept_language.split(modules.config.separator);
        }
        if (lec.questions[i].type === 3) {
          const problem = await modules.models.oj_problem.findOne({
            where: { problem_id: lec.questions[i].question_id }
          });
          if (problem) {
            lec.questions[i].input_description = problem.input;
            lec.questions[i].output_description = problem.output;
            lec.questions[i].sample_input = problem.sample_input;
            lec.questions[i].sample_output = problem.sample_output;
            lec.questions[i].time_limit = problem.time_limit;
            lec.questions[i].memory_limit = problem.memory_limit;
          }
        }
        for (let j = 0; j < lec.questions[i].student_answer_logs.length; j += 1) {
          if (lec.questions[i].student_answer_logs[j].answer) {
            lec.questions[i].student_answer_logs[j].answer =
              lec.questions[i].student_answer_logs[j].answer.split(modules.config.separator);
          } else {
            lec.questions[i].student_answer_logs[j].answer = [];
          }
        }
        if (!lec.questions[i].sqlite_file_guid) {
          lec.questions[i].sql_lite_file = [];
        } else {
          lec.questions[i].sql_lite_file = await modules.models.file.findAll({
            where: { file_guid: lec.questions[i].sqlite_file_guid }
          });
        }
      }
      for (let i = 0; i < lec.surveys.length; i += 1) {
        if (lec.surveys[i].choice) {
          lec.surveys[i].choice = lec.surveys[i].choice.split(modules.config.separator);
        }
        if (lec.surveys[i].student_surveys[0]) {
          lec.surveys[i].student_surveys[0].answer = lec.surveys[i].student_surveys[0].answer.split(modules.config.separator);
        }
      }
    }
  }

  res.json({ result });
}));

router.get('/:id/need-scoring', modules.auth.class, modules.asyncWrapper(async (req, res) => {
  const classId = parseInt(req.params.id, 10);

  const theClass = await modules.models.class.findByPk(classId, {
    include: {
      model: modules.models.lecture,
      include: {
        model: modules.models.lecture_item,
        include: [{
          model: modules.models.question,
          include: [{
            model: modules.models.student_answer_log,
            required: true,
            where: { valid: 1 },
            include: {
              model: modules.models.user, attributes: ['email_id'],
            },
          }, {
            model: modules.models.file,
            attributes: modules.models.file.selects.fileClient,
          }],
        }],
      },
    },
  });

  const items = [];
  for (let i = 0; i < theClass.lectures.length; i += 1) {
    for (let j = 0; j < theClass.lectures[i].lecture_items.length; j += 1) {
      if (theClass.lectures[i].lecture_items[j].questions[0]
        && theClass.lectures[i].lecture_items[j].questions[0].type === 2) {
        items.push({
          scoring_finish: theClass.lectures[i].lecture_items[j].scoring_finish,
          lecture_name: theClass.lectures[i].name,
          lecture_id: theClass.lectures[i].lecture_id,
          lecture_item_name: theClass.lectures[i].lecture_items[j].name,
          lecture_item_description: theClass.lectures[i].lecture_items[j].description,
          lecture_item_id: theClass.lectures[i].lecture_items[j].lecture_item_id,
          type: 'long',
          question: theClass.lectures[i].lecture_items[j].questions[0],
        });
      }
    }
  }
  res.json({ items });
}));

router.get('/:id/user', modules.auth.class, modules.asyncWrapper(async (req, res) => {
  const classId = parseInt(req.params.id, 10);
  const studentList = (await modules.models.class.findByPk(classId, {
    include: {
      model: modules.models.user,
      attributes: modules.models.user.selects.userJoined,
      through: { where: { role: 'student' } },
    },
  })).users;
  res.json(studentList);
}));

// 강사가 학생 등록 용도
router.post('/:id/user/teacher', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const classId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { userId } = req.body;
  // 권한 체크
  if (!(modules.teacherInClass(authId, classId) || authType === 3)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const exist = await modules.models.user_class.findOne({
    where: {
      class_id: classId,
      user_id: userId,
    },
  });

  if (exist) {
    res.json({ success: false });
  } else {
    await modules.models.user_class.create({
      class_id: classId,
      user_id: userId,
      role: 'student',
    });
  
    res.json({ success: true });
  }
}));

// 학생 스스로 등록 용도
router.post('/:id/user', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const classId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const exist = await modules.models.user_class.findOne({
    where: {
      class_id: classId,
      user_id: authId,
    },
  });
  if (exist) {
    throw modules.errorBuilder.default('Already Exist', 409, true);
  }

  await modules.models.user_class.create({
    class_id: classId,
    user_id: authId,
    role: 'student',
  });

  res.json({ success: true });
}));

// 2021-03-24 Docker 상태 변경
router.post('/docker/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {

  const classId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { dockerStatus } = req.body;

  // 1. 선생님 계정인지 토큰으로 체크
  if (!(modules.teacherInClass(authId, classId) || authType === 3)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  var dockerCmd;        // docker ON/OFF 명령어
  var updatePortNum;    // portNum Update 쿼리
  var flag = true;
  // 2-1. 수신된 dockerStatus가 true일 경우 ON
  if(dockerStatus)
  {
    dockerCmd = "docker start class" + classId;
    // 2021-04-13 docker STOP->RUN 전환 시 채점 module.py 실행
    moduleCmd = "sudo docker exec -d class" + classId + " python3 /srv/CAPSTONE1_19_spring/module.py > /dev/null";
    updatePortNum = "UPDATE classes SET portNum=7"+ classId +" WHERE class_id=" + classId;
    flag = true;
  }else{
    dockerCmd = "docker stop class" + classId;
    updatePortNum = "UPDATE classes SET portNum=1000 WHERE class_id=" + classId;
    flag = false;
  }

  await execTimeout10(dockerCmd);
  if (flag == true){
    await execTimeout10(moduleCmd);
  }

  //TODO. docker를 재기동 할 경우 Module.py를 실행시켜줘야한다.
  await db.getQueryResult(updatePortNum);
  res.json({ success: true });

}));

// 2021-03-31 강사 요청으로 인한 Docker 생성
router.post('/dockerCreate/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  // 0. permission check
  const teacherCheck = await modules.teacherInClass(authId, id);
  if (!(authType === 3 || teacherCheck || isWriter)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // 1. Docker Create
  await practiceAutomation({
    class_id: id,
    docker_port: Number(7000 + id),
  });

  // 2. UPDATE Database
  const sql = `UPDATE classes SET portNum=7${id} WHERE class_id = ${id}`;  
  try{
    await db.getQueryResult(sql);
  }catch(e)
  {
    console.error(e);
  }

  // TODO. ADD Student User Account to Docker

  res.json({ success: true });
}));

// 2021-03-31 강사 요청으로 인한 Docker 삭제
router.delete('/dockerDelete/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  // 0. permission check
  const teacherCheck = await modules.teacherInClass(authId, id);
  if (!(authType === 3 || teacherCheck || isWriter)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // 1. Docker stop  
  try{
    await execTimeout10("docker stop class" + id);
  }catch(e)
  {
    console.error(e);
  }
  
  // 2. Docker Remove  
  try{
    await execTimeout10("docker rm class" + id);
  }catch(e)
  {
    console.error(e);
  }
  
  // 3. DB UPDATE (portNum: 7000+classID -> 0)
  const sql = `UPDATE classes SET portNum=0 WHERE class_id = ${id}`;
  try{
    await db.getQueryResult(sql);
  }catch(e)
  {
    console.error(e);
  }
  
  res.json({ success: true });
}));

router.delete('/:id/user/:uid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const classId = parseInt(req.params.id, 10);
  const userId = parseInt(req.params.uid, 10);
  const { authId } = req.decoded;

  if (!(modules.teacherInClass(authId, classId) || authId === userId)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.user_class.destroy({
    where: {
      class_id: classId,
      user_id: userId,
      role: 'student',
    },
  });

  res.json({ success: true });
}));

router.get('/:id/keyword-relations', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const classId = parseInt(req.params.id, 10);

  const query = `SELECT r.node1, r.node2, SUM(r.weight) weight FROM lectures l, classes c, lecture_keyword_relations r 
  WHERE c.class_id=${classId} AND c.class_id=l.class_id AND l.lecture_id=r.lecture_id
  GROUP BY r.node1, r.node2`;

  const resultRelation = await db.getQueryResult(query);

  res.json(resultRelation);
}));


// router.put('/:id/state', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
//   const id = parseInt(req.params.id, 10);
//   const { authId, authType } = req.decoded;
//
//   const teacherCheck = await modules.teacherInClass(authId, id);
//
//   if (!(authType === 3 || teacherCheck)) {
//     throw modules.errorBuilder.default('Permission Denied', 403, true);
//   }
//
//   const { opened } = req.body;
//   const theClass = await modules.models.class.findByPk(id);
//   theClass.opened = opened;
//   await theClass.save();
//
//   res.json({ success: true });
// }));

router.put('/hide/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  
  const teacherCheck = await modules.teacherInClass(authId, id);
  const affiliationAdminCheck = await modules.affAdminInClass(authId, authType, id);
  if (!(authType === 3 || teacherCheck || affiliationAdminCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const theClass = await modules.models.class.update(
    { isHide: 1 },
    {
      where: {
        class_id: id,
      }
    }
  );

  // 21-02-18 과목 삭제 시, 해당 과목에 포함되어있는 파일 삭제 기능 추가  
  const query = `select files.static_path from files inner join boards on boards.class_id=${id} AND boards.board_id = files.board_id`;
  const file_paths = await db.getQueryResult(query);

  // 1. 파일 삭제
  for(let idx = 0; idx < file_paths.length; idx++)
    {
      if (file_paths[idx].static_path !== "")
      {
        fs.unlink(file_paths[idx].static_path, (err) => {
          try{
            if (err) throw err;
          }catch(err)
          {
            console.log(err);
          }        
        });        
      }
      else
      {
        console.log("No files Detected");
      }      
    }

  // 2. DB 삭제
  const del_query = `DELETE f FROM files AS f INNER JOIN boards ON boards.board_id = f.board_id WHERE boards.class_id=${id}`;
  const del_result = await db.getQueryResult(del_query);

  res.json({ success: true });
}));

router.get('/:id/rating', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const classId = parseInt(req.params.id, 10);
  const query = `SELECT ROUND(AVG(rating),1) FROM class_ratings WHERE class_id = ${classId}`;

  const rate = await db.getQueryResult(query);

  res.json({rate: rate[0]['ROUND(AVG(rating),1)']});
}));

router.post('/:class_id/:rating', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const classId = parseInt(req.params.class_id, 10);
  const rate = parseFloat(req.params.rating);

  await modules.updateOrCreate(
    modules.models.class_rating,
    {class_id: classId, user_id: authId},
    {class_id: classId, user_id: authId, rating: rate}
  );

    res.json({ success: true });
}));

router.get('/affiliation/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId = parseInt(req.params.id, 10);

  const result = await modules.models.req_create_class.findOne({
    include: {
      model: modules.models.affiliation_description
    },
    where: {
      class_id: classId,
    },
  });
  if (result !== null) {
    res.json({ success: true, result });
  } else {
    res.json({ success: false });
  }
}));

router.get('/institution/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const id = parseInt(req.params.id, 10);

  let openInstitutionClasses = [];

  if (authType === 0) { // 학생
    openInstitutionClasses = (await modules.models.req_create_class.findAll({
      include: [{
        model: modules.models.affiliation_description,
      }, {
        model: modules.models.class,
        include: {
          model: modules.models.user,
          attributes: modules.models.user.selects.userJoined,
          as: 'master',
        }
      }],
      where: {
        affiliation_id: id,
      }
    }));
  }
  res.json({ openInstitutionClasses });
}));

router.post('/binding', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const class_id = parseInt(req.body.classId, 10);

  // 바인딩 여부 파악
  const exist = await modules.models.user_class.findOne({
    where: {
      role: 'student',
      class_id,
      user_id: authId,
    }
  });

  if (exist === null) {
    result = await modules.models.user_class.create({
      role: 'student',
      class_id,
      user_id: authId,
    });
  }

  res.json({ success: true });
}));

router.get('/open/affiliation/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const id = req.params.id;

  const result = await modules.models.req_create_class.findAll({
    include: [{
      model: modules.models.affiliation_description,
      required: true,
    }, {
      model: modules.models.class,
      include: {
        model: modules.models.user,
        attributes: modules.models.user.selects.userJoined,
        as: 'master',
      },
      required: true,
      where : [{
        isOpenSpace: {
          [Op.like]: id + "%",
        },
        isHide: 0,
      }]
    }],
  });
  res.json({success: true, result});
}));


router.get('/open/affiliation/follow/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const userId = req.params.id;

  const result = await modules.models.affiliation_follow.findAll({
    where: {
      user_id: userId,
    }
  });
  console.log(result);

/*
  const sql = `SELECT * FROM affiliation_follows where user_id= ${userId}`;
  const result = await db.getQueryResult(sql);

  console.log(result);
*/

  res.json({success: true, result});
}));

// 2021-05-10 기관 Follow
router.post('/open/affiliation/follow', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  
  const affiliationId = parseInt(req.body.affiliationId, 10);
  const userId = parseInt(req.body.userId, 10);

  //TODO. 이미 Follow 중인지 Check 
  const exist = await modules.models.affiliation_follow.findOne({
    where: {
      affiliation_id: affiliationId,
      user_id: userId,
    }
  });
  
  if (exist === null){
    console.log("No Info");
    const ret = await modules.models.affiliation_follow.create({
      affiliation_id: affiliationId,
      user_id: userId,
    });
  }else{
  }

  res.json({ success: true });
}));


// 2021-05-10 기관 UnFollow
router.delete('/open/affiliation/follow/:id/:affiliationId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  const userId = req.params.id;
  const affiliationId = req.params.affiliationId;
 
  const ret = await modules.models.affiliation_follow.destroy({
    where: {
      affiliation_id: affiliationId,
      user_id: userId,
    },
  });
  
  res.json({ success: true });
}));

// 집중도 점수에 안들어감
router.put('/concentration/:lectureId/:itemId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res ,next) => {
  const lectureId = parseInt(req.params.lectureId, 10);
  const itemId = parseInt(req.params.itemId, 10);
  const { data } = req.body;

  for (let i = 0; i < data.length; i += 1) {
    await modules.models.student_answer_log.update({
      concentration: data[i].itemConSum * 0.01,
    }, {
      where: {
        student_id: data[i].user_id,
        lecture_id: lectureId,
        item_id: itemId,
      }
    });
  }

  // 집중도 안들어감
  // for (let i = 0; i < data.length; i += 1) {
  //   const sql = `UPDATE student_answer_logs SET finalScore=(student_answer_logs.score * student_answer_logs.concentration * if(student_answer_logs.count=1, 1, if(student_answer_logs.count=2, 1, if(student_answer_logs.count=3, 0.9, if(student_answer_logs.count=4, 0.8, 0.6))))) where student_id =${data[i].user_id} and lecture_id=${lectureId} and item_id=${itemId}`;        
  //   await db.getQueryResult(sql);
  // }

  res.json({ success: true, data });
}));

// 2021-08-07 강의 이름, 강사명 가져오기
router.get('/more-info/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const classId = parseInt(req.params.id, 10);
  
  const sql = `SELECT A.name AS teacher_name, B.name AS class_name FROM users A INNER JOIN classes B ON A.user_id = B.master_id AND B.class_id=${classId}`; 
  const result = await db.getQueryResult(sql);
  
  res.json({success: true, result});
}));


// 2021-08-07 강의 이름, 강사명 가져오기
router.get('/teacher-info/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const teacherId = parseInt(req.params.id, 10);
  
  const sql = `SELECT name from users where user_id=${teacherId}`;
  const result = await db.getQueryResult(sql);
  
  res.json({success: true, result});
}));
module.exports = router;
