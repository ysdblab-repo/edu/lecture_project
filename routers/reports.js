const express = require('express');
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
const _ = require('lodash');
const config = require('../config');
const router = express.Router();

router.post('/calcGrade', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { lectureId, target, option } = req.body;
  const { authId, authType } = req.decoded;

  const lecture = await modules.models.lecture.findByPk(lectureId);
  const teacherCheck = await modules.teacherInClass(authId, lecture.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const student_ids = (await Promise.all(target.map(tar => modules.models.user.findOne({
    where: {
      email_id: tar.email_id,
      type: 0,
    }
  })))).map(student => student.user_id);

  const lectures = await modules.models.lecture.findOne({
    where: {
      lecture_id: lectureId,
    },
    include: [{
      model: modules.models.student_answer_log,
      required: false,
    }]
  });

  const relativePercent = option.same_type === 'median' ? (myScore, scores) => {
    let s = 0;
    let b = 0;
    let t = 0;
    scores.forEach(score => {
      if  (score === myScore) {
        t += 1;
      } else if (score < myScore) {
        s += 1;
      } else {
        b += 1;
      }
    });
    return Math.round((s +((t - 1) / 2)) / (s + t + b) * 10000) / 100;
  } : (myScore, scores) => {
    let s = 0;
    let b = 0;
    scores.forEach(score => {
      if (score <= myScore) {
        s += 1;
      } else {
        b += 1;
      }
    });
    return Math.round((s - 1) / (s + b) * 10000) / 100;
  };

  const absolutePercent = (myScore, maximumScore) => myScore / maximumScore * 100;

  const grade = percentage => option.grades.find(el => el.range[0] <= percentage && el.range[1] > percentage).name;

  const scores = student_ids.map(student_id => {
    const myAnswerLogs = lectures.student_answer_logs.filter(student_answer_log => student_answer_log.lecture_id === lectureId && student_answer_log.student_id === student_id);
    const myScores = myAnswerLogs.map(student_answer_log => student_answer_log.finalScore);
    
    let scoreSum = 0;
    if (myScores.length > 0) {
      scoreSum = myScores.reduce((ac, cu) => ac + cu);
    }
    return {
      student_id,
      score: Math.floor(scoreSum * 100) / 100
    };
  });

  const maximumScore = (await modules.models.lecture_item.findAll({
    where: {
      lecture_id: lectureId,
      type: 0
    },
    include: {
      model: modules.models.question
    }
  })).map(lecture_item => lecture_item.questions[0].score).reduce((ac, cu) => ac + cu);

  scores.forEach(score => {
    score.percentage = option.type === 'relative' ? relativePercent(score.score, scores.map(s => s.score)) : absolutePercent(score.score, maximumScore);
    score.grade = grade(score.percentage);
  });

  scores.sort((a, b) => {
    return a.percentage - b.percentage;
  });

  await modules.models.lecture_grade.destroy({
    where: {
      lecture_id: lectureId
    }
  });

  await Promise.all(scores.map(async score => {
    return modules.models.lecture_grade.create({
      lecture_id: lectureId,
      user_id: score.student_id,
      score: score.score,
      percentage: score.percentage,
      grade: score.grade
    });
  }));

  res.json({
    success: true
  })
}));


router.post('/class-report/', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {
  const { classId, lectureId } = req.body;
  const { authId, authType } = req.decoded;

  // 유효성 검사 : TODO: class_id는 본인이 맞지만, itemId는 다른 사람의 아이템 아이디를 넣은 경우 체크
  if (classId === undefined) throw new Error('classId 은(는) 반드시 입력해야 합니다.');
  if (lectureId === undefined) throw new Error('lectureItemIdList 은(는) 반드시 입력해야 합니다.');
  if (!Number.isInteger(lectureId)) {
    throw new Error('lectureItemIdList 은(는) 정수의 배열로 주어져야 합니다.');
  }

  // 권한 검사
  const check = await modules.models.user_class.findOne({ where: { role: 'teacher', class_id: classId, user_id: authId } });
  if (!check) {
    throw modules.errorBuilder.default('권한이 없는 class_id 에 대해 요청 하였습니다.', 403, true);
  }
  
  // 과목을 수강중인 학생 명단 - user_id로 정렬됨
  const class_student_list = await modules.models.user_class.findAll({
    attributes: [],
    where: {
      role: 'student',
      class_id: classId,
    },
    include: {
      attributes: ['email_id', 'name', 'user_id'],
      model: modules.models.user,
    },
  });

  const lectureItemIdList = (await modules.models.lecture_item.findAll({
    where: {
      lecture_id: lectureId,
      type: 0,
    }
  })).map(data => data.lecture_item_id);

  const grades = (await modules.models.lecture_grade.findAll({
    where: {
      lecture_id: lectureId
    }
  })).map(data => ({
    user_id: data.user_id,
    percentage: data.percentage,
    grade: data.grade
  }));

  // 결과 출력 준비
  const result = [];
  for (let i = 0; i < class_student_list.length; i += 1) {
    const grade = grades.find(grade => grade.user_id === class_student_list[i].user.user_id);
    const obj = {
      // user_id: class_student_list[i].user.user_id, // 추후 매핑에 user_id가 필요하다고 판단된다면, 주석 해제하고 사용할 것 (현재는 순서에 따라)
      email_id: class_student_list[i].user.email_id,
      name: class_student_list[i].user.name,
      percentage: grade ? grade.percentage : undefined,
      grade: grade ? grade.grade : undefined,
    };
    for (let j = 0; j < lectureItemIdList.length; j += 1) {
      obj[lectureItemIdList[j]] = null;
    }
    result.push(obj);
  }

  const itemInfo = [];

  // 점수 추출
  for (let i = 0; i < lectureItemIdList.length; i += 1) {
    // 아이템 아이디, 이름, 타입을 확인한다.
    const lecture_item = await modules.models.lecture_item.findOne({
      attributes: ['lecture_item_id','name','lecture_id', 'type'],
      where: {
        lecture_item_id: lectureItemIdList[i],
      },
    });

    // 아이템 상세 정보는 따로 보내기 위해 저장한다.
    itemInfo.push({
      lecture_item_id: lecture_item.lecture_item_id,
      name: lecture_item.name,
      lecture_id: lecture_item.lecture_id,
    });

    if( lecture_item.type === 0) { // 문항인 경우,
      // question_id 를 찾는다.
      const question = await modules.models.question.findOne({
        attributes: ['question_id'],
        where: {
          lecture_item_id: lectureItemIdList[i],
        },
      });
      for (let j = 0; j < class_student_list.length; j += 1) {
        // 학생 제출 기록을 찾는다.
        const student_answer_log = await modules.models.student_answer_log.findOne({
          attributes: ['score'],
          where: {
            question_id: question.question_id,
            student_id: class_student_list[j].user.user_id,
          },
        });
        // 배열에 넣는다.
        if (student_answer_log) {
          result[j][lectureItemIdList[i]] = student_answer_log.score;
        }
      }
    }
  }

  res.json({ result, itemInfo });
}));

router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const classId = parseInt(req.params.id, 10);
  const lectures = await modules.models.lecture.findAll({ where : { class_id: classId }});
  res.json(lectures);  

}));


// 현재 강의의 문항을 return
router.get('/lectures/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  const lectureItems = await modules.models.lecture_item.findAll({
    where: { lecture_id: lectureId },
    attributes: ['lecture_item_id', 'name'],
    include: [{ 
      model: modules.models.question,
      attributes: ['question_id','type','question', 'choice', 'answer','score'],
      include: [
        { 
          model: modules.models.question_keyword,
          attributes: ['keyword','score_portion'],
          required: true,
        },
        {
          model: modules.models.file,
        }
      ],
    },
    {
      model: modules.models.survey,
      attributes: ['survey_id','type','comment','choice','score'],
      include: [
        {
          model: modules.models.survey_keyword,
          attributes: ['keyword','score_portion'],
          required: true,
        }
      ]
    }],
  })

  let questionResult = [];
  let surveyResult = [];
  for (let i = 0 ; i < lectureItems.length ; i += 1) {
    let question = lectureItems[i].questions[0];
    let survey = lectureItems[i].surveys[0];
    if(survey) {
      let obj = { 'item_id': lectureItems[i].lecture_item_id, 'name': lectureItems[i].name, 'survey':survey.comment, 'type': survey.type,
    'choice': survey.comment, 'score': survey.score, 'survey_keywords':survey.survey_keywords };
      surveyResult.push(obj);
    }
    if(question) {
      if (question.type === 0) {
        // question.choice = question.choice.join(config.separator);
        // question.answer = question.answer.join(config.separator);
      }
      let obj = { 'item_id': lectureItems[i].lecture_item_id, 'name': lectureItems[i].name , 'question': question.question, 'type': question.type,
        'choice': question.choice, 'answer': question.answer, 'score': question.score, 'question_keywords': question.question_keywords, 'file': question.files };
      questionResult.push(obj);
    }
  }

  // console.log(JSON.stringify(questionResult));
  // console.log(JSON.stringify(surveyResult));
  res.json({questionResult, surveyResult});  

}));
router.get('/class-result/:id/type/:type', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const classId = parseInt(req.params.id, 10);
  const type = parseInt(req.params.type, 10);

  console.log(`type - - ${type}`);
  const sql0 = `SELECT l.name, l.lecture_id, rl.activated, rl.id FROM report_lectures rl, lectures l 
  WHERE rl.lecture_id = l.lecture_id AND rl.class_id = ${classId} AND activated = TRUE ORDER BY lecture_id DESC`; // lecture name 이랑 조인해서 
  const lectureResult = await db.getQueryResult(sql0);
  let f_ids = [];
  let f_ids_str = ''
  for (let i = 0 ; i < lectureResult.length ; i += 1) {
    if(lectureResult[i].activated) {
      f_ids.push(parseInt(lectureResult[i].id, 10));
      f_ids_str += `${lectureResult[i].id},`;
    }
  }
  f_ids_str = f_ids_str.substring(0,f_ids_str.length - 1);
  const sql1 = `SELECT f_id,student_id,lecture_id,score FROM report_students WHERE f_id in (${f_ids_str})`; // name 이랑 조인해서 결과 return
  const studentResult = await db.getQueryResult(sql1);
  let result = {};
  const sql2 = `SELECT SUM(score) AS sum_score,lecture_id FROM report_items WHERE \`type\` = ${type} AND f_id in (${f_ids_str}) GROUP BY lecture_id ORDER BY lecture_id DESC`;
  const itemResult = await db.getQueryResult(sql2);
  let lectureResult0 = [];
  for (let i = 0; i < itemResult.length; i += 1) { lectureResult0.push({ 'lecture_id': lectureResult[i].lecture_id, 'name': lectureResult[i].name, 'score': itemResult[i].sum_score})}
  // for (let i = 0; i < itemResult.length; i += 1) { result[itemResult[i].lecture_id] = { 'name': lectureResult[i].name, 'score':itemResult[i].sum_score, 'students': [] }; }
 
  const sql3 = `SELECT u.user_id, u.name FROM user_classes uc, users u WHERE u.user_id = uc.user_id AND uc.class_id = ${classId} AND uc.role = 'student'`
  const userResult = await db.getQueryResult(sql3);
  // for (let i = 0; i < studentResult.length; i +=1) {
    // result[studentResult[i].lecture_id]['students'].push(studentResult[i]);
  // }
  res.json({ lectureResult: lectureResult0, studentResult, userResult});
  // res.json(result);

}));
router.get('/check-lecture/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  const check = await modules.models.report_lecture.findOne({ where: { lecture_id: lectureId, activated: true }});
  // if lecture item is exist
  if (check) {
    res.json({ check: true });
  } else {
    res.json({ check: false });
  }

}));

// post body
router.post('/lectures/:id', modules.auth.tokenCheck, modules.asyncWrapper(async(req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  await modules.models.report_lecture.update({ activated: false, }, { where: { lecture_id: lectureId }}); // 기존의 데이터들을 삭제
  const lecture = await modules.models.lecture.findOne({ where: { lecture_id: lectureId }});
  const score_portions = req.body.result;
  const type = req.body.type;
  let f_id = await modules.models.report_lecture.create({ lecture_id: lectureId, class_id: lecture.class_id , activated: true });

  ///////////////////////// item score 저장 ////////////////////////////////
  let report_items = [];
  let diff_items = []; // 기존의 점수와 다른 아이템들
  let diff_item_scores = [];
  for (let i = 0; i < score_portions.length ; i += 1) {
    if (score_portions[i].changed) {
      diff_items.push(parseInt(score_portions[i].item_id, 10));
      diff_item_scores.push(parseInt(score_portions[i].score, 10));
    }
    let obj = { 'item_id': score_portions[i].item_id, 'score': score_portions[i].score, 'lecture_id': lectureId, 'f_id': f_id.id , 'type': type };
    report_items.push(obj);
  }
  modules.models.report_item.bulkCreate(report_items);

  let item_ids_str = '';
  for (let i = 0 ; i < score_portions.length ; i += 1) {
    item_ids_str += `${score_portions[i].item_id},`;
  }
  item_ids_str = item_ids_str.substring(0,item_ids_str.length - 1);
  
  let studentSaveResult = []
  if (type === 0) // 문항일 경우
  {
    const sql1 = `SELECT student_id, score, ratio, item_id, lecture_id FROM student_answer_logs WHERE lecture_id = ${lectureId} ORDER BY item_id`;
    const studentResult = await db.getQueryResult(sql1);
  
  
    for (let i = 0 ; i < studentResult.length ; i += 1) {
      let idx = diff_items.indexOf(parseInt(studentResult[i].item_id,10));
      if (idx !== -1) {
        let score = diff_item_scores[idx] * parseFloat(studentResult[i].ratio);
        let obj = { 'f_id': f_id.id, 'lecture_id': lectureId, 'item_id': studentResult[i].item_id,  'score': score, 'student_id':studentResult[i].student_id,'type': type };
        studentSaveResult.push(obj);
      } else {
        let itemId = diff_items[idx];
        let obj = { 'f_id': f_id.id, 'lecture_id': lectureId, 'item_id': studentResult[i].item_id, 'score': studentResult[i].score , 'student_id':studentResult[i].student_id, 'type':type };
        studentSaveResult.push(obj);
      }
    }
    await modules.models.report_student.bulkCreate(studentSaveResult);
    res.json(studentSaveResult);
    return;
  
  } else { // 설문
  
    const sql1 = `SELECT * FROM student_surveys WHERE lecture_id = ${lectureId} AND \`type\`=0 ORDER BY item_id`
    const studentResult = await db.getQueryResult(sql1);
    for (let i = 0 ; i < studentResult.length ; i += 1) {

      let idx = diff_items.indexOf(parseInt(studentResult[i].item_id,10));
      if (idx !== -1) {
        let score = diff_item_scores[idx] * parseFloat(studentResult[i].ratio);
        let obj = { 'f_id': f_id.id, 'lecture_id': lectureId, 'item_id': studentResult[i].item_id, 'score': score, 'student_id':studentResult[i].student_id, 'type':type };
        studentSaveResult.push(obj);
      } else {
        let itemId = diff_items[idx];
        let obj = { 'f_id': f_id.id, 'lecture_id': lectureId, 'item_id': studentResult[i].item_id, 'score': studentResult[i].score , 'student_id':studentResult[i].student_id, 'type':type };
        studentSaveResult.push(obj);
      }
    }
    await modules.models.report_student.bulkCreate(studentSaveResult);
    res.json(studentSaveResult);
    return;

  }
  
}));
module.exports = router;