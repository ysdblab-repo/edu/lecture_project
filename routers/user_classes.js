const express = require('express');
const router = express.Router();
const sequelize = require('sequelize')
const modules = require('../modules/frequent-modules');
const db = require('../modules/db')
router.get('/:log_verify_code', modules.asyncWrapper(async (req, res, next) => {
  const log_verify_code = req.params.log_verify_code;

  const realtime_lecture_log = await db.getQueryResult(`select rll.* from users u \
  join realtime_lecture_heartbeats rll on rll.user_id = u.user_id \
  where u.log_verify_code = '${log_verify_code}' \
  order by rll.created_at desc \
  LIMIT 1`); 
  
 class_data = await modules.models.user.find({
    where: {log_verify_code: log_verify_code},
    include: [{model: modules.models.class, required: true,
      include : {model: modules.models.lecture, required: true,
        include : {model: modules.models.lecture_accept_plist, required:false}
      }
    },
	],
  });
  if(class_data==null)
  {
	  res.send({'empty': true});
	  return;
  }
  class_data = JSON.parse(JSON.stringify(class_data))
  var ret_json = {}
  var ret_array = []
  var plist = await modules.models.plist.findAll();
  plist = JSON.parse(JSON.stringify(plist));
  
  for (var i=0;i<class_data.classes.length;i++)
  {
    var classes = class_data.classes[i];
    for (var j=0;j<classes.lectures.length;j++)
    {
      var lectures = classes.lectures[j]
      for (var k=0;k<lectures.lecture_accept_plists.length;k++)
      {
        var ac = lectures.lecture_accept_plists[k]
        for (var z=0;z<plist.length;z++)
        {
          if(plist[z].plist_id == ac.plist_id)
          {
            lectures.lecture_accept_plists[k].name = plist[z].name;
            break;
          }
        }

      }
      ret_array.push(lectures)
    }

  }
  
  try {
	ret_json['last_lecture'] = realtime_lecture_log[0].lecture_id;
	ret_json['last_lecture_time'] = realtime_lecture_log[0].created_at;
  }
  catch(err){
	  ret_json['last_lecture'] = null;
	  ret_json['last_lecture_time'] = null;
  }
  ret_json['user_id'] = class_data.user_id;
  ret_json['lectures'] = ret_array;
  res.json(JSON.parse(JSON.stringify(ret_json)));

}));

router.delete('/classId/:classId/userId/:userId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const classId = parseInt(req.params.classId, 10);
  const userId = parseInt(req.params.userId, 10);
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, classId); // 강사가 class
  const affiliationAdminCheck = await modules.affAdminInClass(authId, authType, classId);

  if (!(authType === 3 || teacherCheck || affiliationAdminCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const user_class = await modules.models.user_class.findOne({
    where: {
      class_id: classId,
      user_id: userId,
      role: 'student',
    },
  });

  if (user_class === null) {
    res.json({ success: false });
  } else {
    await user_class.destroy();
    res.json({ success: true });
  }
}));

router.get('/classId/:classId/student', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const classId = parseInt(req.params.classId, 10);
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, classId); // 강사가 class

  if (!(authType === 3 || authType === 4 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const user_classes = await modules.models.user_class.findAll({
    where: {
      class_id: classId,
      role: 'student',
    },
    include: [{
      model: modules.models.user,
      include: [{
        model: modules.models.enrollment,
        where: {
          class_id: classId,
        },
        required: false,
      }],
      required: false,
    }]
  });

  res.json({ user_classes });
}));

module.exports = router;