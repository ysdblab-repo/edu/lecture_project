const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
var multer  = require('multer');

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const  lectureItemId  = req.body.lecture_item_id;
  const  content = req.body.content;

  let is_teacher = 0;
  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if ((authType === 3 || teacherCkeck)) {
	is_teacher = 1;
  }
  

  const discussion = await modules.models.discussion.create({
    lecture_item_id: lectureItemId,
    user_id: authId,
    is_teacher : is_teacher,
    content : content,
  });

  res.json({ success: true, discussion_id: discussion.discussion_id });
}));

router.post('/file/:id', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lectureItemId  = parseInt(req.params.id, 10);
  
  let is_teacher = 0;
  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if ((authType === 3 || teacherCkeck)) {
	is_teacher = 1;
  }

  const discussion = await modules.models.discussion.create({
    lecture_item_id: lectureItemId,
    user_id: authId,
	is_teacher : is_teacher,
	is_audio : 1,
	content : null,
  });

  const file = await modules.upload.fileAdd(req, 'discussion_id', discussion.discussion_id);
  discussion.content = file.client_path;
  await discussion.save();
  res.json({ success: true, file, discussion });
}));

router.get('/:lecture_item_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureItemId = parseInt(req.params.lecture_item_id, 10);

  const discussion = await modules.models.discussion.findAll({
    where: {
      lecture_item_id: lectureItemId,
    },
    include: [
      { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
    ],
  });
  res.json({ discussion });
}));

router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const id = parseInt(req.params.id, 10);
  const { content } = req.body;

  const discussion = await modules.models.discussion.update(
    { content },
    { where: { lecture_item_id: id } },
  );

  // const discussionInfo = await modules.models.discussion_info.findByPk(id);
  // discussionInfo.student_share = share;
  // discussionInfo.topic = topic;
  // await discussionInfo.save();

  res.json({ success: true });
}));

module.exports = router;
