const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');


router.get('/:user_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {user_id} = req.params;
  // 테이블 검색
  const whole_aff_list = await modules.models.user_belonging.findAll({
    include: [{
      attributes: ['description'],
      model: modules.models.affiliation_description,
    }
    ],
    where: {
      user_id: user_id,
    }
  });
  // console.log(whole_aff_list);

  // 리턴
  res.json({ success: true, whole_aff_list });
}));

router.post('/:user_id/:affiliation_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {user_id, affiliation_id} = req.params;

  const result_exclude = await modules.models.user_belonging.update({
    main_belonging: 0,
  }, {
    where: {
      user_id: user_id,
      main_belonging: 1,
    }
  });
  const result_include = await modules.models.user_belonging.update({
    main_belonging: 1,
  }, {
    where: {
      user_id: user_id,
      affiliation_id: affiliation_id,
    },
  });

  // const where = {};
  // if (user_id) {
  //   where.user_id = user_id;
  // }

  // // 테이블 검색
  // const user_belonging_list = await modules.models.user_belonging.findAll({
  //   attributes: ['user_id', 'affiliation_id', 'main_belonging'],
  //   where,
  // });

  // user_belonging_list.forEach(belong => {
  //   if(belong.dataValues.affiliation_id === Number(affiliation_id)) {
  //     belong.dataValues.main_belonging = 1;
  //   }
  //   else {
  //     belong.dataValues.main_belonging = 0;
  //   }
  // });

  // user_belonging_list.forEach(async (belong) => {
  //   console.log(belong.dataValues);
  //   // const where = {};
  //   // if (user_id) {
  //   //   where.user_id = belong.dataValues.user_id;
  //   //   where.affiliation_id = belong.dataValues.affiliation_id;
  //   // }
  //   const result = await modules.models.user_belonging.update({
  //     main_belonging: belong.dataValues.main_belonging,
  //   }, {
  //     where: {
  //       user_id: belong.dataValues.user_id,
  //       affiliation_id: belong.dataValues.affiliation_id,
  //     },
  //   });
  //   console.log(result);
  // });
}));


module.exports = router;
