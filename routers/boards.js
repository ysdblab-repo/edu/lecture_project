const express = require('express');
const router = express.Router();

const modules = require('../modules/frequent-modules');

router.post('/:bid/reply', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId } = req.decoded;
  const bid = parseInt(req.params.bid, 10);
  const { content } = req.body;

  const board = await modules.models.board.findByPk(bid, {
    include: {
      model: modules.models.class,
    },
  });

  const teacherCheck = await modules.teacherInClass(authId, board.class.class_id);
  let isTeacher = false;
  if (teacherCheck) {
    isTeacher = true;
  }

  const reply = await board.createBoard_reply({
    writer_id: authId,
    is_teacher: isTeacher,
    content,
  });

  const sending = await modules.models.board_reply.findByPk(reply.board_reply_id, {
    include: {
      model: modules.models.user,
      attributes: modules.models.user.selects.userJoined,
    },
  });

  res.json(sending);
}));

router.put('/:bid/reply/:rid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const bid = parseInt(req.params.bid, 10);
  const rid = parseInt(req.params.rid, 10);
  const { content } = req.body;

  const board = await modules.models.board.findByPk(bid, {
    include: {
      model: modules.models.class,
    },
  });

  const reply = await modules.models.board_reply.findByPk(rid, {
    include: {
      model: modules.models.user,
      attributes: modules.models.user.selects.userJoined,
    },
  });
  const isWriter = reply.writer === authId;

  const teacherCheck = await modules.teacherInClass(authId, board.class.class_id);
  if (!(authType === 3 || teacherCheck || isWriter)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  reply.content = content;
  await reply.save();

  res.json(reply);
}));

router.delete('/:bid/reply/:rid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const bid = parseInt(req.params.bid, 10);
  const rid = parseInt(req.params.rid, 10);

  const board = await modules.models.board.findByPk(bid, {
    include: {
      model: modules.models.class,
    },
  });

  const reply = await modules.models.board_reply.findByPk(rid);
  const isWriter = reply.writer_id === authId;

  const teacherCheck = await modules.teacherInClass(authId, board.class.class_id);
  if (!(authType === 3 || teacherCheck || isWriter)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.board_reply.destroy({
    where: { board_reply_id: rid },
  });

  res.json({ success: true });
}));

module.exports = router;
