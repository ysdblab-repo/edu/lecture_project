const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db')
const modules = require('../modules/frequent-modules');


router.get('/', modules.asyncWrapper(async (req, res, next) => {


  const main_classes = await db.getQueryResult("select c.*,u.name as master_name, f.client_path from main_classes mc join classes c on c.class_id = mc.class_id left join files f on f.user_id_profile = c.master_id join users u on u.user_id = c.master_id");

  res.json({ success: true, main_classes });
}));


module.exports = router;
