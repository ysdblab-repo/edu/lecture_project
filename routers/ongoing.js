const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');

router.get('/', modules.asyncWrapper(async (req, res) => {
  res.json({ success: true });
}));

// const async = require('async');
// const modules = require('../modules/frequent-modules');

router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  
  // 강사 token 이랑 
  const lectureId = parseInt(req.params.id, 10);
  
  const sql1 = `SELECT * FROM lectures, user_classes WHERE lecture_id = ${lectureId} and user_classes.class_id = lectures.class_id`;
  const sql2 = `SELECT * FROM heartbeat_counts WHERE (UNIX_TIMESTAMP(CURDATE()) - UNIX_TIMESTAMP(updated_at)) > 150000 AND lecture_id = ${lectureId} ORDER BY updated_at DESC`;
  
  const OnstudentList = await db.getQueryResult(sql2);
  const studentList = await db.getQueryResult(sql1);
//  const on_studentList = [];
  const studentListCnt = studentList.length;
  const OnstudentListCnt = OnstudentList.length;


  res.json({ student_cnt: studentListCnt, student_list: studentList, on_student_cnt: OnstudentListCnt, on_student_list: [] });

}));
module.exports = router;
