const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');

const modules = require('../modules/frequent-modules');

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureItemId } = req.body;

  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const survey = await lectureItem.createSurvey({
    type: 0,
    creator_id : authId,
    lecture_item_id: lectureItemId,
  });

  res.json({ success: true, survey_id: survey.survey_id });
}));

router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const surveyId = parseInt(req.params.id, 10);
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  const survey = await modules.models.survey.findByPk(surveyId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!survey) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, survey.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  for (let i = 0; i < keyLen; i += 1) {
    const key = keys[i];
    if (key === 'comment' || key === 'type') {
      survey[key] = req.body[key];
    } else if (key === 'choice') {
      survey[key] = req.body[key].join(modules.config.separator);
    }
  }
  await survey.save();

  res.json({ success: true });
}));

router.post('/:id/file', modules.auth.survey, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const file = await modules.upload.fileAdd(req, 'survey_id', parseInt(req.params.id, 10));
  res.json({ success: true, file });
}));

router.get('/:id/result', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);

  const allAnswers = await modules.models.student_survey.findAll({
    attributes: [['answer', 'student_answer'], [sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('student_id'))), 'num_students']],
    where: {
      survey_id: id,
    },
    group: 'answer',
  });
  for (let i = 0; i < allAnswers.length; i += 1) {
    allAnswers[i].dataValues.student_answer = allAnswers[i].dataValues.student_answer.split(modules.config.separator);
  }

  const totalNum = await modules.models.student_survey.findOne({
    attributes: [[sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('student_id'))), 'num_students_total']],
    where: {
      survey_id: id,
    },
  });

  res.json({
    num_students_total: totalNum.dataValues.num_students_total,
    student_answers: allAnswers,
  });
}));
const keywordUpsert = async (values, condition) => modules.models
  .survey_keyword.findOne({ where: condition })
  .then((obj) => {
    if (obj) {
      return obj.update(values);
    } else {
      return modules.models.survey_keyword.create(values);
    }
  });

router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const surveyId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const survey = await modules.models.survey.findByPk(surveyId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
        include: {
          model: modules.models.class,
        },
      },
    },
  });
  if (!survey) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, survey.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  let scoreSum = 0;
  const dataLen = data.length;
  for (let i = 0; i < dataLen; i += 1) {
    scoreSum += parseInt(data[i].score, 10);
  }
  const promises = [];
  for (let i = 0; i < dataLen; i += 1) {
    promises.push(keywordUpsert(
      { survey_id: survey.survey_id, lecture_id: survey.lecture_item.lecture_id, lecture_item_id: survey.lecture_item_id, keyword: data[i].keyword, score_portion: data[i].score },
      { survey_id: survey.survey_id, keyword: data[i].keyword },
    ));
  }
  Promise.all(promises);
  survey.score = scoreSum;
  await survey.save();

  res.json({ success: true, score: scoreSum });

  await modules.coverageCalculator.coverageAll(survey.lecture_item.lecture_id, survey.lecture_item.lecture.class_id);
}));

router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const surveyId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const surveyKeywords = (await modules.models.survey.findByPk(surveyId, {
    include: { model: modules.models.survey_keyword },
  })).survey_keywords;

  if (!surveyKeywords) {
    throw modules.errorBuilder.default('Not Found -', 404, true);
  }

  res.json(surveyKeywords);
}));

router.delete('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const surveyId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const survey = await modules.models.survey.findByPk(surveyId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!survey) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, survey.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.survey_keyword.destroy({
    where: {
      survey_id: surveyId,
    },
  });

  res.json({ success: true });
}));


module.exports = router;

// router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
//   const { authId, authType } = req.decoded;
//   const { type, comment, lectureItemId } = req.body;
//   let choice = [];
//   if (type === 0) {
//     choice = req.body.choice;
//   }
//   const choiceString = choice.join(modules.config.separator);
//
//   const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
//     include: {
//       model: modules.models.lecture,
//     },
//   });
//   if (!lectureItem) {
//     throw modules.errorBuilder.default('Not Found', 404, true);
//   }
//   const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
//   if (!(authType === 3 || teacherCkeck)) {
//     throw modules.errorBuilder.default('Permission Denied', 403, true);
//   }
//
//   const survey = await lectureItem.createSurvey({
//     type,
//     comment,
//     choice: choiceString,
//     lecture_item_id: lectureItemId,
//   });
//
//   res.json({ success: true, survey_id: survey.survey_id });
// }));
