const express = require('express');
const router = express.Router();
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const config = require('../config');
const { Op } = require("sequelize");
// 해당 강사의 모든 아이템을 return
router.get('/',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const { authId, authType } = req.decoded;

  if (!(authType === 3 || authType === 1)){
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  const lectureItems = await modules.models.lecture_item.findAll({
    include:{
      model: modules.models.lecture,
      where : {
        teacher_id : authId
      }
    }
  });
  if(lectureItems.length === 0) {
    throw modules.errorBuilder.default('Lecture_item is not exists', 404, true);
  }
  res.json(lectureItems);
}));

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { lectureId, type, order } = req.body;
  const { authId, authType } = req.decoded;

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.lecture_item,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Lecture Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const newItem = await lecture.createLecture_item({
    type,
    order,
  });

  const newList = await modules.models.lecture_item_list.create({
    lecture_id: lecture.lecture_id,
    item_id: newItem.lecture_item_id,
    linked_list: newItem.lecture_item_id
  });

  const newGroup = await modules.models.lecture_item_group.create({
    lecture_id: lecture.lecture_id,
    group_list: newList.lecture_item_list_id,
    opened: 0,
    start: 0,
    end: 0
  });

  res.json({ success: true, lecture_item_id: newItem.lecture_item_id, lecture_item_list_id: newList.lecture_item_list_id, lecture_item_group_id: newGroup.lecture_item_group_id });
}));
// question, survey keyword 검색
router.get('/search',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const { authId, authType } = req.decoded;
  const keyword = req.query.keywords;
 
  let keywords  = [];
  if(!Array.isArray(keyword)){
    keywords.push(keyword);
  }else{
    keywords = keyword;
  }

  var ary = [];
  for ( var i=0; i <keywords.length; i+= 1) {
 
    
    const keywordsql = `select * from lecture_items where lecture_item_id in 
    (select q.lecture_item_id from questions q, question_keywords qk where q.question_id = qk.question_id and qk.keyword like '%${keywords[i]}%')`
    const keywordResult = await db.getQueryResult(keywordsql);
    

    const surveysql = `select * from lecture_items where lecture_item_id in (select s.lecture_item_id from surveys s, survey_keywords sk
       where s.survey_id = sk.survey_id and sk.keyword like '%${keywords[i]}%')`;
    const surveyResult = await db.getQueryResult(surveysql);
    
    surveyResult.forEach(function(e) {  ary.push(e); });
    keywordResult.forEach(function(e) { ary.push(e); });
    console.log(keywordResult.length);
    console.log(surveyResult.length);

  }
  res.json(ary);

}));
router.get('/multiple/:list', modules.auth.tokenCheck ,  modules.asyncWrapper(async (req, res, next) => {
  const data = req.params.list;
  const { authId, authType } = req.decoded;

  let ids = data.split(config.separator);
  let items = [];
  let forEachCount = 0;
  ids.forEach(async id => {
    // 문제 가져와서 보내주기 
    let item = await modules.models.lecture_item.findByPk(ids[i], {
      include: [
        {
           model: modules.models.question,
           include: [
            { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
            { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
            { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
           ]
        },
        {
          model: modules.models.lecture_code_practice,
        }, {
          model: modules.models.discussion,
          include: { model: modules.models.user },
        }, {
          model: modules.models.discussion_info,
        }, {
          model: modules.models.survey,
          include: [
            { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
          ],
        }, {
          model: modules.models.note,
          include: [
            { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
          ],
        }, {
          model: modules.models.individual_question_info,
          include: {
            model: modules.models.question,
            include: [
              { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
              { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
              { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
              { model: modules.models.student_answer_log, required: false,
                include: [
                  { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
                  { model: modules.models.oj_solution, required: false },
                  { model: modules.models.oj_compileinfo, required: false },
                  { model: modules.models.oj_runtimeinfo, required: false },
                  { model: modules.models.feedback, required: false },
                  { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
                ],
              },
              { model: modules.models.lecture_item },
            ],
          }
        },

      ]
    });
    const lec = item.toJSON();
    if (lec.type === 0) {
      const questionSplitFunc = () => {
        return new Promise(async (resolve, reject) => {
          let forEachCount3 = 0;
          lec.questions.forEach(async question => {
            if (question.choice) {
              question.choice = question.choice.split(modules.config.separator);
            }
            if (question.answer) {
              question.answer = question.answer.split(modules.config.separator);
            }
            if (question.accept_language) {
              question.accept_language = question.accept_language.split(modules.config.separator);
            }
            if (question.multi_choice_media) {
              question.multi_choice_media = question.multi_choice_media.split(modules.config.separator);
            }
            if (question.type === 3) {
              const problem = await modules.models.oj_problem.findOne({
                where: { problem_id: question.question_id }
              });
              if (problem) {
                question.input_description = problem.input;
                question.output_description = problem.output;
                question.sample_input = problem.sample_input;
                question.sample_output = problem.sample_output;
                question.time_limit = problem.time_limit;
                question.memory_limit = problem.memory_limit;
              }
            }
            
            if (!question.sqlite_file_guid) {
              question.sql_lite_file = [];
            } else {
              question.sql_lite_file = await modules.models.file.findAll({
                where: { file_guid: question.sqlite_file_guid }
              });
            }
            if (++forEachCount3 === lec.questions.length) {
              resolve(true); // 1 이상의 length에서 정삭적으로 forEach문을 마쳤을 경우
            }
          });
          if (lec.questions.length === 0) { // 0의 length 이던가 forEach문을 정삭적으로 마치지 못했을 때
            resolve(false);
          }
        });
      };

      const res1 = await questionSplitFunc(); 
    }

    lec.surveys.forEach(survey => {
      if (survey.choice) {
        survey.choice = survey.choice.split(modules.config.separator);
      }
    });

    if (lec.type === 6) { // question 중복 표기 방지 (info 속 question, 그냥 question)
      const indivQuestionSplitFunc = () => new Promise(async (resolve, reject) => {
        let forEachCount1 = 0;
        lec.individual_question_infos.forEach(indivQInfo => {
          indivQInfo.difficulty = indivQInfo.difficulty.split(modules.config.separator);
          indivQInfo.keyword = indivQInfo.keyword.split(modules.config.separator);
          let forEachCount2 = 0;
          indivQInfo.questions.forEach(async question => {
            if (question.choice) {
              question.choice = question.choice.split(modules.config.separator);
            }
            if (question.answer) {
              question.answer = question.answer.split(modules.config.separator);
            }
            if (question.accept_language) {
              question.accept_language = question.accept_language.split(modules.config.separator);
            }
            if (question.multi_choice_media) {
              question.multi_choice_media = question.multi_choice_media.split(modules.config.separator);
            }
            if (question.type === 3) {
              const problem = await modules.models.oj_problem.findOne({
                where: { problem_id: question.question_id }
              });
              if (problem) {
                question.input_description = problem.input;
                question.output_description = problem.output;
                question.sample_input = problem.sample_input;
                question.sample_output = problem.sample_output;
                question.time_limit = problem.time_limit;
                question.memory_limit = problem.memory_limit;
              }
            }
            
            if (!question.sqlite_file_guid) {
              question.sql_lite_file = [];
            } else {
              question.sql_lite_file = await modules.models.file.findAll({
                where: { file_guid: question.sqlite_file_guid }
              });
            }
            if (++forEachCount2 === indivQInfo.questions.length) {
              forEachCount1++;
            }
          });
          if (forEachCount1 === lec.individual_question_infos.length) {
            resolve(true);
          }
        });
        if (lec.individual_question_infos.length === 0) {
          resolve(false);
        }
      });
      const res2 = await indivQuestionSplitFunc();
    }
    items.push(lec);
    if (++forEachCount === ids.length) { // 정상적인 상황 모든 forEach 문을 돌고 난뒤에
      res.json({ success: items });
    }
  });
  if (ids.length === 0) {
    res.json({ success: items }); // forEach문 밖으로 나간상황 length 가 0이거나 이상한 오류로(forEachCount 오류로 인한(코딩 미숙))
  }
}));

router.post('/screenConcentration', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { lectureItemId, concentration, start, end } = req.body;
  const { authId } = req.decoded;

  const result = await modules.models.screen_concentration.create({
    lecture_item_id: lectureItemId,
    user_id: authId,
    concentration: concentration,
    start,
    end,
  });

  res.json({
    result
  });
}));

router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureItemId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: [{
      model: modules.models.lecture,
    }, {
      model: modules.models.question,
      include: [
        { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
        { model: modules.models.student_answer_log, required: false,
          include: [
            { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
            { model: modules.models.oj_solution, required: false },
            { model: modules.models.oj_compileinfo, required: false },
            { model: modules.models.oj_runtimeinfo, required: false },
            { model: modules.models.feedback, required: false },
            { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
          ],
        },
      ],
    }, {
      model: modules.models.lecture_code_practice,
    }, {
      model: modules.models.discussion,
      include: { model: modules.models.user },
    }, {
      model: modules.models.discussion_info,
    }, {
      model: modules.models.survey,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.student_survey, where: { student_id: authId }, required: false },
      ],
    }, {
      model: modules.models.note,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
      ],
    }, {
      model: modules.models.coding,
    }, {
      model: modules.models.individual_question_info,
      include: [{
        model: modules.models.question,
        include: [
          { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
          { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
          { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
          { model: modules.models.student_answer_log, required: false,
            include: [
              { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
              { model: modules.models.oj_solution, required: false },
              { model: modules.models.oj_compileinfo, required: false },
              { model: modules.models.oj_runtimeinfo, required: false },
              { model: modules.models.feedback, required: false },
              { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
            ],
          },
          { model: modules.models.lecture_item },
        ],
      }],
    }],
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const lec = lectureItem.toJSON();
  delete lec.lecture;

  const questionSplitFunc = () => new Promise(async (resolve, reject) => {
    let forEachCount = 0;
    lec.questions.forEach(async question => {
      if (question.choice) {
        question.choice = question.choice.split(modules.config.separator);
      }
      if (question.answer) {
        question.answer = question.answer.split(modules.config.separator);
      }
      if (question.choice2) {
        question.choice2 = question.choice2.split(modules.config.separator);
      }
      if (question.accept_language) {
        question.accept_language = question.accept_language.split(modules.config.separator);
      }
      if (question.multi_choice_media) {
        question.multi_choice_media = question.multi_choice_media.split(modules.config.separator);
      }
      if (question.type === 3) {
        const problem = await modules.models.oj_problem.findOne({
          where: { problem_id: question.question_id }
        });
        if (problem) {
          question.input_description = problem.input;
          question.output_description = problem.output;
          question.sample_input = problem.sample_input;
          question.sample_output = problem.sample_output;
          question.time_limit = problem.time_limit;
          question.memory_limit = problem.memory_limit;
          question.sample_code = problem.sample_code;
        }
      }
      for (let j = 0; j < question.student_answer_logs.length; j += 1) {
        if (question.student_answer_logs[j].answer) {
          question.student_answer_logs[j].answer =
            question.student_answer_logs[j].answer.split(modules.config.separator);
        } else {
          question.student_answer_logs[j].answer = [];
        }
      }
      if (!question.sqlite_file_guid) {
        question.sql_lite_file = [];
      } else {
        question.sql_lite_file = await modules.models.file.findAll({
          where: { file_guid: question.sqlite_file_guid }
        });
      }
      if (++forEachCount === lec.questions.length) {
        resolve(true); // 1 이상의 length에서 정삭적으로 forEach문을 마쳤을 경우
      }
    });
    if (lec.questions.length === 0) { // 0의 length 이던가 forEach문을 정삭적으로 마치지 못했을 때
      resolve(false);
    }
  });
  const res1 = await questionSplitFunc(); 
  const indivQuestionSplitFunc = () => {
    return new Promise(async (resolve, reject) => {
      let forEachCount1 = 0;
      lec.individual_question_infos.forEach(indivQInfo => {
        indivQInfo.difficulty = indivQInfo.difficulty.split(modules.config.separator);
        indivQInfo.keyword = indivQInfo.keyword.split(modules.config.separator);
        let forEachCount2 = 0;
        indivQInfo.questions.forEach(async question => {
          if (question.choice) {
            question.choice = question.choice.split(modules.config.separator);
          }
          if (question.answer) {
            question.answer = question.answer.split(modules.config.separator);
          }
          if (question.choice2) {
            question.choice2 = question.choice2.split(modules.config.separator);
          }
          if (question.accept_language) {
            question.accept_language = question.accept_language.split(modules.config.separator);
          }
          if (question.multi_choice_media) {
            question.multi_choice_media = question.multi_choice_media.split(modules.config.separator);
          }
          if (question.type === 3) {
            const problem = await modules.models.oj_problem.findOne({
              where: { problem_id: question.question_id }
            });
            if (problem) {
              question.input_description = problem.input;
              question.output_description = problem.output;
              question.sample_input = problem.sample_input;
              question.sample_output = problem.sample_output;
              question.time_limit = problem.time_limit;
              question.memory_limit = problem.memory_limit;
            }
          }
          
          if (!question.sqlite_file_guid) {
            question.sql_lite_file = [];
          } else {
            question.sql_lite_file = await modules.models.file.findAll({
              where: { file_guid: question.sqlite_file_guid }
            });
          }

          forEachCount2 += 1;
          if (forEachCount2 === indivQInfo.questions.length) {
            forEachCount1 += 1;
            if (forEachCount1 === lec.individual_question_infos.length) {
              resolve(true);
            }
          }
        });
        if (indivQInfo.questions.length === 0) {
          forEachCount1 += 1;
        }
        if (forEachCount1 === lec.individual_question_infos.length) {
          resolve(true);
        }
      });
      if (lec.individual_question_infos.length === 0) { // 0의 length 이던가 forEach문을 정삭적으로 마치지 못했을 때
        resolve(false);
      }
    });
  };
  const res2 = await indivQuestionSplitFunc();

  lec.surveys.forEach(survey => {
    if (survey.choice) {
      survey.choice = survey.choice.split(modules.config.separator);
    }
    if (survey.student_surveys[0]) {
      survey.student_surveys[0].answer = survey.student_surveys[0].answer.split(modules.config.separator);
    }
  })
  
  res.json(lec);
}));

router.delete('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureItemId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  if (lectureItem.type === 6) {
    const individual_questions_to_delete = await modules.models.question.findAll({ where: { info_item_id: lectureItemId }});
    individual_questions_to_delete.forEach((question) => {
      modules.delete.lectureItems(question.lecture_item_id);
    });
  }
  const result = await modules.delete.lectureItems(lectureItemId);

  res.json({ data: result });
}));

router.get('/small-group/item-list/:smallGroupId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const smallGroupId = parseInt(req.params.smallGroupId, 10);

  const result = await modules.models.lecture_item_small_group.findOne({
    where: {
      lecture_item_small_group_id: smallGroupId,
    },
  });
  
  res.json({ result });
}));

router.get('/item-list/lecture-item/:itemListId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const itemListId = parseInt(req.params.itemListId, 10);

  const result = await modules.models.lecture_item_list.findOne({
    where: {
      lecture_item_list_id: itemListId,
    },
  });
  
  res.json({ result });
}));

router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureItemId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  for (let i = 0; i < keyLen; i += 1) {
    const key = keys[i];
    if (key === 'start_time' || key === 'name' || key === 'end_time' ||
      key === 'order' || key === 'result' || key === 'opened' || key === 'scoring_finish' || key === 'sequence' || key === 'offset' || key === 'limit_length') {
      lectureItem[key] = req.body[key];
    }
  }
  await lectureItem.save();

  res.json({ success: true });
}));


router.post('/:id/material', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async( req, res) => {
  const lectureItemId = parseInt(req.params.id, 10);
  const file = await modules.upload.fileAdd(req, 'lecture_item_id', lectureItemId);
  res.json({ success: true, file });
}));


router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureItemId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const sql = `SELECT DISTINCT class_id FROM lectures, lecture_items WHERE lectures.lecture_id = lecture_items.lecture_id AND lecture_items.lecture_item_id = ${lectureItemId}`;
  const lectureItems = await db.getQueryResult(sql);
  
  if (lectureItems.length === 0) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItems[0].class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  let sql2 = `INSERT IGNORE INTO lecture_item_keywords (keyword, weight, lecture_item_id) VALUES `;
  for(let i = 0; i < data.length; i+= 1) {
    let str = `('${data[i].keyword}',${data[i].weight},${lectureItemId})`;
    if (i !== data.length - 1) {
      str += ',';
    }
    sql2 += str;
  }

  const result = await db.getQueryResult(sql2);
  
  res.json({ success: true });

}));
router.delete('/:id/keywords/:keyword_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureItemId = parseInt(req.params.id, 10);
  const keywordId = parseInt(req.params.keyword_id, 10);

  const sql = `DELETE FROM lecture_item_keywords WHERE id = ${keywordId} and lecture_item_id = ${lectureItemId}`;
  const result = await db.getQueryResult(sql);
  res.json({ success: true });

}));
router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async( req, res, next ) => {

  const lectureItemId = parseInt(req.params.id,10);
  const sql = `SELECT keyword,weight,lecture_item_id FROM lecture_item_keywords WHERE lecture_item_id = ${lectureItemId}`;
  const result = await db.getQueryResult(sql);
  const ary = [];
  for (let i = 0; i < result.length; i += 1) {
    let obj = {};
    obj['lecture_item_id'] = result[i].lecture_item_id;
    obj['keyword'] = result[i].keyword;
    obj['weight'] = result[i].weight;
    ary.push(obj);
  }

  res.json(ary);
}));
router.post('/:id/linked_list2', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => { // 장서연 - small group 

  const lectureId = parseInt(req.params.id, 10);
  const { item_id, list} = req.body;

  var itemList = "";
  for( var i = 0; i<list.length-1; i+= 1) {
     itemList += `${list[i].item_id}${modules.config.separator}`;
  }
  itemList += `${list[list.length-1].item_id}`;

  let r = await modules.models.lecture_item_small_group.create({
    lecture_id: lectureId,
    linked_small_group: itemList
  });
  res.json({success: true , lecture_item_small_group_id: r.lecture_item_small_group_id,  list: itemList });

}));
// lectureId
router.post('/:id/linked_list', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { item_id, list} = req.body;

  const lists = await modules.models.lecture_item_list.findAll({
    where: {
      lecture_id: lectureId,
    }
  })
  for ( var i = 0; i<lists.length; i+= 1) {
    
    // 대표문항이 같으면 실패
    if( lists[i].item_id == item_id ) {
      throw modules.errorBuilder.default('Lecture_item is already exists', 404, true);
    }  
    // 연결된 항목이 이미 존재하면 실패
    let itemList = lists[i].linked_list.split(modules.config.separator);
    for ( var j =0; j<itemList.length; j+= 1){
      if( itemList[j] == item_id ) {
        console.log('case 2');
        throw modules.errorBuilder.default('Lecture_item is already exists', 404, true);
      }
    }      
  }
 
  var itemList = "";
  for( var i = 0; i<list.length-1; i+= 1) {
     itemList += `${list[i].item_id}${modules.config.separator}`;
  }
  itemList += `${list[list.length-1].item_id}`;

  let r = await modules.models.lecture_item_list.create({
    lecture_id: lectureId,
    item_id: item_id,
    linked_list: itemList
  });

  res.json({success: true , lecture_item_list_id: r.lecture_item_list_id,  list: itemList });

}));
// lecture_id
router.get('/:id/linked_list', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const list = await modules.models.lecture_item_list.findAll({
     where: {
        lecture_id: lectureId,
     }
  });

  res.json(list);
}))
// delete relation
router.delete('/linked_list/:id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {

  const itemId = parseInt(req.params.id, 10);
  await modules.models.lecture_item_list.destroy({
    where: {
      item_id: itemId,
    }
  });
  res.json({success: true});
}));

router.post('/:id/small_group', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => { // 장서연
  const lectureId = parseInt(req.params.id, 10);
  let { list, during } = req.body;
  let s = await modules.models.lecture_item_small_group.create({
    lecture_id: lectureId,
    linked_small_group: list,
    during: during,
  });
  res.json({success: true, lecture_item_small_group_id: s.lecture_item_small_group_id});
}));

router.post('/:id/group', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  let { list, start, end, ifSmallGroup, timerShow } = req.body;

  let lectureType = await modules.models.lecture.findOne({ where : { lecture_id : lectureId }});
  if (!lectureType){
    // throw modules.errorBuilder.default('')
  } else {
    // 이미 연결된게 있는지 확인
    let listlist = list.split(config.separator);
    for( let i = 0; i<listlist.length; i+= 1) {
      let l = await modules.models.lecture_item_list.findOne({
        where: {
          lecture_item_list_id: listlist[i],
        }
      });
      if ( !l ) {
        l = await modules.models.lecture_item_small_group.findOne({
          where: {
            lecture_item_small_group_id: listlist[i],
          }
        });
      }
      if ( !l ) { throw modules.errorBuilder.default(`lecture_item_list_id - ${listlist[i]} is not exist`, 403, true); }
    }
    // 무인강의일때 
    //console.log(lectureType);
    if ( lectureType.type !== 0 ) {
      await modules.models.lecture_item_group.create({
        lecture_id: lectureId,
        group_list: list,
        opened: 0,
        start,
        end,
        timerShow
      });    
    } else if ( lectureType.type === 0 && ifSmallGroup !== null) { // 유인 강의 셔플링 일때
      await modules.models.lecture_item_group.create({
        lecture_id: lectureId,
        opened: 0,
        group_list: list,
        if_small_group: ifSmallGroup,
        start: start,
        end: end,
      });
    } else { // 유인 강의 셔플링 아닐때
      await modules.models.lecture_item_group.create({
        lecture_id: lectureId,
        opened: 0,
        group_list: list,
        if_small_group: ifSmallGroup,
      });
    }
  }
  res.json({success: true , list: list });

}));
router.put('/:id/group/:gid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const groupId = parseInt(req.params.gid,10);
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  let group = await modules.models.lecture_item_group.findOne({
    where: { lecture_item_group_id: groupId, lecture_id: lectureId }
  });

  if ( group ) {
    for (let i = 0; i < keyLen; i += 1) {
      const key = keys[i];
      if (key === 'start' || key === 'end' || key === 'group_list') {
        group[key] = req.body[key];
      }
    }
    await group.save();
    res.json(group);

  } else {
    throw modules.errorBuilder.default('lecture_item_group is not exist', 403, true);
  }
}));

router.post('/log', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureItemId } = req.body;

  const log = await modules.models.lecture_item_log.findOne({
    where: {
      lecture_item_id: lectureItemId,
      user_id: authId
    }
  });

  if (!log) {
    await modules.models.lecture_item_log.create({
      lecture_item_id: lectureItemId,
      user_id: authId
    }); 
  }

  res.json({
    success: true
  });
}));

router.get('/log/:lecture_item_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lecture_item_id = Number(req.params.lecture_item_id);

  const log = await modules.models.lecture_item_log.findOne({
    where: {
      lecture_item_id,
      user_id: authId
    }
  });

  res.json({
    log
  });
}));

// lectureId 그룹 가져오기
router.get('/:id/group', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const groups = await modules.models.lecture_item_group.findAll({
     where: {
        lecture_id: lectureId,
     }
  });
  let items = [];
  for ( let i = 0 ; i < groups.length ; i += 1 ) {
    let obj = {};
    obj['group_id'] = groups[i].lecture_item_group_id;
    obj['list_ids'] = groups[i].group_list.split(config.separator);
    obj['start'] = groups[i].start;
    obj['end'] = groups[i].end;
    obj['if_small_group'] = groups[i].if_small_group;
    obj['timerShow'] = groups[i].timerShow
    items.push(obj); 
  }
  res.json({ lecture_id: lectureId, list: items });
}));
// group id 그룹 삭제하기
router.delete('/group/:id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {

  const groupId = parseInt(req.params.id, 10);
  await modules.models.lecture_item_group.destroy({
    where: {
      lecture_item_group_id: groupId,
    }
  });
  res.json({success: true});
}));

// lectureId 소그룹 가져오기
router.get('/:id/small-group', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const groups = await modules.models.lecture_item_small_group.findAll({
    where: {
      lecture_id: lectureId,
    }
  });
  res.json({lecture_id: lectureId, group: groups});
}));
// group id 그룹 삭제하기
router.delete('/small-group/:id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const smallGroupId = parseInt(req.params.id, 10);
  await modules.models.lecture_item_small_group.destroy({
    where: {
      lecture_item_small_group_id: smallGroupId,
    }
  });
  res.json({success: true});
}));

// lectureId 아이템 가져오기
router.get('/:id/item', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const items = await modules.models.lecture_item_list.findAll({
    where: {
      lecture_id: lectureId,
    }
  });
  res.json({lecture_id: lectureId, items: items});
}));

// 아이템 키워드 가져오기
router.get('/lecture-item/keyword', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.get('/lecture-item/keyword', modules.asyncWrapper(async (req, res, next) => {

  const { authId, authType } = req.decoded;
  const { lecture_item_id } = req.query;
  const lectureItemId = lecture_item_id;

  // 유효성 검사
  if (!(lecture_item_id)) {
    throw modules.errorBuilder.default('lecture_item_id 는 반드시 입력해야 합니다. ex) .../lecture-item-keyword?lecture_item_id=1', 403, true);
  }

  // TODO: 권한 체크 세분화 - 이사람이 이 강의의 강사인가?

  // 타입이 뭔가?
  const res1 = await modules.models.lecture_item.findByPk(lectureItemId, {
    attributes: ['type'],
  });
  const itemType = res1.type;

  // 타입에 따른 키워드 가져오기
  let res2;

  switch (itemType) {
    case 0: // 문항
      res2 = await modules.models.question_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    case 1: // 설문
      res2 = await modules.models.survey_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    case 2: // 실습
      res2 = await modules.models.lecture_code_practice_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    case 3: // 토론
      res2 = [];
      break;
    case 4: // 자료
      res2 = await modules.models.note_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    case 5: // 실습과제
      res2 = await modules.models.lecture_code_practice_keyword.findAll({
        attributes: ['keyword'],
        where: { lecture_item_id: lectureItemId },
      });
      break;
    default:
      throw modules.errorBuilder.default('키워드를 구하는중에 문제가 발생하였습니다.', 404, true);
  }
  const keywords = res2.map(element => element.keyword);
  res.json({ success: true, keywords });
}));

router.get('/:id/findList', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  //세션 아이템에서 문제를 받아오지 않는 문제를 수정하기 위해 문제만 따로 받아옴.
  const lectureId = parseInt(req.params.id, 10);

  const sql = `SELECT * FROM questions, lecture_items WHERE lecture_items.lecture_item_id = questions.lecture_item_id AND lecture_items.lecture_id = ${lectureId}`;
  const result = await db.getQueryResult(sql);
  res.json({result});
}));

router.post('/shown_log', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { lecture_item_id } = req.body;
  const { authId, authType } = req.decoded;

  const exist = await modules.models.lecture_item_shown_log.findOne({
    where: {
      lecture_item_id,
      student_id: authId
    }
  });
  if (!exist) {
    await modules.models.lecture_item_shown_log.create({
      student_id: authId,
      lecture_item_id,
    });
  }

  res.json({
    success: true
  });
}));

router.get('/shown_log/lecture_id/:lecture_id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const lecture_id = Number(req.params.lecture_id);
  const { authId, authType } = req.decoded;

  const lecture_items = await modules.models.lecture_item.findAll({
    where: {
      lecture_id
    }
  });

  const results = await modules.models.lecture_item_shown_log.findAll({
    where: {
      student_id: authId,
      lecture_item_id: {
        [Op.or]: lecture_items.map(lecture_item => lecture_item.dataValues.lecture_item_id)
      }
    }
  });

  const lecture_item_ids_set = new Set(results.map(result => result.dataValues.lecture_item_id));
  const lecture_item_ids = [...lecture_item_ids_set];

  res.json({ lecture_item_ids });
}));

module.exports = router;
