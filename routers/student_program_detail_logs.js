const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');

function csv_string_to_json(column_array,csvstr)
{
  var ret_array = []
  var rows = csvstr.split('\n');
  for(var i=0;i<rows.length;i++)
  {
    var one_row = rows[i]
    var one_row_column = one_row.split(',')
    if(one_row_column.length!=column_array.length)
    {
      console.log('column length error')
      break;
    }
    ret_json = {};
    for(var j=0;j<column_array.length;j++)
    {
      ret_json[column_array[j]] = one_row_column[j]
    }
    ret_array.push(ret_json)
  }
  return ret_array;
}
/*
로그 프로그램에서 나온 기록들 저장.
*/
router.post('/:user_id', modules.asyncWrapper(async (req, res, next) => {
  var csvdata = req.body.data_value
  console.log(csvdata)
  var jsondata = csv_string_to_json(['time','active_program','activated_time','num_keyboard_type','string_keyboard_type','is_on_lecture', 'mac_address', 'ip_address'], csvdata);
  for(var i=0;i<jsondata.length;i++)
  {
    jsondata[i]['user_id'] = req.params.user_id
    await modules.models.student_program_detail_log.create(jsondata[i])

  }
  res.json({success:true})

}));

module.exports = router;
