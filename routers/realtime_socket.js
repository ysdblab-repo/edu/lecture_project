const express = require('express');
const router = express.Router();
const _ = require('lodash');
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
// const analysis = require('../modules/analysis');
// const time_handler = require('../modules/time-handler');
const lecture_item_group_status = {};
const config = require('../config');

let speaker_socket_id = [];
let speaker_email_id = [];
let speaker_name = [];
let speaker_heartbeat = [];
let speaker_id = [];

let master_socket_id = [];
let master_id = [];
let master_name = [];
let master_heartbeat = [];

const DISCUSSION_LECTURE_ROOM_REPEAT_SIZE = 4;

Object.defineProperty(Array.prototype, 'flat', {
	value: function(depth = 1) {
		return this.reduce(function (flat, toFlatten) {
			return flat.concat((Array.isArray(toFlatten) && (depth>1)) ? toFlatten.flat(depth-1) : toFlatten);
		}, []);
	}
});

module.exports = function (io) {
  const answerLists = {}; // 답안 제출 여부 실시간 확인 , 미제출 학생여부는 클라이언트에서 수강생 목록을 가져와서 비교하여 판단
	const attendanceLists = {}; // 수강생 출석 통계
  // user_id, email_id, name, in_cnt(들어온 횟수), out_cnt(나간 횟수), current(현재 접속여부), ip_cnt(ip count 갯수 [1:정상,2이상:비정상])
  // 강사가 들어오기 전 쌓아두는 로그
  const offAnswerLists = [];
  const authentication = {}; // 학생들 인증번호 제출 리스트
  // 실습 점수 리스트
  // let practice_statuses = {};

	// function emitRoom(io, room_id, type, signal, lecture_id, role = 'both') {
	// 	Object.values(io.to(room_id).connected).filter(socket => socket.lecture_id === lecture_id && ( role === 'both' || socket.role === role )).forEach(socket => {
	// 		socket.emit(type, JSON.stringify(signal));
	// 	});
	// };

  function sendSignalToLecture(io, lecture_id, type, signal){
		io.to(`lecture${lecture_id}`).emit(type, JSON.stringify(signal));
	}
	// function sendSignalToLecture2(io, lecture_id, type, signal){
	// 	io.to('lecture'+lecture_id).emit(type, JSON.stringify(signal));
	// }
	function sendSignalToTeacher(io, lecture_id, type, signal){
		io.to(`lecture_teacher${lecture_id}`).emit(type, JSON.stringify(signal));
	}
	// function sendSignalToTeacher2(io, lecture_id, type, signal){
	// 	io.to('lecture_teacher'+lecture_id).emit(type, JSON.stringify(signal));
	// }
  function sendSignalToDiscussion(io, lecture_item_d, type, signal)
  {
		io.to(`discussion${lecture_item_d}`).emit(type, JSON.stringify(signal));
		// emitRoom(io, 'discussion'+lecture_item_d, type, signal, lecture_id);
	}
	function sendSignalToChat(io, lecture_id, type, signal)
  {
		io.to(`chat${lecture_id}`).emit(type, JSON.stringify(signal));
		// emitRoom(io, 'chat'+lecture_id, type, signal, lecture_id);
	}
	// function sendAnswerResult(io, lecture_id)
	// {
	// 	io.to(`lecture_teacher${lecture_id}`).emit('CHECK_ANSWER_LECTURE', JSON.stringify(answerLists[lecture_id]));
	// 	emitRoom(io, 'lecture_teacher'+lecture_id, 'CHECK_ANSWER_LECTURE', answerLists[lecture_id], lecture_id);
	// 	// io.sockets.in('lecture_teacher'+lecture_id).emit('CHECK_ANSWER_LECTURE', JSON.stringify(answerLists[lecture_id]));
	// 	// io.sockets.in('lecture'+ lecture_id).emit(type, JSON.stringify(ss))
	// }
	// function checkStudentList(io, lecture_id)
	// {
	// 	emitRoom(io, 'lecture_teacher'+lecture_id, 'CHECK_STUDENT_LIST', attendanceLists[lecture_id], lecture_id);
	// 	// io.sockets.in('lecture_teacher'+lecture_id).emit('CHECK_STUDENT_LIST', JSON.stringify(attendanceLists[lecture_id]));
	// }

  io.on('connection', (socket) => {
		socket.on('MASTER', async function(msg){
			io.emit('MASTER',msg);
    });
    
    // 강사가 먼저 들어간 경우 나중에 들어간 학생에게 강사가 이미 들어와있음을 알려야 함
    socket.on('TEACHER_EXIST', async function (msg) {
      const jsonMSG = JSON.parse(msg);
			sendSignalToLecture(io, jsonMSG.lecture_id, 'LECTURE_CONCENTRATION_CALCULATION', { 'signal': modules.faceRecognitionFlag[`${jsonMSG.lecture_id}`] ? true : false });
		});
		
		socket.on('REFRESH_ALL_STUDENT', async msg => {
			const { lecture_id, user_id } = JSON.parse(msg);
			// 둘다 falsy value가 아닐경우 (ex undefined)
			if (lecture_id && user_id) {
				const authorityCheck = await modules.models.lecture.findOne({
					where: {
						lecture_id,
						teacher_id: user_id,
					}
				});

				if (authorityCheck) {
					sendSignalToLecture(io, lecture_id, 'REFRESH_REQUEST');
				} else {
					console.log(`unauthorized teacher_id: ${user_id}, lecture_id: ${lecture_id}`);
				}
			}
		});

		socket.on('JOIN_LECTURE', async msg => {
			const jsonMSG = JSON.parse(msg);
			const lec = await modules.models.lecture.findOne({where: { lecture_id: jsonMSG.lecture_id }});
			const bool = await modules.models.user_class.findOne({ where: { user_id : jsonMSG.user_id }});
			socket.user_id = jsonMSG.user_id;
			socket.lecture_id = jsonMSG.lecture_id;
			socket.role = bool.role;
			console.log(`Join lecture, lecture_id: ${socket.lecture_id}, user_id: ${socket.user_id}, role: ${socket.role}`);
			if ( bool.role === 'student' ){
				socket.join(`lecture${jsonMSG.lecture_id}`);
				/// 없으면 생성
				if (attendanceLists[jsonMSG.lecture_id] === undefined) {
					const sql = `select user_id, email_id, name ,0 as in_cnt, 0 as out_cnt, 0 as current, 0 as ip_cnt from users where user_id in 
					(select uc.user_id from user_classes uc , lectures l where uc.class_id = l.class_id and uc.role = 'student' and l.lecture_id = ${jsonMSG.lecture_id})`;
					attendanceLists[jsonMSG.lecture_id] = await db.getQueryResult(sql);
				}

				let list = attendanceLists[jsonMSG.lecture_id];

				let studentIndex = list.findIndex(element => {
					return element.user_id === jsonMSG.user_id;
				})
				list[studentIndex].in_cnt += 1;
				list[studentIndex].current = 1;

				let ipSql = `SELECT res.user_id, res.ip, COUNT(res.ip) FROM (SELECT ip,user_id FROM homepage_logs WHERE TIMESTAMPDIFF(HOUR,created_at, ADDTIME(now(), '9:0:0.0')) < 3) res GROUP BY res.user_id, res.ip`;
				let ipResult = await db.getQueryResult(ipSql);
				for (let i = 0 ; i < list.length ; i += 1) {
					for (let j = 0 ; j < ipResult.length ; j += 1) {
						if (list[i].user_id === ipResult[j].user_id){
							list[i].ip_cnt += 1;
						}
					}
				}

				sendSignalToTeacher(io, jsonMSG.lecture_id, 'CHECK_STUDENT_LIST', list)
				
		
				await modules.models.lecture_student_login_log.create({
					lecture_id: jsonMSG.lecture_id,
					student_id: jsonMSG.user_id,
					type: 0
				});
			
			} else if (bool.role === 'teacher') {
			// 무인(강사) 강의인 경우 먼저 접속하여 답안을 제출한 학생들 답변이 있는것을 가져와야한다.
				let type = lec.type;

				socket.join('lecture_teacher' + jsonMSG.lecture_id);
				let sql = `select user_id, email_id, name ,0 as in_cnt, 0 as out_cnt, 0 as current, 0 as ip_cnt from users where user_id in 
				(select uc.user_id from user_classes uc , lectures l where uc.class_id = l.class_id and uc.role = 'student' and l.lecture_id = ${jsonMSG.lecture_id})`;
				let res = await db.getQueryResult(sql);


				/// 없으면 생성
				if (attendanceLists[jsonMSG.lecture_id] === undefined) {
					attendanceLists[jsonMSG.lecture_id] = res;
				}

				await modules.models.lecture_start.create({
					teacher_id: jsonMSG.user_id,
					lecture_id: jsonMSG.lecture_id,
				});

				await modules.models.lecture.update({
					is_auto: 1
				}, {
					where: { 
						lecture_id : jsonMSG.lecture_id
					}
				});

				// 강사가 나간지 5분되면 얼굴인식 끝나는데 5분안에 재입장시 그거 취소하는거
				if (modules.faceTimer[`${jsonMSG.lecture_id}`]) {
					clearTimeout(modules.faceTimer[`${jsonMSG.lecture_id}`]);
					modules.faceTimer[`${jsonMSG.lecture_id}`] = null;
				}

				if ( type === 1 ) {
					// 무인[단체] 일 경우 기존에 제출된 로그들을 현재 답안으로 가져오는 작업을 수행한다. 
					let curLecture = offAnswerLists.filter(element => {
						return element.lecture_id === lec.lecture_id;
					});
					if ( curLecture.length > 0 ) {
						let answers = _.groupBy(curLecture,'user_id');
						let users = res
							.filter(dat => answers[dat.user_id] !== undefined)
							.map(dat => {
								const stds = answers[dat.user_id];
								const std = stds.slice(-1)[0]; // last element
								return {
									item_id: std.item_id,
									user_id: std.user_id,
									name: dat.name,
									email_id: dat.email_id,
									type: std.type,
									answer: std.answer,
									count: stds.length
								};
							});
						// for ( let i = 0 ; i < res.length ; i += 1 ) {
							
						// 	let stds = answers[res[i].user_id];	
							
						// 	if ( stds !== undefined ){	 
						// 		let std = stds[stds.length-1];	// 제출 답변이 있다면 가장 마지막에 제출한 답변을 넣는다. 
						// 		// { 'lecture_id':jsonMSG.lecture_id,  'item_id': jsonMSG.item_id, 'user_id': jsonMSG.user_id, 'type':jsonMSG.type,'answer': jsonMSG.answer };
						// 		// 위의 offAnswerLists 객체들을 하단의 answerLists 로 전환 
						// 		// { 'item_id': jsonMSG.item_id, 'user_id': jsonMSG.user_id, 'name': student.name, 'email_id': student.email_id, 'type':jsonMSG.type,'answer': jsonMSG.answer ,'count':0};
						// 		let user = { 'item_id': std.item_id, 'user_id': std.user_id, 'name': res[i].name, 'email_id': res[i].email_id, 'type': std.type, 'answer': std.answer, 'count': stds.length };
						// 		users.push(user);
						// 	}
							
						// }
						answerLists[lec.lecture_id] = users;
						console.log(`offAnswerLists -> answerLists`);
						console.log(` -- 무인강의에서 넘어온 답안리스트 --`);
						console.log(JSON.stringify(users));
						console.log(`-----------------------------------`);
					}
				}
				sendSignalToTeacher(io, jsonMSG.lecture_id, 'CHECK_STUDENT_LIST', attendanceLists[jsonMSG.lecture_id]);
				// sendSignalToLecture(io, jsonMSG.lecture_id, 'LECTURE_TEACHER_JOIN', {'reload' : true });
				sendSignalToTeacher(io, jsonMSG.lecture_id, 'LECTURE_CONCENTRATION_CALCULATION', { 'signal': modules.faceRecognitionFlag[`${jsonMSG.lecture_id}`] ? true : false });
			}
		});

		socket.on('LECTURE_ITEM_LOG', async msg => {
			const jsonMSG = JSON.parse(msg);
			await modules.models.lecture_item_response_log.create({
				type: jsonMSG.type,
				lecture_id: jsonMSG.lecture_id,
				item_id: jsonMSG.item_id,
				order: jsonMSG.order,
				student_id: jsonMSG.user_id,
				start_time: jsonMSG.start_time,
				end_time: jsonMSG.end_time
			});
			
			// 문항이나 설문 제출일 경우 
			if ( jsonMSG.type === 0 || jsonMSG.type === 1 ) {
				// 강사가 없을 경우
				if ( io.sockets.adapter.rooms[`lecture_teacher${jsonMSG.lecture_id}`] === undefined ) {
					// 이 경우 임의의 배열에 값을 넣어준 뒤, 강사가 접속했을때 아이템을 넣어준다.
					let user = { 'lecture_id':jsonMSG.lecture_id,  'item_id': jsonMSG.item_id, 'user_id': jsonMSG.user_id, 'type':jsonMSG.type,'answer': jsonMSG.answer };
					offAnswerLists.push(user);
				} else {
					
					// 강사가 강의에 접속하여 있는 경우 ( 유인 , 무인 ( 강사 참여 ))
					const student = attendanceLists[jsonMSG.lecture_id].find(el => el.user_id === jsonMSG.user_id);
					// for ( let i = 0 ; i < slist.length ; i += 1 ) {
					// 	if ( slist[i].user_id == jsonMSG.user_id) {
					// 		var student = slist[i];
					// 	}
					// }
					let user = {  'item_id': jsonMSG.item_id, 'user_id': jsonMSG.user_id, 'name': student.name, 'email_id': student.email_id, 'type':jsonMSG.type,'answer': jsonMSG.answer ,'count':0};
					if ( jsonMSG.lecture_id in answerLists ) {
						const targetIndex = answerLists[jsonMSG.lecture_id].findIndex(el => el.user_id === jsonMSG.user_id && el.item_id === jsonMSG.item_id);
						if (targetIndex > -1) {
							answerLists[jsonMSG.lecture_id][targetIndex].answer = jsonMSG.answer;
							answerLists[jsonMSG.lecture_id].count += 1;
						} else {
							answerLists[jsonMSG.lecture_id].push(user);
						}
					} else {
						answerLists[jsonMSG.lecture_id] = [user];
					}

					sendSignalToTeacher(io, jsonMSG.lecture_id, 'CHECK_ANSWER_LECTURE', answerLists[jsonMSG.lecture_id]); // 강사에게 변경알림을 보냄
					// sendAnswerResult(io,jsonMSG.lecture_id);	// 강사에게 변경알림을 보냄
				}
			}

		});

		socket.on('LECTURE_DELETE_ITEM_LOG', async function(msg) {

			const jsonMSG = JSON.parse(msg);
			let lectureId = jsonMSG.lecture_id;
			let item_ids = jsonMSG.item_ids.split(config.separator);

			let list = 	answerLists[lectureId];
			if (list === undefined) { return; }
			for ( let i = 0 ; i < list.legnth ; i += 1) {
				if(item_ids.includes(list[i].item_id)) {
					list.splice(i,1);
				}
			}
			answerLists[lectureId] = list;


			let logs = await modules.models.lecture_item_response_log.destroy({ where: { item_id: { [modules.models.Sequelize.Op.in] : item_ids }}});
			let items = await modules.models.lecture_item.findAll({ where: { lecture_item_id: { [modules.models.Sequelize.Op.in]: item_ids }}});

			for (let i = 0 ; i < items.length ; i += 1) {
				if (items[i].type === 0) {
					const questions = await modules.models.student_answer_log.destroy({ where: { item_id: items[i] }});
				} else if (items[i].type === 1) {
					const surveys = await modules.models.student_survey.destroy({ where: { item_id: items[i] }})
				} else {

				}
			}
			sendSignalToTeacher(io, jsonMSG.lecture_id, 'CHECK_ANSWER_LECTURE', answerLists[jsonMSG.lecture_id]); // 강사에게 변경알림을 보냄
			// sendAnswerResult(io,jsonMSG.lecture_id);	// 강사에게 변경알림을 보냄
		});

		const onLeaveLectureFuncT = async (jsonMSG) => {
			delete answerLists[jsonMSG.lecture_id];
			// delete attendanceLists[jsonMSG.lecture_id]; 
			// delete offAnswerLists[jsonMSG.lecture_id];
			const group = await modules.models.lecture_item_group.findOne({
				where : { lecture_id : jsonMSG.lecture_id , opened: 1 }
			});
			if ( group ) {
				group.opened = 0;
				await group.save();
				// TODO 유인 종료 처리
				endGroup(group);
				sendSignalToLecture(io, jsonMSG.lecture_id, 'RELOAD_LECTURE_ITEMS', {'reload': true , 'group_id': group.lecture_item_group_id, 'signal' : 0 });
			}

			await modules.models.lecture_item.update({
				opened: 0
			},{
				where: { lecture_id : jsonMSG.lecture_id }
			});
			await modules.models.lecture.update({
				is_auto: 0
			},{
				where: { lecture_id : jsonMSG.lecture_id}
			});
			// sendSignalToLecture(io, jsonMSG.lecture_id, 'TEACHER_EXIT', {'reload': true });
			// // 이해도 학습결과 요청
			// if (lec && lec.type === 0) {
			// 	await coverage.understandingCoverage(jsonMSG.lecture_id, jsonMSG.user_id, 1); // type = 1 : 키워드 계산 요청
			// 	console.log(`${jsonMSG.lecture_id} 참여도 계산 요청 코드 삽입 예정`);						// console.log(`${jsonMSG.lecture_id} 집중도 계산 요청 코드 삽입 예정`);
			// 	console.log(`${jsonMSG.lecture_id} 이해도 계산 요청`);
			// }
			socket.leave(`lecture_teacher${jsonMSG.lecture_id}`);

			// 얼굴인식 강사가 나가도 5분동안 안끊기게
			if (modules.faceTimer[`${jsonMSG.lecture_id}`]) {
				clearTimeout(modules.faceTimer[`${jsonMSG.lecture_id}`]);
				modules.faceTimer[`${jsonMSG.lecture_id}`] = null;
			}
			modules.faceTimer[`${jsonMSG.lecture_id}`] = setTimeout(async () => {
				modules.faceRecognitionFlag[`${jsonMSG.lecture_id}`] = false;
				const lastOne = (await modules.models.face_status.findAll({
					limit: 1,
					where: {
						lecture_id: jsonMSG.lecture_id,
					},
					order: [ [ 'created_at', 'DESC' ]],
				}))[0];
	
				if (lastOne && lastOne.status === 'start') {
					await modules.models.face_status.create({
						lecture_id: jsonMSG.lecture_id,
						status: 'stop',
					});
				}
				sendSignalToLecture(io, jsonMSG.lecture_id, 'LECTURE_CONCENTRATION_CALCULATION', { 'signal': false });
			}, 300000);
		};

		const onLeaveLectureFuncS = async (jsonMSG) => {
			const list = attendanceLists[jsonMSG.lecture_id];
			// if (attendanceListsOneEyeCheck[jsonMSG.lecture_id]) {
			// 	attendanceListsOneEyeCheck[jsonMSG.lecture_id] = attendanceListsOneEyeCheck[jsonMSG.lecture_id].filter(user => user.user_id !== jsonMSG.user_id);
			// }
			if ( list !== undefined ) {
				const student = list.find(element => element.user_id === jsonMSG.user_id);
				student.out_cnt += 1;
				student.current = 0;
				sendSignalToTeacher(io, jsonMSG.lecture_id, 'CHECK_STUDENT_LIST', list)
				// checkStudentList(io,jsonMSG.lecture_id);
			}	
			socket.leave('lecture' + jsonMSG.lecture_id);
			await modules.models.lecture_student_login_log.create({
				lecture_id: jsonMSG.lecture_id,
				student_id: jsonMSG.user_id,
				type: 1
			});
		}

		const onLeaveLectureFunc = async (jsonMSG, role) => {
			if (role === 'teacher') {
				await onLeaveLectureFuncT(jsonMSG);
			} else if (role === 'student') {
				await onLeaveLectureFuncS(jsonMSG);
			}
			delete socket.lecture_id;
			delete socket.role;
			delete socket.user_id;
		}
		
		socket.on('LEAVE_LECTURE', async function(msg){
			const jsonMSG = JSON.parse(msg);
			const bool = await modules.models.user_class.findOne({ where: { user_id : jsonMSG.user_id }});
			await onLeaveLectureFunc(jsonMSG, bool.role);
			console.log(`Leave lecture, lecture_id: ${jsonMSG.lecture_id}, user_id: ${jsonMSG.user_id}, role: ${bool.role}`)
		});

		// 토론 입장 및 퇴장		 
		socket.on('JOIN_DISCUSSION', function(msg){
			const jsonMSG = JSON.parse(msg);
			socket.join('discussion' + jsonMSG.lecture_item_id);
		});

    socket.on('LEAVE_DISCUSSION', function(msg){
      const jsonMSG = JSON.parse(msg);
      socket.leave('discussion' + jsonMSG.lecture_item_id);
		});

		// 실시간 채팅 입장 및 퇴장
		socket.on('JOIN_CHAT', function(msg){
			const jsonMSG = JSON.parse(msg);
			socket.join('chat' + jsonMSG.lecture_id);
		});

    socket.on('LEAVE_CHAT', function(msg){
      const jsonMSG = JSON.parse(msg);
      socket.leave('chat' + jsonMSG.lecture_id);
    });

    socket.on('SEND_NEW_QNA', async (msg) => {
    	const jsonMSG = JSON.parse(msg);
			const { lecture_id, user_id, content } = jsonMSG;

			let qna = await modules.models.real_time_qna.create({
				content,
				lecture_id,
				user_id,
			});
			qna = await modules.models.real_time_qna.findByPk(qna.real_time_qna_id, {
				include: { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
			});
			sendSignalToLecture(io, lecture_id, 'ARRIVE_NEW_QNA', qna);
		});
		socket.on('START_AUTHENTICATION', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			authentication[jsonMSG.lecture_id] = [];
			await modules.models.lecture_authentication_log.create({ lecture_id: jsonMSG.lecture_id, message: jsonMSG.message, during: jsonMSG.during });
			sendSignalToLecture(io, jsonMSG.lecture_id ,'START_AUTHENTICATION', { reload: true, during: jsonMSG.during, message: jsonMSG.message } );
		});

		socket.on('FINISH_AUTHENTICATION', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			authentication[jsonMSG.lecture_id] = [];
			sendSignalToLecture(io, jsonMSG.lecture_id ,'FINISH_AUTHENTICATION', { reload: true } );
		});

		socket.on('SEND_AUTHENTICATION', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			let student_list = attendanceLists[jsonMSG.lecture_id];
			// console.log(`student list - ${JSON.stringify(student_list)}`);
			let student = student_list.find(function(element){
				return element.user_id === jsonMSG.user_id;
			});
			
			// console.log(`cur student - ${JSON.stringify(student)}`);
			await modules.models.lecture_authentication_student_log.create({ lecture_id: jsonMSG.lecture_id, student_id: jsonMSG.user_id, message: jsonMSG.message });
			let obj = {};
			if (!student) { return; }
			obj['user_id'] = student.user_id;
			obj['name'] = student.name;
			obj['message'] = jsonMSG.message;

			let authentication_list = authentication[jsonMSG.lecture_id];
			// if (authentication_list === undefined) {
			// 	authentication_list = [];
			// }
			authentication_list.push(obj)
			authentication[jsonMSG.lecture_id] = authentication_list;

			console.log(`result - ${JSON.stringify(authentication_list)}`);

			sendSignalToTeacher(io, jsonMSG.lecture_id ,'RECV_AUTHENTICATION', authentication_list );
		});
			
		// 새로운 토론 채팅이 왔을때
		socket.on('SEND_NEW_DISCUSSION', async function(msg){
			let is_teacher = 0;
			const jsonMSG = JSON.parse(msg);
			if(jsonMSG.not_save)
			{
				//console.log(jsonMSG)
				sendSignalToDiscussion(io, jsonMSG.lecture_item_id, 'ARRIVE_NEW_DISCUSSION', jsonMSG.discussion);
				return;
			}
				const lectureItem = await modules.models.lecture_item.findByPk(jsonMSG.lecture_item_id, {
					include: {
						model: modules.models.lecture,
					},
					});
					if (!lectureItem) {
					throw modules.errorBuilder.default('Not Found', 404, true);
					}
			const teacherCkeck = await modules.teacherInClass(jsonMSG.user_id, lectureItem.lecture.class_id);
			if(teacherCkeck)
				is_teacher = 1;
			const discussion = await modules.models.discussion.create({
					lecture_item_id: jsonMSG.lecture_item_id,
					user_id: jsonMSG.user_id,
					is_teacher : is_teacher,
					content : jsonMSG.content,
					});
			const user = await modules.models.user.findByPk(jsonMSG.user_id);
			discussion['user'] = user;
			const d = await modules.models.discussion.findByPk(discussion.discussion_id, {
				include: { model: modules.models.user },
			});
			sendSignalToDiscussion(io, jsonMSG.lecture_item_id, 'ARRIVE_NEW_DISCUSSION', d)	
		});

		// 새로운 실시간 채팅이 왔을때
		socket.on('SEND_NEW_CHAT', async function(msg){
			let is_teacher = 0;
			const jsonMSG = JSON.parse(msg);
		
			if(jsonMSG.not_save)
			{
				sendSignalToChat(io, jsonMSG.lecture_id, 'ARRIVE_NEW_CHAT', jsonMSG.chat);
				return;
			}
			const lecture = await modules.models.lecture.findByPk(jsonMSG.lecture_id);
			if (!lecture) {
				throw modules.errorBuilder.default('Not Found', 404, true);
			}
			const teacherCheck = await modules.teacherInClass(jsonMSG.user_id, lecture.class_id);
			if (teacherCheck) is_teacher = 1;
			const chat = await modules.models.lecture_chating.create({
				lecture_id: jsonMSG.lecture_id,
				user_id: jsonMSG.user_id,
				is_teacher : is_teacher,
				content : jsonMSG.content,
			});
			const user = await modules.models.user.findByPk(jsonMSG.user_id);
			chat['user'] = user;
			const d = await modules.models.lecture_chating.findByPk(chat.lecture_chating_id, {
			 	include: { model: modules.models.user },
			});
			sendSignalToChat(io, jsonMSG.lecture_id, 'ARRIVE_NEW_CHAT', d)	
	 	});

		// const ucHeartbeatCount = async (user_id, lecture_id) => {
		// 	const h = await modules.models.heartbeat_count.findOne({
		// 		where: { user_id, lecture_id },
		// 	});
		// 	if (!h) {
		// 		await modules.models.heartbeat_count.create({
		// 			user_id,
		// 			lecture_id,
		// 		});
		// 	} else {
		// 		h.count += 1;
		// 		await h.save();
		// 	}
		// };
		  
		// 강의 페이지 접속중인지에 대한 허트비트, 발생할때마다 실시간 평균 집중도, 참여도, 이해도를 리턴한다.
		// socket.on('HEART_BEAT', async function(msg){
		// 	const jsonMSG = JSON.parse(msg);
		// 	if(typeof jsonMSG.user_id == 'undefined' || typeof jsonMSG.lecture_id == 'undefined' || jsonMSG.lecture_id == null || jsonMSG.user_id == null)
		// 		return;
		// 	await modules.models.realtime_lecture_heartbeat.create(jsonMSG);
		// 	const res = await db.getQueryResult("select avg(participation_score) as avg_participation_score, avg(concentration_score) as avg_concentration_score, avg(understanding_score) as avg_understanding_score from student_lecture_logs where student_lecture_logs.order = 1 and lecture_id = " + jsonMSG.lecture_id)
		// 	const num_spl_log = await db.getQueryResult("select count(*) as num from student_program_detail_logs spl where spl.user_id = " + jsonMSG.user_id +" and TIMESTAMPDIFF(SECOND,  spl.time, NOW()) < 30");
		// 	// const on_program = num_spl_log[0].num > 0 ? true : false 
		// 	const on_program = true
		// 	await ucHeartbeatCount(jsonMSG.user_id, jsonMSG.lecture_id);
			
		// 	if(res[0].avg_participation_score == null)
		// 		res[0].avg_participation_score = 0;
		// 	if(res[0].avg_understanding_score == null)
		// 		res[0].avg_understanding_score = 0;
		// 	if(res[0].avg_concentration_score == null)
		// 		res[0].avg_concentration_score = 0;
		// 	res[0].on_program = on_program;
		// 	sendSignalToLecture(io, jsonMSG.lecture_id, 'GET_REALTIME_STAT', res)
		//   });
		socket.on('LECTURE_CONCENTRATION_CALCULATION_ACTIVATION', async msg => {
			const msgData = JSON.parse(msg);
			
			modules.faceRecognitionFlag[`${msgData.lecture_id}`] = true;

			const lastOne = (await modules.models.face_status.findAll({
				limit: 1,
				where: {
					lecture_id: msgData.lecture_id,
				},
				order: [ [ 'created_at', 'DESC' ]],
			}))[0];
			
			if (!(lastOne && lastOne.status === 'start')) {
				await modules.models.face_status.create({
					lecture_id: msgData.lecture_id,
					status: 'start',
				});
			}

			sendSignalToLecture(io, msgData.lecture_id, 'LECTURE_CONCENTRATION_CALCULATION', { 'signal': true });
		});
		
		socket.on('LECTURE_CONCENTRATION_CALCULATION_DEACTIVATION', async msg => {
			const msgData = JSON.parse(msg);

			modules.faceRecognitionFlag[`${msgData.lecture_id}`] = false;
			const lastOne = (await modules.models.face_status.findAll({
				limit: 1,
				where: {
					lecture_id: msgData.lecture_id,
				},
				order: [ [ 'created_at', 'DESC' ]],
			}))[0];

			if (lastOne && lastOne.status === 'start') {
				await modules.models.face_status.create({
					lecture_id: msgData.lecture_id,
					status: 'stop',
				});
			}
			sendSignalToLecture(io, msgData.lecture_id, 'LECTURE_CONCENTRATION_CALCULATION', { 'signal': false });
		});
		// 강사가 주기적으로 집참이 타임라인 집계를 업데이트하라고 디비에 시켜야됨.
		// 		socket.on('UPDATE_TIMELINE_LOG', async function(msg){
						
		// 			const jsonMSG = JSON.parse(msg);
		// 			const lecture_id = jsonMSG.lecture_id;
		// 			if (typeof jsonMSG.lecture_id == 'undefined' || jsonMSG.lecture_id == null)
		// 				return;
		// 			let avg_hb = 0;
		// 			let std_hb = 0;
		// 			let student_part = [];
		// 			let ck = false;
		// 			//console.log(jsonMSG)			
		// 			let res = await db.getQueryResult("select max(rlh.count) as hb, l.teacher_id from heartbeat_counts rlh left join lectures l on l.lecture_id = rlh.lecture_id where rlh.user_id != l.teacher_id and rlh.lecture_id =" + lecture_id);
		// 			const teacher_hb = res[0].hb;
		// 			var teacher_id = res[0].teacher_id;
		// 			res = await db.getQueryResult(`select rlh.count as hb, rlh.user_id \
		// 			from heartbeat_counts rlh \
		// 			where rlh.user_id != ${teacher_id} and rlh.lecture_id = ${lecture_id}`)
		// 			if(teacher_hb!=0)
		// 			{
		// 				for(var i = 0; i<res.length; i++)
		// 				{
		// 					res[i].participation_score_order1 = res[i].hb / teacher_hb * 100
		// 					if (res[i].participation_score_order1 > 100)
		// 						res[i].participation_score_order1 = 100;
		// 					res[i].participation_score_order0 = 0;
		// 					res[i].participation_score_order2 = 0;
		// 				}
		// 				student_part = res;
		// 			}		
		// 		// 강의 아이템의 order(예복본) 별로 제출할 수 있는 과제의 수를 찾는다.		
		// 			res = await db.getQueryResult(`select count(*) as li, lli.order \
		// 			from lecture_items lli \
		// 			inner join lectures l on l.lecture_id = lli.lecture_id \
		// 			where lli.lecture_id = ${lecture_id} and lli.past_opened = 1 and lli.type != 2 \
		// 			group by lli.order`)
		// 			num_can_item = 0;
		// 			num_can_item_order0 = 0;
		// 			num_can_item_order1 = 0;
		// 			num_can_item_order2 = 0;
		// 			for (var i = 0; i < res.length; i++)
		// 			{
		// 				num_can_item += parseInt(res[i].li);
		// 				switch(res[i].order)
		// 				{
		// 					case 0 :
		// 						num_can_item_order0 += parseInt(res[i].li);
		// 						break;
		// 					case 1 :
		// 						num_can_item_order1 += parseInt(res[i].li);
		// 						break;
		// 					case 2 :
		// 						num_can_item_order2 += parseInt(res[i].li);
		// 						break;
		// 				}
		// 			}
		// 			if(num_can_item==0)
		// 			  return;				
		// // 그 강의아이템 로그에 대해 유저별, 오더별 참여한 횟수를 구한다.					
		// 			res = await db.getQueryResult(`select count(*) as num_item, user_id, li.order from lecture_item_logs lil \
		// 			inner join lecture_items li on li.lecture_id = ${lecture_id} and lil.lecture_item_id = li.lecture_item_id \
		// 			where lil.user_id != ${teacher_id} \
		// 			group by lil.user_id, li.order order by li.order asc`);

		// 			for(var i=0; i<res.length; i++)
		// 			{
		// 			  ck = false;
		// 			  res[i].participation_score_order0 = 0;
		// 			  res[i].participation_score_order1 = 0;
		// 			  res[i].participation_score_order2 = 0;
		// 			  switch(res[i].order)
		// 			  {
		// 				case 0 :
		// 				  res[i].participation_score_order0 = analysis.divideNotZero(res[i].num_item,num_can_item_order0) * 100;
		// 				  if (res[i].participation_score_order0 > 100)
		// 					res[i].participation_score_order0 = 100;
		// 				  break;
		// 				case 1 :
		// 				  res[i].participation_score_order1 = analysis.divideNotZero(res[i].num_item,num_can_item_order1) * 3 * 100; // hb보다 아이템 참여 기록에 비중을 좀더 주기 위해 300을 곱함
		// 				  if (res[i].participation_score_order1 > 300)
		// 					res[i].participation_score_order1 = 300;
		// 				  break;
		// 				case 2 :
		// 				  res[i].participation_score_order2 = analysis.divideNotZero(res[i].num_item,num_can_item_order2) * 100;
		// 				  if (res[i].participation_score_order2 > 100)
		// 					res[i].participation_score_order2 = 100;
		// 				  break;
		// 			  }

		// 			  // hb에서 만들어진 참여도 기록과 비교해서, 있는 기록이면 이에 위에서 구한 참여도 값을 업데이트한다.
		// 			  for(var j=0; j<student_part.length; j++)
		// 			  {
		// 				if(res[i].user_id == student_part[j].user_id)
		// 				{
		// 				  switch(res[i].order)
		// 				  {
		// 					case 0:
		// 					  student_part[j].participation_score_order0 = (res[i].participation_score_order0);
		// 					  break;
		// 					case 1:
		// 					  student_part[j].participation_score_order1 = (student_part[j].participation_score_order1 + res[i].participation_score_order1);
		// 					  break;
		// 					case 2:
		// 					  student_part[j].participation_score_order2 = (res[i].participation_score_order2);
		// 					  break;
		// 				  }
		// 				  ck = true;
		// 				  break;
		// 				}

		// 			  }
		// 			  //없을 경우 새로 추가한다.
		// 			  if(!ck)
		// 				student_part.push(res[i]);
		// 			}




		// 			for(var i = 0; i < student_part.length; i++) 
		// 			{
		// 				if(num_can_item_order1!=0)
		// 					student_part[i].participation_score_order1 = student_part[i].participation_score_order1 / 4
		// 				analysis.updateOrCreate(modules.models.student_lecture_log,
		// 				{'lecture_id': lecture_id,'user_id': student_part[i].user_id, 'order': 0},
		// 				{'lecture_id': lecture_id, 'user_id': student_part[i].user_id, 'order': 0, 'participation_score': student_part[i].participation_score_order0}
		// 				)
		// 				analysis.updateOrCreate(modules.models.student_lecture_log,
		// 				{'lecture_id': lecture_id,'user_id': student_part[i].user_id, 'order': 1},
		// 				{'lecture_id': lecture_id, 'user_id': student_part[i].user_id, 'order': 1, 'participation_score': student_part[i].participation_score_order1}
		// 				)
		// 				analysis.updateOrCreate(modules.models.student_lecture_log,
		// 				{'lecture_id': lecture_id,'user_id': student_part[i].user_id, 'order': 2},
		// 				{'lecture_id': lecture_id, 'user_id': student_part[i].user_id, 'order': 2, 'participation_score': student_part[i].participation_score_order2}
		// 				)
		// 			}
		// 			db.getQueryResult("insert into student_lecture_timeline_logs(understanding_score, participation_score, concentration_score, log_time, user_id, lecture_id) select understanding_score,participation_score,concentration_score, now(), user_id,lecture_id from student_lecture_logs where student_lecture_logs.order = 1 and lecture_id  = "+lecture_id);

		// 		});

		// socket.on('STUDENT_MUST_RELOAD', function(msg){
		// 	const jsonMSG = JSON.parse(msg);
		// 	sendSignalToLecture(io, jsonMSG.lecture_id, 'RELOAD_LECTURE_ITEM', {'reload': true});
		//   });

		// 유인강의 특정 그룹 활성화
		socket.on('LECTURE_GROUP_ACTIVATION', async function(msg){
			const jsonMSG = JSON.parse(msg);
			const lecture_item_group = await modules.models.lecture_item_group.findOne(
				{
					where: {
						lecture_id : jsonMSG.lecture_id,
						lecture_item_group_id: jsonMSG.group_id,
					},
				},
			);
			lecture_item_group.opened = 1;
			await lecture_item_group.save();

			if ((lecture_item_group.start === null || lecture_item_group.end === 0) && !(jsonMSG.group_id in lecture_item_group_status)) {
				lecture_item_group_status[jsonMSG.group_id] = {
					start_time: Date.now(),
					student_logs: {},
					lecture_items: (await Promise.all(lecture_item_group.group_list.split(config.separator).map(list_id => modules.models.lecture_item_list.findByPk(Number(list_id))))).map(item_list => item_list.linked_list.split(config.separator).map(item_id => Number(item_id))).flat(Infinity)
				};
			}
			sendSignalToLecture(io, jsonMSG.lecture_id, 'RELOAD_LECTURE_ITEMS', { 'reload': true , 'group_id': jsonMSG.group_id, 'signal': 1});
		});

		// 유인강의 특정 그룹 비활성화
		socket.on('LECTURE_GROUP_DEACTIVATION', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			await modules.models.lecture_item_group.update(
				{ opened: 0 }, {
					where: {
						lecture_id : jsonMSG.lecture_id,
						lecture_item_group_id: jsonMSG.group_id,
					},
				},
			);
			// TODO 유인 종료 처리

			if (jsonMSG.group_id in lecture_item_group_status) {
				const during = Date.now() - lecture_item_group_status[jsonMSG.group_id].start_time;
				for (let student_id of Object.keys(lecture_item_group_status[jsonMSG.group_id].student_logs)) {
					await Promise.all(lecture_item_group_status[jsonMSG.group_id].student_logs[student_id].map(async (log, index) => {
						let sal = await modules.models.student_answer_log.findOne({
							where: {
								item_id: log.lecture_item_id,
								student_id,
							}
						});

						sal.absolute_reactivity = Number(Math.max(0.6, Math.min(1, ((19 * during / log.lecture_item_number - 10 * (index > 0 ? log.created_at - lecture_item_group_status[jsonMSG.group_id].student_logs[student_id][index - 1].created_at : log.created_at - lecture_item_group_status[jsonMSG.group_id].start_time)) / (15 * during / log.lecture_item_number))).toFixed(2)));
						sal.finalScore = sal.score * sal.absolute_reactivity * (sal.count < 3 ? 1 : sal.count < 4 ? 0.9 : 0.8);
						return sal.save();
					}));
				}
				delete lecture_item_group_status[jsonMSG.group_id];
			}

			sendSignalToLecture(io, jsonMSG.lecture_id, 'RELOAD_LECTURE_ITEMS', { 'reload' : true , 'group_id': jsonMSG.group_id, 'signal': 0});
		});

		socket.on('REQUEST_SERVER_SIDE_ABSOLUTE_REACTIVITY_CALCULATE', async msg => {
			const { lecture_item_id, user_id, lecture_item_number } = JSON.parse(msg);

			for (let group_id of Object.keys(lecture_item_group_status)) {
				if (lecture_item_group_status[group_id].lecture_items.includes(lecture_item_id)) {
					if (!(user_id in lecture_item_group_status[group_id].student_logs)) {
						lecture_item_group_status[group_id].student_logs[user_id] = [];
					}
					lecture_item_group_status[group_id].student_logs[user_id].push({
						lecture_item_id,
						lecture_item_number,
						created_at: Date.now()
					});
				}
			}
		});

		// 유인강의 셔플링 그룹 활성화
		socket.on('LECTURE_TIME_GROUP_ACTIVATION', async function(msg){
			const jsonMSG = JSON.parse(msg);
			await modules.models.lecture_item_group.update(
				{ opened: 1 }, {
					where: {
						lecture_id : jsonMSG.lecture_id,
						lecture_item_group_id: jsonMSG.group_id,
					},
				},
			);
			sendSignalToLecture(io, jsonMSG.lecture_id, 'SHUFFLING_LECTURE_ITEMS', { 'reload': true , 'group_id': jsonMSG.group_id, 'signal': 1 });
		});

		// 유인강의 셔플링 그룹 비활성화 (정상적으로 끝나서 DB에 업데이트만 되는 경우)
		socket.on('LECTURE_TIME_GROUP_DEACTIVATION', async function(msg){
			const jsonMSG = JSON.parse(msg);
			await modules.models.lecture_item_group.update(
				{ opened: 0 }, {
					where: {
						lecture_id : jsonMSG.lecture_id,
						lecture_item_group_id: jsonMSG.group_id,
					},
				},
			);
			// sendSignalToLecture(io, jsonMSG.lecture_id, 'SHUFFLING_LECTURE_ITEMS', { 'reload': true , 'group_id': jsonMSG.group_id, 'signal': 0 });
		});

		// 유인강의 셔플링 그룹 비활성화 (강사가 비정상적으로 나간 상황 / DB 업데이트 + 학생들 화면에서도 내려줘야 함)
		socket.on('LECTURE_TIME_GROUP_DEACTIVATION_FORCE', async function(msg){
			const jsonMSG = JSON.parse(msg);
			await modules.models.lecture_item_group.update(
				{ opened: 0 }, {
					where: {
						lecture_id : jsonMSG.lecture_id,
						lecture_item_group_id: jsonMSG.group_id,
					},
				},
			);
			sendSignalToLecture(io, jsonMSG.lecture_id, 'SHUFFLING_LECTURE_ITEMS', { 'reload': true , 'group_id': jsonMSG.group_id, 'signal': 0 });
		});

		// 강사가 비디오 콜 보냄
		socket.on('VIDEO_CALL', async function(msg){
			const jsonMSG = JSON.parse(msg);
			// console.log(jsonMSG);
			if (jsonMSG.to !== 'teacher') {
				sendSignalToLecture(io, jsonMSG.lecture_id, 'VIDEO_CALL', {
					'lecture_id': jsonMSG.lecture_id,
					'to': jsonMSG.to,
					'sdp': jsonMSG.sdp,
					'candidate': jsonMSG.candidate,
				});
			} else {
				sendSignalToTeacher(io, jsonMSG.lecture_id, 'VIDEO_CALL', {
					'lecture_id': jsonMSG.lecture_id,
					'from': jsonMSG.from,
					'sdp': jsonMSG.sdp,
					'candidate': jsonMSG.candidate,
				});
			}
		});

		// 학생이 스냅샷 보냄
		socket.on('SEND_SNAPSHOT', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			sendSignalToTeacher(io, jsonMSG.lecture_id, 'SEND_SNAPSHOT', {
				'from_socket_id': socket.id,
				'lecture_id': jsonMSG.lecture_id,
				'from': jsonMSG.from,
				'sdp': jsonMSG.sdp,
				'snapshot': jsonMSG.snapshot,
				'name': jsonMSG.name,
			});
			if (jsonMSG.to === 'everyone') {
				sendSignalToLecture(io, jsonMSG.lecture_id, 'SEND_SNAPSHOT', {
					'from_socket_id': socket.id,
					'lecture_id': jsonMSG.lecture_id,
					'from': jsonMSG.from,
					'sdp': jsonMSG.sdp,
					'snapshot': jsonMSG.snapshot,
					'name': jsonMSG.name,
				});
			}
		});

		// 스냅샷 전송 여부 변경
		socket.on('SNAPSHOT_STATUS_CHANGED', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			sendSignalToLecture(io, jsonMSG.lecture_id, 'SNAPSHOT_STATUS_CHANGED', {
				'from_socket_id': socket.id,
				'lecture_id': jsonMSG.lecture_id,
				'from': jsonMSG.from,
				'status': jsonMSG.status,
			});
		});
		

		// 학생이 강사님께 통화 요청함
		socket.on('REQUEST_CALL', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			sendSignalToTeacher(io, jsonMSG.lecture_id, 'REQUEST_CALL', {
				'email_from': jsonMSG.email_from,
				'user_id_from': jsonMSG.user_id_from,
			});
		});

		// 화상 토론 : 통합
		socket.on('DISCUSSION', async function(msg) {
			const jsonMSG = JSON.parse(msg);

			if (jsonMSG.discussion_room_id) { // 방 참여 요청
				// console.log('방 참여 요청');
				socket.join('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id, () => { // socket room에 참여시
					socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION', JSON.stringify({ // room에 속한 모든 이에게 알림 - 나한테 통화 요청해라
						'from_socket_id': socket.id,
						'from_email_id': jsonMSG.from_email_id,
						'from_name': jsonMSG.from_name,
						'call_me': true,
					}));
					// const chat = await modules.models.small_meeting_room_logs.create({ // 디비에 저장하기
					// 	room_id: jsonMSG.discussion_room_id,
					// 	user_id: jsonMSG.user_id,
					// }); 
				});
				socket.emit('DISCUSSION_SPEAK', JSON.stringify({
					speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
					speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
					speaker_name: speaker_name[jsonMSG.discussion_room_id],
					speaker_id: speaker_id[jsonMSG.discussion_room_id],
				})); // 나에게 현재 발언자 정보 제공
				socket.emit('DISCUSSION_SPEAKER', JSON.stringify({
					speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
					speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
					speaker_name: speaker_name[jsonMSG.discussion_room_id],
					speaker_id: speaker_id[jsonMSG.discussion_room_id],
				})); // 나에게 현재 발언자 정보 제공
				socket.emit('DISCUSSION_MASTER', JSON.stringify({
					master_socket_id: master_socket_id[jsonMSG.discussion_room_id],
					master_id: master_id[jsonMSG.discussion_room_id],
					master_name: master_name[jsonMSG.discussion_room_id],
				})); // 나에게 현재 마스터 정보 제공
				return;
			}

			// 기존 참여자가 새 참가자에게 webRTC peerconnection 연결을 시도 / offer 객체 포함, answer 객체 미포함, iceCandidate 객체 미포함
			// 새 참여자가 기존 참여자의 webRTC peerconneciton 연결을 수락  / offer 객체 미포함, answer 객체 포함, iceCandidate 객체 미포함
			// 효율적인 경로 설정 / offer 객체 미포함, answer 객체 미포함, iceCandidate 객체 포함
			if (jsonMSG.offer || jsonMSG.answer || jsonMSG.candidate) {
				socket.broadcast.to(jsonMSG.to_socket_id).emit('DISCUSSION', JSON.stringify({
					from_socket_id: socket.id,
					from_email_id: jsonMSG.from_email_id,
					from_name: jsonMSG.from_name,
					offer: jsonMSG.offer,
					answer: jsonMSG.answer,
					candidate: jsonMSG.candidate,
				}));
				return;
			}

			if (jsonMSG.candidate) {
				console.log('iceCandidate 감지됨');
			}
		});

		// 화상 토론 : 발언자 관련
		socket.on('DISCUSSION_SPEAK', async function(msg) {
			// console.log('@DISCUSSION_SPEAK');
			const jsonMSG = JSON.parse(msg);

			if (jsonMSG.startSpeak) {
				//  console.log('@startSpeak');
				if ( speaker_socket_id[jsonMSG.discussion_room_id] === undefined || speaker_socket_id[jsonMSG.discussion_room_id] === null) {
					speaker_socket_id[jsonMSG.discussion_room_id] = socket.id;
					speaker_email_id[jsonMSG.discussion_room_id] = jsonMSG.from_email_id;
					speaker_name[jsonMSG.discussion_room_id] = jsonMSG.from_name;

					socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_SPEAK', JSON.stringify({
						speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
						speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
						speaker_name: speaker_name[jsonMSG.discussion_room_id],
					}));

					socket.emit('DISCUSSION_SPEAK', JSON.stringify({
						speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
						speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
						speaker_name: speaker_name[jsonMSG.discussion_room_id],
					}));

					speaker_heartbeat[jsonMSG.discussion_room_id] = setTimeout(() => { // 10 초안에 신호 없으면 권한 만료
						console.log('10 sec left');
						speaker_socket_id[jsonMSG.discussion_room_id] = null;
						speaker_email_id[jsonMSG.discussion_room_id] = null;
						speaker_name[jsonMSG.discussion_room_id] = null;
						
						socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_SPEAK', JSON.stringify({
							speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
							speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
							speaker_name: speaker_name[jsonMSG.discussion_room_id],
							endSpeak: true,
						}));

						socket.emit('DISCUSSION_SPEAK', JSON.stringify({
							speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
							speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
							speaker_name: speaker_name[jsonMSG.discussion_room_id],
							endSpeak: true,
						}));
					}, 10000);
				}
				return;
			}

			if (jsonMSG.heartBeat) {
				// console.log('heartBeat!!!');
				clearTimeout(speaker_heartbeat[jsonMSG.discussion_room_id]); // 기존 타이머 해제
				speaker_heartbeat[jsonMSG.discussion_room_id] = setTimeout(() => { // 10초 타이머 재생성
					speaker_socket_id[jsonMSG.discussion_room_id] = null;
					speaker_email_id[jsonMSG.discussion_room_id] = null;
					speaker_name[jsonMSG.discussion_room_id] = null;

					socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_SPEAK', JSON.stringify({
						speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
						speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
						speaker_name: speaker_name[jsonMSG.discussion_room_id],
						endSpeak: true,
					}));
	
					socket.emit('DISCUSSION_SPEAK', JSON.stringify({
						speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
						speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
						speaker_name: speaker_name[jsonMSG.discussion_room_id],
						endSpeak: true,
					}));
				}, 10000);
			}

			if (jsonMSG.endSpeak) {
				//  console.log('@endSpeak');
				clearTimeout(speaker_heartbeat[jsonMSG.discussion_room_id]); // 기존 타이머 해제

				speaker_socket_id[jsonMSG.discussion_room_id] = null;
				speaker_email_id[jsonMSG.discussion_room_id] = null;
				speaker_name[jsonMSG.discussion_room_id] = null;

				socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_SPEAK', JSON.stringify({
					speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
					speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
					speaker_name: speaker_name[jsonMSG.discussion_room_id],
					endSpeak: true,
				}));

				socket.emit('DISCUSSION_SPEAK', JSON.stringify({
					speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
					speaker_email_id: speaker_email_id[jsonMSG.discussion_room_id],
					speaker_name: speaker_name[jsonMSG.discussion_room_id],
					endSpeak: true,
				}));
				return;
			}
		});


		// 화상 토론 : 발언자
		socket.on('DISCUSSION_SPEAKER', async function(msg) {
			const jsonMSG = JSON.parse(msg);

			if (jsonMSG.getRight) {
				if ( speaker_socket_id[jsonMSG.discussion_room_id] === undefined || speaker_socket_id[jsonMSG.discussion_room_id] === null) {
					speaker_socket_id[jsonMSG.discussion_room_id] = socket.id;
					speaker_id[jsonMSG.discussion_room_id] = jsonMSG.user_id;
					speaker_name[jsonMSG.discussion_room_id] = jsonMSG.name;

					io.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_SPEAKER', JSON.stringify({
						speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
						speaker_id: speaker_id[jsonMSG.discussion_room_id],
						speaker_name: speaker_name[jsonMSG.discussion_room_id],
					}));

					speaker_heartbeat[jsonMSG.discussion_room_id] = setTimeout(() => { // 10 초안에 신호 없으면 권한 만료
						io.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_SPEAKER', JSON.stringify({
							speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
							speaker_id: speaker_id[jsonMSG.discussion_room_id],
							end: true,
						}));
						speaker_socket_id[jsonMSG.discussion_room_id] = null;
						speaker_id[jsonMSG.discussion_room_id] = null;

					}, 10000);
				}
				return;
			}

			if (jsonMSG.heartBeat) {
				clearTimeout(speaker_heartbeat[jsonMSG.discussion_room_id]); // 기존 타이머 해제
				speaker_heartbeat[jsonMSG.discussion_room_id] = setTimeout(() => { // 10초 타이머 재생성
					io.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_SPEAKER', JSON.stringify({
						speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
						speaker_id: speaker_id[jsonMSG.discussion_room_id],
						end: true,
					}));
				}, 10000);
			}

			if (jsonMSG.stop) {
				clearTimeout(speaker_heartbeat[jsonMSG.discussion_room_id]); // 기존 타이머 해제

				io.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_SPEAKER', JSON.stringify({
					speaker_socket_id: speaker_socket_id[jsonMSG.discussion_room_id],
					speaker_id: speaker_id[jsonMSG.discussion_room_id],
					end: true,
				}));
				speaker_socket_id[jsonMSG.discussion_room_id] = null;
				speaker_id[jsonMSG.discussion_room_id] = null;

				return;
			}
		});

		// 화상 토론 : 마스터
		socket.on('DISCUSSION_MASTER', async function(msg) {
			const jsonMSG = JSON.parse(msg);

			if (jsonMSG.getRight) {
				if ( master_socket_id[jsonMSG.discussion_room_id] === undefined || master_socket_id[jsonMSG.discussion_room_id] === null) {
					master_socket_id[jsonMSG.discussion_room_id] = socket.id;
					master_id[jsonMSG.discussion_room_id] = jsonMSG.user_id;
					master_name[jsonMSG.discussion_room_id] = jsonMSG.name;

					io.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_MASTER', JSON.stringify({
						master_socket_id: master_socket_id[jsonMSG.discussion_room_id],
						master_id: master_id[jsonMSG.discussion_room_id],
						master_name: master_name[jsonMSG.discussion_room_id],
					}));

					master_heartbeat[jsonMSG.discussion_room_id] = setTimeout(() => { // 10 초안에 신호 없으면 권한 만료
						io.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_MASTER', JSON.stringify({
							master_socket_id: master_socket_id[jsonMSG.discussion_room_id],
							master_id: master_id[jsonMSG.discussion_room_id],
							end: true,
						}));
						master_socket_id[jsonMSG.discussion_room_id] = null;
						master_id[jsonMSG.discussion_room_id] = null;

					}, 10000);
				}
				return;
			}

			if (jsonMSG.heartBeat) {
				clearTimeout(master_heartbeat[jsonMSG.discussion_room_id]); // 기존 타이머 해제
				master_heartbeat[jsonMSG.discussion_room_id] = setTimeout(() => { // 10초 타이머 재생성
					io.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_MASTER', JSON.stringify({
						master_socket_id: master_socket_id[jsonMSG.discussion_room_id],
						master_id: master_id[jsonMSG.discussion_room_id],
						end: true,
					}));
				}, 10000);
			}

			if (jsonMSG.stop) {
				clearTimeout(master_heartbeat[jsonMSG.discussion_room_id]); // 기존 타이머 해제

				io.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('DISCUSSION_MASTER', JSON.stringify({
					master_socket_id: master_socket_id[jsonMSG.discussion_room_id],
					master_id: master_id[jsonMSG.discussion_room_id],
					end: true,
				}));
				master_socket_id[jsonMSG.discussion_room_id] = null;
				master_id[jsonMSG.discussion_room_id] = null;
				master_name[jsonMSG.discussion_room_id] = null;

				return;
			}
		});

		// 화상 토론 : 채팅 
		socket.on('SEND_NEW_CHAT_DISCUSSION', async function(msg){
			console.log('@SEND_NEW_CHAT_DISCUSSION');
			const jsonMSG = JSON.parse(msg);
			// is teacher?
			socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id).emit('ARRIVE_NEW_CHAT_DISCUSSION', JSON.stringify({ // 나 말고 모든이에게 보내기
				is_teacher: false, // mock
				user: jsonMSG.user,
				content: jsonMSG.content,
			}));
			socket.emit('ARRIVE_NEW_CHAT_DISCUSSION', JSON.stringify({ // 나에게 보내기
				is_teacher: false, // mock
				user: jsonMSG.user,
				content: jsonMSG.content,
			})); 

			const chat = await modules.models.small_group_chatting.create({ // 디비에 저장하기
				room_id: jsonMSG.discussion_room_id,
				user_id: jsonMSG.user.user_id,
				content : jsonMSG.content,
			}); 
		});

		// 화상 토론 : 통합
		socket.on('DISCUSSION2', async function(msg) {
			const jsonMSG = JSON.parse(msg);

			if (jsonMSG.join) { // 방 참여 요청
				console.log('@jsonMSG.join');
				socket.discussion = true;
				// 강사인 경우,
				if (jsonMSG.type === 1) { // node number = 0 으로 저장함
					const where = {
						room_id: jsonMSG.discussion_room_id,
						user_id: jsonMSG.user_id,
						node_number: 0,
					}
					await modules.updateOrCreate(modules.models.realtime_lecture_room, where, {
						room_id: jsonMSG.discussion_room_id,
						user_id: jsonMSG.user_id,
						node_number: 0,
						socket_id: socket.id,
						lecture_id: jsonMSG.lecture_id,
					})
					
					const res3 =  await modules.models.realtime_lecture_room.findAll({ // 미리 들어온 1단계 노드 학생들과 연결 시작
						attributes: ['socket_id'],
						where: {
							room_id: jsonMSG.discussion_room_id,
							// node_number: {[modules.models.Sequelize.Op.in]: [1,2,3,4]}, // TODO: 동적 변환, config 파일로 옮겨가기
							node_number: {[modules.models.Sequelize.Op.in]: [1,2]}, // TODO: 동적 변환, config 파일로 옮겨가기
						},
					});
					console.log('res3 = ', res3);
					if (res3) {
						for(let i = 0 ; i < res3.length ; i += 1) {
							socket.emit('DISCUSSION2', JSON.stringify({  // 나에게 자식 소켓 아이디 제공
								from_socket_id: res3[i].socket_id,
								child_socket_id: res3[i].socket_id,
							}));
						}
					}
					console.log('before sendSignalToLecture');
					sendSignalToLecture(io, jsonMSG.lecture_id, 'BROADCAST_STARTS', {'message' : 'hi' }); // 학생들에게 실시간 강의 시작했음을 알림
				} else { // 이후 참가자인 경우

					// node_number 중에서 가장 빠른 빈 자리가 어딘지 확인
					const res =  await modules.models.realtime_lecture_room.findAll({
						attributes: ['node_number'],
						where: {
							room_id: jsonMSG.discussion_room_id,
						},
						order: [['node_number', 'DESC']],
					});

					let node_numbers = [];
					if (res) {
						res.forEach((x) => {
							node_numbers.push(x.node_number);
						})
					}

					let firstEmptySlot = 1; // 1부터 가장 빠른 빈 슬롯이 어디있는지 탐색

					console.log('before while true // node_numbers = ', node_numbers);
					while (true) {
						if ( !node_numbers.find(element => element === firstEmptySlot)) {
							break;
						}
						firstEmptySlot += 1;
					}

					// 탐색한 node_number를 부여함.
					await modules.models.realtime_lecture_room.create({
						room_id: jsonMSG.discussion_room_id,
						user_id: jsonMSG.user_id,
						node_number: firstEmptySlot,
						socket_id: socket.id,
					});

					const parent_node_number = findParent(2, firstEmptySlot); // TODO: config 파일로 빼내기

					// 연결해야할 socket_id를 탐색
					const res2 =  await modules.models.realtime_lecture_room.findOne({
						attributes: ['socket_id', 'node_number'],
						where: {
							room_id: jsonMSG.discussion_room_id,
							node_number: parent_node_number,
						},
					});

					if(res2) {
						console.log('res2.node_number = ', res2.node_number);
					}

					if (res2) { // 부모가 접속해있으면, 부모에게 자식 소켓 아이디를 제공
						socket.broadcast.to(res2.socket_id).emit('DISCUSSION2', JSON.stringify({
							from_socket_id: socket.id,
							child_socket_id: socket.id,
						}));
					}

					socket.emit('DISCUSSION2', JSON.stringify({ // 나에게 디버깅용 정보 보내기
						debug: true,
						socket_id: socket.id,
						node_number: firstEmptySlot,
					}));
				}
				return;
			}

			// 기존 참여자가 새 참가자에게 webRTC peerconnection 연결을 시도 / offer 객체 포함, answer 객체 미포함, iceCandidate 객체 미포함
			// 새 참여자가 기존 참여자의 webRTC peerconneciton 연결을 수락  / offer 객체 미포함, answer 객체 포함, iceCandidate 객체 미포함
			// 효율적인 경로 설정 / offer 객체 미포함, answer 객체 미포함, iceCandidate 객체 포함
			if (jsonMSG.offer || jsonMSG.answer || jsonMSG.candidate) {
				socket.broadcast.to(jsonMSG.to_socket_id).emit('DISCUSSION2', JSON.stringify({
					from_socket_id: socket.id,
					offer: jsonMSG.offer,
					answer: jsonMSG.answer,
					candidate: jsonMSG.candidate,
				}));
				return;
			}

			if (jsonMSG.leave) {
				const res = await modules.models.realtime_lecture_room.findOne({
					where: {
						socket_id: socket.id,
					},
				});
				if ( res.node_number === 0 ) {
					sendSignalToLecture(io, res.lecture_id, 'BROADCAST_ENDS', {'message' : 'bye' }); // 학생들에게 실시간 강의 종료됨을 알림
				}

				await modules.models.realtime_lecture_room.destroy({
					where: {
						// room_id: jsonMSG.discussion_room_id,
						socket_id: socket.id,
					},
				});
			}

			if (jsonMSG.candidate) {
				console.log('iceCandidate 감지됨');
			}
		});

		// 화상 토론 : lite
		socket.on('DISCUSSION3', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			// console.log('@DISCUSSION3');

			switch (jsonMSG.action) {
				case 'join': {
					socket.join('VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id, async () => {
						console.log(jsonMSG.user_id + ' 번 유저가 VIDEO_DISCUSSION_ROOM_'+jsonMSG.discussion_room_id+' 방에 참여하였습니다.');
						const res1 = await modules.models.small_meeting_room_current.findAll({
							attributes: ['user_id', 'socket_id', 'is_master'],
							where: {
								room_id: jsonMSG.discussion_room_id,
							}
						});
						const userIdList = res1.map(element => element.dataValues.user_id);

						const userList = await modules.models.user.findAll({
							attributes: ['user_id', 'email_id', 'name'],
							where : { user_id: {in: userIdList}},
						});
						socket.emit('DISCUSSION3', JSON.stringify({
							action: 'setUserList',
							userList,
						})); //현재 접속중인 유저 목록 나에게 보내기


						// res1.forEach(element => { //현재 마스터가 있는 경우, 나에게 보내기 - 마스터가 보내야 할 듯
						// 	if(element.is_master === true) {
						// 		socket.emit('DISCUSSION3', JSON.stringify({
						// 			action: 'setMaster',
						// 			user_id: element.user_id,
						// 			socket_id: element.socket_id,
						// 		}));
						// 	}
						// });

						let user = await modules.models.user.findOne({
							attributes: ['user_id', 'email_id', 'name'],
							where : { user_id: jsonMSG.user_id }}
						);

						socket.user_id = user.user_id,
						socket.email_id = user.email_id,
						socket.name = user.name,
						socket.discussion_room_id = jsonMSG.discussion_room_id;
		
						await modules.models.small_meeting_room_current.create({
							room_id: jsonMSG.discussion_room_id,
							user_id: socket.user_id,
							socket_id: socket.id,
						});

						socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+socket.discussion_room_id).emit('DISCUSSION3', JSON.stringify({ // 나를 제외한 토론방에 속한 모두에게 보냄
							action: 'join',
							user_id: socket.user_id,
							email_id: socket.email_id,
							name: socket.name,
							socket_id: socket.id,
						}));
					});
					break;
				}
				case 'leave': {
					socket.leave('VIDEO_DISCUSSION_ROOM_'+socket.discussion_room_id, async () => {
						console.log(socket.user_id + ' 번 유저가 VIDEO_DISCUSSION_ROOM_'+socket.discussion_room_id+' 방을 떠났습니다.');		
						await modules.models.small_meeting_room_current.destroy({
							where: {
								room_id: socket.discussion_room_id,
								user_id: socket.user_id,
							}
						});
						socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+socket.discussion_room_id).emit('DISCUSSION3', JSON.stringify({ // 나를 제외한 토론방에 속한 모두에게 보냄
							action: 'leave',
							user_id: socket.user_id,
							email_id: socket.email_id,
							name: socket.name,
						}));
					});
					break;
				}
				case 'snapshot': {
					// console.log('@snapshot');
					socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+socket.discussion_room_id).emit('DISCUSSION3', JSON.stringify({ // 나를 제외한 토론방에 속한 모두에게 보냄
						action: 'snapshot',
						user_id: socket.user_id,
						email_id: socket.email_id,
						name: socket.name,
						snapshot: jsonMSG.snapshot,
					}));
					break;
				}
				case 'getMaster': {
					await modules.models.small_meeting_room_current.update({
						is_master: false,
					}, {
						where: {
							room_id: socket.discussion_room_id,
						},
					});
					await modules.models.small_meeting_room_current.update({
						is_master: true,
					}, {
						where: {
							room_id: socket.discussion_room_id,
							user_id: socket.user_id,
						},
					});
					socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+socket.discussion_room_id).emit('DISCUSSION3', JSON.stringify({ // 나를 제외한 토론방에 속한 모두에게 보냄
						action: 'setMaster',
						user_id: socket.user_id,
						socket_id: socket.id,
					}));
					socket.emit('DISCUSSION3', JSON.stringify({
						action: 'setMaster',
						user_id: socket.user_id,
						socket_id: socket.id,
					}));
					break;
				}
				case 'callMe': { // 마스터가 이미 들어와 있는데, 새 참가자가 온 경우
					console.log('@callMe');
					socket.broadcast.to(jsonMSG.socket_id).emit('DISCUSSION3', JSON.stringify({ // 대상에게 보냄
						action: 'setMaster',
						user_id: socket.user_id,
						socket_id: socket.id,
					}));
					break;
				}
				case 'getSpeaker': {
					await modules.models.small_meeting_room_current.update({
						is_speaker: false,
					}, {
						where: {
							room_id: socket.discussion_room_id,
						},
					});
					await modules.models.small_meeting_room_current.update({
						is_speaker: true,
					}, {
						where: {
							room_id: socket.discussion_room_id,
							user_id: socket.user_id,
						},
					});
					socket.broadcast.to('VIDEO_DISCUSSION_ROOM_'+socket.discussion_room_id).emit('DISCUSSION3', JSON.stringify({ // 나를 제외한 토론방에 속한 모두에게 보냄
						action: 'setSpeaker',
						user_id: socket.user_id,
						socket_id: socket.id,
						name: socket.name,
					}));
					socket.emit('DISCUSSION3', JSON.stringify({
						action: 'setSpeaker',
						user_id: socket.user_id,
						socket_id: socket.id,
						name: socket.name,
					}));
					break;
				}
				case 'candidate' : { console.log('iceCandidate 감지됨'); }
				case 'offer' :
				case 'answer' : {
					console.log('jsonMSG.action = ', jsonMSG.action);
					// 기존 참여자가 새 참가자에게 webRTC peerconnection 연결을 시도 / offer 객체 포함, answer 객체 미포함, iceCandidate 객체 미포함
					// 새 참여자가 기존 참여자의 webRTC peerconneciton 연결을 수락  / offer 객체 미포함, answer 객체 포함, iceCandidate 객체 미포함
					// 효율적인 경로 설정 / offer 객체 미포함, answer 객체 미포함, iceCandidate 객체 포함
					socket.broadcast.to(jsonMSG.socket_id).emit('DISCUSSION3', JSON.stringify({
						socket_id: socket.id,
						user_id: socket.user_id,
						offer: jsonMSG.offer,
						answer: jsonMSG.answer,
						candidate: jsonMSG.candidate,
						action: jsonMSG.action,
					}));
					break;
				}
			}
		});

		// 강의방
		socket.on('DISCUSSION22', async function(msg) {
			const jsonMSG = JSON.parse(msg);


			if (jsonMSG.join) { // 방 참여 요청
				socket.discussion = true;
					await modules.models.small_meeting_room_current.destroy({ // 이미 참여중이면 삭제
						where: {
							socket_id: socket.id,
						}
					});
					if(jsonMSG.is_teacher) { // 강사인 경우, 0번이 비었다면 0번 배정
						const res1 = await modules.models.small_meeting_room_current.findAll({
							attributes: ['connection_order'],
							where: {
								room_id: jsonMSG.discussion_room_id,
								connection_order: 0,
							}
						});
						if (res1.length == 0) {
							// console.log('강사로 접속했으며, 0번이 비었음');
							await modules.models.small_meeting_room_current.create({
								room_id: jsonMSG.discussion_room_id,
								user_id: jsonMSG.user_id,
								socket_id: socket.id,
								connection_order: 0,
							});

							socket.emit('DISCUSSION22', JSON.stringify({ // 나에게 정보 리턴
								connection_order: 0, // 내 연결 번호 (0 = 마스터)
								parent_order: -1,
								self_return: true,
							}));
							return;
						}
					}
					// 학생인 경우 또는 0번이 이미 찬 경우, 1부터 빈자리 탐색 후 빠른번호 배정
					// 기존 참가자의 connection_order를 탐색하고, 비어있는 숫자중 가장 빠른 숫자를 찾는다.
					const res2 = await modules.models.small_meeting_room_current.findAll({
						attributes: ['connection_order'],
						order: [['connection_order', 'ASC']],
						where: {
							room_id: jsonMSG.discussion_room_id,
							connection_order:{
								gt:0
							}
						}
					});
					const connection_order_list = res2.map(x => x.connection_order);

					let next_connection_order = -1;
					for(let i=0;i<1000;i++){
						if(connection_order_list[i] !== i+1) {
							next_connection_order = i+1;
							break;
						}
					}

					if (next_connection_order == -1) {
						console.log('1000 명 이상은 들어갈 수 없습니다.');
						return;
					}

					// TODO: 낮은 확률로 동시 접속시 처리
					await modules.models.small_meeting_room_current.create({
						room_id: jsonMSG.discussion_room_id,
						user_id: jsonMSG.user_id,
						socket_id: socket.id,
						connection_order: next_connection_order,
					});

					// 부모 찾기. order값을 repeat_size로 나눈다. 나머지가 있으면 버리고, 나머지가 없으면 -1 한다.
					let parent_order = parseInt(next_connection_order / DISCUSSION_LECTURE_ROOM_REPEAT_SIZE);

					const remain = next_connection_order % DISCUSSION_LECTURE_ROOM_REPEAT_SIZE;
					if( remain == 0 ) {
						parent_order -= 1;
					}

					socket.emit('DISCUSSION22', JSON.stringify({ // 나에게 정보 리턴
						connection_order: next_connection_order, // 내 연결 번호
						parent_order, // 부모 연결 번호
						self_return: true,
					}));

					return;
			}

			if (jsonMSG.request_connection) { // 연결 요청
				console.log('jsonMSG.parent_order = ', jsonMSG.parent_order);
				console.log('jsonMSG.discussion_room_id = ', jsonMSG.discussion_room_id);
				const res3 = await modules.models.small_meeting_room_current.findOne({
					attributes: ['socket_id'],
					where: {
						room_id: jsonMSG.discussion_room_id,
						connection_order: jsonMSG.parent_order,
					}
				});
				if (res3) {
					socket.broadcast.to(res3.socket_id).emit('DISCUSSION22', JSON.stringify({ // 부모에게 통화 요청
						'from_socket_id': socket.id,
						// 'from_email_id': jsonMSG.from_email_id,
						// 'from_name': jsonMSG.from_name,
						'call_me': true,
					}));
					return;
				} else {
					console.log('parent is empty');
					socket.emit('DISCUSSION22', JSON.stringify({ // 부모 노드가 알 수 없는 이유로 비어있는 경우 (나에게 리턴)
						from_socket_id: socket.id,
						no_parent: true,
					}));
					return;
				}
			}

			if (jsonMSG.stream_not_prepared) {
				socket.broadcast.to(jsonMSG.to_socket_id).emit('DISCUSSION22', JSON.stringify({ // 연결 받을 준비가 안 된 경우
					'from_socket_id': socket.id,
					'stream_not_prepared': true,
				}));
			}

			if (jsonMSG.reconnect) {
				console.log('@reconnect');

				const res3 = await modules.models.small_meeting_room_current.findOne({
					attributes: ['connection_order'],
					where: {
						socket_id: socket.id,
					}
				});

				const connection_order = res3.connection_order;

				// 자식 찾기. repeat_size를 곱하고, 1, 2, 3 ... repeat_size 까지 더한다.
				const standard = parseInt(connection_order * DISCUSSION_LECTURE_ROOM_REPEAT_SIZE);
				let child_order_list = [];
				for (let i = 0; i < DISCUSSION_LECTURE_ROOM_REPEAT_SIZE ; i+=1) {
					child_order_list.push(standard + 1 + i);
				}

				const res4 = await modules.models.small_meeting_room_current.findAll({
					attributes: ['socket_id'],
					where: {
						room_id: jsonMSG.discussion_room_id,
						connection_order: {in: child_order_list},
					}
				});

				const child_socket_id_list = res4.map(x => x.socket_id);

				child_socket_id_list.forEach((socket_id) => {
					socket.broadcast.to(socket_id).emit('DISCUSSION22', JSON.stringify({ // 자식에게 새 연결 요청
						'from_socket_id': socket.id,
						'reconnect': true,
					}));
				})
			}

			// 기존 참여자가 새 참가자에게 webRTC peerconnection 연결을 시도 / offer 객체 포함, answer 객체 미포함, iceCandidate 객체 미포함
			// 새 참여자가 기존 참여자의 webRTC peerconneciton 연결을 수락  / offer 객체 미포함, answer 객체 포함, iceCandidate 객체 미포함
			// 효율적인 경로 설정 / offer 객체 미포함, answer 객체 미포함, iceCandidate 객체 포함
			if (jsonMSG.offer || jsonMSG.answer || jsonMSG.candidate) {
				socket.broadcast.to(jsonMSG.to_socket_id).emit('DISCUSSION22', JSON.stringify({
					from_socket_id: socket.id,
					from_email_id: jsonMSG.from_email_id,
					from_name: jsonMSG.from_name,
					offer: jsonMSG.offer,
					answer: jsonMSG.answer,
					candidate: jsonMSG.candidate,
				}));
				return;
			}

			if (jsonMSG.candidate) {
				console.log('iceCandidate 감지됨');
			}
		});

		// 스냅샷 방
		socket.on('SNAPSHOT_ROOM', async function(msg) {
			const jsonMSG = JSON.parse(msg);
			console.log('echo1');
			if (jsonMSG.action == 'join') {
				console.log('echo2');
				socket.join('SNAPSHOT_ROOM_'+jsonMSG.snapshot_room_id);
				return;
			}
		});

		socket.on('STUDENT_JOIN_ALLOW', async function(msg){
			const jsonMSG = JSON.parse(msg);
			console.log('student_join_allow')
			await modules.models.lecture_student_login_log.update({
				type: 2
			},{
				where: {
					student_id: jsonMSG.user_id,
					lecture_id: jsonMSG.lecture_id,
					type: 0
				}
			})
		})

		socket.on('REAL_TIME_INFO', async function(msg){
			const jsonMSG = JSON.parse(msg);

			// 학생 답안 이해도를 위한 쿼리
			let answerSQL = `select item_id, 0 as \`type\` ,student_id, answer, ratio, created_at from student_answer_logs WHERE lecture_id = ${jsonMSG.lecture_id}`; // 학생들 제출답안
			let surveySQL = `select item_id, 1 as \`type\`,student_id, answer, ratio, created_at from student_surveys WHERE lecture_id = ${jsonMSG.lecture_id}`;
			let result1 = await db.getQueryResult(answerSQL);
			let result2 = await db.getQueryResult(surveySQL);
			if ( result1.length == 0 ) {
				result1 = [];
			}
			for ( let i = 0 ; i < result2.length ; i += 1 ) {
				result1.push(result2[i]);
			}
		
			let logSql = `select item_id ,\`type\`,student_id, sum(TIME_TO_SEC(TIMEDIFF(end_time, start_time))) as time_sum from lecture_item_response_logs where lecture_id = ${jsonMSG.lecture_id} group by item_id, student_id, \`type\`  order by item_id,student_id `;
			let logResult = await db.getQueryResult(logSql);
			io.sockets.in('lecture_teacher'+jsonMSG.lecture_id).emit('REAL_TIME_INFO', JSON.stringify({ 'understanding': result1, 'response_time': logResult }));
		});
	
		socket.on('disconnect', async () => {
			if (socket.role) {
				console.log(`disconnected, lecture_id: ${socket.lecture_id}, user_id: ${socket.user_id}, role: ${socket.role}`);
				onLeaveLectureFunc({
					user_id: socket.user_id,
					lecture_id: socket.lecture_id,
				}, socket.role);
			}

			if (socket.discussion) {
				console.log('discussion socket disconnected');
				await modules.models.small_meeting_room_current.destroy({
					where: {
						socket_id: socket.id,
					},
				});
			}
		});
		console.log('connected');
		// console.log('socket.id = ', socket.id);
		socket.emit('CONNECTED');
	});


	router.get('/socket_front', modules.asyncWrapper(async (req, res, next) => {

		res.render('socket.html')

	}));

  return router;
}

// ************
// 입력 : 부모당 몇개의 자식이 붙을지
// 입력 : 자식 노드 node_number
// 출력 : 부모 노드 node_number
// ************
function findParent(dimension, child_node_number) {
	
	// 사전 준비 - 사용할 숫자들 배열에 넣음, 단 가장 큰 숫자가 먼저오고, 이후에 나머지 숫자를 넣음
	let components = [];
	components.push(dimension);
	let i = dimension;
	for (i = 1 ; i < dimension ; i += 1) {
		components.push(i);
	}

	// n진수 형태와 약간 다른 중간 산출물을 구함. 0이 사용되지 않고, n진법인 경우 n이 대신 들어가는 형태임
	let r;
	let answer = '';
	while( child_node_number > 0 ) {
		r = child_node_number % dimension;
		answer = components[r] + answer;
				child_node_number = Math.floor(child_node_number / dimension);
		if (r == 0) --child_node_number;
	}
	// 그 중에, 맨 오른쪽 한글자를 빼서 남은 수를 부모로 판단
	let parentString = answer.slice(0, answer.length - 1);

	// 이렇게 구한 수를 다시 십진수로 돌림
	let parentDecimal = 0;
	for( let i = 0 ; i < parentString.length ; i += 1) {
		parentDecimal += parentString[i] * Math.pow(dimension, parentString.length - i - 1);
	}
	return parentDecimal;
}