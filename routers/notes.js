const express = require('express');
const router = express.Router();
const path = require('path');
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next ) => {

    res.json({ success : true });
}));
router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next ) => {

  const { authId, authType } = req.decoded;
  const { lectureItemId } = req.body;

  const sql = `INSERT INTO notes (note_type, lecture_item_id, created_at, updated_at) VALUES(0, ${lectureItemId}, NOW(), NOW())`;
  const result = await db.getQueryResult(sql);
  if (result) {
    res.json({ success: true, note_id: result.insertId });
  }
  else {
    res.json({ success: false });
  }
}));
router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next ) => {

  const noteId = parseInt(req.params.id, 10);
  const { url, youtube_interval, note_type } = req.body;
  let sql = 'UPDATE notes SET ';
  if (note_type !== undefined) sql += `note_type = '${note_type}' `;
  if (url !== undefined) sql += `, url = '${url}' `;
  if (youtube_interval !== undefined) sql += `, youtube_interval = '${youtube_interval}' `;

  sql += `WHERE note_id =${noteId}`;
  const result = await db.getQueryResult(sql);
  if (result.affectedRows !== 0) {
    res.json({ success: true });
  }
  else {
    res.json({ success: false });
  }
}));
router.post('/:id/file', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res) => {
  const file = await modules.upload.fileAdd(req, 'note_id', parseInt(req.params.id, 10));
  res.json({ success: true, file });
}));

const keywordUpsert = async (values, condition) => modules.models
  .note_keyword.findOne({ where: condition })
  .then((obj) => {
    if (obj) {
      return obj.update(values);
    } else {
      return modules.models.note_keyword.create(values);
    }
});
router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const noteId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const note = await modules.models.note.findByPk(noteId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
        include: {
          model: modules.models.class,
        },
      },
    },
  });
  if (!note) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, note.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  let scoreSum = 0;
  const dataLen = data.length;
  for (let i = 0; i < dataLen; i += 1) {
    scoreSum += parseInt(data[i].score, 10);
  }
  const promises = [];
  for (let i = 0; i < dataLen; i += 1) {
    promises.push(keywordUpsert(
      { note_id: note.note_id, lecture_id: note.lecture_item.lecture_id, lecture_item_id: note.lecture_item_id, keyword: data[i].keyword, score_portion: data[i].score },
      { note_id: note.note_id, keyword: data[i].keyword },
    ));
  }
  Promise.all(promises);
  note.score = scoreSum;
  await note.save();

  res.json({ success: true, score: scoreSum });

  await modules.coverageCalculator.coverageAll(note.lecture_item.lecture_id, note.lecture_item.lecture.class_id);
}));

router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const noteId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const noteKeywords = (await modules.models.note.findByPk(noteId, {
    include: { model: modules.models.note_keyword },
  })).note_keywords;

  if (!noteKeywords) {
    throw modules.errorBuilder.default('Not Found -', 404, true);
  }

  res.json(noteKeywords);
}));

router.delete('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const noteId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const note = await modules.models.note.findByPk(noteId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!note) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, note.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.note_keyword.destroy({
    where: {
      note_id: noteId,
    },
  });

  res.json({ success: true });
}));

router.get('/file/:id', modules.asyncWrapper(async(req, res, next) => {

  const { authId, authType } = req.decoded
  const noteId = parseInt(req.params.id, 10);

  await modules.models.downloadlog.created({
    type: 0,
    user_id: authId,
    role: authType,
    type: 0
  });
  const file = await modules.modesls.file.findOne({
    where : {
      note_id: noteId
    }
  });
  res.json(file);
}));

module.exports = router;
