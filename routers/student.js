const express = require('express');
const moment = require('moment');
const router = express.Router();
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const scoreing = require('../modules/scoreing');
const models =  require('../models');
const config = require('../config');
const s3upload = require('../modules/s3upload');

const multer = require('multer');
const storage = multer.memoryStorage();
var upload = multer({ storage : storage });

router.post('/answer', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const data = req.body;
  const q = await models.question.findByPk(data.questionId, {
    include: {
      model: models.lecture_item,
      include: {
        model: models.lecture,
      },
    },
  });

  const i = q.lecture_item;


  const records = await models.student_answer_log.findAll({
    // attributes: ['student_answer_log_id'],
    where: {
      question_id: data.questionId,
      student_id: authId,
      valid: true,
    },
  });

  ////////////////////////////////////////////////////////////////////
  var submittedSolution = null;
  if (records.length === 0 ) {
    // 처음 답안 제출 시
    submittedSolution = await scoreing.submitHandling(authId, data);
  } else {
    // 이미 제출한 기록이 있을 시
    submittedSolution = await scoreing.updateHnadling(authId, data);
  }
  ////////////////////////////////////////////////////////////////////
  submittedSolution.valid = true;
  await submittedSolution.save();
//   if (records.length === 1) {
//     await models.student_answer_log.update({ valid: false }, {
//       field: ['valid'],
//       where: {
//         student_answer_log_id: records[0].student_answer_log_id,
//       },
//     });
//   } else if (records.length !== 0 && records.length !== 1) {
//     throw modules.errorBuilder.default(
//       `In 'student_answer_log' table there's more than
//  one 'valid' data which is true for each combination of student_id and question_id. It's supposed to have only one.`,
//       403, true,
//     );
//   }
  
  res.json({ success: true, student_answer_log_id: submittedSolution.dataValues.student_answer_log_id});

  // scoreing.all(authId, submittedSolution.lecture_id, submittedSolution.class_id, submittedSolution.order);
}), modules.asyncWrapper(async (req, res, next) => { // 활성화시간이 아니거나 닫혀있을때
  throw modules.errorBuilder.default('Expired', 403, true);

}));

router.post('/answer/multimedia', modules.auth.tokenCheck, modules.upload.upload,
  modules.asyncWrapper(async (req, res, next) => {
    const file = await modules.upload.fileAdd(req, 'question_id_multichoice', parseInt(req.params.id, 10));
    res.json({ success: true, file, file2: req.file });
  }));

router.delete('/answer/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId , authType } = req.decoded;
  const id = parseInt(req.params.id);
  if ( authType === 0 ) { 
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  } else {
    const sql = `DELETE FROM student_answer_logs WHERE student_id = ${authId} and question_id = ${id}`;
    await db.getQueryResult(sql);
  }
  res.json({ success: true });
}));
router.post('/answer/:id/imagefile', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const file = await modules.upload.fileAdd(req, 'student_answer_log_id', parseInt(req.params.id, 10));
  await models.student_answer_log.update({
    answer: file.client_path,
  }, { where: { student_answer_log_id: req.params.id } });
  res.json({ success: true, file });
}));
router.post('/answer/:id/voicefile', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const file = await modules.upload.fileAdd(req, 'student_answer_log_id', parseInt(req.params.id, 10));
  await models.student_answer_log.update({
    answer: file.file_guid,
  }, { where: { student_answer_log_id: req.params.id } });
  res.json({ success: true, file });
}));
router.get('/homework/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const homeworkId = parseInt(req.params.id, 10)

  const submit = await modules.models.student_homework.findOne({
    where: { homework_id: homeworkId, student_id: authId },
    include: { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
  });
  if (!submit) {
    res.json({ comment: '', file: [], feedback: '' });
  }
  res.json(submit);
}));

router.post('/homework/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const homeworkId = parseInt(req.params.id, 10);
  const { comment } = req.body;

  let submit = await modules.models.student_homework.findOne({
    where: { homework_id: homeworkId, student_id: authId },
  });
  if (!submit) {
    submit = await modules.models.student_homework.create({
      homework_id: homeworkId, student_id: authId,
    });
  }
  submit.comment = comment;
  await submit.save();

  res.json({ success: true, submit_id: submit.student_homework_id });
}));

router.post('/homework/:id/file', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const homeworkId = parseInt(req.params.id, 10);
  let submit = await modules.models.student_homework.findOne({
    where: { homework_id: homeworkId, student_id: authId },
  });
  if (!submit) {
    submit = await modules.models.student_homework.create({
      homework_id: homeworkId, student_id: authId,
    });
  }

  const file = await modules.upload.fileAdd(req, 'student_homework_id', submit.student_homework_id);
  res.json({ success: true, file, submit_id: submit.student_homework_id });
}));

router.put('/answer/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const logId = parseInt(req.params.id, 10);
  const { score } = req.body;
  const log = await modules.models.student_answer_log.findByPk(logId);
  const question = await modules.models.question.findByPk(log.question_id);
  // 2021-08-26 다운이가 만든 TransScore 사용 안하니까 0으로 박아버림  
  // log.transScore = log.transScore / log.score * score;
  log.transScore = 0;
  
  log.score = score;
  log.ratio = score / question.score;
  
  //2021-08-26 강사가 이해도를 수정하면 finalScore를 다시 계산해야한다.
  // finalScore = 이해도(10) * 집중도 *  신뢰도(count가 1,2 = 1, count가 3 = 0.9, count 4 = 0.8, count 5이상 = 0.6)
  truthLevel = 0 
  if (log.count == 1 || log.count == 2)   truthLevel = 1;
  else if (log.count == 3) truthLevel = 0.9;
  else if (log.count == 4) truthLevel = 0.85;
  else truthLevel = 0.8;

  // 최종점수 = 이해도 * 반응도 * 신뢰도
  //log.finalScore = log.score * log.concentration * truthLevel;

  //2021-09-02 상대 반응도를 사용하지 않고 절대 반응도 사용
  log.finalScore = log.score * log.absolute_reactivity * truthLevel;
  
  await log.save();

  res.json({ success: true });

  // scoreing.all(log.student_id, log.lecture_id, log.class_id, log.order);
}));

router.put('/homework/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res ,next) => {
  const id = parseInt(req.params.id, 10);
  const { feedback } = req.body;
  const studentHomework = await modules.models.student_homework.findByPk(id);

  studentHomework.feedback = feedback;
  await studentHomework.save();

  res.json({ success: true });
}));

router.post('/survey/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId } = req.decoded;
  const { answer, lecture_id, item_id, order, type } = req.body;

  var an = answer.join(modules.config.separator);
  let submit = await modules.models.student_survey.findOne({
    where: { survey_id: id, student_id: authId },
  });

  let survey = await modules.models.survey.findOne({
    where : { survey_id : id }
  });
 
  if (!submit) {

    if ( type === 0 ) {
      an = parseFloat(an);
      var ratio = ( an + 1 ) / 5;
      var score = survey.score * ratio;
      submit = await modules.models.student_survey.create({
        survey_id: id, student_id: authId,
        order , lecture_id, item_id, score, ratio, type, count : 0
      });
    } else {
      submit = await modules.models.student_survey.create({
        survey_id: id, student_id: authId,
        order, lecture_id, item_id, type, count : 0
      });
    }
     
  } else {

    if ( type === 0 ) {
      an = parseFloat(an);
      var ratio = ( an + 1 ) / 5;
      var score = survey.score * ratio;
      submit.score = score;
      submit.ratio = ratio;
    } else { }
    submit.count += 1;

  }
  submit.answer = an;
  await submit.save();

  res.json({ success: true });
}));
router.delete('/survey/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
  const { authId, authType } = req.decoded;
  const id = parseInt(req.params.id);
  if ( authType === 0 ) { 
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  } else {
    const sql = `DELETE FROM student_surveys WHERE student_id = ${authId} and survey_id = ${id}`;
    await db.getQueryResult(sql);
  }
  res.json({ success: true });
}));
//// added 2018 - 11 - 12 
//// response log
////
router.post('/response/:id', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {

  const { authId, authType} = req.decoded;
  const item_id = parseInt(req.params.id, 10);
  const { type, start_time, end_time, order, lecture_id } = req.body;
  
  const result = await modules.models.lecture_item_response_log.create({
    student_id: authId,
    start_time,
    end_time,
    order,
    type,
    item_id,
    lecture_id
  });
  if ( result ) {
    res.json({success: true});
  } else {
    res.json({success: false});
  }


}));
/////////// 자기가 만든 문제들 목록
router.get('/:id/question', modules.auth.tokenCheck, modules.asyncWrapper( async ( req, res, next) => {

  const { authId, authType } = req.decoded; 
  const lectureId = parseInt(req.params.id, 10);
  const lecture = await modules.models.lecture.findOne({ where : { lecture_id: lectureId }});
  let result = [];
  if ( authType === 1 ) {
    // teacher
    if ( lecture.teacher_id === authId ) {
      modules.models.student_question.findAll({
        where: { lecture_id: lectureId }
      }).then(function(result){
        res.json(result);
      })
    } else {
      throw modules.errorBuilder.default('Unauthorized in lecture', 401, true);
    }
    
  } else {
    // student
    let questions = await modules.models.student_question.findAll({
      where : { student_id : authId, lecture_id: lectureId },
      include: [{ model: modules.models.student_question_keyword },
               { model: modules.models.student_question_file }
          ]
    });
    for (let i = 0 ; i < questions.length; i += 1) {
      if (questions[i].type !== 2) {
        questions[i].answer = questions[i].answer.split(config.separator);
        questions[i].choice = questions[i].choice.split(config.separator);
        if (questions[i].choice2) {
          questions[i].choice2 = questions[i].choice2.split(config.separator);
        }
      } else {
        questions[i].answer = questions[i].answer;
        questions[i].choice = [];
        questions[i].choice2 = [];
        let files = await modules.models.student_question_file.findAll({ where: { student_question_id: questions[i].student_question_id }});
        for (let j = 0 ; j < files.length ; j += 1) {
          questions[i].student_question_files.push(files[j]);
        }
      }
    }
    res.json(questions);
  }

})) 
// 문제 생성
router.post('/:id/question',modules.auth.tokenCheck, modules.asyncWrapper( async ( req, res, next) => {

  const { authId, authType } = req.decoded
  const lectureId = parseInt(req.params.id, 10);
  let { name, question, choice, answer, difficulty, type } = req.body;
  // 해당 학생이 수강학생이 맞는지 체크
  if (type === 2) {

    let Q = await modules.models.student_question.create({
      name,
      question,
      score: 0,
      type,
      student_id: authId,
      lecture_id: lectureId,
      answer, // 원래 없었는데 파일에 유튜브 추가 기능 생기면서 유튜브 주소가 answer로 들어옴
    });
    res.json(Q);
  } else {

    answer = answer.join(config.separator);
    choice = choice.join(config.separator);
    let Q = await modules.models.student_question.create({
      name : name,
      question: question,
      answer: answer,
      difficulty: difficulty,
      score: 0,
      type: type,
      choice: choice,
      student_id: authId,
      lecture_id: lectureId
    });
    res.json(Q);
  
  }
 
}));
// 문제 수정 ( 타입 수정 불가 )
router.put('/:id/question/:question_id', modules.auth.tokenCheck, modules.asyncWrapper( async ( req, res, next)=> {

  const { authId, authType } = req.decoded;
  const studentQuestionId = parseInt(req.params.question_id, 10);
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  const question = await modules.models.student_question.findOne({ 
    where: { student_question_id: studentQuestionId },
    include: {
      model: modules.models.lecture
    }
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  if (authType === 1 || authType === 3) {
    // teacher
    if (question.lecture.teacher_id === authId) {
      const key = keys[0];
      if (key === 'isCheck') {
        question[key] = req.body[key];
      }
      await question.save();
      res.json({ success: true });
 
    } else {
      throw modules.errorBuilder.default('Unauthorized in lecture', 401, true);
    }

  } else {
    // teacher
    for (let i = 0; i < keyLen; i += 1) {
      const key = keys[i];
  
      if (key === 'name' || key === 'question' || key === 'difficulty' ) {
        question[modules.toSnakeCase(key)] = req.body[key];
      } else if (key === 'choice' || key === 'answer' ) {
        question[modules.toSnakeCase(key)] = req.body[key].join(config.separator);
      }
    }
    await question.save();
  
    res.json({ success: true });
  }
}));
router.get('/:id/question/:question_id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
  const { authId } = req.decoded;
  const studentQuestionId = parseInt(req.params.question_id, 10);
  const keywords = await modules.models.student_question_keyword.findAll({ where: { student_question_id: studentQuestionId }});
  res.json(keywords);

}))
// 문제 삭제
router.delete('/:id/question/:question_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const studentQuestionId = parseInt(req.params.question_id, 10);
  let result = await modules.models.student_question.findOne({ where: { student_question_id: studentQuestionId }});
  if(result.type === 2) {
  // if file type
    s3upload.deleteFile('studentQuestionAll', { question_id: studentQuestionId });
    await modules.models.student_question.destroy({
      where: { student_question_id: studentQuestionId, student_id : authId }
    })

    res.json({ success: true });    
  } else {
    await modules.models.student_question.destroy({
      where: { student_question_id: studentQuestionId, student_id : authId }
    })
    res.json({ success: true });
  }
}))
// 문제 키워드 추가
router.post('/:id/question/:question_id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {

  const { authId, authType } = req.decoded;
  const studentQuestionId = parseInt(req.params.question_id, 10);
  const { data } = req.body;
  // let keywords = data.split(config.separator);

  const question = await modules.models.student_question.findOne({
    where : { student_question_id : studentQuestionId }
  });
  if (!question) {
    throw modules.errorBuilder.default('question is not found', 404, true);
  }
  let scoreSum = 0;
  for ( let i = 0 ; i < data.length ; i+= 1 ) {
    scoreSum += data[i].score_portion;
    await modules.models.student_question_keyword.create({
      student_question_id: studentQuestionId,
      lecture_id: question.lecture_id,
      keyword: data[i].keyword,
      score_portion: data[i].score_portion
    })
  }
  question.score = scoreSum;
  await question.save();

  res.json({success : true, score : scoreSum });
}));
// 문제 키워드 삭제
router.delete('/:id/question/:question_id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async(req,res,next) => {

  const questionId = parseInt(req.params.question_id, 10);
  const { authId, authType } = req.decoded;
  
  let result = await modules.models.student_question_keyword.destroy({
    where: { student_question_id : questionId  }
  })
  if (!result) {
    
  }
  res.json({success: true});
  

}));
// 문제 파일 추가
router.post('/:id/question/:question_id/file', modules.auth.tokenCheck, upload.single('file'), modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const questionId = parseInt(req.params.question_id, 10);
  const { authId } = req.decoded;

  s3upload.uploadFile('studentQuestion',{ student_id: authId, question_id: questionId }, req.file);
  res.json({ success: true });

}));
router.delete('/:id/question/:question_id/:fid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const fileId = parseInt(req.params.fid, 10);
  const questionId = parseInt(req.params.question_id, 10);
  s3upload.deleteFile('studentQuestion', { question_id: questionId, file_id: fileId });

}));
router.get('/:id/question/:question_id/file', modules.auth.tokenCheck, modules.asyncWrapper(async( req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  const questionId = parseInt(req.params.question_id, 10);
  let files = await modules.models.student_question_file.findAll({ where: { student_question_id: questionId, lecture_id: lectureId }});
  res.json(files);

}));
  // 해당수업에 과제가 있는지 체크
router.get('/:id/question/isCheck', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  const lecture = await modules.models.lecture_homework.findOne({ where: { lecture_id: lectureId }});
  if (lecture) {
    res.json({ result: true});
  } else {
    res.json({ result: false });
  }
}))
router.get('/:id/question/random', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId } = req.decoded;
  // question keyword 연관
  const lectureHomework = await modules.models.lecture_homework.findOne({ where: { lecture_id: lectureId }});
  let evalCnt = lectureHomework.evaluated_cnt;
  let evalSelect = lectureHomework.eval_select;
  
  if(evalSelect === 1) {
    const lecture = await modules.models.lecture.findOne({ 
      attributes: ['class_id'],
      where: { lecture_id: lectureId }
    });
    let classId = lecture.class_id;
    const students = await modules.models.user_class.findAndCountAll({ where: { class_id: classId }});
    let stuNum = students.count;
    evalCnt = parseInt(evalCnt * stuNum / 100);
    if (evalCnt < 1) evalCnt = 1;
  }
  if (evalCnt === undefined) {
    evalCnt = 10;
  }
  const current = Date.now();
  if (lectureHomework.scoringStartAt !== null && lectureHomework.scoringStartAt > current) {
    res.json({ success: false, message: "시작전" });
    return;
  } else if (lectureHomework.scoringEndAt !== null && lectureHomework.scoringEndAt < current) {
    res.json({ success: false, message: "종료" });
    return;
  }
  let estimateCnt = null;
  const estimates = await modules.models.student_question_estimate.findAll({ where: { student_id: authId, lecture_id: lectureId }});
  const estimate_question_ids = estimates.map(es => es.student_question_id);
  if (evalCnt !== null || evalSelect !== null) {
    estimateCnt = evalCnt - estimates.length; // 체점해야할 문항 갯수
  }

  // let questions = await modules.models.student_question.findAll({ where: { lecture_id: lectureId , student_id: { ne: authId }, order: [['scoring_cnt', 'DESC']]}}); // 자기가 만든 문제가 아닌
  const sql = `SELECT * FROM student_questions WHERE lecture_id = ${lectureId} AND student_id != ${authId} ORDER BY scoring_cnt`
  const questions = await db.getQueryResult(sql);
  for (let i = 0 ; i < questions.length ; i += 1) {
    if (questions[i].type !== 2) {
      questions[i].answer = questions[i].answer.split(config.separator);
      questions[i].choice = questions[i].choice.split(config.separator);
      if (questions[i].choice2) {
        questions[i].choice2 = questions[i].choice2.split(config.separator);
      }
    }
  }
  let result;
  if (estimateCnt !== null) {
    result = questions.filter(question => !estimate_question_ids.includes(question.student_question_id)).slice(0, estimateCnt);
  } else {
    result = questions.filter(question => !estimate_question_ids.includes(question.student_question_id))
  }

  res.json({ success: true, result });

  // if (questions.length <= 5 ) {
  //   for (let i = 0 ; i < questions.length ; i += 1){
  //     if (estimate_question_ids.includes(questions[i].student_question_id)) {
  //       questions.splice(i,1);
  //     }
  //   }
  //   res.json({success: true, result: questions});
  //   return;
  // }
  // let len = questions.length; // 총 문항 갯수
  // let results = [];
  // let resultQuestions = []; 
  // for (let i = 0 ; i < estimateCnt ; i += 1) {
  //   let n = parseInt(Math.random() * len, 10);
  //   if (results.includes(n) || estimate_question_ids.includes(questions[n].student_question_id)) {
  //     i -= 1;
  //     continue;
  //   }
  //   results.push(n); // 랜덤값 저장
  //   resultQuestions.push(questions[n]);
  // }

  // res.json({ success: true, result: resultQuestions });

}));
router.get('/question/scoring/:student_question_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { student_question_id } = req.params;

  const estimate = await modules.models.student_question_estimate.findOne({ where: { student_id: authId, student_question_id: student_question_id }});
  res.json({ estimate });
}));

router.post('/:id/question/scoring', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const { authId, authType } = req.decoded;
  const { student_question_id, score, comment } = req.body;
  const lectureId = parseInt(req.params.id, 10);

  const estimate = await modules.models.student_question_estimate.findOne({ where: { student_id : authId, student_question_id: student_question_id }});
  const question = await modules.models.student_question.findOne({ where : { student_question_id }});
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }

  if (estimate) {
    res.json({ success: false, message: '이미 해당 문항에 평가를 수행하였습니다.'});
    return;
  } else {

    await modules.models.student_question_estimate.create({ 
      student_id: authId, lecture_id: lectureId, student_question_id, score, comment
    });
    // let sum = question.scoring_avg;
    const sum = (question.scoring_avg * question.scoring_cnt) + score;
    question.scoring_cnt += 1;
    const avg = sum / question.scoring_cnt;
    question.scoring_avg = avg;
    question.save();
    res.json({ success: true, message: 'success' });
  }

}));
// 2020.05.22 문항 평가 삭제
router.get('/:id/question/:qid/deleting', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
  const { authId , authType } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const questionId = parseInt(req.params.qid, 10);

  const estimate = await modules.models.student_question_estimate.findOne({ where: { student_id : authId, student_question_id: questionId }});
  const question = await modules.models.student_question.findOne({ where : { student_question_id: questionId }});

  if (!estimate) {
    try {
      JSON.parse(question);
    } catch (e) {
      throw modules.errorBuilder.default('Not Found', 404, true);
    }
  } else if (!question) {
    try {
      JSON.parse(question);
    } catch (e) {
      throw modules.errorBuilder.default('Not Found', 404, true);
    }
  } else {
    const score = estimate.dataValues.score;
    const scoringCnt = question.dataValues.scoring_cnt;
    const avg1 = question.dataValues.scoring_avg;
    const newScorigCnt = scoringCnt - 1;
    let avg2 = 0;
    if (newScorigCnt != 0) {
      avg2 = (scoringCnt * avg1 - score) / newScorigCnt;
    }
    const sql1 = `DELETE FROM student_question_estimates WHERE student_id = ${authId} and student_question_id = ${questionId}`;
    const sql2 = `UPDATE student_questions SET scoring_cnt = ${newScorigCnt}, scoring_avg = ${avg2} WHERE student_question_id = ${questionId}`;
    await db.getQueryResult(sql1);
    await db.getQueryResult(sql2);
    res.json({ success: true });
  }
}));
router.get('/:id/check', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  const lecture = await modules.models.lecture_homework.findOne({ where: { lecture_id: lectureId }});
  if (lecture) {
    res.json({ success: true, lecture });
  } else {
    res.json({ success: false });
  }
  
}));
router.get('/:id/restore', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  const sql = `select student_question_id, avg(score) as score  from student_question_estimates where lecture_id = ${lectureId} group by student_question_id`;
  const result = await db.getQueryResult(sql);
  for (let i = 0 ; i < result.length ; i += 1) {
    const sql0 = `update student_questions set scoring_avg = ${result[i].score} where student_question_id = ${result[i].student_question_id}`;
    await db.getQueryResult(sql0);
  }
  res.json({success: true });
}));

// 학생 문제 보이는 시간 표시
router.post('/display_time', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {
  const { authId, authType } = req.decoded;
  const { item_id, lecture_id, class_id, student_local_time } = req.body;

  await modules.models.student_item_display_time_log.create({ 
    student_id: authId,
    lecture_id,
    class_id,
    item_id,
    student_local_time,
  });

  res.json({ success: true });

}));

// 배점 변경
router.put('/transScore/:lectureId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res ,next) => {
  const lectureId = parseInt(req.params.lectureId, 10);
  const { data } = req.body;

  for (let i = 0; i < data.length; i += 1) {
    for (let j = 0; j < data[i].answers.length; j++) {
      await modules.models.student_answer_log.update({
        transScore: data[i].answers[j].score,
      }, {
        where: {
          student_id: data[i].answers[j].student_id,
          lecture_id: lectureId,
          item_id: data[i].answers[j].item_id,
        }
      });
    }
  }

  res.json({ success: true, data });
}));

module.exports = router;
