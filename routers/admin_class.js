const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const execTimeout10 = require('../src/utility');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const { Op } = sequelize;


// 인증 (공통)
router.use(modules.auth.tokenCheck);
router.use(modules.auth.checkTokenIsAdmin);

async function practiceAutomation({class_id, docker_port}) {
  const class_name = "class" + class_id;
  const docker_image_name = "lshghkh/default_hub:v11";

  await execTimeout10("docker run -p " + docker_port + ":8000 -d --name " + class_name + " " + docker_image_name + " jupyterhub");
  await execTimeout10("docker exec " + class_name + " /bin/sh -c \"sed -i '5d' /root/preswot_server_info.py\"");
  await execTimeout10("docker exec " + class_name + " /bin/sh -c \"sed -i '5c\\        self.CLASS_ID = " + class_id + "' /root/preswot_server_info.py\""); 

  const sql0 = `select u.email_id as student_email_id from users u, user_classes uc where u.user_id = uc.user_id and uc.class_id = ${class_id}`;
  const students = await db.getQueryResult(sql0);

  for (let i = 0; i < students.length; i += 1) {
    const student_name = students[i].student_email_id;
    await execTimeout10("docker exec " + class_name + " /bin/sh -c \"sh /srv/scripts/add_user.sh " + student_name + " " + student_name + "\"");
  }
}

router.get('/classname_duplicate_check', modules.asyncWrapper(async (req, res, next) => {
  const { name } = req.query;
  const res1 = await modules.models.class.findOne({
    where: {
      name,
    },
  });
  if (res1)res.status(409).send('이미 등록된 과목명입니다.');
  if (!res1)res.json({ success: true });
}));

router.post('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    name,
    description,
    teacher_user_id,
    start_date,
    end_date,
    opened,
    practice,
    student_user_id,
  } = req.body;

  if (name === undefined) {
    throw new Error('name은 반드시 포함되어야 합니다.');
  }

  // boolean 입력값을 기존 형식에 맞게 수정
  let IntOpened = 1;
  if (opened === true) IntOpened = 0;

  // 과목 개설
  const res1 = await modules.models.class.create({
    name,
    description,
    opened: IntOpened,
    master_id: teacher_user_id[0],
    start_time: start_date,
    end_time: end_date,
  });

  // 매핑 테이블에 입력 - 강사
  for (let index = 0; index < teacher_user_id.length; index += 1) {
    await modules.models.user_class.create({
      role: 'teacher',
      class_id: res1.class_id,
      user_id: teacher_user_id[index],
    });
  }

  // 매핑 테이블에 입력 - 학생
  for (let index = 0; index < student_user_id.length; index += 1) {
    await modules.models.user_class.create({
      role: 'student',
      class_id: res1.class_id,
      user_id: student_user_id[index],
    });
  }

  if (practice === true) {
    // 포트 점유 확인
    const res2 = await modules.models.class.findOne({
      where: { portNum: Number(7000 + res1.class_id) },
    });
    if (res2 !== null) {
      throw new Error('이미 점유중인 포트 번호입니다. 과목을 생성하였으나, 실습 기능 구성은 중단되었습니다.');
    }
    console.log('practice = ', practice);

    // 클래스 테이블에 입력 - 포트번호
    await modules.models.class.update({
      portNum: Number(7000 + res1.class_id),
    }, {
      where: { class_id: res1.class_id },
    });

    // 실습 도커 생성 TODO: 이 기능 사용중에 express를 점유하여 서버가 다른 요청을 받지 못하는것으로 보입니다. 해결 필요
    await practiceAutomation({
      class_id: res1.class_id,
      docker_port: Number(7000 + res1.class_id),
    });
  }

  res.json({ success: true });
}));

router.get('/list', modules.asyncWrapper(async (req, res, next) => {
  const { university_name, department_name, end_date_from, end_date_to, isActive } = req.query;
  const where = {};
  if (university_name !== undefined) where.university_name = university_name;
  if (department_name !== undefined) where.department_name = department_name;
  if (end_date_from !== undefined || end_date_to !== undefined) {
    where.end_date = {
      [Op.gt]: (end_date_from !== undefined ? Date.parse(end_date_from) : Date.parse(null)),
      [Op.lt]: (end_date_to !== undefined ? Date.parse(end_date_to) + 24 * 60 * 60 * 1000 : Date.parse('9999-01-01')),
    };
  }
  if (isActive === 'true') where.isActive = 1;
  if (isActive === 'false') where.isActive = 0;

  const res1 = await modules.models.class.findAll({
    attributes: modules.models.class.selects.set1,
    where,
    include: {
      attributes: ['email_id'],
      as: 'master',
      model: modules.models.user,
    },
  });
  res.json(res1);
}));

router.put('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    name,
    isActive,
    university_name,
    department_name,
    main_teacher_email_id,
    sub_teacher_email_id,
    code,
    description,
    start_date,
    end_date,
    location,
    day_of_week,
    capacity,
  } = req.body;

  if (name === undefined) {
    throw new Error('name은 반드시 포함되어야 합니다.');
  }
  if (university_name === undefined) {
    throw new Error('university_name은 반드시 포함되어야 합니다.');
  }

  // 모든 강사 user_id를 저장할 배열
  let all_teachers_user_id_list = [];
  // 메일 주소로 user_id 추출
  let main_teacher_user_id;
  if (main_teacher_email_id !== undefined) {
    const res2 = await modules.models.user.findOne({
      attributes: ['user_id'],
      where: {
        email_id: main_teacher_email_id,
        type: 1,
      },
    });
    if (res2 === null) {
      throw new Error('main_teacher_email_id: 부적절한 값이 포함되었습니다. 강사가 아니거나 존재하지 않는 계정입니다.');
    }
    main_teacher_user_id = res2.user_id;
    all_teachers_user_id_list.push(main_teacher_user_id);
  }
  let sub_teacher_user_id_list;
  if (sub_teacher_email_id !== undefined) {
    if (Array.isArray(sub_teacher_email_id) !== true) {
      throw new Error('sub_teacher_email_id: 부강사 메일주소는 배열형태로 전달되어야 합니다.');
    }
    const res2 = await modules.models.user.findAll({
      attributes: ['user_id'],
      where: {
        email_id: { [sequelize.Op.in]: sub_teacher_email_id },
        type: 1,
      },
    });
    if (res2 === null
      || res2.length === 0
      || res2.length !== sub_teacher_email_id.length) {
      throw new Error('sub_teacher_email_id: 유효하지 않은 값 또는 중복되는 값 또는 강사가 아닌 계정이 포함되었습니다.');
    }
    if (res2.findIndex(element => (element.user_id === main_teacher_user_id)) !== -1) {
      throw new Error('sub_teacher_email_id: 주강사가 부강사 목록에 중복 포함되었습니다.');
    }
    Array.prototype.push.apply(all_teachers_user_id_list, (res2.map(x => x.user_id)));
  }

  if (all_teachers_user_id_list.length === 0) {
    throw new Error('최소한 1명 이상의 주강사 또는 부강사를 입력해야 합니다.');
  }

  const res1 = await modules.models.class.update({
    name,
    isActive,
    university_name,
    department_name,
    master_id: main_teacher_user_id,
    code,
    description,
    start_time: start_date,
    end_time: end_date,
    location,
    day_of_week,
    capacity,
  }, {
    where: { class_id: req.body.class_id },
  });

  // 에러 처리
  if (res1[0] !== 1) {
    throw new Error('update 문에 영향을 받은 열의 수가 1이 아닙니다.');
  }

  // 매핑 테이블에 해당 class_id로 등록된 열 모두 삭제
  await modules.models.user_class.destroy({
    where: {
      class_id: req.body.class_id,
      role: 'teacher',
    },
  });

  // 매핑 테이블에 입력
  for (let index = 0; index < all_teachers_user_id_list.length; index += 1) { 
    await modules.models.user_class.create({
      role: 'teacher',
      class_id: req.body.class_id,
      user_id: all_teachers_user_id_list[index],
    });
  }

  res.json({ success: true });
}));

router.get('/idnamelist', modules.asyncWrapper(async (req, res, next) => {
  const res1 = await modules.models.class.findAll({
    attributes: ['class_id', 'name'],
  });
  res.json(res1);
}));

router.get('/', modules.asyncWrapper(async (req, res, next) => {
  const { class_id } = req.query;
  const res1 = await modules.models.class.findOne({
    attributes: modules.models.class.selects.set1,
    where: { class_id },
    include: [{
      attributes: ['email_id'],
      as: 'master',
      model: modules.models.user,
    }, {
      attributes: ['user_id'],
      model: modules.models.user_class,
      where: {
        role: 'teacher',
      },
      include: {
        attributes: ['user_id', 'email_id', 'name'],
        model: modules.models.user,
      },
    }],
  });

  res.json(res1);
}));

router.delete('/', modules.asyncWrapper(async (req, res, next) => {
  const { class_id } = req.query;

  const res1 = await modules.models.class.destroy({
    where: { class_id },
  });

  // 응답
  if (res1 !== 1) {
    throw new Error('delete 문에 영향을 받은 열의 수가 1이 아닙니다.');
  }

  res.json({ success: true });
}));

// 에러 처리
router.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({ success: false, message: err.message });
});

module.exports = router;