const express = require('express');
const router = express.Router();

const modules = require('../modules/frequent-modules');
const deleteModule = require('../modules/delete');

// router.post('/', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
router.post('/', modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  console.log("UserPRofile back");
  const { authId, authType } = req.decoded;
  const { lecture_Id, material_type } = req.body;

  if(!req.file || !req.fileInfo) {
    throw modules.errorBuilder.default('No file', 404, true);
  }
  const newMaterial = await modules.models.material.create({
    lecture_id: parseInt(lecture_Id, 10),
    creator_id: authId,
    material_type: parseInt(material_type, 10),
    score_sum: 0,
  });

  const file = await modules.upload.fileAdd(req, 'material_id', parseInt(newMaterial.material_id, 10));

  res.json({ success: true, file });
}));

module.exports = router;
