const express = require('express');
const router = express.Router();
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const config = require('../config');


// 그룹 아이디 수정
router.post('/:itemListId', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  let itemListId = parseInt(req.params.itemListId, 10);
  let { list } = req.body;
  
  let group_list = await modules.models.lecture_item_list.update({
    linked_list: list
  },{
    where: {
      lecture_item_list_id: itemListId,
    },
  });

  res.json({success: true});
}));

module.exports = router;