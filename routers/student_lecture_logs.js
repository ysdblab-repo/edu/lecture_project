const express = require('express');
const router = express.Router();
const sequelize = require('sequelize')
const modules = require('../modules/frequent-modules');
const analysis = require('../modules/analysis');
const db = require('../modules/db')
const time_handler = require('../modules/time-handler')
const fs = require('fs')
const PythonShell = require('python-shell');
const exec = require('child_process').exec;
const config = require('../config');
const scoring = require('../modules/scoreing');
router.post('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
	
	const { authId } = req.decoded;
	const lecture_code_practice_id = req.params.id;
	const { code } = req.body;
	const practice_log = await modules.models.student_practice_log.create({
		lecture_code_practice_id: lecture_code_practice_id,
		code: code,
		user_id: authId
	});
	if ( practice_log ) {
		const practice_log_id = practice_log.dataValues.student_practice_log_id;
		var child = exec(`java –jar ../modules/TRAINING_SCORING.jar ../config.json ${lecture_code_practice_id} ${practice_log_id.dataValues.student_practice_log_id}`, function (error, stdout, stderr) {
	
			if(stdout.indexOf('SUCCESS_SUCEESS') >= 0 ) {
				res.json({ success : true })
			} else {
				res.json({ success: false });
			}

		});
	}
	res.json({ success : true });

}));
router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

	const id = parseInt(req.params.id, 10);
	const studentList = await modules.models.student_practice_log.findAll({
		include: [{model: modules.models.lecture_code_practice, required: true }],
		});
	let answerList = []
	for ( let i = 0; i < studentList.length; i+= 1) { 
		answerList.push(studentList[i].toJSON());
		delete answerList[i].lecture_code_practice;
	}

	res.json({ answerList });
	
}));
//////////////////// 임시 재체점용 /////////////////////////

router.get('/re/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

	const { authId } = req.decoded;
	// teacher Check 
	const questionId = parseInt(req.params.id,10);
	const question = await modules.models.question.findOne({ where : { question_id: questionId }});
	let result = [];
	if ( question.type === 0 ) { // 객관식 
		let result = []
		const studentAnswers = await modules.models.student_answer_log.findAll({ where : {question_id : questionId }});
		for ( let i = 0 ; i < studentAnswers.length ; i += 1 ){
			console.log('객관식 재체점');
			let sAnswer = studentAnswers[i].answer.split(config.separator);
			const { studentScore, studentRatio } = scoring.match(question, sAnswer);

			let str = `${studentAnswers[i].student_id}'s score ${studentAnswers[i].score} -> ${studentScore}`;

			await modules.models.student_answer_log.update(
				{ score : studentScore, ratio : studentRatio }, 
				{ where : {
					question_id: questionId,
					student_id: studentAnswers[i].student_id
				}});

				result.push(str);
		}
		
		res.json({ success: result});
		return;

	} else if ( question.type === 1 ) { // 주관식

	} else if ( question.type === 3) { 		// 코딩문제 
		console.log('코딩 재체점');

		await modules.models.oj_solution.update(
			{ result : 0 },
			{ where: { problem_id: questionId }}
		);
		const students = await modules.models.student_answer_log.update(
			{ score: null },
			{ where : { question_id: questionId,
			student_id: studentAnswers[i].student_id,
			}}
		);
		console.log(students);
		res.json({ success: true });
		return;
	
	} else { 
		console.log('else 재체점');

	}
	res.json({success:true});
}));


module.exports = router;

