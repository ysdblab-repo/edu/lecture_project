const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const modules = require('../modules/frequent-modules');

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { video, comment } = req.body;
  const { authId } = req.decoded;
  const fileName = `${authId}_${Date.now()}.mp4`;
  const fileDirectory  = `${modules.rootPath}/records`;

  fs.writeFile(`${fileDirectory}/${fileName}`, video.replace(/^data:(.*?);base64,/, "").replace(/ /g, '+'), 'base64', async err => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        err
      });
    } else {
      await modules.models.videoRecord.create({
        user_id: authId,
        comment,
        file_path: `${fileDirectory}/${fileName}`
      });
      res.json({
        success: true
      });
    }
  });
}));

router.get('/all', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authType } = req.decoded;

  if (authType !== 3) {
    res.json({
      success: false,
      err: 'Permission denied'
    });
  } else {
    const lists = await modules.models.videoRecord.findAll({
      include: {
        model: modules.models.user,
        attributes: ['user_id', 'email_id', 'name']
      },
      attributes: ['video_id', 'user_id', 'comment']
    });
    res.json({
      success: true,
      data: lists
    });
  }
}));

router.get('/:video_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authType } = req.decoded;
  const { video_id } = req.params;
  if (authType !== 3) {
    res.json({
      success: false,
      err: 'Permission denied'
    });
  } else {
    const videoData = await modules.models.videoRecord.findOne({
      where: {
        video_id
      }
    });
    const videoFile = fs.readFileSync(videoData.dataValues.file_path);
    const videoBase64 = videoFile.toString('base64');
    res.json({
      comment: videoData.dataValues.comment,
      video: videoBase64,
    });
  }
}));

router.delete('/:video_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authType } = req.decoded;
  const { video_id } = req.params;
  if (authType !== 3) {
    res.json({
      success: false,
      err: 'Permission denied'
    });
  } else {
    const videoData = await modules.models.videoRecord.findOne({
      where: {
        video_id
      }
    });
    fs.unlinkSync(videoData.dataValues.file_path);
    await modules.models.videoRecord.destroy({
      where: {
        video_id
      }
    });
    res.json({
      success: true
    });
  }
}));

module.exports = router;