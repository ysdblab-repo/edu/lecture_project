const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    classId,
    lectureId,
    studentId,
    itemId,
    view,
  } = req.query;

  if (itemId !== undefined) {
    // 1. 수강생 명단 파악
    // const sql = `SELECT user_classes.user_id, users.name, users.email_id
    //   FROM user_classes
    //   INNER JOIN users ON user_classes.user_id = users.user_id
    //   WHERE user_classes.class_id = ${classId}
    //     AND user_classes.role = 'student'`;
    // const result = await db.getQueryResult(sql);
    // console.log('@@@ result = ', result);
    // console.log('@@@ result.length = ', result.length);
    // console.log('@@@ result[0] = ', result[0]);
    // res.json(result);

    // 2. 아이템 타입 파악
    const sql = `SELECT lecture_item_id, type
      FROM lecture_items
      WHERE lecture_item_id = ${itemId};`;
    const result = await db.getQueryResult(sql);
    // console.log('@@@ result = ', result);
    // console.log('@@@ result.type = ', result.type);
    // console.log('@@@ result.length = ', result.length);
    // console.log('@@@ result[0] = ', result[0]);
    // console.log('@@@ result[0].type = ', result[0].type);
    // 3. 해당 아이템 제출 기록 수집
    // 4. 학생별 이해도, 집중도, 참여도 계산
    switch (result[0].type) {
      case 0: {
        const sql2 = `SELECT t1.*,
                        IF ( ISNULL(t2.ratio), 0, ROUND(t2.ratio * 100, 2)) AS understanding,
                        IF ( ISNULL(t3.concentration), 0, ROUND(t3.concentration, 2)) AS concentration,
                        IF ( ISNULL(t3.concentration), 0, 100 ) AS participation
                      FROM (
                        SELECT user_classes.user_id, users.name, users.email_id
                        FROM user_classes
                        INNER JOIN users ON user_classes.user_id = users.user_id
                        WHERE user_classes.class_id = ${classId}
                          AND user_classes.role = 'student') t1 -- 수강생 명단 파악
                      LEFT JOIN (
                        SELECT *
                        FROM student_answer_logs
                        WHERE item_id = ${itemId}) t2 -- 수강생 성적 파악
                      ON t1.user_id = t2.student_id
                      LEFT JOIN (
                        SELECT R1.*, ((@ROWNUM:=@ROWNUM+1)*20/R3.total)+80 AS concentration
                        FROM (
                          SELECT item_id, student_id, AVG(end_time - start_time) AS spend_time
                          FROM lecture_item_response_logs
                          WHERE item_id = ${itemId}
                          GROUP BY item_id, student_id
                          ORDER BY spend_time DESC
                        ) R1, (
                          SELECT @ROWNUM:=0
                        ) R2, (
                          SELECT COUNT(DISTINCT item_id, student_id) AS total
                          FROM lecture_item_response_logs
                          WHERE item_id = ${itemId}
                        ) R3
                      ) t3 -- 수강생 제출 소요시간 파악
                      ON t2.student_id = t3.student_id
                      AND t2.item_id = t3.item_id`;
        const result2 = await db.getQueryResult(sql2);
        res.json({ success: true, res: result2 });
        break;
      }
      default: {
        res.json({ success: false, message: `저널링 모듈에 정의되지 않은 아이템 타입 ${result[0].type} 입니다.` });
        break;
      }
    }
    // 5. 출력
  } else if (lectureId !== undefined) { // 강의 아이디가 있는 경우
    switch (view) {
      case 'student': {
        const sql2 = `SELECT t1.*,
                        IF ( ISNULL(t2.avg_ratio), 0, ROUND(t2.avg_ratio * 100, 2)) AS understanding,
                        IF ( ISNULL(t3.concentration), 0, ROUND(t3.concentration, 2)) AS concentration,
                        IF ( ISNULL(t3.concentration), 0, 100 ) AS participation
                      FROM (
                        SELECT user_classes.user_id, users.name, users.email_id
                        FROM user_classes
                        INNER JOIN users ON user_classes.user_id = users.user_id
                        WHERE user_classes.class_id = ${classId}
                          AND user_classes.role = 'student') t1 -- 수강생 명단 파악
                      LEFT JOIN (
                        SELECT R1.lecture_id, R1.student_id, R1.sum_ratio / R2.submit_item_cnt AS avg_ratio
                        FROM (
                          SELECT lecture_id, student_id, SUM(ratio) AS sum_ratio
                          FROM student_answer_logs
                          WHERE lecture_id = ${lectureId}
                          GROUP BY lecture_id, student_id
                        ) R1, (
                          SELECT COUNT(DISTINCT item_id) AS submit_item_cnt
                          FROM student_answer_logs
                          WHERE lecture_id = ${lectureId}
                        ) R2
                      ) t2 -- 수강생이 해당 강의에 속한 아이템에 제출한 성적의 평균
                      ON t1.user_id = t2.student_id
                      LEFT JOIN (
                        SELECT R1.*, ((@ROWNUM:=@ROWNUM+1)*20/R3.total)+80 AS concentration
                        FROM (
                          SELECT lecture_id, student_id, AVG(end_time - start_time) AS spend_time
                          FROM lecture_item_response_logs
                          WHERE lecture_id = ${lectureId}
                          GROUP BY lecture_id, student_id
                          ORDER BY spend_time DESC
                        ) R1, (
                          SELECT @ROWNUM:=0
                        ) R2, (
                          SELECT COUNT(DISTINCT lecture_id, student_id) AS total
                          FROM lecture_item_response_logs
                          WHERE lecture_id = ${lectureId}
                        ) R3
                      ) t3 -- 수강생이 해당 강의에 속한 아이템에 제출한 소요시간 평균의 합계의 등수를 백분율
                      ON t2.student_id = t3.student_id
                      AND t2.lecture_id = t3.lecture_id`;


        //TODO. 2022-07-25 명목 이해도, 유효 이해도, 참여도, 반응도 조회 쿼리 작성
        // 명목 이해도: 전체 문항에 대한 학생의 이해도 평균
        // 유효 이해도: 자신이 푼 문항에 대한 이해도 평균
        // 참여도: 학생이 푼 문항수 / 강의의 전체 문항 수
        // 반응도: 강의의 student_answer_logs의 absolute_reactivity의 평균



        const result2 = await db.getQueryResult(sql2);
        res.json({ success: true, res: result2 });
        break;
      }
      case 'keyword': {
        break;
      }
      default: { // view === default 포함
        const sql2 = `SELECT t1.*,
                        IF ( ISNULL(t2.understanding), 0, ROUND(t2.understanding * 100, 2)) AS understanding,
                        IF ( ISNULL(t2.participation), 0, ROUND(t2.participation * 100, 2)) AS participation
                      FROM (
                        SELECT lecture_item_id, name
                        FROM lecture_items
                        WHERE lecture_id = ${lectureId}) t1 -- 강의에 속한 아이템 목록 파악
                      LEFT JOIN (
                        SELECT R1.item_id, R1.sum_ratio / R2.cnt_student AS understanding, R1.cnt_submit / R2.cnt_student AS participation
                        FROM (
                          SELECT item_id, SUM(ratio) AS sum_ratio, COUNT(*) AS cnt_submit
                          FROM student_answer_logs
                          WHERE lecture_Id = ${lectureId}
                          GROUP BY item_id
                        ) R1, ( -- 문항별 제출된 ratio 합, 문항별 제출 수 합
                          SELECT COUNT(*) AS cnt_student
                          FROM user_classes
                          WHERE class_id = ${classId}
                            AND role = 'student'
                        ) R2 -- 전체 수강생 수
                      ) t2
                      ON t1.lecture_item_id = t2.item_id`;
        const result2 = await db.getQueryResult(sql2);
        res.json({ success: true, res: result2 });
        break;
      }
    }
  } else if (studentId !== undefined) {
    switch (view) {
      case 'student': {
        const sql5 = `SELECT t1.*,
        IF ( ISNULL(t2.understanding_myoungmok), 0, ROUND(t2.understanding_myoungmok/r5.item_cnt * 100, 2)) AS understanding_myoungmok,
        IF ( ISNULL(t2.cnt_question), 0, ROUND(t2.understanding_myoungmok/t2.cnt_submit * 100, 2)) AS understanding,
        IF ( ISNULL(t2.cnt_question), 0, ROUND(t2.cnt_submit/r5.item_cnt * 100, 2)) AS participation,
        IF ( ISNULL(t2.reactivity), 0, round(t2.reactivity * 100, 2)) AS reactivity,
        IF ( ISNULL(t2.cnt_submit), 0, CONCAT(t2.cnt_submit, " / ", r5.item_cnt)) AS cnt_question
          FROM (
            SELECT lecture_id, name
            FROM lectures
            WHERE class_id = ${classId}) t1
          LEFT JOIN (
            SELECT R1.lecture_id, SUM(R1.sum_ratio) as understanding_myoungmok, AVG(R1.sum_ratio / R1.cnt_submit) AS understanding, SUM(R1.cnt_submit) AS cnt_submit, R3.item_cnt AS cnt_question, (AVG(R1.absolute_reactivity)/40)*100 AS reactivity
            FROM (
              SELECT lecture_id, item_id, SUM(ratio) AS sum_ratio, COUNT(*) AS cnt_submit, AVG(absolute_reactivity - 0.6) AS absolute_reactivity
              FROM student_answer_logs
              WHERE lecture_id IN (
                SELECT lecture_id
                FROM lectures
                WHERE class_id = ${classId}
              ) AND student_id = ${studentId}
              GROUP BY item_id
            ) R1, (
              select a.lecture_id AS lecture_id, count(a.lecture_item_id) as item_cnt
              from lecture_items a
                left outer join (
                  select * from lectures
                ) b
                on a.lecture_id = b.lecture_id
                where b.class_id = ${classId}
                group by a.lecture_id
            ) R3
            WHERE R1.lecture_id = R3.lecture_id
            GROUP BY R1.lecture_id
          ) t2
          ON t1.lecture_id = t2.lecture_id
          LEFT JOIN (select a.lecture_id AS lecture_id, count(a.lecture_item_id) as item_cnt
              from lecture_items a
                left outer join (
                  select * from lectures
                ) b
                on a.lecture_id = b.lecture_id
                where b.class_id = ${classId}
                group by a.lecture_id) r5
                ON r5.lecture_id = t1.lecture_id`

        const result5 = await db.getQueryResult(sql5);
        res.json({ success: true, res: result5 });
        break;
      }
      default:{
        console.log("error")
      }
    }
  } else if (classId !== undefined) { // 과목 아이디만 있는 경우
    switch (view) {
      case 'student': {
        // const sql2 = `SELECT t1.*,
        //                 IF ( ISNULL(t2.avg_ratio), 0, ROUND(t2.avg_ratio * 100, 2)) AS understanding,
        //                 IF ( ISNULL(t3.concentration), 0, ROUND(t3.concentration, 2)) AS concentration,
        //                 IF ( ISNULL(t3.concentration), 0, 100 ) AS participation
        //               FROM (
        //                 SELECT user_classes.user_id, users.name, users.email_id
        //                 FROM user_classes
        //                 INNER JOIN users ON user_classes.user_id = users.user_id
        //                 WHERE user_classes.class_id = ${classId}
        //                   AND user_classes.role = 'student') t1 -- 수강생 명단 파악
        //               LEFT JOIN (
        //                   SELECT student_id, SUM(ratio), COUNT(*), SUM(ratio) / COUNT(*) AS avg_ratio
        //                   FROM student_answer_logs
        //                   WHERE lecture_id IN (
        //                     SELECT lecture_id FROM lectures WHERE class_id = ${classId}
        //                   )
        //                   GROUP BY student_id
        //               ) t2 -- 수강생 성적 파악
        //               ON t1.user_id = t2.student_id
        //               LEFT JOIN (
        //                 SELECT R1.*, ((@ROWNUM:=@ROWNUM+1)*20/R3.total)+80 AS concentration
        //                 FROM (
        //                   SELECT student_id, AVG(end_time - start_time) AS spend_time
        //                   FROM lecture_item_response_logs
        //                   WHERE lecture_id IN (
        //                     SELECT lecture_id FROM lectures WHERE class_id = ${classId}
        //                   )
        //                   GROUP BY student_id
        //                   ORDER BY spend_time DESC
        //                 ) R1, (
        //                   SELECT @ROWNUM:=0
        //                 ) R2, (
        //                   SELECT COUNT(DISTINCT student_id) AS total
        //                   FROM lecture_item_response_logs
        //                   WHERE lecture_id IN (
        //                     SELECT lecture_id FROM lectures WHERE class_id = ${classId}
        //                   )
        //                 ) R3
        //               ) t3 -- 수강생 제출 소요시간 파악
        //               ON t2.student_id = t3.student_id`;

        const sql2 = `SELECT t1.*,
                        IF ( ISNULL(t2.avg_ratio), 0, ROUND(((t2.avg_ratio * t2.submit_cnt)/r1.item_cnt) * 100, 2)) AS understanding_myoungmok,
                        IF ( ISNULL(t2.avg_ratio), 0, ROUND(t2.avg_ratio * 100, 2)) AS understanding,
                        IF ( ISNULL(t2.absolute_reactivity), 0, ROUND(t2.absolute_reactivity, 2)) AS reactivity,
                        IF ( ISNULL(t2.submit_cnt), 0, ROUND((t2.submit_cnt / r1.item_cnt)*100,2)) AS participation,
                        IF ( ISNULL(t2.submit_cnt), 0, CONCAT(t2.submit_cnt, " / ", r1.item_cnt)) AS cnt_question,
                        IF ( ISNULL(t3.student_id), -1, t3.student_id) AS student_id
                      FROM (
                        SELECT user_classes.user_id, users.name, users.email_id
                        FROM user_classes
                        INNER JOIN users ON user_classes.user_id = users.user_id
                        WHERE user_classes.class_id = ${classId}
                          AND user_classes.role = 'student') t1
                      LEFT JOIN (
                          SELECT student_id, SUM(ratio), COUNT(*) AS submit_cnt, SUM(ratio) / COUNT(*) AS avg_ratio, ((AVG(absolute_reactivity - 0.6)*100)/40)*100 AS absolute_reactivity
                          FROM student_answer_logs
                          WHERE lecture_id IN (
                            SELECT lecture_id FROM lectures WHERE class_id = ${classId}
                          )
                          GROUP BY student_id
                      ) t2
                      ON t1.user_id = t2.student_id
                      LEFT JOIN (
                        SELECT R1.*, ((@ROWNUM:=@ROWNUM+1)*20/R3.total)+80 AS concentration
                        FROM (
                          SELECT student_id, AVG(end_time - start_time) AS spend_time
                          FROM lecture_item_response_logs
                          WHERE lecture_id IN (
                            SELECT lecture_id FROM lectures WHERE class_id = ${classId}
                          )
                        GROUP BY student_id
                        ORDER BY spend_time DESC
                        ) R1, (
                          SELECT @ROWNUM:=0
                        ) R2, (
                          SELECT COUNT(DISTINCT student_id) AS total
                          FROM lecture_item_response_logs
                          WHERE lecture_id IN (
                            SELECT lecture_id FROM lectures WHERE class_id = ${classId}
                          )
                        ) R3
                      ) t3
                      ON t2.student_id = t3.student_id,
                    (SELECT COUNT(*) AS item_cnt
                    FROM lecture_items a, lectures b
                    WHERE a.lecture_id = b.lecture_id AND b.class_id = ${classId})
                    r1`;

        const result2 = await db.getQueryResult(sql2);
        res.json({ success: true, res: result2 });
        break;
      }
      case 'keyword': {
        break;
      }
      default: { // view === default 포함
        // const sql2 = `SELECT t1.*,
        //                 IF ( ISNULL(t2.understanding), 0, ROUND(t2.understanding * 100, 2)) AS understanding,
        //                 IF ( ISNULL(t2.participation), 0, ROUND(t2.participation * 100, 2)) AS participation
        //               FROM (
        //                 SELECT lecture_id, name
        //                 FROM lectures
        //                 WHERE class_id = ${classId}) t1 -- 과목에 속한 강의 목록 파악
        //               LEFT JOIN (
        //                 SELECT R1.lecture_id, AVG(R1.sum_ratio / R2.cnt_student) AS understanding, AVG(R1.cnt_submit / R2.cnt_student) AS participation
        //                 FROM (
        //                   SELECT lecture_id, item_id, SUM(ratio) AS sum_ratio, COUNT(*) AS cnt_submit
        //                   FROM student_answer_logs
        //                   WHERE lecture_id IN (
        //                     SELECT lecture_id
        //                     FROM lectures
        //                     WHERE class_id = ${classId}
        //                   )
        //                   GROUP BY item_id
        //                 ) R1, ( -- 문항별 제출된 ratio 합, 문항별 제출 수 합
        //                   SELECT COUNT(*) AS cnt_student
        //                   FROM user_classes
        //                   WHERE class_id = ${classId}
        //                     AND role = 'student'
        //                 ) R2, (-- 전체 수강생 수
        //                   select a.lecture_id, count(a.lecture_item_id) as item_cnt
        //                   from lecture_items a
        //                     left outer join (
        //                       select * from lectures
        //                     ) b
        //                     on a.lecture_id = b.lecture_id
        //                     where b.class_id = ${classId}
        //                     group by a.lecture_id
        //                 ) R3,
        //                 GROUP BY R1.lecture_id
        //               ) t2
        //               ON t1.lecture_id = t2.lecture_id`;

        const sql2 = `SELECT t1.*,
          IF ( ISNULL(t2.understanding_myoungmok), 0, ROUND(t2.understanding_myoungmok * 100, 2)) AS understanding_myoungmok,
          IF ( ISNULL(t2.understanding), 0, ROUND(t2.understanding * 100, 2)) AS understanding,
          IF ( ISNULL(t2.participation), 0, ROUND(t2.participation * 100, 2)) AS participation,
          IF ( ISNULL(t2.reactivity), 0, round(t2.reactivity * 100, 2)) AS reactivity,
          IF ( ISNULL(t2.cnt_question), 0, t2.cnt_question) AS cnt_question
            FROM (
              SELECT lecture_id, name
              FROM lectures
              WHERE class_id = ${classId}) t1
            LEFT JOIN (
              SELECT R1.lecture_id, R2.cnt_student AS cnt_student, AVG(R1.sum_ratio / R2.cnt_student) as understanding_myoungmok, AVG(R1.sum_ratio / R1.cnt_submit) AS understanding, AVG(R1.cnt_submit / R2.cnt_student) AS participation, R3.item_cnt AS cnt_question, (AVG(R1.absolute_reactivity)/40)*100 AS reactivity
              FROM (
                SELECT lecture_id, item_id, SUM(ratio) AS sum_ratio, COUNT(*) AS cnt_submit, AVG(absolute_reactivity - 0.6) AS absolute_reactivity
                FROM student_answer_logs
                WHERE lecture_id IN (
                  SELECT lecture_id
                  FROM lectures
                  WHERE class_id = ${classId}
                )
                GROUP BY item_id
              ) R1, (
                SELECT COUNT(*) AS cnt_student
                FROM user_classes
                WHERE class_id = ${classId}
                  AND role = 'student'
              ) R2, (
                select a.lecture_id AS lecture_id, count(a.lecture_item_id) as item_cnt
                from lecture_items a
                  left outer join (
                    select * from lectures
                  ) b
                  on a.lecture_id = b.lecture_id
                  where b.class_id = ${classId}
                  group by a.lecture_id
              ) R3
              WHERE R1.lecture_id = R3.lecture_id
              GROUP BY R1.lecture_id
            ) t2
            ON t1.lecture_id = t2.lecture_id`;

        const result2 = await db.getQueryResult(sql2);
        res.json({ success: true, res: result2 });
        break;
      }
    }
  } else {
    throw new Error('unexpected case :else');
  }
}));

router.get('/baseInfo', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    classId,
    lectureId,
    itemId,
  } = req.query;

  const result = {};
  const res0 = await modules.models.class.findByPk(classId, {
    attributes: ['name'],
  });
  result.className = res0.name;
  if (lectureId !== undefined) {
    const res1 = await modules.models.lecture.findByPk(lectureId, {
      attributes: ['name'],
    });
    result.lectureName = res1.name;
  }
  if (itemId !== undefined) {
    const res2 = await modules.models.lecture_item.findByPk(itemId, {
      attributes: ['name'],
    });
    result.itemName = res2.name;
  }
  res.json({ success: true, res: result });
}));
module.exports = router;
