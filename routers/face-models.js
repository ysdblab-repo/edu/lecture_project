const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');
const config = require('../config');
const modules = require('../modules/frequent-modules');
AWS.config.update({
  accessKeyId:  config.aws.accessKeyId,
  secretAccessKey: config.aws.secretAccessKey,
  region: 'ap-northeast-2'
})
const fs = require('fs');
const multer = require('multer');
const storage = multer.memoryStorage();
var upload = multer({ storage : storage });

// make lecture
router.get('/lecture/:id/:mode', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res, next) => {

  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const mode = parseInt(req.params.mode, 10);
  const lecture = await modules.models.lecture.findOne({ where: { lecture_id: lectureId }});
  let lectureAuth = await modules.models.lecture_face_auth.findOne({ where: {lecture_id: lectireId }});
  if (mode === 1) {
    // turn on
    if (lectureAuth) {
      lectureAuth.mode = 1; // turn on 
      lectureAuth.count += 1;
      lectureAuth.save();
    } else {
      await modules.models.lecture_face_auth.create({ lecture_id: lectureId, mode: 1, class_id: lecture.class_id, type: lecture.type, count: 1 });
    }
  } else {
    // off
    lectureAuth.mode = 0;
    lectureAuth.save();
  }
  res.json({ success: true });

}));
// 클라이언트에서 활성화된 얼굴인증이 시작되는 lecture_id 리스트
router.get('/lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res, next) => {
  
  const { authId } = req.decoded;
  const sql = `SELECT lfa.lecture_id FROM user_classes uc, lecture_face_auths lfa WHERE uc.class_id = lfa.class_id AND lfa.mode = 1 AND uc.user_id = ${authId}`;
  let lectureIds = await modules.db.getQueryResult(sql);
  res.json(lectureIds);

}));
// lecture에 해당하는 결과값 
router.post('/lecture/result', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res, next) => {

  const { authId } = req.decoded;
  // const lectureId = parseInt(req.params.id, 10);
  const { lecture_ids, result } = req.body;
  // console.log(`lecture_ids - ${JSON.stringify(lecture_ids)}`);
  // console.log(`result - ${JSON.stringify(result)}`);
  // const { x_position, y_position, frame_width, frame_height, confidence, lecture_ids } = req.body;

  for (let i = 0 ; i < lecture_ids.length ; i += 1) {
    for (let j = 0; j < result.length; j += 1) {
      await modules.models.student_face_auth.create({ lecture_id: lecture_ids[i], student_id: authId, x_position: result[j].x_position, 
        y_position: result[j].y_position, frame_width: result[j].frame_width, frame_height: result[j].frame_height , confidence: result[j].confidence });
    }
  }
  res.json({ success: true });

}));

router.get('/lectures', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res, next) => {
  const { authId } = req.decoded;
  modules.models.sequelize.query(`SELECT l.class_id, l.lecture_id, l.is_auto, l.\`type\`, l.start_time,l.end_time, l.teacher_id FROM user_classes uc, lectures l WHERE uc.class_id = l.class_id AND uc.user_id = ${authId} AND ( (NOW() BETWEEN l.start_time AND  l.end_time ) OR ( l.is_auto = 1))`
  ,{ type: modules.models.Sequelize.QueryTypes.SELECT}).then(async function(result){
    for ( let i = 0 ; i < result.length ; i += 1 ){
      let teacher = await modules.models.user.findOne({ where : { user_id : result[i].teacher_id }});
      result[i]['teacher_name'] = teacher.name;
      console.log(`${teacher.name}`);
    };
    for ( let i = 0 ; i < result.length; i += 1) {
      let lecture = await modules.models.lecture.findOne({ where: { lecture_id : result[i].lecture_id }});
      result[i]['lecture_name'] = lecture.name;
      console.log(`${lecture.name}`);

    }
    res.json(result);
  })
}))

router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async ( req,res,next) => {
  const { authId } = req.decoded;
  // let user = await modules.models.user.findOne({
  //   where: { user_id: authId }
  // });
  const model = await modules.models.user_face_model.findOne({ 
    where: { user_id: authId }
  });
  if ( model ) {
    res.json({ model : model.model });
  } else {
    res.json({ model : null });
    // throw modules.errorBuilder.default('user information is not correct', 403, true);
  }

}));
router.post('/', modules.auth.tokenCheck,  upload.single('model'), modules.asyncWrapper(async (req, res, next) => {

  const { authId } = req.decoded;
  const body = req.file;

  console.log(body);

  // let user = await modules.models.user.findOne({
  //   where: { user_id : authId }
  // });
  // if ( user.major ) {
  //   // s3 delete
  //   console.log('delete');
  //   let param = {
  //     Bucket: 'preswot-service',
  //     Key: `user-model/${authId}.yml` 
  //   };
  //   let s3 = new AWS.S3();
  //   let result = await s3.deleteObject(param);

  // }
  let exist = -1;
  let user = await modules.models.user_face_model.findOne({
    where: { user_id: authId }
  });
  if (user) {
    // s3 delete
    exist = 1;
    console.log(`delete`);
    let param = {
      Bucket: 'preswot-service',
      Key: `user-model/${authId}.yml` 
    };
    let s3 = new AWS.S3();
    let result = await s3.deleteObject(param);
  } else {
    exist = 0;
  }
  let s3_param = {
    Bucket: 'preswot-service',
    Key: `user-model/${authId}.yml`,
    ACL: 'public-read',
    ContentType: body.mimetype
  };
  var s3obj = new AWS.S3({ params: s3_param });
  s3obj.upload({ Body: body.buffer }).send(async function(err, data){
    
    let url = data.Location;
    console.log(url);

    if ( err ) {
      res.json({success: false});
    } else {
      // user.major = url;
      // user.save();
      if (exist == 1) {
        user.count += 1;
        user.model = url;
        user.save();
      } else {
        await modules.models.user_face_model.create({ user_id: authId, model: url });
      }
      res.json({ success : true})
    }
  });

}));
// router.post('/attendance', modules.auth.tokenCheck, modules.asyncWrapper( async(req,res,next) => {
  
//   const { authId } = req.decoded;
//   const { x_position, y_position, frame_width, frame_height, confidence, lecture_id } = req.body;
//   let result = await modules.models.student_attendance_face.create({
//     x_position, y_position, frame_width, frame_height, confidence, lecture_id,
//     student_id: authId
//   });
//   if ( result ) {
//     res.json({ success: true });
//   } else {
//     throw modules.errorBuilder.default('information is not correct', 403, true);
//   }
// }));

router.get('/lecture_check/:id', modules.auth.tokenCheck, modules.asyncWrapper( async (req,res) => {

  const { authId } = req.decoded;
  let lectureId = parseInt(req.params.id, 10);
  let lec = await modules.models.lecture.findOne({ where: { lecture_id: lectureId }});
  console.log(`result-opened - ${lec.result_opened}`);
  if (lec.result_opened) {
    res.json({ check: true });
  } else {
    res.json({ check: false });
  }
}));
router.get('/:id/result', modules.auth.tokenCheck, modules.asyncWrapper( async (req,res) => {

  const lectureId = parseInt(req.params.id, 10);
  // model 여부 , 결과값, 인증 여부
  const sql0 = `select u.user_id, u.name, u.email_id from users u, (select uc.user_id from user_classes uc, lectures l where uc.class_id = l.class_id and l.lecture_id = ${lectureId} and uc.role = 'student')result where u.user_id = result.user_id`
  const sql1 = `SELECT lecture_id, student_id, avg(confidence) as confidence FROM student_face_auths sfa WHERE sfa.lecture_id = ${lectureId} group by lecture_id, student_id`
  const result0 = await modules.db.getQueryResult(sql0)
  const result1 = await modules.db.getQueryResult(sql1);
  
  const result2 = [];
  for (let i = 0 ; i < result0.length ; i += 1) {
    result2.push(result0[i])
    result2[i]['done'] = 0;
    for (let j = 0 ; j < result1.length ; j += 1) {
      if(result2[i].user_id === result1[j].student_id) {
        result2[i]['done'] = 1;
        if(result1[j].confidence > 50) {
          result2[i]['confidence'] = 1;
        } else {
          result2[i]['confidence'] = 0;
        }
      }
    }
  }
  
  res.json(result2);

}));
router.get('/test', modules.asyncWrapper( async (req,res) => {

  const users = await modules.models.user.findAll();
  let result = [];
  for (let i = 0 ; i < users.length ; i += 1) {
    if (users[i].major) {
      await modules.models.user_face_model.create({ user_id: users[i].user_id, model: users[i].major, count: 1});
      result.push(users[i].user_id);
    }

  }
  res.json(result);

}));
module.exports = router;