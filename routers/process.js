const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const _ = require('lodash');
router.post('/:type/result', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  // 현재 수업중인 학생들이 보내는 프로그램 정보
  const { authId } = req.decoded;
  const { process_name, lecture_id, key_len } = req.body;
  const type = parseInt(req.params.type, 10);

  if (type === 0) {
    // 활성화중인 프로세스 정보
    let results = [];
    for (let i = 0 ; i < lecture_id.length ; i ++) {
      const result = await modules.models.process_student_log.create({ lecture_id: lecture_id[i], student_id: authId, process_name });
      results.push(result);
    }
    if (results.length >= 0 ) {
      return res.json({ success: true });
    } else {
      return res.json({ success: false });
    }
  } else {
    // key logger 정보
    let results = [];
    for (let i = 0 ; i < lecture_id.length ; i ++) {
      const result = await modules.models.process_student_key_log.create({ lecture_id: lecture_id[i], student_id: authId, keylen: key_len });
      results.push(result);
    }
    if (results.length >= 0) {
      return res.json({ success: true });
    } else {
      return res.json({ success: false });
    }
  }
}));
// router.get('/:lectureId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
//   // 강사가 호출하는 list
//   const { authId, authType } = req.decoded;
//   const lectureId = req.params.lectureId;

//   const students = await modules.models.process_student_log.findAll({ where: { lecture_id : lectureId }});
//   let results = _.groupBy(students, 'process_name');
//   let names = Object.keys(results);
//   const programs = await modules.models.process_info.findAll({ where : { [modules.models.Sequelize.Op.in]: {  names }}}); // name 

//   // names와 programs 결과 분리
//   // if (다르면) 
//   // modules.models.process_info.create({ process_name: names[i] })

//   let responseResult = {};

//   // [ { process_name: 'kakao.exe', description: '카카오톡', type: '금지': students: [] }, { ... }]
//   res.json(responseResult); 

// }
module.exports = router;