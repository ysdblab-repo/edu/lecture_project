const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const coverage = require('../modules/coverage-calculator');
const _ = require('lodash');
const db = require('../modules/db');
router.get('/',modules.asyncWrapper(async(req, res, next) => {

  const sql = `select * from lecture_items`;
  const result = await db.getQueryResult(sql);
  // var a = itemDivoide(result);
  
  // res.json(a);
}));
// router.get('/understanding/:id',modules.auth.tokenCheck,modules.asyncWrapper(async(req, res,next) => {

//   const { authId, authType } = req.decoded;
//   let classId = parseInt(req.params.id, 10);

//   const teacherCkeck = await modules.teacherInClass(authId, classId);
//   if (!(authType === 3 || teacherCkeck)) {
//     throw modules.errorBuilder.default('Permission Denied', 403, true);
//   }
//   // 답안 제출시 문항별 문항 키워드 이해도 커버리지, 문항 키워드 중요도 득점 비율 계산
// }));
router.get('/students/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const classId = parseInt(req.params.id,10);
  const sql = `select u.user_id, name, email_id from user_classes uc, users u where u.user_id = uc.user_id and uc.role = 'student' and uc.class_id = ${classId}`;
  const result = await db.getQueryResult(sql);
  res.json(result);
}));
router.get('/concentration/:id',modules.auth.tokenCheck,modules.asyncWrapper(async(req, res,next) => {

  const { authId, authType } = req.decoded;
  let lectureId = parseInt(req.params.id, 10);

  // const teacherCkeck = await modules.teacherInClass(authId, classId);
  // if (!(authType === 3 || teacherCkeck)) {
  //   throw modules.errorBuilder.default('Permission Denied', 403, true);
  // }
  // 답안 제출시 문항별 문항 키워드 이해도 커버리지, 문항 키워드 중요도 득점 비율 계산
  let result = await coverage.concentrationCoverage(lectureId);
  res.json(result);

}));
router.get('/understanding/:id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {

  const { authId, authType } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);

  console.log(`understanding`);
  let result = await coverage.understandingCoverage(lectureId, authId , 0); // type : 0 , keyword 계산 요청 x
  res.json(result);

}));
router.get('/participation/:id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res,next) => {

  const { authId, authType } = req.decoded;
  let lectureId = parseInt(req.params.id, 10);

  // const teacherCkeck = await modules.teacherInClass(authId, classId);
  // if (!(authType === 3 || teacherCkeck)) {
  //   throw modules.errorBuilder.default('Permission Denied', 403, true);
  // }
  // const lectures = await modules.models.lecture.findAll({
  //   where : { class_id : classId }
  // });
  // let promises = [];
  
  // for ( let i = 0 ; i < lectures.length ; i += 1 ) { 
  //   promises.push(coverage.participationCoverage(lectures[i].lecture_id));  
  // }
  // await promises.all
  let result = await coverage.participationCoverage(lectureId);
  res.json(result);
  // 답안 제출시 문항별 문항 키워드 이해도 커버리지, 문항 키워드 중요도 득점 비율 계산
}));
// lectureId
router.get('/keyword-students/:id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res) => {
  
  const lectureId = parseInt(req.params.id, 10);
  
  const sql = `select * from student_lecture_keyword_coverages sc, users u where sc.student_id = u.user_id and sc.lecture_id = ${lectureId}`;
  const result = await db.getQueryResult(sql);

  res.json(result);
}));
router.get('/keyword-items/:id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next )=> {

  const lectureId = parseInt(req.params.id, 10);

  const k_sql = `select keyword from lecture_keywords where lecture_id = ${lectureId}`;
  const q_sql = `select li.lecture_item_id, li.name, qk.question as content, qk.score_portion, qk.keyword from 
  (select q.question_id,q.lecture_item_id, q.question, qk.score_portion, qk.keyword  from questions q , question_keywords qk where q.question_id = qk.question_id
     and qk.lecture_id = ${lectureId} )qk, lecture_items li where qk.lecture_item_id = li.lecture_item_id`;
  const s_sql = `select li.lecture_item_id, li.name, s.\`comment\` as content ,s.score, s.keyword, s.score_portion from lecture_items li,( select s.\`comment\`, s.score,
   s.lecture_item_id, sk.keyword, sk.score_portion from surveys s, survey_keywords sk where s.survey_id = sk.survey_id and sk.lecture_id = ${lectureId} ) s 
   where li.lecture_item_id = s.lecture_item_id`;
  const n_sql = `select keyword, li.lecture_item_id, li.name, li.lecture_id ,qk.score_portion  from note_keywords qk,
  lecture_items li where qk.lecture_item_id = li.lecture_item_id and li.lecture_id = ${lectureId}`

  const student_sql = `select distinct u.name, r.student_id, r.lecture_id ,r.o , r.keyword, r.res from users u,(select student_id, lecture_id, \`order\` as o, keyword, item_keyword_understanding_score_ratio/item_keyword_score_ratio as res
  from student_lecture_understanding_logs where lecture_id = ${lectureId} and lecture_start_id = (select max(lecture_start_id) as ls_id from lecture_starts where lecture_id = ${lectureId} ))r where u.user_id = r.student_id`

  const keyword_result_sql = `select res1.keyword, u.name, res1.score from (
    select  keyword, student_id, avg(score) score from(
      select lecture_id, keyword, student_id, item_id,  ratio as score  from 
      (select lecture_id, keyword, lecture_item_id, score_portion from question_keywords where lecture_id = ${lectureId}) ke,
      (select student_id, item_id, ratio from student_answer_logs where lecture_id = ${lectureId}) st
      where ke.lecture_item_id=st.item_id 
    ) res group by lecture_id, keyword, student_id
  )res1, users u where res1.student_id = u.user_id 
  `;

  


  let k_result,q_result,n_result,s_result,student_result, keyword_result;
  await Promise.all([
    new Promise(function(resolve,reject){
      resolve(db.getQueryResult(k_sql));
    }),
    new Promise(function(resolve,reject){
      resolve(db.getQueryResult(q_sql));
    }),
    new Promise(function(resolve,reject){
      resolve(db.getQueryResult(s_sql));
    }),
    new Promise(function(resolve,reject){
      resolve(db.getQueryResult(n_sql));
    }),
    new Promise(function(resolve,reject){
      resolve(db.getQueryResult(student_sql));
    }),
    new Promise(function(resolve, reject){
      resolve(db.getQueryResult(keyword_result_sql));
    }),
  ]).then(function(values){

    k_result = values[0];
    q_result = _.groupBy(values[1],'keyword');
    s_result = _.groupBy(values[2],'keyword');;
    n_result = _.groupBy(values[3],'keyword');
    student_result = _.groupBy(values[4],'keyword')
    keyword_result = _.groupBy(values[5], 'keyword')

  })


  console.log()
  let responseResult = [];
  for ( let i = 0 ; i < k_result.length ; i += 1 ){
    let obj = {"keyword" : k_result[i].keyword };
    obj['survey'] = s_result[k_result[i].keyword];
    obj['question'] = q_result[k_result[i].keyword];
    obj['note'] = n_result[k_result[i].keyword];
    obj['students'] = student_result[k_result[i].keyword];
    obj['students2'] = keyword_result[k_result[i].keyword];

    responseResult.push(obj); 
    // obj['practice']
  }

  res.json({"result": responseResult});
  // // avg 
  // const sql = `select class_keyword as keyword, sum(item_keyword_understanding_ratio)/ count(*) as avg from student_lecture_keyword_coverages 
  // where lecture_id = ${lectureId} group by class_keyword`
  // const avgResult = await db.getQueryResult(sql);
  

  // // individual
  // const sql1 = `select users.name, cover.class_keyword keyword, cover.item_keyword_understanding_ratio as ratio from
  //  student_lecture_keyword_coverages cover , users  where cover.lecture_id = ${lectureId}  and users.user_id = cover.student_id order by keyword`
  // const studentResult = await db.getQueryResult(sql1);

  // const studentsResult = _.groupBy(studentResult, function(student){
  //   return student.keyword;
  // });

  // res.json({avg: avgResult, studentsResult});
}))
router.get('/student_result/:id', modules.auth.tokenCheck, modules.asyncWrapper(async( req,res) => {

  const lectureId = parseInt(req.params.id, 10);
  const students = `select * from lectures`
  const sql1 = `select * from lecture_items where lecture_id = ${lectureId}`;
  
  const result1 = await db.getQueryResult(sql1);

  const sql2 = `SELECT u.user_id, u.name FROM users u WHERE u.user_id IN (SELECT uc.user_id FROM lectures l , user_classes uc WHERE l.class_id = uc.class_id AND l.lecture_id = ${lectureId})`;
  const result2 = await db.getQueryResult(sql2);

  const sql3 = `select * from lecture_item_response_logs where lecture_id = ${lectureId}`;
  const result3 = await db.getQueryResult(sql3);

  const sql4 = `select * from student_answer_logs where lecture_id = ${lectureId}`;
  const result4 = await db.getQueryResult(sql4);

  
  

  
  var studentList = [];
  for ( let i = 0 ; i < result2.length ; i += 1 ) {
    studentList.push({'student_id': result2[i].user_id, 'name': result2[i].name });
  }
  
  let itemLogs = _.groupBy(result3,'student_id');
  for ( let i = 0 ; i < studentList.length ; i += 1 ) {
    studentList[i]['items'] = itemLogs[studentList[i].student_id];
  }

  // 참여도랑 이해도는 log 여부와 ratio 로 대체,
  // 집중도 삽입 코드 예정
  res.json(studentList);


}));
module.exports = router;