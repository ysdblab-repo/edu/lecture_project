const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    classId,
    userIdAttitudeArray,
  } = req.body;

  // 유효성 검사
  if (Array.isArray(userIdAttitudeArray) === false) {
    throw new Error('userIdAttitudeArray: 부적절한 형식: 배열이 아닙니다. ');
  }
  for (let i = 0; i < userIdAttitudeArray.length; i += 1) {
    if (userIdAttitudeArray[i].userId === undefined || userIdAttitudeArray[i].attitude === undefined) {
      throw new Error('userIdAttitudeArray: 부적절한 형식: userId 또는 attitude 값이 없는 인자를 입력받았습니다. ');
    }
  }
  for (let i = 0; i < userIdAttitudeArray.length; i += 1) {
    const where = {
      class_id: classId,
      user_id: userIdAttitudeArray[i].userId,
    };
    await modules.updateOrCreate(modules.models.attitude, where, {
      class_id: classId,
      user_id: userIdAttitudeArray[i].userId,
      attitude: userIdAttitudeArray[i].attitude,
    });
  }

  res.json({ success: true, size: userIdAttitudeArray.length });
}));

router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    classId,
  } = req.query;

  const userIdAttitudeArray = await modules.models.attitude.findAll({
    attributes: ['user_id', 'attitude'],
    where: { class_id: classId },
  });

  res.json({ success: true, classId, userIdAttitudeArray });
}));

module.exports = router;
