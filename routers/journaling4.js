const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');

router.post('/question_item', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

    const {
    class_id,
    lecture_id,
    item_result,
  } = req.body;

  // 유효성 검사
  if (Array.isArray(item_result) === false) {
    throw new Error('item_result: 부적절한 형식: 배열이 아닙니다. ');
  }
  for (let i = 0; i < item_result.length; i += 1) {
    if (item_result[i].journaling_type === undefined
      || item_result[i].lecture_item_id === undefined
      || item_result[i].absolute_concentration === undefined
      || item_result[i].relative_concentration === undefined
      || item_result[i].absolute_understanding === undefined
      || item_result[i].relative_understanding === undefined
      || item_result[i].absolute_participation === undefined) {
      throw new Error('item_result: 부적절한 형식: 값이 없는 인자를 입력받았습니다. ');
    }
  }

  for (let i = 0; i < item_result.length; i += 1) {
    const where = {
      class_id,
      lecture_id,
      student_id: item_result[i].student_id,
      lecture_item_id: item_result[i].lecture_item_id,
      question_id: item_result[i].question_id,
    };
    await modules.updateOrCreate(modules.models.question_item_journaling, where, {
      absolute_participation: item_result[i].absolute_participation,
      relative_participation: item_result[i].relative_participation,
      absolute_understanding: item_result[i].absolute_understanding,
      relative_understanding: item_result[i].relative_understanding,
      absolute_concentration: item_result[i].absolute_concentration,
      relative_concentration: item_result[i].relative_concentration,
      journaling_type: item_result[i].journaling_type,
      class_id,
      lecture_id,
      lecture_item_id: item_result[i].lecture_item_id,
      question_id: item_result[i].question_id,
      student_id: item_result[i].student_id,
    });
  }

  res.json({ success: true, size: item_result.length });
}));

router.post('/question_lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const {
    class_id,
    lecture_id,
    lecture_result,
  } = req.body;

  // 유효성 검사
  if (Array.isArray(lecture_result) === false) {
    throw new Error('lecture_result: 부적절한 형식: 배열이 아닙니다. ');
  }
  for (let i = 0; i < lecture_result.length; i += 1) {
    if (lecture_result[i].journaling_type === undefined
      || lecture_result[i].lecture_item_id === undefined
      || lecture_result[i].absolute_concentration === undefined
      || lecture_result[i].relative_concentration === undefined
      || lecture_result[i].absolute_understanding === undefined
      || lecture_result[i].relative_understanding === undefined
      || lecture_result[i].absolute_participation === undefined) {
      throw new Error('lecture_result: 부적절한 형식: 값이 없는 인자를 입력받았습니다. ');
    }
  }

  for (let i = 0; i < lecture_result.length; i += 1) {
    const where = {
      class_id,
      lecture_id,
      student_id: lecture_result[i].student_id,
      lecture_item_id: lecture_result[i].lecture_item_id,
      question_id: lecture_result[i].question_id,
    };
    await modules.updateOrCreate(modules.models.question_lecture_journaling, where, {
      absolute_participation: lecture_result[i].absolute_participation,
      relative_participation: lecture_result[i].relative_participation,
      absolute_understanding: lecture_result[i].absolute_understanding,
      relative_understanding: lecture_result[i].relative_understanding,
      absolute_concentration: lecture_result[i].absolute_concentration,
      relative_concentration: lecture_result[i].relative_concentration,
      journaling_type: lecture_result[i].journaling_type,
      class_id,
      lecture_id,
      lecture_item_id: lecture_result[i].lecture_item_id,
      question_id: lecture_result[i].question_id,
      student_id: lecture_result[i].student_id,
    });
  }

  res.json({ success: true, size: lecture_result.length });
}));

router.post('/question_preview', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const {
    class_id,
    lecture_id,
    preview_result,
  } = req.body;

  // 유효성 검사
  if (Array.isArray(preview_result) === false) {
    throw new Error('preview_result: 부적절한 형식: 배열이 아닙니다. ');
  }
  for (let i = 0; i < preview_result.length; i += 1) {
    if (preview_result[i].journaling_type === undefined
      || preview_result[i].lecture_item_id === undefined
      || preview_result[i].absolute_concentration === undefined
      || preview_result[i].relative_concentration === undefined
      || preview_result[i].absolute_understanding === undefined
      || preview_result[i].relative_understanding === undefined
      || preview_result[i].absolute_participation === undefined) {
      throw new Error('preview_result: 부적절한 형식: 값이 없는 인자를 입력받았습니다. ');
    }
  }

  for (let i = 0; i < preview_result.length; i += 1) {
    const where = {
      class_id,
      lecture_id,
      student_id: preview_result[i].student_id,
      lecture_item_id: preview_result[i].lecture_item_id,
      question_id: preview_result[i].question_id,
    };
    await modules.updateOrCreate(modules.models.question_preview_journaling, where, {
      absolute_participation: preview_result[i].absolute_participation,
      relative_participation: preview_result[i].relative_participation,
      absolute_understanding: preview_result[i].absolute_understanding,
      relative_understanding: preview_result[i].relative_understanding,
      absolute_concentration: preview_result[i].absolute_concentration,
      relative_concentration: preview_result[i].relative_concentration,
      journaling_type: preview_result[i].journaling_type,
      class_id,
      lecture_id,
      lecture_item_id: preview_result[i].lecture_item_id,
      question_id: preview_result[i].question_id,
      student_id: preview_result[i].student_id,
    });
  }

  res.json({ success: true, size: preview_result.length });
}));

router.post('/question_current', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    class_id,
    lecture_id,
    current_result,
  } = req.body;

  // 유효성 검사
  if (Array.isArray(current_result) === false) {
    throw new Error('current_result: 부적절한 형식: 배열이 아닙니다. ');
  }
  for (let i = 0; i < current_result.length; i += 1) {
    if (current_result[i].journaling_type === undefined
      || current_result[i].lecture_item_id === undefined
      || current_result[i].absolute_concentration === undefined
      || current_result[i].relative_concentration === undefined
      || current_result[i].absolute_understanding === undefined
      || current_result[i].relative_understanding === undefined
      || current_result[i].absolute_participation === undefined) {
      throw new Error('current_result: 부적절한 형식: 값이 없는 인자를 입력받았습니다. ');
    }
  }

  for (let i = 0; i < current_result.length; i += 1) {
    const where = {
      class_id,
      lecture_id,
      student_id: current_result[i].student_id,
      lecture_item_id: current_result[i].lecture_item_id,
      question_id: current_result[i].question_id,
    };
    await modules.updateOrCreate(modules.models.question_current_journaling, where, {
      absolute_participation: current_result[i].absolute_participation,
      relative_participation: current_result[i].relative_participation,
      absolute_understanding: current_result[i].absolute_understanding,
      relative_understanding: current_result[i].relative_understanding,
      absolute_concentration: current_result[i].absolute_concentration,
      relative_concentration: current_result[i].relative_concentration,
      journaling_type: current_result[i].journaling_type,
      class_id,
      lecture_id,
      lecture_item_id: current_result[i].lecture_item_id,
      question_id: current_result[i].question_id,
      student_id: current_result[i].student_id,
    });
  }

  res.json({ success: true, size: current_result.length });
}));

router.post('/question_review', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    class_id,
    lecture_id,
    review_result,
  } = req.body;

  // 유효성 검사
  if (Array.isArray(review_result) === false) {
    throw new Error('review_result: 부적절한 형식: 배열이 아닙니다. ');
  }
  for (let i = 0; i < review_result.length; i += 1) {
    if (review_result[i].journaling_type === undefined
      || review_result[i].lecture_item_id === undefined
      || review_result[i].absolute_concentration === undefined
      || review_result[i].relative_concentration === undefined
      || review_result[i].absolute_understanding === undefined
      || review_result[i].relative_understanding === undefined
      || review_result[i].absolute_participation === undefined) {
      throw new Error('review_result: 부적절한 형식: 값이 없는 인자를 입력받았습니다. ');
    }
  }

  for (let i = 0; i < review_result.length; i += 1) {
    const where = {
      class_id,
      lecture_id,
      student_id: review_result[i].student_id,
      lecture_item_id: review_result[i].lecture_item_id,
      question_id: review_result[i].question_id,
    };
    await modules.updateOrCreate(modules.models.question_review_journaling, where, {
      absolute_participation: review_result[i].absolute_participation,
      relative_participation: review_result[i].relative_participation,
      absolute_understanding: review_result[i].absolute_understanding,
      relative_understanding: review_result[i].relative_understanding,
      absolute_concentration: review_result[i].absolute_concentration,
      relative_concentration: review_result[i].relative_concentration,
      journaling_type: review_result[i].journaling_type,
      class_id,
      lecture_id,
      lecture_item_id: review_result[i].lecture_item_id,
      question_id: review_result[i].question_id,
      student_id: review_result[i].student_id,
    });
  }

  res.json({ success: true, size: review_result.length });
}));

router.get('/question_item', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.get('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    lecture_id,
    class_id,
  } = req.query;

  // 유효성 검사
  if (lecture_id === undefined && class_id === undefined) {
    throw new Error('lecture_id와 class_id 반드시 입력해주세요.');
  }
  
  const item_result = await modules.models.question_item_journaling.findAll({
    include: [{
      model: modules.models.user,
      attributes: ['email_id', 'name'],
    },
    {
      model: modules.models.lecture,
      attributes: ['name'],
    },
    {
      model: modules.models.lecture_item,
      attributes: ['name'],
    }],
    where: { class_id, lecture_id },
  });

  res.json({ success: true, item_result });
}));

router.get('/question_lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.get('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    class_id,
  } = req.query;
  // 유효성 검사
  if (class_id === undefined) {
    throw new Error('class_id 반드시 입력해주세요.');
  }

  const lecture_result = await modules.models.question_lecture_journaling.findAll({
    include: [{
      model: modules.models.user,
      attributes: ['email_id', 'name'],
    },
    {
      model: modules.models.lecture,
      attributes: ['name'],
    }],
    where: { class_id },
  });

  res.json({ success: true, lecture_result });  
}));

router.get('/question_lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.get('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    class_id,
  } = req.query;
  // 유효성 검사
  if (class_id === undefined) {
    throw new Error('class_id 반드시 입력해주세요.');
  }

  const lecture_result = await modules.models.question_lecture_journaling.findAll({
    include: [{
      model: modules.models.user,
      attributes: ['email_id', 'name'],
    },
    {
      model: modules.models.lecture,
      attributes: ['name'],
    }],
    where: { class_id },
  });

  res.json({ success: true, lecture_result });  
}));

router.get('/question_item/type', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.get('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    lecture_id,
    class_id,
  } = req.query;
  
  // 유효성 검사
  if (lecture_id === undefined && class_id === undefined) {
    throw new Error('lecture_id와 class_id 반드시 입력해주세요.');
  }
    
  const item_result = [];
  const item_result1 = await modules.models.question_current_journaling.findAll({
    include: [{
      model: modules.models.user,
      attributes: ['email_id', 'name'],
    },
    {
      model: modules.models.lecture,
      attributes: ['name'],
    },
    {
      model: modules.models.lecture_item,
      attributes: ['name', 'order'],
    }],
    where: { class_id, lecture_id },
  });
  item_result1.forEach((x) => {
    item_result.push(x);
  });
  
  const item_result2 = await modules.models.question_preview_journaling.findAll({
    include: [{
      model: modules.models.user,
      attributes: ['email_id', 'name'],
    },
    {
      model: modules.models.lecture,
      attributes: ['name'],
    },
    {
      model: modules.models.lecture_item,
      attributes: ['name', 'order'],
    }],
    where: { class_id, lecture_id },
  });
  item_result2.forEach((x) => {
    item_result.push(x);
  });

  const item_result3 = await modules.models.question_review_journaling.findAll({
    include: [{
      model: modules.models.user,
      attributes: ['email_id', 'name'],
    },
    {
      model: modules.models.lecture,
      attributes: ['name'],
    },
    {
      model: modules.models.lecture_item,
      attributes: ['name', 'order'],
    }],
    where: { class_id, lecture_id },
  });
  item_result3.forEach((x) => {
    item_result.push(x);
  });

  res.json({ success: true, item_result });
}));

router.get('/question_lecture/type', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.get('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    class_id,
  } = req.query;
  // 유효성 검사
  if (class_id === undefined) {
    throw new Error('class_id 반드시 입력해주세요.');
  }

  const lecture_result = await modules.models.question_current_journaling.findAll({
    include: [{
      model: modules.models.user,
      attributes: ['email_id', 'name'],
    },
    {
      model: modules.models.lecture_item,
      attributes: ['name', 'order'],
    }],
    where: { class_id },
  });

  res.json({ success: true, lecture_result });
}));

router.get('/baseInfo', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    lecture_item_id,
    lecture_id,
    class_id,
  } = req.query;

  const result = {};
  const res0 = await modules.models.class.findByPk(class_id, {
    attributes: ['name'],
  });
  result.className = res0.name;
  
  if (lecture_id !== undefined) {
    const res1 = await modules.models.lecture.findByPk(lecture_id, {
      attributes: ['name'],
    });
    result.lectureName = res1.name;
    }
    if (lecture_item_id !== undefined) {
      const res2 = await modules.models.lecture_item.findByPk(lecture_item_id, {
        attributes: ['name'],
      });
      result.itemName = res2.name;
    }

    res.json({ success: true, res: result });
  }));

  router.post('/keyword_journaling', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const {
      class_id,
      lecture_id,
      keyword_result,
    } = req.body;
  // 유효성 검사
  if (Array.isArray(keyword_result) === false) {
    throw new Error('keyword_result: 부적절한 형식: 배열이 아닙니다. ');
  }
  
  
  for (let i = 0; i < keyword_result.length; i += 1) {
    if (keyword_result[i].journaling_type === undefined
      || keyword_result[i].lecture_item_id === undefined
      || keyword_result[i].absolute_concentration === undefined
      || keyword_result[i].relative_concentration === undefined
      || keyword_result[i].absolute_understanding === undefined
      || keyword_result[i].relative_understanding === undefined
      || keyword_result[i].absolute_participation === undefined) {
      throw new Error('keyword_result: 부적절한 형식: 값이 없는 인자를 입력받았습니다. ');
    }
  }
  
  for (let i = 0; i < keyword_result.length; i += 1) {
    const where = {
      class_id,
      lecture_id,
      student_id: keyword_result[i].student_id,
      lecture_item_id: keyword_result[i].lecture_item_id,
      question_id: keyword_result[i].question_id,
    };
    await modules.updateOrCreate(modules.models.question_keyword_journaling, where, {
      keyword: keyword_result[i].keyword, 
      absolute_participation: keyword_result[i].absolute_participation,
      relative_participation: keyword_result[i].relative_participation,
      absolute_understanding: keyword_result[i].absolute_understanding,
      relative_understanding: keyword_result[i].relative_understanding,
      absolute_concentration: keyword_result[i].absolute_concentration,
      relative_concentration: keyword_result[i].relative_concentration,
      journaling_type: keyword_result[i].journaling_type,
      class_id,
      lecture_id,
      lecture_item_id: keyword_result[i].lecture_item_id,
      question_id: keyword_result[i].question_id,
      student_id: keyword_result[i].student_id,
    });
  }
  
  res.json({ success: true, size: keyword_result.length });
  }));
  
  
  router.get('/keyword_journaling', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    // router.get('/', modules.asyncWrapper(async (req, res, next) => {
    const {
      class_id,
      lecture_id
    } = req.query;

    // 유효성 검사
    if (lecture_id === undefined && class_id === undefined ) throw new Error('lecture_id, class_id를 반드시 입력해주세요.');
    
      const item_result = await modules.models.question_keyword_journaling.findAll({
        where: {class_id, lecture_id },
      });
      res.json({ success: true, item_result });
  }));
  

  router.get('/keyword_journaling/class', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    // router.get('/', modules.asyncWrapper(async (req, res, next) => {
    const {
      class_id,
    } = req.query;

    // 유효성 검사
    if (class_id === undefined ) throw new Error(' class_id를 반드시 입력해주세요.');
    
      const item_result = await modules.models.question_keyword_journaling.findAll({
        where: {class_id},
      });
      res.json({ success: true, item_result });
  }));


module.exports = router;
