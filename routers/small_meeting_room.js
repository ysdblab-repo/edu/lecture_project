const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');

// 소그룹방 생성
router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const { name, type, classId, max_occupancy, password, lectureId} = req.body;
  const { authId, authType } = req.decoded;

  // 2021-04-07 lectureID를받아 있을 경우 insert

  const newRoom = await modules.models.small_meeting_room.create({
    class_id: classId,
    user_id: authId,
    type,
    name,
    lecture_id: lectureId,
    max_occupancy,
    password,
  });

  res.json({ success: true, small_meeting_room: newRoom });
}));


// 소그룹방 수정
router.put('/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const room_id = req.params.room_id;
  const { name, max_occupancy, password } = req.body;
  const { authId, authType } = req.decoded;

  const room = await modules.models.small_meeting_room.findByPk(room_id);
  if(!room) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  // permission
  if (!(authType === 3 || authType === 1 || authId === room.user_id)){
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  await modules.models.small_meeting_room.update({
    name,
    max_occupancy,
    password,
  }, {
    where: {
      room_id: room_id,
    }});
  res.json({ success: true });
  
}));

// 소그룹방 삭제
router.delete('/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const room_id = parseInt(req.params.room_id, 10);
  const { authId, authType } = req.decoded;
  const room = await modules.models.small_meeting_room.findByPk(room_id);
  
  if(!room) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  if (!(authType === 3 || authType === 1 || authId === room.user_id)){
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  // 관련 입장 권한 삭제
  await modules.models.small_meeting_room_permission.destroy({
    where: {
      room_id,
    }
  });
  await modules.models.small_group_chatting.destroy({
    where: {
      room_id,
    }
  })
  await modules.models.small_meeting_room.destroy({
    where: {
      room_id,
    }
  });
  res.json({ success: true });
}));

// 소그룹방 목록
router.get('/:class_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId  = parseInt(req.params.class_id, 10);

  const rooms = await modules.models.small_meeting_room.findAll({
    where: {
      class_id: classId,
      type: {in: [0, 2, 3]},
    }
  });
  console.log("rooms: ", rooms);
  // if ( rooms.length === 0) {
  //   throw modules.errorBuilder.default('Room does not exists', 404, true);
  // }

  res.json(rooms);
}));

// 소그룹방 로그 생성
router.post('/log/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const room_id = req.params.room_id;

  const count_user = await modules.models.small_meeting_room_log.findAll({
    where: {
      room_id,
    }
  });

  const count_user_limit = await modules.models.small_meeting_room.findOne({
    where: {
      room_id,
    }
  });
  if (count_user.length === count_user_limit.dataValues.max_occupancy) {
    res.json({success: false, message: 'No'});
  }

  const newLog = await modules.updateOrCreate(
    modules.models.small_meeting_room_log,
    {user_id: authId, room_id: room_id},
    {user_id: authId, room_id: room_id}
  );

  // const newLog = await modules.models.small_meeting_room_log.create({
  //   user_id: authId,
  //   room_id: room_id,
  // });
  res.json({ success: true });
}));

// 소그룹방 로그 삭제
router.delete('/log/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const roomId = parseInt(req.params.room_id, 10);
  const { authId, authType } = req.decoded;

  await modules.models.small_meeting_room_log.destroy({
    where: {
      room_id: roomId,
      user_id: authId,
    },
  });

  res.json({ success: true });
}));

// 입장 검사: 다른 방에 들어갔는지 검사, 방에 들어올 권한이 있는 사람인지, 방에 비밀번호가 걸려있는지 검사 (보안상 실제 번호는 전송하지 않음)
router.get('/entrace_check/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const { authId } = req.decoded;
  const roomId = parseInt(req.params.room_id, 10);

  const duplicate = await modules.models.small_meeting_room_log.findOne({
    where: {
      user_id: authId,
    },
  });

  let isDuplicated = false;
  if (duplicate !== null) isDuplicated = true;

  const right = await modules.models.small_meeting_room_permission.findOne({
    where: {
      room_id: roomId,
      user_id: authId,
    },
  });

  let hasPermission = false;
  if (right !== null) hasPermission = true;

  const room = await modules.models.small_meeting_room.findOne({
    where: {
      room_id: roomId,
    },
  });

  let hasPassword = true;

  if (room.password === null) hasPassword = false;

  res.json({ isDuplicated, hasPermission, hasPassword });
}));

// 입장 검사: 받아온 비밀번호가 실제 비밀번호와 일치하는지 검사
router.get('/password_check/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const { password } = req.query;
  const roomId = parseInt(req.params.room_id, 10);

  const room = await modules.models.small_meeting_room.findOne({
    where: {
      room_id: roomId,
    },
  });

  let correct = false;
  if (room.password === password) {
    correct = true;
  }

  res.json({ correct });
}));

// 소그룹방 현재 인원 계산
router.get(`/log/:room_id`, modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const roomId = req.params.room_id;
  const sql = `SELECT count(*) FROM small_meeting_room_logs WHERE room_id = ${roomId}`;
  const counts = await db.getQueryResult(sql);
  
  res.json({ count: counts[0]['count(*)'] });
}))

// 권한 생성
router.post('/permission/:room_id/:user_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const room_id = req.params.room_id;
  const user_id = req.params.user_id;
  
  const room = await modules.models.small_meeting_room.findByPk(room_id);
  const newPermission = await modules.models.small_meeting_room_permission.create({
    room_id: room_id,
    user_id: user_id,
    class_id: room.class_id,
  });
  res.json({ success: true });
}));
// 권한 삭제 //비밀번호가 생성된 방에 입장할 때 1회 비밀번호가 정답이면 permission 테이블에 로그가 남는다.
// 소그룹방이 삭제되면 해당 로그도 같이 삭제되는 것이 확인.
router.delete('/permission/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {
  const roomId = parseInt(req.params.room_id, 10);

  await modules.models.small_meeting_room_permission.destroy({
    where: {
      room_id: roomId,
    },
  });

  res.json({ success: true });
}));
// 권한 불러오기
router.get('/permission/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const roomId  = parseInt(req.params.room_id, 10);

  const permissions = await modules.models.small_meeting_room_permission.findAll({
    where: {
      room_id: roomId
    }
  });

  res.json(permissions);
}));
// 권한 불러오기 - class의 모든 권한
router.get('/permission/class/:class_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId  = parseInt(req.params.class_id, 10);

  const permissions = await modules.models.small_meeting_room_permission.findAll({
    where: {
      class_id: classId
    }
  });

  res.json(permissions);
}));

module.exports = router;
