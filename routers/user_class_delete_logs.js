const express = require('express');
const router = express.Router();
const sequelize = require('sequelize')
const modules = require('../modules/frequent-modules');
const db = require('../modules/db')

router.get('/classId/:classId/userId/:userId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const classId = parseInt(req.params.classId, 10);
  const userId = parseInt(req.params.userId, 10);
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, classId); // 강사가 class

  if (!(authType === 3 || authType === 4 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  try{
  const user_class_delete_log = await modules.models.user_class_delete_log.create({
      class_id: classId,
      user_id: userId,
      role: 'student',
  });
  }
  catch(err){
    const user_class_delete_log_err = await modules.models.user_class_delete_log.findOne({
      class_id: classId,
      user_id: userId,
      role: 'student',
    });
    //이미 삭제 신청을 완료하여 error가 발생한 경우 log테이블에서 삭제
    await user_class_delete_log_err.destroy();    
  }
    res.json({ success: true });
  
}));

module.exports = router;