const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');

// 강사가 생성 요청을 등록
router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.post('/', modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사

  // user_id, affiliation_id, class_id 매개 받음
  const { authId } = req.decoded;
  const { affiliation_id, type, class_name, comment, subject_type, id_2nd } = req.body;

  let result = [];
  const category = await modules.models.open_space_2nd.findOne({
    where: {
      affiliation_id,
      name: id_2nd
    }
  });

  // 테이블에 넣음
  if (category !== null) {
    result = await modules.models.req_create_class.create({
      subject_type,
      user_id: authId,
      affiliation_id,
      type,
      class_name,
      comment,
      id_2nd: category.dataValues.id,
    });
  } else {
    result = await modules.models.req_create_class.create({
      subject_type,
      user_id: authId,
      affiliation_id,
      type,
      class_name,
      comment,
    });
  }

  if (type === '4') { // 맛보기 과목
    const req_id = result.req_id;

    // 자동 과목 개설
    const newClass = await modules.models.class.create({
      name: class_name,
      summary: comment,
      start_time: new Date(),
      end_time: new Date(),
      view_type: 1,
      sign_up_start_time: new Date(),
      sign_up_end_time: new Date(),
      num: '',
      type: 4,
      master_id: authId,
    });

    await modules.models.user_class.create({
      role: 'teacher',
      class_id: newClass.class_id,
      user_id: authId,
    });

    const inputClassId = await modules.models.req_create_class.findOne({ where: { req_id } });
    inputClassId.status = 3;
    inputClassId.class_id = newClass.class_id;
    await inputClassId.save();
  }

  // 리턴
  res.json({ success: true, result });
}));

// 기관 관리자가 요청 목록을 봄
router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.get('/', modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사
  const { authId, authType } = req.decoded;
  const { user_id } = req.query;
  let { pageNum, limit, status, affiliation_id } = req.query;

  // 기관 관리자일 경우, 본인 기관 아이디 알아내기
  if (authType === 4) {
    // 요청자 id로 기관 id 확보
    const temp = await modules.models.user_belonging.findOne({
      attributes: ['affiliation_id'],
      where: {
        user_id: authId,
      },
    });
    // eslint-disable-next-line prefer-destructuring
    affiliation_id = temp.affiliation_id;
  }

  if (!pageNum) pageNum = 0;
  if (limit) limit = Number.parseInt(limit, 10);
  if (!limit) limit = 10;

  let offset = 0;
  if (pageNum > 1) {
    offset = limit * (pageNum - 1);
  }

  const where = {};
  if (affiliation_id) {
    where.affiliation_id = affiliation_id;
  }
  if (user_id) {
    where.user_id = user_id;
  }
  if (status) {
    where.status = status;
  }

  // 테이블 검색
  const result = await modules.models.req_create_class.findAndCountAll({
    include: [
      {
        attributes: ['affiliation_id', 'description'],
        model: modules.models.affiliation_description,
      },
      {
        attributes: ['email_id', 'name'],
        model: modules.models.user,
      },
    ],
    where,
    offset,
    limit,
  });

  // 리턴
  res.json({ success: true, result: result.rows, count: result.count });
}));

// 기관 관리자가 강사의 생성 요청을 수락
router.post('/accept', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.post('/accept', modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사

  // req_id 매개 받음
  const { req_id } = req.body;

  // 테이블 갱신
  const result = await modules.models.req_create_class.update({
    status: 1,
  }, { where: { req_id } });

  // TODO: 만든 과목에 대한 후속 처리

  // 리턴
  res.json({ success: true, result });
}));

// 기관 관리자가 강사의 생성 요청을 거절
router.post('/reject', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.post('/reject', modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사

  // req_id 매개 받음
  const { req_id } = req.body;

  // 테이블 갱신
  const result = await modules.models.req_create_class.update({
    status: 2,
  }, { where: { req_id } });

  // TODO: 만든 과목에 대한 후속 처리

  // 리턴
  res.json({ success: true, result });
}));

// 강사가 요청 목록을 봄
router.get('/userId/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // router.get('/', modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사
  const { authId, authType } = req.decoded;
  const { user_id } = req.query;

  const where = {};
  if (user_id) {
    where.user_id = user_id;
  }

  // 테이블 검색
  const result = await modules.models.req_create_class.findAndCountAll({
    include: [
      {
        attributes: ['affiliation_id', 'description'],
        model: modules.models.affiliation_description,
      },
      {
        attributes: ['email_id', 'name'],
        model: modules.models.user,
      },
    ],
    where,
  });

  // 리턴
  res.json({ success: true, result: result.rows, count: result.count });
}));

// 기관 관리자 생성 시 등록
router.post('/admin', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사

  // user_id, affiliation_id, class_id 매개 받음
  const { authId } = req.decoded;
  const { affiliation_id, type, class_name, comment, subject_type } = req.body;

  // 테이블에 넣음
  const result = await modules.models.req_create_class.create({
    subject_type,
    affiliation_id,
    type,
    class_name,
    comment,
    status: 1
  });

  // 리턴
  res.json({ success: true, result });
}));

// 기관 관리자 주제 생성 시 class_passwords_by_admin 테이블에 password 추가
router.post('/class_passwords_by_admin', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const {
    password,
    req_id,
  } = req.body;

  const temp = await modules.models.user_belonging.findOne({
    attributes: ['affiliation_id'],
    where: {
      user_id: authId,
    },
  });

  const class_pw = await modules.models.class_passwords_by_admin.create({
    password,
    req_id,
    affiliation_id: temp.affiliation_id,
    create_check: 0,
  });


  res.json({ success: true, class_pw, });
}));

// password 중복 체크
router.get('/class/password/:password', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const password = req.params.password;
  const { authId } = req.decoded;

  const class_password = await modules.models.class_passwords_by_admin.findOne({
    where: {
      password: password,
    },
  });

  res.json({ class_password });
}));

router.get('/class_password', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  let list = [];
  if (authType === 4) {
    const temp = await modules.models.user_belonging.findOne({
      attributes: ['affiliation_id'],
      where: {
        user_id: authId,
      },
    });
    list = await modules.models.class_passwords_by_admin.findAll({
      include: {
        model: modules.models.req_create_class,
      },
      where: {
        create_check: 0,
        affiliation_id: temp.affiliation_id,
      },
    });
  } else {
    list = await modules.models.class_passwords_by_admin.findAll({
      include: {
        model: modules.models.req_create_class,
      },
      where: {
        create_check: 0,
      },
    });
  }

  res.json({ list });
}));

router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const id = parseInt(req.params.id, 10);

  const result = await modules.models.req_create_class.findByPk(id);

  res.json({ result });
}));

router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const reqClass = await modules.models.req_create_class.findByPk(id);

  const discussion = await modules.models.req_create_class.update(
    { user_id: authId },
    { where: { req_id: id } },
  );

  res.json({ success: true });
}));

router.delete('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  // 인증 생략
  await modules.models.req_create_class.destroy({
    where: {
      req_id: id,
    },
  });

  res.json({ success: true });
}));

module.exports = router;
