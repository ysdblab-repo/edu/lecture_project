const express = require('express');
const router = express.Router();

const modules = require('../modules/frequent-modules');


/*Insert into teacher_streaming_keys*/
router.post('/', modules.asyncWrapper(async (req, res, next) => {
  await modules.models.lecture.create(req.body);

  res.send({
    success: true,
  });

}));

module.exports = router;
