const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const analysis = require('../modules/analysis');
const time_handler = require('../modules/time-handler');
const execTimeout10 = require('../src/utility');

function tomydate(datestr)
{
	d = new Date(datestr);
	return time_handler.toMysqlFormat(d)
}
function tomydate_add9(datestr)
{
	d = new Date(datestr);
	d = time_handler.addHours(d,9);
	return time_handler.toMysqlFormat(d)
}


router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureItemId } = req.body;


  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.lecture,
    },
  });
  console.log(lectureItem.lecture.class_id);
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }


  console.log("lecute_item_id: ", lectureItemId);
  const practice = await modules.models.lecture_code_practice.create({
    lecture_item_id: lectureItemId,
    user_id: authId,
  });

  const usersInClass = await modules.models.user_class.findAll({
    where: {
      class_id: lectureItem.lecture.class_id
    },
    include: {
      model: modules.models.user,
    },
  });
  
  // 2021-03-18 실습 폴더 생성
  var createEduDir = "sudo docker exec class" + lectureItem.lecture.class_id + " su educator -c 'mkdir -p /home/educator/practice_item_" + lectureItemId +"'";
  try{
    await execTimeout10(createEduDir);
  }catch(e){
    console.log(e);
  }

  for (const user of usersInClass){
    var createStuDir = "sudo docker exec class" + lectureItem.lecture.class_id + " su '"+ user.user.email_id+"' -c 'mkdir -p /home/'"+ user.user.email_id +"'/practice_item_" + lectureItemId + "'";
    try{      
      await execTimeout10(createStuDir);
    }catch(e){
      console.log(e);
    }    
  };

  // 2021-03-18 실습 파일 생성
  var createEduIpynb = "sudo docker exec class" + lectureItem.lecture.class_id + " su educator -c 'cp /home/Untitled.ipynb /home/educator/practice_item_" + lectureItemId +"/"+ lectureItemId + ".ipynb'";
  try{
    await execTimeout10(createEduIpynb);
  }catch(e){
    console.log(e);
  }
  /*
  for (const user of usersInClass){
    var createStuIpynb = "sudo docker exec class" + lectureItem.lecture.class_id + " su '" + user.user.email_id + "' -c 'cp /home/Untitled.ipynb /home/'" + user.user.email_id + "'/practice_item_" + lectureItemId + "/" + lectureItemId + ".ipynb'";
    try{
      await execTimeout10(createStuIpynb);
    }catch(e){
      console.log(e);
    }
  };
  */
  res.json({ success: true, practice_id: practice.lecture_code_practice_id });

}));
router.post('/getStudent', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const { email_id, item_id, class_id } = req.body;
  const userSql = `SELECT user_id FROM users WHERE email_id = '${email_id}'`;
  const result = await db.getQueryResult(userSql);
  if(!result){

  }
  const student_id = result[0]['user_id'];
  const class_name = "class" + class_id;
  // console.log(student_email);
  await execTimeout10("sudo docker exec " + class_name + " cp /home/" + email_id + "/practice_item_" + item_id + "/Untitled.ipynb /home/educator/codecopy/"+ item_id + "_" + student_id + ".ipynb");
  
  await execTimeout10("sudo docker exec " + class_name + " python /downloads/test.py " + student_id +" " +item_id + " " + class_id);
  
  const resultSql = `SELECT * FROM student_jupyter_code WHERE user_id = ${student_id} AND item_id = ${item_id} ORDER BY id DESC limit 1`
  const codeResult = await db.getQueryResult(resultSql);
  res.json({ codeResult });

}));
router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const practice_id = parseInt(req.params.id, 10);
  const code = req.body.code;

  const lecture_code_practice = await modules.models.lecture_code_practice.findByPk(practice_id);

  const lectureItem = await modules.models.lecture_item.findByPk(lecture_code_practice.lecture_item_id, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }

  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const practice = await modules.models.lecture_code_practice.update({
    code : code,
  }, {
    where: {
      lecture_code_practice_id: practice_id,
    }});

  res.json({ success: true });
}));

router.get('/:id',  modules.asyncWrapper(async (req, res, next) =>
{
 // const { authId, authType } = req.decoded;
  const practice_id = parseInt(req.params.id, 10);

  const lecture_code_practice = await modules.models.lecture_code_practice.findByPk(practice_id);

  const lectureItem = await modules.models.lecture_item.findByPk(lecture_code_practice.lecture_item_id, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  /*
  TODO:
  */
  const code = lecture_code_practice.code;
  const is_jupyter = lecture_code_practice.is_jupyter;
	if(is_jupyter) // 주피터 노트북인 경우
	{
		const q = 'select spl.code, spl.user_id, u.name from student_practice_logs spl \
		join users u on u.user_id  = spl.user_id \
		where spl.modified_time > "'+tomydate_add9(lecture_code_practice.activated_time)+'" and spl.modified_time < "'+tomydate_add9(lecture_code_practice.deactivated_time)+'" \
		order by spl.modified_time desc'
		console.log(q)
		const users = await db.getQueryResult(q);
		const u_json = {};
		for (var i = 0; i< users.length; i++)
		{
			if (typeof u_json[users[i].user_id] == 'undefined')
				 u_json[users[i].user_id] = users[i]
		}
		const new_users = []
		for (var key in u_json)
		{
			if (u_json.hasOwnProperty(key))
			{
				lc = analysis.LCS(code.toUpperCase(), u_json[key].code.toUpperCase());
				u_json[key].practice_score = Math.min(code.length, lc.length) / code.length * 100
				new_users.push(u_json[key])
			}
		}

		res.json({ success: true, 'users': new_users });
	}
	else
	{
		const q = 'select GROUP_CONCAT(spdl.string_keyboard_type order by spdl.time asc SEPARATOR "") as type, spdl.user_id from lectures l  join classes c on c.class_id = l.class_id join user_classes uc on uc.class_id = c.class_id and uc.role = "student" join student_program_detail_logs spdl on spdl.user_id = uc.user_id and spdl.time > "' + tomydate(lecture_code_practice.activated_time)+'" and  spdl.time < "' + tomydate(lecture_code_practice.deactivated_time)+'"  where l.lecture_id = '+lectureItem.lecture_id+' and spdl.num_keyboard_type !=0 group by spdl.user_id'
		const users = await db.getQueryResult(q);
		for (var i = 0; i< users.length; i++)
		{
			users[i].type = analysis.keylogger_parsing(users[i].type)
			lc = analysis.LCS(code.toUpperCase(), users[i].type);
			users[i].practice_score = Math.min(code.length, lc.length) / code.length * 100
		}
		res.json({ success: true, users });
	}


}));

const keywordUpsert = async (values, condition) => modules.models
  .lecture_code_practice_keyword.findOne({ where: condition })
  .then((obj) => {
    if (obj) {
      return obj.update(values);
    } else {
      return modules.models.lecture_code_practice_keyword.create(values);
    }
  });

router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const practiceId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const practice = await modules.models.lecture_code_practice.findByPk(practiceId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
        include: {
          model: modules.models.class,
        },
      },
    },
  });
  if (!practice) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, practice.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  let scoreSum = 0;
  const dataLen = data.length;
  for (let i = 0; i < dataLen; i += 1) {
    scoreSum += parseInt(data[i].score, 10);
  }
  const promises = [];
  for (let i = 0; i < dataLen; i += 1) {
    promises.push(keywordUpsert(
      { lecture_code_practice_id: practice.lecture_code_practice_id, lecture_id: practice.lecture_item.lecture_id, lecture_item_id: practice.lecture_item_id, keyword: data[i].keyword, score_portion: data[i].score },
      { lecture_code_practice_id: practice.lecture_code_practice_id, keyword: data[i].keyword },
    ));
  }
  Promise.all(promises);
  practice.score = scoreSum;
  await practice.save();

  res.json({ success: true, score: scoreSum });

  // await modules.coverageCalculator.coverageAll(question.lecture_item.lecture_id, question.lecture_item.lecture.class_id);
}));

router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const practiceId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const practiceKeywords = (await modules.models.lecture_code_practice.findByPk(practiceId, {
    include: { model: modules.models.lecture_code_practice_keyword },
  })).lecture_code_practice_keywords;

  if (!practiceKeywords) {
    throw modules.errorBuilder.default('Not Found -', 404, true);
  }

  res.json(practiceKeywords);
}));

router.delete('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const practiceId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const practice = await modules.models.lecture_code_practice.findByPk(practiceId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!practice) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, practice.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.lecture_code_practice_keyword.destroy({
    where: {
      lecture_code_practice_id: practiceId,
    },
  });

  res.json({ success: true });
}));




module.exports = router;
