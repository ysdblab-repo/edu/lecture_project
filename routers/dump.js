const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const util = require('util');

const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
const dump = require('../modules/dump');

router.get('/', modules.asyncWrapper(async (req, res) => {
  const thePath = path.join(modules.config.appRoot, '/dumps');
  if (!fs.existsSync(thePath)) {
    res.json({ list: [] });
  }
  const rs = fs.readdirSync(thePath);

  res.json({ list: rs });
}));

router.get('/dump', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  await dump.dump();
  const thePath = path.join(modules.config.appRoot, '/dumps');
  const rs = fs.readdirSync(thePath);

  res.json({ list: rs });
}));

router.post('/reset', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { sel } = req.body;

  res.json({ success: true });

  await dump.reset();
  await dump.restore(sel);
}));

router.post('/delete', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { sel } = req.body;

  const thePath = path.join(modules.config.appRoot, '/dumps', sel);
  if (fs.existsSync(thePath)) {
    fs.unlinkSync(thePath);
  }
  const thePath2 = path.join(modules.config.appRoot, '/dumps');
  const rs = fs.readdirSync(thePath2);

  res.json({ list: rs });
}));

router.post('/rename', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { oldName, newName } = req.body;

  const changedName = newName.replace(' ', '_');
  const oldPath = path.join(modules.config.appRoot, '/dumps', oldName);
  const newPath = path.join(modules.config.appRoot, '/dumps', changedName);
  fs.renameSync(oldPath, newPath);

  const thePath = path.join(modules.config.appRoot, '/dumps');
  const rs = fs.readdirSync(thePath);

  res.json({ list: rs });
}));

module.exports = router;
