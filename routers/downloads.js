const express = require('express');
const router = express.Router();
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const fs = require('fs');
const shell = require('shelljs');
const { Op } = require("sequelize");
router.get('/download_logfile', modules.asyncWrapper(async (req, res, next) => {
  res.download('public/download/preswot.zip');
}));

router.get('/download_keyboard_watcher', modules.asyncWrapper(async (req, res, next) => {
  res.download('public/download/keyboard_watcher.zip');
}));

router.get('/edu_cam', modules.asyncWrapper(async (req, res, next) => {
  res.download('public/download/edu_cam.zip');
}));

router.get('/studentListForm', modules.asyncWrapper(async (req, res, next) => {
  res.download('public/download/studentListForm.xlsx');
}));

router.get('/studentEvaluation/:lecture_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lecture_id } = req.params;

  const { class_id } = (await modules.models.lecture.findOne({
    where: {
      lecture_id
    }
  })).dataValues;

  const teacherCheck = await modules.teacherInClass(authId, class_id);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  //estimate에서 평가 기록 찾아오기.
  const student_question_esimates = await Promise.all((await modules.models.student_question_estimate.findAll({
    where: {
      lecture_id
    },
  })).map(async data => {
    const user = (await modules.models.user.findOne({
      where: {
        user_id: data.dataValues.student_id
      }
    })).dataValues;

    return {
      student_question_id: data.dataValues.student_question_id,
      name: user.name,
      email: user.email_id,
      score: data.dataValues.score,
      comment: data.dataValues.comment,
    };
  }));

  const student_questions = await Promise.all((await modules.models.student_question.findAll({
    where: {
      student_question_id: {
        [Op.or]: student_question_esimates.map(estimates => estimates.student_question_id)
      }
    }
  })).map(async data => {
    const user = (await modules.models.user.findOne({
      where: {
        user_id: data.dataValues.student_id
      }
    })).dataValues;
    return {
      student_question_id: data.dataValues.student_question_id,
      type: data.dataValues.type, //파일 형식 구분을 위해서 type을 추가로 받아온다.
      question_name: data.dataValues.name,
      question_question: data.dataValues.question,
      question_choice: data.dataValues.choice,
      name: user.name,
      email: user.email_id
    };
  }));

  const sqe = student_question_esimates.map(sqe => {
  const sq = student_questions.find(sq => sq.student_question_id === sqe.student_question_id);

    return {
      nuga: sqe.name,
      nugaEmail: sqe.email,
      type: sq.type,
      question_name: sq.question_name,
      question_question: sq.question_question,
      question_choice: sq.question_choice,
      nuguege: sq.name,
      nuguegeEmail: sq.email,
      score: sqe.score,
      comment: sqe.comment,
    };
  });
  // const headers = ['평가자 이름', '평가자 이메일', '피 평가자 이름', '피 평가자 이메일', '점수', '한줄평'];
  // const content = headers.map(str => `\"${str}\"`).join(',') + '\n' + sqe.map(sqe => Object.values(sqe).map(str => `\"${String(str)}\"`).join(',')).join('\n');
  const content = sqe;

  res.json({
    name: 'studentEvaluationResult.xlsx',
    content,
  });
}));

router.get('/studentResult/:lecture_id/:type', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lecture_id } = req.params;
  const type = Number(req.params.type);

  const { class_id } = (await modules.models.lecture.findOne({
    where: {
      lecture_id
    }
  })).dataValues;

  const teacherCheck = await modules.teacherInClass(authId, class_id);

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  const lectureTypeString = [{
    ko: '유인'
  }, {
    ko: '무인[단체]'
  }, {
    ko: '무인[개인]'
  }, {
    ko: '무인[개인]'
  }];

  const lectureItemTypeString = [{
    ko: '문항',
    en: 'Question'
  }, {
    ko: '설문',
    en: 'Survey'
  }, {
    ko: '실습',
    en: 'Practice'
  }, {
    ko: '토론',
    en: 'Discussion'
  }, {
    ko: '자료',
    en: 'Note'
  }, {
    ko: '코딩 과제',
    en: 'coding'
  }, {
    ko: '개인화 문항',
    en: 'Individual question'
  }, {
    ko: '개인화 문항',
    en: 'Individual question'
  }];

  const lectureItemSubTypeString = [[
    {
      ko: '객관'
    },
    {
      ko: '단답'
    },
    {
      ko: '서술'
    },
    {
      ko: 'SW'
    },
    {
      ko: 'SQL'
    },
    {
      ko: ''
    },
    {
      ko: '멀티미디어'
    },
    {
      ko: '연결형'
    },
  ], [
    {
      ko: '객관'
    },
    {
      ko: '서술'
    },
  ]];

  let content = '';
  const headers = ['세션명', '세션유형', '아이템명', '아이템 유형', '학생명', '학생이메일', '제출답안', '점수'];
  if (type === 1) {
    const ss = await Promise.all((await modules.models.student_survey.findAll({
      where: {
        lecture_id
      },
      include: [{
        model: modules.models.user
      }]
    })).map(async data => {
      const lecture = await modules.models.lecture.findOne({
        where: {
          lecture_id: data.dataValues.lecture_id
        }
      });

      const lecture_item = await modules.models.lecture_item.findOne({
        where: {
          lecture_item_id: data.dataValues.item_id
        },
        include: [{
          model: modules.models.survey,
          required: false,
        }]
      });
      return {
        lecture_name: lecture.dataValues.name,
        lecture_type: lectureTypeString[lecture.dataValues.type].ko,
        item_name: lecture_item.dataValues.name,
        item_type: `${lectureItemTypeString[lecture_item.dataValues.type].ko}${lecture_item.dataValues.type < 2 ? `-${lectureItemSubTypeString[lecture_item.dataValues.type][lecture_item.dataValues.surveys[0].dataValues.type].ko}` : ''}`,
        user_name: data.dataValues.user.dataValues.name,
        user_email: data.dataValues.user.dataValues.email_id,
        answer: data.dataValues.answer,
        score: data.dataValues.score === null ? '' : data.dataValues.score,
      };
    }));
    content = headers.map(str => `\"${str}\"`).join(',') + '\n' + ss.map(ss => Object.values(ss).map(str => `\"${String(str).split(modules.config.separator).join(', ')}\"`).join(',')).join('\n');
  } else if (type === 0) {
    const sal = (await modules.models.student_answer_log.findAll({
      where: {
        lecture_id
      },
      include: [{
        model: modules.models.user
      }, {
        model: modules.models.lecture_item,
        include: [{
          model: modules.models.question,
          required: false,
        }]
      }, {
        model: modules.models.lecture
      }]
    }))
    .filter(data => data.dataValues.lecture_item.dataValues.type === Number(type))
    .map(data => ({
      lecture_name: data.dataValues.lecture.name,
      lecture_type: lectureTypeString[data.dataValues.lecture.type].ko,
      item_name: data.dataValues.lecture_item.dataValues.name,
      item_type: `${lectureItemTypeString[data.dataValues.lecture_item.dataValues.type].ko}${data.dataValues.lecture_item.dataValues.type < 2 ? `-${lectureItemSubTypeString[data.dataValues.lecture_item.dataValues.type][data.dataValues.lecture_item.dataValues.questions[0].dataValues.type].ko}` : ''}`,
      user_name: data.dataValues.user.dataValues.name,
      user_email: data.dataValues.user.dataValues.email_id,
      answer: data.dataValues.answer,
      score: data.dataValues.score,
    }));
    content = headers.map(str => `\"${str}\"`).join(',') + '\n' + sal.map(sal => Object.values(sal).map(str => `\"${String(str).split(modules.config.separator).join(', ')}\"`).join(',')).join('\n');
  }

  res.json({
    name: `student${lectureItemTypeString[type].en}Result.csv`,
    content,
  });
}));

router.get('/keywordListForm', modules.asyncWrapper(async (req, res, next) => {
  res.download('public/download/keywordListForm.xlsx');
}));

router.get('/questionForm', modules.asyncWrapper(async (req, res, next) => {
  res.download('public/download/questionForm.xlsx');
}));
/*
const user_id = req.params.user_id;
const user = await modules.models.user.findOne({
  where: {
    user_id: user_id,
  },
});
const log_verify_code = user.log_verify_code;
fs.writeFile("download/log_program/cert", log_verify_code, function(err) {
  if(err) {
      return console.log(err);
  }

// Async call to exec()
shell.exec('rm -rf /home/ubuntu/academy-test/back/lecture_project/download/log_program/aa.zip', function(status, output) {
  console.log('1111111111111')
  shell.exec('zip -rj /home/ubuntu/academy-test/back/lecture_project/download/log_program/aa.zip /home/ubuntu/academy-test/back/lecture_project/download/log_program/*', function(status, output) {
    console.log('22222222')

  });
});
*/




//}));

module.exports = router;
