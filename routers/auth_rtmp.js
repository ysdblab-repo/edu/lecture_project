const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');

router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  console.log('req.body = ', req.body);
  console.log('req.query = ', req.query);
  res.json({ success: true });
}));

router.post('/question_item', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

    const {
    class_id,
    lecture_id,
    item_result,
  } = req.body;

  // 유효성 검사
  if (Array.isArray(item_result) === false) {
    throw new Error('item_result: 부적절한 형식: 배열이 아닙니다. ');
  }
  for (let i = 0; i < item_result.length; i += 1) {
    if (item_result[i].journaling_type === undefined
      || item_result[i].lecture_item_id === undefined
      || item_result[i].absolute_concentration === undefined
      || item_result[i].relative_concentration === undefined
      || item_result[i].absolute_understanding === undefined
      || item_result[i].relative_understanding === undefined
      || item_result[i].absolute_participation === undefined) {
      throw new Error('item_result: 부적절한 형식: 값이 없는 인자를 입력받았습니다. ');
    }
  }

  for (let i = 0; i < item_result.length; i += 1) {
    const where = {
      class_id,
      lecture_id,
      student_id: item_result[i].student_id,
      lecture_item_id: item_result[i].lecture_item_id,
      question_id: item_result[i].question_id,
    };
    await modules.updateOrCreate(modules.models.question_item_journaling, where, {
      absolute_participation: item_result[i].absolute_participation,
      relative_participation: item_result[i].relative_participation,
      absolute_understanding: item_result[i].absolute_understanding,
      relative_understanding: item_result[i].relative_understanding,
      absolute_concentration: item_result[i].absolute_concentration,
      relative_concentration: item_result[i].relative_concentration,
      journaling_type: item_result[i].journaling_type,
      class_id,
      lecture_id,
      lecture_item_id: item_result[i].lecture_item_id,
      question_id: item_result[i].question_id,
      student_id: item_result[i].student_id,
    });
  }

  res.json({ success: true, size: item_result.length });
}));

module.exports = router;
