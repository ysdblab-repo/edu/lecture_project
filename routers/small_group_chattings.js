const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const path = require('path');
var multer  = require('multer');
const { include } = require('underscore');

// 실시간 채팅 가져오기
router.get('/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const roomId = parseInt(req.params.room_id, 10);

  const chatData = await modules.models.small_group_chatting.findAll({
    where: {
      room_id: roomId,
    },
    include: [
      { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
    ],
  });
  res.json({ chatData });
}));

// 해당 강의 채팅 내용 삭제 200511
router.delete('/:room_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const roomId = parseInt(req.params.room_id, 10);

  // DB에서는 삭제하지 않고 connection 끊고 남김
  await modules.models.small_group_chatting.update({
    room_id: null,
    origin_room_id: roomId,
  }, {
    where: { room_id: roomId },
  });
  
  res.json({ success: true });
}));

router.post('/file/:room_id', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const roomId = parseInt(req.params.room_id, 10);

  await modules.models.file.create({
    file_guid: req.fileInfo.fileGuid,
    static_path: req.fileInfo.staticPath,
    client_path: req.fileInfo.clientPath,
    file_type: req.fileInfo.extern,
    name: path.basename(req.fileInfo.clientPath),
    uploader_id: req.decoded.authId,
    room_id: roomId,
  });

  res.json({ fileGuid: req.fileInfo.fileGuid });
}));

router.get('/filelist/:room_id', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const roomId = parseInt(req.params.room_id, 10);

  const fileList = await modules.models.file.findAll({
    where: {
      room_id: roomId,
    },
    include: [
      {
        attributes: ['user_id','name'],
        model: modules.models.user,
      },
    ],
  });

  res.json({ fileList });
}));

router.delete('/file/:guid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const guid = req.params.guid;
  const { authId } = req.decoded;

  const file = await modules.models.file.findOne({
    where: { file_guid: guid }
  })

  if (!file) {
    throw new Error('삭제하려는 대상이 존재하지 않습니다.');
  }

  if (file.uploader_id !== authId) {
    throw new Error('삭제할 권한이 없는 파일입니다.');
  }

  const count = await modules.models.file.destroy({
    where: { file_guid: guid }
  })

  console.log('count = ', count);

  res.json(count);
}));
module.exports = router;
