const express = require('express');
const router = express.Router();
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const _ = require('underscore');
const sequelize = require('sequelize');
const { where } = require('sequelize');
const { Op } = sequelize;

// 기관 isOpen(개방공간 여부) 체크
router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const id = parseInt(req.params.id, 10);

  const affiliation = await modules.models.affiliation_description.findOne({
    attributes: ['isOpen'],
    where: {
      affiliation_id: id,
    }
  });

  res.json({ affiliation });
}));

module.exports = router;