const express = require('express');
const path = require('path');
const router = express.Router();
const fs = require('fs');
const modules = require('../modules/frequent-modules');
const deleteModule = require('../modules/delete');

router.post('/:classId', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {

  const classId = req.params.classId;
  await modules.models.file.create({
  file_guid: req.fileInfo.fileGuid,
  static_path: req.fileInfo.staticPath,
  client_path: req.fileInfo.clientPath,
  file_type: req.fileInfo.extern,
  name: path.basename(req.fileInfo.clientPath),
  uploader_id: req.decoded.authId,
  class_id: classId,
  });
  res.json({ fileGuid: req.fileInfo.fileGuid });
}));

router.post('/', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  await modules.models.file.create({
    file_guid: req.fileInfo.fileGuid,
    static_path: req.fileInfo.staticPath,
    client_path: req.fileInfo.clientPath,
    file_type: req.fileInfo.extern,
    name: path.basename(req.fileInfo.clientPath),
    uploader_id: req.decoded.authId,
  });

  res.json({ fileGuid: req.fileInfo.fileGuid });
}));

router.get('/previous-sqlite-file', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;

  const files = await modules.db.getQueryResult(`SELECT DISTINCT file_guid, client_path,
file_type, name FROM files as f join questions as q on
q.sqlite_file_guid=f.file_guid where q.creator_id=${authId}`);

  res.json({ files });
}));

router.get('/haarcascades/:name', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const name = req.params.name;
  const static_path = `${modules.rootPath}/public/haarcascades/${name}`;

  res.sendFile(static_path);
}));

router.delete('/:guid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const fileGuid = req.params.guid;
  await deleteModule.files(fileGuid);
  
  res.json({ success: true });
}));

router.delete('/:guid/sqlite/:qid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const fileGuid = req.params.guid;
  const questionId = parseInt(req.params.qid, 10);

  const questions = await modules.models.question.findAll({
    where: { sqlite_file_guid: fileGuid },
  });

  if (questions.length > 1) {
    await modules.models.question.update({
      sqlite_file_guid: null,
    }, { where: { question_id: questionId } });
  } else {
    await deleteModule.files(fileGuid);
  }

  res.json({ success: true });
}));

router.get('/:guid', modules.asyncWrapper(async (req, res) => {


  const fileGuid = req.params.guid;
  const file = await modules.models.file.findOne({
    where : {
      file_guid : fileGuid
    }
  });
  // var file =  fs.readFileSync(path.join(__dirname, file.static_path));
  // res.download(file.static_path);
  var options = {
    headers: {
      'Content-Type': 'multipart/form-data',
      'x-timestamp': Date.now(),
      'x-sent': true
    }
  }

  res.sendFile(file.static_path, options, function (err) {
    if (err) {
      next(err)
    } else {
      console.log('Sent:', file.static_path)
    }
  })
}));

router.get('/name/:guid', modules.asyncWrapper(async (req, res) => {
  const client_path = decodeURIComponent(req.params.guid);
  const file = await modules.models.file.findOne({
    attributes: ['client_path', 'name'],
    where: {
      client_path,
    }
  });
  // var file =  fs.readFileSync(path.join(__dirname, file.static_path));
  res.json(file);
}));
module.exports = router;
