const express = require('express');
const router = express.Router();
const util = require('util');
const db = require('../modules/db');
const exec = util.promisify(require('child_process').exec);
const path = require('path');
const modules = require('../modules/frequent-modules');
const config = require('../config');
const async = require('async');
const fs = require('fs');

// 해당 강사의 모든 강의를 return
router.get('/',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  if (!(authType === 3 || authType === 1)){
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  const lectures = await modules.models.lecture.findAll({
    where: {
      teacher_id: authId
    }
  });
  if ( lectures.length === 0) {
    throw modules.errorBuilder.default('Lecture is not exists', 404, true);
  }
  res.json(lectures);
}));

// 2021-04-13 lecture_id로 개설된 회의방의 room_id를 가져온다
router.get('/smallroomid/:lecture_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.lecture_id, 10);

  console.log("received lecture id: ", lectureId);

  const sql = `SELECT room_id FROM small_meeting_rooms WHERE lecture_id = ${lectureId}`;
  const result = await db.getQueryResult(sql);

  console.log("room id: ", result);
 
   if ( result.length === 0) {
    throw modules.errorBuilder.default('room_id is not exists', 204, true);
  }

  res.json({ result });
}));

router.get('/activate_lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lecture_list = await db.getQueryResult(`select *, c.name as class_name, l.name as lecture_name from user_classes uc join lectures l on l.class_id = uc.class_id and ((UNIX_TIMESTAMP(now()) >= (UNIX_TIMESTAMP(l.start_time))) and (UNIX_TIMESTAMP(now()) <= (UNIX_TIMESTAMP(l.end_time)))) join classes c on c.class_id = l.class_id where uc.user_id = ${authId} and uc.role='student' order by c.class_id desc`);
  res.json({ 'lecture_list': lecture_list });
}));
router.get('/:id/student-list', modules.auth.tokenCheck, modules.asyncWrapper(async( req,res, next) => {
  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const stdSql = `select u.name, u.user_id, u.email_id from users u where user_id in (select uc.user_id from user_classes uc , lectures l where uc.class_id = l.class_id and l.lecture_id = ${lectureId} and uc.role = 'student')`
  const students = await db.getQueryResult(stdSql);
  res.json(students);
}))
//과목당 강사의 수 책정.
router.get('/:id/teacher-list', modules.auth.tokenCheck, modules.asyncWrapper(async( req,res, next) => {
  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const teacherSql = `select u.name, u.user_id, u.email_id from users u
   where user_id in (select uc.user_id from user_classes uc , lectures l where uc.class_id = l.class_id and l.lecture_id = ${lectureId} and uc.role = 'teacher')`
  const teachers = await db.getQueryResult(teacherSql);
  res.json(teachers);
}))
router.get('/:id/is-lectured', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const { authId, authType } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  let login_logs = await modules.models.lecture_student_login_log.findAll({
    where: {
      student_id: authId,
      lecture_id: lectureId,
      type: 0
    }
  });
  if ( login_logs.length > 0 ){
    res.json({ login: true });
  } else {
    res.json({ login: false });
  }

}));

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { classId, type, name, start_time, end_time } = req.body;
  const { authId, authType } = req.decoded;

  const teacherCheck = await modules.teacherInClass(authId, classId);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const lectureList = await modules.models.lecture.findAll({
    where: { class_id: classId }
  });

  const newLecture = await modules.models.lecture.create({
    class_id: classId,
    teacher_id: authId,
    type,
    name,
    start_time,
    end_time,
    sequence: lectureList.length + 1,
  });

  // default로 chrome
  let defaultP = await modules.models.plist.findAll({
    where: { name: 'chrome' },
  });
  // chrome 없으면 생성
  let chromeId;
  if (defaultP.length === 0) {
    defaultP = await modules.models.plist.create({
      name: 'chrome',
    });
    chromeId = defaultP.plist_id;
  } else {
    chromeId = defaultP[0].dataValues.plist_id;
  }

  await modules.models.lecture_accept_plist.create({
    lecture_id: newLecture.lecture_id,
    plist_id: chromeId,
  });

  res.json({ success: true, lecture: newLecture });
}));

router.post('/faceRecognition', modules.auth.tokenCheck, async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureId, imgCount, faceCount } = req.body;

  const result = await modules.models.face_log.create({
    lecture_id: lectureId,
    user_id: authId,
    concentration: (faceCount / imgCount),
    time: Date.now(),
  });

  res.json({ success: true });
});

router.post('/snapshotStatus', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { lectureId, status } = req.body;
  const { authId, authType } = req.decoded;

  const lecture = await modules.models.lecture.findByPk(lectureId);
  const classId = lecture.class_id;

  const teacherCheck = await modules.teacherInClass(authId, classId);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const updateResult = await modules.models.lecture.update({
    snapshot_share_status: status,
  }, {
    where: { lecture_id: lectureId }
  });

  res.json({ success: true, updateResult });
}));

router.get('/snapshotStatus', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { lectureId } = req.query;
  const findResult = await modules.models.lecture.findByPk(lectureId, {
    attributes: ['snapshot_share_status'],
  });

  res.json({ success: true, findResult });
}));


router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.question,
        require: false,
      }
    },
    order: [
      [{ model: modules.models.lecture_item }, 'order', 'ASC'],
      [{ model: modules.models.lecture_item }, 'start_time', 'ASC'],
    ],
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Lecture Not Found', 404, true);
  }

  res.json(lecture);
}));

router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const keys = Object.keys(req.body);
  // const keyLen = keys.length;

  const lecture = await modules.models.lecture.findByPk(id, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const errorTypeOneKey = [], errorTypeTwoKey = [];

  for (const key of keys) {
    if (modules.modelAttributeList(modules.models.lecture).includes(key)) {
      if (Object.keys(lecture.dataValues).includes(key)) {
        lecture[key] = req.body[key];
      } else {
        errorTypeTwoKey.push(key);
      }
    } else {
      errorTypeOneKey.push(key);
    }
  }
  
  if (errorTypeOneKey.length > 0) {
    console.log(`router/lecture.js, put('/:id')에서 sequelize model에 정의 되어 있지 않은 속성인 ${errorTypeOneKey.toString()} 이 들어왔습니다.`);
    throw modules.errorBuilder.default(`관리자에게 강의 수정 기능 오류를 문의 부탁드립니다.`, 500, true);
  }

  if (errorTypeTwoKey.length > 0) {
    console.log(`router/lecture.js, put('/:id')에서 sequelize model에 정의 되어 있는 속성 중 db 에 ${errorTypeTwoKey.toString()} 속성이 존재하지 않습니다.`);
    throw modules.errorBuilder.default(`관리자에게 강의 수정 기능 오류를 문의 부탁드립니다.`, 500, true);
  }
  /* 그룹 삭제는 프론트에서 190806
  if (key === 'type') {
    // 삭제
    await modules.models.lecture_item_group.destroy({
      where: { lecture_id : lecture.lecture_id }
    });
  }
  */
  await lecture.save();

  res.json({ success: true });
}));

router.put('/video_link/:lecture_id/:video_link', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const video_link = req.params.video_link;
  const lecture_id = req.params.lecture_id;
  const { authId, authType } = req.decoded;
  const lecture = await modules.models.lecture.findByPk(lecture_id, {
    include: {
      model: modules.models.class,
    },
  });

  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.lecture.update({
    video_link : video_link,
  }, {
    where: {
      lecture_id: lecture_id,
    }});
  res.json({ success: true });
}));

router.delete('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const lecture = await modules.models.lecture.findByPk(id, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  await modules.delete.lecture(id);

  res.json({ success: true });
}));

// // 강의자료 & 참고자료 //

router.get('/:id/materials', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const lec = await modules.models.lecture.findByPk(parseInt(req.params.id, 10), {
    include: [
      { model: modules.models.file, as: 'material', attributes: modules.models.file.selects.fileClient },
      { model: modules.models.file, as: 'additional_material', attributes: modules.models.file.selects.fileClient },
    ],
  });

  res.json({ material: lec.material, additional: lec.additional_material });
}));
// -- 강의자료 & 참고자료 //

const keywordUpsert = async (values, condition) => modules.models
  .lecture_keyword.findOne({ where: condition })
  .then((obj) => {
    if (obj) {
      return obj.update(values);
    } else {
      return modules.models.lecture_keyword.create(values);
    }
  });

router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const promises = [];
  const dataLen = data.length;
  for (let i = 0; i < dataLen; i += 1) {
    promises.push(keywordUpsert(
      { lecture_id: lectureId, keyword: data[i].keyword, weight: data[i].weight },
      { lecture_id: lectureId, keyword: data[i].keyword },
    ));
  }

  Promise.all(promises);
  await modules.coverageCalculator.coverageAll(lectureId, lecture.class_id);

  res.json({ success: true });

}));

router.post('/:id/keyword', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { keyword, weight } = req.body;

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  const keywords = await modules.models.lecture_keyword.findAll({
    where: {
      lecture_id: lectureId,
      keyword
    }
  });
  if ( keywords.length > 0 ) {
    throw modules.errorBuilder.default('keyword is already exist', 404, true); 
  }
  let result = await modules.models.lecture_keyword.create({
    lecture_id: lectureId,
    keyword,
    weight
  });

  await modules.coverageCalculator.lectureKeywordCalculate(lectureId);
  res.json({ keyword: result });
  
}));

router.put('/:id/keyword/:kid', modules.auth.tokenCheck, modules.asyncWrapper( async(req,res) => {

  
  const lectureId = parseInt(req.params.id, 10);
  const keywordId = parseInt(req.params.kid, 10);
  const { keyword, weight } = req.body;
  let k = await modules.models.lecture_keyword.findOne({ where: { lecture_id: lectureId, id: keywordId }});
  if (!k) {
    throw modules.errorBuilder.default('keyword is not Found', 404, true);
  }
  // 만약 강의를 진행한적이 있으면 변경 불가
  let slog = await modules.models.student_answer_log.findOne({ where: { lecture_id: lectureId }});
  if (slog) {
    res.json({ success: false, message: '수업을 진행한 강의의 키워드는 수정할 수 없습니다.' });
  }

  let promises = [];
  promises.push(modules.models.lecture_keyword.update({
    keyword,
    weight
  },{
    where: {
      id: keywordId,
      lecture_id: lectureId,
    },
  }));
  promises.push(modules.models.lecture_keyword_relation.update({
    node1: keyword
  },{
    where: {
      lecture_id: lectureId,
      node1: k.keyword
    }
  }));
  promises.push(modules.models.lecture_keyword_relation.update({
    node2: keyword
  },{
    where: {
      lecture_id: lectureId,
      node2: k.keyword
    }
  }));
  promises.push(modules.models.material_keyword.update({
    keyword
  },{
    where: {
      keyword: k.keyword,
      lecture_id: lectureId,
    },
  }));
  promises.push(modules.models.question_keyword.update({
    keyword
  },{
    where: {
      keyword: k.keyword,
      lecture_id: lectureId,
    },
  }));
  promises.push(modules.models.note_keyword.update({
    keyword
  },{
    where: {
      keyword: k.keyword,
      lecture_id: lectureId,
    },
  }));
  await Promise.all(promises);
  await modules.coverageCalculator.lectureKeywordCalculate(lectureId);

  res.json({ success: true });

}));

const relationUpsert = async (values, condition) => modules.models
  .lecture_keyword_relation.findOne({ where: condition })
  .then((obj) => {
    if (obj) {
      return obj.update(values);
    } else {
      return modules.models.lecture_keyword_relation.create(values);
    }
  });

router.post('/:id/keyword-relations', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const promises = [];
  const dataLen = data.length;
  for (let i = 0; i < dataLen; i += 1) {
    promises.push(relationUpsert(
      { lecture_id: lectureId, node1: data[i].node1, node2: data[i].node2, weight: data[i].weight },
      { lecture_id: lectureId, node1: data[i].node1, node2: data[i].node2 },
    ));
  }

  res.json({ success: true });
}));
/////////////////////////////////// 삭제 예정 ////////////////////////////////////////////////////////////

// router.delete('/:id/keywords/:keyword', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
//   const lectureId = parseInt(req.params.id, 10);
//   const { keyword } = req.params;
//   const { authId, authType } = req.decoded;

//   const lecture = await modules.models.lecture.findByPk(lectureId, {
//     include: {
//       model: modules.models.class,
//     },
//   });
//   if (!lecture) {
//     throw modules.errorBuilder.default('Not Found', 404, true);
//   }
//   const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
//   if (!(authType === 3 || teacherCheck)) {
//     throw modules.errorBuilder.default('Permission Denied', 403, true);
//   }


//   await modules.models.lecture_keyword_relation.destroy({
//     where: {
//       $or: [
//         { node1: keyword },
//         { node2: keyword },
//       ],
//       lecture_id: lectureId,
//     },
//   });
//   await modules.models.lecture_keyword.destroy({
//     where: {
//       keyword,
//       lecture_id: lectureId,
//     },
//   });
//   await modules.models.material_keyword.destroy({
//     where: {
//       keyword,
//       lecture_id: lectureId,
//     },
//   });
//   await modules.models.question_keyword.destroy({
//     where: {
//       keyword,
//       lecture_id: lectureId,
//     },
//   });
//   await modules.models.note_keyword.destroy({
//     where: {
//       keyword,
//       lecture_id: lectureId,
//     },
//   });


//   res.json({ success: true });
// }));


/////////////////////////////////////////////////////////////////////////////////////////////////////////
router.delete('/:id/keyword/:kid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const keywordId = parseInt(req.params.kid, 10);

  const { authId, authType } = req.decoded;

  const k = await modules.models.lecture_keyword.findOne({ where: { lecture_id: lectureId, id: keywordId }});
  if(!k){
    throw modules.errorBuilder.default('keyword is not Found', 404, true);
  }
  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  
  let slog = await modules.models.student_answer_log.findOne({ where: { lecture_id: lectureId }});
  if (slog) {
    res.json({ success: false, message: '수업을 진행한 강의의 키워드는 삭제할 수 없습니다.' });
    return;
  }

  let qk = await modules.models.question_keyword.findAll({ where: { keyword: k.keyword, lecture_id: lectureId }});
  let sk = await modules.models.survey_keyword.findAll({ where: { keyword: k.keyword, lecture_id: lectureId }});
  let nk = await modules.models.note_keyword.findAll({ where: { keyword: k.keyword, lecture_id: lectureId }});
  let total = qk.length + sk.length + nk.length;
  if (total > 0){
    res.json({ 
      success: false,
      message: '해당 키워드에 아이템이 묶여있어 수정할 수 없습니다.',
      total: total,
      question: qk,
      survey: sk,
      note: nk
    })
    return;
  }

  await modules.models.lecture_keyword_relation.destroy({
    where: {
      $or: [
        { node1: k.keyword },
        { node2: k.keyword },
      ],
      lecture_id: lectureId,
    },
  });
  await modules.models.lecture_keyword.destroy({
    where: {
      keyword: k.keyword,
      lecture_id: lectureId,
    },
  });
  await modules.models.material_keyword.destroy({
    where: {
      keyword: k.keyword,
      lecture_id: lectureId,
    },
  });
  await modules.models.question_keyword.destroy({
    where: {
      keyword: k.keyword,
      lecture_id: lectureId,
    },
  });
  await modules.models.note_keyword.destroy({
    where: {
      keyword: k.keyword,
      lecture_id: lectureId,
    },
  });

  await modules.coverageCalculator.lectureKeywordCalculate(lectureId);

  res.json({ success: true });
}));
// 해당 과목의 아이템을 한번에 release 
router.get('/getItems/:id',modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  
  const lectureItems = await modules.models.lecture_items.findAll({
    where : {
      lecture_id : lectureId
    }
  });
  // shuffle 시 showing_order 를 수정할 예정

}));

// 셔플 순서 return 190806
router.get('/getHash/:id/:len',modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {

  const hashId = parseInt(req.params.id, 10);
  const length = parseInt(req.params.len, 10);
  const { authId, authType } = req.decoded;
  
  const hashString = await modules.models.lecture_shuffle_hashtable.findOne({
    where : {
      len: length,
      id: hashId,
    }
  });
  const hashValue = hashString.shuffle.split('<$!<>');
  res.json({ hashValue });
}));


router.delete('/:id/keyword-relations/:node1/:node2', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { node1, node2 } = req.params;
  const { authId, authType } = req.decoded;

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.lecture_keyword_relation.destroy({
    where: {
      node1, node2,
    },
  });

  res.json({ success: true });
}));

router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);

  const keywords = (await modules.models.lecture.findByPk(lectureId, {
    include: { model: modules.models.lecture_keyword }
  })).lecture_keywords;

  res.json(keywords);
}));
router.get('/:id/plist',  modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  let returnJson = {};

  const plist = (await modules.models.lecture.findByPk(lectureId, {
    include: { model: modules.models.lecture_accept_plist, include : {
		model: modules.models.plist
	} },
  })).lecture_accept_plists;
  returnJson['lecture_accept_plist'] = plist;

  //const plist_all = await db.getQueryResult('select * from plists where plists.plist_id not in (select plist_id from lecture_accept_plists where lecture_id = ' + lectureId + ')');
  const plist_all = await db.getQueryResult('select * from plists');
  returnJson['plist_all'] = plist_all;

  res.json(returnJson);
}));
router.post('/:id/plist',  modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const movedKeys = req.body.movedKeys;

  await modules.db.getQueryResult(`delete from lecture_accept_plists where lecture_id = ${lectureId}`);


  for(let i = 0 ; i < movedKeys.length; i++)
  {
	  modules.models.lecture_accept_plist.create({'plist_id': movedKeys[i], 'lecture_id': lectureId});
  }
  res.json({'success': true});
}));

router.get('/:id/keyword-relations', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);

  const relations = (await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.lecture_keyword_relation,
      attributes: modules.models.lecture_keyword_relation.selects.lectureKeywordRelationSimple,
    },
  })).lecture_keyword_relations;

  res.json(relations);
}));

router.get('/:id/coverage', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

 /* const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const avgCoverage = await modules.models.lecture_coverage.findByPk(lectureId);
  const keywordCoverages = await modules.models.lecture_keyword.findAll({
    where: { lecture_id: lectureId },
    attributes: modules.models.lecture_keyword.selects.lectureCoverage,
    raw: true,
  });

  const keywordQuestions = await modules.db.getQueryResult(`select qk.keyword as keyword,
qk.question_id as question_id, i.name as item_name, l.name as lecture_name,
i.lecture_item_id as lecture_item_id , l.lecture_id as lecture_id from
question_keywords as qk
join questions as q on qk.question_id=q.question_id
join lecture_items as i on i.lecture_item_id=q.lecture_item_id
join lectures as l on i.lecture_id=l.lecture_id
where qk.lecture_id=${lectureId}
order by keyword`);
  const keywordMaterial = await modules.db.getQueryResult(`select mk.keyword as keyword,
mk.material_id as material_id, i.name as item_name, l.name as lecture_name,
i.lecture_item_id as lecture_item_id, l.lecture_id as lecture_id
from material_keywords as mk
join materials as m on mk.material_id=m.material_id
join lecture_items as i on i.lecture_item_id=m.lecture_item_id
join lectures as l on i.lecture_id=l.lecture_id
where mk.lecture_id=${lectureId}
order by keyword`);

  for (let i = 0; i < keywordCoverages.length; i += 1) {
    keywordCoverages[i].questions = [];
    keywordCoverages[i].materials = [];
  }
  let pre = '^&%&@(*$^';
  let preIndex = 0;
  for (let i = 0; i < keywordQuestions.length; i += 1) {
    const cur = keywordQuestions[i].keyword;
    if (pre === cur) {
      keywordCoverages[preIndex].questions.push(keywordQuestions[i]);
    } else {
      for (let j = 0; j < keywordCoverages.length; j += 1) {
        if (keywordCoverages[j].keyword === keywordQuestions[i].keyword) {
          pre = cur;
          preIndex = j;
          keywordCoverages[preIndex].questions.push(keywordQuestions[i]);
        }
      }
    }
  }
  pre = '^&%&@(*$^';
  preIndex = 0;
  for (let i = 0; i < keywordMaterial.length; i += 1) {
    const cur = keywordMaterial[i].keyword;
    if (pre === cur) {
      keywordCoverages[preIndex].materials.push(keywordMaterial[i]);
    } else {
      for (let j = 0; j < keywordCoverages.length; j += 1) {
        if (keywordCoverages[j].keyword === keywordMaterial[i].keyword) {
          pre = cur;
          preIndex = j;
          keywordCoverages[preIndex].materials.push(keywordMaterial[i]);
        }
      }
    }
  }
*/
  res.json({ success: false });
}));

router.post('/:id/keyword-extractor', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { numberOfKeyword, minKeywordLength, isFast } = req.body;
  const { authId, authType } = req.decoded;

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  if (lecture.keyword_state === 1) {
    throw modules.errorBuilder.default('Processing', 409, true);
  }
  lecture.keyword_state = 1;
  await lecture.save();

  res.json({ ing: true });

  let err = null;
  try {
    const commend = `java -jar \
  ${path.join(modules.config.appRoot, 'modules/keyword.jar')} \
  ${path.join(modules.config.appRoot, 'config-keyword-extractor.json')} \
  ${lectureId} ${numberOfKeyword} ${minKeywordLength} ${isFast} 0.5 0.5`;
    const { stdout, stderr } = await exec(commend);

    if (stdout.indexOf('SUCCESS') >= 0) {
      lecture.keyword_state = 2;
      await lecture.save();
    } else {
      lecture.keyword_state = 3;
      await lecture.save();
      err = stderr + stdout;
    }
  } catch (e) {
    lecture.keyword_state = 3;
    await lecture.save();
    e.notSend = true;
    throw e;
  }
  if (err) {
    throw modules.errorBuilder.default(err, 500, false, true);
  }

  await modules.coverageCalculator.metarialScoreSum(lectureId);
  await modules.coverageCalculator.coverageAll(lectureId, lecture.class_id);
}));

router.get('/:id/qna', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const lectureId = parseInt(req.params.id, 10);

  const qna = await modules.models.real_time_qna.findAll({
    lecture_id: lectureId,
    include: { model: modules.models.user, attributes: modules.models.user.selects.userJoined },
  });

  res.json({ qna });
}));

router.get('/:id/qna/:qnaId/send', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const lectureId = parseInt(req.params.id, 10);
  const qnaId = parseInt(req.params.qnaId, 10);

  const qna = await modules.models.real_time_qna.findByPk(qnaId);
  const lecture = await modules.models.lecture.findByPk(lectureId);
  const classId = lecture.class_id;

  await modules.models.board.create({
    name: qna.content,
    content: qna.content,
    class_id: classId,
    writer_id: qna.user_id,
  });

  await modules.models.real_time_qna.destroy({
    where: {
      real_time_qna_id: qna.real_time_qna_id,
    },
  });

  res.json({ success: true });
}));
router.get('/:id/on-student-count', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {
  let lecture_id = parseInt(req.params.id, 10);
  let lec = await modules.models.lecture_student_login_status.findOne({ where : { lecture_id : lecture_id }});
  if (lec) { res.json(lec) }
  else {
    let log = await modules.models.lecture_student_login_status.create({
      lecture_id: lecture_id,
      count : 0,
    })
    res.json(log);
  }
}));
router.get('/:id/opened-item', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId } = req.decoded;

  const lectureItem = await modules.models.lecture_item.findAll({
    where: {
      lecture_id: lectureId,
      opened: 1,
    },
    include: [{
      model: modules.models.lecture,
    }, {
      model: modules.models.question,
      include: [
        { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
        { model: modules.models.student_answer_log,
          where: { student_id: authId },
          required: false,
          include: [
            { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
            { model: modules.models.oj_solution, required: false },
            { model: modules.models.oj_compileinfo, required: false },
            { model: modules.models.oj_runtimeinfo, required: false },
            { model: modules.models.feedback, required: false },
          ],
        },
      ],
    }, {
      model: modules.models.lecture_code_practice,
    }, {
      model: modules.models.discussion,
      include: { model: modules.models.user },
    }, {
      model: modules.models.discussion_info,
    }, {
      model: modules.models.survey,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.student_survey, where: { student_id: authId }, required: false },
      ],
    }, {
      model: modules.models.note,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.student_survey, where: { student_id: authId }, required: false },
      ],
    }],
  });

  if (!lectureItem) {
    res.json(null);
    return;
  }
  let lecs = []
  for ( let i = 0; i < lectureItem.length; i+= 1 ) {
    lecs.push(lectureItem[i].toJSON());
  }
  for ( let j = 0; j < lecs.length; j += 1 ) {
    delete lecs[j].lecture;
    if ( lecs[j].questions == undefined ) continue;
    for (let i = 0; i < lecs[j].questions.length; i += 1) {
      
      if (lecs[j].questions[i].choice) {
        lecs[j].questions[i].choice = lecs[j].questions[i].choice.split(modules.config.separator);
      }
      if (lecs[j].questions[i].answer) {
        lecs[j].questions[i].answer = lecs[j].questions[i].answer.split(modules.config.separator);
      }
      if (lecs[j].questions[i].choice2) {
        lecs[j].questions[i].choice2 = lecs[j].questions[i].choice2.split(modules.config.separator);
      }
      if (lecs[j].questions[i].accept_language) {
        lecs[j].questions[i].accept_language = lecs[j].questions[i].accept_language.split(modules.config.separator);
      }
      if (lecs[j].questions[i].type === 3) {
        const problem = await modules.models.oj_problem.findOne({
          where: { problem_id: lecs[j].questions[i].question_id }
        });
        
        if (problem) {
          lecs[j].questions[i].input_description = problem.input;
          lecs[j].questions[i].output_description = problem.output;
          lecs[j].questions[i].sample_input = problem.sample_input;
          lecs[j].questions[i].sample_output = problem.sample_output;
          lecs[j].questions[i].sample_code = problem.sample_code;
          lecs[j].questions[i].time_limit = problem.time_limit;
          lecs[j].questions[i].memory_limit = problem.memory_limit;
        } 
      }
      if ( lecs[j].questions[i].student_answer_log == undefined ) continue;
      for (let j = 0; j < lecs[j].questions[i].student_answer_logs.length; j += 1) {
        if (lecs[j].questions[i].student_answer_logs[j].answer) {
          lecs[j].questions[i].student_answer_logs[j].answer =
              lecs[j].questions[i].student_answer_logs[j].answer.split(modules.config.separator);
        } else {
          lecs[j].questions[i].student_answer_logs[j].answer = [];
        }
      }
      if (!lecs[j].questions[i].sqlite_file_guid) {
        lecs[j].questions[i].sql_lite_file = [];
      } else {
        lecs[j].questions[i].sql_lite_file = await modules.models.file.findAll({
          where: { file_guid: lecs[j].questions[i].sqlite_file_guid }
        });
      }
    }
    for (let i = 0; i < lecs[j].surveys.length; i += 1) {
      if (lecs[j].surveys[i].choice) {
        lecs[j].surveys[i].choice = lecs[j].surveys[i].choice.split(modules.config.separator);
      }
      if (lecs[j].surveys[i].student_surveys[0]) {
        lecs[j].surveys[i].student_surveys[0].answer = lecs[j].surveys[i].student_surveys[0].answer.split(modules.config.separator);
      }    
    }
    
  }

  res.json(lecs);
}));
// router.get('/:lectureId/student/status', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
//   const { authId } = req.decoded;
//   const lectureId = parseInt(req.params.lectureId, 10);

//   const result = await modules.models.face_log.findOne({
//     where: {
//       user_id: authId,
//       lecture_id: lectureId,
//     }
//   });

//   res.json({ concentration: result === null ? 0 : result.concentration });
// }));
// lectureId 로
router.post('/lecture_grade/:lecture_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const lectureId = parseInt(req.params.lecture_id, 10);
  const { authId, authType } = req.decoded;
  const lecture = await modules.models.lecture.findOne({
    where: {
      lecture_id: lectureId
    }
  });

  const teacherCheck = await modules.teacherInClass(authId, lecture.class_id); // 강사가 groupd에 속해있는지 판단하는 모듈

  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  res.json({
    success: true,
  })
}));

router.get('/group/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId } = req.decoded;
  let uid = authId % 100;

  
  // lecture_linked_list 
  // lecture_group  두개를 가져와서 순서를 정한 다음에

  let group = await modules.models.lecture_item_group.findOne({
    where: {
      lecture_id : lectureId,
      opened: 1,
    }
  });

  if ( !group ) {
    res.json([]);
    return true;
  } 

  let total_items = [];
  let linked_lists = [];
  if (group.if_small_group) {
    linked_lists = (await Promise.all(group.group_list.split(config.separator).map(linked_list => modules.models.lecture_item_small_group.findOne({
      where: {
        lecture_item_small_group_id: parseInt(linked_list, 10)
      }
    })))).map(linked_list => linked_list.dataValues);

    res.json({
      ids: linked_lists,
      mannedShuffling: true,
      timeAfter: new Date() - new Date(group.dataValues.updatedAt)
    });
    return;
  } else {
    linked_lists = group.group_list.split(config.separator)
  }

  // 이 리스트 항목을 섞음
  if ( linked_lists.length === 1 ) {

    let linked_list = await modules.models.lecture_item_list.findOne({
      where : {
        lecture_item_list_id: linked_lists[0]
      }
    });
    let tmp = linked_list.linked_list.split(config.separator);
    for ( let i = 0 ; i < tmp.length; i += 1 ) {
      total_items.push(parseInt(tmp[i],10));
    }
    // 그냥 그대로 return 
  } else {

    let s = await modules.models.lecture_shuffle_hashtable.findOne({
      where: {
        len: linked_lists.length,
        id: uid,
      }
    });
    let hash_value = s.shuffle.split(config.separator);
    let shuffled_linked_lists = [];
    for ( let i = 0 ; i < hash_value.length ; i += 1 ){
      shuffled_linked_lists.push(linked_lists[hash_value[i]]);
    }
    for ( let j = 0 ; j < shuffled_linked_lists.length ; j += 1 ) {
      let list_ids = shuffled_linked_lists[j];
      let list = await modules.models.lecture_item_list.findOne({
        where: {
          lecture_item_list_id: list_ids
        }
      });

      const lists = list ? list.linked_list.split(config.separator) : [];
      lists.forEach(item_id => {
        total_items.push(parseInt(item_id, 10));
      });
    }
  }
  const lectureItem = await modules.models.lecture_item.findAll({
    where: {
      lecture_item_id: { [modules.models.Sequelize.Op.in]:  total_items }
    },
    include: [{
      model: modules.models.lecture,
    }, {
      model: modules.models.question,
      include: [
        { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
        { model: modules.models.student_answer_log,
          where: { student_id: authId },
          required: false,
          include: [
            { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
            { model: modules.models.oj_solution, required: false },
            { model: modules.models.oj_compileinfo, required: false },
            { model: modules.models.oj_runtimeinfo, required: false },
            { model: modules.models.feedback, required: false },
          ],
        },
      ],
    }, {
      model: modules.models.lecture_code_practice,
    }, {
      model: modules.models.discussion,
      include: { model: modules.models.user },
    }, {
      model: modules.models.discussion_info,
    }, {
      model: modules.models.survey,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.student_survey, where: { student_id: authId }, required: false },
      ],
    }, {
      model: modules.models.note,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
      ],
    }, {
      model: modules.models.individual_question_info,
      include: {
        model: modules.models.question,
        include: [
          { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
          { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
          { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
          { model: modules.models.student_answer_log,
            where: { student_id: authId },
            required: false,
            include: [
              { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
              { model: modules.models.oj_solution, required: false },
              { model: modules.models.oj_compileinfo, required: false },
              { model: modules.models.oj_runtimeinfo, required: false },
              { model: modules.models.feedback, required: false },
            ],
          },
        ],
      }
    }],
  });

  if (!lectureItem) {
    res.json(null);
    return;
  }
  let lecs = []
  for ( let i = 0; i < lectureItem.length; i+= 1 ) {
    lecs.push(lectureItem[i].toJSON());
  }
  let individualQuestionIndice;

  // 개인화 문항 일반 문항으로 전환 전처리(학생마다 다른 idx의 문항을 봄)
  // promise return 을 통하여 이중 await을 담고 있는 이중 forEach문을
  // 모두 기다린 다음에 다음으로 실행
  const convertIndividualToQuestion = () => {
    return new Promise(async (resolve) => {
      let forEachCount = 0;
      lecs.forEach((lec) => {
        let forEachCount1 = 0;
        lec.individual_question_infos.forEach(async (info) => {
          // 변환 하고자 하는 개인화 문항 정보 아이템의 각 학생에게 부여된 모든 indice를 검색(새로운 indice 계산)
          const res1 = await modules.models.individual_question_student_indice.findAll({
            where: {
              lecture_item_id: info.lecture_item_id,
            }
          });
          // 해당 info의 모든 학생의 indice배정 기록중 현재 요청한 학생의 id와 동일한 것이 존재하는지
          const res2 = res1.filter((indice) => {
            if (indice.dataValues.student_id === authId) {
              return true;
            } else {
              return false;
            }
          });
          if (res2.length === 0) { // 존재하지 않으면 생성
            await modules.models.individual_question_student_indice.create({
              student_id: authId,
              lecture_item_id: info.lecture_item_id,
              question_indice: res1.length,
            });
            individualQuestionIndice = res1.length;
          } else { // 존재하면 이전에 저장된 indice를 사용
            individualQuestionIndice = res2[0].question_indice;
          }
          // 개별 indice에 따른 문항으로 변경 후(questions array에 삽입)
          lec.questions.push(info.questions[individualQuestionIndice % info.questions.length]);
          forEachCount1 += 1;
          if (forEachCount1 === lec.individual_question_infos.length) {
            lec.individual_question_infos = []; // 개인화 문항 정보 array를 비우기
            if (lec.type === 6) { // lec의 타입을 개인화 문항에서 문항으로 변경
              lec.type = 0;
            }
            forEachCount += 1;
            // 이중 forEach 문을 모두 마치면 resolve(true)로 promise의 pending상태를 종료시킴
            if (forEachCount === lecs.length) {
              resolve(true);
            }
          }
        });
        if (lec.individual_question_infos.length === 0) {
          forEachCount += 1;
        }
        if (forEachCount === lecs.length) {
          resolve(true);
        }
      });
      if (lecs.length === 0) {
        resolve(false);
      }
    });
  };
  await convertIndividualToQuestion();
  // 강의 아이템의 문항의 보기와 답안등을 separator로 split 하는과정
  for ( let j = 0; j < lecs.length; j += 1 ) {
    delete lecs[j].lecture;
    if ( lecs[j].questions == undefined ) continue;
    for (let i = 0; i < lecs[j].questions.length; i += 1) {
      
      if (lecs[j].questions[i].choice) {
        lecs[j].questions[i].choice = lecs[j].questions[i].choice.split(modules.config.separator);
      }
      if (lecs[j].questions[i].answer) {
        lecs[j].questions[i].answer = lecs[j].questions[i].answer.split(modules.config.separator);
      }
      if (lecs[j].questions[i].choice2) {
        lecs[j].questions[i].choice2 = lecs[j].questions[i].choice2.split(modules.config.separator);
      }
      if (lecs[j].questions[i].accept_language) {
        lecs[j].questions[i].accept_language = lecs[j].questions[i].accept_language.split(modules.config.separator);
      }
      if (lecs[j].questions[i].multi_choice_media) {
        lecs[j].questions[i].multi_choice_media = lecs[j].questions[i].multi_choice_media.split(modules.config.separator);
      }
      if (lecs[j].questions[i].type === 3) {
        const problem = await modules.models.oj_problem.findOne({
          where: { problem_id: lecs[j].questions[i].question_id }
        });
        
        if (problem) {
          lecs[j].questions[i].input_description = problem.input;
          lecs[j].questions[i].output_description = problem.output;
          lecs[j].questions[i].sample_input = problem.sample_input;
          lecs[j].questions[i].sample_output = problem.sample_output;
          lecs[j].questions[i].sample_code = problem.sample_code;
          lecs[j].questions[i].time_limit = problem.time_limit;
          lecs[j].questions[i].memory_limit = problem.memory_limit;
        } 
      }
      if ( lecs[j].questions[i].student_answer_log == undefined ) continue;
      for (let j = 0; j < lecs[j].questions[i].student_answer_logs.length; j += 1) {
        if (lecs[j].questions[i].student_answer_logs[j].answer) {
          lecs[j].questions[i].student_answer_logs[j].answer =
              lecs[j].questions[i].student_answer_logs[j].answer.split(modules.config.separator);
        } else {
          lecs[j].questions[i].student_answer_logs[j].answer = [];
        }
      }
      if (!lecs[j].questions[i].sqlite_file_guid) {
        lecs[j].questions[i].sql_lite_file = [];
      } else {
        lecs[j].questions[i].sql_lite_file = await modules.models.file.findAll({
          where: { file_guid: lecs[j].questions[i].sqlite_file_guid }
        });
      }
      if ( lecs[j].questions[i].student_answer_log == undefined ) continue;
      for (let j = 0; j < lecs[j].questions[i].student_answer_logs.length; j += 1) {
        if (lecs[j].questions[i].student_answer_logs[j].answer) {
          lecs[j].questions[i].student_answer_logs[j].answer =
              lecs[j].questions[i].student_answer_logs[j].answer.split(modules.config.separator);
        } else {
          lecs[j].questions[i].student_answer_logs[j].answer = [];
        }
      }
    }
    for (let i = 0; i < lecs[j].surveys.length; i += 1) {
      if (lecs[j].surveys[i].choice) {
        lecs[j].surveys[i].choice = lecs[j].surveys[i].choice.split(modules.config.separator);
      }
      if (lecs[j].surveys[i].student_surveys[0]) {
        lecs[j].surveys[i].student_surveys[0].answer = lecs[j].surveys[i].student_surveys[0].answer.split(modules.config.separator);
      }    
    }
    
  }

  let shuffled_items = [];
  for ( let i = 0 ; i < total_items.length ; i += 1 ) {
    for ( let j = 0 ; j < lecs.length ; j += 1) {
      if ( total_items[i] === lecs[j].lecture_item_id ) {
        shuffled_items.push(lecs[j]);
      }
    }
  }

  res.json({
    ids: shuffled_items,
  });
}))
router.post('/copy', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const { classId, lectureInfoArray } = req.body;
  const newLcArray = lectureInfoArray.map((item) => {
    return {
      classId: item.classId,
      type: item.type,
      name: item.name,
      oriLcId: item.originLectureId,
      start_time: item.start_time,
      end_time: item.end_time,
    };
  });
  
  const successLc = 0;
  const totalLc = newLcArray.length;
  const teacherCheck = await modules.teacherInClass(authId, classId);
  
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await newLcArray.forEach(async (item) => {

    const newLecture = await modules.models.lecture.create({
      class_id: item.classId,
      teacher_id: authId,
      type: item.type,
      name: item.name,
      start_time: item.start_time,
      end_time: item.end_time,
    });

    if (newLecture) successLc += 1;
  });

  res.json({ success: true, successLecNum: successLc, totalLecNum: totalLc });

}));
router.get('/:id/files', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
  let lectureId = parseInt(req.params.id);
  let { authId } = req.decoded;

  const lectureItem = await modules.models.lecture_item.findAll({
    where: {
      lecture_id: lectureId,
    },
    include: [ {
      model: modules.models.question,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.file, as: 'question_material' ,attributes: modules.models.file.selects.fileClient },
      ],
    }, {
      model: modules.models.lecture_code_practice,
    }, {
      model: modules.models.survey,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
      ],
    }, {
      model: modules.models.note,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
      ],
    }],
  });

  const material = await modules.models.material.findAll({
    where: {
      lecture_id: lectureId,
      material_type: 0,
    },
    include: [{
      model: modules.models.file, attributes: modules.models.file.selects.fileClient
    }]
  });
  //res.json(lectureItem);
  let lecs = []
  let mats = []
  for ( let i = 0; i < material.length; i += 1) {
    mats.push(material[i].toJSON());
  }
  for ( let i = 0; i < lectureItem.length; i+= 1 ) {
    lecs.push(lectureItem[i].toJSON());
  }
  let notefiles = []
  let questionfiles = []
  let matfiles = []
  for ( let i = 0; i < lecs.length; i += 1 ) {
    for ( let j = 0; j < lecs[i].notes.length; j += 1 ) {
      for ( let k = 0; k < lecs[i].notes[j].files.length; k += 1 ) {
        notefiles.push(lecs[i].notes[j].files[k]);
      }
    }
    for ( let j = 0; j < lecs[i].questions.length; j += 1 ) {
      for ( let k = 0 ; k < lecs[i].questions[j].files.length ; k+= 1 ) {
        questionfiles.push(lecs[i].questions[j].files[k]);
      }
    }
    for ( let j = 0; j < lecs[i].questions.length; j += 1 ) {
      for ( let k = 0 ; k < lecs[i].questions[j].question_material.length ; k+= 1 ) {
        questionfiles.push(lecs[i].questions[j].question_material[k]);
      }
    } 
  }
  for ( let i = 0; i < mats.length; i += 1 ) {
    if ( mats[i].file ) {
      matfiles.push(mats[i].file);
    }
  }
  res.json({ question_files : questionfiles, note_files : notefiles, material_files: matfiles });
}))
router.post('/:id/keyword-extractor', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
  const lecture_id = parseInt(req.params.id, 10);
  const { files, w1, w2  } = req.body;

  var str = ""
  for ( let i = 0; i < files.length; i += 1 ) {
    str += `${filese[i]} `;
  }
  
  var child = exec(`java –jar ../modules/LECTURE_KEYWORD.jar ../config.json ${lecture_id} ${str}${w1} ${w2}` , function (error, stdout, stderr) {
  
    if(stdout.indexOf('SUCCESS_SUCEESS') >= 0 ) {
      res.json({ success : true })
    } else {
      res.json({ success: false });
    }

  });

}));

router.post('/:id/extract-keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { guidList, w1, w2 } = req.body;
  const { authId, authType } = req.decoded;

  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: {
      model: modules.models.class,
    },
  });
  if (!lecture) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  if (lecture.keyword_state === 1) {
    throw modules.errorBuilder.default('Processing', 409, true);
  }

  let err = null;
  try {
    let guidCommand = '';
    if (guidList instanceof Array) {
      if (guidList.length !== 0) {
        guidCommand += `${guidList.length}`;
        guidList.forEach((element) => {
          guidCommand += ` ${element}`;
        });
      }
    }
    const commend = `java -jar -Dfile.encoding=UTF-8 \
  ${path.join(modules.config.appRoot, 'modules/LECTURE_KEYWORD.jar')} \
  ${path.join(modules.config.appRoot, 'config/config-keyword-extractor.json')} \
  ${lectureId} ${guidCommand} ${w1} ${w2}`;
    const { stdout, stderr } = await exec(commend);

    if (stdout.indexOf('SUCCESS_SUCCESS') >= 0) {
      const data = stdout.split('SUCCESS_SUCCESS');
      res.json(JSON.parse(data[1]));
    } else {
      err = stderr + stdout;
    }
  } catch (e) {
    throw e;
  }
  if (err) {
    throw modules.errorBuilder.default(err, 500, false, true);
  }

}));

router.post('/:id/material-keyword', modules.auth.tokenCheck, modules.asyncWrapper(async(req, res, next) => {

  let lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;
 
  const sql = `SELECT DISTINCT class_id FROM lectures WHERE lecture_id = ${lectureId}`;
  const check = await db.getQueryResult(sql);
  if (check.length === 0) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, check[0].class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
 for(let i=0; i< data.length; i++){
   const where ={
     lecture_id:lectureId,
     keyword:data[i].keyword
   }
 await modules.updateOrCreate(modules.models.lecture_material_keyword, where, {
    lecture_id: lectureId,  
    weight:data[i].weight,
    keyword:data[i].keyword,
  });
  }
  /*
  let sql2 = `INSERT IGNORE INTO lecture_material_keywords (keyword, weight, lecture_id) VALUES `;
  for(let i = 0; i < data.length; i+= 1) {
    let str = `('${data[i].keyword}',${data[i].weight},${lectureId})`;
    if (i !== data.length - 1) {
      str += ',';
    }
    sql2 += str;
  }

  const result = await db.getQueryResult(sql2);*/
  
  res.json({ success: true });

}));
router.delete('/:id/material-keyword', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const check = await modules.models.lecture.findOne({
    where:{
      lecture_id : lectureId
    }
  })
 
  const teacherCkeck = await modules.teacherInClass(authId, check.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.lecture_material_keyword.destroy({
    where: {
      lecture_id: lectureId,
    },
  });

  res.json({ success: true });
}));
router.get('/:id/material-keyword', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const lectureMaterialKeywords = await modules.models.lecture_material_keyword.findAll({
    where: {
      lecture_id: lectureId,
    }
  });

  if (!lectureMaterialKeywords) {
    throw modules.errorBuilder.default('Not Found -', 404, true);
  }

  res.json(lectureMaterialKeywords);
}));
router.post('/gradeUse', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureId, gradeUse } = req.body;

  const { class_id } = (await modules.models.lecture.findOne({
    where: {
      lecture_id: lectureId
    }
  })).dataValues;

  const teacherCheck = await modules.teacherInClass(authId, class_id); // 강사가 class

  if (!(authType === 3 || teacherCheck )) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.lecture.update({
    gradeUse
  }, {
    where: {
      lecture_id: lectureId
    }
  });

  res.json({
    success: true
  });
}));

// 강의 결과 공개 여부 변경
router.get('/:id/result-opened/:result', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const result = parseInt(req.params.result, 10); // 0 or 1
  
  let update = await modules.models.lecture.update({
    result_opened: result
  },{
    where: {
      lecture_id: lectureId
    }
  });
  if ( update ) {
    res.json({ success: true  });
  } else {
    res.json({ success: false });
  }
}));

// 강의 시험 모드인지 선택 190805
router.get('/:id/test-mode/:test', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const test = parseInt(req.params.test, 10); // 0 or 1
  
  let update = await modules.models.lecture.update({
    test_mode: test
  },{
    where: {
      lecture_id: lectureId
    }
  });
  if ( update ) {
    res.json({ success: true  });
  } else {
    res.json({ success: false });
  }
}));

// 학생이 접속했을때 출석 인증 중인지 확인 190920
router.get('/:id/authentication', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  let authti = await modules.models.lecture_authentication_log.findAll({
    where: {
      lecture_id: lectureId,
    },
    order: [['id', 'DESC']],
  });
  res.json({ authti, serverTime: Date.now() });
}));

router.post('/:id/homework', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { homeworkStartAt, homeworkEndAt, scoringStartAt, scoringEndAt, evaluatedCnt, evalSelect } = req.body;
  // data check 
  let result = await modules.models.lecture_homework.create({
    homeworkStartAt, homeworkEndAt, scoringStartAt, scoringEndAt, lecture_id: lectureId,  
    evaluated_cnt: evaluatedCnt, eval_select: evalSelect,
  });
  if ( result ) {
    res.json({success: true, result});
  } else {
    throw modules.errorBuilder.default('create failure', 403, true);
  }

}));
// 2020.06.05 숙제 등록 2 (max score 추가)
router.post('/:id/homework2', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { homeworkStartAt, homeworkEndAt, scoringStartAt, scoringEndAt, evaluatedCnt, evalSelect, maxScore, comment } = req.body;

  const { class_id } = (await modules.models.lecture.findOne({
    where: {
      lecture_id: lectureId
    }
  })).dataValues;

  const teacherCheck = await modules.teacherInClass(authId, class_id); // 강사가 class

  if (!(authType === 3 || teacherCheck )) { // 관리자이거나 teacher가 해당 group에 속해 있거나
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // data check
  let result = await modules.models.lecture_homework.create({
    homeworkStartAt,
    homeworkEndAt,
    scoringStartAt,
    scoringEndAt,
    lecture_id: lectureId,  
    evaluated_cnt: evaluatedCnt,
    eval_select: evalSelect,
    max_score: maxScore,
    comment
  });
  if ( result ) {
    res.json({success: true, result});
  } else {
    throw modules.errorBuilder.default('create failure', 403, true);
  }

}));
router.get('/:id/homework', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  // teacherCheck
  let result = await modules.models.lecture_homework.findOne({ where: { lecture_id: lectureId }});
  if ( result ) {
    res.json({ success: true, result });
  } else {
    res.json({ success: false });
  }

}));

router.delete('/:id/homework', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  // teacherCheck
  let result = await modules.models.lecture_homework.destroy({ where: { lecture_id: lectureId }});
  if ( result ) {
    res.json({ success: true });
  } else {
    res.json({ success: true });
  }

}));
// 2020.06.05 get homework
router.get('/:id/getHomework', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  
  const sql = `select * from lecture_homeworks lh where lh.lecture_id = ${lectureId}`;
  const result = await db.getQueryResult(sql);
  res.json(result);
}));
router.get('/:id/homework/result', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const sql = `select * from student_questions sq, users u where sq.student_id = u.user_id and sq.lecture_id = ${lectureId} order by score`;
  const result = await db.getQueryResult(sql);
  res.json(result);
}));
// 2020.05.21
router.get('/:id/homework2/:sid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
  const lectureId = parseInt(req.params.id, 10);
  const stuId = parseInt(req.params.sid, 10);
  
  const sql = `select * from student_question_estimates sqe where sqe.lecture_id = ${lectureId} and sqe.student_id = ${stuId};`;
  console.log(lectureId, stuId);
  const result = await db.getQueryResult(sql);
  res.json(result);
}));
// 2020.05.22
router.get('/:qid/homework3', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
  const qId = parseInt(req.params.qid, 10);
  
  const sql = `select * from student_questions sq where sq.student_question_id = ${qId};`;
  const result = await db.getQueryResult(sql);
  res.json(result);
}));
router.get('/:id/homework/:qid', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
  const lectureId = parseInt(req.params.id, 10);
  const studentQuestionId = parseInt(req.params.qid, 10);

  const question = await modules.models.student_question.findOne({ 
    where: { student_question_id: studentQuestionId },
    include: [
      {
        model: modules.models.student_question_keyword,
      } 
    ],
  });
  const user = await modules.models.user.findOne({ where: { user_id: question.student_id } });
  question['student_id'] = user;
  res.json(question);

}));
router.get('/:id/homework-attendance', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const sql = `select uc.user_id from lectures l, user_classes uc where l.class_id = uc.class_id and l.lecture_id = ${lectureId}`;
  const students = await db.getQueryResult(sql);
  
  const questions = await modules.models.student_question.findAll({ where: { lecture_id: lectureId }});
  
  let tmp = {}, result = [];
  for (let i = 0 ; i < students.length ; i += 1) {
    tmp[students[i].user_id] = 1;
  }
  for (let i = 0 ; i < questions.length ; i += 1 ) {
    if (tmp[questions[i].student_id]) { delete tmp[questions[i].student_id]; }
  }
  for ( var k in tmp ) {
    result.push(parseInt(k, 10));
  }
  const users = await modules.models.user.findAll({ where: { user_id: { [modules.models.Sequelize.Op.in]: result }}});
  res.json(users);

}));
router.get('/:id/scoring-attendance', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const lectureId = parseInt(req.params.id, 10);
  const sql = `select uc.user_id from lectures l, user_classes uc where l.class_id = uc.class_id and l.lecture_id = ${lectureId}`;
  const students = await db.getQueryResult(sql);
  const questions = await modules.models.student_question_estimate.findAll({ where: { lecture_id: lectureId }});
  
  let tmp = {}, result = [];
  for (let i = 0 ; i < students.length ; i += 1) {
    tmp[students[i].user_id] = 1;
  }
  for (let i = 0 ; i < questions.length ; i += 1 ) {
    if (tmp[questions[i].student_id]) { delete tmp[questions[i].student_id]; }
  }
  for ( var k in tmp ) {
    result.push(parseInt(k, 10));
  }
  const users = await modules.models.user.findAll({ where: { user_id: { [modules.models.Sequelize.Op.in]: result }}});
  res.json(users);


}));

router.get('/:id/homework/pick/:qid', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res) => {

  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const studentQuestionId = parseInt(req.params.qid, 10);

  // const lecture = await modules.models.lecture.findOne({ where: { lecture_id: lectureId }});
  const items = await modules.models.lecture_item.findAll({ where: { lecture_id: lectureId }});
  const studentQuestion = await modules.models.student_question.findOne({ where: { student_question_id: studentQuestionId }});
  const studentQuestionKeywords = await modules.models.student_question_keyword.findAll({ where : { student_question_id: studentQuestionId }});


  const newItem = await modules.models.lecture_item.create({ name: studentQuestion.name, type: 1 , order: 1, sequence: items.length + 1 }); // sequence 어떻게하지
  const question = await modules.models.question.create({ 
    lecture_item_id: newItem.lecture_item_id,
    creator_id: authId,
    question: studentQuestion.question,
    answer: studentQuestion.answer,
    choice: studentQuestion.choice,
    choice2: studentQuestion.choice2,
    type: studentQuestion.type,
    score: studentQuestion.score,
    difficulty: studentQuestion.difficulty,
    answer_media_type: studentQuestion.answer_media_type,
    question_media_type: studentQuestion.question_media_type,
    question_media: studentQuestion.question_media,
  });
  let keywords = [];
  for (let i = 0 ; i < studentQuestionKeywords.length ; i += 1) {
    let res = await modules.models.question_keyword.create({ question_id: question.question_id, keyword: studentQuestionKeywords[i].keyword, score_portion: studentQuestionKeywords[i].score_portion });
    keywords.push(res);
  }

  res.json({ success: true, question, keywords });
  
}));

router.post('/screenConcentration', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureId, lectureScreenConcentration, start, end } = req.body;

  const result = await modules.models.screen_concentration.create({
    lecture_id: lectureId,
    user_id: authId,
    concentration: lectureScreenConcentration,
    start,
    end,
  });

  res.json({
    result
  });
}));

module.exports = router;
