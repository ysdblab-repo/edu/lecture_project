const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const modules = require('../modules/frequent-modules');

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
// router.post('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    lecture_id,
    leader_follower_cheating_array,
  } = req.body;
  // 유효성 검사
  if (Array.isArray(leader_follower_cheating_array) === false) {
    throw new Error('leader_follower_cheating_array: 부적절한 형식: 배열이 아닙니다. ');
  }
  for (let i = 0; i < leader_follower_cheating_array.length; i += 1) {
    if (leader_follower_cheating_array[i].leader_id === undefined
      || leader_follower_cheating_array[i].follower_id === undefined
      || leader_follower_cheating_array[i].cheating === undefined) {
      throw new Error('leader_follower_cheating_array: 부적절한 형식: leader_id 또는 follower_id 또는 cheating 값이 없는 인자를 입력받았습니다. ');
    }
  }
  for (let i = 0; i < leader_follower_cheating_array.length; i += 1) {
    const where = {
      lecture_id,
      leader_id: leader_follower_cheating_array[i].leader_id,
      follower_id: leader_follower_cheating_array[i].follower_id,
    };
    await modules.updateOrCreate(modules.models.cheating_small, where, {
      lecture_id,
      leader_id: leader_follower_cheating_array[i].leader_id,
      follower_id: leader_follower_cheating_array[i].follower_id,
      cheating: leader_follower_cheating_array[i].cheating,
    });
  }

  res.json({ success: true, size: leader_follower_cheating_array.length });
}));

router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
// router.get('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    lecture_id,
    class_id,
  } = req.query;

  // 유효성 검사
  if (lecture_id === undefined && class_id === undefined) throw new Error('lecture_id와 class_id중 하나는 반드시 입력해주세요. ');

  if (lecture_id !== undefined) {
    const leader_follower_cheating_array = await modules.models.cheating_small.findAll({
      attributes: ['leader_id', 'follower_id', 'cheating'],
      where: { lecture_id },
    });

    res.json({ success: true, lecture_id, leader_follower_cheating_array });
  } else if (class_id !== undefined) {
    const lecture_id_array = await modules.models.lecture.findAll({
      attributes: ['lecture_id'],
      where: {
        class_id,
      },
    }).map(x => x.lecture_id);
    const leader_follower_cheating_array = await modules.models.cheating_small.findAll({
      attributes: ['leader_id', 'follower_id', [sequelize.fn('sum', sequelize.col('cheating')), 'cheating']],
      where: {
        lecture_id: {
          [sequelize.Op.in]: lecture_id_array,
        },
      },
      group: ['leader_id', 'follower_id'],
    });
    res.json({ success: true, class_id, leader_follower_cheating_array });
  } else {
    res.json({ success: false, message: '예상치 못한 오류입니다. 개발자에게 문의해주세요.' });
  }
}));

module.exports = router;
