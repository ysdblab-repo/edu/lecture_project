const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const util = require('util');

const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
const exec = util.promisify(require('child_process').exec);
const dump = require('../modules/dump');

router.get('/', modules.auth.tokenCheckTest, modules.asyncWrapper(async(req,res) => {

  console.log(`test get api`);
  res.json({ success: true });
})); 

router.post('/', modules.asyncWrapper(async (req, res) => {
  console.log(req.body)
  res.json({ success: true });
}));

router.get('/reset', modules.asyncWrapper(async (req, res) => {
  await dump.reset();
  res.json({ success: true });
}));

router.get('/:qid', modules.asyncWrapper(async (req, res, next) => {
  const qid = parseInt(req.params.qid, 10);
  const k = 2;
  const length = 3;

  let commend = `java -jar \
${path.join(modules.config.appRoot, 'modules/scoring.jar')} \
${path.join(modules.config.appRoot, 'config-keyword-extractor.json')} \
1 ${qid} ${k} ${length}`;
  console.log(commend);
  const a = await exec(commend);

  if (!(a.stdout.indexOf('SUCCESS') >= 0)) {
    res.json({ error: true });
    return;
  }
  commend = `java -jar \
${path.join(modules.config.appRoot, 'modules/scoring.jar')} \
${path.join(modules.config.appRoot, 'config-keyword-extractor.json')} \
3 ${qid}`;
  console.log(commend);
  const b = await exec(commend);
  if (!(b.stdout.indexOf('SUCCESS') >= 0)) {
    res.json({ error: true });
    return;
  }

  const re = JSON.parse(b.stdout.slice(b.stdout.indexOf('RESULT_SC-') + 10, b.stdout.lastIndexOf('SUCCESS')));

  res.json(re.Result);
}));

router.get('/lpkoji', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
}));


module.exports = router;

// router.get('/lec/:id', modules.asyncWrapper(async (req, res, next) => {
//   const id = parseInt(req.params.id, 10);
//   const m = await modules.models.material.findAll({
//     include: {
//       model: modules.models.lecture_item,
//       required: true,
//       where: { lecture_id: id },
//     },
//   });
//
//   res.json({m});
// }));
//
// router.get('/class/:id', modules.asyncWrapper(async (req, res, next) => {
//   const id = parseInt(req.params.id, 10);
//   await require('../modules/coverage-calculator').class(id);
//
//   res.json({});
// }));
//
// router.get('/ssss', modules.asyncWrapper(async (req, res, next) => {
//   await scoreing.ordered(1, 1, 0);
//   await scoreing.ordered(1, 1, 1);
//   await scoreing.ordered(1, 1, 2);
//   res.json({});
// }));
//
// router.get('/asd', modules.asyncWrapper(async (req, res, next) => {
//   const arr = await modules.models.file.findAll();
//
//   for (let i = 0; i < arr.length; i++) {
//     arr[i].name = path.basename(arr[i].client_path);
//     await arr[i].save();
//   }
//
//   res.json({ });
// }));
//
//

// const studentAnswers = await modules.models.student_answer_log.findAll({
//   where: { question_id: 1 },
// });
// const counting = {};
// for (let i = 0; i < studentAnswers.length; i += 1) {
//   studentAnswers[i].answer = studentAnswers[i].answer.split(modules.config.separator);
//   for (let j = 0; j < studentAnswers[i].answer.length; j += 1) {
//     if (counting[studentAnswers[i].answer[j]]) {
//       counting[studentAnswers[i].answer[j]] += 1;
//     } else {
//       counting[studentAnswers[i].answer[j]] = 1;
//     }
//   }
// }

// const question = await modules.models.Question.findByPk(a, {
//   include: {
//     model: modules.models.Lecture_item,
//     include: {
//       model: modules.models.Lecture,
//       include: {
//         model: modules.models.Class,
//       },
//     },
//   },
// });
//
// const r = {
//   studentId: 5,
//   data: [],
// };
// r.data.push({
//   questionOd: 8,
//   answers: [],
//   interval: 0,
// });
// r.data[0].answers.push('이해도', '참여도');


// const question = await modules.models.Question.findByPk(4, {
//   include: [{
//     model: modules.models.Lecture_item,
//     include: {
//       model: modules.models.Lecture,
//     },
//   }, {
//     model: modules.models.Problem_testcase,
//   }],
// });

// const the = await modules.models.Class.findAll({
//   include: [
//     { model: modules.models.User, attributes: modules.models.User.selects.joined, through: { where: { role: 'teacher' } } },
//     { model: modules.models.User, as: 'Master' },
//   ],
// });
