const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');

/*
// 인증 (공통)
router.use(modules.auth.tokenCheck);
router.use(modules.auth.checkTokenIsAdmin);
*/

// 등록
router.post('/', modules.asyncWrapper(async (req, res) => {
  const data = req.body;

  // 등록
  await modules.models.department.create(data);

  // 응답
  res.json({ success: true });
}));

// 목록 조회
router.get('/list', modules.asyncWrapper(async (req, res) => {
  const { university_name } = req.query; // eslint-disable-line

  // 조회
  const univList = await modules.models.department.findAll({
    attributes: modules.models.department.selects.set1,
    where: { university_name },
    include: {
      model: modules.models.university,
      attributes: ['name'],
    },
  });

  // 응답
  res.json(univList);
}));

// 수정
router.put('/', modules.asyncWrapper(async (req, res) => {
  const {
    old_university_name,
    new_university_name,
    code,
    old_name,
    new_name,
    part,
    manager_name,
    manager_email,
    manager_phone_number,
  } = req.body;

  if (old_university_name === undefined) {
    throw new Error('old_university_name은 반드시 입력되어야 합니다.');
  }

  if (old_name === undefined) {
    throw new Error('old_name은 반드시 입력되어야 합니다.');
  }

  const contents = {
    code,
    part,
    manager_name,
    manager_email,
    manager_phone_number,
  };
  if (new_university_name !== undefined) contents.university_name = new_university_name;
  if (new_name !== undefined) contents.name = new_name;

  // 조회
  const effectedRowNum = await modules.models.department.update(contents, {
    where: {
      university_name: old_university_name,
      name: old_name,
    },
  });

  // 응답
  if (effectedRowNum[0] !== 1) {
    throw new Error('update 문에 영향을 받은 열의 수가 1이 아닙니다.');
  }
  res.json({ success: true });
}));

// 이름 목록 조회
router.get('/namelist', modules.asyncWrapper(async (req, res) => {
  const { university_name } = req.query; // eslint-disable-line

  // 조회
  const univNameList = await modules.models.department.findAll({
    attributes: ['name'],
    where: { university_name },
  });

  // 응답
  res.json(univNameList);
}));

// 단일 학과 조회
router.get('/', modules.asyncWrapper(async (req, res) => {
  const { university_name, name } = req.query; // eslint-disable-line

  // 조회
  const deptInfo = await modules.models.department.findOne({
    attributes: modules.models.department.selects.set1,
    where: { university_name, name },
    include: {
      model: modules.models.university,
      attributes: ['name'],
    },
  });

  if (deptInfo === null) {
    throw new Error('조건에 맞는 학과를 찾을 수 없습니다.');
  }
  // 응답
  res.json(deptInfo);
}));

router.delete('/', modules.asyncWrapper(async (req, res) => {
  const { university_name, name } = req.query; // eslint-disable-line

  // 삭제
  const effectedRowNum = await modules.models.department.destroy({
    where: { university_name, name },
  });

  // 응답
  if (effectedRowNum !== 1) {
    throw new Error('delete 문에 영향을 받은 열의 수가 1이 아닙니다.');
  }
  res.json({ success: true });
}));

// 에러 처리
router.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({ success: false, message: err.message });
});

module.exports = router;
