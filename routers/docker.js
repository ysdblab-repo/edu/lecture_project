const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
const execTimeout10 = require('../src/utility');

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

// router.post('/new/:classId/port/:dockerPort', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
// router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
router.post('/', modules.asyncWrapper(async (req, res, next) => {
  
  // try { 
  //   const res1 = await execTimeout10('dir');
  //   console.log('@@@@@@@ res1 = ', res1);
  //   console.log('@@@@@@@ res1.indexOf() = ', res1.indexOf('Error: Command failed:'));
  // } catch (e) {
  //   console.log('잡았다~!');
  //   throw e;
  // }

  // const { authId, authType } = req.decoded;
  // const {
  //   class_id,
  //   port_num,
  // } = req.body;

  // // 유효성 검사
  // if (type === undefined) throw new Error('type 은(는) 반드시 입력해야 합니다.');
  // if (port_num === undefined) throw new Error('port_num 은(는) 반드시 입력해야 합니다.');

  // // edcuators or managers
  // if (authType === 3 || authType === 1) {
  //   const class_id = parseInt(class_id, 10);
  //   const docker_port = parseInt(port_num, 10);
  //   const class_name = "class" + class_id;
  //   const docker_image_name = "preswotdocker2020/practice:v17";
    
    
  //   await execTimeout10("sudo docker run -p " + docker_port + ":8000 -d --name " + class_name + " " + docker_image_name + " jupyterhub");
  //   await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"sed -i '5c\\        self.CLASS_ID = " + class_id + "' /root/preswot_server_info.py\""); 

  //   const sql0 = `select u.email_id as student_email_id from users u, user_classes uc where u.user_id = uc.user_id and uc.class_id = ${class_id}`;
  //   const students = await db.getQueryResult(sql0);

  //   for (let i = 0 ; i < students.length ; i += 1){
  //     const student_name = students[i].student_email_id;
  //     await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"sh /srv/scripts/add_user.sh " + student_name + " " + student_name + "\"");
  //   }

  //   await execTimeout10("sudo docker exec -d " + class_name + " /bin/sh -c \"python /srv/CAPSTONE1_19_spring/module.py > /dev/null \"");

  //   res.json({ success: true });
  await sleep(3000);
  res.json({ success: true });
  // } else {
  //   throw modules.errorBuilder.default('Managers only.', 404, true);
  // }
}));

router.post('/user', modules.asyncWrapper(async (req, res, next) => {
  const { class_id, user_id } = req.body;

  // 유효성 검사
  if (class_id === undefined) throw new Error('class_id 은(는) 반드시 입력해야 합니다.');
  if (user_id === undefined) throw new Error('user_id 은(는) 반드시 입력해야 합니다.');
  const class_name = `class${class_id}`;
  console.log('class_name = ', class_name);

  // class_id로 만들어진 도커가 있는지 확인
  const res1 = await execTimeout10('docker ps');
  if (res1.indexOf(class_name) === -1) {
    throw modules.errorBuilder.default('class_id에 해당하는 도커가 아직 생성되지 않았습니다.', 500, true);
  }

  // email_id 확보
  const sql0 = `select email_id, type from users where user_id = ${user_id}`;
  const email_id_list = await db.getQueryResult(sql0);
  if ( email_id_list.length !== 1 ) {
    throw modules.errorBuilder.default('부적절한 user_id : 해당 유저는 존재하지 않습니다.', 500, true);
  }
  if ( email_id_list[0].type !== 0 ) {
    throw modules.errorBuilder.default('부적절한 user_id : 해당 유저는 학생이 아닙니다.', 500, true);
  }
  const email_id = email_id_list[0].email_id;

  // add_user.sh 실행하여 유저와 유저 디렉토리 추가 (유저가 이미 있는 경우에 중복 실행해도 문제되지 않음)
  try {
    await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"sh /srv/scripts/add_user.sh " + email_id + " " + email_id + "\"");
  } catch (e) {
    console.log(e);
    throw modules.errorBuilder.default('이 부분은 이미 만들어진 유저를 대상으로 한 경우 생길 수 있음. 꼬인 경우 도커에 userdel -r "유저이름"을 직접 입력하여 제거 후 진행 가능', 500, true);
  }

  // 실습 아이템과 코딩 과제 만들어진 목록을 확인
  const sql1 = `select * from lecture_items WHERE type IN (2, 5) and lecture_id in ( select lecture_id from lectures where class_id = ${class_id})`;
  const lecture_item_id_list = await db.getQueryResult(sql1);

  // home/유저 디렉토리 안에 아이템 디렉토리 생성 (디렉토리가 이미 있는 경우에 중복 실행해도 문제되지 않음)
  for (let i = 0; i < lecture_item_id_list.length; i += 1) {
    await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"mkdir /home/" + email_id + "/practice_item_"+lecture_item_id_list[i].lecture_item_id+"\"");
    await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"chown -R " + email_id + ":" + email_id + " /home/" + email_id +"\"");
  }

  // module.py 재시작
  try {
    await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"ps -ef | grep module.py | awk '{print \\$2}' | xargs kill -9\"");
  } catch(e) {
    console.log(e);
    console.log('grep 명령을 kill하려다보니 없어서 생기는 경우 이 부분은 에러가 아님.')
  }
  await sleep(500);
  await execTimeout10("sudo docker exec -d " + class_name + " /bin/sh -c \"python /srv/CAPSTONE1_19_spring/module.py > /dev/null \"");

  // // n초 뒤, 실행 확인 후 리턴 - grep -v grep 을 할 경우, 리턴되는 결과가 없으면 버그로 인식해서 에러를 던지는 관계로 일단 주석..
  // await sleep(500);
  // const res3 = await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"ps -ef | grep module.py | grep \-v grep\"");
  // if (res3.indexOf('module.py') !== -1) {
  //   res.json({ success: true });
  // } else {
  //   throw modules.errorBuilder.default('유저 추가 후 module.py를 껏다 켜는 과정이후에 module.py가 켜지지 않았습니다. 백엔드 routers/docker.js의 .post(/user) 요청을 점검해주세요.', 500, true);
  // }
  res.json({ success: true });
}));
module.exports = router;
