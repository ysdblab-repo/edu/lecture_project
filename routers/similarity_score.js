const express = require('express');
const router = express.Router();
const util = require('util');
const db = require('../modules/db');
const exec = util.promisify(require('child_process').exec);
const path = require('path');
const modules = require('../modules/frequent-modules');
const config = require('../config');
const async = require('async');

// return scores according to login type (educator or student)
router.get('/:id/result',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

    const { authId, authType } = req.decoded;
    const classId = parseInt(req.params.id, 10);
    
    const lectures = await modules.models.lecture.findAll({ where : { class_id: classId }});
    const sql0 = `SELECT li.name as item_name, result.class_id, result.lecture_item_id, result.email_id, result.user_id, result.name, result.code_score, result.output_score,result.submitDate FROM lecture_items li,(SELECT  ss.class_id, ss.lecture_item_id, ss.email_id, u.user_id, u.name, ss.code_score, ss.output_score, MAX(createdAt) as submitDate FROM similarity_scores ss, 
    users u WHERE ss.email_id = u.email_id AND ss.class_id = ${classId} group by ss.class_id, ss.lecture_item_id, ss.email_id, u.user_id, u.name, ss.code_score, ss.output_score) result WHERE li.lecture_item_id = result.lecture_item_id`;

    const scores = await db.getQueryResult(sql0);
    let items = [];
    for (let i = 0; i < scores.length ; i += 1) {
        items.push(parseInt(scores[i].lecture_item_id,10));
        if(!isNaN(parseFloat(scores[i].code_score))) {
            scores[i].code_score = parseFloat(scores[i].code_score);
        }
        if(!isNaN(parseFloat(scores[i].output_score))) {
            scores[i].output_score = parseFloat(scores[i].output_score);
        }
    }
    items = items.reduce(function(a,b){
        if(a.indexOf(b) < 0) a.push(b);
        return a;
    },[]);
    
    const lectureItems = await modules.models.lecture_item.findAll({ where: { lecture_item_id: { [modules.models.Sequelize.Op.in]: items } }});
    let lectureSaved = {};
    for ( let i = 0 ; i < lectureItems.length ; i += 1) {
        lectureSaved[`${lectureItems[i].lecture_item_id}`] = lectureItems[i].lecture_id;
    }
    let result = {};
    for (let i = 0 ; i < lectures.length ; i += 1) {
        result[`${lectures[i].lecture_id}`] = [];
    }
    for (let i = 0 ; i < scores.length ; i += 1) {
        let lectureId = lectureSaved[scores[i].lecture_item_id];
        let ary = result[`${lectureId}`];
        if (ary === undefined) { continue; }
        ary.push(scores[i]);
        result[lectureId] = ary;
    }
    res.json(result);

  
}));
router.get('/:id',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const { authId, authType } = req.decoded;
    const itemId = parseInt(req.params.id, 10);


    // edcuators or managers
    if (authType === 3 || authType === 1){
        educator_sql = "SELECT distinct similarity_scores.* ,\n" +
        "	lecture_code_practices.score AS points, \n" +
        "	if( similarity_scores.code_score <> -1 , lecture_code_practices.score * similarity_scores.code_score , similarity_scores.code_score) AS code_score2, \n" +
        "	if( similarity_scores.output_score <> -1 , lecture_code_practices.score * similarity_scores.output_score , similarity_scores.output_score) AS output_score2, \n" +
        "   users.name \n" + 
        "FROM similarity_scores \n" +
        "LEFT outer JOIN users \n" + 
        " ON (similarity_scores.email_id=users.email_id) \n" + 
        "LEFT JOIN lecture_code_practices \n" + 
        "ON (similarity_scores.lecture_item_id = lecture_code_practices.lecture_item_id) \n" +
        "WHERE (similarity_scores.email_id, DATETIME) \n" +
        "        IN (SELECT s.email_id, MAX(datetime)\n" +
        "            FROM similarity_scores s \n" +
        "            WHERE lecture_item_id = " + itemId + "\n" +
        "            GROUP BY email_id);";
        const scores = await modules.models.sequelize.query(educator_sql,
            {type: modules.models.Sequelize.QueryTypes.SELECT});
        if ( scores.length === 0) {
            res.json([]);
        } else {
            res.json(scores);
        }
    }
    // students
    else {
        student_sql = "SELECT distinct similarity_scores.* ,\n" +
        "	lecture_code_practices.score AS points, \n" +
        "	if( similarity_scores.code_score <> -1 , lecture_code_practices.score * similarity_scores.code_score , similarity_scores.code_score) AS code_score2, \n" +
        "	if( similarity_scores.output_score <> -1 , lecture_code_practices.score * similarity_scores.output_score , similarity_scores.output_score) AS output_score2 \n" + 
        "FROM similarity_scores \n" +
        "LEFT JOIN lecture_code_practices \n" + 
        "ON (similarity_scores.lecture_item_id = lecture_code_practices.lecture_item_id) \n" +
            "    WHERE (email_id, datetime)\n" +
            "        IN (SELECT email_id, MAX(datetime)\n" +
            "            FROM similarity_scores\n" +
            "            WHERE lecture_item_id = " + itemId + "\n" +
            "            GROUP BY email_id) AND email_id\n" +
            "               IN (SELECT email_id\n" +
            "                   FROM users\n" +
            "                   WHERE user_id = " + authId + ");";
        const scores = await modules.models.sequelize.query(student_sql,
            {type: modules.models.Sequelize.QueryTypes.SELECT});
        if ( scores.length === 0) {
            res.json([]);
        } else {
            res.json(scores);
        }
    }
}));

router.get('/:id/results',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const { authId, authType } = req.decoded;
    const class_id = parseInt(req.params.id, 10);

    // edcuators or managers
    if (authType === 3 || authType === 1){
        educator_sql = "SELECT *\n" +
            "    FROM similarity_scores\n" +
            "    WHERE (email_id, datetime)\n" +
            "        IN (SELECT email_id, MAX(datetime)\n" +
            "            FROM similarity_scores\n" +
            "            WHERE class_id = " + class_id + "\n" +
            "            GROUP BY lecture_item_id);";
        const scores = await modules.models.sequelize.query(educator_sql,
            {type: modules.models.Sequelize.QueryTypes.SELECT});
        if ( scores.length === 0) {
            res.json([])
        }
        res.json(scores);
    }
    // students are not allowed
    else {
        throw modules.errorBuilder.default('Permission Denied', 403, true);
    }
}));

module.exports = router;
