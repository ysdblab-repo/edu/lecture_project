const express = require('express');
const modules = require('../modules/frequent-modules');
const sequelize = require('sequelize');
const db = require('../modules/db');
const router = express.Router();

router.get('/:id/0', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const classId = parseInt(req.params.id, 10);
	const lectures = await modules.models.lecture.findAll({ where : { class_id: classId, type: 0 }});
  res.json(lectures);

}));

router.get('/lecture/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
  const lec_auth_logs = await modules.models.lecture_authentication_log.findAll({ where : { lecture_id: lectureId }});
  res.json(lec_auth_logs);

}));

router.get('/lecture/student/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {

  const lectureId = parseInt(req.params.id, 10);
	const sql = `SELECT distinct a.student_id, b.name, b.email_id, DATE_FORMAT(a.createdAt, '%Y-%m-%d') AS auth_date FROM lecture_authentication_student_logs AS a JOIN users AS b WHERE lecture_id=${lectureId} AND a.student_id = b.user_id`;
	const result = await db.getQueryResult(sql);
	res.json(result);

}));

router.get('/student/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {
	const studentId = parseInt(req.params.id, 10);
	const sql = `SELECT * FROM lecture_authentication_student_logs WHERE student_id=${studentId}`;
	const result = await db.getQueryResult(sql);
	res.json(result);
	
}));

router.get('/all/lecture', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {
	const lectureIds = req.query.lectures;
	const lectures = lectureIds.split('$');
	let allLectureAuths = [];
	for(let i=0; i<lectures.length-1; i++) {
		let sql = `SELECT * FROM lecture_authentication_student_logs WHERE lecture_id=${lectures[i]};`;
		const result = await db.getQueryResult(sql);
		if(result.length !== 0)
			allLectureAuths.push(result);
	}
	
	res.json(allLectureAuths);
	
}));

module.exports = router;