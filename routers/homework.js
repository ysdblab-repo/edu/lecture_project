const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');

const modules = require('../modules/frequent-modules');

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureItemId } = req.body;

  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const homework = await lectureItem.createHomework({
    creator_id: authId,
  });

  res.json({ success: true, homework_id: homework.homework_id });
}));

router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const homeworkId = parseInt(req.params.id, 10);
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  const homework = await modules.models.homework.findByPk(homeworkId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!homework) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, homework.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  for (let i = 0; i < keyLen; i += 1) {
    const key = keys[i];
    if (key === 'comment') {
      homework[key] = req.body[key];
    }
  }
  await homework.save();

  res.json({ success: true });
}));

router.post('/:id/file', modules.auth.homework, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const file = await modules.upload.fileAdd(req, 'homework_id', parseInt(req.params.id, 10));
  res.json({ success: true, file });
}));

router.get('/:id/result', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const homeworkId = parseInt(req.params.id, 10);

  const totalNum = await modules.models.student_homework.findOne({
    attributes: [[sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('student_id'))), 'submit_number']],
    where: {
      homework_id: homeworkId,
    },
  });

  res.json({ num_students_total: totalNum.dataValues.submit_number });
}));
const keywordUpsert = async (values, condition) => modules.models
  .homework_keyword.findOne({ where: condition })
  .then((obj) => {
    if (obj) {
      return obj.update(values);
    } else {
      return modules.models.homework_keyword.create(values);
    }
  });

router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const homeworkId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const homework = await modules.models.homework.findByPk(homeworkId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
        include: {
          model: modules.models.class,
        },
      },
    },
  });
  if (!homework) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, homework.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  let scoreSum = 0;
  const dataLen = data.length;
  for (let i = 0; i < dataLen; i += 1) {
    scoreSum += parseInt(data[i].score, 10);
  }
  const promises = [];
  for (let i = 0; i < dataLen; i += 1) {
    promises.push(keywordUpsert(
      { homework_id: homework.homework_id, lecture_id: homework.lecture_item.lecture_id, lecture_item_id: homework.lecture_item_id, keyword: data[i].keyword, score_portion: data[i].score },
      { homework_id: homework.homework_id, keyword: data[i].keyword },
    ));
  }
  Promise.all(promises);
  homework.score = scoreSum;
  await homework.save();

  res.json({ success: true, score: scoreSum });

  await modules.coverageCalculator.coverageAll(question.lecture_item.lecture_id, question.lecture_item.lecture.class_id);
}));

router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const homeworkId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const homeworkKeywords = (await modules.models.homework.findByPk(homeworkId, {
    include: { model: modules.models.homework_keyword },
  })).homework_keywords;

  if (!homeworkKeywords) {
    throw modules.errorBuilder.default('Not Found -', 404, true);
  }

  res.json(homeworkKeywords);
}));

router.delete('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const homeworkId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const homework = await modules.models.homework.findByPk(homeworkId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!homework) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, homework.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.homework_keyword.destroy({
    where: {
      homework_id: homeworkId,
    },
  });

  res.json({ success: true });
}));


module.exports = router;
