const express = require('express');
const modules = require('../modules/frequent-modules');
const router = express.Router();

router.get('/class/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const id = parseInt(req.params.id, 10);
  // const { authId, authType } = req.decoded;
  //
  // const teacherCkeck = await modules.teacherInClass(authId, id);
  // if (!(authType === 3 || teacherCkeck)) {
  //   throw modules.errorBuilder.default('Permission Denied', 403, true);
  // }

  const c = await modules.models.class.findByPk(id, {
    include: {
      model: modules.models.lecture,
    },
  });

  const l = c.lectures;
  const lLength = l.length;

  const result = [];

  for (let i = 0; i < lLength; i += 1) {
    const lectureId = l[i].lecture_id;

    const q = `select s.lecture_id, s.\`order\`, avg(s.participation_score) 
as avg_participation_score, stddev(s.participation_score) as stddev_participation_score,
min(participation_score) as min_participation_score, max(participation_score) as max_participation_score,
avg(s.understanding_score) as avg_understanding_score, stddev(s.understanding_score)
as stddev_understanding_score, min(understanding_score) as min_understanding_score,
max(understanding_score) as max_understanding_score, avg(s.concentration_score) as avg_concentration_score,
stddev(s.concentration_score) as stddev_concentration_score, min(concentration_score) as min_concentration_score,
max(concentration_score) as max_concentration_score, count(user_id) as number_of_student
from student_lecture_logs as s
join lectures as l on s.lecture_id=${lectureId}
 group by s.\`order\`, s.lecture_id`;
    const r = await modules.db.getQueryResult(q);

    if (r.length > 1) {
      result.push(r[0]);
      result.push(r[1]);
      result.push(r[2]);
    } else {
      result.push({
        lecture_id: lectureId,
        order: 0,
        avg_participation_score: 0,
        stddev_participation_score: 0,
        min_participation_score: 0,
        max_participation_score: 0,
        avg_understanding_score: 0,
        stddev_understanding_score: 0,
        min_understanding_score: 0,
        max_understanding_score: 0,
        avg_concentration_score: 0,
        stddev_concentration_score: 0,
        min_concentration_score: 0,
        max_concentration_score: 0,
        number_of_student: 0,
      });
      result.push({
        lecture_id: lectureId,
        order: 1,
        avg_participation_score: 0,
        stddev_participation_score: 0,
        min_participation_score: 0,
        max_participation_score: 0,
        avg_understanding_score: 0,
        stddev_understanding_score: 0,
        min_understanding_score: 0,
        max_understanding_score: 0,
        avg_concentration_score: 0,
        stddev_concentration_score: 0,
        min_concentration_score: 0,
        max_concentration_score: 0,
        number_of_student: 0,
      });
      result.push({
        lecture_id: lectureId,
        order: 2,
        avg_participation_score: 0,
        stddev_participation_score: 0,
        min_participation_score: 0,
        max_participation_score: 0,
        avg_understanding_score: 0,
        stddev_understanding_score: 0,
        min_understanding_score: 0,
        max_understanding_score: 0,
        avg_concentration_score: 0,
        stddev_concentration_score: 0,
        min_concentration_score: 0,
        max_concentration_score: 0,
        number_of_student: 0,
      });
    }
  }

//   const q = `select s.lecture_id, s.\`order\`, avg(s.participation_score)
// as avg_participation_score, stddev(s.participation_score) as stddev_participation_score,
// min(participation_score) as min_participation_score, max(participation_score) as max_participation_score,
// avg(s.understanding_score) as avg_understanding_score, stddev(s.understanding_score)
// as stddev_understanding_score, min(understanding_score) as min_understanding_score,
// max(understanding_score) as max_understanding_score, avg(s.concentration_score) as avg_concentration_score,
// stddev(s.concentration_score) as stddev_concentration_score, min(concentration_score) as min_concentration_score,
// max(concentration_score) as max_concentration_score, count(user_id) as number_of_student
// from student_lecture_logs as s
// join lectures as l on s.lecture_id=l.lecture_id where l.class_id=${id}
//  group by s.\`order\`, s.lecture_id`;
  // const result = await modules.db.getQueryResult(q);

  res.json(result);
}));

router.get('/user/:id/class/:classId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const id = parseInt(req.params.id, 10);
  const classId = parseInt(req.params.classId, 10);

  const c = await modules.models.class.findByPk(classId, {
    include: {
      model: modules.models.lecture,
    },
  });

  const l = c.lectures;
  const lLength = l.length;

  const result = [];

  for (let i = 0; i < lLength; i += 1) {
    const lectureId = l[i].lecture_id;

    let q = `select @rownum3 := @rownum3+ 1 as rank_concentration_score, x3.*
from (select * from
(select @rownum2 := @rownum2+1 as rank_understanding_score, x2.*
from (select * from 
(select @rownum1 := @rownum1+1 as rank_participation_score, x.* 
from (select ori.lecture_id, ori.\`order\`,
 ori.user_id, ori.participation_score,
ori.understanding_score, ori.concentration_score
from student_lecture_logs as ori
where ori.lecture_id=${lectureId} and ori.\`order\`=0
order by ori.participation_score desc
limit 18446744073709551615) as x, (select @rownum1 := 0) as t) as k1
order by k1.understanding_score desc) as x2, (select @rownum2 := 0) as t2) as k2
order by k2.concentration_score desc) as x3, (select @rownum3 := 0) as t3
where x3.user_id=${id}`;
    const pre = await modules.db.getQueryResult(q);
    q = `select @rownum3 := @rownum3+ 1 as rank_concentration_score, x3.*
from (select * from
(select @rownum2 := @rownum2+1 as rank_understanding_score, x2.*
from (select * from 
(select @rownum1 := @rownum1+1 as rank_participation_score, x.* 
from (select ori.lecture_id, ori.\`order\`,
 ori.user_id, ori.participation_score,
ori.understanding_score, ori.concentration_score
from student_lecture_logs as ori
where ori.lecture_id=${lectureId} and ori.\`order\`=1
order by ori.participation_score desc
limit 18446744073709551615) as x, (select @rownum1 := 0) as t) as k1
order by k1.understanding_score desc) as x2, (select @rownum2 := 0) as t2) as k2
order by k2.concentration_score desc) as x3, (select @rownum3 := 0) as t3
where x3.user_id=${id}`;
    const lec = await modules.db.getQueryResult(q);
    q = `select @rownum3 := @rownum3+ 1 as rank_concentration_score, x3.*
from (select * from
(select @rownum2 := @rownum2+1 as rank_understanding_score, x2.*
from (select * from 
(select @rownum1 := @rownum1+1 as rank_participation_score, x.* 
from (select ori.lecture_id, ori.\`order\`,
 ori.user_id, ori.participation_score,
ori.understanding_score, ori.concentration_score
from student_lecture_logs as ori
where ori.lecture_id=${lectureId} and ori.\`order\`=2
order by ori.participation_score desc
limit 18446744073709551615) as x, (select @rownum1 := 0) as t) as k1
order by k1.understanding_score desc) as x2, (select @rownum2 := 0) as t2) as k2
order by k2.concentration_score desc) as x3, (select @rownum3 := 0) as t3
where x3.user_id=${id}`;
    const rev = await modules.db.getQueryResult(q);
    result.push({
      user_id: id,
      lecture_id: lectureId,
    });
    const index = result.length - 1;
    if (pre.length !== 0) {
      result[index].pre_participation_score = pre[0].participation_score;
      result[index].pre_understanding_score = pre[0].understanding_score;
      result[index].pre_concentration_score = pre[0].concentration_score;
      result[index].pre_rank_participation_score = pre[0].rank_participation_score;
      result[index].pre_rank_understanding_score = pre[0].rank_understanding_score;
      result[index].pre_rank_concentration_score = pre[0].rank_concentration_score;
    } else {
      result[index].pre_participation_score = -1;
      result[index].pre_understanding_score = -1;
      result[index].pre_concentration_score = -1;
      result[index].pre_rank_participation_score = -1;
      result[index].pre_rank_understanding_score = -1;
      result[index].pre_rank_concentration_score = -1;
    }
    if (lec.length !== 0) {
      result[index].lec_participation_score = lec[0].participation_score;
      result[index].lec_understanding_score = lec[0].understanding_score;
      result[index].lec_concentration_score = lec[0].concentration_score;
      result[index].lec_rank_participation_score = lec[0].rank_participation_score;
      result[index].lec_rank_understanding_score = lec[0].rank_understanding_score;
      result[index].lec_rank_concentration_score = lec[0].rank_concentration_score;
    } else {
      result[index].lec_participation_score = -1;
      result[index].lec_understanding_score = -1;
      result[index].lec_concentration_score = -1;
      result[index].lec_rank_participation_score = -1;
      result[index].lec_rank_understanding_score = -1;
      result[index].lec_rank_concentration_score = -1;
    }
    if (rev.length !== 0) {
      result[index].rev_participation_score = rev[0].participation_score;
      result[index].rev_understanding_score = rev[0].understanding_score;
      result[index].rev_concentration_score = rev[0].concentration_score;
      result[index].rev_rank_participation_score = rev[0].rank_participation_score;
      result[index].rev_rank_understanding_score = rev[0].rank_understanding_score;
      result[index].rev_rank_concentration_score = rev[0].rank_concentration_score;
    } else {
      result[index].rev_participation_score = -1;
      result[index].rev_understanding_score = -1;
      result[index].rev_concentration_score = -1;
      result[index].rev_rank_participation_score = -1;
      result[index].rev_rank_understanding_score = -1;
      result[index].rev_rank_concentration_score = -1;
    }
  }

  res.json(result);
}));

module.exports = router;
