const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');

// 추가
// 조회

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    lecture_item_id,
    answer,
    user_id_array,
  } = req.body;

  // 유효성 검사
  if (Array.isArray(user_id_array) === false) {
    throw new Error('user_id_array: 부적절한 형식: 배열이 아닙니다. ');
  }
  const count = await modules.models.user.findAll({
    where: {
      user_id: {
        [modules.models.Sequelize.Op.in]: user_id_array,
      },
      type: 0,
    },
  });
  if (count.length !== user_id_array.length) {
    throw new Error('user_id_array: 학생이 아니거나 등록되지 않은 user_id가 포함되어 있습니다. ');
  }

  // 없으면 생성하고, 있으면 update 함
  const where = {
    lecture_item_id,
    answer,
  };
  const res1 = await modules.updateOrCreate(modules.models.cheating_large, where, {
    lecture_item_id,
    answer,
  });
  const { cheating_id } = res1.item;

  // 없으면 생성하고, 있으면 update 함 (있으면 pass 하는것으로 하려 했으나, 기존 코드를 재활용함)
  for (let i = 0; i < user_id_array.length; i += 1) {
    const where2 = {
      cheating_id,
      user_id: user_id_array[i],
    };
    await modules.updateOrCreate(modules.models.user_cheating, where2, {
      cheating_id,
      user_id: user_id_array[i],
    });
  }

  res.json({ success: true });
}));

router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const {
    lecture_item_id,
  } = req.query;

  if (lecture_item_id === undefined) throw new Error('lecture_item_id: 반드시 입력해주세요. ');

  const answer_user_array = await modules.models.cheating_large.findAll({
    attributes: ['answer'],
    where: { lecture_item_id },
    include: {
      attributes: ['user_id', 'email_id', 'name'],
      model: modules.models.user,
    },
  });

  res.json({ success: true, lecture_item_id, answer_user_array });
}));

module.exports = router;
