const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');

/*
// 인증 (공통)
router.use(modules.auth.tokenCheck);
router.use(modules.auth.checkTokenIsAdmin);
*/

// 등록
router.post('/', modules.asyncWrapper(async (req, res) => {
  const data = req.body;

  // 등록
  await modules.models.university.create(data);

  // 응답
  res.json({ success: true });
}));

// 목록 조회
router.get('/list', modules.asyncWrapper(async (req, res) => {

  // 조회
  const univList = await modules.models.university.findAll({
    attributes: modules.models.university.selects.set1,
  });

  // 응답
  res.json(univList);
}));

// 수정
router.put('/', modules.asyncWrapper(async (req, res) => {
  const {
    code,
    old_name,
    new_name,
    address,
    manager_name,
    manager_email,
    manager_phone_number,
  } = req.body;

  if (old_name === undefined) {
    throw new Error('old_name은 반드시 입력되어야 합니다.');
  }

  const contents = {
    code,
    address,
    manager_name,
    manager_email,
    manager_phone_number,
  };
  if (new_name !== undefined) contents.name = new_name;

  // 수정
  const res1 = await modules.models.university.update(contents, {
    where: { name: old_name },
  });

  // 응답
  if (res1[0] !== 1) {
    throw new Error('update 문에 영향을 받은 열의 수가 1이 아닙니다.');
  }
  res.json({ success: true });
}));

// 대학 이름 목록 조회
router.get('/namelist', modules.asyncWrapper(async (req, res) => {
  // 조회
  const univList = await modules.models.university.findAll({
    attributes: ['name'],
  });

  // 응답
  res.json(univList);
}));

// 단일 대학 정보 조회
router.get('/', modules.asyncWrapper(async (req, res) => {
  const { name } = req.query;

  // 조회
  const res1 = await modules.models.university.findOne({
    attributes: modules.models.university.selects.set1,
    where: { name },
  });

  // 응답
  res.json(res1);
}));

// 삭제
router.delete('/', modules.asyncWrapper(async (req, res) => {
  const { name } = req.query;

  const res1 = await modules.models.university.destroy({
    where: { name },
  });

  // 응답
  if (res1 !== 1) {
    throw new Error('delete 문에 영향을 받은 열의 수가 1이 아닙니다.');
  }
  res.json({ success: true });
}));

// 에러 처리
router.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({ success: false, message: err.message });
});

module.exports = router;
