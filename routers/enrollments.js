const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
var multer  = require('multer');
const execTimeout10 = require('../src/utility');

// 학생이 공개 과목에 대해 수강신청 -> 대기중
router.post('/apply', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId = req.body.class_id;

  const studentEnroll = await modules.models.enrollment.create({
    class_id: classId,
    user_id: authId,
    status: 0, // 우선은 대기중으로
  });

  const result = await modules.models.class.findOne({
    attributes: ['type'],
    where: {
      class_id: classId,
    },
  });

  if (parseInt(result.type, 10) === 4) {
    await modules.models.enrollment.update({
      status: 1, // 강사가 승인
    }, {
      where: {
        class_id: classId,
        user_id: authId,
      }
    });
    // user_classes에도 추가
    await modules.models.user_class.create({
      role: 'student',
      class_id: classId,
      user_id: authId,
    });
  }

  res.json({ success: true, result });
}));

router.delete('/apply/:class_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId = req.params.class_id;

  const studentEnroll = await modules.models.enrollment.findOne({
    where: {
      class_id: classId,
      user_id: authId,
      status: 0,
    }
  });

  if (studentEnroll === null) {
    res.json({ success: false });
  } else {
    await studentEnroll.destroy();
    res.json({ success: true });
  }
}));

router.delete('/cancel_preview/:class_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId = req.params.class_id;

  const studentEnroll = await modules.models.enrollment.findOne({
    where: {
      class_id: classId,
      user_id: authId,
      status: 1,
    }
  });
  if (studentEnroll === null) {
    res.json({ success: false });
  } else {
    // user_classes에도 추가
    const result = await modules.models.user_class.findOne({
      where: {
        role: 'student',
        class_id: classId,
        user_id: authId,
      }
    });
    await studentEnroll.destroy();
    await result.destroy();
  }
  res.json({ success: true });
}));

router.get('/apply', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;

  const list = await modules.models.enrollment.findAll({
    where: {
      user_id: authId,
    }
  });

  res.json({ list });
}));

// 해당 강의가 서버시간 기준으로 수강 신청 시간 전인지 중인지 후인지 return
router.get('/check_time/:class_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const classId = parseInt(req.params.class_id, 10);

  const classData = await modules.models.class.findOne({
    where: {
      class_id: classId,
    }
  });

  let status;
  
  if (classData.sign_up_start_time > Date.now()) {
    status = 'before';
  } else if (classData.sign_up_end_time >= Date.now()) {
    status = 'during';
  } else {
    status = 'after';
  }

  res.json({ status });
}));

// 강사 입장에서 수강신청 대기중인 학생들 목록 불러오기
router.get('/waiting/:class_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId = parseInt(req.params.class_id, 10);

  const enrollData = await modules.models.enrollment.findAll({
    where: {
      class_id: classId,
      status: 0,
    },
    include: [
      { model: modules.models.user }
    ]
  });
  console.log(enrollData);
  res.json({ enrollData });
}));

// 강사가 특정 학생 수강신청 승인
router.put('/permit/:class_id/:user_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId = parseInt(req.params.class_id, 10);
  const userId = parseInt(req.params.user_id, 10);

  // 수강신청 승인
  await modules.models.enrollment.update({
    status: 1, // 강사가 승인
  }, {
    where: {
      class_id: classId,
      user_id: userId,
    }
  });

  // user_classes에도 추가
  await modules.models.user_class.create({
    role: 'student',
    class_id: classId,
    user_id: userId,
  });
  res.json({ success: true });

  // 2021-03-29 실습과목에 학생 수락 시 Docker에 계정을 생성한다.
  // 1. 현재 과목이 실습 과목인지 확인
  const classData = await modules.models.class.findOne({
    where: {
      class_id: classId,
    }
  });  
  // 2. user_id로 학생의 email_id를 가져온다
  if (classData.practice === 1){
    const user = await modules.models.user.findOne({
      where: {
        user_id: userId,
      },
    });
    // 3. adduser
    const add_user_cmd = "sudo docker exec class" + classId + " adduser -q --gecos \"\" --disabled-password '" + user.email_id + "' --force-badname";
    try{
      await execTimeout10(add_user_cmd);
    }
    catch(e){
      console.error("add user error: ", e);
    }
  }
}));

// 강사가 특정 학생 수강신청 거절
router.put('/refuse/:class_id/:user_id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const classId = parseInt(req.params.class_id, 10);
  const userId = parseInt(req.params.user_id, 10);

  // 수강신청 거절
  await modules.models.enrollment.update({
    status: 2, // 강사가 거절
  }, {
    where: {
      class_id: classId,
      user_id: userId,
    }
  });
  res.json({ success: true });
}));

// 엑셀로 업로드 할 경우 수강신청 성공 승인
router.post('/excel', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const {class_id, user_id } = req.body;

  const where = {
    class_id,
    user_id,
  };

  const studentEnroll = await modules.updateOrCreate(modules.models.enrollment, where, {
    class_id,
    user_id,
    status: 1, // 강사가 승인
  });
  
  res.json({ success: true, studentEnroll });
}));

module.exports = router;
