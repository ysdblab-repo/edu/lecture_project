const express = require('express');
const router = express.Router();
const sequelize = require('sequelize')
const modules = require('../modules/frequent-modules');

var updateOrCreate = function (model, where, newItem, onCreate, onUpdate, onError) {
  // First try to find the record
  model.findOne({where: where}).then(function (foundItem) {
    if (!foundItem) {
      // Item not found, create a new one
      model.create(newItem)
        .then(onCreate)
        .catch(onError);
    } else {
      // Found an item, update it
      model.update(newItem, {where: where})
        .then(onUpdate)
        .catch(onError);
      ;
    }
  }).catch(onError);
}


router.post('/update_cs', modules.asyncWrapper(async (req, res, next) => {
  console.log(req.body)
  updateOrCreate(
    modules.models.student_selfstudy_log,
    {lecture_id: req.body.lecture_id,user_id: req.body.user_id},
    {lecture_id: req.body.lecture_id,user_id: req.body.user_id,
      concentration_score_type: req.body.concentration_score_type,
      concentration_score_time: req.body.concentration_score_time,
	  concentration_score: (parseFloat(req.body.concentration_score_type) + parseFloat(req.body.concentration_score_time)) / 2
	  },
    function () {
      res.json({success:true})
      console.log('created');
    },
    function () {
      res.json({success:true})
      console.log('updated');
    },
    console.log);
}));
router.get('/get_by_user/:user_id', modules.asyncWrapper(async (req, res, next) => {

  const user_id = req.params.user_id;
  modules.models.student_selfstudy_log.find({ where: {'user_id': user_id} }).then(c => {
    res.json(c);
  })}));
  
module.exports = router;
