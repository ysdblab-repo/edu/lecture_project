const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const { Op } = require("sequelize");

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const lectureItemId = parseInt(req.body.lectureItemId, 10);
  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId);

  if (lectureItem === null) { // 해당 lectureItemId가 존재하지 않으면
    throw modules.errorBuilder.default('Invalid lectureItemId', 404, true);
  } else if (lectureItem.type !== 6) {
    throw modules.errorBuilder.default('Invalid lectureItem type', 406, true);
  }

  const teacherId = (await modules.models.lecture.findByPk(lectureItem.lecture_id)).teacher_id;
  const teacherCheck = (teacherId === authId);  // 입력한 lectureItemId의 강사인가?

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 lectureItem의 강사(제작자) 이던가
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const res1 = await modules.models.individual_question_info.create({
    lecture_item_id: lectureItemId,
    creator_id: authId,
  });

  res.json({ data: res1 });
}));

router.put('/:lectureItemId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const lectureItemId = parseInt(req.params.lectureItemId, 10);
  let { type, difficulty, keyword } = req.body;
  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.individual_question_info,
    }
  });

  if (lectureItem === null) { // 해당 lectureItemId가 존재하지 않으면
    throw modules.errorBuilder.default('Invalid lectureItemId', 404, true);
  } else if (lectureItem.type !== 6) {
    throw modules.errorBuilder.default('Invalid lectureItem type', 406, true);
  }
  // 권한 체크
  const teacherId = (await modules.models.lecture.findByPk(lectureItem.lecture_id)).teacher_id;
  const teacherCheck = (teacherId === authId);  // 입력한 lectureItemId의 강사인가?

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 lectureItem의 강사(제작자) 이던가
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  // 변경 조건 존재 확인
  if (type === undefined) {
    throw modules.errorBuilder.default('type is undefined', 406, true);
  }
  if (difficulty === undefined) {
    throw modules.errorBuilder.default('difficulty is undefined', 406, true);
  }
  if (keyword === undefined) {
    throw modules.errorBuilder.default('keyword is undefined', 406, true);
  }
  // 변경 수행
  const typeRes = await modules.models.individual_question_info.update({ type: type }, {
    where: { info_id: lectureItem.individual_question_infos[0].info_id }
  });
  difficulty = difficulty.split(',');
  if (difficulty.length > 2) { // difficulty가 min,max 두개보다 많으면
    throw modules.errorBuilder.default(`invalid difficulty length: ${difficulty.length}`, 406, true);
  }
  difficulty = difficulty.join(modules.config.separator);
  const diffRes = await modules.models.individual_question_info.update({ difficulty: difficulty }, {
    where: { info_id: lectureItem.individual_question_infos[0].info_id }
  });
  keyword = keyword.split(',').join(modules.config.separator);
  const keyRes = await modules.models.individual_question_info.update({ keyword: keyword }, {
    where: { info_id: lectureItem.individual_question_infos[0].info_id }
  });
  res.json({ data: { typeRes: typeRes, diffRes: diffRes, keyRes: keyRes }});
}));

router.delete('/:lectureItemId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const lectureItemId = parseInt(req.params.lectureItemId, 10);

  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId);

  const lecture = await modules.models.lecture.findByPk(lectureItem.lecture_id);

  if (lectureItem === null) { // 해당 개인화 문항 정보가 존재하지 않으면
    throw modules.errorBuilder.default('Invalid lectureItemId', 404, true);
  } else if (lectureItem.type !== 6) {
    throw modules.errorBuilder.default('Invalid lectureItem type', 406, true);
  }

  const teacherCheck = (lecture.teacher_id === authId); // 자신의 개인화 문항 정보에 접근하는지 검사

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 자신의 개인화 문항에 접근하지 않으면
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const destroyLectureItem = await modules.models.lecture_item.destroy({ where: { lecture_item_id: lectureItemId }});

  res.json({ data: destroyLectureItem });
}));

/* info 아이템과는 다르게 기존에 은행에 있던 item을 가져오기 때문에 lecture_item_id가 아닌 bankItemId를 사용
  lecture_item(type 7 개인화 문항(정보X))을 info(기존에 새로 만들어진 lecture_item에 info를 mapping)와는 다르게 여기서 만드는 
  이유는 기존의 bank item의 정보를 그대로 가져오기 때문에 여기서 만드는 것이 편하기 떄문
*/
router.post('/questions', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  let { lectureItemId, lectureId, bankItemId } = req.body;
  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.individual_question_info
    }
  });

  if (lectureItem === null) { // 해당 개인화 문항 정보가 존재하지 않으면
    throw modules.errorBuilder.default('Invalid lectureItemId', 404, true);
  }
  if (lectureItem.type !== 6) {
    throw modules.errorBuilder.default('Invalid lectureItem type', 406, true);
  }
  if (lectureItem.individual_question_infos === null) {
    throw modules.errorBuilder.default('individual_question_info is not defined', 404, true);
  }
  if (lectureId === undefined) { // 옮길 강의 id가 존재하지 않으면
    throw modules.errorBuilder.default('lectureId is undefined', 406, true);
  }
  if (bankItemId === undefined) { // 옮겨질 강의 아이템 id가 존재하지 않으면
    throw modules.errorBuilder.default('bankItemId is undefined', 406, true);
  }

  bankItemId = bankItemId.split(',');
  const items = await modules.models.lecture_item.findAll({ where: { lecture_id: lectureId }}); // sequence 계산 용도

  let checkFunc = () => { // 가져오려는 아이템에 대한 권한들 확인
    return new Promise(async (resolve, reject) => {
      const lecture = await modules.models.lecture.findByPk(lectureId);
      const teacherId = lecture.teacher_id;
      let groupIds = new Set();
      let groupTeacherIdList;
      let forEachCount = 0;

      bankItemId.forEach(async itemId => { // 검증용
        const bankLectureItem = await modules.models.bank_lecture_item.findByPk(itemId);
        if (bankLectureItem === null) {
          throw modules.errorBuilder.default('unvalid bankItemId', 406, true);
        }
        groupIds.add(bankLectureItem.group_id);
        forEachCount++;
        if (forEachCount === bankItemId.length) {
          groupTeacherIdList = await modules.models.teacher_group.findAll({ where: {
            group_id: {
              [Op.or]: Array.from(groupIds),
            },
          }});
          const teacherCheck = (teacherId === authId && groupTeacherIdList.some(id => id.user_id === authId )); // 입력한 lectureId의 강사이고 bankItem의 소속 그룹에 접근할 권한이 있는가?
          if (!(authType === 3 || teacherCheck)) { // 관리자이거나 lectureItem의 강사(제작자) 이던가
            resolve(false);
          }
          resolve(true);
        }
      });
    });
  };

  const resCheckFunc = await checkFunc();

  if (resCheckFunc === false) { // 접근 권한이 없을때
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  // 접근 권한이 있으면
  let individualQuestionRes = [];
  let forEachCount2 = 0;

  bankItemId.forEach(async itemId => {
    const bankLectureItem = await modules.models.bank_lecture_item.findByPk(itemId, { // 은행에 저장되어있는 아이템 찾기
      include: {
        model: modules.models.bank_question,
        include: [
          { model: modules.models.bank_problem_testcase}, 
          { model: modules.models.bank_oj_problem },
          { model: modules.models.bank_file}, 
          { model: modules.models.bank_file, as: 'question_material' },
          { model: modules.models.bank_question_keyword} 
        ],
      }
    });

    const newActiveLectureItem = await modules.models.lecture_item.create({ // 새 lecture item 생성
      name: bankLectureItem.name,
      start_time: bankLectureItem.start_time,
      end_time: bankLectureItem.end_time,
      type: 7, // type 7 개인화 문항 개별 아이템
      past_opend: 0,
      order: bankLectureItem.order,
      sequence: items.length+1,    // 기존에 있던 item len + 1
      result: bankLectureItem.result,
      offset: bankLectureItem.offset,
      opend: 0,
      scoring_finish: 0,
      lecture_id: lectureId,
    });

    const newActiveLectureItemId = newActiveLectureItem.lecture_item_id;

    // 아이템 복제
    const bankQuestions = bankLectureItem.bank_questions;
    const newActiveQuestions = await Promise.all(bankQuestions.map(item => (async () => {
      const newActiveQuestion = await modules.models.question.create({
        lecture_item_id: newActiveLectureItemId,
        type: item.type,
        question: item.question,
        choice: item.choice,
        choice2: item.choice2,
        answer: item.answer,
        score: item.score,
        is_ordering_answer: item.is_ordering_answer,
        accept_language: item.accept_language,
        order: (item.order === null ? 0 : item.order),
        showing_order: (item.showing_order === null ? 0 : item.showing_order),
        difficulty: item.difficulty,
        timer: item.timer,
        creator_id: item.creator_id,
        sqlite_file_guid: item.sqlite_file_guid,
        info_id: lectureItem.individual_question_infos[0].info_id,
        info_item_id: lectureItemId,
        answer_media_type: item.answer_media_type,
        question_media_type: item.question_media_type,
        question_media: item.question_media,
      });

      const bankQuestionKeywords = item.bank_question_keywords;
      const newQuestionKeywords = await Promise.all(bankQuestionKeywords.map(key => (async () => {
        const newQuestionKeyword = await modules.models.question_keyword.create({
          lecture_item_id: newActiveLectureItemId,
          score_portion: key.score_portion,
          keyword: key.keyword,
          question_id: newActiveQuestion.question_id,
          lecture_id: lectureId,
        });
        return newQuestionKeyword;
      })()));
  
      const ojProblem = item.bank_oj_problem;
      const problemTestCases = item.bank_problem_testcases;
      let newActiveOjProblem = null;
      if (ojProblem !== null) {
        const bankOjProblem = await modules.models.oj_problem.create({
          problem_id: newActiveQuestion.question_id,
          title: ojProblem.title,
          description: ojProblem.description,
          input: ojProblem.input,
          output: ojProblem.output,
          sample_input: ojProblem.sample_input,
          sample_output: ojProblem.sample_output,
          spj: ojProblem.spj,
          hint: ojProblem.hint,
          source: ojProblem.source,
          in_date: ojProblem.in_date,
          time_limit: ojProblem.time_limit,
          memory_limit: ojProblem.memory_limit,
          defunct: ojProblem.defunct,
          accepted: ojProblem.accepted,
          submit: 0,
          solved: 0, // 생각 해보자 여기 ... 0 으로 초기화 되는게 맞는거 같은데
        });
      }
  
      let newActiveProblemTestCases = [];
      if (problemTestCases.length > 0) {
        newActiveProblemTestCases = await Promise.all(problemTestCases.map(testcase => (async () => {
          const bankProblemTestCase = await modules.models.problem_testcase.create({
            question_id: newActiveQuestion.question_id,
            num: testcase.num,
            input: testcase.input,
            output: testcase.output,
            input_path: testcase.input_path,
            output_path: testcase.output_path,
          });
          return newActiveProblemTestCases;
        })()));
      }
      const files = item.bank_files;
      if ( files.length > 0 ) {
        files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'question_id', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
        })
      }
  
      const material_files = item.question_material;
      if ( material_files.length > 0) {
        material_files.forEach(function(element){
          modules.upload.copy(element.static_path, element.name, 'question_id_material', newActiveQuestion.question_id, authId, newActiveLectureItemId, 1);
        })
      }
      return {
        newActiveQuestion,
        newActiveOjProblem,
        newActiveProblemTestCases
      };
    })()));
    individualQuestionRes.push(newActiveQuestions[0]);
    forEachCount2++;
    if (forEachCount2 === bankItemId.length) { // forEach 다 돌리면 마지막에
      res.json({ success: true, data: individualQuestionRes });
    }
  });
}));

router.put('/questions/:lectureItemId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const { score } = req.body;
  const lectureItemId = parseInt(req.params.lectureItemId, 10);
  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId);
  const lecture = await modules.models.lecture.findByPk(lectureItem.lecture_id);
  if (lectureItem.type !== 7) { // 올바르지 않은 타입 개인화 문항이 아닐때
    throw modules.errorBuilder.default('Invalid lectureItemId', 406, true);
  }
  if (lecture.teacher_id !== authId) { // 접근 권한 없음
    throw modules.errorBuilder.default('Permission denied', 403, true);
  }
  const res1 = modules.models.question.update({ score: score }, {
    where: { lecture_item_id: lectureItemId }
  });

  res.json({ data: res1 });
}));

router.delete('/questions/:lectureItemId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const lectureItemId = parseInt(req.params.lectureItemId, 10);
  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId);
  const lecture = await modules.models.lecture.findByPk(lectureItem.lecture_id);

  if (lectureItem === null) { // 해당 개인화 문항 정보가 존재하지 않으면
    throw modules.errorBuilder.default('Invalid lectureItemId', 404, true);
  } else if (lectureItem.type !== 7) {
    throw modules.errorBuilder.default('Invalid lectureItem type', 406, true);
  }

  const teacherCheck = (lecture.teacher_id === authId); // 자신의 개인화 문항 정보에 접근하는지 검사

  if (!(authType === 3 || teacherCheck)) { // 관리자이거나 자신의 개인화 문항에 접근하지 않으면
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const destroyLectureItem = await modules.models.lecture_item.destroy({ where: { lecture_item_id: lectureItemId }});

  res.json({ data: destroyLectureItem });
}));

router.get('/questions/indices/:lectureItemId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const lectureItemId = parseInt(req.params.lectureItemId, 10);
  const res1 = await modules.models.lecture_item.findByPk(lectureItemId);
  if (!(res1.dataValues.type === 6)) {
    throw modules.errorBuilder.default('Invalid lectureItem type', 406, true);
  }
  const res2 = await modules.models.individual_question_student_indice.findAll({
    where: {
      lecture_item_id: lectureItemId,
    }
  });
  // 해당 info의 모든 학생의 indice배정 기록중 현재 요청한 학생의 id와 동일한 것이 존재하는지
  const res3 = res2.filter((indice) => {
    if (indice.dataValues.student_id === authId) {
      return true;
    } else {
      return false;
    }
  });
  if (res3.length === 0) { // 존재하지 않으면 생성
    await modules.models.individual_question_student_indice.create({
      student_id: authId,
      lecture_item_id: lectureItemId,
      question_indice: res2.length,
    });
    individualQuestionIndice = res2.length;
  } else { // 존재하면 이전에 저장된 indice를 사용
    individualQuestionIndice = res3[0].question_indice;
  }
  res.json({ data: { indice: individualQuestionIndice} });
}));

module.exports = router;
