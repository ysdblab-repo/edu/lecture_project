const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const modules = require('../modules/frequent-modules');
const mailer = require('../modules/mailer');
const db = require('../modules/db');
const { sequelize } = require('../models');
const { Op } = require("sequelize");
const { isNull } = require('lodash');

router.get('/', modules.auth.tokenCheck, (req, res) => {
  res.json({ success: true });
});

router.post('/login/v4', modules.asyncWrapper(async (req, res) => {
  const {
    email_id,
    password,
    permanent,
  } = req.body;
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');

  const user = await modules.models.user.findOne({
    where: {
      email_id
    },
  });
  let terms = null;
  if (!user) {
    throw modules.errorBuilder.default('user is not exist', 403, true);
  } else if ( user.password !== encrypted ) {
    throw modules.errorBuilder.default('password is not correct', 406, true);
  } 
  if (user.terms_agreed) {
    terms = 1;
  } else {
    terms = 0;
  }
  const time = permanent ? 100000 : 14;
  const { accessToken, refreshToken } = await modules.sign_v4(user.dataValues, time);

  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;


  await modules.models.login_log.create({
    ip,
    user_id: user.user_id,
  });

  res.cookies('refreshToken', refreshToken, { expires: new Date(Date.now() + 1000 * 60 * 60 * time)}, 'httpOnly');

  res.json({ success: true, terms, accessToken });
}));

router.post('/login', modules.asyncWrapper(async (req, res) => {
  const {
    email_id,
    password,
    permanent,
  } = req.body;
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');

  // const user = await modules.models.user.findOne({ //choi -->
  //   where: {
  //     email_id,
  //   },
  // });
  // 위에는 기존에 사용했던 코드.
  const user = await modules.models.user.findOne({
    where: sequelize.and(
      {email_id: email_id},
      sequelize.where(
        sequelize.col('type'), sequelize.col('current_role')
      )
    )
  });

  let terms = null;
  if (!user) {
    throw modules.errorBuilder.default('user is not exist', 403, true);
  } else if ( user.password !== encrypted ) {
    throw modules.errorBuilder.default('password is not correct', 406, true);
  } 
  if (user.terms_agreed) {
    terms = 1;
  } else {
    terms = 0;
  }
  const token = await modules.sign(user.dataValues, permanent);
  // token 값이 jwt의 값이랑 같다.

  
 

  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

  await modules.models.login_log.create({
    ip,
    user_id: user.user_id,
  });

  const user1 = await modules.models.user.findOne({
    where: {
      email_id: email_id,
    }
  });
  // if (isNull(user1)) {
  //   // res.json({ success: false, flag : 3 });
  //   res.json({ success: true, terms, token });
  // } else {
  //   const temp = await modules.models.login_check_phone.findOne({
  //     where: {
  //       user_id: user1.user_id,
  //     }
  //   });
  //   if (temp.flag == true ) {
  //     // console.log('pc로 이미 로그인');
  //     res.json({ success: false, terms, token });
  //   }else if (temp.flag == false ) {
  //     // console.log('mobile로 이미 로그인');
  //     res.json({ success: false, terms, token });
  //   }
  // }
  
  


  res.json({ success: true, terms, token });
  
}));
// wzh

router.post('/login_check_phone', modules.asyncWrapper(async (req, res) => {
  const {
    flag,
    // ip,
    email_id,
  } = req.body;

  const user = await modules.models.user.findOne({
    where: {
      email_id: email_id,
    }
  });
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

  await modules.models.login_check_phone.create({
    flag: flag,
    ip: ip,
    user_id: user.user_id,
  });
  res.json({ successs: true });
}))
router.post('/login_check_phone_delete', modules.asyncWrapper(async (req, res) => {
  const {
    user_id,
  } = req.body;
  await modules.models.login_check_phone.destroy({
    where: { user_id },
  });
  res.json({ success: true });
}))
router.post('/login_check_phone_flag', modules.asyncWrapper(async (req, res) => {
  const {
    email_id,
  } = req.body;
  const user = await modules.models.user.findOne({
    where: {
      email_id: email_id,
    }
  });
  if (isNull(user)) {
    res.json({ success: false, flag : 3 });
  } else {
    const temp = await modules.models.login_check_phone.findOne({
      where: {
        user_id: user.user_id,
      }
    });
    if (isNull(temp)) {
      res.json({ success: true });
    }else if (temp.flag == true ) {
      // console.log('pc로 이미 로그인');
      res.json({ success: false, flag : 1 });
    }else if (temp.flag == false ) {
      // console.log('mobile로 이미 로그인');
      res.json({ success: false, flag : 2 });
    }
  }


  // res.json({ success: true ,flag: temp.flag});
}));
router.post('/login_check_phone_ip', modules.asyncWrapper(async (req, res) => {
  const {
    email_id,
  } = req.body;

  const user = await modules.models.user.findOne({
    where: {
      email_id,
    },
  });
  
  let user_id=user.user_id;
  const user2=await modules.models.login_check_phone.findAll({
    where: { user_id },
  });

  res.json({ success: true,user,user2});
}))


router.get('/:email/:password/login_check', modules.asyncWrapper(async (req, res) => {
  const { email, password } = req.params;
 
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');

  // const user = await modules.models.user.findOne({ //choi -->
  //   where: {
  //     email_id,
  //   },
  // });
  // 위에는 기존에 사용했던 코드.
  
  const user = await modules.models.user.findOne
  ({
    where: sequelize.and(
      {email_id: email},
      sequelize.where(
        sequelize.col('type'), sequelize.col('current_role')
      )
    )
  });
  
  let terms = null;
  if (!user) {
    throw modules.errorBuilder.default('user is not exist', 403, true);
  } else if ( user.password !== encrypted ) {
    throw modules.errorBuilder.default('password is not correct', 406, true);
  } 
  

  res.json({ success: true, user}); 
  
}));
router.post('/test-login', modules.asyncWrapper(async (req, res, next) => {
  const {
    email_id,
    password,
  } = req.body;
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');

  const user = await modules.models.user.findOne({
    where: {
      email_id,
    },
  });
  if (!user) {
    throw modules.errorBuilder.default('user is not exist', 403, true);
  } else if ( user.password !== encrypted ) {
    throw modules.errorBuilder.default('password is not correct', 406, true);
  } 
  const token = await modules.sign(user.dataValues);
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

  modules.login(ip, token, user.user_id);

  // await modules.models.login_log.create({
  //   ip,
  //   user_id: user.user_id,
  // });

  res.json({ success: true, token });
}));
router.get('/terms', modules.auth.tokenCheck, modules.asyncWrapper(async(req,res) => {
    
  const { authId } = req.decoded;
  const sql = `UPDATE users SET terms_agreed = now() where user_id = ${authId}`;
  let result = await modules.db.getQueryResult(sql);
  res.json(result);
    
}));
router.get('/email-verify', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, email_id } = req.decoded;
  const verificationInfo = await modules.models.email_verification.findOne({
    where: {
      user_id: authId,
    },
  });

  res.json({ success: true });

  await mailer.sendVerificationMail(email_id, authId, verificationInfo.dataValues.rand);
}));

router.get('/verify/:id/:rand', modules.asyncWrapper(async (req, res, next) => {
  const { id, rand } = req.params;
  const user_id = parseInt(id, 10);
  const verificationInfo = await modules.models.email_verification.findOne({
    where: { user_id },
  });
  if (!verificationInfo) {
    return res.send('<h1>Already Verified</h1>');
  }
  if (verificationInfo.dataValues.rand !== rand) {
    throw modules.errorBuilder.default('Verification Code Incorrect', 403, true);
  }

  await Promise.all([
    modules.models.user.update({
      email_verified: 1,
    }, {
      where: { user_id },
    }),
    modules.models.email_verification.destroy({
      where: { user_id },
    }),
  ]);

  res.send('<h1>Email Verified</h1>');
}));

router.get('/:email/find-password', modules.asyncWrapper(async (req, res, next) => {
  const email_id = req.params.email;

  const user = await modules.models.user.findOne({
    where: { email_id },
  });
  if (!user) {
    throw modules.errorBuilder.default('Email Not Found', 404, true);
  }
  const rand = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(`${email_id}${(new Date()).getTime()}`)
    .digest('hex');

  await modules.models.find_password.destroy({
    where: { user_id: user.user_id },
  });

  await modules.models.find_password.create({
    user_id: user.user_id,
    rand,
  });

  res.json({ success: true });

  await mailer.sendFindPasswordMail(email_id, user.user_id, rand);
}));

router.put('/:id/password', modules.asyncWrapper(async (req, res, next) => {
  const user_id = parseInt(req.params.id, 10);
  const { newPassword, key } = req.body;

  const randInfo = await modules.models.find_password.findOne({
    where: { user_id },
  });
  const user = await modules.models.user.findOne({
    where: { user_id },
  });
  if (!randInfo) {
    throw modules.errorBuilder.default('Finding History Not Found', 404, true);
  }
  if (randInfo.rand !== key) {
    throw modules.errorBuilder.default('Incorrect Key', 403, true);
  }

  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(newPassword)
    .digest('hex');

  user.password = encrypted;
  await user.save();

  res.json({ success: true });
}));

module.exports = router;
