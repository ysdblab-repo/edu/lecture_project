const express = require('express');
const router = express.Router();

const modules = require('../modules/frequent-modules');
const deleteModule = require('../modules/delete');

router.post('/', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  console.log("material back");
  const { authId, authType } = req.decoded;
  const { lecture_Id, material_type } = req.body;

  const lecture = await modules.models.lecture.findByPk(parseInt(lecture_Id, 10), {
    include: {
      model: modules.models.class,
    },
  });

  if (!lecture) {
    throw modules.errorBuilder.default('Lecture Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, lecture.class.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  if(!req.file || !req.fileInfo) {
    throw modules.errorBuilder.default('No file', 404, true);
  }
  const newMaterial = await modules.models.material.create({
    lecture_id: parseInt(lecture_Id, 10),
    creator_id: authId,
    material_type: parseInt(material_type, 10),
    score_sum: 0,
  });

  const file = await modules.upload.fileAdd2(req, 'material_id', parseInt(newMaterial.material_id, 10),lecture.class_id);

  res.json({ success: true, file });
}));

router.get('/:lectureId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

  const { authId, authType } = req.decoded;
  const lectureId = req.params.lectureId;

  const lecture = await modules.models.lecture.findByPk(parseInt(lectureId, 10), {
    include: {
      model: modules.models.class,
    },
  });

  if (!lecture) {
    throw modules.errorBuilder.default('Lecture Not Found', 404, true);
  }

  const material = await modules.models.material.findAll({
    where: {
      lecture_id: lectureId,
      material_type: 0,
    },
    include: [{
      model: modules.models.file,
      attributes: modules.models.file.selects.fileClient
    }, {
      model: modules.models.material_keyword,
      attributes: modules.models.material_keyword.selects.keywordInfo,
    }]
  });

  res.json(material);
}));

router.get('/additional/:lectureId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const lectureId = req.params.lectureId;

  const lecture = await modules.models.lecture.findByPk(parseInt(lectureId, 10), {
    include: {
      model: modules.models.class,
    },
  });

  if (!lecture) {
    throw modules.errorBuilder.default('Lecture Not Found', 404, true);
  }

  const material = await modules.models.material.findAll({
    where: {
      lecture_id: lectureId,
      material_type: 1,
    },
    include: [{
      model: modules.models.file,
      attributes: modules.models.file.selects.fileClient
    }, {
      model: modules.models.material_keyword,
      attributes: modules.models.material_keyword.selects.keywordInfo,
    }]
  });

  res.json(material);
}));

router.post('/:materialId/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const materialId = parseInt(req.params.materialId, 10);
  const { authId, authType } = req.decoded;
  const { keyword, score } = req.body;

  const material = await modules.models.material.findByPk(materialId, {
    include: {
      model: modules.models.lecture,
      include: {
        model: modules.models.class,
      }
    }
  });

  if (!material) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, material.lecture.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const newMaterialKeyword = await modules.models.material_keyword.create({
    material_id: material.material_id,
    lecture_id: material.lecture_id,
    keyword: keyword,
    score_portion: parseInt(score, 10)
  });

  if (!newMaterialKeyword) {
    throw modules.errorBuilder.default('Fail to Make keyword', 404, true);
  }
  const score_sum = material.score_sum + parseInt(score, 10);
  material.score_sum = score_sum;
  await material.save();
  res.json({ success: true, score_sum });

}));

router.put('/:materialId/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const materialId = parseInt(req.params.materialId, 10);
  const { authId, authType } = req.decoded;
  const { keyword, score } = req.body;

  const material = await modules.models.material.findByPk(materialId, {
    include: {
      model: modules.models.lecture,
      include: {
        model: modules.models.class,
      }
    }
  });

  if (!material) {
    throw modules.errorBuilder.default('No Material', 404, true);
  }

  const teacherCheck = await modules.teacherInClass(authId, material.lecture.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const materialKeyword = await modules.models.material_keyword.findOne({
    where: {
      keyword: keyword,
      material_id: materialId
    }
  });

  if (!materialKeyword) {
    throw modules.errorBuilder.default('No Material Keyword', 404, true);
  }

  const diff = parseInt(score, 10) - materialKeyword.score_portion;

  material.score_sum += diff;
  materialKeyword.score_portion = parseInt(score, 10);

  await material.save();
  await materialKeyword.save();

  const scoreSum = material.score_sum;

  res.json({ success: true, scoreSum });

}));

router.get('/:materialId/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const materialId = parseInt(req.params.materialId, 10);
  const { authId, authType } = req.decoded;

  const materialKeywords = (await modules.models.material.findByPk(materialId, {
    include: { model: modules.models.material_keyword },
  })).material_keywords;

  if (!materialKeywords) {
    throw modules.errorBuilder.default('Not Found -', 404, true);
  }

  res.json(materialKeywords);
}));

router.delete('/:materialId/keywords/:keyword', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const materialId = parseInt(req.params.materialId, 10);
  const keyword = req.params.keyword;
  const { authId, authType } = req.decoded;
  // const { keyword } = req.body; http delete에서 request body는 올 수 없음

  const material = await modules.models.material.findByPk(materialId, {
    include: {
      model: modules.models.lecture,
      include: {
        model: modules.models.class,
      },
    },
  });
  if (!material) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCheck = await modules.teacherInClass(authId, material.lecture.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  const materialKeyword = await modules.models.material_keyword.findOne({
    where: { keyword: keyword }
  });

  material.score_sum -= materialKeyword.score_portion;
  await material.save();
  await modules.models.material_keyword.destroy({
    where: {
      keyword: keyword,
      material_id: materialId
    },
  });

  res.json({ success: true });
}));

router.delete('/:materialId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const materialId = parseInt(req.params.materialId, 10);
  const { authId, authType } = req.decoded;

  const file = await modules.models.file.findOne({
    include: {
      model: modules.models.material,
      where: { material_id: materialId },
      include: {
        model: modules.models.lecture,
        include: {
          model: modules.models.class,
        },
      },
    }
  });
  const guid = file.file_guid;

  if (!file) {
    throw modules.errorBuilder.default('No file', 404, true);
  }

  const teacherCheck = await modules.teacherInClass(authId, file.material.lecture.class_id);
  if (!(authType === 3 || teacherCheck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await deleteModule.files(guid);
  await modules.models.material.destroy({
    where: {
      material_id: materialId
    }
  });

  res.json({ success: true });
}));

module.exports = router;


// router.delete('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
//   const { authId, authType } = req.decoded;
//   const materialId = parseInt(req.params.id, 10);
//
//   const material = await modules.models.material.findByPk(materialId, {
//     include: {
//       model: modules.models.lecture_item,
//       include: {
//         model: modules.models.lecture,
//       },
//     },
//   });
//   if (!material) {
//     throw modules.errorBuilder.default('Not Found', 404, true);
//   }
//   const teacherCkeck = await modules.teacherInClass(authId, material.lecture_item.lecture.class_id);
//   if (!(authType === 3 || teacherCkeck)) {
//     throw modules.errorBuilder.default('Permission Denied', 403, true);
//   }
//
//   await modules.models.material.destroy({
//     where: {
//       material_id: materialId,
//     },
//   });
//
//   res.json({ success: true });
// }));
