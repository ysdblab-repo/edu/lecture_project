const express = require('express');
const router = express.Router();

const modules = require('../modules/frequent-modules');

/*
로그 프로그램 실행, 종료 여부를 기록
*/
router.post('/', modules.asyncWrapper(async (req, res, next) => {
  const query = req.body
  console.log(req)
  await modules.models.student_program_log.destroy({
    where: {
      'user_id':query.user_id
    }
  })
  await modules.models.student_program_log.create(query);
  res.json({success:true})

}));

module.exports = router;