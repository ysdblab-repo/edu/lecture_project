const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const modules = require('../modules/frequent-modules');

router.post('/:lecture_id/:user_id', modules.asyncWrapper(async (req, res, next) => {
  await modules.models.realtime_lecture_heartbeat.create({ lecture_id: req.params.lecture_id, user_id: req.params.user_id });
  next();
  res.send({ success: true });
}));


module.exports = router;
