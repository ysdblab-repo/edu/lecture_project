const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const execTimeout10 = require('../src/utility');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
const { Op } = sequelize;
const mailer = require('../modules/mailer');

router.get('/class_list', modules.asyncWrapper(async (req, res, next) => {
  const res1 = await modules.models.kdata_class.findAll({ where : {}});
  // if (res1)res.status(409).send('이미 등록된 과목명입니다.');
  res.json({ class_list: res1 });
}));

router.post('/auth_mail', modules.asyncWrapper(async (req, res, next) => {
  const {
    email_id,
  } = req.body;
    
  //preswot에 등록된 email_id 인지 검사
  const res1 = await modules.models.user.findAll({ where : {email_id}});
  console.log('res1.length = ', res1.length);
  if(res1.length != 0) {
    throw new Error('기존 시스템에 등록된 메일 주소입니다.');
  }

  // kdata_apply에 등록된 email_id 인지 검사
  const res2 = await modules.models.kdata_apply.findAll({ where : {email_id}});
  if(res2.length != 0) {
    throw new Error('이미 수강 신청중인 메일 주소입니다.');
  }

  // 둘다 통과한 경우 - 기존 인증번호 제거 - 난수 생성 - db에 저장 - 이메일 발송
  await modules.models.mail_auth.destroy({
    where: {
      email_id,
    },
  })

  const rand = Math.floor(Math.random() * 899999) + 100000;

  await modules.models.mail_auth.create({
    email_id,
    auth_number: rand,
  })

  await mailer.sendAuthMail(email_id, rand);

  res.json({ success: true });
}));

router.post('/apply', modules.asyncWrapper(async (req, res, next) => {
  const {
    name,
    email_id,
    auth_number,
    class_id_list,
  } = req.body;

  if (name === undefined || name.length == 0) {
    throw new Error('name은(는) 반드시 포함되어야 합니다.');
  }
  if (email_id === undefined || email_id.length == 0) {
    throw new Error('email_id은(는) 반드시 포함되어야 합니다.');
  }
  if (auth_number === undefined || auth_number.length == 0) {
    throw new Error('auth_number은(는) 반드시 포함되어야 합니다.');
  }
  if (class_id_list === undefined || class_id_list.length == 0) {
    throw new Error('class_id_list은(는) 반드시 포함되어야 합니다.');
  }
  let class_id_list_str = null;
  if(Array.isArray(class_id_list)) {
    class_id_list_str = class_id_list.toString();
  } else {
    throw new Error('class_id_list은(는) 배열 값이 입력되어야 합니다.');
  }

  // email_id가 중복되는지 확인
    //preswot에 등록된 email_id 인지 검사
    const res1 = await modules.models.user.findAll({ where : {email_id}});
    console.log('res1.length = ', res1.length);
    if(res1.length != 0) {
      throw new Error('기존 시스템에 등록된 메일 주소입니다.');
    }
  
    // kdata_apply에 등록된 email_id 인지 검사
    const res2 = await modules.models.kdata_apply.findAll({ where : {email_id}});
    if(res2.length != 0) {
      throw new Error('이미 수강 신청중인 메일 주소입니다.');
    }

    // auth_number가 적절한지 확인
    const res3 = await modules.models.mail_auth.findAll({ where : {email_id, auth_number}});
    console.log('res3.length = ', res3.length);
    if(res3.length != 1) {
      throw new Error('메일 인증 오류입니다. 메일을 인증해주세요.');
    }

  // db에 입력
  await modules.models.kdata_apply.create({
    name,
    email_id,
    class_id_list: class_id_list_str,
  });

  // 완료
  res.json({ success: true });
}));

// 에러 처리
router.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({ success: false, message: err.message });
});

module.exports = router;