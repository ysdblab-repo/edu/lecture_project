const express = require('express');
const router = express.Router();
const db = require('../modules/db')
const modules = require('../modules/frequent-modules');

router.get('/student_class_logs/:user_id/:class_id/:is_student', modules.asyncWrapper(async (req, res, next) => {
  const user_id = req.params.user_id;
  const class_id = req.params.class_id;
  const is_student = req.params.is_student;

  //학생 개인의 저널링
  if(is_student==1) {
	  var queryResult = await db.getQueryResult(`SELECT *, c.name as class_name, \
 (select count(*) from user_classes where class_id = c.class_id and role = 'student') as class_num_student, \
  (select avg(concentration_score) from student_lecture_logs join user_classes on user_classes.class_id = ${class_id} and user_classes.user_id = student_lecture_logs.user_id) as class_avg_concentration_score, \
 (select avg(participation_score) from student_lecture_logs join user_classes on user_classes.class_id = ${class_id} and user_classes.user_id = student_lecture_logs.user_id) as class_avg_participation_score, \
 (select avg(understanding_score) from student_lecture_logs join user_classes on user_classes.class_id = ${class_id} and user_classes.user_id = student_lecture_logs.user_id) as class_avg_understanding_score, \
 (select max(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as max_participation_score, \
 (select min(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as min_participation_score, \
 (select max(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as max_concentration_score, \
 (select min(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as min_concentration_score, \
 (select max(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as max_understanding_score, \
 (select min(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as min_understanding_score, \
 (select avg(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as avg_understanding_score, \
 (select avg(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as avg_concentration_score, \
 (select avg(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as avg_participation_score \
from classes c \
join lectures l on c.class_id = l.class_id \
join student_lecture_logs sll on sll.lecture_id = l.lecture_id and sll.user_id = ${user_id} and sll.order = 1 \
where c.class_id = ${class_id}`);
  }
  // 강사가 보는 학생 클래스 저널링
  else {
   var queryResult = await db.getQueryResult(`SELECT *, c.name as class_name, \
 (select count(*) from user_classes where class_id = c.class_id and role = 'student') as class_num_student, \
 (select avg(concentration_score) from student_lecture_logs join user_classes on user_classes.class_id = ${class_id} and user_classes.user_id = student_lecture_logs.user_id) as class_avg_concentration_score, \
 (select avg(participation_score) from student_lecture_logs join user_classes on user_classes.class_id = ${class_id} and user_classes.user_id = student_lecture_logs.user_id) as class_avg_participation_score, \
 (select avg(understanding_score) from student_lecture_logs join user_classes on user_classes.class_id = ${class_id} and user_classes.user_id = student_lecture_logs.user_id) as class_avg_understanding_score, \
 (select max(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as max_participation_score, \
 (select min(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as min_participation_score, \
 (select max(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as max_concentration_score, \
 (select min(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as min_concentration_score, \
 (select max(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as max_understanding_score, \
 (select min(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as min_understanding_score, \
 (select avg(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as avg_understanding_score, \
 (select avg(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as avg_concentration_score, \
 (select avg(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as avg_participation_score \
from classes c \
join lectures l on c.class_id = l.class_id \
join student_lecture_logs sll on sll.lecture_id = l.lecture_id \
where c.class_id = ${class_id}`);
  }
  var lecture_id_json = {};
  var i = queryResult.length;
  while(i--)
  {
	  if (typeof lecture_id_json[queryResult[i].lecture_id] == 'undefined')
		  lecture_id_json[queryResult[i].lecture_id] = 1;
	  else{
		  queryResult.splice(i,1);
	  }
  }
 res.json(queryResult);
}));

router.get('/class_whole_stat/:user_id/:class_id/:is_student', modules.asyncWrapper(async (req, res, next) => {
  const user_id = req.params.user_id;
  const class_id = req.params.class_id;
  const is_student = req.params.is_student;
}));

//강의 저널링
router.get('/:user_id/:lecture_id/:is_student', modules.asyncWrapper(async (req, res, next) => {
  const user_id = req.params.user_id;
  const lecture_id = req.params.lecture_id;
  const is_student = req.params.is_student;
  if(is_student==1){
  var queryResult = await db.getQueryResult(`select \
 (select max(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_max_participation_score, \
 (select min(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_min_participation_score, \
 (select max(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_max_participation_score, \
 (select min(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_min_participation_score, \
 (select max(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_max_participation_score, \
 (select min(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_min_participation_score, \
 (select max(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_max_concentration_score, \
 (select min(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_min_concentration_score, \
 (select max(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_max_concentration_score, \
 (select min(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_min_concentration_score, \
 (select max(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_max_concentration_score, \
 (select min(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_min_concentration_score, \
 (select max(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_max_understanding_score, \
 (select min(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_min_understanding_score, \
 (select max(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_max_understanding_score, \
 (select min(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_min_understanding_score, \
 (select max(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_max_understanding_score, \
 (select min(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_min_understanding_score, \	
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .25 \
LIMIT 1) as o0_q1_participation_score, \	
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .25 \
LIMIT 1) as o1_q1_participation_score, \
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .25 \
LIMIT 1) as o2_q1_participation_score, \
\
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .5 \
LIMIT 1) as o0_q2_participation_score, \	
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .5 \
LIMIT 1) as o1_q2_participation_score, \
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .5 \
LIMIT 1) as o2_q2_participation_score, \
\
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .75 \
LIMIT 1) as o0_q3_participation_score, \	
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .75 \
LIMIT 1) as o1_q3_participation_score, \
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .75 \
LIMIT 1) as o2_q3_participation_score, \
\
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .25 \
LIMIT 1) as o0_q1_understanding_score, \	
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .25 \
LIMIT 1) as o1_q1_understanding_score, \
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .25 \
LIMIT 1) as o2_q1_understanding_score, \
\
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .5 \
LIMIT 1) as o0_q2_understanding_score, \	
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .5 \
LIMIT 1) as o1_q2_understanding_score, \
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .5 \
LIMIT 1) as o2_q2_understanding_score, \
\
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .75 \
LIMIT 1) as o0_q3_understanding_score, \	
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .75 \
LIMIT 1) as o1_q3_understanding_score, \
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .75 \
LIMIT 1) as o2_q3_understanding_score, \
\
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .25 \
LIMIT 1) as o0_q1_concentration_score, \	
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .25 \
LIMIT 1) as o1_q1_concentration_score, \
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .25 \
LIMIT 1) as o2_q1_concentration_score, \
\
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .5 \
LIMIT 1) as o0_q2_concentration_score, \	
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .5 \
LIMIT 1) as o1_q2_concentration_score, \
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .5 \
LIMIT 1) as o2_q2_concentration_score, \
\
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .75 \
LIMIT 1) as o0_q3_concentration_score, \	
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .75 \
LIMIT 1) as o1_q3_concentration_score, \
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .75 \
LIMIT 1) as o2_q3_concentration_score, \
l.*, \
(select understanding_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 0) as o0_my_understanding, \
(select understanding_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 1) as o1_my_understanding, \
(select understanding_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 2) as o2_my_understanding, \
(select participation_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 0) as o0_my_participation, \
(select participation_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 1) as o1_my_participation, \
(select participation_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 2) as o2_my_participation, \
(select concentration_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 0) as o0_my_concentration, \
(select concentration_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 1) as o1_my_concentration, \
(select concentration_score from student_lecture_logs where user_id = ${user_id} and lecture_id = ${lecture_id} and student_lecture_logs.order = 2) as o2_my_concentration, \
(select count(distinct(student_lecture_logs.user_id)) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as num_student_on_lecture, \
(select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 0 \
 and z.concentration_score > o0_my_concentration) as o0_num_better_concentration_score, \
 (select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 1 \
 and z.concentration_score > o0_my_concentration) as o1_num_better_concentration_score, \
 (select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 2 \
 and z.concentration_score > o0_my_concentration) as o2_num_better_concentration_score, \
 (select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 0 \
 and z.understanding_score > o0_my_understanding) as o0_num_better_understanding_score, \
 (select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 1 \
 and z.understanding_score > o0_my_understanding) as o1_num_better_understanding_score, \
 (select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 2 \
 and z.understanding_score > o0_my_understanding) as o2_num_better_understanding_score, \
  (select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 0 \
 and z.participation_score > o0_my_participation) as o0_num_better_participation_score, \
 (select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 1 \
 and z.participation_score > o0_my_participation) as o1_num_better_participation_score, \
 (select count(*) from student_lecture_logs z where z.lecture_id = l.lecture_id \
 and z.user_id != ${user_id} and z.order = 2 \
 and z.participation_score > o0_my_participation) as o2_num_better_participation_score \
	from lectures l \ 
	where l.lecture_id = ${lecture_id}`);
  var queryResult2 = await db.getQueryResult(`select DATE_FORMAT(log_time,'%H:%i:%s') as log_time, avg(understanding_score) as avg_understanding_score,avg(concentration_score) as avg_concentration_score,avg(participation_score) as avg_participation_score from student_lecture_timeline_logs where lecture_id = ${lecture_id} and user_id = ${user_id} group by log_time`);
  }
  else{
	 var queryResult = await db.getQueryResult(`select \
 (select max(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_max_participation_score, \
 (select min(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_min_participation_score, \
 (select max(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_max_participation_score, \
 (select min(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_min_participation_score, \
 (select max(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_max_participation_score, \
 (select min(student_lecture_logs.participation_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_min_participation_score, \
 (select max(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_max_concentration_score, \
 (select min(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_min_concentration_score, \
 (select max(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_max_concentration_score, \
 (select min(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_min_concentration_score, \
 (select max(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_max_concentration_score, \
 (select min(student_lecture_logs.concentration_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_min_concentration_score, \
 (select max(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_max_understanding_score, \
 (select min(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 0) as o0_min_understanding_score, \
 (select max(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_max_understanding_score, \
 (select min(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 1) as o1_min_understanding_score, \
 (select max(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_max_understanding_score, \
 (select min(student_lecture_logs.understanding_score) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id and student_lecture_logs.order = 2) as o2_min_understanding_score, \	
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .25 \
LIMIT 1) as o0_q1_participation_score, \	
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .25 \
LIMIT 1) as o1_q1_participation_score, \
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .25 \
LIMIT 1) as o2_q1_participation_score, \
\
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .5 \
LIMIT 1) as o0_q2_participation_score, \	
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .5 \
LIMIT 1) as o1_q2_participation_score, \
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .5 \
LIMIT 1) as o2_q2_participation_score, \
\
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .75 \
LIMIT 1) as o0_q3_participation_score, \	
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .75 \
LIMIT 1) as o1_q3_participation_score, \
(select x.participation_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.participation_score \
having sum(SIGN(1-SIGN(y.participation_score-x.participation_score))) / count(*) >= .75 \
LIMIT 1) as o2_q3_participation_score, \
\
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .25 \
LIMIT 1) as o0_q1_understanding_score, \	
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .25 \
LIMIT 1) as o1_q1_understanding_score, \
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .25 \
LIMIT 1) as o2_q1_understanding_score, \
\
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .5 \
LIMIT 1) as o0_q2_understanding_score, \	
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .5 \
LIMIT 1) as o1_q2_understanding_score, \
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .5 \
LIMIT 1) as o2_q2_understanding_score, \
\
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .75 \
LIMIT 1) as o0_q3_understanding_score, \	
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .75 \
LIMIT 1) as o1_q3_understanding_score, \
(select x.understanding_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.understanding_score \
having sum(SIGN(1-SIGN(y.understanding_score-x.understanding_score))) / count(*) >= .75 \
LIMIT 1) as o2_q3_understanding_score, \
\
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .25 \
LIMIT 1) as o0_q1_concentration_score, \	
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .25 \
LIMIT 1) as o1_q1_concentration_score, \
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .25 \
LIMIT 1) as o2_q1_concentration_score, \
\
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .5 \
LIMIT 1) as o0_q2_concentration_score, \	
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .5 \
LIMIT 1) as o1_q2_concentration_score, \
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .5 \
LIMIT 1) as o2_q2_concentration_score, \
\
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 0 and y.order = 0 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .75 \
LIMIT 1) as o0_q3_concentration_score, \	
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 1 and y.order = 1 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .75 \
LIMIT 1) as o1_q3_concentration_score, \
(select x.concentration_score \
from student_lecture_logs x, student_lecture_logs y \
where x.lecture_id = l.lecture_id and y.lecture_id = l.lecture_id and x.order = 2 and y.order = 2 \
group by x.concentration_score \
having sum(SIGN(1-SIGN(y.concentration_score-x.concentration_score))) / count(*) >= .75 \
LIMIT 1) as o2_q3_concentration_score, \
l.*, \
(select count(distinct(student_lecture_logs.user_id)) from student_lecture_logs where student_lecture_logs.lecture_id = l.lecture_id) as num_student_on_lecture
	from lectures l where l.lecture_id = ${lecture_id}`);
  var queryResult2 = await db.getQueryResult(`select DATE_FORMAT(log_time,'%H:%i:%s') as log_time, avg(understanding_score) as avg_understanding_score,avg(concentration_score) as avg_concentration_score,avg(participation_score) as avg_participation_score from student_lecture_timeline_logs where lecture_id = ${lecture_id} group by log_time`);
  }
  var resultJson = {};
  resultJson['result1'] = queryResult;
  resultJson['result2'] = queryResult2;
  res.json(resultJson);

}));

router.get(
  '/student_class_keyword/:user_id/:class_id/:is_student',
  modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const user_id = parseInt(req.params.user_id, 10);
    const class_id = parseInt(req.params.class_id, 10);
    const is_student = parseInt(req.params.is_student, 10);

    let data = [];

    /*
    if (is_student) {
      data = await modules.models.student_class_keyword.findAll({
        where: {
          student_id: user_id,
          class_id,
        },
        raw: true,
      });
      const keywordMaterial = await modules.db.getQueryResult(`select mk.keyword as keyword, mk.material_id as material_id,
l.name as lecture_name, i.name as item_name, i.lecture_item_id as lecture_item_id, l.lecture_id as lecture_id
from material_keywords as mk join lectures as l 
on l.lecture_id=mk.lecture_id and l.class_id=${class_id}
join materials as m on mk.material_id=m.material_id
join lecture_items as i on i.lecture_item_id=m.lecture_item_id
order by keyword`);

      for (let i = 0; i < data.length; i += 1) {
        data[i].materials = [];
      }
      let pre = '^&%&@(*$^';
      let preIndex = 0;
      for (let i = 0; i < keywordMaterial.length; i += 1) {
        const cur = keywordMaterial[i].keyword;
        if (pre === cur) {
          data[preIndex].materials.push(keywordMaterial[i]);
        } else {
          for (let j = 0; j < data.length; j += 1) {
            if (data[j].keyword === keywordMaterial[i].keyword) {
              pre = cur;
              preIndex = j;
              data[preIndex].materials.push(keywordMaterial[i]);
            }
          }
        }
      }
    } else {
      data = await modules.db.getQueryResult(`select keyword, avg(understanding) as understanding
from student_class_keywords where class_id=${class_id} group by class_id, keyword;`);
    }
*/
    res.json(data);
  })
);

router.get(
  '/student_lecture_keyword/:user_id/:lecture_id/:is_student',
  modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const user_id = parseInt(req.params.user_id, 10);
    const lecture_id = parseInt(req.params.lecture_id, 10);
    const is_student = parseInt(req.params.is_student, 10);

    let data = [];

    /*
    if (is_student) {
      data = await db.getQueryResult(`select keyword, avg(understanding) as understanding
from student_lecture_ordered_keywords
where student_id=${user_id} and lecture_id=${lecture_id}
group by student_id, lecture_id, keyword `);

      const keywordMaterial = await modules.db.getQueryResult(`select mk.keyword as keyword,
mk.material_id as material_id, i.name as item_name, l.name as lecture_name,
i.lecture_item_id as lecture_item_id, l.lecture_id as lecture_id
from material_keywords as mk
join materials as m on mk.material_id=m.material_id
join lecture_items as i on i.lecture_item_id=m.lecture_item_id
join lectures as l on i.lecture_id=l.lecture_id
where mk.lecture_id=${lecture_id}
order by keyword`);

      for (let i = 0; i < data.length; i += 1) {
        data[i].materials = [];
      }
      let pre = '^&%&@(*$^';
      let preIndex = 0;
      for (let i = 0; i < keywordMaterial.length; i += 1) {
        const cur = keywordMaterial[i].keyword;
        if (pre === cur) {
          data[preIndex].materials.push(keywordMaterial[i]);
        } else {
          for (let j = 0; j < data.length; j += 1) {
            if (data[j].keyword === keywordMaterial[i].keyword) {
              pre = cur;
              preIndex = j;
              data[preIndex].materials.push(keywordMaterial[i]);
            }
          }
        }
      }
    } else {
      data = await db.getQueryResult(`select keyword, avg(understanding) as understanding
from student_lecture_ordered_keywords where lecture_id=${lecture_id} group by lecture_id, keyword;`)
    }
*/
    res.json(data);
  })
);

module.exports = router;

