const express = require('express');
const router = express.Router();
const util = require('util');
const db = require('../modules/db');
const exec = util.promisify(require('child_process').exec);
const execTimeout10 = require('../src/utility');
const path = require('path');
const modules = require('../modules/frequent-modules');
const config = require('../config');
const async = require('async');
const child_process = require("child_process");
const execCmd = require("child_process").exec;
const fs = require('fs');

// return scores according to login type (educator or student)
router.get('/:id/assignment/:itemId',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const { authId, authType } = req.decoded;
    const class_id = parseInt(req.params.id, 10);
    // const assignment_name = req.params.assignment;
    const item_id = parseInt(req.params.itemId, 10);

    const item = await modules.models.lecture_item.findOne({ where: {lecture_item_id: item_id }});
    
    const sql0 = `select u.name,u.email_id from users u , user_classes uc where u.user_id = uc.user_id and uc.role = 'student' and uc.class_id = ${class_id}`;
    const students = await db.getQueryResult(sql0);

    //
    // edcuators or managers
    if (authType === 3 || authType === 1){
        await execTimeout10("sudo docker exec class"+ class_id + " /bin/bash -c \"cd /home/educator && python /home/educator/coding_asnmt_uploader.py " + item.name + " " + item_id + "\"");
    // students
    }
    else {
        res.json([]);
    }
    res.json({ success: true });
}));
router.get('/:id/item-result/:item_id',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

    const classId = parseInt(req.params.id, 10);
    const lectureItemId = parseInt(req.params.item_id, 10);
    let codings = await modules.models.coding_assignment.findAll({ where: { lecture_item_id: lectureItemId }});
    res.json(codings);
}));
router.get('/:id/results',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

    const { authId, authType } = req.decoded;
    const classId = parseInt(req.params.id, 10);
    
    const lectures = await modules.models.lecture.findAll({ where : { class_id: classId }});
    const sql0 = `select li.name as item_name, result.class_id, result.lecture_item_id,result.email_id, result.user_id, result.name as name, result.score, result.max_score,result.code_score, result.max_code_score, result.written_score, result.max_written_score from lecture_items li,(select ca.class_id, ca.lecture_item_id, ca.email_id, u.user_id, u.name, ca.score, ca.max_score, ca.code_score,
        ca.max_code_score, ca.written_score, ca.max_written_score from coding_assignments ca, users u where ca.email_id = u.email_id and ca.class_id = ${classId})result where result.lecture_item_id = li.lecture_item_id`;
    let codings = await db.getQueryResult(sql0);
    let items = [];
    for (let i = 0; i < codings.length ; i += 1) {
        items.push(parseInt(codings[i].lecture_item_id,10));
    }
    items = items.reduce(function(a,b){
        if(a.indexOf(b) < 0) a.push(b);
        return a;
    },[]);

    const lectureItems = await modules.models.lecture_item.findAll({ where: { lecture_item_id: { [modules.models.Sequelize.Op.in]: items } }});
    let lectureSaved = {};
    for ( let i = 0 ; i < lectureItems.length ; i += 1) {
        lectureSaved[`${lectureItems[i].lecture_item_id}`] = lectureItems[i].lecture_id;
    }
    let result = {};
    for (let i = 0 ; i < lectures.length ; i += 1) {
        result[`${lectures[i].lecture_id}`] = [];
    }
    for (let i = 0 ; i < codings.length ; i += 1) {
        let lectureId = lectureSaved[codings[i].lecture_item_id];
        let ary = result[`${lectureId}`];
        if (ary === undefined) { continue; }
        ary.push(codings[i]);
        result[lectureId] = ary;
    }
    res.json(result);
    
}));
router.get('/:id/assignment/results',modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const { authId, authType } = req.decoded;
    const class_id = parseInt(req.params.id, 10);

    // edcuators or managers
    if (authType === 3 || authType === 1){
        educator_sql = "SELECT *\n" +
            "    FROM coding_assignments\n" +
            "    WHERE class_id = " + class_id + "\n" +
            "    GROUP BY lecture_item_id;";
        const scores = await modules.models.sequelize.query(educator_sql,
            {type: modules.models.Sequelize.QueryTypes.SELECT});
        if ( scores.length === 0) {
            res.json([]);
        }
        res.json(scores);
    }
    // students
    else {
        throw modules.errorBuilder.default('Permission Denied', 403, true);
    }
}));

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureItemId } = req.body;

  const lectureItem = await modules.models.lecture_item.findOne({ where: {lecture_item_id: lectureItemId }});
  const newCoding = await modules.models.coding.create({ lecture_item_id: lectureItemId, creator_id: authId })

  res.json({ success: true, coding_id: newCoding.coding_id });
}));
router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
    const { authId, authType } = req.decoded;
    const codingId = parseInt(req.params.id, 10);
    const description = req.body.descroption;
    const coding = await modules.models.coding.findOne({ where: { coding_id: codingId }});

    coding.description = description;

    await coding.save();

    res.json({ success: true });
}));
router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const coidngId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const coding = await modules.models.coding.findOne({ where: { coding_id: coidngId }});

  let scoreSum = 0;
  const dataLen = data.length;
  let bulkInsert = [];
  for (let i = 0; i < dataLen; i += 1) {
    let obj = { coding_id: coding.coding_id, keyword: data[i].keyword, score_portion: data[i].score, lecture_id: coding.lecture_id };
    bulkInsert.push(obj);
    scoreSum += parseInt(data[i].score, 10);
  }
  await modules.models.coding_keyword.bulkCreate(bulkInsert);
  coding.score = scoreSum;
  await coding.save();

  res.json({ success: true, score: scoreSum });

}));

router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const codingId = parseInt(req.params.id, 10);
    const { authId, authType } = req.decoded;

    const codingKeywords = await modules.models.coding_keyword.findAll({ where: { coding_id: codingId }});

    res.json(codingKeywords);
}));

router.delete('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
    const codingId = parseInt(req.params.id, 10);
    const { authId, authType } = req.decoded;

    // 인증 생략
    await modules.models.coding_keyword.destroy({
        where: {
        coding_id: codingId,
        },
    });

    res.json({ success: true });
}));

router.post('/upload', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
	const { authId, authType } = req.decoded;
  const { classId, itemId } = req.body;
	const filePath = path.join(modules.config.appRoot, `public/materials/docker_item_${itemId}`);
  if (fs.existsSync(filePath)) {
		console.log('exists');
	} else {
		fs.mkdirSync(filePath);
	}
    const classname = `class${classId}`;
    /*
  const cmd = `sudo docker cp ${classname}:/home/educator/practice_item_${itemId}/Untitled.ipynb ${filePath}`;
	await execCmd(cmd, function (error, stdout, stderr) {
		console.log(stdout);
		console.log(stderr);
    }); */
    await execTimeout10("sudo docker cp " + classname + ":/home/educator/practice_item_" + itemId + "/Untitled.ipynb " + filePath);
	const where = {
		file_guid: `docker_practice_item${itemId}`
	};
  await modules.updateOrCreate(modules.models.file, where, {
		file_guid: `docker_practice_item${itemId}`,
		static_path: `${classname}:/home/educator/practice_item_${itemId}/Untitled.ipynb`,
		client_path: `${filePath}/Untitled.ipynb`,
		file_type: '.ipynb',
		name: 'Untitled.ipynb',
		uploader_id: authId,
		jupyter_id: itemId
  });
  
	res.json({ success: true });
}));

module.exports = router;
