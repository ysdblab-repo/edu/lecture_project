const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');

// 실시간 p2p 강의기능 사용중인지 확인
router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사
  const { room_id } = req.query;

  const res2 =  await modules.models.realtime_lecture_room.findOne({
    where: {
      room_id,
      node_number: 0,
    },
  });
  
  const isBroadCasting = res2 ? true : false;
  res.json({ isBroadCasting })
}));

module.exports = router;
