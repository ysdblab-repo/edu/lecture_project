const express = require('express');
const router = express.Router();
const fs = require('fs')
const db = require('../modules/db')
const modules = require('../modules/frequent-modules');
const PythonShell = require('python-shell');
var updateOrCreate = function (model, where, newItem, onCreate, onUpdate, onError) {
  // First try to find the record
  model.findOne({where: where}).then(function (foundItem) {
    if (!foundItem) {
      // Item not found, create a new one
      model.create(newItem)
        .then(onCreate)
        .catch(onError);
    } else {
      // Found an item, update it
      model.update(newItem, {where: where})
        .then(onUpdate)
        .catch(onError);
      ;
    }
  }).catch(onError);
}
function saveImage(filename, data){
  var myBuffer = new Buffer(data.length);
  for (var i = 0; i < data.length; i++) {
      myBuffer[i] = data[i];
  }
  fs.writeFile(filename, myBuffer, function(err) {
      if(err) {
          console.log(err);
      } else {
          console.log("The file was saved!");
      }
  });
}
/*
var multer = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/student_lecture_faces/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname) // cb 콜백함수를 통해 전송된 파일 이름 설정
  }
})
var upload = multer({ storage: storage })
*/
router.post('/upload', function(req, res){
	
	console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
	var filename = req.body.key
	var path = '/uploads/student_lecture_faces/' + filename
	var imagestream = req.body.image.replace(/ /gi,'+')
	fs.writeFile('public/uploads/student_lecture_faces/' + filename,imagestream,{encoding:'base64'}, (err) => {
    console.log(err);
});
	//saveImage('public/uploads/student_lecture_faces/' + filename, imagestream)
	if(filename.split('_').length<3)
	{
		console.log('file name error')
		return
	}
	var user_id = filename.split('_')[0]
	var lecture_id = filename.split('_')[1]
	console.log(user_id+'@@@@@@@@@@@@@@@@@@@@@@@')
	var options = {
	  mode: 'text',
	  pythonPath: '/usr/bin/python',
	  pythonOptions: ['-u'],
	  scriptPath: 'test/',
	  args: ['public/' + path],
	};

	// student_lecture_log 최근 사진 업데이트
	updateOrCreate(
		modules.models.student_lecture_log,
		{lecture_id: lecture_id,user_id: user_id},
		{lecture_id: lecture_id,user_id: user_id,latest_pic_path: path},
		function () {
		res.json({success:true})
		console.log('created');
		},
		function () {
		res.json({success:true})
		console.log('updated');
		});

	// 벡터화 및 사진 경로 저장
	PythonShell.run('vectorization.py', options, function (err, results) {
	  if (err) throw err;
		// results is an array consisting of messages collected during execution
		//console.log('results: %j', results);
		const face_detected = results.toString()=='0' ? 0:1;
		let now_flag = 0;
		var numofcollections = 20;
		var query = 'select count(*) from student_lecture_faces where flag = 1 and user_id = :uid';
		var values = {uid:user_id};
		modules.models.sequelize.query(query,{replacements:values}).spread(function(result,metadata){
			console.log(result[0]['count(*)'].toString());
			if(parseInt(result[0]['count(*)'])==numofcollections)//20개를 모아서 평균값을 계산함.
			{
				query = 'select face_vector from student_lecture_faces where user_id = :uid';
				modules.models.sequelize.query(query,{replacements:values}).spread(function(vec_results, vec_metadata){
					var para = vec_results[0]['face_vector'];
					for(var i = 1; i<result[0]['count(*)']; i++)
					{
						para += ','+vec_results[i]['face_vector'];
					}
					PythonShell.run('avg.py', {mode:'text', pythonPath:'/usr/bin/python',pythonOption:['-u'],scriptPath:'test/',args:[para]},
						function(avgerr, avgresults){
							if(avgerr) throw err;
							var avgvalues = {uid:user_id, vector:avgresults.toString()};
							query = 'insert into student_avg_faces(user_id, avg_face_vector) values(:uid, :vector )';
							modules.models.sequelize.query(query,{replacements:avgvalues}).then(aqresult => {
								console.log(aqresult);
							}
						);}
					);
				},function(err){});
				if(!face_detected)
					now_flag = 0;
				else now_flag = 1;
				modules.models.student_lecture_face.create({'user_id':user_id,'lecture_id':lecture_id,'path':path,'face_vector':results.toString(),'flag':now_flag});
			}
			else if(parseInt(result[0]['count(*)'])<numofcollections){//20장을 모으지 못했을 때
				if(!face_detected)
					now_flag = 0;
				else now_flag = 1;
				modules.models.student_lecture_face.create({'user_id':user_id,'lecture_id':lecture_id,'path':path,'face_vector':results.toString(),'flag':now_flag});
			}
			else {//20장을 모았을 때,
				console.log('colectred@@@@@@@@@@@@');
				if(!face_detected)
					modules.models.student_lecture_face.create({'user_id':user_id,'lecture_id':lecture_id,'path':path,'face_vector':results.toString(),'flag':2});
				else
				{
					query = "SELECT avg_face_vector from student_avg_faces where user_id="+user_id;
       				modules.models.sequelize.query(query).spread((avg_result, avg_metadata)=>{
					var tmpstr = avg_result[0]['avg_face_vector'].toString()+' ';
					PythonShell.run('certify.py', {mode:'text', pythonPath:'/usr/bin/python', pythonOption:['-u'],scriptPath:'test/', args:[tmpstr, results.toString()]}, function(cererr, cerresults)
					{
						if(cererr) throw cererr;
						if(cerresults.toString()==1){
							console.log("certified");	
							modules.models.student_lecture_face.create({'user_id':user_id,'lecture_id':lecture_id,'path':path,'face_vector':results.toString(),'flag':4});
						
						}
						else{
							modules.models.student_lecture_face.create({'user_id':user_id,'lecture_id':lecture_id,'path':path,'face_vector':results.toString(),'flag':3});
							console.log("not certified");
						}
						updateOrCreate(
						modules.models.student_lecture_log,
						{'lecture_id': lecture_id,'user_id': user_id},
						{'lecture_id': lecture_id, 'user_id': user_id, 'face_auth': parseInt(cerresults.toString())},
						function () {
						console.log('created');
						},
						function () {
						console.log('updated');
						});
					});
        			});	
				}		
			}
		}, function(err){
			//여기는 해당 사용자 얼굴 개수 확인하는 select문 쿼리 실패했을 때인데 이때도 추가를 해야할지 모르겠는 부분
			modules.models.student_lecture_face.create({'user_id':user_id,'lecture_id':lecture_id,'path':path,'face_vector':results.toString(),'flag':0})

		});

	db.getQueryResult(`SELECT count(*) as count_flag, flag FROM student_lecture_faces where user_id = ${user_id} and lecture_id = ${lecture_id} group by flag`)
	.then(function(res){
		let num_flag_0 = 0;
		let num_flag_1 = 0;
		let num_flag_2 = 0;
		let num_flag_3 = 0;
		let num_flag_4 = 0;
		for (var i=0; i<res.length; i++)
		{
			switch(res[i].flag)
			{
				case 0:
					num_flag_0 += res[i].count_flag;
					break;
				case 1:
					num_flag_1 += res[i].count_flag;
					break;
				case 2:
					num_flag_2 += res[i].count_flag;
					break;
				case 3:
					num_flag_3 += res[i].count_flag;
					break;
				case 4:
					num_flag_4 += res[i].count_flag;
					break;
			}
		}
		const num_total_flag = (num_flag_2 + num_flag_3 + num_flag_4)
		if(num_total_flag !=0)
		{
			const face_concentration = num_flag_4 / num_total_flag * 100
			updateOrCreate(
			modules.models.student_lecture_log,
			{'lecture_id': lecture_id,'user_id': user_id},
			{'lecture_id': lecture_id, 'user_id': user_id, 'concentration_score_face': face_concentration},
			function () {
			console.log('created');
			},
			function () {
			console.log('updated');
			});	
		}
		
	
		
	})
	});
  });


module.exports = router;
