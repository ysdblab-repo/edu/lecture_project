const express = require('express');
const router = express.Router();

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureItemId } = req.body;

  const lectureItem = await modules.models.lecture_item.findOne({ 
    where: { lecture_id: lectureId }
  }, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // const newQuestion = await lectureItem.createQuestion({
  //   type: 0,
  //   order: 0,
  //   showing_order: 0,
  //   creator_id: authId,
  // });
  const newCodingPractice = await modules.models.create({ });

  res.json({ success: true, coding_practice_id: newCodingPractice.coding_practice_id });
}));
router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const codingPracticeId = parseInt(req.params.id, 10);
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  const codingPractice = await modules.models.coding_practice.findOne({ where: { coding_practice_id: codingPracticeId }});


  const codingType = question.type === 3;
  for (let i = 0; i < keyLen; i += 1) {
    const key = keys[i];
    if (key === 'question' || key === 'isOrderingAnswer' || key === 'score' || key === 'difficulty' || key === 'timer') {
      codingPractice[modules.toSnakeCase(key)] = req.body[key];
    } else if (key === 'choice' || key === 'answer' || key === 'acceptLanguage') {
      codingPractice[modules.toSnakeCase(key)] = req.body[key].join(modules.config.separator);
    } else if (codingType && (key === 'input' || key === 'output' ||
        key === 'sampleInput' || key === 'sampleOutput' || key === 'timeLimit' || key === 'memoryLimit')) {
      codingPractice[modules.toSnakeCase(key)] = req.body[key];
    }
  }
  await codingPractice.save();

  res.json({ success: true });
}));
router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const codingPracticeId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const codingPractice = await modules.models.coding_practice.findOne({ where: { coding_practice_id: codingPracticeId }});

  let scoreSum = 0;
  const dataLen = data.length;
  let bulkInsert = [];
  for (let i = 0; i < dataLen; i += 1) {
    let obj = { coding_practice_id: codingPractice.coding_practice_id, keyword: data[i].keyword, score_portion: data[i].score }
    bulkInsert.push(obj);
    scoreSum += parseInt(data[i].score, 10);
  }
  codingPractice.score = scoreSum;
  await codingPractice.save();

  res.json({ success: true, score: scoreSum });

  // await modules.coverageCalculator.coverageAll(question.lecture_item.lecture_id, question.lecture_item.lecture.class_id);
}));

router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const questionId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const questionKeywords = (await modules.models.question.findByPk(questionId, {
    include: { model: modules.models.question_keyword },
  })).question_keywords;

  if (!questionKeywords) {
    throw modules.errorBuilder.default('Not Found -', 404, true);
  }

  res.json(questionKeywords);
}));

router.delete('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const questionId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const question = await modules.models.question.findByPk(questionId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.question_keyword.destroy({
    where: {
      question_id: questionId,
    },
  });

  res.json({ success: true });
}));


module.exports = router;