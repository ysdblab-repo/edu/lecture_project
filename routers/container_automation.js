const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
const execTimeout10 = require('../src/utility');

router.post('/new/:classId/port/:dockerPort', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;

  // edcuators or managers
  if (authType === 3 || authType === 1) {
    const class_id = parseInt(req.params.classId, 10);
    const docker_port = parseInt(req.params.dockerPort, 10);
    const class_name = "class" + class_id;
    const docker_image_name = "preswotdocker2020/practice:v18";
    
    await execTimeout10("sudo docker run -p " + docker_port + ":8000 -d --name " + class_name + " " + docker_image_name + " jupyterhub");
    await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"sed -i '5c\\        self.CLASS_ID = " + class_id + "' /root/preswot_server_info.py\""); 

    const sql0 = `select u.email_id as student_email_id from users u, user_classes uc where u.user_id = uc.user_id and uc.class_id = ${class_id}`;
    const students = await db.getQueryResult(sql0);

    for (let i = 0 ; i < students.length ; i += 1){
      const student_name = students[i].student_email_id;
      await execTimeout10("sudo docker exec " + class_name + " /bin/sh -c \"sh /srv/scripts/add_user.sh " + student_name + " " + student_name + "\"");
    }

    await execTimeout10("sudo docker exec -d " + class_name + " /bin/sh -c \"python /srv/CAPSTONE1_19_spring/module.py > /dev/null \"");

    res.json({ success: true });
  } else {
    throw modules.errorBuilder.default('Managers only.', 404, true);
  }
}));

module.exports = router;
