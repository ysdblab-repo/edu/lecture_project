const express = require('express');
const { Op } = require('sequelize');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');

// 기관 관리자가 요청 목록을 봄
router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
// router.get('/', modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사, affiliation_id 필터

  const { user_id, affiliation_id } = req.query;

  const where = {};
  if (user_id) {
    where.user_id = user_id;
  }
  if (affiliation_id) {
    where.affiliation_id = affiliation_id;
  }

  // 테이블 검색
  const result = await modules.models.user_belonging.findAll({
    include: [
      {
        attributes: ['affiliation_id', 'description', 'isOpen'],
        model: modules.models.affiliation_description,
      },
    ],
    where,
  });

  // 리턴
  res.json({ success: true, result });
}));

// 개방 공간 리스트
router.get('/openSpaceList', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // TODO: 유효성 검사, affiliation_id 필터
  const { authType, authId } = req.decoded;

  // 테이블 검색
  const result = await modules.models.affiliation_description.findAll({
    attributes: ['affiliation_id', 'description'],
    where: {
      isOpen: {
        [Op.gt]: 2,
      },
    }
  });

  // 리턴
  res.json({ success: true, result });
}));

// 기관 관리자가 강사 등록
router.post('/binding_teacher', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authType, authId } = req.decoded;
  const { userId } = req.body;

  if (authType !== 4) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const belonging = await modules.models.user_belonging.findOne({
    where: {
      user_id: authId,
    },
  });

  const class_belonging = await modules.models.class_belonging.findOne({
    where: {
      affiliation_id: belonging.dataValues.affiliation_id,
    },
  });

  const where = {
    user_id: userId,
    affiliation_id: class_belonging.dataValues.affiliation_id,
    class_belonging_id: class_belonging.dataValues.class_belonging_id,
  }
  const result = await modules.updateOrCreate(modules.models.user_belonging, where, {
    user_id: userId,
    affiliation_id: class_belonging.dataValues.affiliation_id,
    class_belonging_id: class_belonging.dataValues.class_belonging_id,
  })

  // 리턴
  res.json({ success: true, result });
}));

module.exports = router;
