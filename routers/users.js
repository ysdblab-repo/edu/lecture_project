var fs = require('fs');
var multer = require('multer');

const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const sequelize = require('sequelize');
const modules = require('../modules/frequent-modules');
const db = require('../modules/db');
const { Op } = sequelize;

router.get('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  let where = null;
  if (req.query.email_id) {
    where = { email_id: req.query.email_id };
  }
  const users = await modules.models.user.findAll({
    attributes: modules.models.user.selects.userJoined,
    where,
  });
  res.json(users);
}));

// router.post('/upload', upload.single('uploadFile'), function(req, res, next){
//   const tasks = await db2.doQuery(`SELECT * from user_profile`);
//   res.json(tasks);
//   console.log(req.file);
// });

const upload = multer({
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    }
  }),
});

router.post('/upload', upload.single('uploadFile'), modules.asyncWrapper(async(req, res, next) => {

  // console.log("upload back", req);


  const email_id = req.body.email_id;
  // console.log("Email : " + email_id);
  const filename = req.file.filename;
  // console.log("filename: " + filename);
  //
  // console.log("ID", email_id);
  // console.log("FN", filename);
  //
  const sql = `INSERT INTO user_profiles (email_id, filename, created_at, updated_at) VALUES ('${email_id}', '${filename}', NOW(), NOW())`;
  const result = await db.getQueryResult(sql);
  if (result) {
    res.json({ success: true, result });
  }
  else {
    res.json({ success: false });
  }
}));

// router.post('/upload', upload.single('uploadFile'), wrapper.asyncMiddleware(async (req, res, next) => {
//
//   console.log("file", req.file);
//   const email_id = req.body.email_id;
//   const filename = req.file.filename;
//   console.log(await db2.doQuery(`INSERT INTO user_profiles (email_id, filename, created_at, updated_at) VALUES ('${email_id}', '${filename}', NOW(), NOW())`));
//   });
router.get('/belonging', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;

  const user_belongings = await modules.models.user_belonging.findAll({
    include: [{
      model: modules.models.affiliation_description,
      required: true,
      where: {
        isOpen: 0,
      }
    }, {
      model: modules.models.class_belonging,
      include : [{
        model: modules.models.class_password,
      }]
    }],
    where: {
      user_id: authId,
    },
  });
  const response = [];
  if (user_belongings.length > 0) {
    user_belongings.forEach(async(belonging) => {
      const affiliation_id = belonging.class_belonging.affiliation_id;
      const classList = await modules.models.req_create_class.findAll({
        where: {
          affiliation_id,
          status: 3,
        },
      });
      const result = await Promise.all(classList.map(req => modules.models.class.findOne({
        where: {
          isHide: 0,
          [Op.or]: [{
            [Op.and]: {
              class_id: req.class_id,
              sign_up_start_time: {
                [Op.lt]: Date.now(),
              },
              sign_up_end_time: {
                [Op.gt]: Date.now(),
              },
            }
          }, {
            [Op.and]: {
              class_id: req.class_id,
              type: 4,
            }
          }]
        },
        include: [{
          model: modules.models.user,
          as: 'master',
        }, {
          model: modules.models.enrollment,
          where: {
            user_id: authId,
          },
          required: false,
        }],
      })));

      const notNullResult = result.filter(item => item !== null);

      // 이미 수강중인 과목은 제외
      const result2 = await Promise.all(notNullResult.map(classData => modules.models.user_class.findOne({
        where: {
          class_id: classData.class_id,
          user_id: authId,
          role: 'student',
        }
      })));

      const notNullResult2 = result2.filter(item => item !== null);

      const alreadyEnrollClassList = notNullResult2.map(userClassData => userClassData.class_id);

      let objectData = {
        affiliation_name: belonging.affiliation_description.description,
        main_belonging: belonging.main_belonging,
        affiliation_id: belonging.affiliation_id,
        classList: notNullResult.filter(classData => !alreadyEnrollClassList.includes(classData.class_id)),
      };

      response.push(objectData);
      if (response.length === user_belongings.length) {
        res.json({ response });
      }
    });
  } else {
    res.json({});
  }
}));

router.post('/belonging', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const code = req.body.code;

  const belonging = await modules.models.class_belonging.findOne({
    where: {
      belonging_pwd: code,
    },
  });

  if (belonging === null) {
    res.json({ data: 'unvalid' });
  } else {
    const user_belonging_check = await modules.models.user_belonging.findOne({
      where: {
        user_id: authId,
        affiliation_id: belonging.affiliation_id,
        class_belonging_id: belonging.class_belonging_id,
      }
    });

    if (user_belonging_check === null) {
      const user_belonging = await modules.models.user_belonging.create({
        user_id: authId,
        affiliation_id: belonging.affiliation_id,
        class_belonging_id: belonging.class_belonging_id,
      });

      res.json({ data: user_belonging });
    } else {
      res.json({ data: 'already exist' });
    }
  }
}));

router.post('/belongingExcel', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId } = req.decoded;
  const { user_id, code} = req.body;

  const belonging = await modules.models.class_belonging.findOne({
    where: {
      belonging_pwd: code,
    },
  });

  // 학생 소속 체크
  try{
    const user_belonging_check = await modules.models.user_belonging.findOne({
      where: {
        user_id,
        affiliation_id: belonging.affiliation_id,
        class_belonging_id: belonging.class_belonging_id,
      }
    });
  }catch(exception)
  {
    console.error("user_id: ", user_id);
  }
  

  // 학생 소속 등록
  try{
    if (user_belonging_check === null) {
      const user_belonging = await modules.models.user_belonging.create({
        user_id,
        affiliation_id: belonging.affiliation_id,
        class_belonging_id: belonging.class_belonging_id,
      });
  
      res.json({ data: user_belonging });
    } else {
      res.json({ data: 'already exist' });
    }
  }
  catch(exception)
  {
    console.error("user_id: ", user_id);
    res.json({ data: 'user_id not exist' });
  }
  


}));

router.get('/search', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  let where = null;
  if (req.query.email) {
    where = { email_id: req.query.email, type: 0 };//type이 학생인 계정만 불러온다.
  }

  const users = await modules.models.user.findAll({
    attributes: modules.models.user.selects.userJoined,
    where,
    limit: 10,
  });
  res.json(users);
}));
router.get('/homepage-log/:page', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const page = req.params.page;
  const { authId, authType } = req.decoded;
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const newUser = await modules.models.homepage_log.create({
    user_id : authId,
    page_name: page,
    ip: ip,
  });
  res.json({'success': true});
}));

/*
router.get('/check_log_key/:log_verify_code', modules.asyncWrapper(async (req, res, next) => {
  const log_verify_code = req.params.log_verify_code;
  const c = await modules.models.user.find({ where: { log_verify_code } });

  res.json(c);
}));
*/
router.get('/search/log_verify_code/:log_verify_code', modules.asyncWrapper(async (req, res, next) => {
  const log_verify_code = req.params.log_verify_code;
  const user = await modules.models.user.findOne({
    where: {
      log_verify_code: log_verify_code,
    },
  });
  if (!user) {
    throw modules.errorBuilder.default('login information is not correct', 403, true);
  }
  else{
	  res.json(user);
  }
}));

router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { id } = req.params;
  const { authId, authType } = req.decoded;

  let userInfo = [];
  if (parseInt(id, 10) === authId || authType === 3) {
    userInfo = await modules.models.user.findByPk(parseInt(id, 10), {
      attributes: modules.models.user.selects.userAuth,
    });
  } else {
    userInfo = await modules.models.user.findByPk(parseInt(id, 10), {
      attributes: modules.models.user.selects.userJoined,
    });
  }
  res.json({'success': true, userInfo});
}));

router.post('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => { 
const{ id }= req.params;
const id_aaa = parseInt(id, 10);
let check_auth_type;

check_id = await modules.models.user.findByPk(id_aaa,{
  where: {
    user_id: id_aaa
  },
})
const email_count = await modules.models.user.findAll({
  where : {
    email_id: check_id.email_id
  },
})
if(email_count.length == 1){
  const rand = crypto.createHmac('sha1', modules.config.jwtSecret)
      .update(`${check_id.email_id}${(new Date()).getTime()}`)
      .digest('hex');
  if(email_count[0].type == 1) check_auth_type = 0;
  if(email_count[0].type == 0) check_auth_type = 1;
  const newUser_same_account = await modules.models.user.create({ //똑같은 아이디를 만들어주는 부분
    email_id: check_id.email_id,
    password: check_id.password,
    name: check_id.name,
    type: check_auth_type, //강사로 신청하면 학생으로, 학생으로 신청하면 강사로 변경해준다.
    birth: check_id.birth,
    sex: check_id.sex,
    address: check_id.address,
    phone: check_id.phone,
    major: check_id.major,
    belong: check_id.belong,
    log_verify_code: rand,
    current_role: check_id.current_role, //학생-강사 계정 유형 변경을 위한 변수 0 : 학생 1: 강사
  });
    await newUser_same_account.createEmail_verification({
      rand,
    });
}
if(check_id.type ==0 ){ await modules.models.user.update({current_role : 1 },{where:{email_id: check_id.email_id,},});}
if(check_id.type ==1 ){ await modules.models.user.update({current_role : 0,},{where:{email_id: check_id.email_id,},});}

const user_kkk = await modules.models.user.findOne({
  where: sequelize.and(
    {email_id: check_id.email_id},
    sequelize.where(
      sequelize.col('type'), sequelize.col('current_role')
    )
  )
});

let terms = null;
if (user_kkk.terms_agreed) {
  terms = 1;
} else {
  terms = 0;
}
const permanent = false;
const token = await modules.sign(user_kkk.dataValues, permanent);
// token 값이 jwt의 값이랑 같다.
console.log(token);

const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
await modules.models.login_log.create({
  ip,
  user_id: user_kkk.user_id,
});

res.json({success: true, token});

}));

router.post('/:id/mobile_check', modules.asyncWrapper(async (req, res, next) => { 
  //위에 post(:id)와 다른 점은 token check을 실시하지 않는 다는 것이다.
  //위에는 로그인이 완료된 후에 계정 변환이 이루어지는 반면
  //mobile은 로그인 전에 이루어진다. 따라서 로그인 여부가 변경되면 반드시 해당 부분도 변경해야 한다.
  const{ id }= req.params;
  const id_aaa = parseInt(id, 10);
  let check_auth_type;
  
  check_id = await modules.models.user.findByPk(id_aaa,{
    where: {
      user_id: id_aaa
    },
  })
  const email_count = await modules.models.user.findAll({
    where : {
      email_id: check_id.email_id
    },
  })
  if(email_count.length == 1){
    const rand = crypto.createHmac('sha1', modules.config.jwtSecret)
        .update(`${check_id.email_id}${(new Date()).getTime()}`)
        .digest('hex');
    if(email_count[0].type == 1) check_auth_type = 0;
    if(email_count[0].type == 0) check_auth_type = 1;
    const newUser_same_account = await modules.models.user.create({ //똑같은 아이디를 만들어주는 부분
      email_id: check_id.email_id,
      password: check_id.password,
      name: check_id.name,
      type: check_auth_type, //강사로 신청하면 학생으로, 학생으로 신청하면 강사로 변경해준다.
      birth: check_id.birth,
      sex: check_id.sex,
      address: check_id.address,
      phone: check_id.phone,
      major: check_id.major,
      belong: check_id.belong,
      log_verify_code: rand,
      current_role: check_id.current_role, //학생-강사 계정 유형 변경을 위한 변수 0 : 학생 1: 강사
    });
      await newUser_same_account.createEmail_verification({
        rand,
      });
  }
  if(check_id.type ==0 ){ await modules.models.user.update({current_role : 1 },{where:{email_id: check_id.email_id,},});}
  if(check_id.type ==1 ){ await modules.models.user.update({current_role : 0,},{where:{email_id: check_id.email_id,},});}
  
  const user_kkk = await modules.models.user.findOne({
    where: sequelize.and(
      {email_id: check_id.email_id},
      sequelize.where(
        sequelize.col('type'), sequelize.col('current_role')
      )
    )
  });
  
  let terms = null;
  if (user_kkk.terms_agreed) {
    terms = 1;
  } else {
    terms = 0;
  }
  const permanent = false;
  const token = await modules.sign(user_kkk.dataValues, permanent);
  // token 값이 jwt의 값이랑 같다.
  console.log(token);
  
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  await modules.models.login_log.create({
    ip,
    user_id: user_kkk.user_id,
  });
  
  res.json({success: true, token});
  
  }));


router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { id } = req.params;
  const { authId, authType } = req.decoded;
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

 
  let userInfo = [];
  let finduseremail = [];

  finduseremail = await modules.models.user.findByPk(parseInt(id,10),{attributes: modules.models.user.selects.userAuth,});
  userInfo = await modules.models.user.findAll({ //계정이 2개일때 변경하는 경우 계정 2개 모두다 변경하는 것으로 수정완료
    where: {
      email_id: finduseremail.email_id
    },
  });

  // if (parseInt(id, 10) === authId) {
  //   userInfo = await modules.models.user.findByPk(parseInt(id, 10), {
  //     attributes: modules.models.user.selects.userAuth,
  //   });
  // }

  if (!userInfo) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  for(let j = 0; j<userInfo.length; j++){

   for (let i = 0; i < keyLen; i += 1) {
      const key = keys[i];
     if (key === 'birth' || key === 'sex' || key === 'address' || key === 'phone' || key === 'major' || key === 'belong') {
        userInfo[j][key] = req.body[key];
     } else if (key === 'password') {
        if (req.body[key]) {
          const newEncrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
            .update(req.body[key])
            .digest('hex');
          userInfo[j][key] = newEncrypted;
        }
      }
   }
   await userInfo[j].save();
}
  res.json(userInfo);
}));

router.post('/check', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { password } = req.body;
  const { authId, authType } = req.decoded;
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');
  const user = await modules.models.user.findByPk(authId);
  if (user.password !== encrypted) {
    throw modules.errorBuilder.default('Password Incorrect', 403, true);
  }
  res.json({ success: true });
}));

// router.post('/upload', modules.asyncWrapper(async (req, res, next) => {
//   console.log(req.file);
//
//   res.json({success: true});
// }));

router.post('/', modules.asyncWrapper(async (req, res, next) => {
  const {
    type,
    email_id,
    password,
    name,
    birth,
    sex,
    address,
    phone,
    major,
    belong,
  } = req.body;
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');
  const userCheck = await modules.models.user.findAll({
    where: {
      email_id,
    },
  });
  if (userCheck.length > 0) {
    throw modules.errorBuilder.default('ID Exists', 409, true);
  }
  const rand = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(`${email_id}${(new Date()).getTime()}`)
    .digest('hex');
  const newUser = await modules.models.user.create({
    email_id,
    password: encrypted,
    name,
    type,
    birth,
    sex,
    address,
    phone,
    major,
    belong,
    log_verify_code: rand,
    current_role: type, //학생-강사 계정 유형 변경을 위한 변수 0 : 학생 1: 강사
  });
  await newUser.createEmail_verification({
    rand,
  });
  if(req.body.type == 1){ //강사로 회원가입하면 학생으로도 계정 생성.
    const rand_new = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(`${email_id}${(new Date()).getTime()}`)
    .digest('hex');
  const newUser_new = await modules.models.user.create({
    email_id,
    password: encrypted,
    name,
    type: 0,
    birth,
    sex,
    address,
    phone,
    major,
    belong,
    log_verify_code: rand,
    current_role: 1, //학생-강사 계정 유형 변경을 위한 변수 0 : 학생 1: 강사
  });
  await newUser_new.createEmail_verification({
    rand_new,
  });
  }
  const token = await modules.sign(newUser.dataValues);
  res.json({
    success: true,
    token,
    user_id: newUser.user_id,
  });
}));

router.put('/:id/password', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { id } = req.params;
  const { password, newPassword } = req.body;
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');
  const newEncrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(newPassword)
    .digest('hex');

  const user = await modules.models.user.findByPk(parseInt(id, 10));
  if (user.password !== encrypted) {
    throw modules.errorBuilder.default('Password Incorrect', 403, true);
  }

  user.password = newEncrypted;
  await user.save();

  res.json({ success: true });
}));

router.delete('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  let { id } = req.params;
  const { authId, authType } = req.decoded;
  id = parseInt(id, 10);

  if (!(id === authId || authType === 3)) {
    throw modules.errorBuilder.default('Forbidden', 403, true);
  }

  await modules.models.user.destroy({
    where: { id },
  });

  res.json({ success: true });
}));

// router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  
//   const userId = parseInt(req.params.id, 10);
  
//   const sql = `select name from users where user_id = ${userId}`;
//   const result = await db.getQueryResult(sql);
//   res.json(result);
// }));

router.get('/check/:email/:name',  modules.asyncWrapper(async (req, res, next) => {
  
  const email = req.params.email;
  const name = req.params.name;

  const userCheck = await modules.models.user.findOne({
    where: {
      email_id: email,
      name: name,
      type: 1
    },
  });

  if (!userCheck) {
    res.json({ success: false });
  } else {
    res.json({ success: true, userCheck });
  }
}));
// 2021-05-14 알림을 가져온다.
router.get('/event/notification/:id',  modules.asyncWrapper(async (req, res, next) => {
  
  const { id } = req.params;

  const result = await modules.models.event_notification.findAll({
    limit: 10,
    where:{
      user_id: id
    },
    order: [
      ['created_at', 'DESC'],
    ],
  });

  // 시간보정
  result.forEach(x =>
  {
     x.created_at.setHours(x.created_at.getHours() + 9);
  });

  res.json(result);

}));

router.get('/:email_id/:password', modules.asyncWrapper(async (req, res, next) => {
  const email_id = req.params.email_id;
  const password = req.params.password;
  const encrypted = crypto.createHmac('sha1', modules.config.jwtSecret)
    .update(password)
    .digest('hex');
  const user = await modules.models.user.findOne({
    where: {
      email_id,
      password: encrypted,
    },
  });
  if (!user) {
    throw modules.errorBuilder.default('login information is not correct', 403, true);
  }
  else{
	  res.json(user);
  }
}));

module.exports = router;
