const db = require('../modules/db');
const express = require('express');
const modules = require('../modules/frequent-modules');
const router = express.Router();
const config = require('../config');
function offsetSort(a, b) { if(a.offset == b.offset){ return 0} return  a.offset > b.offset ? 1 : -1; }
router.get('/online/:id', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {

  const { authId } = req.decoded;
  const lectureId = req.params.id;
  const logs = await modules.models.automatic_lecture_login_history.findAll({
    where : { lecture_id : lectureId, user_id : authId },
    order: [
      ['history_id', 'DESC'],
    ],
    limit: 1,
  });
  let offsetSum = 0;
  // null 값이면 나가기
  for ( let i = 0 ; i < logs.length ; i += 1) {
    if (logs[i].offset === null) {
      break;
    }
    offsetSum += logs[i].offset;
  }
  const unlockStage = await modules.models.student_lecture_item_group_history.findAll({
    where: { lecture_id : lectureId, user_id: authId },
    order: [
      ['history_id', 'DESC'],
    ],
    limit: 1,
  });
  res.json({ student_id : authId, lecture_id : lectureId, offset: offsetSum, unlockStage: unlockStage[0] === undefined ? 1 : unlockStage[0].unlock_stage });
}));
router.post('/online/unlockStage', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {
  const { authId } = req.decoded;
  const { lectureId , unlockStage } = req.body;
  let result = await modules.models.student_lecture_item_group_history.create({
    lecture_id: lectureId,
    user_id: authId,
    unlock_stage: unlockStage,
  });
  if ( result ) {
    res.json({ success: true });
  } else {
    throw modules.errorBuilder.default(``, 403, true);
  }
}));
router.post('/online/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {
  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const { lecture_item_group_id , offset } = req.body;
  let result = await modules.models.automatic_lecture_login_history.create({
    user_id: authId,
    lecture_id: lectureId,
    lecture_type: 2, // 무인[개인]
    lecture_item_group_id: lecture_item_group_id,
    offset,
  });
  if ( result ) {
    res.json({success: true});
  } else{
    throw modules.errorBuilder.default(``, 403, true);
  }
}));

router.delete('/online/:id', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {

  const { authId } = req.decoded;
  const lectureId = req.params.id;
  const logs = await modules.models.automatic_lecture_login_history.destroy({
    where : { lecture_id: lectureId, user_id: authId}
  })
  res.json({ result : logs });
}));

router.post('/offline/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req,res,next) => {
  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const { lecture_item_group_id, offset } = req.body;
  let result = await modules.models.automatic_lecture_login_history.create({
    user_id: authId,
    lecture_id: lectureId,
    lecture_type: 1, // 무인[단체]
    lecture_item_group_id: lecture_item_group_id,
    offset,
  });
  if ( result ) {
    res.json({success: true});
  } else {
    throw modules.errorBuilder.default(``, 403, true);
  }
}));
router.get('/online/:id/students', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {
  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  let result = await modules.models.lecture_student_login_log.findAll({
    where: { lecture_id: lectureId }
  });
  res.json(result);
}));
// item id
router.get('/online/:id/time/:item_id', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {
  
  const { authId } = req.decoded;
  const lectureId = parseInt(req.params.id, 10);
  const lectureItemId = parseInt(req.params.item_id, 10);
  let groups = await modules.models.lecture_item_group.findAll({ where : { lecture_id : lectureId }});
  let group_idx = -1;
  for ( let i = 0 ; i < groups.length ; i += 1 ) {
    
    let list_ids = groups[i].group_list.split(config.separator);
    console.log(list_ids);
    let lists = await modules.models.lecture_item_list.findAll({
      where : { lecture_item_list_id : { [modules.models.Sequelize.Op.in]: list_ids }}
    });
    for ( let j = 0 ; j < lists.length ; j += 1 ) {
      let item_ids = lists[j].linked_list.split(config.separator);
      if ( item_ids.includes(`${lectureItemId}`)){
        group_idx = i;
        break;
      }
    }
  }
  if ( group_idx === -1 ) {
    res.json( { success: false });
  } else { 
    res.json({ success: true, start: groups[group_idx].start, end: groups[group_idx].end });
  }
}));
// router.get('/online/join/:id', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {

//   // 학생이 들어간 시간이 기준으로
//   const { authId } = req.decoded;
//   const lecture_id = req.params.id;
//   const log = await modules.models.automatic_lecture_login_history.findOne({
//     where: { user_id: authId, lecture_id: lecture_id, lecture_type: 1 },
//   })
//   const lecItems = await modules.models.lecture_item.findAll({
//     where: {
//       lecture_id: lecture_id,
//     },
//     include: [{
//       model: modules.models.lecture,
//     }, {
//       model: modules.models.question,
//       include: [
//         { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
//         { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
//         { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
//         { model: modules.models.student_answer_log,
//           where: { student_id: authId },
//           required: false,
//           include: [
//             { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
//             { model: modules.models.oj_solution, required: false },
//             { model: modules.models.oj_compileinfo, required: false },
//             { model: modules.models.oj_runtimeinfo, required: false },
//             { model: modules.models.feedback, required: false },
//           ],
//         },
//       ],
//     }, {
//       model: modules.models.lecture_code_practice,
//     }, {
//       model: modules.models.discussion,
//       include: { model: modules.models.user },
//     }, {
//       model: modules.models.discussion_info,
//     }, {
//       model: modules.models.survey,
//       include: [
//         { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
//         { model: modules.models.student_survey, where: { student_id: authId }, required: false },
//       ],
//     }, {
//       model: modules.models.note,
//       include: [
//         { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
//       ],
//     }],
//   });

//   lecItems.sort(offsetSort);

//   let items = [];
//   let rawItems = [];


//   for( let i = 0; i < lecItems.length; i += 1 ) {
//     items.push(lecItems[i].toJSON());
//   }
  
//   for ( let j = 0; j < items.length; j += 1 ) {
//     if ( items[j].questions == undefined ) continue;
    
//     for (let i = 0; i < items[j].questions.length; i += 1) {
      
//       if (items[j].questions[i].choice) {
//         items[j].questions[i].choice = items[j].questions[i].choice.split(modules.config.separator);
//       }
//       if (items[j].questions[i].answer) {
//         items[j].questions[i].answer = items[j].questions[i].answer.split(modules.config.separator);
//       }
//       if (items[j].questions[i].accept_language) {
//         items[j].questions[i].accept_language = items[j].questions[i].accept_language.split(modules.config.separator);
//       }
//       if (items[j].questions[i].type === 3) {
//         const problem = await modules.models.oj_problem.findOne({
//           where: { problem_id: items[j].questions[i].question_id }
//         });
        
//         if (problem) {
//           items[j].questions[i].input_description = problem.input;
//           items[j].questions[i].output_description = problem.output;
//           items[j].questions[i].sample_input = problem.sample_input;
//           items[j].questions[i].sample_output = problem.sample_output;
//           items[j].questions[i].time_limit = problem.time_limit;
//           items[j].questions[i].memory_limit = problem.memory_limit;
//         } 
//       }
//       if ( items[j].questions[i].student_answer_log == undefined ) continue;
//       for (let j = 0; j < items[j].questions[i].student_answer_logs.length; j += 1) {
//         if (items[j].questions[i].student_answer_logs[j].answer) {
//           items[j].questions[i].student_answer_logs[j].answer =
//           items[j].questions[i].student_answer_logs[j].answer.split(modules.config.separator);
//         } else {
//           items[j].questions[i].student_answer_logs[j].answer = [];
//         }
//       }
//       if (!items[j].questions[i].sqlite_file_guid) {
//         items[j].questions[i].sql_lite_file = [];
//       } else {
//         items[j].questions[i].sql_lite_file = await modules.models.file.findAll({
//           where: { file_guid: items[j].questions[i].sqlite_file_guid }
//         });
//       }
//     }
//     for (let i = 0; i < items[j].surveys.length; i += 1) {
//       if (items[j].surveys[i].choice) {
//         items[j].surveys[i].choice = items[j].surveys[i].choice.split(modules.config.separator);
//       }
//       if (items[j].surveys[i].student_surveys[0]) {
//         items[j].surveys[i].student_surveys[0].answer = items[j].surveys[i].student_surveys[0].answer.split(modules.config.separator);
//       }    
//     }
//   }
//   // copy objects
//   rawItems = modules.cloneObject(items);
//   // 접속 로그가 있을 경우 해당 아이템과 offset 을 불러와 그 시간을 차감해서 보내준다.
//   if ( log ) {
//     let filteredItems = []
//     let idx = -1;
//     // find lecture_item_id 's offset
//     for ( let i = 0; i < items.length; i += 1 ) { 
//       // console.log(`${i} : ${items[i].lecture_item_id} - ${log.lecture_id}`);

//       if (items[i].lecture_item_id == log.lecture_item_id ) {
//         idx = i;
//         break;
//       } 
//     }
//     if ( idx != -1 ) {
//       for ( let i = 0; i < items.length; i += 1 ) { 
  
//         if ( items[i].offset >= items[idx].offset ) 
//           filteredItems.push(items[i]); 
//       }
      
//       let off = items[idx].offset + log.offset;
      
//       for ( let i = 0; i < filteredItems.length; i += 1 ) { 
//         filteredItems[i].offset -= off;
//         if ( filteredItems[i].offset < 0 ) filteredItems[i].offset = 0;
//       }
  
//     } else {
      
//       for ( let i = 0; i < items.length; i += 1 ) {
//         filteredItems.push(items[i]);
//       }
//     }
 
//     res.json({ items: filteredItems , offset: log.offset, rawItems: rawItems });
  
//   } else {
  
//     res.json({ items: items , offset: -1, rawItems: rawItems });
  
//   }

// }));
// router.post('/online/leave', modules.auth.tokenCheck, modules.asyncWrapper( async( req, res, next ) => {

//   const { authId , authType } = req.decoded;
//   const { lecture_id, lecture_item_id, offset } = req.body;
  
//   // login log에서 접속 로그가 있다면 갱신, 그렇지 않다면 삽입
//   const log = await modules.models.automatic_lecture_login_history.findOne({ where: {user_id: authId, lecture_id, lecture_type: 1 }});
//   if ( log ) {
//     // console.log('update');
//     await modules.models.automatic_lecture_login_history.update({ lecture_item_id: lecture_item_id , offset: offset }, { where: { lecture_id, user_id: authId }, });
//   } else {
//     // console.log('create');
//     await modules.models.automatic_lecture_login_history.create({
//       user_id: authId,
//       lecture_id,
//       lecture_type: 1,
//       lecture_item_id,
//       offset,
//     });
//   }

//   res.json({ success: true });
// }));
// router.get('/offline/join/:id', modules.auth.tokenCheck, modules.asyncWrapper(async ( req, res, next) => {

//   // 정해진 시간이 있음
//   const { authId } = req.decoded;
//   const lecture_id =  req.params.id;
  
//   const lec = await modules.models.lecture.findOne({ where: { lecture_id }});
//   const lecItems = await modules.models.lecture_item.findAll({
//     where: {
//       lecture_id: lecture_id,
//     },
//     include: [{
//       model: modules.models.lecture,
//     }, {
//       model: modules.models.question,
//       include: [
//         { model: modules.models.file, as: 'question_material', attributes: modules.models.file.selects.fileClient },
//         { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
//         { model: modules.models.problem_testcase, attributes: modules.models.problem_testcase.selects.problemTestcaseSimple },
//         { model: modules.models.student_answer_log,
//           where: { student_id: authId },
//           required: false,
//           include: [
//             { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
//             { model: modules.models.oj_solution, required: false },
//             { model: modules.models.oj_compileinfo, required: false },
//             { model: modules.models.oj_runtimeinfo, required: false },
//             { model: modules.models.feedback, required: false },
//           ],
//         },
//       ],
//     }, {
//       model: modules.models.lecture_code_practice,
//     }, {
//       model: modules.models.discussion,
//       include: { model: modules.models.user },
//     }, {
//       model: modules.models.discussion_info,
//     }, {
//       model: modules.models.survey,
//       include: [
//         { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
//         { model: modules.models.student_survey, where: { student_id: authId }, required: false },
//       ],
//     }, {
//       model: modules.models.note,
//       include: [
//         { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
//       ],
//     }],
//   });

//   lecItems.sort(offsetSort);

//   let items = [];
//   let rawItems = [];

//   for( let i = 0; i < lecItems.length; i += 1 ) {
//     items.push(lecItems[i].toJSON());
//   }
  
//   for ( let j = 0; j < items.length; j += 1 ) {
//     if ( items[j].questions == undefined ) continue;
//     for (let i = 0; i < items[j].questions.length; i += 1) {
      
//       if (items[j].questions[i].choice) {
//         items[j].questions[i].choice = items[j].questions[i].choice.split(modules.config.separator);
//       }
//       if (items[j].questions[i].answer) {
//         items[j].questions[i].answer = items[j].questions[i].answer.split(modules.config.separator);
//       }
//       if (items[j].questions[i].accept_language) {
//         items[j].questions[i].accept_language = items[j].questions[i].accept_language.split(modules.config.separator);
//       }
//       if (items[j].questions[i].type === 3) {
//         const problem = await modules.models.oj_problem.findOne({
//           where: { problem_id: items[j].questions[i].question_id }
//         });
        
//         if (problem) {
//           items[j].questions[i].input_description = problem.input;
//           items[j].questions[i].output_description = problem.output;
//           items[j].questions[i].sample_input = problem.sample_input;
//           items[j].questions[i].sample_output = problem.sample_output;
//           items[j].questions[i].time_limit = problem.time_limit;
//           items[j].questions[i].memory_limit = problem.memory_limit;
//         } 
//       }
//       if ( items[j].questions[i].student_answer_log == undefined ) continue;
//       for (let j = 0; j < items[j].questions[i].student_answer_logs.length; j += 1) {
//         if (items[j].questions[i].student_answer_logs[j].answer) {
//           items[j].questions[i].student_answer_logs[j].answer =
//           items[j].questions[i].student_answer_logs[j].answer.split(modules.config.separator);
//         } else {
//           items[j].questions[i].student_answer_logs[j].answer = [];
//         }
//       }
//       if (!items[j].questions[i].sqlite_file_guid) {
//         items[j].questions[i].sql_lite_file = [];
//       } else {
//         items[j].questions[i].sql_lite_file = await modules.models.file.findAll({
//           where: { file_guid: items[j].questions[i].sqlite_file_guid }
//         });
//       }
//     }
//     for (let i = 0; i < items[j].surveys.length; i += 1) {
//       if (items[j].surveys[i].choice) {
//         items[j].surveys[i].choice = items[j].surveys[i].choice.split(modules.config.separator);
//       }
//       if (items[j].surveys[i].student_surveys[0]) {
//         items[j].surveys[i].student_surveys[0].answer = items[j].surveys[i].student_surveys[0].answer.split(modules.config.separator);
//       }    
//     }
//   }

//   let filteredItems = [];
//   rawItems = modules.cloneObject(items);

//   const curtime = Math.floor(new Date().getTime()/1000);
//   const starttime = lec.start_time/1000;
//   const endtime = lec.end_time/1000;
//   // console.log(starttime);
//   // console.log(endtime);
//   // console.log(curtime);

//   items.sort(offsetSort);

//   if( curtime < starttime ) {
//     res.json({ error: "시작전" });
//   }
//    else if ( curtime > endtime ) {
//      res.json({ error: "수업종료"});
//   } else {
//     //  lecture의 활성화 시작시간으로부터 현재 시간의 흐름만큼 차감
//     for ( let i = 1; i < items.length; i += 1 ) {
//       let t = starttime + items[i].offset ;
//       //console.log(`${i} curtime - ${curtime/1000} ${t/1000} , starttime - ${starttime/1000}`);
//       if ( t > curtime )
//         filteredItems.push(items[i-1]);
//     }
//     filteredItems.push(items[items.length-1]);
//   }
//   for ( let i = 0; i < filteredItems.length; i += 1 ) {
//    // filteredItems[i].offset = Math.floor(( starttime + items[i].offset * 1000 - curtime ) / 1000);
//     // console.log(`${i} diff - ${curtime-starttime} , item offset - ${filteredItems[i].offset}`);

//     filteredItems[i].offset = filteredItems[i].offset - (curtime-starttime);
//     if ( filteredItems[i].offset < 0 ) filteredItems[i].offset = 0;
//   }
  
//   res.json({ items: filteredItems , rawItems : rawItems });

// }))
// router.get('/offline/leave/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {

//   const { authId } = req.decoded;
//   const lecture_id = req.params.id;
//   const curTime = new Date().getTime();
//   const log = await modules.models.automatic_lecture_login_history.findOne({ where: { user_id: authId, lecture_id: lecture_id, lecture_type: 0 } });
  
//   if (log) {
//     await modules.models.automatic_lecture_login_history.update({ updated_at : curTime }, { where: { user_id: authId, lecture_id, lecture_type: 0} });
//   } else { 
//     await modules.models.automatic_lecture_login_history.create({ user_id: authId, lecture_id, lecture_type: 0 })
//   }
//   res.json({success: true});

// }));

module.exports = router;