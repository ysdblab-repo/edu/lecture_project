const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const db = require('../modules/db')
const modules = require('../modules/frequent-modules');
const analysis = require('../modules/analysis')

router.get('/', modules.asyncWrapper(async (req, res, next) => {
  res.render('mobile/login_mobile.html')
}));

router.get('/test', modules.asyncWrapper(async (req, res, next) => {
	let avg_hb = 0;
    let std_hb = 0;
	const lecture_id = 1
	let student_part = [];
	let ck = false;
	
   db.getQueryResult("select std(a.num_hb) as std_hb, avg(a.num_hb) as avg_hb from (select count(*) as num_hb, user_id from realtime_lecture_heartbeats where lecture_id = " +lecture_id+ " group by user_id) as a")
   .then(function(res)
   {
		avg_hb = res[0].avg_hb;
		std_hb = res[0].std_hb;
		if(avg_hb==null || std_hb==null)
			return;
		db.getQueryResult("select count(*) as num_hb, user_id from realtime_lecture_heartbeats where lecture_id = "+lecture_id+" group by user_id")
		.then(function(res)
		{
			for(var i=0; i<res.length; i++)
			{
				res[i].z_hb = Math.abs(res[i].num_hb - avg_hb) / std_hb;
			}
			student_part = res;
		})
   })
   db.getQueryResult("select std(a.num_part) as std_part, avg(a.num_part) as avg_part  from (select count(*) as num_part, lil.user_id from lecture_item_logs lil inner join lecture_items li on li.lecture_item_id = lil.lecture_item_id and li.lecture_id = " + lecture_id + " group by lil.user_id) as a")
   .then(function(res)
   {
		avg_part = res[0].avg_part;
		std_part = res[0].std_part;
		if(avg_part==null || std_part==null)
			return;
		db.getQueryResult("select count(*) as num_part, lil.user_id from lecture_item_logs lil inner join lecture_items li on li.lecture_item_id = lil.lecture_item_id and li.lecture_id = "+lecture_id+" group by lil.user_id")
		.then(function(res)
		{
			
			for(var i=0; i<res.length; i++)
			{
				res[i].z_hb = Math.abs(res[i].num_part - avg_part) / avg_part;
				ck = false;
				for(var j=0; j<student_part.length; j++)
				{
					if(student_part[j].user_id == res[i].user_id)
					{
						student_part[j].z_hb = student_part[j].z_hb + res[i].z_hb;
						ck = true;
						break;
					}
				}
				if(!ck)
					student_part.push(res[i]);
			}
			console.log(student_part)
			console.log(analysis.addArrayZscore(student_part,'z_hb'))
			
		})
   })
}));
router.get('/student_lecture_score/:lecture_id', modules.asyncWrapper(async (req, res, next) => {
	res.render('student_lecture_score.ejs', {
    lecture_id: req.params.lecture_id
  })
}));

module.exports = router;
