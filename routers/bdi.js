const express = require('express');
const router = express.Router();
const sequelize = require('sequelize');
const db = require('../modules/db');
const modules = require('../modules/frequent-modules');
var multer  = require('multer');

router.post('/', modules.asyncWrapper(async (req, res, next) => {
  const { email, name, type, phone } = req.body;

  const bdi_apply = await modules.models.bdi_apply.create({ email, name, type, phone });

  res.json({ success: true });
}));

module.exports = router;
