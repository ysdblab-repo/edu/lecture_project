const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const _rimraf = require('rimraf');
const util = require('util');
const rimraf = util.promisify(_rimraf);
const sequelize = require('sequelize');
const db = require('../modules/db');
const exec = util.promisify(require('child_process').exec);

const modules = require('../modules/frequent-modules');

router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const { lectureItemId } = req.body;

  const lectureItem = await modules.models.lecture_item.findByPk(lectureItemId, {
    include: {
      model: modules.models.lecture,
    },
  });
  if (!lectureItem) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, lectureItem.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const newQuestion = await lectureItem.createQuestion({
    type: 0,
    order: 0,
    showing_order: 0,
    creator_id: authId,
  });

  res.json({ success: true, question_id: newQuestion.question_id });
}));

router.post('/:id/testcases', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const questionId = parseInt(req.params.id, 10);
  const { testcase } = req.body;
  const question = await modules.models.question.findByPk(questionId, {
    include: [{
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      }
    }],
    // }, {
    //   model: modules.models.problem_testcase,
    // }],
    // order: [
    //   [{ model: modules.models.problem_testcase }, 'num', 'ASC'],
    // ],
  });
  
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }

  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // testcase 삭제
  await modules.models.problem_testcase.destroy({
    where: {
      question_id: questionId,
    }
  });

  // testcase 생성
  await Promise.all(testcase.map((eachCase, index) => modules.models.problem_testcase.create({
    question_id: questionId,
    num: index + 1,
    input: eachCase.input,
    output: eachCase.output,
    score_ratio: eachCase.score_ratio,
    timeout: eachCase.timeout,
    memory: eachCase.memory,
  })));

  // const inLen = testcase.length;
  // for (let i = 0; i < inLen; i += 1) {
  //   const { input, output } = testcase[i];
  //   const input_path = path.join(modules.config.ojTestCasesPath, questionId.toString(), `test${newNum}.in`);
  //   const output_path = path.join(modules.config.ojTestCasesPath, questionId.toString(), `test${newNum}.out`);

  //   await modules.models.problem_testcase.create({
  //     question_id: questionId,
  //     num: newNum,
  //     input,
  //     output,
  //     input_path,
  //     output_path,
  //   });

  //   await modules.writeFilePromise(input_path, `${input}\r\n`);
  //   await modules.writeFilePromise(output_path, output);

  //   newNum += 1;
  // }

  res.json({ success: true });
}));

router.post('/testcase/output', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authType } = req.decoded;

  if (!(authType === 1 || authType === 3 || authType === 4)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  const { language, code, input } = req.body;

  const { success, output, unlink } = await modules.runCode(language, code, input);

  res.json({
    success,
    output,
    unlink
  });

}));

router.put('/:id/testcases/:num', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const questionId = parseInt(req.params.id, 10);
  const num = parseInt(req.params.num, 10);
  const { input, output } = req.body;

  const question = await modules.models.question.findByPk(questionId, {
    include: [{
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    }, {
      model: modules.models.problem_testcase,
    }],
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  const testcase = await modules.models.problem_testcase.findOne({
    where: { question_id: questionId, num },
  });
  if (!testcase) {
    throw modules.errorBuilder.default('Testcase Not Found', 404, true);
  }

  if (input) {
    testcase.input = input;
    if (fs.existsSync(testcase.input_path)) {
      fs.unlinkSync(testcase.input_path);
    }
    await modules.writeFilePromise(testcase.input_path, `${input}\r\n`);
  }
  if (output) {
    testcase.output = output;
    if (fs.existsSync(testcase.output_path)) {
      fs.unlinkSync(testcase.output_path);
    }
    await modules.writeFilePromise(testcase.output_path, output);
  }
  testcase.save();

  res.json({ success: true });
}));

router.delete('/:id/testcases/:num', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const questionId = parseInt(req.params.id, 10);
  const num = parseInt(req.params.num, 10);

  const question = await modules.models.question.findByPk(questionId, {
    include: [{
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    }, {
      model: modules.models.problem_testcase,
    }],
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.delete.testcases(questionId, num);
  res.json({ success: true });
}));

router.put('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const { authId, authType } = req.decoded;
  const questionId = parseInt(req.params.id, 10);
  const keys = Object.keys(req.body);
  const keyLen = keys.length;

  const question = await modules.models.question.findByPk(questionId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  let problem = 'nonType';
  if (question.type === 3) {
    problem = await modules.models.oj_problem.findOne({
      where: { problem_id: questionId },
    });
  }

  const codingType = question.type === 3;
  let problemCond = {};
  for (let i = 0; i < keyLen; i += 1) {
    const key = keys[i];
    if (key === 'question' || key === 'isOrderingAnswer' || key === 'score' || key === 'difficulty'
      || key === 'timer' || key === 'multiChoiceMediaType' || key === 'answerMediaType'
      || key === 'questionMediaType' || key === 'questionMedia' || key === 'accept_language' ) {
      question[modules.toSnakeCase(key)] = req.body[key];
    } else if (key === 'choice' || key === 'answer' || key === 'choice2' || key === 'multiChoiceMedia') {
      question[modules.toSnakeCase(key)] = req.body[key].join(modules.config.separator);
    } else if (codingType && (key === 'input' || key === 'output' || key === 'sample_code'
      || key === 'sampleInput' || key === 'sampleOutput' || key === 'timeLimit' || key === 'memoryLimit')) {
      if (problem != 'nonType') {
        if (problem) {
          problem[modules.toSnakeCase(key)] = req.body[key];
        } else {
          problemCond[modules.toSnakeCase(key)] = req.body[key];
        }
      }
    }
  }
  await question.save();
  if (problem != 'nonType') {
    if (problem) {
      await problem.save();
    } else {
      await modules.models.oj_problem.create({
        ...problemCond,
        problem_id: questionId
      });
    }
  }

  res.json({ success: true });
}));

router.put('/:id/type', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const questionId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { type } = req.body;

  const question = await modules.models.question.findByPk(questionId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  if (type === question.type) {
    res.json({ success: true });
    return;
  }
  if (type === 3) {
    await modules.models.problem_testcase.destroy({
      where: { question_id: questionId },
    });

    // const testcaseDir = path.join(modules.config.ojTestCasesPath, questionId.toString());
    // if (fs.existsSync(testcaseDir)) {
      // await rimraf(testcaseDir);
    // }
    // fs.mkdirSync(testcaseDir);

  //   await modules.models.oj_problem.create({
  //     problem_id: questionId,
  //     title: 'none',
  //     description: question.question,
  //     input: '',
  //     output: '',
  //     sample_input: '',
  //     sample_output: '',
  //     spj: '0',
  //     hint: 'none',
  //     source: 'admin',
  //     time_limit: 5,
  //     memory_limit: 128,
  //     defunct: 'N',
  //   }, { ignoreDuplicates: true });
  }
  question.type = type;
  await question.save();
  res.json({ success: true });
}));

router.post('/:id/file', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const file = await modules.upload.fileAdd(req, 'question_id', parseInt(req.params.id, 10));
  res.json({ success: true, file });
}));
router.post('/:id/material-file', modules.auth.tokenCheck, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const file = await modules.upload.fileAdd(req, 'question_id_material', parseInt(req.params.id, 10));
  res.json({ success: true, file });
}));
router.post('/:id/multichoice-file', modules.auth.tokenCheck, modules.upload.upload,
  modules.asyncWrapper(async (req, res, next) => {
    const file = await modules.upload.fileAdd(req, 'question_id_multichoice', parseInt(req.params.id, 10));
    res.json({ success: true, file, file2: req.file });
  }));
router.post('/:id/question-voice-file', modules.auth.tokenCheck, modules.upload.upload,
  modules.asyncWrapper(async (req, res, next) => {
    const file = await modules.upload.fileAdd(req, 'question_id_voice', parseInt(req.params.id, 10));
    res.json({ success: true, file, file2: req.file });
  }));

router.post('/:id/sql-lite-file', modules.auth.questions, modules.upload.upload, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  await modules.models.question.update({
    sqlite_file_guid: req.fileInfo.fileGuid,
  }, {
    where: { question_id: id },
  });

  const sqlLiteFile = await modules.models.file.create({
    file_guid: req.fileInfo.fileGuid,
    static_path: req.fileInfo.staticPath,
    client_path: req.fileInfo.clientPath,
    name: path.basename(req.fileInfo.clientPath),
    file_type: req.fileInfo.extern,
    uploader_id: req.decoded.authId,
  });

  res.json({ success: true, sqlLiteFile });
}));

const keywordUpsert = async (values, condition) => modules.models
  .question_keyword.findOne({ where: condition })
  .then((obj) => {
    if (obj) {
      return obj.update(values);
    } else {
      return modules.models.question_keyword.create(values);
    }
  });

router.post('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const questionId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { data } = req.body;

  const question = await modules.models.question.findByPk(questionId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
        include: {
          model: modules.models.class,
        },
      },
    },
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  let scoreSum = 0;
  const dataLen = data.length;
  for (let i = 0; i < dataLen; i += 1) {
    scoreSum += parseInt(data[i].score, 10);
  }
  const promises = [];
  for (let i = 0; i < dataLen; i += 1) {
    promises.push(keywordUpsert(
      { question_id: question.question_id, lecture_id: question.lecture_item.lecture_id, lecture_item_id: question.lecture_item_id, keyword: data[i].keyword, score_portion: data[i].score },
      { question_id: question.question_id, keyword: data[i].keyword },
    ));
  }
  Promise.all(promises);
  question.score = scoreSum;
  await question.save();

  res.json({ success: true, score: scoreSum });

  await modules.coverageCalculator.coverageAll(question.lecture_item.lecture_id, question.lecture_item.lecture.class_id);
}));

router.get('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const questionId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const questionKeywords = (await modules.models.question.findByPk(questionId, {
    include: { model: modules.models.question_keyword },
  })).question_keywords;

  if (!questionKeywords) {
    throw modules.errorBuilder.default('Not Found -', 404, true);
  }

  res.json(questionKeywords);
}));

router.delete('/:id/keywords', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const questionId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const question = await modules.models.question.findByPk(questionId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  await modules.models.question_keyword.destroy({
    where: {
      question_id: questionId,
    },
  });

  res.json({ success: true });
}));

router.get('/:id/result', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const questionId = parseInt(req.params.id, 10);

  const sql1 = `SELECT DISTINCT answer FROM questions WHERE question_id = ${questionId}`;
  const result1 = await db.getQueryResult(sql1); // 정답
  const sql3 = `SELECT answer, COUNT(*) AS CNT FROM student_answer_logs WHERE question_id = ${questionId} GROUP BY answer`;
  let result03 = await db.getQueryResult(sql3);
  let numStudentsTotal = 0;
  let numStudentsAnswer = 0;
  let numStudentsPartialAnswer = 0;
  const studentsAnswers = [];
  let result3 = [];
  for (let i = 0 ; i < result03.length ; i += 1) {
    let obj = { 'answer2': result03[i].answer.split(modules.config.separator), 'answer' : result03[i].answer, 'CNT': result03[i].CNT }
    result3.push(obj);
  }
  const answer = result1[0].answer.split(modules.config.separator);
  // console.log(answer);
  if (answer.length === 1) {

    for (let i = 0; i < result3.length; i+= 1) {
      if (answer[0] === result3[i].answer2) {
        numStudentsAnswer = result3[i].CNT;
      }
      else if (result3[i].answer2.includes(answer[0])) {
        numStudentsPartialAnswer += result3[i].CNT;
      }
      numStudentsTotal += result3[i].CNT;
      const tmp = { student_answer: result3[i].answer, num_students: result3[i].CNT };
      studentsAnswers.push(tmp);
    }
  }
  else {
    for (let i = 0; i < result3.length; i += 1) {
      // if (result1[0].answer === result3[i].answer) {
      //   numStudentsAnswer = result3[i].CNT;
      // }
      // else {
      //   for (let j = 0; j < answer.length; j += 1) {
      //     if (result3[i].answer.includes(answer[j])) {
      //       numStudentsPartialAnswer += result3[i].CNT;
      //     }
      //   }
      // }
      let len = answer.filter(value => result3[i].answer2.includes(value));
      if (len.length === answer.length) {
        numStudentsAnswer += result3[i].CNT;
      } else if (len.length !== 0) {
        numStudentsPartialAnswer += result3[i].CNT;
      }
      numStudentsTotal += result3[i].CNT;
      const tmp = { student_answer: result3[i].answer, num_students: result3[i].CNT };
      studentsAnswers.push(tmp);
    }
  }
  res.json({ num_students_total: numStudentsTotal, num_students_answer: numStudentsAnswer, num_students_partial_answer: numStudentsPartialAnswer, student_answers: studentsAnswers });
 //   res.json({ num_students_total: totalNum.dataValues.num_students_total, num_students_answer: answerNum.dataValues.num_students_answer, student_answers: allAnswers });
}));

router.get('/:id/:classId/result', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const questionId = parseInt(req.params.id, 10);

  const classId = parseInt(req.params.classId);
  const sql0 = `select * from users where user_id in (select hl.user_id from homepage_logs hl,user_classes uc
    where hl.user_id = uc.user_id and date(hl.created_at) = date(curdate()) and role = 'student' and uc.class_id = ${classId} group by hl.user_id)`;

  const result0 = await db.getQueryResult(sql0);
  let loginStudents = [];
  for(let i = 0; i < result0.length; i+= 1) {
    let obj = {}
    obj['user_id'] = result0[i].user_id;
    obj['email_id'] = result0[i].email_id;
    obj['name'] = result0[i].name;
    loginStudents.push(obj);
  }

  const sql1 = `SELECT DISTINCT answer FROM questions WHERE question_id = ${questionId}`;
  const result1 = await db.getQueryResult(sql1); // 정답
  const sql3 = `SELECT answer, COUNT(*) AS CNT FROM student_answer_logs WHERE question_id = ${questionId} GROUP BY answer`;
  let result03 = await db.getQueryResult(sql3);
  let numStudentsTotal = 0;
  let numStudentsAnswer = 0;
  let numStudentsPartialAnswer = 0;
  const studentsAnswers = [];
  let result3 = [];
  // 선택 번호, 선택한 수 저장 ex) answer2: ['1', '3'], answer: '1<>3', CNT: 10
  for (let i = 0 ; i < result03.length ; i += 1) {
    let obj = { 'answer2': result03[i].answer.split(modules.config.separator), 'answer' : result03[i].answer, 'CNT': result03[i].CNT }
    result3.push(obj);
  }
  const answer = result1[0].answer.split(modules.config.separator); // 정답
  // console.log(answer);
  // console.log(result3)
  if (answer.length === 1) {
    for (let i = 0; i < result3.length; i+= 1) { // 학생이 선택한 답의 갯수
      if (parseInt(answer[0], 10) === parseInt(result3[i].answer2, 10)) { // 정답과 학생이 선택한 답과 비교
        numStudentsAnswer = result3[i].CNT;
      }
      else if (result3[i].answer2.includes(answer[0])) {
        numStudentsPartialAnswer += result3[i].CNT;
      }
      numStudentsTotal += result3[i].CNT;
      const tmp = { student_answer: result3[i].answer, num_students: result3[i].CNT };
      studentsAnswers.push(tmp);
    }
  }
  else {
    for (let i = 0; i < result3.length; i += 1) {
      let count = 0;
      for (let q = 0; q < answer.length; q += 1) {
        for (let w = 0; w < result3[i].answer2.length; w += 1) {
          if (parseInt(answer[q], 10) === parseInt(result3[i].answer2[w], 10)) {
            count += 1;
          }
        }
      }
      if (count === answer.length && count === result3[i].answer2.length) {
        numStudentsAnswer += result3[i].CNT;
      } else if (count !== 0) {
        numStudentsPartialAnswer += result3[i].CNT;
      }
      /* console.logparseInt(result3[i].answer2, 10)
      let len = answer.filter(value => result3[i].answer2.includes(value));
      if (len.length === answer.length) {
        numStudentsAnswer += result3[i].CNT;
      } else if (len.length !== 0) {
        numStudentsPartialAnswer += result3[i].CNT;
      } */
      numStudentsTotal += result3[i].CNT;
      const tmp = { student_answer: result3[i].answer, num_students: result3[i].CNT };
      studentsAnswers.push(tmp);
    }
  }
  res.json({ num_students_total: numStudentsTotal, num_students_answer: numStudentsAnswer, num_students_partial_answer: numStudentsPartialAnswer, student_answers: studentsAnswers, login_students: loginStudents });
}));

router.put('/:id/sqlite/:guid', modules.auth.questions, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { guid } = req.params;

  req.theQuestion.sqlite_file_guid = guid;
  await req.theQuestion.save();
  const files = await modules.models.file.findAll({
    where: { file_guid: guid },
  });

  res.json({ files });
}));

router.post('/:id/extract-answer', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  // item id 를 받음.
  const itemId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { ratioStudent } = req.body;

  const q = await modules.models.question.findOne({
    where: { lecture_item_id : itemId }
  });
  const questionId = q.question_id;
  // TODO 권한처리 점검
  const question = await modules.models.question.findByPk(questionId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
        include: {
          model: modules.models.class,
        },
      },
    },
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // TODO 예외처리 점검
  let err = null;
  try {
    const commend = `java -jar -Dfile.encoding=UTF-8 \
    ${path.join(modules.config.appRoot, 'modules/ESSAY_SCORING.jar')} \
    ${path.join(modules.config.appRoot, 'config/config-keyword-extractor.json')} \
    1 ${questionId} ${ratioStudent}`;
    const { stdout, stderr } = await exec(commend, { encoding: 'UTF-8' });
    console.log('commend = ', commend);
    if (stdout.indexOf('SUCCESS_SUCCESS') >= 0) {
      const data = stdout.split('SUCCESS_SUCCESS');
      console.log('data[1].toString("utf-8")', data[1].toString('utf8'));
      console.log('JSON.parse(data[1]) = ', JSON.parse(data[1]));
      res.json(JSON.parse(data[1]));
    } else {
      console.log('@quesiton.js / stderr = ', stderr);
      console.log('@quesiton.js / stdout = ', stdout);
      err = stderr + stdout;
    }
  } catch (e) {
    console.log('@quesiton.js / catch e = ', e);
    throw e;
  }
  if (err) {
    throw modules.errorBuilder.default(err, 500, false, true);
  }
}));

router.post('/:id/auto-grade-description', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const itemId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;
  const { teacherSideList, studentSideList } = req.body;

  const q = await modules.models.question.findOne({
    where: { lecture_item_id : itemId }
  });
  const questionId = q.question_id;

  // TODO 권한처리 점검
  const question = await modules.models.question.findByPk(questionId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
        include: {
          model: modules.models.class,
        },
      },
    },
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }

  // 강사님을 기준으로 추출한 모든 키워드, 배점쌍 모듈에 입력
  for (let i = 0; i < teacherSideList.length; i += 1) {
    let err = null;
    try {
      const commend = `java -jar -Dfile.encoding=UTF-8 \
      ${path.join(modules.config.appRoot, 'modules/ESSAY_SCORING.jar')} \
      ${path.join(modules.config.appRoot, 'config/config-keyword-extractor.json')} \
      2 1 ${questionId} ${teacherSideList[i].word} ${teacherSideList[i].score}`;
      console.log('commend = ', commend);
      const { stdout, stderr } = await exec(commend); // eslint-disable-line
      if (stdout.indexOf('SUCCESS_SUCCESS') < 0) {
        console.log('@quesiton.js / else: stdout = ', stdout);
        console.log('@quesiton.js / else: stderr = ', stderr);
        err = stderr + stdout;
      }
    } catch (e) {
      console.log('@quesiton.js / catch e = ', e);
      throw e;
    }
    if (err) {
      throw modules.errorBuilder.default(err, 500, false, true);
    }
  }

  // 학생을 기준으로 추출한 모든 키워드, 배점쌍 모듈에 입력
  for (let i = 0; i < studentSideList.length; i += 1) {
    let err = null;
    try {
      const commend = `java -jar -Dfile.encoding=UTF-8 \
      ${path.join(modules.config.appRoot, 'modules/ESSAY_SCORING.jar')} \
      ${path.join(modules.config.appRoot, 'config/config-keyword-extractor.json')} \
      2 2 ${questionId} ${studentSideList[i].word} ${studentSideList[i].score}`;
      console.log('commend = ', commend);
      const { stdout, stderr } = await exec(commend); // eslint-disable-line
      if (stdout.indexOf('SUCCESS_SUCCESS') < 0) {
        console.log('@quesiton.js / else: stdout = ', stdout);
        console.log('@quesiton.js / else: stderr = ', stderr);
        err = stderr + stdout;
      }
    } catch (e) {
      console.log('@quesiton.js / catch e = ', e);
      throw e;
    }
    if (err) {
      throw modules.errorBuilder.default(err, 500, false, true);
    }
  }

  // 결과 출력
  let err = null;
  try {
    const commend = `java -jar -Dfile.encoding=UTF-8 \
    ${path.join(modules.config.appRoot, 'modules/ESSAY_SCORING.jar')} \
    ${path.join(modules.config.appRoot, 'config/config-keyword-extractor.json')} \
    3 ${questionId}`;
    console.log('commend = ', commend);
    const { stdout, stderr } = await exec(commend); // eslint-disable-line
    if (stdout.indexOf('SUCCESS_SUCCESS') >= 0) {
      const data = stdout.split('SUCCESS_SUCCESS');
      console.log('@결과 출력 data[1] = ', data[1]);
      res.json(JSON.parse(data[1]));
    } else {
      console.log('@quesiton.js / stderr = ', stderr);
      console.log('@quesiton.js / stdout = ', stdout);
      err = stderr + stdout;
    }
  } catch (e) {
    console.log('@quesiton.js / catch e = ', e);
    throw e;
  }
  if (err) {
    throw modules.errorBuilder.default(err, 500, false, true);
  }
}));
module.exports = router;


// 강의 항목을 제거하도록
// router.delete('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
//   const { authId, authType } = req.decoded;
//   const questionId = parseInt(req.params.id, 10);
//
//   const question = await modules.models.question.findByPk(questionId, {
//     include: {
//       model: modules.models.lecture_item,
//       include: {
//         model: modules.models.lecture,
//       },
//     },
//   });
//   if (!question) {
//     throw modules.errorBuilder.default('Not Found', 404, true);
//   }
//   const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
//   if (!(authType === 3 || teacherCkeck)) {
//     throw modules.errorBuilder.default('Permission Denied', 403, true);
//   }
//
//   await modules.models.question.destroy({
//     where: {
//       question_id: questionId,
//     },
//   });
//
//   res.json({ success: true });
// }));

// 문항 순서 변경 제거됨.
// router.put('/:id/order', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
//   const { isForward } = req.body;
//   const questionId = parseInt(req.params.id, 10);
//   const { authId, authType } = req.decoded;
//
//   const question = await modules.models.question.findByPk(questionId, {
//     include: {
//       model: modules.models.lecture_item,
//       include: {
//         model: modules.models.lecture,
//       },
//     },
//   });
//   if (!question) {
//     throw modules.errorBuilder.default('Not Found', 404, true);
//   }
//   const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
//   if (!(authType === 3 || teacherCkeck)) {
//     throw modules.errorBuilder.default('Permission Denied', 403, true);
//   }
//
//   let otherQuestion = {};
//   if (isForward) {
//     otherQuestion = await modules.models.question.findOne({
//       where: {
//         lecture_item_id: question.lecture_item_id,
//         order: {
//           lt: question.order,
//         },
//       },
//       order: [
//         ['order', 'DESC'],
//       ],
//     });
//   } else {
//     otherQuestion = await modules.models.question.findOne({
//       where: {
//         lecture_item_id: question.lecture_item_id,
//         order: {
//           gt: question.order,
//         },
//       },
//       order: [
//         ['order', 'ASC'],
//       ],
//     });
//   }
//   if (!otherQuestion) {
//     throw modules.errorBuilder.default(isForward ? 'Already First' : 'Already Last', 405, true);
//   }
//   const { order } = question;
//   question.order = otherQuestion.order;
//   otherQuestion.order = order;
//   await question.save();
//   await otherQuestion.save();
//
//   res.json({ success: true });
// }));