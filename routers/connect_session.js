const express = require('express');
const db = require('../modules/db');
const router = express.Router();

const modules = require('../modules/frequent-modules');
const { sequelize } = require('../models');

// connect_session 로그 생성
router.post('/', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const { source, target, class_id, condition_lower, condition_upper } = req.body;
  
  const res1 = await modules.models.connect_session.create({
    source,
    target,
    class_id,
    condition_lower,
    condition_upper,
  });

  // const res2 = await modules.models.student_answer_log.findAll({
  //   attribute: [
  //     [sequelize.fn('sum', sequelize.col('score')), 'sum_understanding'],
  //     [sequelize.fn('count', sequelize.col('score')), 'count_understanding'], 
  //   ],
  //   where: {
  //     lecture_id: source,
  //     class_id,
  //   },
  //   group: ['student_id'],
  // });

  const sql = `SELECT SUM(score)/COUNT(score)*100 as understanding, student_id FROM student_answer_logs WHERE lecture_id = ${source} AND class_id = ${class_id} GROUP BY student_id;`;
  const res2 = await db.getQueryResult(sql);


  for (let i = 0; i < res2.length; i += 1) {
    if (parseInt(condition_upper, 10) !== 100) {
      if (res2[i].understanding >= condition_lower && res2[i].understanding < condition_upper) {
        await modules.models.connect_session_permission.create({
          connect_session_id: res1.dataValues.id,
          lecture_id: target,
          user_id: res2[i].student_id,
          class_id,
        });
      }
    } else {
      if (res2[i].understanding >= condition_lower && res2[i].understanding <= condition_upper) {
        await modules.models.connect_session_permission.create({
          connect_session_id: res1.dataValues.id,
          lecture_id: target,
          user_id: res2[i].student_id,
          class_id,
        });
      }
    }
  }
  
  res.json({ success: true });

}));

// connect_session 로그 수정
router.put('/:classId/:lectureId', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const class_id = parseInt(req.params.classId, 10);
  const lecture_id = parseInt(req.params.lectureId, 10);

  const connect = await modules.models.connect_session.update(
    { isActive: 1 },
    { where: {
        source: lecture_id,
        class_id,
      }
    },
  );
  
  res.json({ success: true });
}));

// connect_session 로그 조회
router.get('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const id = parseInt(req.params.id, 10);
  
//  const session_list = await modules.models.connect_session.findAll({
//    where: {
//      source: id,
//    },
//    include: [{
//      model: modules.models.lecture,
//    }],
//  });

  const sql = `SELECT * FROM connect_sessions cs, lectures l, lectures ll WHERE cs.source = l.lecture_id AND cs.target = ll.lecture_id AND cs.source = ${id};`;
  const session_list = await db.getQueryResult(sql);
  
  if ( session_list.length !== 0 ) {
    res.json({ success: true, session_list });
  } else {
    res.json({ success: false });
  }
}));

// connect_session class 단위 로그 조회
router.get('/class/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res) => {
  const { authId, authType } = req.decoded;
  const id = parseInt(req.params.id, 10);
  
  const sql = `SELECT * FROM connect_sessions cs, lectures l, lectures ll WHERE cs.source = l.lecture_id AND cs.target = ll.lecture_id AND cs.class_id = ${id};`;
  const session_list = await db.getQueryResult(sql);
  
  if ( session_list.length !== 0 ) {
    res.json({ success: true, session_list });
  } else {
    res.json({ success: false });
  }
}));

router.delete('/:id', modules.auth.tokenCheck, modules.asyncWrapper(async (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const session_list_info = await modules.models.connect_session.findAll({ where: { source: id } });
  if (!session_list_info) {
    throw modules.errorBuilder.default('Connect Session List Not Found', 409, true);
  }

  for (i = 0; i < session_list_info.length; i += 1) {
    await modules.models.connect_session_permission.destroy({
      where: {
        connect_session_id: session_list_info[i].dataValues.id,
      },
    });
  }
  await modules.models.connect_session.destroy({
    where: {
      source: id,
    },
  });

  res.json({ success: true });
}));

module.exports = router;