const express = require('express');
const router = express.Router();
const modules = require('../modules/frequent-modules');
/*
// 인증 (공통)
router.use(modules.auth.tokenCheck);
router.use(modules.auth.checkTokenIsAdmin);
*/

// 강사그룹 등록
router.post('/', modules.asyncWrapper(async (req, res) => {
  const {
    name,
    university_name,
    department_name,
    email_id_list,
  } = req.body;

  // 유저 검증, user_id 획득
  const user_id_list = [];
  const res4 = await modules.models.user.findAll({
    attributes: ['type', 'user_id'],
    where: {
      email_id: email_id_list,
    },
  });
  if ((email_id_list instanceof Array) && (email_id_list.length !== res4.length)) {
    throw new Error('매개변수로 받은 email_id중, 존재하지않는 유저가 포함되어 있습니다.');
  }
  for (let i = 0; i < res4.length; i += 1) {
    user_id_list.push(res4[i].user_id);
    if (res4[i].type !== 1) {
      throw new Error(`${email_id_list[i]} 유저는 강사가 아닙니다.`);
    }
  }

  // 강의 그룹 생성
  await modules.models.bank_group.create({
    name,
    university_name,
    department_name,
  });

  // group_id 검색
  const res1 = await modules.models.bank_group.findOne({
    attributes: ['group_id'],
    where: {
      name,
      university_name,
      department_name,
    },
  });
  if (res1 === null) {
    throw new Error(`${university_name}, ${department_name}, ${name} 에 알맞은 group_id가 없습니다.`);
  }
  const { group_id } = res1;
  
  // 강의 그룹에 강사 추가
  if (email_id_list instanceof Array) {
    for (let i = 0; i < email_id_list.length; i += 1) {
      await modules.models.teacher_group.create({
        group_id,
        user_id: user_id_list[i],
      });
    }
  }

  // 응답
  res.json({ success: true });
}));

// 강의 그룹 삭제
router.delete('/', modules.asyncWrapper(async (req, res) => {
  const { group_id } = req.query; // eslint-disable-line

  // 삭제
  const effectedRowNum = await modules.models.bank_group.destroy({
    where: {
      group_id,
    },
  });

  // 응답
  if (effectedRowNum !== 1) {
    throw new Error('delete 문에 영향을 받은 열의 수가 1이 아닙니다.');
  }

  res.json({ success: true });
}));

// 목록 조회
router.get('/list', modules.asyncWrapper(async (req, res) => {
  const { university_name, department_name } = req.query; // eslint-disable-line

  // bank_groups 테이블에서 그룹 id 목록 찾는다.
  const whereCondition = {};
  if (university_name !== undefined) whereCondition.university_name = university_name;
  if (department_name !== undefined) whereCondition.department_name = department_name;
  const res1 = await modules.models.bank_group.findAll({
    attributes: ['group_id'],
    order: [['group_id', 'ASC']],
    where: whereCondition,
  });
  const group_id_list = res1.map(x => x.group_id);

  // teacher_groups 테이블에서 해당 그룹 id 속한 목록 가져온다.
  const res2 = await modules.models.teacher_group.findAll({
    attributes: [],
    where: {
      group_id: group_id_list,
    },
    include: [
      {
        attributes: ['email_id', 'name', 'department_name'],
        model: modules.models.user,
      },
      {
        attributes: ['group_id', 'name'],
        model: modules.models.bank_group,
      },
    ],
  });

  /*
  const res1 = await modules.models.bank_group.findAll({
    attributes: ['name'],
    where: { university_name, department_name },
    order: [['name', 'ASC']],
    include: [
      {
        attributes: ['email_id'],
        model: modules.models.teacher_group,
        include: [
          {
            attributes: ['email_id', 'name', 'department_name'],
            model: modules.models.user,
          },
        ],
      },
    ],
  });

  const res2 = await modules.models.bank_group.findAll({
    attributes: ['name'],
    where: { university_name, department_name },
    order: [['name', 'ASC']],
    include: [
      {
        attributes: ['email_id'],
        model: modules.models.teacher_group,
        include: [
          {
            attributes: ['email_id', 'name', 'department_name'],
            model: modules.models.user,
          },
        ],
      },
    ],
  });
  */

  /*
  for (let i = 0; i < res1.length; i += 1) {
    const res2 = await modules.models.bank_group.findOne({
      attributes: ['name'],
      where: { email_id: res1[i].group_id },
    });
    res1[i].dataValues.group_name = res2.name;
  }

  for (let i = 0; i < res1.length; i += 1) {
    const res2 = await modules.models.user.findOne({
      attributes: ['name', 'department_name'],
      where: { email_id: res1[i].email_id },
    });
    res1[i].dataValues.name = res2.name;
    res1[i].dataValues.department_name = res2.department_name;
  }
  */
  res.json(res2);
}));

// 수정
router.put('/', modules.asyncWrapper(async (req, res) => {
  const {
    group_id,
    university_name,
    department_name,
    new_name,
    email_id_list,
  } = req.body;

  // 유저 검증, user_id 획득
  const user_id_list = [];
  const res4 = await modules.models.user.findAll({
    attributes: ['type', 'user_id'],
    where: {
      email_id: email_id_list,
    },
  });
  if ((email_id_list instanceof Array) && (email_id_list.length !== res4.length)) {
    throw new Error('매개변수로 받은 email_id중, 존재하지않는 유저가 포함되어 있습니다.');
  }
  for (let i = 0; i < res4.length; i += 1) {
    user_id_list.push(res4[i].user_id);
    if (res4[i].type !== 1) {
      throw new Error(`${email_id_list[i]} 유저는 강사가 아닙니다.`);
    }
  }

  // 은행 수정
  const updateContents = {};
  if (new_name !== undefined) updateContents.name = new_name;
  if (university_name !== undefined) updateContents.university_name = university_name;
  if (department_name !== undefined) updateContents.department_name = department_name;
  const effectedRowNum = await modules.models.bank_group.update(updateContents, {
    where: {
      group_id,
    },
  });
  if (effectedRowNum[0] !== 1) {
    throw new Error('update 문에 영향을 받은 열의 수가 1이 아닙니다.');
  }

  // 해당 은행의 모든 강사 삭제
  await modules.models.teacher_group.destroy({
    where: {
      group_id,
    },
  });

  // 강의 그룹에 강사 추가
  for (let i = 0; i < email_id_list.length; i += 1) {
    await modules.models.teacher_group.create({
      group_id,
      user_id: user_id_list[i],
    });
  }

  // 응답
  res.json({ success: true });
}));

// 수정할 강의 은행 조회
router.get('/', modules.asyncWrapper(async (req, res) => {
  const { group_id } = req.query; // eslint-disable-line

  // teacher_groups 테이블에서 해당 그룹 id 속한 목록 가져온다.
  const res1 = await modules.models.bank_group.findOne({
    attributes: ['group_id', 'name', 'university_name', 'department_name'],
    where: {
      group_id,
    },
    include: [
      {
        attributes: ['user_id'],
        model: modules.models.teacher_group,
        include: [
          {
            attributes: ['email_id', 'name', 'department_name'],
            model: modules.models.user,
          },
        ],
      },
    ],
  });

  
  /*
  const res1 = await modules.models.bank_group.findAll({
    attributes: ['name'],
    where: { university_name, department_name },
    order: [['name', 'ASC']],
    include: [
      {
        attributes: ['email_id'],
        model: modules.models.teacher_group,
        include: [
          {
            attributes: ['email_id', 'name', 'department_name'],
            model: modules.models.user,
          },
        ],
      },
    ],
  });

  const res2 = await modules.models.bank_group.findAll({
    attributes: ['name'],
    where: { university_name, department_name },
    order: [['name', 'ASC']],
    include: [
      {
        attributes: ['email_id'],
        model: modules.models.teacher_group,
        include: [
          {
            attributes: ['email_id', 'name', 'department_name'],
            model: modules.models.user,
          },
        ],
      },
    ],
  });
  */

  res.json(res1);
}));


// 에러 처리
router.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({ success: false, message: err.message });
});

module.exports = router;


/*

// 강사그룹에 강사 추가
router.post('/teacher', modules.asyncWrapper(async (req, res) => {
  const { name, university_name, department_name, email_id_list } = req.body;

  // 대학이름, 학과이름, 그룹이름을 주고 group_id를 받아오기
  const res1 = await modules.models.bank_group.findOne({
    attributes: ['group_id'],
    where: {
      name,
      department_name,
      university_name,
    },
  });

  if (res1 === null) {
    throw new Error('매개변수와 일치하는 group_id가 없습니다.');
  }
  const { group_id } = res1;

  // 유저 검증
  const res4 = await modules.models.user.findAll({
    attributes: ['type'],
    where: {
      email_id: email_id_list,
    },
  });
  if (email_id_list.length !== res4.length) {
    throw new Error('매개변수로 받은 email_id중, 존재하지않는 유저가 포함되어 있습니다.');
  }
  for (let i = 0; i < res4.length; i += 1) {
    if (res4[i].type !== 1) {
      throw new Error(`${email_id_list[i]} 유저는 강사가 아닙니다.`);
    }
  }

  // 저장
  for (let i = 0; i < email_id_list.length; i += 1) {
    await modules.models.teacher_group.create({
      group_id,
      email_id: email_id_list[i],
    });
  }

  // 응답
  res.json({ success: true });
}));

// 강사 그룹에서 강사 삭제
router.delete('/teacher', modules.asyncWrapper(async (req, res) => {
  const { university_name, department_name, group_name, email_id_list } = req.query; // eslint-disable-line
  const email_id_array = email_id_list.split(',');

  // group_id 받아오기
  const res1 = await modules.models.bank_group.findOne({
    attributes: ['group_id'],
    where: {
      name: group_name,
      university_name,
      department_name,
    },
  });
  if (res1 === null) {
    throw new Error(`${university_name}, ${department_name}, ${group_name} 에 알맞은 group_id가 없습니다.`);
  }
  const { group_id } = res1;
  
  // email_id_array으로 user_id_list 받아오기
  // const user_id_list = [];
  // for (let i = 0; i < email_id_array.length; i += 1) {
  //   const res2 = await modules.models.user.findOne({
  //     attributes: ['user_id'],
  //     where: {
  //       email_id: email_id_array[i],
  //     },
  //   });
  //   if (res2 === null) {
  //     throw new Error(`email_id = ${email_id_array[i]} 에 일치하는 user_id가 없습니다.`);
  //   }
  //   user_id_list.push(res2.user_id);
  // }

  // 유저 검증
  const res4 = await modules.models.user.findAll({
    attributes: ['type'],
    where: {
      email_id: email_id_array,
    },
  });
  if (email_id_array.length !== res4.length) {
    throw new Error('매개변수로 받은 email_id중, 존재하지않는 유저가 포함되어 있습니다.');
  }

  // 유저 검증
  const res5 = await modules.models.teacher_group.findAll({
    where: {
      group_id,
      email_id: email_id_array,
    },
  });
  if (email_id_array.length !== res5.length) {
    throw new Error('매개변수로 받은 email_id중, 해당 그룹에 없는 유저가 포함되어 있습니다.');
  }

  // 삭제
  const res6 = await modules.models.teacher_group.destroy({
    where: {
      group_id,
      email_id: email_id_array,
    },
  });

  res.json({ success: true });
}));

*/