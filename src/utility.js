const { exec } = require('child_process');

module.exports = function execTimeout10(cmd) {
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        console.warn(error);
        reject(error);
      }
      resolve(stdout || stderr);
    });
    setTimeout(reject, 10000, 'Promise timed out after 10000 ms');
  });
}
