-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.2.12-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win64
-- HeidiSQL 버전:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- extractor_scoring 데이터베이스 구조 내보내기
CREATE DATABASE IF NOT EXISTS `extractor_scoring` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `extractor_scoring`;

-- 테이블 extractor_scoring.analysised_word 구조 내보내기
CREATE TABLE IF NOT EXISTS `analysised_word` (
  `q_id` int(11) NOT NULL,
  `word` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `avg_count` double NOT NULL,
  `stdev_count` double NOT NULL,
  `avg_count_ratio` double NOT NULL,
  `stdev_count_ratio` double NOT NULL,
  `number_of_students` int(11) NOT NULL,
  `ratio_of_students` double NOT NULL,
  PRIMARY KEY (`q_id`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_scoring.answer_word 구조 내보내기
CREATE TABLE IF NOT EXISTS `answer_word` (
  `q_id` int(11) NOT NULL,
  `word` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  `existence` int(11) NOT NULL,
  PRIMARY KEY (`q_id`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_scoring.lecturer_answer 구조 내보내기
CREATE TABLE IF NOT EXISTS `lecturer_answer` (
  `q_id` int(11) NOT NULL,
  `word` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`q_id`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_scoring.scoring_result 구조 내보내기
CREATE TABLE IF NOT EXISTS `scoring_result` (
  `q_id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `score1` double NOT NULL,
  `score2` double NOT NULL,
  `score3` double NOT NULL,
  `score4` double NOT NULL,
  `normalized_score1` double NOT NULL,
  `normalized_score2` double NOT NULL,
  `normalized_score3` double NOT NULL,
  `normalized_score4` double NOT NULL,
  PRIMARY KEY (`q_id`,`student`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_scoring.student_answer 구조 내보내기
CREATE TABLE IF NOT EXISTS `student_answer` (
  `q_id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `word` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `count_ratio` double NOT NULL,
  PRIMARY KEY (`q_id`,`student`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 내보낼 데이터가 선택되어 있지 않습니다.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
