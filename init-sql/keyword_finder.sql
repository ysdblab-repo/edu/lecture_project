-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.2.12-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win64
-- HeidiSQL 버전:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- extractor_keyword_finder 데이터베이스 구조 내보내기
CREATE DATABASE IF NOT EXISTS `extractor_keyword_finder` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `extractor_keyword_finder`;

-- 테이블 extractor_keyword_finder.analysised_lecture_word 구조 내보내기
CREATE TABLE IF NOT EXISTS `analysised_lecture_word` (
  `lecture` int(11) NOT NULL,
  `word` varchar(50) NOT NULL,
  `avg_count` double NOT NULL,
  `stdev_count` double NOT NULL,
  `sum_count` double NOT NULL,
  `avg_count_ratio` double NOT NULL,
  `stdev_count_ratio` double NOT NULL,
  `sum_count_ratio` double NOT NULL,
  `num_of_materials` int(11) NOT NULL,
  `importance` double NOT NULL,
  `not_selected` int(11) NOT NULL,
  PRIMARY KEY (`lecture`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_keyword_finder.analysised_material_word 구조 내보내기
CREATE TABLE IF NOT EXISTS `analysised_material_word` (
  `lecture` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `word` varchar(50) NOT NULL,
  `avg_count` double NOT NULL,
  `stdev_count` double NOT NULL,
  `sum_count` double NOT NULL,
  `avg_count_ratio` double NOT NULL,
  `stdev_count_ratio` double NOT NULL,
  `sum_count_ratio` double NOT NULL,
  `num_of_materials` int(11) NOT NULL,
  `importance` double NOT NULL,
  `not_selected` int(11) NOT NULL,
  PRIMARY KEY (`lecture`,`material`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_keyword_finder.guideword 구조 내보내기
CREATE TABLE IF NOT EXISTS `guideword` (
  `lecture` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `file` text NOT NULL,
  `word` varchar(50) NOT NULL,
  `count` int(11) NOT NULL,
  `count_ratio` double NOT NULL,
  PRIMARY KEY (`lecture`,`material`,`word`,`file`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_keyword_finder.lecture_keyword 구조 내보내기
CREATE TABLE IF NOT EXISTS `lecture_keyword` (
  `lecture` int(11) NOT NULL,
  `word` varchar(50) NOT NULL,
  `importance` double NOT NULL,
  PRIMARY KEY (`lecture`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_keyword_finder.lecture_parameter 구조 내보내기
CREATE TABLE IF NOT EXISTS `lecture_parameter` (
  `lecture` int(11) NOT NULL,
  `w1` double NOT NULL,
  `w2` double NOT NULL,
  PRIMARY KEY (`lecture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.
-- 테이블 extractor_keyword_finder.material_keyword 구조 내보내기
CREATE TABLE IF NOT EXISTS `material_keyword` (
  `lecture` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `word` varchar(50) NOT NULL,
  `importance` double NOT NULL,
  PRIMARY KEY (`lecture`,`material`,`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
