const schedule = require('node-schedule');
const config = require('../config');
const db = require('./db');
const coverage = require('./coverage-calculator');
const modules = require('../modules/frequent-modules');

async function deleteSchedule(lecture) {

  var j = schedule.scheduledJobs[`lecture_${lecture.lecture_id}`]
  if ( j !== undefined ) {
    j.cancel();
  }

}
async function enrollSchedule (lecture) {

  let date = new Date(lecture.end_time)
  date.setHours(date.getHours(), date.getMinutes(), date.getSeconds());
  console.log(`${lecture.name}[${lecture.lecture_id}] 강의의 계산시간이 ${date}에 등록되었습니다.`);
  var ss = schedule.scheduleJob(`lecture_${lecture.lecture_id}`,date, function(){
      modules.models.lecture_start.create({
        lecture_id: lecture.lecture_id,
        teacher_id: lecture.teacher_id
      }).then(() => coverage.understandingCoverage(lecture.lecture_id,lecture.teacher_id,1));
  })
}
async function modifySchedule (lecture)  {
    deleteSchedule(lecture).then(() => enrollSchedule(lecture));
}
async function scheduleList () {
  console.log(`schedule list ${JSON.stringify(schedule.scheduledJobs)}`);
}
exports.deleteSchedule = deleteSchedule;
exports.enrollSchedule = enrollSchedule;
exports.modifySchedule = modifySchedule;
exports.scheduleList = scheduleList;