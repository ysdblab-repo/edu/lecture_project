const PythonShell = require('python-shell');
const config = require('../config');
const path = require('path');

function modifySqlOut(sqlResult) {
  let arrayJson = [];
  let res;

  if (sqlResult.result === 0 || sqlResult.result === 1) {
    const field = sqlResult.result_field;
    const value = sqlResult.result_value;
    const numOfField = field.length;

    for (let v of value) {
      let strJson = '{';
      for (let i = 0; i < numOfField; i++) {
        strJson += ` ${JSON.stringify(field[i])}: ${JSON.stringify(v[i])}`;
        if (i < numOfField - 1)
          strJson += ', ';
      }
      strJson += ' }';
      arrayJson.push(JSON.parse(strJson));
    }

    res = `{ "result": ${JSON.stringify(sqlResult.result)}, "result_value": ${JSON.stringify(arrayJson)}`;

    if (sqlResult.result === 1)
      res += `, "error": ${JSON.stringify(sqlResult.error)}`;
  } else if (sqlResult.result === 2)
    res = `{ "result": ${JSON.stringify(sqlResult.result)}, "error": ${JSON.stringify(sqlResult.error)}`;

  res += ' }';

  return JSON.parse(res);
}

exports.judge = (instructor_sql, student_sql, dbpath) => new Promise((resolve, reject) => {
  const options = {
    mode: 'json',
    args: [instructor_sql, student_sql, dbpath],
    scriptPath: path.join(config.appRoot, '/modules'),
  };
  const shell = new PythonShell('judge_sql_print.py', options);
  shell.on('message', (message) => {
    resolve(modifySqlOut(message));
  });
  shell.on('error', (error) => {
    reject(error);
  });
});
