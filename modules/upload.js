const multer = require('multer');
const path = require('path');
const fs = require('fs');

const modules = require('./frequent-modules');

const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
const generateGUID = () => `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const fileGuid = generateGUID();
    const filePath = path.join(modules.config.appRoot, `public/materials/${fileGuid}`);
    const fileParsed = path.parse(file.originalname);

    req.fileInfo = {};
    req.fileInfo.fileGuid = fileGuid;
    req.fileInfo.staticPath = path.join(filePath, file.originalname);
    req.fileInfo.clientPath = path.join('/materials/', fileGuid, file.originalname);
    req.fileInfo.extern = fileParsed.ext;

    fs.mkdirSync(filePath);
    cb(null, filePath);
  },
  filename: (req, file, cb) => {
    const fileParsed = path.parse(file.originalname);
    cb(null, fileParsed.base);
  },
});
const upload = multer({
  storage,
  limits: {
    fileSize: 1024 * 1024 * 1024,
  },
});
exports.upload = upload.single('file');
exports.uploadMulti = upload.array('file');

// type 0 - file -> bank , type 1 - bank -> file
exports.copy = async (staticPath, name, foreignKey, key, authId, lcItemId, type) => {
  const fileGuid = generateGUID();
  const newStaticPath = path.join(modules.config.appRoot, `public/materials/${fileGuid}`, name);
  const newClientPath = path.join('/materials/', fileGuid, name);
  const fileParsed = path.parse(name);
  fs.mkdirSync(path.join(modules.config.appRoot, `public/materials/${fileGuid}`));

  fs.copyFile(staticPath, path.join(`public/materials/${fileGuid}`, name), (err) => {
    if (err) throw err;
    console.log("copy Success");
  })

  let newFile = null;

  if (type === 0) {
    newFile = await modules.models.bank_file.create({
      file_guid: fileGuid,
      static_path: newStaticPath,
      client_path: newClientPath,
      name: name,
      file_type:  fileParsed.ext,
      [foreignKey]: key,
      uploader_id: authId,
      lecture_item_id: lcItemId,
    });
  } else if (type === 1) {
    newFile = await modules.models.file.create({
      file_guid: fileGuid,
      static_path: newStaticPath,
      client_path: newClientPath,
      name: name,
      file_type:  fileParsed.ext,
      [foreignKey]: key,
      uploader_id: authId,
      lecture_item_id: lcItemId,
    });
  }

  return newFile;
}
exports.fileAdd = async (req, foreignKey, key) => {
  await modules.models.file.create({
    file_guid: req.fileInfo.fileGuid,
    static_path: req.fileInfo.staticPath,
    client_path: req.fileInfo.clientPath,
    name: path.basename(req.fileInfo.clientPath),
    file_type: req.fileInfo.extern,
    [foreignKey]: key,
    uploader_id: req.decoded.authId,
  });

  return modules.models.file.findOne({
    where: { file_guid: req.fileInfo.fileGuid }, attributes: modules.models.file.selects.fileClient,
  });
};
exports.fileAdd2 = async (req, foreignKey, key, class_id) => { //파일 추가시 class_id 추가하기 위한 export. 모든 파일에 class_id 추가하는 것을 목표로 설계

  await modules.models.file.create({
    file_guid: req.fileInfo.fileGuid,
    static_path: req.fileInfo.staticPath,
    client_path: req.fileInfo.clientPath,
    name: path.basename(req.fileInfo.clientPath),
    file_type: req.fileInfo.extern,
    [foreignKey]: key,
    uploader_id: req.decoded.authId,
    class_id: class_id,
  });

  return modules.models.file.findOne({
    where: { file_guid: req.fileInfo.fileGuid }, attributes: modules.models.file.selects.fileClient,
  });
};
