const modules = require('./frequent-modules');
// const discord = require('../discord');
module.exports = (err, req, res, next) => {
  res.status(err.status || 500);

  if (!err.notDisplay) {
    console.log(err);
  }

  modules.models.server_error_log.create({
    status: err.status || 500,
    code: err.code,
    message: err.message,
    stack: err.stack,
    errno: err.errno,
    syscall: err.syscall,
    path: err.path,
    address: err.address,
    port: err.port,
    client_ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
  });
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  // if ((ip.includes('165.132') || ip.includes('127.0'))){}else{
  //   discord.discordHandler(`${ip} - ${err.message}`);
  // }
  if (!err.notSend) {
    res.json({
      error: true,
      status: err.status || 500,
      message: err.message,
    });
  }
};
