const jwt = require('jsonwebtoken');
const errorBuilder = require('./error-builder');
const config = require('../config');
const db = require('./db');
const asyncWrapper = require('./asyncWrapper');
const modules = require('./frequent-modules');

const verify = token => new Promise((resolve, reject) => {
  jwt.verify(token, config.jwtSecret, (err, decoded) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        return reject(errorBuilder.default('EXPIRED', 401, true));
      }
      return reject(errorBuilder.default('Login Token Error', 401, true));
    }
    return resolve(decoded);
  });
});

exports.verify = token => verify(token);

const verify_test2 = (token, ip) => new Promise((resolve, reject) => {
  jwt.verify(token, config.jwtSecret, async (err, decoded) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        return reject(errorBuilder.default('EXPIRED', 401, true));
      }
      return reject(errorBuilder.default('Login Token Error', 401, true));
    }
    console.log(`ip - ${ip}`);
    let sql = `SELECT * FROM test_logins WHERE user_id = ${decoded.authId} AND ip = '${ip}' AND now() < expired`;
    let result = await db.getQueryResult(sql);
    if (result.length === 0) {
      return reject(errorBuilder.default('다시 로그인하여 주세요.', 401, true));
    }
    let sql2 = `DELETE FROM test_logins WHERE user_id = ${decoded.authId} AND ip != '${ip}'`;
    await db.getQueryResult(sql2);
    return resolve(decoded);
  });
});

const verify_test = (token, ip) => new Promise((resolve, reject) => {
   jwt.verify(token, config.jwtSecret, async (err, decoded) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        return reject(errorBuilder.default('EXPIRED', 401, true));
      }
      return reject(errorBuilder.default('Login Token Error', 401, true));
    }
  
    let res = await modules.models.test_login.findAll({ where : { user_id: decoded.authId }}); // token createdAt > curtime()

    if (res.length >= 2) {
  
      let first = res[0].ip;
      for (let i = 0; i < res.length ; i += 1) {
        if (res[i].ip !== first){
          await modules.models.test_login.destroy({ where: { user_id: decoded.authId }});
          return reject(errorBuilder.default('다른 ip에서 접속한 이력이 발견되었습니다. 다시 로그인하여 주세요.', 401, true));
        }
      }
      return resolve(decoded);
    } else if (res.length === 1) {
      await modules.models.test_login.update({
        ip
      },{
        where: { user_id: decoded.authId }
      })
      return resolve(decoded);
    } else if (res.length === 0) {
      return reject(errorBuilder.default('다시 로그인하여 주세요.', 401, true));
    }
  });
});

// const duplicatedVerify = (user_id, ip) => new Promise(async (resolve, reject) => {

//   let res = await modules.models.test_login.findAll({ where : { user_id }}); // token createdAt > curtime()
//   console.log(`ip len -${res.length}`);
//   if (res.length >= 2) {

//     let first = res[0].ip;
//     for (let i = 0; i < res.length ; i += 1) {
//       if (res[i].ip !== first){
//         await modules.models.test_login.destroy({ where: { user_id }});
//         console.log(`다른 ip 접속 확인`);
//         resolve(-1); // 동시접속으로 둘다 로그인해제 , 재로그인하세요
//       }
//     }
//     resolve(1); // 한 계정에서 여러번 접속

//   } else if (res.length === 1) {
//     await modules.models.test_login.update({
//       ip
//     },{
//       where: { user_id }
//     })
//     console.log('같은 ip에서 접속');
//     resolve(1);
//   } else if (res.length === 0) {
//     resolve(0)
//   }

// })

// async function duplicatedVerify(user_id, ip)  {
//   let res = await modules.models.test_login.findAll({ where : { user_id }}); // token createdAt > curtime()
//   console.log(`ip len -${res.length}`);
//   if (res.length >= 2) {
//     await modules.models.test_login.destroy({ where: { user_id }});
//     console.log(`다른 ip 접속 확인`);
//     return -1; // 동시접속으로 둘다 로그인해제 , 재로그인하세요
//   } else if (res.length === 1) {
//     console.log('같은 ip에서 접속');
//     return 1;
//   } else if (res.length === 0) {
//     return 0;
//   }
// }


exports.tokenCheckTest = asyncWrapper(async (req, res, next) => {
  const token = req.headers['x-access-token'] || req.query.token;
  if (!token) {
    throw errorBuilder.default('Not Logged In', 401, true);
  }
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  
  req.decoded = await verify_test2(token, ip);
  next();
});


exports.tokenCheck = asyncWrapper(async (req, res, next) => {
  const token = req.headers['x-access-token'] || req.query.token;

  if (!token) {
    throw errorBuilder.default('Not Logged In', 401, true);
  }
  req.decoded = await verify(token);
  next();
});


exports.tokenCheckNotError = asyncWrapper(async (req, res, next) => {
  const token = req.headers['x-access-token'] || req.query.token;

  if (token) {
    req.decoded = await verify(token);
  }
  next();
});




exports.checkTokenIsAdmin = asyncWrapper(async (req, res, next) => {
  const { authType } = req.decoded;
  if (!(authType === 3)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  next();
});

exports.questions = asyncWrapper(async (req, res, next) => {
  const token = req.headers['x-access-token'] || req.query.token;
  if (!token) {
    throw errorBuilder.default('Not Logged In', 401, true);
  }
  req.decoded = await verify(token);

  const questionId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const question = await modules.models.question.findByPk(questionId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!question) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, question.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  req.theQuestion = question;
  next();
});

exports.materials = asyncWrapper(async (req, res, next) => {
  const token = req.headers['x-access-token'] || req.query.token;
  if (!token) {
    throw errorBuilder.default('Not Logged In', 401, true);
  }
  req.decoded = await verify(token);

  const materialId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const material = await modules.models.material.findByPk(materialId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!material) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, material.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  req.theMaterial = material;
  next();
});

exports.homework = asyncWrapper(async (req, res, next) => {
  const token = req.headers['x-access-token'] || req.query.token;
  if (!token) {
    throw errorBuilder.default('Not Logged In', 401, true);
  }
  req.decoded = await verify(token);

  const homeworkId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const homework = await modules.models.homework.findByPk(homeworkId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!homework) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, homework.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  req.thehomework = homework;
  next();
});

exports.survey = asyncWrapper(async (req, res, next) => {
  const token = req.headers['x-access-token'] || req.query.token;
  if (!token) {
    throw errorBuilder.default('Not Logged In', 401, true);
  }
  req.decoded = await verify(token);

  const surveyId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const survey = await modules.models.survey.findByPk(surveyId, {
    include: {
      model: modules.models.lecture_item,
      include: {
        model: modules.models.lecture,
      },
    },
  });
  if (!survey) {
    throw modules.errorBuilder.default('Not Found', 404, true);
  }
  const teacherCkeck = await modules.teacherInClass(authId, survey.lecture_item.lecture.class_id);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  req.theSurvey = survey;
  next();
});

exports.class = asyncWrapper(async (req, res, next) => {
  const token = req.headers['x-access-token'] || req.query.token;
  if (!token) {
    throw errorBuilder.default('Not Logged In', 401, true);
  }
  req.decoded = await verify(token);

  const classId = parseInt(req.params.id, 10);
  const { authId, authType } = req.decoded;

  const teacherCkeck = await modules.teacherInClass(authId, classId);
  if (!(authType === 3 || teacherCkeck)) {
    throw modules.errorBuilder.default('Permission Denied', 403, true);
  }
  req.theClass = await modules.models.class.findByPk(classId);
  next();
});
