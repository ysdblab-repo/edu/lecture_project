const path = require('path');
const fs = require('fs');
const _rimraf = require('rimraf');
const util = require('util');
const rimraf = util.promisify(_rimraf);

const modules = require('./frequent-modules');

exports.files = async (fileGuid) => {
  const fileInfo = await modules.models.file.findOne({
    where: { file_guid: fileGuid },
  });

  if (fileInfo) {
    const parsedPath = path.parse(fileInfo.static_path);
    if (fs.existsSync(parsedPath.dir)) {
      await rimraf(parsedPath.dir);
    }
  }
  await modules.models.file.destroy({
    where: { file_guid: fileGuid },
  });
};

exports.testcases = async (questionId, num) => {
  const testcase = await modules.models.problem_testcase.findOne({
    where: { question_id: questionId, num },
  });
  if (!testcase) {
    return;
  }

  if (fs.existsSync(testcase.input_path)) {
    fs.unlinkSync(testcase.input_path);
  }
  if (fs.existsSync(testcase.output_path)) {
    fs.unlinkSync(testcase.output_path);
  }
  await modules.models.problem_testcase.destroy({
    where: { question_id: questionId, num },
  });
};

exports.lectureItems = async (itemId) => {
  const lectureItem = (await modules.models.lecture_item.findByPk(itemId, {
    include: [{
      model: modules.models.question,
      include: [
        { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
        { model: modules.models.problem_testcase },
      ],
    }, {
      model: modules.models.survey,
      include: { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
    }, {
      model: modules.models.homework,
      include: { model: modules.models.file, attributes: modules.models.file.selects.fileClient },
    }],
  })).toJSON();

  const deleteFilePromises = [1];
  for (let i = 0; i < lectureItem.questions.length; i += 1) {
    for (let j = 0; j < lectureItem.questions[i].files.length; j += 1) {
      deleteFilePromises.push(this.files(lectureItem.questions[i].files[j].file_guid));
    }
    for (let j = 0; j < lectureItem.questions[i].problem_testcases.length; j += 1) {
      deleteFilePromises.push(this.testcases(lectureItem.questions[i].question_id, lectureItem.questions[i].problem_testcases[j].num));
    }
  }
  for (let i = 0; i < lectureItem.homework.length; i += 1) {
    for (let j = 0; j < lectureItem.homework[i].files.length; j += 1) {
      deleteFilePromises.push(this.files(lectureItem.homework[i].files[j].file_guid));
    }
  }
  for (let i = 0; i < lectureItem.surveys.length; i += 1) {
    for (let j = 0; j < lectureItem.surveys[i].files.length; j += 1) {
      deleteFilePromises.push(this.files(lectureItem.surveys[i].files[j].file_guid));
    }
  }

  await Promise.all(deleteFilePromises);
  return await modules.models.lecture_item.destroy({
    where: {
      lecture_item_id: itemId,
    },
  });
};

exports.lecture = async (lectureId) => {
  const lecture = await modules.models.lecture.findByPk(lectureId, {
    include: { model: modules.models.lecture_item },
  });

  const deleteItemPromises = [1];
  for (let i = 0; i < lecture.lecture_items.length; i += 1) {
    deleteItemPromises.push(this.lectureItems(lecture.lecture_items[i].lecture_item_id));
  }

  await Promise.all(deleteItemPromises);
  await modules.models.lecture.destroy({
    where: {
      lecture_id: lectureId,
    },
  });
};

exports.class = async (classId) => {
  const theClass = await modules.models.class.findByPk(classId, {
    include: { model: modules.models.lecture },
  });

  const deleteLecturePromises = [1];
  for (let i = 0; i < theClass.lectures.length; i += 1) {
    deleteLecturePromises.push(this.lecture(theClass.lectures[i].lecture_id));
  }

  await Promise.all(deleteLecturePromises);
  await modules.models.class.destroy({
    where: {
      class_id: classId,
    },
  });
};

// 은행 관련 삭제 코드 추가 190904
// 은행 파일 삭제
exports.bankFiles = async (fileGuid) => {
  const fileInfo = await modules.models.bank_file.findOne({
    where: { file_guid: fileGuid },
  });

  if (fileInfo) {
    const parsedPath = path.parse(fileInfo.static_path);
    if (fs.existsSync(parsedPath.dir)) {
      await rimraf(parsedPath.dir);
    }
  }
  await modules.models.bank_file.destroy({
    where: { file_guid: fileGuid },
  });
};

// 은생 testcase 삭제
exports.bankTestcases = async (questionId, num) => {
  const testcase = await modules.models.bank_problem_testcase.findOne({
    where: { question_id: questionId, num },
  });
  if (!testcase) {
    return;
  }

  if (fs.existsSync(testcase.input_path)) {
    fs.unlinkSync(testcase.input_path);
  }
  if (fs.existsSync(testcase.output_path)) {
    fs.unlinkSync(testcase.output_path);
  }
  await modules.models.bank_problem_testcase.destroy({
    where: { question_id: questionId, num },
  });
};

// 은행 강의 아이템 삭제
exports.bankLectureItems = async (itemId) => {
  const bankLectureItem = (await modules.models.bank_lecture_item.findByPk(itemId, {
    include: [{
      model: modules.models.bank_question,
      include: [
        { model: modules.models.bank_file, attributes: modules.models.bank_file.selects.fileClient },
        { model: modules.models.bank_problem_testcase },
      ],
    }, {
      model: modules.models.bank_survey,
      include: { model: modules.models.bank_file, attributes: modules.models.bank_file.selects.fileClient },
    }, {
      model: modules.models.bank_homework,
      include: { model: modules.models.bank_file, attributes: modules.models.bank_file.selects.fileClient },
    }],
  })).toJSON();

  const deleteFilePromises = [1];
  for (let i = 0; i < bankLectureItem.bank_questions.length; i += 1) {
    for (let j = 0; j < bankLectureItem.bank_questions[i].bank_files.length; j += 1) {
      deleteFilePromises.push(this.bankFiles(bankLectureItem.bank_questions[i].bank_files[j].file_guid));
    }
    for (let j = 0; j < bankLectureItem.bank_questions[i].bank_problem_testcases.length; j += 1) {
      deleteFilePromises.push(this.bankTestcases(bankLectureItem.bank_questions[i].question_id, bankLectureItem.bank_questions[i].bank_problem_testcases[j].num));
    }
  }
  for (let i = 0; i < bankLectureItem.bank_homeworks.length; i += 1) {
    for (let j = 0; j < bankLectureItem.bank_homeworks[i].bank_files.length; j += 1) {
      deleteFilePromises.push(this.bankFiles(bankLectureItem.bank_homework[i].bank_files[j].file_guid));
    }
  }
  for (let i = 0; i < bankLectureItem.bank_surveys.length; i += 1) {
    for (let j = 0; j < bankLectureItem.bank_surveys[i].bank_files.length; j += 1) {
      deleteFilePromises.push(this.bankFiles(bankLectureItem.bank_surveys[i].bank_files[j].file_guid));
    }
  }

  await Promise.all(deleteFilePromises);
  await modules.models.bank_lecture_item.destroy({
    where: {
      lecture_item_id: itemId,
    },
  });
};

// 은행 강의 삭제
exports.bank_lecture = async (lectureId) => {
  const bank_lecture = await modules.models.bank_lecture.findByPk(lectureId, {
    include: { model: modules.models.bank_lecture_item },
  });

  const deleteBankItemPromises = [1];
  for (let i = 0; i < bank_lecture.bank_lecture_items.length; i += 1) {
    deleteBankItemPromises.push(this.bankLectureItems(bank_lecture.bank_lecture_items[i].lecture_item_id));
  }

  await Promise.all(deleteBankItemPromises);
  await modules.models.bank_lecture.destroy({
    where: {
      lecture_id: lectureId,
    },
  });
};
