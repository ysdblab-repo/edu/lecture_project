const db = require('./db');
const models = require('../models');
const modules = require('./frequent-modules');
const config = require('../config');
const errorBuilder = require('./error-builder');
const sqlJudge = require('./run-sql-judger');
const cluster = require('cluster');

const numMatching = (answers, studentAnswers) => {
  let num = 0;
  const answerLength = answers.length;
  const studentLength = studentAnswers.length;
  for (let i = 0; i < answerLength; i += 1) {
    for (let j = 0; j < studentLength; j += 1) {
      if (answers[i] === studentAnswers[j].toString()) {
        num += 1;
        break;
      }
    }
  }
  return num;
};
const numMatching7 = (answers, studentAnswers) => {
  let num = 0;
  const answerLength = answers.length;
  const studentLength = studentAnswers.length;
  for (let i = 0; i < answerLength && i < studentLength ; i += 1) {
    if (answers[i] === studentAnswers[i].toString()) {
      num += 1;
    }
  }
  return num;
};
const shortNumMatching = (answer, studentAnswers) => {
  let num = 0;
  const answerLength = answers.length;
  const studentLength = studentAnswers.length;
  for (let i = 0; i < answerLength; i += 1) {
    for (let j = 0; j < studentLength; j += 1) {
      let answer = answer[i].toUpperCase().split(' ').join('').split('\n').join('');
      const studentAnswer = studentAnswers[j].toUpperCase().split(' ').join('').split('\n').join('');
      if (answer === studentAnswer) {
        num += 1;
        break;
      }
    }
  }
  return num;
}

exports.oj_solution_judge = async (solution_id) => {
  const oj_sol = await models.oj_solution.findOne({
    where: solution_id
  });
  const sal = await models.student_answer_log.findOne({
    where: {
      student_answer_log_id: oj_sol.student_answer_log_id
    },
    include: [{
      model: models.oj_source_code
    }, {
      model: models.question,
      include: [{
        model: models.problem_testcase
      }],
    }]
  });
  let ratio;
  try {
    ratio = (await this.oj_solution_scoreing(oj_sol, sal)).ratio;
  } catch (err) {
    console.log(err);
  }
  oj_sol.judged = 1;
  oj_sol.judgetime = models.sequelize.fn('NOW');
  sal.score = sal.dataValues.question.dataValues.score * ratio;
  sal.ratio = ratio;
  await Promise.all([oj_sol.save(), sal.save()]);
}

exports.oj_solution_scoreing = async (oj_solution, student_answer_log) => {
  // 테스트 케이스 및 코드 추출
  const code = student_answer_log.answer;
  const testcases = student_answer_log.question.problem_testcases.map(testcase => ({
    testcase_id: testcase.testcase_id,
    input: testcase.input,
    output: testcase.output,
    score_ratio: testcase.score_ratio,
    memory: testcase.memory,
    timeout: testcase.timeout
  }));
  const language = student_answer_log.question.accept_language;
  const runResults = [];

  //테스트 케이스 실행
  for (let i = 0; i < testcases.length; i++) {
    const runResult = await modules.runCode(language, code, testcases[i].input, testcases[i].memory, testcases[i].timeout);
    runResults.push(runResult);
  }

  // 실행 결과 출력
  await Promise.all(runResults.map((runResult, index) => models.problem_judge_log.create({
    question_id: student_answer_log.question_id,
    user_id: student_answer_log.student_id,
    solution_id: oj_solution.solution_id,
    testcase_id: testcases[index].testcase_id,
    input: testcases[index].input,
    output: runResult.output,
    expected_output: testcases[index].output,
    correct: !runResult.timeout && this.oj_solution_compare_output(runResult.output, testcases[index].output, (a, b) => a === b),
    time: runResult.time,
    memory: testcases[index].memory,
    time_limit: testcases[index].timeout,
    memory_limit: testcases[index].memory,
    comment: runResult.timeout ? 'timeout' : ''
  })));

  // 점수 계산
  const score_sum = runResults.map((runResult, index) => !runResult.timeout && this.oj_solution_compare_output(runResult.output, testcases[index].output, (a, b) => a === b) ? testcases[index].score_ratio : 0).reduce((a, b) => a + b);
  const total_score = testcases.map(testcase => testcase.score_ratio).reduce((a,b) => a + b);

  // 지표 분석
  const tokens = await modules.tokenizePython(code);
  const statistics = await modules.tokenStatisticsPython(tokens);
  const ast = await modules.astCodePython(code);
  // 분석 결과 저장
  await models.oj_analysis.create({
    solution_id: oj_solution.solution_id,
    ...statistics,
    ...ast,
  });
  const ratio = score_sum / total_score;
  return {
    // score: score_sum,
    ratio,
  };
};

exports.oj_solution_compare_output = (output, answer, correctFunc) => {
  return correctFunc(output, answer);
}

exports.all = async (studentId, lectureId, classId, order) => {
  // const promises = [];
  // promises.push(this.ordered(studentId, lectureId, order));
  // promises.push(this.lecture(studentId, lectureId));
  // promises.push(this.class(studentId, classId));
  return Promise.all(promises);
};
// 주관식 답안 매칭
exports.match1 = (question, studentAnswers) => {
  

  const answer = question.answer.split(config.separator);
  let Answers = [];
  for ( let i = 0 ; i < answer.length ; i += 1 ){
    Answers.push(answer[i].toUpperCase().split(' ').join('').split('\n').join(''));
  }
  const studentAnswer = studentAnswers[0].toUpperCase().split(' ').join('').split('\n').join('');
  let studentRatio = 0;
  for ( let i = 0 ; i < Answers.length ; i += 1 ){
    if ( studentAnswer === Answers[i] ){
      studentRatio = 1;
      break;
    }
  }
  const studentScore = (question.score * studentRatio).toFixed(1);
  console.log(`score -${studentScore}`);

  return { studentScore, studentRatio };

}
// 재 체점용
exports.match11 = (question, studentAnswers) => {
  
  const answer = question.answer.toUpperCase().split(' ').join('').split('\n').join('');
  const studentAnswer = studentAnswers.toUpperCase().split(' ').join('').split('\n').join('');
  let studentRatio = 0;
  if ( answer === studentAnswer ){
    studentRatio = 1;
  }else {
    studentRatio = 0;
  }
  const studentScore = (question.score * studentRatio).toFixed(1);
  return { studentScore, studentRatio };

}
// 주관식 여러 답안 체점용
exports.match111 = (question, studentAnswers) => {

  const answers = question.answer.split(config.separator);
  const matchingCount = shortNumMatching(answers, studentAnswers);
  const studentRatio = matchingCount / ((answers.length + studentAnswers.length) - matchingCount);
  const studentScore = (question.score * studentRatio).toFixed(1);
  return { studentScore, studentRatio };
}
// 객관식 답안 매칭
exports.match = (question, studentAnswers) => {
  const answers = question.answer.split(config.separator);
  const matchingCount = numMatching(answers, studentAnswers);
  const studentRatio = matchingCount / ((answers.length + studentAnswers.length) - matchingCount);
  const studentScore = (question.score * studentRatio).toFixed(1);
  return { studentScore, studentRatio };
};

// 객관식 답안 매칭
exports.match7 = (question, studentAnswers) => {
  const answers = question.answer.split(config.separator);
  const matchingCount = numMatching7(answers, studentAnswers);
  const studentRatio = matchingCount / answers.length;
  const studentScore = (question.score * studentRatio).toFixed(1);
  return { studentScore, studentRatio };
};

const codeLanguage = (languageString) => {
  let languageInt;
  if (languageString === 'c') {
    languageInt = 0;
  } else if (languageString === 'cpp') {
    languageInt = 1;
  } else if (languageString === 'cshop') {
    languageInt = 9;
  } else if (languageString === 'java') {
    languageInt = 3;
  } else if (languageString === 'python3') {
    languageInt = 19;
  } else if (languageString === 'shell_script') {
    languageInt = 5;
  } else if (languageString === 'free_basic') {
    languageInt = 11;
  } else if (languageString === 'go') {
    languageInt = 17;
  } else if (languageString === 'php') {
    languageInt = 7;
  }
  return languageInt;
};

// 2021-08-25 2회 이상 [제출]할 때는 update Handling
exports.updateHnadling = async (studentId, data) => {
  const question = await models.question.findByPk(data.questionId, {
    include: [{
      model: models.lecture_item,
      include: {
        model: models.lecture,
        include: {
          model: models.class,
        },
      },
    },
    ],
  });
  question.sql_lite_file = await db.getQueryResult(`SELECT * FROM 
files as f join questions as q on q.sqlite_file_guid=f.file_guid where q.question_id=${data.questionId}`);
  const student = await models.student_answer_log.findOne({
    where : {
      student_id : studentId,
      question_id : data.questionId
    }
  });
  if ( question.type === 0 ) {    //객관식
    const { studentScore, studentRatio } = this.match(question, data.answers);  
    student.score = studentScore;
    student.ratio = studentRatio;
    student.answer = data.answers.join(config.separator);
    student.student_local_time = data.localTime;
    student.absolute_reactivity = data.absolute_reactivity;
  }
  else if ( question.type === 1 ) {   //주관식(단답)
    const { studentScore, studentRatio } = this.match1(question, data.answers);  
    student.score = studentScore;
    student.ratio = studentRatio;
    student.answer = data.answers.join(config.separator);
    student.student_local_time = data.localTime;
    student.absolute_reactivity = data.absolute_reactivity;
  }
  else if ( question.type === 2 ) {   //주관식(서술)
    student.answer = data.answers.join(config.separator);
    student.student_local_time = data.localTime;
    student.absolute_reactivity = data.absolute_reactivity;
  }
  else if ( question.type === 3) {    //SW
    student.answer = data.answers[0];
    student.student_local_time = data.localTime;
    student.score = null;
    student.ratio = null;
    student.absolute_reactivity = data.absolute_reactivity;
    await models.oj_solution.create({
      student_answer_log_id: student.student_answer_log_id,
      problem_id: student.question_id,
      user_id: student.student_id,
      language: codeLanguage(data.codeLanguage[0]),
      ip: '1.1.1.1',
      judged: 0,
    });
  } 
  else if ( question.type === 4 ) {   // SQL

    student.score = 0;
    student.ratio = 0;
    student.student_local_time = data.localTime;
    student.absolute_reactivity = data.absolute_reactivity;

    const sqlJudgeResult = await sqlJudge.judge(question.answer.replace(';', ''), data.answers[0].replace(';', ''), question.sql_lite_file[0].static_path);
    const resultSqlOut = JSON.stringify(sqlJudgeResult.result_value);

    if (sqlJudgeResult.result === 0) {
      await models.feedback.update({
        sql_out: resultSqlOut,
        sql_result_code: sqlJudgeResult.result
      }, {
        where : {
        student_answer_log_id: student.student_answer_log_id,
      }});
      student.score = question.score;
      student.ratio = 1.0;
    } else if (sqlJudgeResult.result === 1) {
      await models.feedback.update({
        sql_out: resultSqlOut,
        sql_result_code: sqlJudgeResult.result,
        sql_error_message: sqlJudgeResult.error
      }, { 
        where : {
        student_answer_log_id: student.student_answer_log_id,
      }});
    } else if (sqlJudgeResult.result === 2) {
      await models.feedback.update({
        sql_result_code: sqlJudgeResult.result,
        sql_error_message: sqlJudgeResult.error
      },{ where : {
        student_answer_log_id: student.student_answer_log_id,
      }});
    } else {
      throw errorBuilder.default('There are some problems with result code', 403, true);
    }
  }
  else if ( question.type === 7 ) {   //연결형
    const { studentScore, studentRatio } = this.match7(question, data.answers);  
    student.score = studentScore;
    student.ratio = studentRatio;
    student.absolute_reactivity = data.absolute_reactivity;
    student.answer = data.answers.join(config.separator);
    student.student_local_time = data.localTime;
  }
  student.count += 1;
  // 여기로 finalscore 계산 이동
  student.finalScore = student.score * (student.absolute_reactivity || 1) * (student.count < 3 ? 1 : student.count < 4 ? 0.9 : student.count < 5 ? 0.85 : 0.8);
  await student.save();

  //2021-08-25 제출 할 때마다 fianlScore를 다시 계산한다.  
  // finalScore = 이해도(10) * 집중도 *  신뢰도(count가 1,2 = 1, count가 3 = 0.9, count 4 = 0.8, count 5이상 = 0.6)
  // concentration이 없으니까 지금 update가 안된다.
  //const sql = `UPDATE student_answer_logs SET finalScore=(student_answer_logs.score * student_answer_logs.concentration * if(student_answer_logs.count=1, 1, if(student_answer_logs.count=2, 1, if(student_answer_logs.count=3, 0.9, if(student_answer_logs.count=4, 0.8, 0.6))))) where student_id =${studentId} and question_id=${data.questionId}`;
  
  //2021-09-02 절대 반응도는 [유인], [무인]개인, [무인]단체 모두 계산하기 때문에 절대 반응도를 점수에 반영
  // const sql = `UPDATE student_answer_logs SET finalScore=(student_answer_logs.score * student_answer_logs.absolute_reactivity * if(student_answer_logs.count=1, 1, if(student_answer_logs.count=2, 1, if(student_answer_logs.count=3, 0.9, if(student_answer_logs.count=4, if(student_answer_logs.count > 4 ), 0.6))))) where student_id =${studentId} and question_id=${data.questionId}`;
  // console.log("sql: ", sql);
  // await db.getQueryResult(sql);
  
  return student;
}
exports.submitHandling = async (studentId, data) => {
  let sol = null;
  const question = await models.question.findByPk(data.questionId, {
    include: [{
      model: models.lecture_item,
      include: {
        model: models.lecture,
        include: {
          model: models.class,
        },
      },
    },
    ],
  });
  question.sql_lite_file = await db.getQueryResult(`SELECT * FROM files as f join questions as q on q.sqlite_file_guid=f.file_guid where q.question_id=${data.questionId}`);
  
  // 2021-08-25 학생이 [제출] 버튼을 누르면 우선 student_answer_logs에 값을 넣는다
  if (question.type === 0) { // 객관식, 단답형
    const { studentScore, studentRatio } = this.match(question, data.answers);  
    sol = await models.student_answer_log.create({
      student_id: studentId,
      class_id: question.lecture_item.lecture.class.class_id,
      lecture_id: question.lecture_item.lecture.lecture_id,
      item_id: question.lecture_item.lecture_item_id,
      question_id: question.question_id,
      answer: data.answers.join(config.separator),
      score: studentScore,
      ratio: studentRatio,
      interval: data.interval,
      order: question.lecture_item.order,
      count : 1,
      student_local_time: data.localTime,
      finalScore: studentScore * (data.absolute_reactivity || 1),
      absolute_reactivity: data.absolute_reactivity
    });
  } else if ( question.type === 1 ) {
    const { studentScore, studentRatio } = this.match1(question, data.answers);
    sol = await models.student_answer_log.create({
      student_id: studentId,
      class_id: question.lecture_item.lecture.class.class_id,
      lecture_id: question.lecture_item.lecture.lecture_id,
      item_id: question.lecture_item.lecture_item_id,
      question_id: question.question_id,
      answer: data.answers.join(config.separator),
      score: studentScore,
      ratio: studentRatio,
      interval: data.interval,
      order: question.lecture_item.order,
      count : 1,
      student_local_time: data.localTime,
      finalScore: studentScore * (data.absolute_reactivity || 1),
      absolute_reactivity: data.absolute_reactivity
    });
  } else if (question.type === 2) { // 서술형
    sol = await models.student_answer_log.create({
      student_id: studentId,
      class_id: question.lecture_item.lecture.class.class_id,
      lecture_id: question.lecture_item.lecture.lecture_id,
      item_id: question.lecture_item.lecture_item_id,
      question_id: question.question_id,
      answer: data.answers.join(config.separator),
      interval: data.interval,
      order: question.lecture_item.order,
      count : 1,
      student_local_time: data.localTime,      
      absolute_reactivity: data.absolute_reactivity
    });
  } else if (question.type === 3) { // 코딩 문제
    sol = await models.student_answer_log.create({
      student_id: studentId,
      class_id: question.lecture_item.lecture.class.class_id,
      lecture_id: question.lecture_item.lecture.lecture_id,
      item_id: question.lecture_item.lecture_item_id,
      question_id: question.question_id,
      answer: data.answers[0],
      interval: data.interval,
      order: question.lecture_item.order,
      count : 1,
      student_local_time: data.localTime,
      absolute_reactivity: data.absolute_reactivity
    });
    await models.oj_solution.create({
      student_answer_log_id: sol.student_answer_log_id,
      problem_id: sol.question_id,
      user_id: sol.student_id,
      language: codeLanguage(data.codeLanguage[0]),
      ip: '1.1.1.1',
    });
  } else if (question.type === 4) { // sql
    sol = await models.student_answer_log.create({
      student_id: studentId,
      class_id: question.lecture_item.lecture.class.class_id,
      lecture_id: question.lecture_item.lecture.lecture_id,
      item_id: question.lecture_item.lecture_item_id,
      question_id: question.question_id,
      answer: data.answers[0],
      interval: data.interval,
      score: 0,
      ratio: 0,
      order: question.lecture_item.order,
      count: 1,
      student_local_time: data.localTime,
      finalScore: 0,
      absolute_reactivity: data.absolute_reactivity
    });

    const sqlJudgeResult = await sqlJudge.judge(question.answer.replace(';', ''), data.answers[0].replace(';', ''), question.sql_lite_file[0].static_path);
    const resultSqlOut = JSON.stringify(sqlJudgeResult.result_value);

    if (sqlJudgeResult.result === 0) {
      await models.feedback.create({
        sql_out: resultSqlOut,
        sql_result_code: sqlJudgeResult.result,
        student_answer_log_id: sol.student_answer_log_id,
      });
      sol.score = question.score;
      sol.ratio = 1.0;
      await sol.save();
    } else if (sqlJudgeResult.result === 1) {
      await models.feedback.create({
        sql_out: resultSqlOut,
        sql_result_code: sqlJudgeResult.result,
        sql_error_message: sqlJudgeResult.error,
        student_answer_log_id: sol.student_answer_log_id,
      });
    } else if (sqlJudgeResult.result === 2) {
      await models.feedback.create({
        sql_result_code: sqlJudgeResult.result,
        sql_error_message: sqlJudgeResult.error,
        student_answer_log_id: sol.student_answer_log_id,
      });
    } else {
      throw errorBuilder.default('There are some problems with result code', 403, true);
    }
  } else if (question.type === 7) {
    const { studentScore, studentRatio } = this.match7(question, data.answers);  
    sol = await models.student_answer_log.create({
      student_id: studentId,
      class_id: question.lecture_item.lecture.class.class_id,
      lecture_id: question.lecture_item.lecture.lecture_id,
      item_id: question.lecture_item.lecture_item_id,
      question_id: question.question_id,
      answer: data.answers.join(config.separator),
      score: studentScore,
      ratio: studentRatio,
      interval: data.interval,
      order: question.lecture_item.order,
      count : 1,
      student_local_time: data.localTime,
      finalScore: studentScore * (data.absolute_reactivity || 1),
      absolute_reactivity: data.absolute_reactivity
    });
  } else {
    sol = await models.student_answer_log.create({
      student_id: studentId,
      class_id: question.lecture_item.lecture.class.class_id,
      lecture_id: question.lecture_item.lecture.lecture_id,
      item_id: question.lecture_item.lecture_item_id,
      question_id: question.question_id,
      answer: data.answers.join(config.separator),
      interval: data.interval,
      order: question.lecture_item.order,
      count: 1,
      student_local_time: data.localTime,
      absolute_reactivity: data.absolute_reactivity
    });
  }
  return sol;
};

exports.ordered = async (studentId, lectureId, order) => {
  const keywordSql = `insert into student_lecture_ordered_keywords 
select ${studentId}, ${lectureId}, t.keyword, ${order}, (t.r/t.s)*100 from 
(select sum(score_portion) as s, sum(score_portion*ratio) as r,
a.keyword as keyword from question_keywords as a
join student_answer_logs as b on a.question_id=b.question_id and b.valid=1
and b.student_id=${studentId} and b.lecture_id=${lectureId} and b.\`order\`=${order}
and b.ratio is not null
group by a.lecture_id, a.keyword, b.\`order\`) as t
on duplicate key update understanding=(t.r/t.s)*100`;
  const avgSql = `insert into student_lecture_logs
(user_id, lecture_id, \`order\`, understanding_score)
select ${studentId}, ${lectureId}, ${order}, t.v from 
(select sum(c.weight_ratio*b.understanding) as v
from student_lecture_ordered_keywords as b
join lecture_keywords as c on b.lecture_id=c.lecture_id
and b.keyword=c.keyword and b.student_id=${studentId} and b.lecture_id=${lectureId}
and b.\`order\`=${order}
group by b.lecture_id) as t
on duplicate key update understanding_score=t.v`;

  await db.getQueryResult(keywordSql);
  await db.getQueryResult(avgSql);
};

// exports.ordered = async (studentId, lectureId, order) => {
//   const keywordSql = `insert into student_lecture_ordered_keywords
// select ${studentId}, ${lectureId}, t.keyword, ${order}, t.r/t.s from
// (select sum(score_portion) as s, sum(score_portion*ratio) as r,
// a.keyword as keyword from question_keywords as a
// join student_answer_logs as b on a.question_id=b.question_id and b.valid=1
// and b.student_id=${studentId} and b.lecture_id=${lectureId} and b.\`order\`=${order}
// group by a.lecture_id, a.keyword, b.\`order\`) as t
// on duplicate key update understanding=t.r/t.s`;
//   const avgSql = `insert into student_lecture_ordered_logs
// (student_id, lecture_id, \`order\`, avg_understanding)
// select ${studentId}, ${lectureId}, ${order}, t.v from
// (select sum(c.weight_ratio*b.understanding) as v
// from student_lecture_ordered_keywords as b
// join lecture_keywords as c on b.lecture_id=c.lecture_id
// and b.keyword=c.keyword and b.student_id=${studentId} and b.lecture_id=${lectureId}
// and b.\`order\`=${order}
// group by b.lecture_id) as t
// on duplicate key update avg_understanding=t.v`;
//
//   await db.getQueryResult(keywordSql);
//   await db.getQueryResult(avgSql);
// };


exports.lecture = async (studentId, lectureId) => {
  const keywordSql = `insert into student_lecture_keywords 
select ${studentId}, ${lectureId}, t.keyword, (t.r/t.s)*100 from 
(select sum(score_portion) as s, sum(score_portion*ratio) as r,
a.keyword as keyword from question_keywords as a
join student_answer_logs as b on a.question_id=b.question_id and b.valid=1
and b.student_id=${studentId} and b.lecture_id=${lectureId}
group by a.lecture_id, a.keyword) as t
on duplicate key update understanding=(t.r/t.s)*100`;
  const avgSql = `insert into student_lecture_logs
(user_id, lecture_id, understanding_score) 
select ${studentId}, ${lectureId}, t.v from 
(select sum(c.weight_ratio*b.understanding) as v
from student_lecture_keywords as b
join lecture_keywords as c on b.lecture_id=c.lecture_id
and b.keyword=c.keyword and b.student_id=${studentId} and b.lecture_id=${lectureId}
group by b.lecture_id) as t
on duplicate key update understanding_score=t.v`;

  await db.getQueryResult(keywordSql);
  await db.getQueryResult(avgSql);
};

exports.class = async (studentId, classId) => {
  const keywordSql = `insert into student_class_keywords 
select ${studentId}, ${classId}, t.keyword, (t.r/t.s)*100 from 
(select sum(score_portion) as s, sum(score_portion*ratio) as r,
a.keyword as keyword from question_keywords as a
join student_answer_logs as b on a.question_id=b.question_id and b.valid=1
and b.student_id=${studentId} and b.class_id=${classId} and b.ratio is not null
group by a.keyword) as t
on duplicate key update understanding=(t.r/t.s)*100`;
  const avgSql = `insert into student_class_understandings
select ${studentId}, ${classId}, t.v from 
(select sum(c.weight_ratio*b.understanding) as v
from student_class_keywords as b
join class_keywords as c on b.class_id=c.class_id
and b.keyword=c.keyword and b.student_id=${studentId} and b.class_id=${classId}
group by b.class_id) as t
on duplicate key update avg_understanding=t.v`;

  await db.getQueryResult(keywordSql);
  await db.getQueryResult(avgSql);
};
