const fs = require('fs');
// const path = require('path');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
// const sequelize = require('sequelize');
const db = require('./db');
const { exec, spawn } = require('child_process');
exports.db = require('./db');
exports.models = require('../models');
exports.auth = require('./auth');
exports.asyncWrapper = require('./asyncWrapper');
exports.errorBuilder = require('./error-builder');
exports.config = require('../config');
exports.upload = require('./upload');
exports.delete = require('./delete');
exports.coverageCalculator = require('./coverage-calculator');
exports.cloneObject = obj => JSON.parse(JSON.stringify(obj));
exports.faceRecognitionFlag = [];
exports.faceTimer = [];
exports.teacherInClass = async (teacherId, classId) => this.models.user_class.findOne({
  where: {
    user_id: teacherId,
    class_id: classId,
    role: 'teacher',
  },
});
exports.affAdminInClass = async (adminId, authType, classId) => {
  if (authType !== 4) {
    return false;
  } else {
    const temp = await this.models.user_belonging.findOne({
      attributes: ['affiliation_id'],
      where: {
        user_id: adminId,
      },
    });

    const temp2 = (await this.models.req_create_class.findAll({
      attributes: ['class_id'],
      where: {
        affiliation_id: temp.affiliation_id,
      },
    })).map(data => data.dataValues.class_id).filter(class_id => class_id);

    return temp2.includes(classId);
  }
}
exports.studentInClass = async (studentId, classId) => this.models.user_class.findOne({
  where: {
    user_id: studentId,
    class_id: classId,
    role: 'student',
  },
});
exports.computeReliability = count => {
  if (count < 3) {
    return 1;
  } else if (count < 4) {
    return 0.9;
  } else if (count < 5) {
    return 0.85;
  } else {
    return 0.8;
  }
};

exports.tokenStatisticsPython = tokens => new Promise(async (resolve, reject) => {
	const NUM_TOKENS = tokens.length;
	const BUILT_IN_FUNCTION = ['abs', 'all', 'any', 'ascii', 'bin', 'bool',
		'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod',
		'compile', 'complex', 'delattr', 'dict', 'dir', 'divmod', 'enumerate',
		'eval', 'exec', 'filter', 'float', 'format', 'frozenset', 'getattr',
		'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int',
		'isinstance', 'issubclass', 'iter', 'len', 'list', 'locals', 'map',
		'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord',
		'pow', 'print', 'property', 'range', 'repr', 'reversed', 'round',
		'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum',
		'super', 'tuple', 'type', 'lets', 'zip', '__import__'];
	const OPERATORS = ['PLUS', 'MINUS', 'STAR', 'SLASH', 'PERCENT', 'DOUBLESTAR',
		'DOUBLESLASH', 'PLUSEQUAL', 'MINEQUAL', 'STAREQUAL', 'SLASHEQUAL', 'PERCENTEQUAL',
		'DOUBLESTAREQUAL', 'DOUBLESLASHEQUAL'];
	let func_container = [];
	let let_container = [];
	let int_container = [];
	let double_container = [];
	let string_container = [];

	let num_loop_total = 0;
	let num_loop_upper = 0;
	let num_loop_lower = 0;
	let num_branch_total = 0;
	let num_branch_upper = 0;
	let num_branch_lower = 0;
	let num_function_declared = 0;
	let num_function_used = 0;
	let num_var_declared = 0;
	let num_var_used = 0;
	let num_array_declared = 0;
	let num_int_declared = 0;
	let num_int_used = 0;
	let num_double_declared = 0;
	let num_double_used = 0;
	let num_string_declared = 0;
	let num_string_used = 0;
	let num_operator = 0;

  let num_assignment = 0;
  let num_comparision_operator = 0;
  let num_class = 0;

	let line_per_loop_avg = 0; // line_per_loop_sum / num_outer_loop
	let line_per_loop_sum = 0;
	let num_outer_loop = 0;

	let line_per_branch_avg = 0; // line_per_branch_sum / num_outer_branch
	let line_per_branch_sum = 0;
	let num_outer_branch = 0;

	let line_per_function_avg = 0; // line_per_function_sum / num_function_declared
	let line_per_function_sum = 0;

	// set letiables related to info of functions(def)
	let isFuncSet = false;
	let isIndentSet = false;
	let count_indent_and_dedent = 0;
	for (let i = 0; i < NUM_TOKENS; i++) {
    if (tokens[i].type === 'EQUAL') {
      num_assignment++;
    }

    if (tokens[i].type === 'EQEQUAL') {
      num_comparision_operator++;
    }

    if (tokens[i].type === 'KEYWORD' && tokens[i].string === 'class') {
      num_class++;
    }

		if (tokens[i].type === 'KEYWORD' && tokens[i].string === 'def') {
			if (!isFuncSet) { // prevention of counting nested function declaration
				num_function_declared++;
				if (i + 1 < NUM_TOKENS) {
					if (tokens[i + 1].type === 'ID') {
						func_container.push(tokens[i + 1].string);
					}
				}
			}
			isFuncSet = true;
		}
		if (isFuncSet) {
			if (tokens[i].type === 'INDENT') {
				isIndentSet = true;
				count_indent_and_dedent++;
			} else if (tokens[i].type === 'DEDENT') {
				count_indent_and_dedent--;
			} else if (isIndentSet && tokens[i].type === 'NEWLINE') {
				line_per_function_sum++;
			}
		}
		if (isIndentSet && count_indent_and_dedent <= 0) {
			isIndentSet = false;
			isFuncSet = false;
		}
	}
	if (num_function_declared > 0) {
		line_per_function_avg = line_per_function_sum / num_function_declared;
	}

	// token analyzing
	let isLoopSet = false;
	let isBranchSet = false;
	let count_indent_and_dedent_for_loop = 0;
	let isIndentSetLoop = false;
	let count_indent_and_dedent_for_branch = 0;
	let isIndentSetBranch = false;
	for (let i = 0; i < NUM_TOKENS; i++) {
		//check loop
		if (tokens[i].type === 'KEYWORD' && (tokens[i].string === 'for' || tokens[i].string === 'while')) {
			if (i < NUM_TOKENS / 2) {
				num_loop_upper++;
			} else {
				num_loop_lower++;
			}
			if (!isLoopSet) { // prevention of counting nested loop for num_outer_loop
				num_outer_loop++;
			}
			isLoopSet = true;
		}
		if (isLoopSet) {
			if (tokens[i].type === 'INDENT') {
				isIndentSetLoop = true;
				count_indent_and_dedent_for_loop++;
			} else if (tokens[i].type === 'DEDENT') {
				count_indent_and_dedent_for_loop--;
			} else if (isIndentSetLoop && tokens[i].type === 'NEWLINE') {
				line_per_loop_sum++;
			}
		}
		if (isIndentSetLoop && count_indent_and_dedent_for_loop <= 0) {
			isIndentSetLoop = false;
			isLoopSet = false;
			count_indent_and_dedent_for_loop = 0;
		}

		//check branch
		if (tokens[i].type === 'KEYWORD' &&
			(tokens[i].string === 'if' || tokens[i].string === 'elif' || tokens[i].string === 'else')) {
			if (i < NUM_TOKENS / 2) {
				num_branch_upper++;
			} else {
				num_branch_lower++;
			}
			if (!isBranchSet) { // prevention of counting nested branch for num_outer_branch
				num_outer_branch++;
			}
			isBranchSet = true;
		}
		if (isBranchSet) {
			if (tokens[i].type === 'INDENT') {
				isIndentSetBranch = true;
				count_indent_and_dedent_for_branch++;
			} else if (tokens[i].type === 'DEDENT') {
				count_indent_and_dedent_for_branch--;
			} else if (isIndentSetBranch && tokens[i].type === 'NEWLINE') {
				line_per_branch_sum++;
			}
		}
		if (isIndentSetBranch && count_indent_and_dedent_for_branch <= 0) {
			isIndentSetBranch = false;
			isBranchSet = false;
			count_indent_and_dedent_for_branch = 0;
		}

		//check function_used
		if (tokens[i].type === 'ID' && // type ID
			!BUILT_IN_FUNCTION.includes(tokens[i].string) && // not built in function
			i + 1 < NUM_TOKENS && tokens[i + 1].type === 'LPAR' && // combination: ID LPAR
			tokens[i - 1].string !== 'def' && // not declaration
			func_container.includes(tokens[i].string)) { // declared function
			num_function_used++;
		}

		// check let declared and used
		if (tokens[i].type === 'ID' &&
			!BUILT_IN_FUNCTION.includes(tokens[i].string) &&
			!func_container.includes(tokens[i].string)) {
			if (let_container.includes(tokens[i].string)) { // already declared
				num_var_used++;
				if (int_container.includes(tokens[i].string)) {
					num_int_used++;
				} else if (double_container.includes(tokens[i].string)) {
					num_double_used++;
				} else if (string_container.includes(tokens[i].string)) {
					num_string_used++;
				}
			} else {
				if (i + 2 < NUM_TOKENS && tokens[i + 1].type === 'EQUAL') { // ID EQUAL combination
					num_var_declared++;
					let_container.push(tokens[i].string);
					if (tokens[i + 2].type === 'NUMBER') { // ID EQUAL NUMBER combi
						if (tokens[i + 2].string.indexOf(".") == -1) {
							num_int_declared++;
							int_container.push(tokens[i].string);
						} else {
							num_double_declared++;
							double_container.push(tokens[i].string);
						}
					} else if (tokens[i + 2].type === 'STRING') { // ID EQUAL STRING combi
						num_string_declared++;
						string_container.push(tokens[i].string);
					} else if (tokens[i + 2].type === 'LSQB' || tokens[i + 2].string === 'array' || tokens[i + 2].string === 'list') { // ID EQUAL {LSQB|array|list} combi
						num_array_declared++;
					}
				}
			}
		}

		if (OPERATORS.includes(tokens[i].type)) {
			if (i + 1 < NUM_TOKENS &&
				(tokens[i + 1].type === 'NUMBER' || func_container.includes(tokens[i + 1].string) || (tokens[i].type === 'ID' && (int_container.includes(tokens[i + 1].string) || double_container.includes(tokens[i + 1].string)))) &&
				(tokens[i - 1].type === 'NUMBER' || tokens[i - 1].type === 'RPAR' || (tokens[i].type === 'ID' && (int_container.includes(tokens[i - 1].string) || double_container.includes(tokens[i - 1].string)))))
				num_operator++;
		}
	}
	num_loop_total = num_loop_upper + num_loop_lower;
	num_branch_total = num_branch_upper + num_branch_lower;
	if (num_outer_loop > 0)
		line_per_loop_avg = line_per_loop_sum / num_outer_loop;
	if (num_outer_branch > 0)
		line_per_branch_avg = line_per_branch_sum / num_outer_branch;
	resolve({
		num_loop_total,
		// num_loop_upper,
		// num_loop_lower,
		num_branch_total,
		// num_branch_upper,
		// num_branch_lower,
		num_function_declared,
		num_function_used,
		num_var_declared,
		num_var_used,
		// num_array_declared,
		// num_int_declared,
		// num_int_used,
		// num_double_declared,
		// num_double_used,
		// num_string_declared,
		// num_string_used,
		// line_per_loop_avg,
		// line_per_branch_avg,
		// line_per_function_avg,
		num_operator,
    num_assignment,
    num_comparision_operator,
    num_class
	});
});

exports.tokenizePython = async code => new Promise(async resolve => {
  const python3Command = await this.python3Command();
  const tokenizeProcess = spawn(python3Command.cmd, [`${this.rootPath}/scripts/python/lex/lex.py`]);
  tokenizeProcess.stdout.on('data', data => {
      let jsonParsed;
      let error = false;
      try {
          jsonParsed = JSON.parse(data.toString());
      }   catch (err) {
          error = err;
      }
      if (!error) {
          resolve(jsonParsed.map(token => {
              token['string'] = token['type'] === 'STRING' ? token['string'].slice(1,-1) : token['string'];
              return token;
          }));
      } else {
          resolve(`err: ${error}`);
      }
  });
  tokenizeProcess.stderr.on('data', data => {
      resolve(`stderr: ${data}`);
  });
  tokenizeProcess.stdin.write(code);
  tokenizeProcess.stdin.end();
});

exports.astCodePython = code => new Promise(async resolve => {
  const python3Command = await this.python3Command();
  const pythonProcess = spawn(python3Command.cmd, [`${this.rootPath}/scripts/python/ast/ast_code.py`]);
  let output = [];
  pythonProcess.stdout.on('data',  data => {
    output = (data.toString()).split(',');
  });

  pythonProcess.stderr.on('data', data => {
    resolve({
      success: 'false',
      err: data,
    });
  });

  pythonProcess.on('exit', code => {
    resolve({
      ast_node_number: Number(output[0]),
      // ast_fanout_min: Number(output[1]),
      // ast_fanout_max: Number(output[2]),
      // ast_fanout_avg: Number(output[3]),
      // ast_field_min: Number(output[4]),
      // ast_field_max: Number(output[5]),
      // ast_field_avg: Number(output[6]),
      ast_depth: Number(output[7]),
    });
  });
  
  pythonProcess.stdin.write(code);
  pythonProcess.stdin.end();
});

exports.modelAttributeList = (model, includeAutoCreate = false) => Object.entries(model.attributes).filter(attribute => includeAutoCreate ? true : !(attribute[1]._autoGenerated)).map(attribute => attribute[0]);
// root directory path
exports.rootPath = '';
exports.python3Command = () => new Promise(async resolve => {
  exec('python -V', (err, stdout, stderr) => {
    if (err) {
      resolve({
        success: false,
        err,
        stderr
      });
    }
    const regex = /3.[0-9]+.[0-9]+/;
    const version = stdout.split(' ')[1];
    if (regex.test(version)) {
      resolve({
        success: true,
        cmd: 'python'
      });
    } else {
      exec('python3 -V', (err, stdout, stderr) => {
        if (err) {
          resolve({
            success: false,
            err,
            stderr
          });
        }
        const regex = /3.[0-9]+.[0-9]+/;
        const version = stdout.split(' ')[1];
        if (regex.test(version)) {
          resolve({
            success: true,
            cmd: 'python3'
          });
        }
      });
    }
  });
});

// exports.scoringSoftware = (question_id, user_id, language, code, testCases) => new Promise(async resolve => {
//   let filePath = null;

//   if (language === 'python3') {
//     filePath = `${this.rootPath}/testCodes/python3/${user_id}_${Date.now()}.py`;
//   }
//   if (filePath) {
//     fs.writeFileSync(filePath, code);
//     // python command version check
//     const pythonCommand = await this.python3Command();

//     if (pythonCommand.success === false) {
//       throw 'python command error';
//     } else {
//       // run code
//       const outputs = await Promise.all(testCases.map(testCase => new Promise((resolve) => {
//         let startTime;
//         const pythonProcess = spawn(pythonCommand.cmd, [filePath]);
//         const output = [];

//         pythonProcess.stdout.on('data',  data => {
//           output.push(data.toString());
//         });
//         pythonProcess.stderr.on('data', data => {
//           resolve({
//             success: false,
//             score_ratio: testCase.score_ratio,
//             input: testCase.input,
//             testcase_id: testCase.testcase_id,
//             expected_output: testCase.output,
//             err: data.toString(),
//             time: Date.now() - startTime,
//             memory: undefined,
//             time_limit: testCase.timeout,
//             memory_limit: testCase.memory,
//           });
//         });
//         pythonProcess.on('exit', code => {
//           const outputParsed = output.join('').replace('\r\n', '\n');
//           resolve({
//             success: true,
//             score_ratio: testCase.score_ratio,
//             input: testCase.input,
//             testcase_id: testCase.testcase_id,
//             expected_output: testCase.output,
//             output: outputParsed,
//             time: Date.now() - startTime,
//             memory: undefined,
//             time_limit: testCase.timeout,
//             memory_limit: testCase.memory,
//           });
//         });

//         startTime = Date.now();
//         pythonProcess.stdin.write(testCase.input);
//         pythonProcess.stdin.end();
//       })));

//       // before finish running code remove python file
//       try {
//         fs.unlinkSync(filePath);
//       } catch (err) {
//         console.log(err);
//       }

//       const results = outputs.map(output => {
//         const isTimeout = output.time > output.time_limit;
//         const isMemoryOver = output.memory ? (output.memory > output.memory_limit) : false;
//         const outputCheck = (output.success === true ? null : (output.err ? `Error: ${output.err}` : `Incorrect output`));
//         const comment = outputCheck === null ? (isTimeout ? 'Timeout' : isMemoryOver ? 'Memory over' : '') : outputCheck;
//         return {
//           question_id,
//           user_id,
//           testcase_id: output.testcase_id,
//           input: output.input,
//           output: output.output,
//           expected_output: output.expected_output,
//           correct: (comment === ''),
//           score_ratio: output.score_ratio,
//           time: output.time,
//           memory: output.memory,
//           time_limit: output.time_limit,
//           memory_limit: output.memory_limit,
//           comment,
//         };
//       });

//       const ratio = results.reduce((acc, cur) => ({ score_ratio: (acc.correct ? acc.score_ratio : 0) + (cur.correct ? cur.score_ratio : 0), correct: true })).score_ratio
//       / outputs.reduce((acc, cur) => ({ score_ratio: acc.score_ratio + cur.score_ratio })).score_ratio;

//       await Promise.all(results.map(result => this.models.problem_judge_log.create({
//         ...result,
//         score_ratio: undefined, // 제거용도
//       })));

//       resolve({
//         success: true,
//         ratio
//       });
//     }
//   } else {
//     resolve({
//       success: false,
//     })
//   }
// });

exports.filePathMakeForDuplicates = (filePath, index) => {
  if (fs.existsSync(`${filePath}_${process.pid}_${index}`)) {
    return this.filePathMakeForDuplicates(filePath, index + 1);
  }
  return `${filePath}_${process.pid}_${index}`;
}

exports.saveFile = (filePath, data) => new Promise(resolve => {
  fs.writeFile(filePath, data, async err => {
    if (fs.existsSync(filePath)) {
      resolve();
    } else {
      await this.saveFile(filePath, data);
    }
  });
});

exports.runCode = (language, code, input, memoryLimit = 0, timeLimit = 0) => new Promise(async resolve => {
  let filePath = null;
  if (language.toLowerCase() === 'python3') {
    filePath = `${this.filePathMakeForDuplicates(`${this.rootPath}/testCodes/python3/${Date.now()}`, 0)}.py`;
  }

  if (filePath === null) {
    resolve({
      success: false,
      output: 'invalid language'
    });
  }

  await this.saveFile(filePath, code);

  const { success, output, timeout, time } = await new Promise(async resolve => {
    const python3Command = await this.python3Command();
    const pythonProcess = spawn(python3Command.cmd, [filePath]);
    const outputArray = [];
    let startTime;

    pythonProcess.stdout.on('data',  data => {
      outputArray.push(data.toString());
    });

    pythonProcess.stderr.on('data', data => {
      resolve({
        success: false,
        output: data.toString(),
      });
    });

    pythonProcess.on('exit', exitCode => {
      const time = startTime ? Date.now() - startTime : 0;
      const timeout = time > timeLimit;
      resolve({
        timeout,
        time,
        success: exitCode === 0,
        output: outputArray.join('').replace('\r\n', '\n'),
      });
    });

    pythonProcess.stdin.write(input);
    pythonProcess.stdin.end();
    if (timeLimit > 0) {
      startTime = Date.now();
      setTimeout(() => {
        pythonProcess.kill('SIGINT');
      }, 10 * timeLimit);
    }
  });

  let unlink = undefined;
  if (fs.existsSync(filePath)) {
    try {
      fs.unlinkSync(filePath);
    } catch (err) {
      unlink = err;
    }
  }

  resolve({
    success,
    output,
    timeout,
    timeLimit,
    time
  });

});

exports.writeFilePromise = async (path, content) => new Promise((resolve, reject) => {
  fs.writeFile(path, content, (err) => {
    if (err) return reject(err);
    return resolve(true);
  });
});

exports.sign_v4 = async (user, time = 14) => new Promise((resolve, reject) => {
  jwt.sign(
    {
      authId: user.user_id,
      email_id: user.email_id,
      name: user.name,
      emailVerified: user.email_verified,
      authType: user.type,
    },
    this.config.jwtSecret,
    {
      expiresIn: '1h',
      issuer: 'dblab',
      subject: 'userInfo',
    }, (err, token) => {
      if (err) return reject(err);
      const accessToken = this.encrypt_aes252cbc(token, this.config.jwtSecret, 'base64');
      const refreshToken = this.encrypt_aes252cbc(JSON.stringify({
        authId: user.user_id,
        email_id: user.email_id,
        name: user.name,
        emailVerified: user.email_verified,
        authType: user.type,
        expires: Date.now() + 1000 * 60 * 60 * time
      }), this.config.jwtSecret, 'utf-8');
      return resolve({ accessToken, refreshToken });
    },
  );
});

exports.encrypt_aes252cbc = (text, secretKey, type) => {
  const cipher = crypto.createCipheriv('aes-256-cbc', secretKey);
  let result = cipher.update(text, type, 'base64');
  return result + cipher.final('base64'); 
}

exports.decrypt_aes252cbc = (cryptogram, secretKey, type) => {
  const decipher = crypto.createDecipheriv('aes-256-cbc', secretKey);
  let result = decipher.update(cryptogram, 'base64', type);
  return result + decipher.final(type);
}

exports.sign = async (user, permanent = false) => new Promise((resolve, reject) => {
  jwt.sign(
    {
      authId: user.user_id,
      email_id: user.email_id,
      name: user.name,
      emailVerified: user.email_verified,
      authType: user.type,
    },
    this.config.jwtSecret,
    {
      expiresIn: permanent ? '123456789h' : '14h',
      issuer: 'dblab',
      subject: 'userInfo',
    }, (err, token) => {
      if (err) return reject(err);
      return resolve(token);
    },
  );
});
exports.login = async (ip, token, userId) => {
  const log = await this.models.test_login.create({
    ip, token, userId,
  });
  const sql = `UPDATE test_logins SET expired = DATE_ADD(expired, INTERVAL 14 HOUR) WHERE id = ${log.id}`;
  await db.getQueryResult(sql);
  await this.models.login_log.create({
    ip, userId,
  });
};

exports.addFiles = async (fileGuid, idField, id) => {
  const promises = [];
  const fileLen = fileGuid.length;
  for (let i = 0; i < fileLen; i += 1) {
    promises.push(this.models.file.update(
      { [idField]: id },
      { where: { file_guid: fileGuid[i] } },
    ));
  }
  await Promise.all(promises);
};

exports.toSnakeCase = str => str.split(/(?=[A-Z])/).join('_').toLowerCase();

exports.teacherInGroup = async (teacherId, groupId) => this.models.teacher_group.findOne({
  where: {
    user_id: teacherId,
    group_id: groupId,
  },
});

exports.updateOrCreate = async (model, where, newItem) => {
  // First try to find the record
  const foundItem = await model.findOne({ where });
  if (!foundItem) {
    // Item not found, create a new one
    const item = await model.create(newItem);
    return { item, created: true };
  }
  // Found an item, update it
  // const item = await model.update(newItem, { where });
  // return { item, created: false };

  await model.update(newItem, { where });
  return { item: foundItem, created: false };
};
