const time_handler = require('./time-handler')
exports.addArrayZscore = function(arr,column_name) {
    var n = arr.length;
    var sum = 0;

    arr.map(function(data) {
        sum+=data[column_name];
    });

    var mean = sum / n;

    var variance = 0.0;
    var v1 = 0.0;
    var v2 = 0.0;

    if (n != 1) {
        for (var i = 0; i<n; i++) {
            v1 = v1 + (arr[i][column_name] - mean) * (arr[i][column_name] - mean);
            v2 = v2 + (arr[i][column_name] - mean);
        }

        v2 = v2 * v2 / n;
        variance = (v1 - v2) / (n-1);
		if(variance ==0) variance = 0.1;
        stddev = Math.sqrt(variance);
		for(var i = 0; i < n; i++){
			console.log(mean,stddev,arr[i][column_name])
		arr[i].z_score = Math.abs(arr[i][column_name] - mean) / stddev
		}
    }
	else
	{
		for(var i = 0; i < n; i++){
		arr[i].z_score = 100;
		}
	}


    return arr;
};
exports.getDateDifference = function (a, b) {
	const date_a = new Date(a);
	const date_b = new Date(b);

	return (date_b.getTime() - date_a.getTime()) / 1000;
}
exports.divideNotZero = function(a,b)
{
	if(b == 0)
		return 0
	else
		return a/b;
}
exports.IsJsonString = function(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
exports.divideNotZeroReturnOne = function(a,b)
{
  if(a == 0)
	return 0;
  if(b == 0)
    return 1;
  else
    return a/b;
}
/*
mode 가 1이면 로그 데이터의 time 컬럼이 한국시간으로 날라오기때문에 UTC 시간으로 변환해야함.
*/
exports.csv_string_to_json = function (column_array,csvstr, mode)
{
	
  var ret_array = []
 
  var rows = csvstr.split('\n');
  for(var i=0;i<rows.length;i++)
  {
    var one_row = rows[i]
    var one_row_column = one_row.split(',')
    if(one_row_column.length!=column_array.length)
    {
      continue;
    }
    ret_json = {};
	var ret_flag = true;
    for(var j=0;j<column_array.length;j++)
    {
		if (column_array[j]=='time')
		{
			let td = new Date(one_row_column[j]);
			if(td.toDateString() == "Invalid Date")
			{
				ret_flag = false;
				break;
			}
			else
			{
				if (mode == 1)
				{
					td = time_handler.minusHours(td,9);
					ret_json[column_array[j]] = time_handler.toUTCMysqlFormat(td)
				}
				else
					ret_json[column_array[j]] = time_handler.toUTCMysqlFormat(td)
				
			}
				

		}
		else
			ret_json[column_array[j]] = one_row_column[j]
    }
	if (ret_flag)
		ret_array.push(ret_json)
  }
  return ret_array;
}
exports.keylogger_parsing = function(str)
{
	str = str.replace(/!/gi, "\n");
	str = str.replace(/%/gi, "\t");
	var errcnt = 0
	var max_errcnt = 1000
	while(1)
	{
		if(str.indexOf('$')==(-1) || errcnt > max_errcnt)
			break;
		var willberemoved = str.indexOf('#')
		str = str.slice(0,willberemoved) + str.slice(willberemoved+2,str.length)
		errcnt ++;
	}
	var errcnt = 0
	while(1)
	{
		if(str.indexOf('#')==(-1) || errcnt > max_errcnt)
			break;
		var willberemoved = str.indexOf('#')
		str = str.slice(0,willberemoved-1) + str.slice(willberemoved+1,str.length)
		errcnt ++;
	}
	while(1)
	{
		if(str.indexOf('\b')==(-1) || errcnt > max_errcnt)
			break;
		var willberemoved = str.indexOf('\b')
		str = str.slice(0,willberemoved-1) + str.slice(willberemoved+1,str.length)
		errcnt ++;
	}
	

	str = str.replace(/\?1/gi, "!"); 
	str = str.replace(/\?2/gi, "@"); 
	str = str.replace(/\?3/gi, "#"); 
	str = str.replace(/\?4/gi, "$"); 
	str = str.replace(/\?5/gi, "%"); 
	str = str.replace(/\?6/gi, "^"); 
	str = str.replace(/\?7/gi, "&"); 
	str = str.replace(/\?8/gi, "*"); 
	str = str.replace(/\?9/gi, "("); 
	str = str.replace(/\?0/gi, ")"); 
	str = str.replace(/\?/gi, ""); 
	str = str.replace(/[^a-zA-Z0-9 ]/g, '');
	console.log(str)
	return str;
}
exports.LCS = function (s1, s2) {
    var result = [];
    for (var i=0; i<=s1.length; i++) {
        result.push([]);
        for (var j=0; j<=s2.length; j++) {
            var currValue = 0;
            if (i==0 || j==0) {
                currValue = 0;
            } else if (s1.charAt(i-1) == s2.charAt(j-1)) {
                currValue = result[i-1][j-1] + 1;
            } else {
                currValue = Math.max(result[i][j-1], result[i-1][j]);
            }
            result[i].push(currValue);
        }
    }

    var i = s1.length;
    var j = s2.length;

    var s3 = '';
    while (result[i][j] > 0) {
        if(s1.charAt(i-1) == s2.charAt(j-1) && (result[i-1][j-1] +1 == result[i][j])) {
            s3 = s1.charAt(i-1) + s3;
            i = i-1;
            j = j-1;
        } else if (result[i-1][j] > result[i][j-1])
            i = i-1;
        else
            j = j-1;
    }
    return s3;
}
exports.updateOrCreate = function (model, where, newItem, onCreate, onUpdate, onError) {
  // First try to find the record
  model.findOne({where: where}).then(function (foundItem) {
    if (!foundItem) {
      // Item not found, create a new one
      model.create(newItem)
        .then(onCreate)
        .catch(onError);
    } else {
      // Found an item, update it
      model.update(newItem, {where: where})
        .then(onUpdate)
        .catch(onError);
      ;
    }
  }).catch(onError);
}
