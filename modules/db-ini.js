const path = require('path');
const fs = require('fs');
const db = require('./db');
const config = require('../config');


module.exports = async () => {
  let sql = fs.readFileSync(path.join(config.appRoot, 'init-sql/keyword_finder.sql'), 'utf8');
  await db.getQueryResult(`${sql} USE \`${config.db.database}\``);
  sql = fs.readFileSync(path.join(config.appRoot, 'init-sql/scoring.sql'), 'utf8');
  await db.getQueryResult(`${sql} USE \`${config.db.database}\``);
  sql = fs.readFileSync(path.join(config.appRoot, 'init-sql/word.sql'), 'utf8');
  await db.getQueryResult(`${sql} USE \`${config.db.database}\``);
};
