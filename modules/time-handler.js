function twoDigits(d) 
{
	if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

exports.toMysqlFormat = function (d) 
{
	return d.getFullYear() + "-" + twoDigits(1 + d.getMonth()) + "-" + twoDigits(d.getDate()) + " " + twoDigits(d.getHours()) + ":" + twoDigits(d.getMinutes()) + ":" + twoDigits(d.getSeconds());
}

exports.toUTCMysqlFormat = function (d) 
{
	return d.getUTCFullYear() + "-" + twoDigits(1 + d.getUTCMonth()) + "-" + twoDigits(d.getUTCDate()) + " " + twoDigits(d.getUTCHours()) + ":" + twoDigits(d.getUTCMinutes()) + ":" + twoDigits(d.getUTCSeconds());
}
exports.addHours = function(d,h) 
{    
   d.setTime(d.getTime() + (h*60*60*1000)); 
   return d;   
}
exports.minusHours = function(d,h) 
{    
   d.setTime(d.getTime() - (h*60*60*1000)); 
   return d;   
}