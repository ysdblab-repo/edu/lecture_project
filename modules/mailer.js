const nodemailer = require('nodemailer');
const config = require('../config');

const transporter = nodemailer.createTransport(config.email);

// exports.sendVerificationMail = (email, userId, rand) => new Promise((resolve, reject) => {
//   const url = `http://${config.host}:${config.port}/auth/verify/${userId}/${rand}`;
//   const mailOptions = {
//     from: `Do Not Reply${config.email.auth.user}`,
//     to: email,
//     subject: 'Please confirm your Email account',
//     html: `Hello,<br> Please Click on the link to verify your email.<br>
// <a href="${url}">Click here to verify</a>`,
//   };

//   transporter.sendMail(mailOptions, (err, info) => {
//     if (err) return reject(err);
//     return resolve(info);
//   });
// });

exports.sendFindPasswordMail = (email, userId, rand) => new Promise((resolve, reject) => {
  const url = `https://${config.frontAddress}/forgot-password/${rand}/${userId}`;
  const mailOption = {
    from: `Do Not Reply${config.email.auth.user}`,
    to: email,
    subject: '[Preswot] Finding your password',
    html: `<a href="${url}">Click here to change your password</a>`,
  };

  transporter.sendMail(mailOption, (err, info) => {
    if (err) return reject(err);
    return resolve(info);
  });
  
});

exports.sendAuthMail = (email, rand) => new Promise((resolve, reject) => {
  const mailOption = {
    from: `Do Not Reply this mail`,
    to: email,
    subject: '[Bestswot] 메일 소유 인증',
    html: `인증번호를 전달드립니다.</a> <p>${rand}</p>`,
  };

  transporter.sendMail(mailOption, (err, info) => {
    if (err) return reject(err);
    return resolve(info);
  });
});
