const model = require('../models');
const { Queue } = require('../class/Queue');
const scoreingQueue = new Queue();
const doneQueue = new Queue();
let jobSize = 0;
const scoreingInterval = async () => {
  if (scoreingQueue.size() === 0 && doneQueue.size() === jobSize) {
    doneQueue.purge();
    let oj_solutions = await model.oj_solution.findAll({
      where: {
        judged: 0
      }
    });
    jobSize = oj_solutions.length;
    oj_solutions.forEach(oj_solution => {
      scoreingQueue.enqueue(oj_solution.solution_id);
    });
    setTimeout(scoreingInterval, 3000);
  } else {
    setTimeout(scoreingInterval, 1000);
  }
};

scoreingInterval();

exports.scoreingQueue = scoreingQueue;
exports.doneQueue = doneQueue;