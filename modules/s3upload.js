const AWS = require('aws-sdk');
const config = require('../config');
const modules = require('../modules/frequent-modules');
AWS.config.update({
  accessKeyId:  config.aws.accessKeyId,
  secretAccessKey: config.aws.secretAccessKey,
  region: 'ap-northeast-2'
})
async function uploadFile(type, param, body) {

  switch (type) {
    case 'model':
      break;

    case 'studentQuestion':

      let question = await modules.models.student_question.findOne({ where: { student_question_id: param.question_id }});
      question.fileCnt += 1;
      await question.save();
      let s3_param = {
        Bucket: 'preswot-service',
        Key: `studentQuestion/${param.question_id}/${body.originalname}`,
        ACL: 'public-read',
        ContentType: body.mimetype
      };
      var s3obj = new AWS.S3({ params: s3_param });
      s3obj.upload({ Body: body.buffer }).send(async function(err, data) {
        let url = data.Location;
        if (err) {
          console.log(`studentQUestion file upload error`);
        } else {
          await modules.models.student_question_file.create({ lecture_id: question.lecture_id, student_question_id: param.question_id, user_id: param.student_id, file_url: url, file_name: body.originalname });
        }
      })
      break;

    default:
      break;
  }
}
async function deleteFile(type, param) {

  let s3 = new AWS.S3();

  switch(type) {
    case 'studentQuestion': 
      const studentQuestion = await modules.models.student_question.findOne({ where: { student_question_id : param.question_id }});
      const studentQuestionFile = await modules.models.student_question_file.findOne({ where: { id: param.file_id }});
      console.log(JSON.stringify(studentQuestionFile));
      let s3_param = {
        Bucket: 'preswot-service',
        Key: `studentQuestion/${param.question_id}/${studentQuestionFile.file_name}` 
      }; 
      console.log(`delete file - ${JSON.stringify(s3_param)}`);

      await modules.models.student_question_file.destroy({ where: { id: param.file_id }});
      s3.deleteObject(s3_param, function(err, data) {
        if(err) { console.log(err);}
        else { console.log(`success`); }
      });
      studentQuestion.fileCnt -= 1;
      studentQuestion.save();
      break;
    case 'studentQuestionAll':
    const files = await modules.models.student_question_file.findAll({ where: { student_question_id: param.question_id }});
    for (let i = 0 ; i < files.length ; i += 1) {
      let s3_param2 = {
        Bucket: 'preswot-service',
        Key: `studentQuestion/${param.question_id}/${files[i].file_name}` 
      };
      s3.deleteObject(s3_param2, function(err, data) {
        if(err) { console.log(err);}
        else { console.log(`success`); }
      });

    }
    await modules.models.student_question_file.destroy({ where: { student_question_id: param.question_id }});

      break;
    default:
      break;
  }
  
}
exports.deleteFile = deleteFile;
exports.uploadFile = uploadFile;