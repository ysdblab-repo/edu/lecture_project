const mysqlDump = require('mysqldump');
const moment = require('moment');
const config = require('../config');
const modules = require('../modules/frequent-modules');
const path = require('path');
const fs = require('fs');

const dumpPromise = (dest) =>
  new Promise((resolve, reject) => {
    mysqlDump({
      host: config.db.host,
      user: config.db.user,
      password: config.db.password,
      database: config.db.database,
      dest,
    }, (err) => {
      if (err) {
        console.log(err);
        reject(err);
      }
      resolve(true);
    });
  });

exports.dump = async () => {
  const cur = moment();
  const thePath = path.join(config.appRoot, '/dumps/');

  if (!fs.existsSync(thePath)) {
    fs.mkdirSync(thePath);
  }

  const dest = path.join(config.appRoot, '/dumps/', `${cur.format('YYYY-MM-DD-HHmmss')}.sql`);
  await dumpPromise(dest);
};

exports.reset = async () => {
  await modules.models.sequelize.sync({ force: true });
};

exports.restore = async (sel) => {
  const sql = `SET FOREIGN_KEY_CHECKS = 0;
${fs.readFileSync(path.join(config.appRoot, '/dumps', sel), 'utf8')}
SET FOREIGN_KEY_CHECKS = 1;`;

  await modules.db.getQueryResult(sql);
};
