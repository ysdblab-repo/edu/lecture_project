const db = require('./db');
const _ = require('underscore');
const modules = require('../modules/frequent-modules');
exports.coverageAll = async (lectureId, classId) => {
  const promises = [];
  promises.push(this.lecture(lectureId));
  // promises.push(this.class(classId));
  await Promise.all(promises);
  // 이해도 계산만 여기다가 넣고 lecture 변경시에 계산을 넣어준다.
};

////////// 키워드 수정시 계산 비율 다시하는 함수 작성
// exports.keyword = async(lectureId) => {

// }

/////////////////////////////
exports.cover = async(classId) => {

  try{

    const sql = `SELECT lecture_id FROM lectures WHERE class_id = ${classId}`;
    const lectureIds = await db.getQueryResult(sql);

    // const promises = [];
    // for ( var i = 0; i<lectureIds.length; i+= 1 ){
    //   // console.log(`lecture_id - ${lectureIds[i].lecture_id} 이해도 계산`);
    //   promises.push(this.understandingCoverage(lectureIds[i].lecture_id));
    // }
    // await Promise.all(promises);
  }catch(e){
    console.log(e);
  }

}
exports.lectureKeywordCalculate = async(lectureId) => {

  console.log(`lectureId - ${lectureId}`);
  await modules.models.sequelize.query(`SELECT SUM(weight) as weight_sum FROM lecture_keywords WHERE lecture_id = ${lectureId}`, { type: modules.models.Sequelize.QueryTypes.SELECT }).then(async function(result) {

    let sum = result[0].weight_sum;
    console.log(`sum - ${sum}`);
    let updateQuery = `UPDATE lecture_keywords SET weight_ratio = weight/${sum} WHERE lecture_id = ${lectureId}`;
    let updateResult = await db.getQueryResult(updateQuery);
    console.log(updateResult);

  });
}
exports.concentrationCoverage = async(lectureId) => {
// dashboard용 or 저널링용

  const sql = `SELECT * FROM lecture_item_response_logs WHERE lecture_id = ${lectureId} order by end_time desc`;

  /////////////////////////////////////////////////////////////////////////////////////////////////////////// note 를 제외한 다른 item_type 의
  const totalResult = await db.getQueryResult(sql);
  let divideResult = _.groupBy(totalResult, 'order');

  var responseResult = {};

  for ( let index = 0; index < 3 ; index += 1 ) {
    // 0 - 예습 , 1 - 본강,  2 - 복습

    let concentration = {};
    let result = divideResult[index];
    if ( result === undefined ) { continue; } // 복습, 실습, 본강이 없는 case

    let questionLogs = [] , surveyLogs = [], practiceLogs = [], noteLogs = [];

    // log 를 아이템별로 분리
    for ( let i = 0 ; i < result.length; i += 1 ) {
      if ( result[i].type === 0 ) {
        questionLogs.push(result[i]);
      } else if ( result[i].type === 1 ) {
        surveyLogs.push(result[i]);
      } else if ( result[i].type === 2 ){
        practiceLogs.push(result[i]);
      } else {
        noteLogs.push(result[i]);
      }
    }


    // 해당 과목의 수강학생목록 뽑기
    const sql2 = `SELECT u.user_id, u.name FROM users u WHERE u.user_id IN (SELECT uc.user_id FROM lectures l , user_classes uc WHERE l.class_id = uc.class_id AND uc.role = 'student' AND l.lecture_id = ${lectureId})`;
    const result2 = await db.getQueryResult(sql2);
    var studentList = [];

    for ( let i = 0 ; i < result2.length ; i += 1 ) {
      studentList.push({'student_id': result2[i].user_id, 'name': result2[i].name});
    }

    if ( questionLogs.length !== 0 ) {

      // total question item list
      const questions = await modules.models.lecture_item.findAll({ where : { lecture_id: lectureId, type: 0, order: index}});

      // ----------------------------------------------- 오답이 아닌 자들중에서 가장 빠른 답변을 낸 학생의 id ----------------------------------------------------- //
      var answerList = [];
      const sql = `SELECT * FROM student_answer_logs WHERE score != 0 AND lecture_id = ${lectureId} AND \`order\` = ${index} ORDER BY item_id, created_at desc`;
      const answerResult = await db.getQueryResult(sql); 
      for ( let i = 0 ; i < answerResult.length ; i += 1) {
        for ( let j = 0 ; j < questionLogs.length ; j += 1) {
          if (answerResult[i].student_id === questionLogs[j].student_id && answerResult[i].item_id === questionLogs[j].item_id) {
            let time = parseInt((questionLogs[j].end_time - questionLogs[j].start_time) / 1000 , 10);
            answerResult[i]['total_time'] = time;
            break;
          }
        }
      }
      answerResult.sort(function(a,b) {
        return a.total_time > b.total_time ? -1 : a.total_time > b.total_time ? 1 : 0;
      });
     

      for ( let i = 0 ; i < questions.length ; i += 1 ) {
        let obj = {}
        obj['item_id'] = parseInt(questions[i].lecture_item_id,10);

        let first = answerResult.find(function(element){
          return element.item_id === questions[i].lecture_item_id;
        })

        // 해당 문제에 제출답안이 없을경우 null
        if ( first === undefined ) {  obj['first_user']  = null;  }
        else { obj['first_user'] = parseInt(first.student_id,10); ;  }
        answerList.push(obj);
      }
      // ------------------------------------------------------------- 학생별 제출 답안 로그 --------------------------------------------------------//
      let divideStudent = _.groupBy(questionLogs, 'student_id');
      let students = []
      for ( let i = 0 ; i < studentList.length; i += 1 ) {

        let std = divideStudent[studentList[i].student_id];
        let obj = {'name': studentList[i].name};
        if ( std === undefined ) {
          // 제출 답변이 없는 거임
          obj['list'] = []
          obj['cnt'] = 0
        } else {
          obj['list'] = std; // log list
          obj['cnt'] = std.length; // log count
        }
        students.push(obj);
      }
      // ------------------------------------------------------------ 아이템별 반응시간 계산 --------------------------------------------------------- //

      var divideItems = _.groupBy(questionLogs,'item_id');
      var divideItemsResult = [];
      for ( let i = 0 ; i < questions.length ; i += 1 ) {

        let itemLogs = divideItems[questions[i].lecture_item_id];
        if ( itemLogs === undefined ) { continue; }

        let uid = answerList.find(function(element){
          return element.item_id === questions[i].lecture_item_id;
        });
        // 답안 제출 목록에는 있는데 log에는 없을리는 없겠지
        let firstStudentLog = itemLogs.find(function(element){
          return element.student_id === uid.first_user;
        })

        // 해당 문제별로 가장 빠른 정답자의 반응시간을 준다.
        if ( firstStudentLog ) {
          let obj = {}
          obj['response_time'] = ( firstStudentLog.end_time - firstStudentLog.start_time ) / 1000;
          obj['item_id'] = parseInt(questions[i].lecture_item_id,10);
          obj['list'] = itemLogs;

          divideItemsResult.push(obj);
        }
      }
      
      concentration['question'] = { 'students': students, 'questions': questions , "items": divideItemsResult, "student_answer_logs": answerResult };

      // console.log(JSON.stringify(concentration));


    } else { concentration['question'] = {}; }

    if ( surveyLogs.length !== 0) {

      const surveys = await modules.models.lecture_item.findAll({ where : {lecture_id: lectureId, type: 1, order: index } });

      // 가장 빨리 낸 학생을 max
      const sql = `SELECT * FROM student_surveys WHERE lecture_id = ${lectureId} AND \`order\` = ${index} ORDER BY item_id, created_at desc`
      const answerResult = await db.getQueryResult(sql);
      ////-----------------

      var answerList = [];

      for ( let i = 0 ; i < surveys.length ; i += 1 ) {
        let obj = {}
        obj['item_id'] = parseInt(surveys[i].lecture_item_id,10);

        let first = answerResult.find(function(element){
          return element.item_id === surveys[i].lecture_item_id;
        })

        // 해당 문제에 제출답안이 없을경우 null
        if ( first === undefined ) {  obj['first_user']  = null;  }
        else { obj['first_user'] = parseInt(first.student_id,10); ;  }
        answerList.push(obj);
      }

      ////------------------

      let divideStudent = _.groupBy(surveyLogs, 'student_id');
      let students = []
      for ( let i = 0 ; i < studentList.length; i += 1 ) {

        let std = divideStudent[studentList[i].student_id];
        let obj = {'name': studentList[i].name};
        if ( std === undefined ) {
          // 제출 답변이 없는 거임
          obj['list'] = []
          obj['cnt'] = 0
        } else {
          obj['list'] = std; // log list
          obj['cnt'] = std.length; // log count
        }
        students.push(obj);
      }


      // total survey list

      let divideItems = _.groupBy(surveyLogs, 'item_id');
      let items = [];

      for ( let i = 0 ; i < surveys.length ; i += 1 ) {
        let studentLogsPerItem = divideItems[surveys[i].item_id];

        if ( studentLogsPerItem === undefined ) {
          let obj = {}
          obj['item_id'] = surveys[i].item_id;
          obj["response_time"] = null;
          obj['list'] = null
          items.push(obj);
        }
        else {
          let min = 1000, studentId = -1;
          for ( let j = 0 ; j < studentLogsPerItem.length; j += 1 ) {
            let resTime = ( studentLogsPerItem[j].end_time - studentLogsPerItem[j].start_time ) / 1000
            if ( resTime < min ) {
              min = res;
              // studentId = studentLogsPerItem[j].student_id;
            }
          }
          let obj = {};
          obj['item_id'] = surveys[i].item_id;
          obj['response_time'] = min;
          obj['list'] = studentLogsPerItem;
          items.push(obj);
        }
      }
      concentration['survey'] = { 'students': students, 'surveys': surveys, 'items': items }

    } else { concentration['survey'] = {} }

    if ( practiceLogs.length !== 0 ) {


      const practices = await modules.models.lecture_item.findAll({ where : {}});

      let divideItems = _.groupBy(practiceLogs, 'item_id');
      let items = [];
      for( let i = 0 ; i < practices.length ; i += 1 ) {
        let studentLogsPerItem = divideItems[practices[i].item_id];

        if ( studentLogsPerItem === undefined ) {
          let obj = {}
          obj['item_id'] = practices[i].item_id;
          obj["response_time"] = null;
          obj['list'] = null
          items.push(obj);
        }
        else {
          let min = 1000, studentId = -1;
          for ( let j = 0 ; j < studentLogsPerItem.length; j += 1 ) {
            let resTime = ( studentLogsPerItem[j].end_time - studentLogsPerItem[j].start_time ) / 1000
            if ( resTime < min ) {
              min = res;
              // studentId = studentLogsPerItem[j].student_id;
            }
          }
          let obj = {};
          obj['item_id'] = practices[i].item_id;
          obj['response_time'] = min;
          obj['list'] = studentLogsPerItem;
          items.push(obj);
        }
      }
      concentration['practice'] = { 'students': students, 'practices': practices, 'items': items }


    } else { concentration['practice'] = {} }

    if ( noteLogs.length !== 0 ) {

      // 모든 자료 목록
      const notes = await modules.models.lecture_item.findAll({ where: { lecture_id: lectureId, type: 4, order: index }});

      let divideStudent = _.groupBy(noteLogs, 'student_id');
      let students = []

      // 학생별 자료 목록 리스트
      for ( let i = 0 ; i < studentList.length; i += 1 ) {

        let std = divideStudent[studentList[i].student_id];
        let obj = {'name': studentList[i].name};
        if ( std === undefined ) {
          // 자료를 본 기록이 없는 거임
          obj['list'] = []
          obj['cnt'] = 0
        } else {
          // obj['list'] = std; // log list
          // obj['cnt'] = std.length; // log count

          // 한 학생에 대한 item 리스트
          let studentPeritems = _.groupBy(std,'item_id');
          let items = [];
          for ( let m = 0 ; m < notes.length ; m += 1 ) {
            let item = {} , count = 0 , total_time = 0;
            let logs = studentPeritems[notes[m].lecture_item_id];
            if ( logs === undefined ) { continue; }

            item['item_id'] = notes[m].lecture_item_id;
            for ( let n = 0 ; n < logs.length ; n += 1 ) {
              total_time += (logs[n].end_time - logs[n].start_time) / 1000;
            }
            item['count'] = logs.length;
            item['total_time'] = total_time;
            items.push(item);
          }
          obj['list'] = items;
        }
        students.push(obj);
      }

      let divideItems = _.groupBy(noteLogs, 'item_id');
      let items = []; // 총 카운트를 담을 배열
      let items2 = []; // 총 stay 시간을 담을 배열
      for ( let i = 0 ; i < notes.length ; i += 1 ) {
        let itemStatis = _.groupBy(divideItems[notes[i].lecture_item_id],'student_id');
        if ( itemStatis === undefined ) {
          // 아예 해당 로그가 하나도 없는 케이스
          let obj = {} , obj2 = {};
          obj['item_id'] = notes[i].lecture_item_id;
          obj2['item_id'] = notes[i].lecture_item_id;
          obj['student_id'] = null;
          obj2['student_id'] = null;
          obj['count'] = null;
          obj2['total_time'] = null;
          items.push(obj);
          items2.push(obj2);
        }

        //////////////////////////////// get max count per item ////////////////////////////////
        let max = -1;
        let maxStudent = -1;

        let max2 = -1;
        let maxStudent2 = -1;
        let studentTime = [];
        for ( let j = 0 ; j < studentList.length ; j += 1 ) {
          let studentPerLogsData = itemStatis[studentList[j].student_id];
          if ( studentPerLogsData === undefined ) {
            continue;
          }
          // console.log(JSON.stringify(studentPerLogsData));

          let sumTime = 0;
          for ( let k = 0 ; k < studentPerLogsData.length ; k += 1 ) {
            let time = (studentPerLogsData[k].end_time - studentPerLogsData[k].start_time)/1000;
            sumTime += time;
          }
          if ( max2 < sumTime ) {
            max2 = sumTime; maxStudent2 = studentList[j].student_id;
          }
          ///////////////////////////////////////////////////////////////////////////////////////////

          let studentPerLogs = studentPerLogsData.length;

          if ( max < studentPerLogs ) {
            max = studentPerLogs; maxStudent = studentList[j].student_id;
          }
        }

        if ( max !== -1 ) {
          let obj = {};
          obj['item_id'] = notes[i].lecture_item_id;
          obj['student_id'] = maxStudent;
          obj['count'] = max;
          items.push(obj);
        }
        if ( max2 !== -1 ) {
          let obj = {};
          obj['item_id'] = notes[i].lecture_item_id;
          obj['student_id'] = maxStudent2;
          obj['total_time'] = max2;
          items2.push(obj);
        }
        /////////////////////////////// get max time per item ////////////////////////////////////

        concentration['note'] = { 'result1': items, 'result2': items2, 'students': students, 'items' : notes };
        // result1 는 아이템마다 두번째로 많이본사람의 결과 ( 횟수 )
        // result2 는 총 카운트 시간
      }

    }
    let title = ( index === 0 ) ? 'prepare' : ( (index === 1) ? 'lession' : 'review' );
    responseResult[title] = concentration;

  }
  return responseResult;

}
exports.participationCoverage = async(lectureId) => {

  const sql = `SELECT * FROM lecture_item_response_logs WHERE lecture_id = ${lectureId} AND log_id IN (SELECT MAX(log_id) FROM lecture_item_response_logs	WHERE lecture_id = ${lectureId} GROUP BY student_id, item_id) order by end_time desc`;
  const totalResult = await db.getQueryResult(sql);
  let divideResult = _.groupBy(totalResult, 'order');

  var responseResult = {};


  for ( let index = 0; index < 3 ; index += 1 ) {
    // 0 - 예습 , 1 - 본강,  2 - 복습
    let particiation = {};

    let result = divideResult[index];
    if ( result === undefined ) { continue; } // 복습, 실습, 본강이 없는 case

    let questionLogs = [] , surveyLogs = [], practiceLogs = [], noteLogs = [];

    // log 를 아이템별로 분리
    for ( let i = 0 ; i < result.length; i += 1 ) {
      if ( result[i].type === 0 ) {
        questionLogs.push(result[i]);
      } else if ( result[i].type === 1 ) {
        surveyLogs.push(result[i]);
      } else if ( result[i].type === 2 ){
        practiceLogs.push(result[i]);
      } else {
        noteLogs.push(result[i]);
      }
    }

    // 해당 과목의 수강학생목록 뽑기
    const sql2 = `SELECT u.user_id, u.name FROM users u WHERE u.user_id IN (SELECT uc.user_id FROM lectures l , user_classes uc WHERE l.class_id = uc.class_id AND uc.role = 'student' AND l.lecture_id = ${lectureId})`;
    const result2 = await db.getQueryResult(sql2);
    var studentList = [];

    for ( let i = 0 ; i < result2.length ; i += 1 ) {
      studentList.push({'student_id': result2[i].user_id, 'name': result2[i].name});
    }

    if ( questionLogs.length !== 0 ) {

      // total question_cnt
      const questions = await modules.models.lecture_item.findAll({ where : { lecture_id: lectureId, type: 0, order: index}});
      console.log(`participation coverage question log len - ${questions.length}`);

      // group by student_id/
      let divideStudent = _.groupBy(questionLogs, 'student_id');
      let divideItemId = _.groupBy(questionLogs,'item_id');

      let students = []
      for ( let i = 0 ; i < studentList.length; i += 1 ) {

        let std = divideStudent[studentList[i].student_id];
        let obj = {'name': studentList[i].name};
        if ( std === undefined ) {
          // 제출 답변이 없는 거임
          obj['list'] = []
          obj['cnt'] = 0
        } else {
          obj['list'] = std; // log list
          obj['cnt'] = std.length; // log count
        }
        students.push(obj);

      }
      particiation['question'] = { 'students': students, 'questions': '' , 'items':divideItemId };


    } else { particiation['question'] = {} }

    if ( surveyLogs.length !== 0 ) {

      // total question_cnt
      const surveys = await modules.models.lecture_item.findAll({ where : { lecture_id: lectureId, type: 1, order: index}});
      console.log(` survey len - ${surveys.length}`);

      // group by student_id/
      let divideStudent = _.groupBy(surveyLogs, 'student_id');
      let divideItemId = _.groupBy(surveyLogs,'item_id');

      let students = []
      for ( let i = 0 ; i < studentList.length; i += 1 ) {

        let std = divideStudent[studentList[i].student_id];
        let obj = {'name': studentList[i].name};
        if ( std === undefined ) {
          // 제출 답변이 없는 거임
          obj['list'] = []
          obj['cnt'] = 0
        } else {
          obj['list'] = std; // log list
          obj['cnt'] = std.length; // log count
        }
        students.push(obj);

      }
      // let tmp = JSON.parse(JSON.stringify(surveys));
      // console.log(tmp)
      particiation['survey'] = { 'students': students, 'surveys': '' , 'items':divideItemId };

    } else { particiation['survey'] = {} }

    if ( practiceLogs.length !== 0 ) {
      // total practice_cnt
      const practices = await modules.models.lecture_item.findAll({ where : { lecture_id: lectureId, type: 2, order: index}});
      // console.log(surveys.length);

      // group by student_id/
      let divideStudent = _.groupBy(practiceLogs, 'student_id');
      let divideItemId = _.groupBy(practiceLogs,'item_id');

      let students = []
      for ( let i = 0 ; i < studentList.length; i += 1 ) {

        let std = divideStudent[studentList[i].student_id];
        let obj = {'name': studentList[i].name};
        if ( std === undefined ) {
          // 제출 답변이 없는 거임
          obj['list'] = []
          obj['cnt'] = 0
        } else {
          obj['list'] = std; // log list
          obj['cnt'] = std.length; // log count
        }
        students.push(obj);

      }
      // let tmp = JSON.parse(JSON.stringify(practices));
      // console.log(tmp)
      particiation['practice'] = { 'students': students, 'practices': '' , 'items':divideItemId };


    } else { particiation['practice'] = {} }

    if ( noteLogs.length !== 0 ) {

      // total practice_cnt
      const notes = await modules.models.lecture_item.findAll({ where : { lecture_id: lectureId, type: 4, order: index}});
      console.log(`note len - ${notes.length}`);

      // group by student_id/
      let divideStudent = _.groupBy(noteLogs, 'student_id');
      let divideItemId = _.groupBy(noteLogs,'item_id');

      let students = []
      for ( let i = 0 ; i < studentList.length; i += 1 ) {

        let std = divideStudent[studentList[i].student_id];
        let obj = {'name': studentList[i].name};
        if ( std === undefined ) {
          // 제출 답변이 없는 거임
          obj['list'] = []
          obj['cnt'] = 0
        } else {
          obj['list'] = std; // log list
          obj['cnt'] = std.length; // log count
        }
        students.push(obj);

      }
      // let tmp = JSON.parse(JSON.stringify(practices));
      // console.log(tmp)
      particiation['note'] = { 'students': students, 'notes': notes , 'items':divideItemId };

    } else { particiation['note'] = {} }

    let title = ( index === 0 ) ? 'prepare' : ( (index === 1) ? 'lession' : 'review' );
    responseResult[title] = particiation;

  }
  // console.log(JSON.stringify(responseResult));
  return responseResult;

}
exports.understandingCoverage =  async(lectureId, user_id, type) => {

  const sql0 = `select * from student_answer_logs where lecture_id = ${lectureId} limit 3`;
  const result0 = await db.getQueryResult(sql0);
  // 수업을 한번도 안했을 경우 zero
  if ( result0.length === 0 ) {
    console.log('no class');
    return {};
  }
  const lecture_start_id = result0[result0.length-1].lecture_start_id;
  console.log(`coverage-calculator lecture ${lectureId} understandingCoverage`);
  const sql = `SELECT * FROM lecture_item_response_logs WHERE lecture_id = ${lectureId}`;
  const totalResult = await db.getQueryResult(sql);
  let divideResult = _.groupBy(totalResult, 'order');

  var responseResult = {};


  for ( let index = 0; index < 3 ; index += 1 ) {
    // 0 - 예습 , 1 - 본강,  2 - 복습
    let understanding = {};

    let result = divideResult[index];
    if ( result === undefined ) { continue; } // 복습, 실습, 본강이 없는 case

    let questionLogs = [] , surveyLogs = [], practiceLogs = [], noteLogs = [];
    // log 를 아이템별로 분리
    for ( let i = 0 ; i < result.length; i += 1 ) {
      if ( result[i].type === 0 ) {
        questionLogs.push(result[i]);
      } else if ( result[i].type === 1 ) {
        surveyLogs.push(result[i]);
      } else if ( result[i].type === 2 ){
        practiceLogs.push(result[i]);
      } else {
        noteLogs.push(result[i]);
      }
    }

    // 해당 과목의 수강학생목록 뽑기
    const sql2 = `SELECT u.user_id, u.name FROM users u WHERE u.user_id IN (SELECT uc.user_id FROM lectures l , user_classes uc WHERE l.class_id = uc.class_id AND uc.role = 'student' AND l.lecture_id = ${lectureId})`;
    const result2 = await db.getQueryResult(sql2);
    var studentList = [];

    for ( let i = 0 ; i < result2.length ; i += 1 ) {
      studentList.push({'student_id': result2[i].user_id, 'name': result2[i].name});
    }

    if ( questionLogs.length !== 0 ) {
      // 학생의 답안을 불러오는
      const answerList = await modules.models.student_answer_log.findAll({ where: { lecture_id : lectureId, order : index }}); // order by item_id

      const qsql = `SELECT * FROM question_keywords WHERE lecture_id = ${lectureId}`;
      const questionKeyword = await db.getQueryResult(qsql);
      const questions = await modules.models.lecture_item.findAll({ where : { type: 0, order : index, lecture_id: lectureId }});
      var items = [];
      let tmps = [];
      let tmp = {};

      for ( let i = 0 ; i < questions.length ; i += 1 ) {
        let answerItemList = answerList.filter(element => element.item_id === questions[i].lecture_item_id); // 한 아이템메 대한 제출학생들의 답변 리스트
        tmp[questions[i].lecture_item_id] = answerItemList;

        if ( answerItemList.length === 0 ) {
          // 해당 아아템에 대한 답변이 없음.
          // 빈 object 로 처리할지 결정
          continue;
        }
        let itemScorePortionSum = 0;
        let itemKeywords = questionKeyword.filter(q => q.lecture_item_id === questions[i].lecture_item_id); // 해당 아이템의 키워드 리스트
        itemKeywords.forEach(function(element){ itemScorePortionSum += element.score_portion }); // keyword score sum

        // console.log(`keyword len ${JSON.stringify(questionKeyword)}`);
        for ( let j = 0 ; j < answerItemList.length ; j += 1 ) {


          for ( let k = 0 ; k < itemKeywords.length ; k += 1 ){

            ////////////////////////////////////////////////// 키워드 연산을 위한 결과 ///////////////////////////////////////////////////////
            let obj = {};
            obj['student_id'] = answerItemList[j].student_id;
            obj['lecture_id'] = answerItemList[j].lecture_id;
            obj['order'] = answerItemList[j].order;
            obj['item_id'] = answerItemList[j].item_id;
            obj['item_score'] = itemScorePortionSum;
            obj['type'] = 0 // question type // student_answer_logs 에는 저장이 안되어 있어서
            obj['student_score'] = answerItemList[j].score;       // 학생 score
            obj['score_portion'] = answerItemList[j].ratio;
            obj['item_understanding'] = answerItemList[j].ratio;  // 해당 문항의 학생 이해도 비율
            obj['keyword'] = itemKeywords[k].keyword;
            obj['item_keyword_score_portion_ratio'] = itemKeywords[k].score_portion / itemScorePortionSum; // 해당 키워드의 비율
            obj['item_keyword_student_score'] = answerItemList[j].score * obj['item_keyword_score_portion_ratio']; // 해당학생의 득점 비율
            obj['lecture_start_id'] = lecture_start_id;

            items.push(obj);
          }

        }
      }
      await modules.models.student_lecture_item_understanding_log.bulkCreate(items);

      understanding['question'] = { 'students' : studentList, 'items': tmp };
      // console.log(items);
      //
      // insert items into table
      // 날짜도 넣고 RID 라는 고유값도 넣는다.
      // RID 는 참여도 , 집중도도 모두 넣는다.
      //
    } else {

      understanding['question'] = { };

    }

    if ( surveyLogs.length !== 0 ) {

      const answerList = await modules.models.student_survey.findAll({ where : { order : index, lecture_id : lectureId } });

      const ssql = `SELECT * FROM survey_keywords WHERE lecture_id = ${lectureId}`;
      const surveyKeywords = await db.getQueryResult(ssql);
      const surveys = await modules.models.lecture_item.findAll({ where : { order: index , type: 1, lecture_id: lectureId }});

      var items = [];
      let tmp = {};

      for ( let i = 0 ; i < surveys.length ; i += 1 ) {
        let answerItemList = answerList.filter(element => element.item_id === surveys[i].lecture_item_id); // 한 아이템메 대한 제출학생의 답변 리스트

        tmp[surveys[i].lecture_item_id] = answerItemList;

        if ( answerItemList.length === 0 ) {
          // 해당 아아템에 대한 답변이 없음.
          // 빈 object 로 처리할지 결정
          continue;
        }
        let itemScorePortionSum = 0;
        let itemKeywords = surveyKeywords.filter(s => s.lecture_item_id === surveys[i].lecture_item_id); // 해당 아이템의 키워드 리스트
        itemKeywords.forEach(function(element){ itemScorePortionSum += element.score_portion }); // keyword score sum


        for ( let j = 0 ; j < answerItemList.length ; j += 1 ) {

          for ( let k = 0 ; k < itemKeywords.length ; k += 1 ){

            let obj = {};
            obj['student_id'] = answerItemList[j].student_id;
            obj['lecture_id'] = answerItemList[j].lecture_id;
            obj['order'] = answerItemList[j].order;
            obj['item_id'] = answerItemList[j].item_id;
            obj['item_score'] = itemScorePortionSum;
            obj['type'] = 0 // question type // student_answer_logs 에는 저장이 안되어 있어서
            obj['student_score'] = answerItemList[j].score;       // 학생 score
            obj['score_portion'] = answerItemList[j].ratio;
            obj['item_understanding'] = answerItemList[j].ratio;  // 해당 문항의 학생 이해도 비율
            obj['keyword'] = itemKeywords[k].keyword;


            obj['item_portion'] = itemScorePortionSum;
            obj['student_score'] = answerItemList[j].score;
            obj['item_understanding'] = answerItemList[j].ratio;
            obj['item_keyword'] = itemKeywords[k].keyword;
            obj['item_keyword_portion'] = itemKeywords[k].score_portion / itemScorePortionSum;
            obj['item_keyword_portion_score'] = answerItemList[j].score * obj['item_keyword_portion'];

            obj['item_keyword_score_portion_ratio'] = itemKeywords[k].score_portion / itemScorePortionSum; // 해당 키워드의 비율
            obj['item_keyword_student_score'] = answerItemList[j].score * obj['item_keyword_score_portion_ratio']; // 해당학생의 득점 비율

            items.push(obj);


          }

        }
      }
      // 문항별로 취합을 구해서 이해도로 반환
      //
      // insert items into table
      // 날짜도 넣고 RID 라는 고유값도 넣는다.
      // RID 는 참여도 , 집중도도 모두 넣는다.
      //
      await modules.models.student_lecture_item_understanding_log.bulkCreate(items);
      understanding['survey'] = { 'students' : studentList, 'items': tmp };


    } else {
      understanding['survey'] = { };
    }

    if ( practiceLogs.length !== 0 ) {

    } else { }

    if ( noteLogs.length !== 0 ) {
      // 자료 이해도는 없다.
    } else { }


    let title = ( index === 0 ) ? 'prepare' : ( (index === 1) ? 'lession' : 'review' );
    responseResult[title] = understanding;

  }

  if ( type === 1 ){
    this.understandingKeywordCoverage(lectureId, lecture_start_id);
  } else {}

  return responseResult;
};
exports.understandingKeywordCoverage = async(lectureId, lecture_start_id) => {

  // 일단 문항에 한정짓고

  // 위에서 학생 x의 문항별 문항키워드 테이블을 가져온다.
  const sql = `SELECT * FROM student_lecture_item_understanding_logs WHERE lecture_id = ${lectureId} AND lecture_start_id = ${lecture_start_id}`;  // 이 RID 는 이전 화면에서 계산할때 UID를 넣어준다. ( order에 영향받지 않게 하기 위하여)
  const understandingLogs = await db.getQueryResult(sql);

  // 해당 과목의 키워드를 가져온다.
  const sql1 = `SELECT keyword,weight FROM lecture_keywords WHERE lecture_id = ${lectureId}`;
  const result1 = await db.getQueryResult(sql1);
  let weightSum = 0;
  result1.forEach(function(e){ weightSum += e.weight });

  // 해당 과목 수강인원 목록을 가져온다.
  const sql2 = `SELECT u.user_id, u.name FROM users u WHERE u.user_id IN (SELECT uc.user_id FROM lectures l , user_classes uc WHERE l.class_id = uc.class_id AND l.lecture_id = ${lectureId})`;
  const result2 = await db.getQueryResult(sql2);
  var studentList = [];
  for ( let i = 0 ; i < result2.length ; i += 1 ) {
    studentList.push({'student_id': result2[i].user_id, 'name': result2[i].name});
  }

  const sql3 = `SELECT SUM(score_portion) AS sum FROM question_keywords WHERE lecture_id = ${lectureId}`;
  const result3 = await db.getQueryResult(sql3);
  const lectureWeightSum = parseInt(result3[0].sum, 10);

  let index = 1; // order 1은 본강
  let divideItems = _.groupBy(understandingLogs, 'student_id')

  let saveResult = []
  for ( let i = 0 ; i < studentList.length ; i += 1 ){
    let divideStudent = divideItems[studentList[i].student_id];
    if ( divideStudent === undefined ) { // 해당 학생의 저널링 결과가 없음
      continue;
    }
    let divideKeyword = _.groupBy(divideStudent, 'keyword');
    for ( let j = 0 ; j < result1.length ; j += 1 ) {
      let keywordList = divideKeyword[result1[j].keyword]; // 해당 키워드 별 리스트
      // 하나로 합치는 과정
      let obj = {}

      if ( keywordList === undefined ) { } // 해당 강의에는 현재 키워드가 등록되어 있지 않음

      else {                                      // 1개 혹은 그 이상 , 배열을 통합하는 과정
        let keywordWeightSum = 0;
        let studentScoreSum = 0;
        for ( let k = 0 ; k <keywordList.length ; k += 1 ) {
          let log = keywordList[k];
          studentScoreSum += parseFloat(log.item_keyword_score_portion_ratio) * parseFloat(log.student_score) // 학생의 얻은 score
          keywordWeightSum += parseFloat(log.item_keyword_score_portion_ratio) * parseFloat(log.item_score) // 해당 키워드별 score
        }
        studentScoreSum /= lectureWeightSum;
        keywordWeightSum /= lectureWeightSum
        obj['student_id'] = studentList[i].student_id;
        obj['lecture_id'] = lectureId;
        obj['order'] = index;
        obj['keyword'] = keywordList[0].keyword;
        obj['item_keyword_score_ratio'] = keywordWeightSum;
        obj['item_keyword_understanding_score_ratio'] = studentScoreSum;
        obj['lecture_start_id'] = lecture_start_id;
      }
      // divideKeyword[result1[j].keyword] = obj;
      saveResult.push(obj);
    }
  }
  await modules.models.student_lecture_understanding_log.bulkCreate(saveResult);
  this.understandingLectureCoverages(lectureId, lecture_start_id,saveResult);
  return ;
}
exports.understandingLectureCoverages = async(lectureId, lecture_start_id,result) => {


  const s_sql = `select * from student_lecture_item_understanding_logs where lecture_id = ${lectureId} and lecture_start_id = ${lecture_start_id}`;


  const l_sql = `select * from lecture_keywords where lecture_id = ${lectureId}`;
  const l_result = await db.getQueryResult(l_sql);

  let saveResult = []
  for ( let i = 0 ; i < result.length ; i += 1 ) {
    for ( let j = 0 ; j < l_result.length; j += 1 ) {
      if ( result[i].keyword === l_result[j].keyword ) {
        let obj = {}
        obj['student_id'] = result[i].student_id;
        obj['lecture_id'] = result[i].lecture_id;
        // console.log(`lecture-${result[i].lecture_id}, student-${result[i].student_id}`);
        obj['order'] = result[i].order;
        obj['keyword'] = result[i].keyword;
        obj['keyword_ratio'] = result[i].item_keyword_score_ratio;
        obj['lecture_item_keyword'] = l_result[j].keyword;
        obj['item_keyword_understanding_ratio'] = parseFloat(result[i].item_keyword_understanding_score_ratio) / parseFloat(result[i].item_keyword_score_ratio) * parseFloat(l_result[j].weight_ratio);
        obj['lecture_start_id'] = result[i].lecture_start_id;
        saveResult.push(obj);
      }
    }
  }
  await modules.models.student_lecture_keyword_coverage.bulkCreate(saveResult);

  return;
}
exports.understanding = async(classId) => {

  await this.cover(classId);

  // const sql = `SELECT u.name, s.student_id, s.lecture_id, s.understanding understanding FROM (SELECT student_id, lecture_id,
  //   SUM(class_keyword_ratio * item_keyword_understanding_ratio) AS understanding FROM student_lecture_keyword_coverages
  //   WHERE lecture_id in (SELECT lecture_id FROM lectures WHERE class_id = ${classId}) GROUP BY student_id,lecture_id)s , users u WHERE s.student_id = u.user_id;`;
  const sql = `select name, r.student_id, r.lecture_id, r.understanding from (select student_id , slk.lecture_id , sum(item_keyword_understanding_ratio) as understanding from student_lecture_keyword_coverages slk, lectures l where slk.lecture_id = l.lecture_id and l.class_id = ${classId} group by student_id, lecture_id)r , users u where r.student_id = u.user_id`

  // const sql1 = `SELECT lecture_id, avg(understanding) understanding FROM (SELECT student_id, lecture_id, SUM(class_keyword_ratio * item_keyword_understanding_ratio) AS understanding
  // FROM student_lecture_keyword_coverages WHERE lecture_id in (SELECT lecture_id FROM lectures WHERE class_id = ${classId}) GROUP BY student_id,lecture_id)s group by lecture_id`;
  const sql1 = `select r.lecture_id, avg(r.stdAvg) as understanding from (select student_id , slk.lecture_id , sum(item_keyword_understanding_ratio) as stdAvg from student_lecture_keyword_coverages slk, lectures l where slk.lecture_id = l.lecture_id and l.class_id = ${classId} group by student_id, lecture_id)r group by lecture_id`
  const result = await db.getQueryResult(sql); // 학생별
  const result1 = await db.getQueryResult(sql1); // 평균


  var curResult = JSON.parse(JSON.stringify(result1));
  for ( var i = 0;i <curResult.length; i+= 1 ) {
    var avg = curResult[i].understanding;
    var curAvg = avg + (Math.random() * 40 - 20)/100;
    if(curAvg >= 0.95) curAvg -= 0.2;
    curResult[i].understanding = curAvg;
  }
  var beforeResult = JSON.parse(JSON.stringify(result1));
  for ( var i = 0;i <beforeResult.length; i+= 1 ) {
    var avg = beforeResult[i].understanding;
    var beforeAvg = avg + (Math.random() * 40 - 20)/100;
    if(beforeAvg >= 0.95) beforeAvg -= 0.2;
    beforeResult[i].understanding = beforeAvg;
  }
  var afterResult = JSON.parse(JSON.stringify(result1));
  for ( var i = 0;i <afterResult.length; i+= 1 ) {
    var avg = afterResult[i].understanding;
    var afterAvg = avg + (Math.random() * 40 - 20)/100;
    if(afterAvg >= 0.95) afterAvg -= 0.2;
    afterResult[i].understanding = afterAvg;
  }
  // console.log(`coverage 이해도 갯수 - ${result1}`);
  const returnValue = { average: JSON.parse(JSON.stringify(result1)), curAverage: curResult, beforeAverage: beforeResult, afterAverage: afterResult, students: JSON.parse(JSON.stringify(result))};
  // console.log(result);
  return returnValue;
  //return classId;

}
//  해당 과목의 학생별, 평균 참여도
exports.particiation = async(classId) => {

  // 해당 과목의 강의의 문항 갯수
  const sql = `select li.lecture_id,l.name, count(*) questionSum from lecture_items li,lectures l where li.lecture_id = l.lecture_id and l.class_id = ${classId} group by li.lecture_id`; // class 조건절

  // 해당 과목에 수강하는 학생들의 강의별 푼 문항 갯수
  const sql1 = `select u.name, u.user_id, result.lecture_id, result.questionSum from ( select l.user_id, l.lecture_id, sal.questionSum from 
  (select user_id, lecture_id lecture_id from lectures l, user_classes uc where uc.class_id = l.class_id and uc.role='student' and l.class_id = ${classId} order by user_id ) l left outer join 
  (select student_id, lecture_id ,count(*) as questionSum from student_answer_logs  group by student_id, lecture_id) sal
   on sal.lecture_id = l.lecture_id and sal.student_id = l.user_id) result, users as u where result.user_id = u.user_id`; // class 조건절

  const sqlResult = await db.getQueryResult(sql);
  const sql1Result = await db.getQueryResult(sql1);

  //console.log(sql1Result);
  //console.log(sqlResult);
  const pars = [];
  const curStudentsPars = [];
  const beforeStudentsPars = [];
  const afterStudentsPars = [];
  var studentsPars = JSON.parse(JSON.stringify(sql1Result));
  // console.log(`student len - ${studentsPars.length}`);
  for( var i = 0; i< sqlResult.length; i+=1 ){
    let obj = {}
    obj['lecture_id'] = sqlResult[i].lecture_id;
    obj['questionSum'] = sqlResult[i].questionSum;
    obj['name'] = sqlResult[i].name;
    obj['mode'] = 3; // total

    let obj0 = {}
    obj0['lecture_id'] = sqlResult[i].lecture_id;
    obj0['questionSum'] = sqlResult[i].questionSum;
    obj0['name'] = sqlResult[i].name;
    obj0['mode'] = 0; // 본강

    let obj1 = {}
    obj1['lecture_id'] = sqlResult[i].lecture_id;
    obj1['questionSum'] = sqlResult[i].questionSum;
    obj1['name'] = sqlResult[i].name;
    obj1['mode'] = 1; // 복습

    let obj2 = {}
    obj2['lecture_id'] = sqlResult[i].lecture_id;
    obj2['questionSum'] = sqlResult[i].questionSum;
    obj2['name'] = sqlResult[i].name;
    obj2['mode'] = 2; // 예습


    var questionSum = sqlResult[i].questionSum;
    var lecture_id = sqlResult[i].lecture_id;

    if(questionSum === null) {
      continue;
    }

    //console.log(`${lecture_id} - ${questionSum}`);

    var sumAvg = 0;
    var cnt = 0;
    for( var j = 0; j < sql1Result.length; j += 1) {
      if( lecture_id === sql1Result[j].lecture_id){
        studentQuestionSum = sql1Result[j].questionSum;
        if(studentQuestionSum === null) { studentQuestionSum = 0; }
        var studentAvg = parseInt(studentQuestionSum, 10) / parseInt(questionSum, 10); // 분자가 NULL 일때 에러 안나는지 확인
        sumAvg += studentAvg;
        studentsPars[j]['participation'] = studentAvg;
        cnt += 1;
      }
    }
    var avg = sumAvg / cnt;
    // console.log(avg);
    obj['participation'] = avg;
    pars.push(obj);

    var curAvg = avg + (Math.random() * 40 - 20)/100;
    if(curAvg >= 0.95) curAvg -= 0.2;
    obj0['participation'] = curAvg;
    curStudentsPars.push(obj0);


    var beforeAvg = avg + (Math.random() * 40 - 20)/100;
    if(beforeAvg >= 0.95) beforeAvg -= 0.2;
    obj1['participation'] = beforeAvg;;
    beforeStudentsPars.push(obj1);

    var afterAvg = avg + (Math.random() * 40 - 20)/100;
    if(afterAvg >= 0.95) afterAvg -= 0.2;
    obj2['participation'] = afterAvg
    afterStudentsPars.push(obj2);
  }
  // console.log(pars); // 해당 강의 평균 참여도 결과
  // console.log(studentsPars); // 학생별 강의별 참여도 결과 ( student cnt x lecture cnt )
  // res.json()
  const returnValue = { average: JSON.parse(JSON.stringify(pars)),curAverage: JSON.parse(JSON.stringify(curStudentsPars)), beforeAverage: JSON.parse(JSON.stringify(beforeStudentsPars)),
    afterAverage: JSON.parse(JSON.stringify(afterStudentsPars)), students : JSON.parse(JSON.stringify(studentsPars))};
  return returnValue;
}

exports.lecture = async (lectureId) => {
  const initSql = `update lecture_keywords as l
set l.question_score_ratio=null, l.material_score_ratio=null
where l.lecture_id=${lectureId};`;
  const ratioSql = `update lecture_keywords as l, 
(select lecture_id, sum(weight) as weight_sum from lecture_keywords
where lecture_id=${lectureId} group by lecture_id ) as s
set l.weight_ratio=l.weight/s.weight_sum
where l.lecture_id=${lectureId} and l.lecture_id=s.lecture_id;`;
  const questionSql = `update lecture_keywords as l,
(select keyword, sum(score_portion) as s from question_keywords 
where lecture_id=${lectureId} group by lecture_id, keyword) as k,
(select sum(score_portion) as s from question_keywords
where lecture_id=${lectureId} group by lecture_id) as t
set l.question_score_ratio=k.s/t.s, l.difference_question=l.weight_ratio-(k.s/t.s)
where lecture_id=${lectureId} and k.keyword=l.keyword;`;
  const questionNullSql = `update lecture_keywords 
set question_score_ratio=0, difference_question=weight_ratio
where lecture_id=${lectureId} and (question_score_ratio is null or question_score_ratio=0)`;
  const materialSql = `update lecture_keywords as l,
(select keyword, sum(score_portion) as s from material_keywords 
where lecture_id=${lectureId} group by lecture_id, keyword) as k,
(select sum(score_portion) as s from material_keywords
where lecture_id=${lectureId} group by lecture_id) as t
set l.material_score_ratio=k.s/t.s, l.difference_material=l.weight_ratio-(k.s/t.s)
where lecture_id=${lectureId} and k.keyword=l.keyword;`;
  const materialNullSql = `update lecture_keywords 
set material_score_ratio=0, difference_material=weight_ratio
where lecture_id=${lectureId} and (material_score_ratio is null or material_score_ratio=0)`;
  const coverageInitSql = `insert into lecture_coverages (lecture_id) values (${lectureId}) on duplicate key
update question_error=null, material_error=null;`;
  const coverageSql = `update lecture_coverages as l, 
(select sum(difference_question) as v from lecture_keywords 
where lecture_id=${lectureId} and difference_question>0 group by lecture_id) as qe,
(select sum(difference_material) as v from lecture_keywords
where lecture_id=${lectureId} and difference_material>0 group by lecture_id) as me
set question_error=qe.v, material_error=me.v
where lecture_id=${lectureId};`;

  const question = async () => {
    await db.getQueryResult(questionSql);
    await db.getQueryResult(questionNullSql);
  };
  const material = async () => {
    await db.getQueryResult(materialSql);
    await db.getQueryResult(materialNullSql);
  };

  // const practices = async () => {
  //   await db.getQueryResult()
  //   await db.getQueryResult
  // }

  // const survey = async () => {
  //   await db.getQueryResult()
  //   await db.getQueryResult()
  // }

  await db.getQueryResult(initSql);
  await db.getQueryResult(ratioSql);
  await Promise.all([question(), material()]);
  await db.getQueryResult(coverageInitSql);
  await db.getQueryResult(coverageSql);
};

/////// ---------------------------------------------------------------------------      위에는 지건호 작성       ---------------------------------------------------------------
exports.survey = async(lectureId) => {
  const sql1 = `select survey_id from surveys where type = 0 and score is not null and score != 0 and 
  choice = '매우 그렇지 않다.<$!<>그렇지 않다.<$!<>보통이다.<$!<>그렇다.<$!<>매우 그렇다.' and lecture_item_id in 
  ( select lecture_item_id from lecture_items where lecture_id = ${lectureId})`;
  const sql02 = `delete from student_survey_item_understanding_logs where survey_id i n ()`;

  const sql2 = `insert into student_survey_item_understanding_logs select  slog.student_id, sk.lecture_id, slog.survey_id, (select sum(score_portion) from survey_keywords
   where survey_id = slog.survey_id) as score ,  (slog.answer+1)/5 as survey_result  , (slog.answer+1)/5 as survey_understanding ,sk.keyword, 
   sk.score_portion/(select sum(score_portion) from survey_keywords where survey_id = slog.survey_id) as survey_keyword_score_ratio , 
   sk.score_portion/(select sum(score_portion) from survey_keywords where survey_id = slog.survey_id) * (slog.answer+1)/5  as survey_keyword_understanding_score_ratio 
   from student_surveys as slog, survey_keywords as sk  where sk.survey_id = slog.survey_id and sk.survey_id in (13,14,15,16)`;

  const sql3 = `insert into student_survey_understanding_logs select student_id, lecture_id,keyword, sum(score * survey_keyword_score_ratio) / ( select sum(score*survey_keyword_score_ratio) 
   from student_survey_item_understanding_logs as s1 where s1.lecture_id = s2.lecture_id) as survey_keyword_score_ratio, 
   sum(score *survey_keyword_score_ratio*survey_understanding) / (select sum(score*survey_keyword_score_ratio) from student_survey_item_understanding_logs as s1
    where s1.lecture_id = s2.lecture_id)   as survey_keyword_understanding_score_ratio from student_survey_item_understanding_logs as s2
     group by lecture_id, student_id, keyword`;

  const sql4 = `insert into student_survey_keyword_coverages select ul.student_id, ul.lecture_id , lk.keyword as class_keyword , lk.weight_ratio as class_keyword_ratio,
   ul.keyword as lecture_survey_keyword, ul.survey_keyword_understanding_score_ratio/ ul.survey_keyword_score_ratio as survey_keyword_understanding_ratio  
   from  lecture_keywords lk , student_survey_understanding_logs ul where ul.lecture_id = lk.lecture_id and ul.keyword = lk.keyword`

}


exports.metarialScoreSum = async (lectureId) => {
  const sql = `update materials as m, (select sum(score_portion) as v, 
material_id from material_keywords where lecture_id=${lectureId} group by material_id)
as r set m.score_sum=r.v where r.material_id=m.material_id`;
  await db.getQueryResult(sql);
};
