const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
// const errorBuilder = require('./modules/error-builder');
const fs = require('fs');
const socketIO = require('socket.io');
const https = require('https');
const http = require('http');
const realTimeSocket = require('./routers/realtime_socket');
const models = require('./models');
const config = require('./config');
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false, limit: '1024mb' }));
app.use(bodyParser.json({ limit: '1024mb' }));
app.use(morgan('dev'));

app.use(express.static('public'));

let options;
if (config.protocol === 'HTTPS' || config.protocol === 'https') {
  options = {
    key: fs.readFileSync('credential/wiseswot.private.key'),
    cert: fs.readFileSync('credential/www.wiseswot.com.crt'),
  };
}

app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use((err, req, res, next) => {
  if (!err.notDisplay) console.log(err);
  res.status(err.status || 500);

  res.json({
    error: true,
    status: err.status || 500,
    message: err.message,
  });
});

models.sequelize.sync()
  .then(() => {
    if (config.protocol === 'HTTPS' || config.protocol === 'https') {
      return https.createServer(options, app).listen(config.socket_port);
    }
    return http.createServer(app).listen(config.socket_port);
  })
  .then((server) => {
    const io = socketIO.listen(server, {
      pingInterval: 5000,
    });
    app.use('/', realTimeSocket(io));
    console.log(`Socket-Server is running on port ${config.socket_port}`);
  });

//   var httpserver = https.createServer(options, app).listen(8001, () => {
//    console.log('Express is running');
//  });

// https.createServer(options, app).listen(config.port, () => {
//   console.log(`Express is running on port ${config.port}`);
// });
