module.exports = (sequelize, DataTypes) => {
  const problem_judge_log = sequelize.define('problem_judge_log', {
    log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    question_id: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    solution_id: {
      type: DataTypes.INTEGER,
    },
    testcase_id: {
      type: DataTypes.INTEGER,
    },
    input: {
      type: DataTypes.TEXT,
    },
    output: {
      type: DataTypes.TEXT,
    },
    expected_output: {
      type: DataTypes.TEXT,
    },
    correct: {
      type: DataTypes.BOOLEAN
    },
    time: {
      type: DataTypes.INTEGER,
    },
    memory: {
      type: DataTypes.INTEGER,
    },
    time_limit: {
      type: DataTypes.INTEGER,
    },
    memory_limit: {
      type: DataTypes.INTEGER,
    },
    comment: {
      type: DataTypes.STRING
    },
  }, {
    underscored: true,
  });

  problem_judge_log.associate = (models) => {
    models.problem_judge_log.belongsTo(models.question, { onDelete: 'cascade', foreignKey: 'question_id' });
    models.problem_judge_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.problem_judge_log.belongsTo(models.problem_testcase, { onDelete: 'cascade', foreignKey: 'testcase_id' });
    models.problem_judge_log.belongsTo(models.oj_solution, { onDelete: 'cascade', foreignKey: 'solution_id' });
  };

  return problem_judge_log;
};
