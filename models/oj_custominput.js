module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_custominput', {
    solution_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    input_text: {
      type: DataTypes.TEXT,
    },
  }, {
    timestamps: false,
    tableName: 'oj_custominput',
  });

  // model.associate = (models) => {
  //   models.oj_custominput.belongsTo(models.oj_solution, { foreignKey: 'solution_id' });
  // };

  return model;
};
