module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_file', {
    file_guid: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    static_path: {
      type: DataTypes.STRING,
    },
    client_path: {
      type: DataTypes.STRING,
    },
    file_type: {
      type: DataTypes.STRING,
    },
    name: {
      type: DataTypes.STRING,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
    jupyter_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null,
    },
  }, {
    underscored: true,
  });
 
  model.associate = (models) => {
    models.bank_file.belongsTo(models.bank_class, { foreignKey: 'class_id' });
    models.bank_file.belongsTo(models.bank_material, { foreignKey: 'material_id' });
    models.bank_file.belongsTo(models.bank_question, { foreignKey: 'question_id' });
    models.bank_file.belongsTo(models.bank_question, { as: 'question_material', foreignKey: 'question_id_material' });
    models.bank_file.belongsTo(models.user, { foreignKey: 'uploader_id' });
    models.bank_file.belongsTo(models.bank_homework, { foreignKey: 'homework_id' });
    models.bank_file.belongsTo(models.bank_survey, { foreignKey: 'survey_id' });
    models.bank_file.belongsTo(models.bank_note, { foreignKey: 'note_id' });
  };

  model.selects = {
    fileClient: ['file_guid', 'client_path', 'file_type', 'name'],
  };

  return model;
};
