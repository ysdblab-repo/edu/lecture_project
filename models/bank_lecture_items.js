module.exports = (sequelize, DataTypes) => {
  const lectureItem = sequelize.define('bank_lecture_item', {
    lecture_item_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    start_time: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      comment: '강의 시작후 활성화 시간 sec',
    },
    end_time: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      comment: '강의 시작후 활성화 종료 시간 sec',
    },
    offset: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      // comment: '0문제지, 1설문지, 2자료',
      comment: '0 문항, 1 설문, 2 실습, 3 토론, 4 자료, 5 코딩과제',
    },
    past_opened: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '과거에 임시활성화로 제출을 받았는가?',
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    sequence: {
      type: DataTypes.INTEGER,
    },
    result: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '닫힌 이후에 결과 비공개0 / 닫힌 이후에 결과 공개1 / 공개 안하면서 중복제출 불가2',
    },
    opened: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0: 비공개, 1: 공개(닫힘), 2: 진행중(열림), 3: 끝남(결과 공개) 4: 끝남(결과 비공개)',
      // 근데 실제로 쓰이는건 임시 비활성화냐 아니냐로 쓰고 있는 것 같음 (2018/03/28 09:54 안동진 작성)
    },
    scoring_finish: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  });

  lectureItem.associate = (models) => {
    models.bank_lecture_item.belongsTo(models.bank_lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.bank_lecture_item.hasMany(models.bank_question, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_lecture_item.hasMany(models.bank_survey, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_lecture_item.hasMany(models.bank_homework, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_lecture_item.hasMany(models.bank_lecture_code_practice, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_lecture_item.hasOne(models.bank_discussion_info, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_lecture_item.hasMany(models.bank_note, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_lecture_item.belongsTo(models.serial_group, { foreignKey: 'lecture_item_serial_no' });
    models.bank_lecture_item.belongsTo(models.bank_group, { foreignKey: 'group_id' });
    models.bank_lecture_item.belongsTo(models.user, { foreignKey: 'latest_store_teacher_id' });
    models.bank_lecture_item.hasMany(models.bank_lecture_item_keyword, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_lecture_item.belongsTo(models.bank_history, { onDelete: 'cascade', foreignKey: 'bank_history_id' });
  };

  lectureItem.selects = {
    lcItemInfo: ['lecture_item_id', 'name', 'start_time', 'end_time', 'type', 'order', 'sequence', 'result', 'latest_store_teacher_id', 'offset'],
    name: ['lecture_item_id', 'name'],
  };
  return lectureItem;
};
