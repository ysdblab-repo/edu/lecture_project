module.exports = (sequelize, DataTypes) => {
  const individualQuestionInfo = sequelize.define('individual_question_info', {
    info_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    type: {
      type: DataTypes.INTEGER,
    },
    difficulty: {
      type: DataTypes.STRING,
    },
    keyword: {
      type: DataTypes.STRING,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    underscored: true,
  });

  individualQuestionInfo.associate = (models) => {
    models.individual_question_info.belongsTo(models.lecture_item, { onDelete: 'cascade',  foreignKey: 'lecture_item_id' });
    models.individual_question_info.hasMany(models.question, { foreignKey: 'info_id' });
    models.individual_question_info.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'creator_id' });
  };

  return individualQuestionInfo;
};
