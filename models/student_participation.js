module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_participation', {
    student_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    question_participation_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
    },
    question_cnt: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    survey_participation_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
    },
    survey_cnt: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    practice_participation_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
    },
    practice_cnt: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    discussion_participation_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
    },
    discussion_cnt: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.student_practice_log.belongsTo(models.user, { foreignKey: 'user_id' });
    models.student_practice_log.belongsTo(models.lecture_code_practice, { foreignKey: 'lecture_code_practice_id' })
  };

  return model;
};
