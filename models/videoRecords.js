module.exports = (sequelize, DataTypes) => {
  const videoRecord = sequelize.define('videoRecord', {
    video_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    file_path: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    comment: {
      type: DataTypes.TEXT,
      allowNull: false,
    }
  }, {
		underscored: true,
		timestamps: false,
  });

  videoRecord.associate = (models) => {
    models.videoRecord.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return videoRecord;
}