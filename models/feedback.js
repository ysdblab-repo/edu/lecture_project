module.exports = (sequelize, DataTypes) => {
  const feedback = sequelize.define('feedback', {
    feedback_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    feedback: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    sql_out: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    sql_result_code: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    sql_error_message: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
  }, {
    underscored: true,
  });

  feedback.associate = (models) => {
    models.feedback.belongsTo(models.student_answer_log, { onDelete: 'cascade', foreignKey: 'student_answer_log_id' });
  };

  return feedback;
};
