module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('small_meeting_room',{
    room_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0
      // 0 - 정규, 1 - 임시
    },
    max_occupancy: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    class_id: {
      type: DataTypes.INTEGER,
    },
    // 생성자
    user_id: {
      type: DataTypes.INTEGER,
    },
    // 2021-04-13 추가
    // 세션 -> [회의방]개설 시 사용
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    password: {
      type: DataTypes.STRING,
      defaultValue: null,
    }
  }, {
    timestamps: true,
  });

  model.associate = (models) => {
    models.small_meeting_room.belongsTo(models.class, { foreignKey: 'class_id' });
    models.small_meeting_room.belongsTo(models.user, { foreignKey: 'user_id' });
    models.small_meeting_room.hasMany(models.small_meeting_room_log, { onDelete: 'cascade', foreignKey: 'room_id' });
    models.small_meeting_room.hasMany(models.small_group_chatting, { onDelete: 'cascade', foreignKey: 'room_id' });
    models.small_meeting_room.hasMany(models.file, { foreignKey: 'room_id' });
  }

  return model;
}
