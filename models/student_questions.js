module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_question', {
    student_question_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    type: {
      type: DataTypes.INTEGER
    },
    name: {
      type: DataTypes.STRING,
    },
    question: {
      type: DataTypes.STRING,
    },
    choice: {
      type: DataTypes.STRING
    },
    answer: {
      type: DataTypes.STRING,
    }, 
    score: {
      type: DataTypes.INTEGER,
    },
    difficulty: {
      type: DataTypes.INTEGER
    },
    fileCnt: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    scoring_avg: {
      type: DataTypes.FLOAT,
    },
    scoring_cnt: {
      type: DataTypes.INTEGER,
    },
    student_id : {
      type : DataTypes.INTEGER
    },
    isCheck: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  });
  model.associate = (models) => {
    models.student_question.belongsTo(models.lecture, { foreignKey : 'lecture_id'});
    models.student_question.hasMany(models.student_question_keyword, { foreignKey: 'student_question_id'});
    models.student_question.hasMany(models.student_question_file, { foreignKey: 'student_question_id' });

  };

  return model;
}