module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('location',{
    location_id : {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    longitude: {
      type: DataTypes.FLOAT,
    },
    latitude:{
      type: DataTypes.FLOAT
    },
    location_type: {
      type: DataTypes.INTEGER,
    },
    thumnail: {
      type: DataTypes.STRING
    }
  });
  return model;

}