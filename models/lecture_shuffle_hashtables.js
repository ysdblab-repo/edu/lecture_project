module.exports = (sequelize, DataTypes) => {

  const model = sequelize.define('lecture_shuffle_hashtable', {
   
    len: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    id : {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    shuffle: {
      type: DataTypes.STRING,
    }
  });

  return model;

}