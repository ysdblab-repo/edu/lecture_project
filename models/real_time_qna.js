module.exports = (sequelize, DataTypes) => {
  const realTimeQna = sequelize.define('real_time_qna', {
    real_time_qna_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    content: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

  realTimeQna.associate = (models) => {
    models.real_time_qna.belongsTo(models.user, { foreignKey: 'user_id' });
    models.real_time_qna.belongsTo(models.lecture, { foreignKey: 'lecture_id' });
  };

  return realTimeQna;
};
