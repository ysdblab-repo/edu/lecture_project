module.exports = (sequelize, DataTypes) => {
  const homework = sequelize.define('bank_homework', {
    homework_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });

  homework.associate = (models) => {
    models.bank_homework.belongsTo(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_homework.hasMany(models.bank_file, { foreignKey: 'homework_id' });
    models.bank_homework.hasMany(models.bank_homework_keyword, { onDelete: 'cascade', foreignKey: 'homework_id'});
  };

  return homework;
};
