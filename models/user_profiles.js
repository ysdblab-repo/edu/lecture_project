module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('user_profile', {
    profile_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    material_type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    }
  }, {
    underscored: true,
  });

  // model.associate = (models) => {
  //   models.material.hasMany(models.material_keyword, { onDelete: 'cascade', foreignKey: 'material_id' });
  //   models.material.hasOne(models.file, { foreignKey: 'material_id' });
  // };

  return model;
};
