module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_survey_item_understanding_log', {
    student_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    survey_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    score: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.FLOAT,
    },
    item_understanding: {
      type: DataTypes.FLOAT,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    item_keyword_score_ratio: {
      type: DataTypes.FLOAT,
    },
    item_keyword_understanding_score_ratio: {
      type: DataTypes.FLOAT,
    },
  }, {
    timestamps: false,
  });

  return model;
};
