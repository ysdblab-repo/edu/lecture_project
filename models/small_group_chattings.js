module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('small_group_chatting', {
    small_group_chating_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    room_id: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    content: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    origin_room_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    }
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.small_group_chatting.belongsTo(models.user, { foreignKey: 'user_id' });
    models.small_group_chatting.belongsTo(models.small_meeting_room, { foreignKey: 'room_id' });
  };

  return model;
};