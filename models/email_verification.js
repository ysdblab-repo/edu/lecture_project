module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('email_verification', {
    email_verification_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    rand: {
      type: DataTypes.STRING(1000),
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.email_verification.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return model;
};
