module.exports = (sequelize, DataTypes) => {
  const materials = sequelize.define('material', {
    material_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    score_sum: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    material_type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    }
  }, {
    underscored: true,
  });

  materials.associate = (models) => {
    models.material.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.material.belongsTo(models.user, { foreignKey: 'creator_id' });
    models.material.hasMany(models.material_keyword, { onDelete: 'cascade', foreignKey: 'material_id' });
    models.material.hasOne(models.file, { foreignKey: 'material_id' });
  };

  return materials;
};
