module.exports = (sequelize, DataTypes) => {
  const studentSurvey = sequelize.define('student_survey', {
    student_survey_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    order: {
      type: DataTypes.INTEGER
    },
    answer: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    score: {
      type: DataTypes.FLOAT,      
    },
    ratio: {
      type: DataTypes.FLOAT,      
    },
    type : {
      type: DataTypes.INTEGER,      
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    item_id: {
      type: DataTypes.INTEGER,
    },
    count : {
      type : DataTypes.INTEGER
    },
    survey_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    student_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
  }, {
    underscored: true,
  });

  studentSurvey.associate = (models) => {
    models.student_survey.belongsTo(models.survey, { onDelete: 'cascade', foreignKey: 'survey_id' });
    models.student_survey.belongsTo(models.user, { foreignKey: 'student_id' });
  };

  return studentSurvey;
};
