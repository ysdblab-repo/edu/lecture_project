module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_oj_problem', {
    problem_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      defaultValue: '',
    },
    description: {
      type: DataTypes.TEXT,
    },
    input: {
      type: DataTypes.TEXT,
    },
    output: {
      type: DataTypes.TEXT,
    },
    sample_input: {
      type: DataTypes.TEXT,
    },
    sample_output: {
      type: DataTypes.TEXT,
    },
    spj: {
      type: DataTypes.STRING(1),
      defaultValue: '0',
    },
    hint: {
      type: DataTypes.TEXT,
    },
    source: {
      type: DataTypes.STRING,
    },
    in_date: {
      type: DataTypes.DATE,
      defaultValue: null,
    },
    time_limit: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    memory_limit: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    defunct: {
      type: DataTypes.STRING(1),
      defaultValue: 'N',
    },
    accepted: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    submit: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    solved: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    timestamps: false,
    tableName: 'bank_oj_problem',
  });

  model.associate = (models) => {
    models.bank_oj_problem.belongsTo(models.bank_question, { onDelete: 'cascade', foreignKey: 'problem_id' });
  };

  return model;
};
