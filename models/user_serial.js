module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('user_serial', {
  },{
    timestamps: false,
  },{
        indexes: [
                {
        primaryKey: true,
        fields: ['user_id', 'serial_no'],
                }
    ]
  });
  model.associate = (models) => {
    models.user_serial.belongsTo(models.user, { foreignKey: 'user_id' });
    models.user_serial.belongsTo(models.serial_group, { foreignKey: 'serial_no' });
  };
  model.removeAttribute('id');

  model.selects = {
    nothing: [],
  };
  return model;
};
