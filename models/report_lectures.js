module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('report_lecture', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    class_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    activated: {
      type: DataTypes.BOOLEAN,
      defaultValue: 1,
    },
  },{
    underscored: true,
  });

  return model;
};
