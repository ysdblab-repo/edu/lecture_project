module.exports = (sequelize, DataTypes) => {
  const classes = sequelize.define('class', {
    class_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    summary: {
      type: DataTypes.TEXT,
    },
    description: {
      type: DataTypes.TEXT,
    },
    capacity: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    start_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    end_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    opened: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    lecturer_description: {
      type: DataTypes.TEXT,
    },
    department_name: {
      type: DataTypes.STRING,
    },
    university_name: {
      type: DataTypes.STRING,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    location: {
      type: DataTypes.STRING,
    },
    day_of_week: {
      type: DataTypes.STRING,
    },
    code: {
      type: DataTypes.STRING,
    },
    portNum: {
      type: DataTypes.INTEGER,
    },
    practice: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    view_type: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
      comment: '0: 수강신청 시 비공개, 1: 수강신청 시 공개',
    },
    sign_up_start_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      comment: '수강신청 시작 시간'
    },
    sign_up_end_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      comment: '수강신청 종료 시간'
    },
    num: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
      comment: '학정 번호'
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1,
      comment: '과목 타입'
    },
    isHide: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '과목 숨기기 여부(0: 보이기, 1: 숨기기)'
    },
    isOpenSpace: {
      type: DataTypes.STRING,
      allowNull: true,
      comment: '개방공간 공개여부(null: 비공개, value: open2nd_id, isOpen)'
    },
  }, {
    underscored: true,
  });

  classes.associate = (models) => {
    models.class.hasMany(models.lecture, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.belongsToMany(models.user, { through: models.user_class, foreignKey: 'class_id' });
    models.class.hasMany(models.student_answer_log, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.hasMany(models.user_class, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.belongsTo(models.user, { as: 'master', foreignKey: 'master_id' });
    models.class.hasMany(models.board, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.hasMany(models.notice, { foreignKey: 'class_id' });
    models.class.hasMany(models.main_class, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.hasOne(models.file, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.belongsTo(models.serial_group, { foreignKey: 'class_serial_no' });
    models.class.hasMany(models.question_keyword_journaling, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.hasOne(models.class_password, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.hasMany(models.enrollment, { foreignKey: 'class_id' });
    models.class.hasMany(models.class_enroll_password, { foreignKey: 'class_id' });
    models.class.hasMany(models.connect_session, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.hasMany(models.connect_session_permission, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.hasMany(models.req_create_class, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.class.hasMany(models.user_class_delete_log, { onDelete: 'cascade', foreignKey: 'class_id' });
    // coding practice part
    // models.class.hasMany(models.similarity_score, {foreignKey: 'class_id'});
    // models.class.hasMany(models.coding_assignment, {foreignKey: 'class_id'});
  };

  classes.selects = {
    set1: [
      'class_id',
      'code',
      'name',
      'isActive',
      'description',
      'location',
      'day_of_week',
      'department_name',
      'university_name',
      'capacity',
      [sequelize.fn('date_format', sequelize.col('start_time'), '%Y-%m-%d'), 'start_date'],
      [sequelize.fn('date_format', sequelize.col('end_time'), '%Y-%m-%d'), 'end_date'],
      'created_at',
      'updated_at',
    ],
  };

  return classes;
};
