module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('question_keyword', {
    question_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.question_keyword.belongsTo(models.question, { onDelete: 'cascade', foreignKey: 'question_id' });
  };

  return model;
};
