module.exports = (sequelize, DataTypes) => {
  
  const model = sequelize.define('student_question_file',{
    id: {
      type : DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    lecture_id: {
      type : DataTypes.INTEGER,
    },
    student_question_id: {
      type : DataTypes.INTEGER,
    },
    file_url: {
      type: DataTypes.STRING,
    },
    file_name: {
      type: DataTypes.STRING,
    },
    user_id: {
      type : DataTypes.INTEGER,
    },
  });

  model.associate = (models) => {
    models.student_question_keyword.belongsTo(models.student_question, { onDelete: 'cascade', foreignKey: 'student_question_id' });
  };

  return model;
}