// 무인 개인에서 학생이 문항을 풀거나 해당 문항의 그룹의 시간이 끝났을때 강의에 재접속시 보이지 않게 하기 위한 테이블
module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_item_shown_log', {
    log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    student_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.lecture_item_shown_log.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.lecture_item_shown_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'student_id' });
  };

  return model;
};
