module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_sim', {
    s_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    sim_s_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    sim: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
  }, {
    timestamps: false,
    tableName: 'oj_sim',
  });

  // model.associate = (models) => {
  //   models.oj_compileinfo.belongsTo(models.oj_solution, { foreignKey: 'solution_id' });
  // };

  return model;
};
