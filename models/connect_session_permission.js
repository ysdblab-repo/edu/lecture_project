module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('connect_session_permission', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
  }, {
    timestamps: true,
  });

  model.associate = (models) => {
    models.connect_session_permission.belongsTo(models.connect_session, { foreignKey: 'connect_session_id' });
    models.connect_session_permission.belongsTo(models.lecture, { foreignKey: 'lecture_id' });
    models.connect_session_permission.belongsTo(models.user, { foreignKey: 'user_id' });
    models.connect_session_permission.belongsTo(models.class, { foreignKey: 'class_id' });
  };

  return model;
};
