module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_lecture_face', {
    student_lecture_face_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
	  autoIncrement: true,
    },
	path: {
      type: DataTypes.STRING,
    },
	face_vector: {
	  type: DataTypes.STRING(10000),
	},
	flag: {
	  type: DataTypes.INTEGER,
	},
	order: {
	  type: DataTypes.INTEGER,
	  defaultValue: 1,
	},
  }, {
	underscored: true,
  });

  model.associate = (models) => {
    models.student_lecture_face.belongsTo(models.user, { foreignKey: 'user_id' });
	models.student_lecture_face.belongsTo(models.lecture, { foreignKey: 'lecture_id' });
  };

  return model;
};
