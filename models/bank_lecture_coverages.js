module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_lecture_coverage', {
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    question_error: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    material_error: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.bank_lecture_coverage.belongsTo(models.bank_lecture, { foreignKey: 'lecture_id' });
  };

  return model;
};
