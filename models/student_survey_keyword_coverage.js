module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_survey_keyword_coverage', {
    student_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    class_keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    class_keyword_ratio: {
      type: DataTypes.FLOAT,
    },
    lectures_survey_keyword: {
      type: DataTypes.STRING,
    },
    survey_keyword_understanding_ratio: {
      type: DataTypes.FLOAT,
    }
  }, {
    timestamps: false,
  });

  return model;
};
