module.exports = (sequelize, DataTypes) => {

  const model = sequelize.define('student_question_estimate',{
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
	    autoIncrement: true,
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    student_question_id: {
      type: DataTypes.INTEGER,
    },
    score: {
      type: DataTypes.INTEGER,
    },
    comment: {
      type: DataTypes.STRING,
      defaultValue: ''
    }
  });

  return model;
}