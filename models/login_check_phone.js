module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('login_check_phone', {
    login_check_phone_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    flag: {
      type: DataTypes.BOOLEAN,
    },
    ip: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.login_check_phone.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return model;
};
