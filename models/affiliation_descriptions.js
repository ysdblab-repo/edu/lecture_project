module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('affiliation_description', {
    affiliation_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    logo: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    isOpen: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
    },
  }, {
    timestamps: true,
  });

  
  model.associate = (models) => { 
    models.affiliation_description.hasMany(models.class_belonging, { foreignKey: 'affiliation_id' });
    models.affiliation_description.hasMany(models.user_belonging, { foreignKey: 'affiliation_id' });
    models.affiliation_description.hasMany(models.class_passwords_by_admin, { foreignKey: 'affiliation_id' });
    models.affiliation_description.hasMany(models.open_space_2nd, { foreignKey: 'affiliation_id' });
    models.affiliation_description.hasMany(models.req_create_class, { foreignKey: 'affiliation_id' });

  };

  return model;
};