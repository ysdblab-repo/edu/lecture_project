module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_material_keyword',{
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0
      // 0 - youtube
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    weight: {
      type: DataTypes.INTEGER
    }
  });
  return model;
}