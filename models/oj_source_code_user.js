module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_source_code_user', {
    solution_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    source: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  }, {
    timestamps: false,
    tableName: 'oj_source_code_user',
  });

  // model.associate = (models) => {
  //   models.oj_source_code_user.belongsTo(models.oj_solution, { foreignKey: 'solution_id' });
  // };

  return model;
};
