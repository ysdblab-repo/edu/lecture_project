module.exports = (sequelize, DataTypes) => {
  const plist = sequelize.define('plist', {
    plist_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
  }, {
    underscored: true,
  });

  plist.associate = (models) => {
    models.plist.hasMany(models.lecture_accept_plist, { onDelete: 'cascade', foreignKey: 'plist_id' });
  };

  return plist;
};
