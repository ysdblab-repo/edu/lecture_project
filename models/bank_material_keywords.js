module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_material_keyword', {
    material_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    score: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.bank_material_keyword.belongsTo(models.bank_material, { onDelete: 'cascade', foreignKey: 'material_id' });
  };

  model.selects = {
    keywordInfo: ['material_id', 'keyword', 'score'],
  };

  return model;
};
