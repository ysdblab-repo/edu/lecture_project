module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_lecture_understanding_log', {
    pk_id:{
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0 - 본강, 1 - 복습, 2 - 예습'
    },
    keyword: {
      type: DataTypes.STRING,
    },
    item_keyword_score_ratio: {
      type: DataTypes.FLOAT,
    },
    item_keyword_understanding_score_ratio: {
      type: DataTypes.FLOAT,
    }
  }, {
    timestamps: false,
  });
  model.associate = (models) => {
    models.student_lecture_understanding_log.belongsTo(models.lecture_start, { onDelete: 'cascade', foreignKey: 'lecture_start_id' });

  }
  return model;
};
