module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('find_password', {
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    rand: {
      type: DataTypes.STRING(1000),
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.find_password.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id'});
  };

  return model;
};
