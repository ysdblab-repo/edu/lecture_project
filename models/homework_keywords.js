module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('homework_keyword', {
    homework_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.homework_keyword.belongsTo(models.homework, { onDelete: 'cascade', foreignKey: 'homework_id' });
  };
  return model;
};
