module.exports = (sequelize, DataTypes) => {
	const model = sequelize.define('individual_question_student_indice', {
		student_indice_id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		student_id: {
			type: DataTypes.INTEGER,
		},
		lecture_item_id: {
			type: DataTypes.INTEGER,
		},
		question_indice: {
			type: DataTypes.INTEGER,
		},
	}, {
		underscored: true,
		timestamps: true,
	});

	model.associate = (models) => {
		models.individual_question_student_indice.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'student_id' });
		models.individual_question_student_indice.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
	};

	return model;
};
  