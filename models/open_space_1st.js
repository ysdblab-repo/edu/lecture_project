module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('open_space_1st',{
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.open_space_1st.hasMany(models.open_space_2nd, { foreignKey: 'id_1st' });
  }

  return model;
}
