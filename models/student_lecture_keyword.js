module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_lecture_keyword', {
    student_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    understanding: {
      type: DataTypes.FLOAT,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.student_lecture_keyword.belongsTo(models.user, { foreignKey: 'student_id' });
  };

  return model;
};
