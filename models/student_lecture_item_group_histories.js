module.exports = (sequelize, DataTypes) => {
  const student_lecture_item_group_histories = sequelize.define('student_lecture_item_group_history', {
    history_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      allowNull: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      allowNull: true,
    },
    unlock_stage: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
      allowNull: false,
    }
  }, {
    underscored: true,
  });

  student_lecture_item_group_histories.associate = (models) => {
    models.student_lecture_item_group_history.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.student_lecture_item_group_history.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return student_lecture_item_group_histories;
};
