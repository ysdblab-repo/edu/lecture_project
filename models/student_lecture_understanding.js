module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_lecture_understanding', {
    student_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    avg_understanding: {
      type: DataTypes.FLOAT,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.student_lecture_understanding.belongsTo(models.user, { foreignKey: 'student_id' });
  };

  return model;
};
