module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_ping_log', {
    ping_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    href: {
      type: DataTypes.STRING,
    }
  }, {
    underscored: true,
    timestamps: true,
  });

  model.associate = (models) => {
    models.lecture_ping_log.belongsTo(models.lecture, { foreignKey: 'lecture_id' });
    models.lecture_ping_log.belongsTo(models.user, { foreignKey: 'user_id' });
  };

  return model;
};
