module.exports = (sequelize, DataTypes) => {
  const questions = sequelize.define('bank_question', {
    question_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0객관식, 1단답형, 2장문, 3파일 제출',
    },
    question: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '문제 글',
    },
    choice: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '객관식 선택지 구분자로 구분',
    },
    choice2: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '선긋기 선택지 구분자로 구분',
    },
    answer: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '답이 여러개일경우 구분자로 구분',
    },
    is_ordering_answer: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '단답형에서 답의 순서가 필요한지 0: 노필요, 1: 필요',
    },
    accept_language: {
      type: DataTypes.STRING(1000),
    },
    order: {
      type: DataTypes.INTEGER,
      comment: '문제의 순서',
    },
    showing_order: {
      type: DataTypes.INTEGER,
      comment: '문제가 보여지는 순서',
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      comment: '배점',
    },
    difficulty: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      comment: '난이도',
    },
    timer: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null,
      comment: '문제 제한시간',
    },
    sqlite_file_guid: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
    question_media_type: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    question_media: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    answer_media_type: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
  }, {
    underscored: true,
  });

  questions.associate = (models) => {
    models.bank_question.belongsTo(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_question.hasMany(models.bank_problem_testcase, { onDelete: 'cascade', foreignKey: 'question_id' });
    models.bank_question.hasMany(models.bank_file, { foreignKey: 'question_id' });
    models.bank_question.hasMany(models.bank_file, { as: 'question_material', foreignKey: 'question_id_material' });
    
    models.bank_question.hasOne(models.bank_oj_problem, { onDelete: 'cascade', foreignKey: 'problem_id' });
    // belongsTo에서 hasMany로 수정
    models.bank_question.hasMany(models.bank_question_keyword, { onDelete: 'cascade', foreignKey: 'question_id' });
  };

  questions.selects = {
    questionStudent: ['question_id', 'type', 'question', 'choice', 'choice2', 'question_media', 'question_media_type', 'answer_media_type', 'score',
      'order', 'showing_order', 'difficulty', 'timer', 'lecture_item_id'],
    nothing: [],
  };

  return questions;
};
