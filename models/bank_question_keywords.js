module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_question_keyword', {
    question_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.bank_question_keyword.belongsTo(models.bank_question, { onDelete: 'cascade', foreignKey: 'question_id' });
    models.bank_question_keyword.belongsTo(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
  };

  return model;
};
