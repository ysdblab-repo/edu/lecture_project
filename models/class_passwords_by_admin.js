module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('class_passwords_by_admin', {
    class_password_id: {
      type: DataTypes.INTEGER,
      autoIncrement:true,
      primaryKey: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    create_check: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: '0',
    },
  });

  model.associate = (models) => {
    models.class_passwords_by_admin.belongsTo(models.affiliation_description, {onDelete: 'cascade', foreignKey:'affiliation_id'});
    models.class_passwords_by_admin.belongsTo(models.req_create_class, {onDelete: 'cascade', foreignKey:'req_id'});
  };

  return model;
};