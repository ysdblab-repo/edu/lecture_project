module.exports = (sequelize, DataTypes) => {
  const classes = sequelize.define('bank_class', {
    class_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    summary: {
      type: DataTypes.TEXT,
    },
    description: {
      type: DataTypes.TEXT,
    },
    capacity: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    start_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    end_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    origin_created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    origin_updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    opened: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    lecturer_description: {
      type: DataTypes.TEXT,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
    timestamp: true,
  });

  classes.associate = (models) => {
    models.bank_class.belongsTo(models.user, { foreignKey: 'latest_store_teacher_id' });
    models.bank_class.hasMany(models.bank_lecture, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.bank_class.hasOne(models.bank_file, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.bank_class.belongsTo(models.serial_group, { as: 'serial_number', foreignKey: 'class_serial_no' });
    models.bank_class.belongsTo(models.bank_group, { foreignKey: 'group_id' });
    models.bank_class.belongsTo(models.bank_history, { onDelete:'cascade', foreignKey: 'bank_history_id'});

  };

  classes.selects = {
    classInfo: ['class_id', 'name', 'description', 'summary', 'capacity', 'start_time', 'end_time', 'lecturer_description', 'created_at', 'updated_at'],
    name: ['class_id', 'name']
  };

  return classes;
};
