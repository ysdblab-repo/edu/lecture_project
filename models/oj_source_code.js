module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_source_code', {
    solution_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    source: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  }, {
    timestamps: false,
    tableName: 'oj_source_code',
  });

  model.associate = (models) => {
    models.oj_source_code.belongsTo(models.student_answer_log, { onDelete: 'cascade',  foreignKey: 'solution_id' });
  };

  return model;
};
