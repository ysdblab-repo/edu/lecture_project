module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_question_keyword', {
    student_question_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.student_question_keyword.belongsTo(models.student_question, { onDelete: 'cascade', foreignKey: 'student_question_id' });
  };

  return model;
};
