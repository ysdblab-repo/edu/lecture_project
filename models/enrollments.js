module.exports = (sequelize, DataTypes) => {
  const enrollments = sequelize.define('enrollment', {
    class_id: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0: 대기중 / 1: 승인 / 2: 거절',
    },
  }, {
    underscored: true,
  });

  enrollments.associate = (models) => {
    models.enrollment.belongsTo(models.class, { foreignKey: 'class_id' });
    models.enrollment.belongsTo(models.user, { foreignKey: 'user_id' });
  };

  return enrollments;
};
