module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('user_class', {
    role: {
      type: DataTypes.STRING,
    },
    class_id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
  }, {
    timestamps: false,
  });
  model.associate = (models) => {
    models.user_class.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user_class.belongsTo(models.class, { onDelete: 'cascade', foreignKey: 'class_id' });
  }
  return model;
};
