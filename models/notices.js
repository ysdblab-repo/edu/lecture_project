module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('notice', {
    notice_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.TEXT,
    },
    content: {
      type: DataTypes.TEXT,
    },
    num_views: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    class_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  }, {
    timestamp: true,
    underscored: true,
  });

  model.associate = (models) => {
    models.notice.belongsTo(models.class, { foreignKey: 'class_id', onDelete: 'cascade' });
    models.notice.belongsTo(models.user, { foreignKey: 'user_id', onDelete: 'cascade' });
    models.notice.hasMany(models.file, { foreignKey: 'notice_id' });
    models.notice.hasMany(models.notice_reply, { foreignKey: 'notice_id' });
  };

  return model;
};
