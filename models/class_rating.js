module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('class_rating', {
    class_rating_id: {
      type: DataTypes.INTEGER,
      autoIncrement:true,
      primaryKey: true,
    },
    class_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    rating: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
  }, {
    timestamps: true,
  });

  model.associate = (models) => {
    models.class_rating.belongsTo(models.class, { foreignKey: 'class_id'});
    models.class_rating.belongsTo(models.user, { foreignKey:'user_id'});
  };

  return model;
};
