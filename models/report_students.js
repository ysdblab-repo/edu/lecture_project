module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('report_student', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    f_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    type:{
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    student_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    item_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    }
  },{
    underscored: true,
  });

  return model;
};
