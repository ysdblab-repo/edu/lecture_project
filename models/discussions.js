module.exports = (sequelize, DataTypes) => {
  const discussion = sequelize.define('discussion', {
    discussion_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    content: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    is_teacher: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    is_audio: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  }, {
    underscored: true,
  });

  discussion.associate = (models) => {
    models.discussion.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.discussion.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.discussion.hasMany(models.file, { foreignKey: 'discussion_id' });
  };

  return discussion;
};
