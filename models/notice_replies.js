module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('notice_reply', {
    notice_reply_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    notice_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    content: {
      type: DataTypes.TEXT,
      defaultValue: '',
      allowNull: false,
    },
    is_teacher: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
  }, {
    timestamp: true,
    underscored: true,
  });

  model.associate = (models) => {
    models.notice_reply.belongsTo(models.notice, { foreignKey: 'notice_id', onDelete: 'cascade' });
    models.notice_reply.belongsTo(models.user, { foreignKey: 'user_id', onDelete: 'cascade' });
  };

  return model;
};
