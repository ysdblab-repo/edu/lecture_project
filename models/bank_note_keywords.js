module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_note_keyword', {
    note_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.bank_note_keyword.belongsTo(models.bank_note, { onDelete: 'cascade', foreignKey: 'note_id' });
  };

  return model;
};
