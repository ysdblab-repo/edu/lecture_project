module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_privilege', {
    user_id: {
      type: DataTypes.STRING,
      defaultValue: '',
      primaryKey: true,
    },
    rightstr: {
      type: DataTypes.STRING,
      defaultValue: '',
    },
    defunct: {
      type: DataTypes.STRING,
      defaultValue: 'N',
    },
  }, {
    timestamps: false,
    tableName: 'oj_privilege',
  });

  // model.associate = (models) => {
  //   models.oj_compileinfo.belongsTo(models.oj_solution, { foreignKey: 'solution_id' });
  // };

  return model;
};
