module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('mail_auth', {
    auth_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    email_id: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    auth_number: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    timestamps: true,
  });
  return model;
};
