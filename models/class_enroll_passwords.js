module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('class_enroll_password', {
    class_enroll_password_id: {
      type: DataTypes.INTEGER,
      autoIncrement:true,
      primaryKey: true,
    },
    class_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  });

  model.associate = (models) => {
    models.class_password.belongsTo(models.class, {onDelete: 'cascade',foreignKey: 'class_id'});
  };

  return model;
};