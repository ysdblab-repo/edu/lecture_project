module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_practice_log', {
    student_practice_log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
	    autoIncrement: true,
    },
    order: {
      type: DataTypes.INTEGER,
    },
    code: {
      type: DataTypes.TEXT,
    },
    score: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
    },
    ratio: {
      type: DataTypes.INTEGER,
    },
	  modified_time: {
      type: DataTypes.DATE,
    },
    lecture_code_practice_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER
    },
    item_id: {
      type: DataTypes.INTEGER
    },
    student_id: {
      type: DataTypes.INTEGER,
    }
  });

  model.associate = (models) => {
    models.student_practice_log.belongsTo(models.user, { foreignKey: 'user_id' });
    models.student_practice_log.belongsTo(models.lecture_code_practice, { foreignKey: 'lecture_code_practice_id' })
  };

  return model;
};
