module.exports = (sequelize, DataTypes) => {
  const discussionInfo = sequelize.define('discussion_info', {
    lecture_item_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    topic: {
      type: DataTypes.STRING,
      defaultValue: '',
    },
    student_share: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    difficulty: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      comment: '난이도',
    },
  }, {
    underscored: true,
  });

  discussionInfo.associate = (models) => {
    models.discussion_info.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
  };

  return discussionInfo;
};
