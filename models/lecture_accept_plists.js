module.exports = (sequelize, DataTypes) => {
  const lecture_accept_plist = sequelize.define('lecture_accept_plist', {
    lecture_accept_plist_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
  }, {
    underscored: true,
  });

  lecture_accept_plist.associate = (models) => {
    models.lecture_accept_plist.belongsTo(models.plist, { onDelete: 'cascade', foreignKey: 'plist_id' });
    models.lecture_accept_plist.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
  };

  return lecture_accept_plist;
};
