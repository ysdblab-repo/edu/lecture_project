module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_attendance_face', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    student_id: {
      type: DataTypes.INTEGER
    },
    x_position: {
      type: DataTypes.INTEGER,
    },
    y_position: {
      type: DataTypes.FLOAT,
    },
    frame_width: {
      type: DataTypes.FLOAT,
    },
    frame_height: {
      type: DataTypes.FLOAT,
    },
    confidence: {
      type: DataTypes.FLOAT,
    }
  });
  return model;
}