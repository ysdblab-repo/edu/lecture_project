module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('serial_group', {
    serial_no: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

 
  model.associate = (models) => {
    models.serial_group.hasMany(models.bank_class, { foreignKey: 'class_serial_no' });
    models.serial_group.hasMany(models.bank_lecture, { foreignKey: 'lecture_serial_no' });
    models.serial_group.hasMany(models.bank_lecture_item, { foreignKey: 'lecture_item_serial_no' });
    models.serial_group.hasMany(models.class, { foreignKey: 'class_serial_no' });
    models.serial_group.hasMany(models.lecture, { foreignKey: 'lecture_serial_no' });
    models.serial_group.hasMany(models.lecture_item, { foreignKey: 'lecture_item_serial_no' });
    models.serial_group.hasMany(models.user_serial, { foreignKey: 'serial_no' });
  };

  model.selects = {
    serialInfo: ['serial_no', 'name'],
  };

  return model;
};
