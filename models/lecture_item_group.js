module.exports = (sequelize, DataTypes) => {

  const model = sequelize.define('lecture_item_group',{

    lecture_item_group_id: {
      type : DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    opened: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    group_list: {
      type: DataTypes.STRING,
    },
    start: {
      type: DataTypes.INTEGER,
    },
    end: {
      type: DataTypes.INTEGER
    },
    if_small_group: {
      type: DataTypes.INTEGER,
      comment: '1이면 유인강의 셔플링 - lecture_item_time_list로 // 0 또는 null이면 기본 - lecture_item_list로',
    },
    timerShow: {
      type: DataTypes.BOOLEAN,
    }
  });

  // model.associate = (models) => {
  //   models.lecture.hasMany()
  // }
  return model;
}
