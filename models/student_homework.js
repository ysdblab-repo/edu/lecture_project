module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_homework', {
    student_homework_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    feedback: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.student_homework.belongsTo(models.homework, { onDelete: 'cascade', foreignKey: 'homework_id' });
    models.student_homework.belongsTo(models.user, { foreignKey: 'student_id' });
    models.student_homework.hasMany(models.file, { foreignKey: 'student_homework_id' });
  };

  return model;
};
