module.exports = (sequelize, DataTypes) => {
  const lectureCodePractice = sequelize.define('lecture_code_practice', {
    lecture_code_practice_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    code: {
      type: DataTypes.TEXT,
    },
	  is_jupyter: {
	    type: DataTypes.INTEGER,
	    allowNull: false,
      defaultValue: 1,
    },
    language_type: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
    },
	  activated_time: {
      type: DataTypes.DATE,
	    allowNull: true,
      defaultValue: null,
    },
	  deactivated_time: {
      type: DataTypes.DATE,
	    allowNull: true,
      defaultValue: null,
    },
    score: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0,
    },
    difficulty: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      comment: '난이도',
    },
  }, {
    underscored: true,
  });

  lectureCodePractice.associate = (models) => {
    models.lecture_code_practice.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.lecture_code_practice.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.lecture_code_practice.hasMany(models.student_practice_log, { onDelete: 'cascade', foreignKey: 'student_practice_log_id'});
    models.lecture_code_practice.hasMany(models.lecture_code_practice_keyword, { onDelete: 'cascade', foreignKey: 'lecture_code_practice_id'});

  };

  return lectureCodePractice;
};
