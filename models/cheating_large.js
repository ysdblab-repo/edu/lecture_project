module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('cheating_large', {
    cheating_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    answer: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
    indexes: [{ unique: true, fields: ['lecture_item_id', 'answer'] }],
  });

  model.associate = (models) => {
    models.cheating_large.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.cheating_large.belongsToMany(models.user, { through: models.user_cheating, foreignKey: 'cheating_id' });
  };

  return model;
};
