module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('file', {
    file_guid: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    static_path: {
      type: DataTypes.STRING,
    },
    client_path: {
      type: DataTypes.STRING,
    },
    file_type: {
      type: DataTypes.STRING,
    },
    name: {
      type: DataTypes.STRING,
    },
    jupyter_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
     defaultValue: null,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.file.belongsTo(models.material, { foreignKey: 'material_id' });
    models.file.belongsTo(models.question, { foreignKey: 'question_id' });
    models.file.belongsTo(models.question, { as: 'question_material', foreignKey: 'question_id_material' });
    models.file.belongsTo(models.question, { as: 'question_multichoice', foreignKey: 'question_id_multichoice' });
    models.file.belongsTo(models.question, { as: 'question_voice', foreignKey: 'question_id_voice' });
    models.file.belongsTo(models.small_meeting_room, { foreignKey: 'room_id' });
    models.file.belongsTo(models.board, { foreignKey: 'board_id' });
    models.file.belongsTo(models.notice, { foreignKey: 'notice_id', onDelete: 'cascade' });
    models.file.belongsTo(models.user, { foreignKey: 'uploader_id' });
    models.file.belongsTo(models.homework, { foreignKey: 'homework_id' });
    models.file.belongsTo(models.survey, { foreignKey: 'survey_id' });
    models.file.belongsTo(models.note, { foreignKey: 'note_id' });
    models.file.belongsTo(models.student_homework, { foreignKey: 'student_homework_id' });
    models.file.belongsTo(models.discussion, { foreignKey: 'discussion_id' });
    models.file.belongsTo(models.class, { foreignKey: 'class_id' });
    models.file.belongsTo(models.user, { as: 'user_profile', foreignKey: 'user_id_profile' });
    models.file.belongsTo(models.student_answer_log, { foreignKey: 'student_answer_log_id' });
  
    models.file.belongsTo(models.student_question, { foreignKey: 'student_question_id'});
  };

  model.selects = {
    fileClient: ['file_guid', 'client_path', 'file_type', 'name'],
  };

  return model;
};
