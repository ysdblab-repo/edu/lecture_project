module.exports = (sequelize, DataTypes) => {
  const materials = sequelize.define('bank_material', {
    material_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    score_sum: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    material_type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });

  materials.associate = (models) => {
    models.bank_material.belongsTo(models.bank_lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.bank_material.hasMany(models.bank_material_keyword, { onDelete: 'cascade', foreignKey: 'material_id' });
    models.bank_material.hasOne(models.bank_file, { foreignKey: 'material_id' });
  };

  return materials;
};
