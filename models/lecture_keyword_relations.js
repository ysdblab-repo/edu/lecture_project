module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_keyword_relation', {
    lecture_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    node1: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    node2: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    weight: {
      type: DataTypes.INTEGER,
      defaultValue: 5,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.lecture_keyword_relation.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
  };

  model.selects = {
    lectureKeywordRelationSimple: ['lecture_id', 'node1', 'node2', 'weight'],
  };

  return model;
};
