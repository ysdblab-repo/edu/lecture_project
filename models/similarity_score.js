module.exports = (sequelize, DataTypes) => {
    const model = sequelize.define('similarity_score', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement: true,
        },
        class_id: {
            type: DataTypes.INTEGER,
        },
        email_id: {
            type: DataTypes.STRING,
            defaultValue:'email_id not found'
        },
        lecture_item_id: {
            type: DataTypes.INTEGER,
        },
        datetime: {
            type: DataTypes.DATE,
            defaultValue:'2019-01-01 00:00:00',
        },
        code_score: {
            type: DataTypes.FLOAT,
        },
        output_score:{
            type: DataTypes.FLOAT,
        },
        file_name:{
            type: DataTypes.STRING,
            defaultValue:'file_name not found'
        },
        code_str:{
             type: DataTypes.TEXT,
             defaultValue:'code string not found'
        },
        output_str:{
             type: DataTypes.TEXT,
             defaultValue:'output string not found'
        },
    });

    model.associate = (models) => {
        //models.similarity_score.belongsTo(models.lecture, { foreignKey : 'lecture_id'});
        //models.similarity_score.belongsTo(models.class, { foreignKey: 'class_id' });
        // cannot use string(object) as an foreignKey in current version
        // https://github.com/sequelize/sequelize/issues/2536
        //models.similarity_score.belongsTo(models.user, { foreignKey: 'email_id' });
        //models.similarity_score.belongsTo(models.lecture_item, { foreignKey: 'lecture_item_id' });
    };
    return model;
}
