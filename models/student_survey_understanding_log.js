module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_survey_understanding_log', {
    student_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0 - 본강, 1 - 복습, 2 - 예습'
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    survey_keyword_studscore_ratio: {
      type: DataTypes.FLOAT,
    },
    survey_keyword_understanding_score_ratio: {
      type: DataTypes.FLOAT,
    }
  }, {
    timestamps: false,
  });

  return model;
};
