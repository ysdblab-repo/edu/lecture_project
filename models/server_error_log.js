module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('server_error_log', {
    server_error_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    code: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    message: {
      type: DataTypes.TEXT,
      defaultValue: null,
    },
    stack: {
      type: DataTypes.TEXT,
      defaultValue: null,
    },
    errno: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    syscall: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    path: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    address: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    port: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    client_ip: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

  return model;
};
