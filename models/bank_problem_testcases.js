module.exports = (sequelize, DataTypes) => {
  const testcases = sequelize.define('bank_problem_testcase', {
    testcase_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    question_id: {
      type: DataTypes.INTEGER,
    },
    num: {
      type: DataTypes.INTEGER,
    },
    input: {
      type: DataTypes.TEXT,
    },
    output: {
      type: DataTypes.TEXT,
    },
    input_path: {
      type: DataTypes.STRING,
    },
    output_path: {
      type: DataTypes.STRING,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
    score_ratio: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    memory: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    timeout: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
  }, {
    underscored: true,
  });

  testcases.associate = (models) => {
    models.bank_problem_testcase.belongsTo(models.bank_question, { onDelete: 'cascade', foreignKey: 'question_id' });
  };

  testcases.selects = {
    problemTestcaseSimple: ['question_id', 'input', 'output', 'num', 'score_ratio', 'memory', 'timeout'],
  };

  return testcases;
};
