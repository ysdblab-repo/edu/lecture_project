module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_lecture_item_list', {

    lecture_item_list_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    item_id: {
      type: DataTypes.INTEGER,
    },
    linked_list: {
      type: DataTypes.STRING,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  });
  return model;
}