module.exports = (sequelize, DataTypes) => {
  const lectures = sequelize.define('lecture', {
    lecture_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    lecture_type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    location: {
      type: DataTypes.STRING(2000),
      defaultValue: null,
    },
    sequence: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    start_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    end_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    is_auto: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      // 0 : 평소상태, 1 : 강의중
    },
    result_opened: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    test_mode: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    video_link: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    keyword_state: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      // 0: 실행전, 1: 실행중, 2: 실행끝, 3: 에러
    },
    media_type: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    audio_guid: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    webcam_link: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
    },
    snapshot_share_status: { // 강의실에서 학생 간 스냅샷 전송 여부 결정
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    grade_use: { // 강의 점수에 학점 표기 여부
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    group_move: { // 그룹간 이동
      type: DataTypes.INTEGER, // 0: 불가능형, 1: 선택적 가능형, 2: 가능형
      allowNull: false,
      defaultValue: 0,
    },
    live_type: { // 생방송 유형
      type: DataTypes.INTEGER, // 0: 유튜브, 1: 강의방, 2: 사용안함
      allowNull: false,
      defaultValue: 2
    },
    programming: { // 프로그래밍 교육 여부
      type: DataTypes.INTEGER, // 0: 예, 1 아니오 
      allowNull: false,
      defaultValue: 1
    }
  }, {
    underscored: true,
  });

  lectures.associate = (models) => {
    models.lecture.belongsTo(models.class, { foreignKey: 'class_id' });
    models.lecture.belongsTo(models.user, { foreignKey: 'teacher_id' });
    models.lecture.hasMany(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.student_answer_log, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.lecture_accept_plist, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.lecture_keyword, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.lecture_keyword_relation, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasOne(models.lecture_coverage, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.student_lecture_face, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.student_selfstudy_log, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.student_lecture_log, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.teacher_streaming_key, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.realtime_lecture_heartbeat, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.heartbeat_count, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.student_lecture_timeline_log, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.material, { as: 'material', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.real_time_qna, { foreignKey: 'lecture_id' });
    models.lecture.belongsTo(models.serial_group, { foreignKey: 'lecture_serial_no' });
    models.lecture.hasMany(models.automatic_lecture_login_history, { foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.student_question, { onDelete: 'cascade', foreignKey: 'lecture_id'});
    models.lecture.hasMany(models.lecture_student_login_log, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.question_keyword_journaling, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.lecture_chating, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.student_lecture_item_group_history, { foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.connect_session, { onDelete: 'cascade', foreignKey: 'source' });
    models.lecture.hasMany(models.connect_session, { onDelete: 'cascade', foreignKey: 'target' });
    models.lecture.hasMany(models.connect_session_permission, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.face_status, { foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.face_log, { foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.face_concentration, { foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.screen_concentration, { foreignKey: 'lecture_id' });
    models.lecture.hasMany(models.lecture_ping_log, { foreignKey: 'lecture_id' });
    models.lecture.hasOne(models.lecture_grade, { foreignKey: 'lecture_id' });
  };

  return lectures;
};
