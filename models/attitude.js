module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('attitude', {
    class_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    attitude: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    underscored: true,
    indexes: [{ fields: ['user_id', 'class_id'] }],
  });

  model.associate = (models) => {
    models.attitude.belongsTo(models.class, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.attitude.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return model;
};
