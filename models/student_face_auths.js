module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_face_auth',{
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    lecture_id: {
      type: DataTypes.INTEGER
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
    x_position: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0,
    },
    y_position: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0,
    },
    frame_width: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0,
    },
    frame_height: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0,
    },
    confidence: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0,
    },
  });
  return model;
}