module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('event_notification', {
    event_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    isMaster: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    affiliation_name: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    class_name: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    lecture_name: {
      type: DataTypes.STRING,
      allowNull: true,
    },    
    event_type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    importance: {
      type: DataTypes.INTEGER,
    },
    discription: {
      type: DataTypes.STRING,
    },
    isRead: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    }
  }, {
    timestamps: true,
    underscored: true
  });

  // model.associate = (models) => {
  //   models.automatic_lecture_login_history.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
  //   models.automatic_lecture_login_history.belongsTo(models.user, { foreignKey: 'user_id' });
  // };

  return model;
}
