module.exports = (sequelize, DataTypes) => {
  const lecture_grade = sequelize.define('lecture_grade', {
    grade_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    score: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
    },
    percentage: {
      type: DataTypes.FLOAT,
      defaultValue: 0
    },
    grade: {
      type: DataTypes.STRING,
    }
  }, {
    underscored: true,
  });

  lecture_grade.associate = (models) => {
    models.lecture_grade.belongsTo(models.lecture, { foreignKey: 'lecture_id', onDelete: 'cascade' });
    models.lecture_grade.belongsTo(models.user, { foreignKey: 'user_id', onDelete: 'cascade'});
  };

  return lecture_grade;
};
