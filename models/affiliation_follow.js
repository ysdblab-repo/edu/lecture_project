module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('affiliation_follow', {
    affiliation_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    // created_at: {
    //   type: DataTypes.DATE,
    //   allowNull: true,
    //   defaultValue: null,
    // },    
  },{
    timestamps: true,
    underscored: true,
  });

  // model.associate = (models) => { 
  //   models.affiliation_description.hasMany(models.class_belonging, { foreignKey: 'affiliation_id' });
  //   models.affiliation_description.hasMany(models.user_belonging, { foreignKey: 'affiliation_id' });
  //   models.affiliation_description.hasMany(models.class_passwords_by_admin, { foreignKey: 'affiliation_id' });
  //   models.affiliation_description.hasMany(models.open_space_2nd, { foreignKey: 'affiliation_id' });
  //   models.affiliation_description.hasMany(models.req_create_class, { foreignKey: 'affiliation_id' });

  // };


  return model;
};
