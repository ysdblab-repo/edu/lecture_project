module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('user_face_model', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    user_id: {
      type: DataTypes.INTEGER
    },
    model: {
      type: DataTypes.STRING,
    },
    count: {
      // 재학습 횟수
      type: DataTypes.INTEGER,
    }
  }, {
    timestamps: false,
  });
  return model;
};
