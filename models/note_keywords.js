module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('note_keyword', {
    note_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.note_keyword.belongsTo(models.note, { onDelete: 'cascade', foreignKey: 'note_id' });
  };

  return model;
};
