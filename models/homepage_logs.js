module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('homepage_log', {
    homepage_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    ip: {
      type: DataTypes.STRING,
    },
    page_name: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.homepage_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return model;
};
