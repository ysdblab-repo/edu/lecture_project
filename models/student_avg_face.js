module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_avg_face', {
    user_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    avg_face_vector: {
      type: DataTypes.TEXT,
    },
	}, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.student_avg_face.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return model;
};
