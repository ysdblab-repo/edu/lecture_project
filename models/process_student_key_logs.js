module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('process_student_key_log', {
    process_student_key_log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    keylen: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });

  return model;
};
