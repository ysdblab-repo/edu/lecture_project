module.exports = (sequelize, DataTypes) => {
  const tsk = sequelize.define('teacher_streaming_key', {
    teacher_streaming_key_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    key: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '=login id',
    },
  }, {
    underscored: true,
  });

  tsk.associate = (models) => {
    models.teacher_streaming_key.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });

  };


  return tsk;
};
