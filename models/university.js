module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('university', {
    name: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    code: {
      type: DataTypes.STRING,
    },
    address: {
      type: DataTypes.STRING,
    },
    manager_name: {
      type: DataTypes.STRING,
    },
    manager_email: {
      type: DataTypes.STRING,
    },
    manager_phone_number: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.university.hasMany(models.department, { onDelete: 'cascade', foreignKey: 'university_name' });
  };

  model.selects = {
    set1: ['code', 'name', 'address', 'manager_name', 'manager_email', 'manager_phone_number'],
  };

  return model;
};
