module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_history', {
    bank_history_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    creator_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    take_list: {
      type: DataTypes.STRING,
    },
    type: { // 0: class, 1: lecture, 2: lecture_item,
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: true,
  });

  model.selects = {
    nothing: [],
  };
  model.associate = (models) => {
    models.bank_history.hasMany(models.bank_class, { onDelete: 'cascade', foreignKey: 'bank_history_id' });
    models.bank_history.hasMany(models.bank_lecture, { onDelete: 'cascade', foreignKey: 'bank_history_id' });
    models.bank_history.hasMany(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'bank_history_id' });
  };

  return model;
};
