module.exports = (sequelize, DataTypes) => {
  const face_status = sequelize.define('face_status', {
    log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()'),
    },
    updated_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()'),
    }
  }, {
		underscored: true,
		timestamps: false,
  });

  face_status.associate = (models) => {
    models.face_status.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey : 'lecture_id' });
  };

  return face_status;
}