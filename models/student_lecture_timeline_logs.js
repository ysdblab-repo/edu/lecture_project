module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_lecture_timeline_log', {
    student_lecture_timeline_log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
	  autoIncrement: true,
    },
    understanding_score: {
      type: DataTypes.FLOAT,
	  defaultValue: 0,
    },
	participation_score: {
      type: DataTypes.FLOAT,
	  defaultValue: 0,
    },
	concentration_score: {
      type: DataTypes.FLOAT,
	  defaultValue: 0,
    },
	log_time: {
      type: DataTypes.DATE,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.student_lecture_timeline_log.belongsTo(models.user, { foreignKey: 'user_id' });
	models.student_lecture_timeline_log.belongsTo(models.lecture, { foreignKey: 'lecture_id' });
  };

  return model;
};
