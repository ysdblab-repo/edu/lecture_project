module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('login_log', {
    login_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    ip: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.login_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return model;
};
