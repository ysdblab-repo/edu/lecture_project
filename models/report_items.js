module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('report_item', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    f_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      // 0 - 문항, 1 - 설문
    },
    item_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
  },{
    underscored: true,
  });

  return model;
};
