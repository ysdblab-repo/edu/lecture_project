module.exports = (sequelize, DataTypes) => {
  const student_lecture_log = sequelize.define('student_lecture_log', {
    student_lecture_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    start_time: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    end_time: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    participation_score: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    understanding_score: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    concentration_score: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
  }, {
    underscored: true,
  });

  student_lecture_log.associate = (models) => {
    models.student_lecture_log.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.student_lecture_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return student_lecture_log;
};
