module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('realtime_lecture_room',{
    room_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    node_number: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    socket_id: {
      type: DataTypes.STRING,
    },
    lecture_id: { // 강사 긴급 종료시, 학생들에게 신호를 보낼때 강사의 위치를 쉽게 찾기 위한 목적
      type: DataTypes.INTEGER,
    }
  }, {
    timestamps: true,
  });

  model.associate = (models) => {
    models.realtime_lecture_room.belongsTo(models.user, { foreignKey: 'user_id' });
  }

  return model;
}