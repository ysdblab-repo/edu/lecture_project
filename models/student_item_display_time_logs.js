module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_item_display_time_log', {
    student_item_display_time_log: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    student_local_time: {
      type: DataTypes.DATE,
      defaultValue: null,
    }
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.student_item_display_time_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'student_id' });
    models.student_item_display_time_log.belongsTo(models.class, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.student_item_display_time_log.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.student_item_display_time_log.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'item_id' });
  };

  return model;
};
