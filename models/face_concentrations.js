module.exports = (sequelize, DataTypes) => {
  const face_concentration = sequelize.define('face_concentration', {
    log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    concentration: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    count: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()'),
    },
    updated_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()'),
    }
  }, {
		underscored: true,
		timestamps: false,
  });

  face_concentration.associate = (models) => {
    models.face_concentration.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey : 'lecture_id' });
    models.face_concentration.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return face_concentration;
}