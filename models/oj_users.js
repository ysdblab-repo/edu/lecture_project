module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_users', {
    user_id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    email: {
      type: DataTypes.STRING,
    },
    submit: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    solved: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    defunct: {
      type: DataTypes.STRING(1),
      defaultValue: 'N',
    },
    ip: {
      type: DataTypes.STRING(32),
    },
    accesstime: {
      type: DataTypes.DATE,
      defaultValue: null,
    },
    volume: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    language: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    password: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    reg_time: {
      type: DataTypes.DATE,
      defaultValue: null,
    },
    nick: {
      type: DataTypes.STRING,
    },
    school: {
      type: DataTypes.STRING,
    },
  }, {
    timestamps: false,
    tableName: 'oj_users',
  });

  // model.associate = (models) => {
  //   models.oj_source_code_user.belongsTo(models.oj_solution, { foreignKey: 'solution_id' });
  // };

  return model;
};
