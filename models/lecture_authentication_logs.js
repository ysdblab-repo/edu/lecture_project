module.exports = (sequelize, DataTypes) => {
  const lecture_authentication_log = sequelize.define('lecture_authentication_log', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    message: {
      type: DataTypes.STRING,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    during: {
      type: DataTypes.INTEGER,
    },
  });

  return lecture_authentication_log;

};
