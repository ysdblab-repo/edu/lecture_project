module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_download_log',{
    lecture_download_log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    type: {
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER
    }
    
  });
  return model;
}