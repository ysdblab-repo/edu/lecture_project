module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('teacher_group', {
    group_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
  },{
    timestamps: false,
  });
  model.associate = (models) => {
    models.teacher_group.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.teacher_group.belongsTo(models.bank_group, { onDelete: 'cascade', foreignKey: 'group_id' });
  };
  model.removeAttribute('id');

  model.selects = {
    nothing: [],
  };
  return model;
};
