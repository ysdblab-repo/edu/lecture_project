module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_item_small_group', {
    lecture_item_small_group_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    linked_small_group: {
      type: DataTypes.STRING,
      comment: '이 small_group에 포함되는 list들의 집합',
    },
    during: {
      type: DataTypes.INTEGER,
    }
  });
  return model;
}