module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('cheating_small', {
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    leader_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    follower_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    cheating: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.cheating_small.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.cheating_small.belongsTo(models.user, { onDelete: 'cascade', targetKey: 'user_id', foreignKey: 'leader_id' });
    models.cheating_small.belongsTo(models.user, { onDelete: 'cascade', targetKey: 'user_id', foreignKey: 'follower_id' });
  };

  return model;
};
