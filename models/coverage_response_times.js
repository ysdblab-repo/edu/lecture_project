module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('coverage_response_time',{
    log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    lecture_id: {
      type: DataTypes.INTEGER
    },
    estimated_time: {
      type: DataTypes.TIME,
    }
  })
  return model;
} 