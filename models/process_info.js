module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('process_info', {
    process_info_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.TEXT,
    },
    type: {
      type: DataTypes.INTEGER,
      comment: '허용프로그램인지 아닌지',
      defaultValue: 0,
    },
  }, {
    underscored: true,
  });

  return model;
};
