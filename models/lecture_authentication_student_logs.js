module.exports = (sequelize, DataTypes) => {
  const lecture_authentication_student_log = sequelize.define('lecture_authentication_student_log', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    message: {
      type: DataTypes.STRING,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
  });

  lecture_authentication_student_log.associate = (models) => {
    models.lecture_authentication_student_log.belongsTo(models.user, { foreignKey: 'student_id' });
  };

  return lecture_authentication_student_log;

};
