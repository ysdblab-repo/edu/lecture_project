module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('small_meeting_room_permission',{
    room_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    user_id: {
      type:DataTypes.INTEGER,
      primaryKey: true,
    },
    class_id: {
      type: DataTypes.INTEGER,
    }
  }, {
    timestamps: true,
  });

  model.associate = (models) => {
    models.small_meeting_room_permission.belongsTo(models.small_meeting_room, { foreignKey : 'room_id' });
    models.small_meeting_room_permission.belongsTo(models.user, { foreignKey: 'user_id' });
    models.small_meeting_room_permission.belongsTo(models.class, { foreignKey: 'class_id' });
  }

  return model;
}