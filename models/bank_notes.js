module.exports = (sequelize, DataTypes) => {
  const note = sequelize.define('bank_note', {
    note_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    note_type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0객관식, 1단답형, 2장문, 3파일 제출',
    },
    url: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    youtube_interval: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '답이 여러개일경우 구분자로 구분',
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    note_file: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });

  note.associate = (models) => {
    models.bank_note.belongsTo(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_note.hasMany(models.bank_file, { foreignKey: 'note_id' });
    models.bank_note.hasMany(models.bank_note_keyword, { foreignKey: 'note_id'});
  };

  return note;
};