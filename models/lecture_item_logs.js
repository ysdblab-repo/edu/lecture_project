module.exports = (sequelize, DataTypes) => {
  const lectureItem = sequelize.define('lecture_item_log', {
    lecture_item_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
  }, {
    underscored: true,
  });

  lectureItem.associate = (models) => {
    models.lecture_item_log.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.lecture_item_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return lectureItem;
};
