module.exports = (sequelize, DataTypes) => {
  const lectureChating = sequelize.define('lecture_chating', {
    lecture_chating_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    content: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    is_teacher: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    origin_lecture_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    }
  }, {
    underscored: true,
  });

  lectureChating.associate = (models) => {
    models.lecture_chating.belongsTo(models.user, { foreignKey: 'user_id' });
    models.lecture_chating.belongsTo(models.lecture, { foreignKey: 'lecture_id' });
  };

  return lectureChating;
};