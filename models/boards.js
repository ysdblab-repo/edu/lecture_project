module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('board', {
    board_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.TEXT,
    },
    content: {
      type: DataTypes.TEXT,
    },
    num_views: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.board.belongsTo(models.class, { foreignKey: 'class_id' });
    models.board.belongsTo(models.user, { foreignKey: 'writer_id' });
    models.board.hasMany(models.file, { foreignKey: 'board_id' });
    models.board.hasMany(models.board_reply, { foreignKey: 'board_id' });
  };

  return model;
};
