module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    user_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    email_id: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '=login id',
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(1000),
      allowNull: false,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0: user 1: teacher 2: profession 3: super admin 4: department admin',
    },
    birth: {
      type: DataTypes.DATE,
    },
    sex: {
      type: DataTypes.INTEGER,
      comment: '0: male 1: female',
    },
    address: {
      type: DataTypes.TEXT,
    },
    phone: {
      type: DataTypes.STRING,
    },
    major: {
      type: DataTypes.STRING,
    },
    belong: {
      type: DataTypes.STRING,
    },
    main_ip: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    email_verified: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    log_verify_code: {
      type: DataTypes.STRING,
    },
    terms_agreed: {
      type: DataTypes.DATE,
      defaultValue: null
    },
    career: {
      type: DataTypes.STRING,
    },
    account_bank: {
      type: DataTypes.STRING,
    },
    account_number: {
      type: DataTypes.STRING,
    },
    department_name: {
      type: DataTypes.STRING,
    },
    university_name: {
      type: DataTypes.STRING,
    },
    current_role: {
      type: DataTypes.INTEGER,
      comment: "강사 학생 계정 변환 구별 column",
    },
    
   
  }, {
    underscored: true,
  });

  user.associate = (models) => {
    models.user.hasOne(models.email_verification, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.lecture, { foreignKey: 'teacher_id' });
    models.user.belongsToMany(models.class, { through: models.user_class, foreignKey: 'user_id' });
    models.user.belongsToMany(models.cheating_large, { through: models.user_cheating, foreignKey: 'user_id' });
    models.user.hasMany(models.material, { foreignKey: 'creator_id' });
    models.user.hasMany(models.question, { foreignKey: 'creator_id' });
    models.user.hasMany(models.note,  { foreignKey: 'creator_id' });
    models.user.hasMany(models.login_log, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.login_check_phone, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_answer_log, { foreignKey: 'student_id' });
    models.user.hasMany(models.user_class, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.class, { as: 'master', foreignKey: 'master_id' });
    models.user.hasOne(models.find_password, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_lecture_keyword, { foreignKey: 'student_id' });
    models.user.hasMany(models.student_lecture_understanding, { foreignKey: 'student_id' });
    models.user.hasMany(models.board, { foreignKey: 'writer_id' });
    models.user.hasMany(models.board_reply, { foreignKey: 'writer_id' });
    models.user.hasMany(models.notice, { foreignKey: 'user_id' });
    models.user.hasMany(models.notice_reply, { foreignKey: 'user_id' });
    models.user.hasMany(models.student_program_log, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_program_detail_log, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.teacher_streaming_key, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_lecture_face, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_selfstudy_log, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_avg_face, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.lecture_item_log, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_lecture_ordered_log, { onDelete: 'cascade', foreignKey: 'student_id' });
    models.user.hasMany(models.student_lecture_timeline_log, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_homework, { onDelete: 'cascade', foreignKey: 'student_id' });
    models.user.hasMany(models.student_survey, { onDelete: 'cascade', foreignKey: 'student_id' });
    models.user.hasMany(models.homepage_log, { foreignKey: 'user_id' });
    models.user.hasMany(models.lecture_code_practice, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.discussion, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.student_practice_log, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasOne(models.file, { as: 'user_profile', foreignKey: 'user_id_profile' });
    models.user.hasMany(models.real_time_qna, { foreignKey: 'user_id' });
    models.user.hasMany(models.user_serial, { foreignKey: 'user_id' });
    models.user.hasMany(models.automatic_lecture_login_history, { onDelete: 'cascade', foreignKey: 'user_id'});
    models.user.hasMany(models.student_lecture_item_group_history, { foreignKey: 'user_id' });
    models.user.hasMany(models.bank_class, { foreignKey: 'latest_store_teacher_id' });
    models.user.hasMany(models.bank_lecture, { foreignKey: 'latest_store_teacher_id' });
    models.user.hasMany(models.bank_lecture_item, { foreignKey: 'latest_store_teacher_id' });
    models.user.hasMany(models.bank_file, { foreignKey: 'uploader_id' });
    models.user.hasMany(models.lecture_student_login_log, { onDelete: 'cascade', foreignKey: 'student_id' });
    models.user.hasMany(models.question_keyword_journaling, { onDelete: 'cascade', foreignKey: 'student_id' });
    models.user.hasMany(models.individual_question_student_indice, { foreignKey: 'student_id' });
    models.user.hasMany(models.individual_question_info, { foreignKey: 'creator_id' });
    models.user.hasMany(models.lecture_chating, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.small_group_chatting, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.enrollment, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.user_belonging, { foreignKey: 'user_id' });
    models.user.hasMany(models.req_create_class, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.connect_session_permission, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.face_log, { foreignKey: 'user_id' });
    models.user.hasMany(models.face_concentration, { foreignKey: 'user_id' });
    models.user.hasMany(models.screen_concentration, { foreignKey: 'user_id' });
    models.user.hasMany(models.lecture_authentication_student_log, { foreignKey: 'user_id' });
    models.user.hasMany(models.problem_judge_log, { foreignKey: 'user_id' });
    models.user.hasMany(models.lecture_grade, { foreignKey: 'user_id' });
    models.user.hasMany(models.lecture_ping_log, { foreignKey: 'user_id' });
    models.user.hasMany(models.user_class_delete_log, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user.hasMany(models.lecture_item_shown_log, { foreignKey: 'student_id' });
  };

  user.selects = {
    userJoined: ['user_id', 'email_id', 'name', 'type'],
    userAuth: ['user_id', 'email_id', 'name', 'type', 'birth', 'sex', 'address',
      'phone', 'major', 'belong', 'main_ip', 'email_verified', 'log_verify_code', 'created_at'],
    set1: [
      'user_id',
      'email_id',
      'name',
      'type',
      [sequelize.fn('date_format', sequelize.col('birth'), '%Y-%m-%d'), 'birth'],
      'sex',
      'address',
      'phone',
      'account_bank',
      'account_number',
      'department_name',
      'university_name'], // 공통
    set2: ['major', 'career'], // 강사
  };

  return user;
};
