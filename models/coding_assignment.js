module.exports = (sequelize, DataTypes) => {
    const model = sequelize.define('coding_assignment', {
        class_id: {
            type: DataTypes.INTEGER,
            primaryKey:true,
        },
        email_id: {
            type: DataTypes.STRING,
            defaultValue:'email_id not found',
            primaryKey:true,
        },
        lecture_item_id: {
            type: DataTypes.INTEGER,
            primaryKey:true,
        },
        assignment_name: {
            type: DataTypes.STRING,
            defaultValue:'assignment_name not found',
            primaryKey:true,
        },
        submitted_time: {
            type: DataTypes.DATE,
            defaultValue: null,
        },
        score: {
            type: DataTypes.FLOAT,
            defaultValue: null,
        },
        max_score:{
            type: DataTypes.FLOAT,
            defaultValue: null,
        },
        code_score: {
            type: DataTypes.FLOAT,
            defaultValue: null,
        },
        max_code_score: {
            type: DataTypes.FLOAT,
            defaultValue: null,
        },
        written_score: {
            type: DataTypes.FLOAT,
            defaultValue: null,
        },
        max_written_score: {
            type: DataTypes.FLOAT,
            defaultValue: null,
        },
    });
    model.associate = (models) => {
        //models.coding_assignment.belongsTo(models.lecture, { foreignKey : 'lecture_id'});
        //models.coding_assignment.belongsTo(models.class, { foreignKey: 'class_id', primaryKey: true, });
        //models.coding_assignment.belongsTo(models.lecture_item, { foreignKey: 'lecture_item_id', primaryKey: true, });

        // cannot use string(object) as an foreignKey in current version
        // https://github.com/sequelize/sequelize/issues/2536
        //models.coding_assignment.belongsTo(models.user, { foreignKey: 'email_id' });
    };
    return model;
}
