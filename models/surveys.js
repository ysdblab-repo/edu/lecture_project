module.exports = (sequelize, DataTypes) => {
  const survey = sequelize.define('survey', {
    survey_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    type: {
      type: DataTypes.INTEGER,
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    choice: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  }, {
    underscored: true,
  });

  survey.associate = (models) => {
    models.survey.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.survey.belongsTo(models.user, { foreignKey: 'creator_id' });
    models.survey.hasMany(models.file, { foreignKey: 'survey_id' });
    models.survey.hasMany(models.student_survey, { foreignKey: 'survey_id' });
    models.survey.hasMany(models.survey_keyword, { foreignKey: 'survey_id' });
  };

  return survey;
};
