module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_code_practice_keyword', {
    lecture_code_practice_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.lecture_code_practice_keyword.belongsTo(models.lecture_code_practice, { onDelete: 'cascade', foreignKey: 'lecture_code_practice_id' });
  };
  return model;
};
