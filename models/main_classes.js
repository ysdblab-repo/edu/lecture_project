module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('main_class', {
    main_class_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.main_class.belongsTo(models.class, { onDelete: 'cascade', foreignKey: 'class_id' });
  };

  return model;
};
