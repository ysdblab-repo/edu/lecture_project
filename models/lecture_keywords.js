module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_keyword', {
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    keyword: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    weight: {
      type: DataTypes.FLOAT,
    },
    weight_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    question_score_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    material_score_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    difference_question: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    difference_material: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
  }, {
    timestamps: false,
    indexes: [{ unique: true, fields: ['lecture_id', 'keyword'] }],
  });

  model.associate = (models) => {
    models.lecture_keyword.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
  };

  model.selects = {
    lectureKeywordSimple: ['lecture_id', 'keyword', 'weight'],
    lectureCoverage: ['lecture_id', 'keyword', 'weight', 'difference_question', 'difference_material'],
  };

  return model;
};
