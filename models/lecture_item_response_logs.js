module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_item_response_log', {
    log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    order: {
      type: DataTypes.INTEGER
    },
        // 예습, 복습, 본강 타입
    lecture_id: {
      type: DataTypes.INTEGER
    },
    item_id: {
      type: DataTypes.INTEGER
    },
    type: {
      type: DataTypes.INTEGER,
    },
        // 문항 type
    student_id: {
      type: DataTypes.INTEGER
    },
    start_time: {
      type: DataTypes.DATE
    },
    end_time: {
      type: DataTypes.DATE
    }
  });
  return model;
}