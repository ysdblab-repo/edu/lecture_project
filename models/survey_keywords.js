module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('survey_keyword', {
    survey_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.survey_keyword.belongsTo(models.survey, { onDelete: 'cascade', foreignKey: 'survey_id' });
  };

  return model;
};
