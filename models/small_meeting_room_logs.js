module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('small_meeting_room_log',{
    room_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0
      // 0 - 입장, 1 - 퇴장
    },
  }, {
    timestamps: true,
  });

  model.associate = (models) => {
    models.small_meeting_room_log.belongsTo(models.user, { foreignKey: 'user_id' });
    models.small_meeting_room_log.belongsTo(models.small_meeting_room, { onDelete: 'cascade', foreignKey: 'room_id'});
  }

  return model;
}