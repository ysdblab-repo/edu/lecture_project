module.exports = (sequelize, DataTypes) => {
  const discussionInfo = sequelize.define('bank_discussion_info', {
    lecture_item_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    topic: {
      type: DataTypes.STRING,
      defaultValue: '',
    },
    student_share: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    difficulty: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      comment: '난이도',
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });

  discussionInfo.associate = (models) => {
    models.bank_discussion_info.belongsTo(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
  };

  return discussionInfo;
};
