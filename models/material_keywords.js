module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('material_keyword', {
    material_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.material_keyword.belongsTo(models.material, { onDelete: 'cascade', foreignKey: 'material_id' });
  };

  model.selects = {
    keywordInfo: ['material_id', 'keyword', 'score_portion'],
  };

  return model;
};
