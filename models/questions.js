module.exports = (sequelize, DataTypes) => {
  const questions = sequelize.define('question', {
    question_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0객관식, 1단답형, 2장문, 3파일 제출',
    },
    question: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '문제 글',
    },
    choice: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '객관식 선택지 구분자로 구분',
    },
    choice2: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '보기 2',
    },
    answer: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '답이 여러개일경우 구분자로 구분',
    },
    is_ordering_answer: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '단답형에서 답의 순서가 필요한지 0: 노필요, 1: 필요',
    },
    accept_language: {
      type: DataTypes.STRING(1000),
    },
    order: {
      type: DataTypes.INTEGER,
      comment: '문제의 순서',
    },
    showing_order: {
      type: DataTypes.INTEGER,
      comment: '문제가 보여지는 순서',
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      comment: '배점',
    },
    difficulty: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      comment: '난이도',
    },
    timer: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null,
      comment: '문제 제한시간',
    },
    sqlite_file_guid: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    multi_choice_media_type: {
      type: DataTypes.TEXT,
      defaultValue: 'none',
      comment: '객관식 멀티미디어 타입: none, youtube, image, voice',
    },
    multi_choice_media: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '객관식 멀티미디어',
    },
    answer_media_type: {
      type: DataTypes.TEXT,
      defaultValue: 'none',
      comment: '단답에서 사용하는 다양한 멀티미디어로 답변 받기 타입: none, youtube, image, voice',
    },
    question_media_type: {
      type: DataTypes.TEXT,
      defaultValue: 'none',
      comment: '문항 문제 미디어 타입: none, youtube, voice',
    },
    question_media: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '문항 미디어',
    },
    info_item_id: {
      type: DataTypes.INTEGER,
    }
  }, {
    underscored: true,
  });

  questions.associate = (models) => {
    models.question.hasMany(models.file, { foreignKey: 'question_id' });
    models.question.hasMany(models.file, { as: 'question_material', foreignKey: 'question_id_material' });
    models.question.hasMany(models.file, { as: 'question_multichoice', foreignKey: 'question_id_multichoice' });
    models.question.hasMany(models.file, { as: 'question_voice', foreignKey: 'question_id_voice' });
    models.question.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.question.belongsTo(models.individual_question_info, { onDelete: 'cascade', foreignKey: 'info_id' });
    models.question.belongsTo(models.user, { foreignKey: 'creator_id' });
    models.question.hasMany(models.problem_testcase, { onDelete: 'cascade', foreignKey: 'question_id' });
    models.question.hasMany(models.question_keyword, { onDelete: 'cascade', foreignKey: 'question_id' });
    models.question.hasMany(models.student_answer_log, { onDelete: 'cascade', foreignKey: 'question_id' });
    models.question.hasOne(models.oj_problem, { onDelete: 'cascade', foreignKey: 'problem_id' });
    models.question.hasMany(models.question_keyword_journaling, { onDelete: 'cascade', foreignKey: 'question_id' });
    models.question.hasMany(models.problem_judge_log, { foreignKey: 'question_id' });
  };

  questions.selects = {
    questionStudent: ['question_id', 'type', 'question', 'choice', 'score', 'choice2',
      'order', 'showing_order', 'difficulty', 'timer', 'lecture_item_id', 'creator_id'],
  };

  return questions;
};
