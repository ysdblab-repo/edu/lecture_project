module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_lecture_ordered_log', {
    student_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    order: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    avg_understanding: {
      type: DataTypes.FLOAT,
	  defaultValue: 0,
    },
	avg_participation: {
      type: DataTypes.FLOAT,
	  defaultValue: 0,
    },
	avg_concentration: {
      type: DataTypes.FLOAT,
	  defaultValue: 0,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.student_lecture_ordered_log.belongsTo(models.user, { foreignKey: 'student_id' });
  };

  return model;
};
