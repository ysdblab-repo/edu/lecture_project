module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('automatic_lecture_login_history', {
    history_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_type: {
      type: DataTypes.INTEGER,
    },
    lecture_item_group_id: {
      type: DataTypes.INTEGER,
    },
    offset: {
      type: DataTypes.INTEGER,
    }
  }, {
    underscored: true
  });

  model.associate = (models) => {
    models.automatic_lecture_login_history.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.automatic_lecture_login_history.belongsTo(models.user, { foreignKey: 'user_id' });
  };

  return model;
}