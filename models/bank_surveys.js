module.exports = (sequelize, DataTypes) => {
  const survey = sequelize.define('bank_survey', {
    survey_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    type: {
      type: DataTypes.INTEGER,
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    choice: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });

  survey.associate = (models) => {
    models.bank_survey.belongsTo(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_survey.hasMany(models.bank_file, { foreignKey: 'survey_id' });
    models.bank_survey.hasMany(models.bank_survey_keyword, { onDelete: 'cascade', foreignKey: 'survey_id' });

  };

  return survey;
};
