module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_coverage', {
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    question_error: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    material_error: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.lecture_coverage.belongsTo(models.lecture, { foreignKey: 'lecture_id' });
  };

  return model;
};
