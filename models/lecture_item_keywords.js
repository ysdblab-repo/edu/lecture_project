module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_item_keyword', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    keyword: {
      type: DataTypes.STRING,
    },
    weight: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    weight_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    question_score_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    material_score_ratio: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    difference_question: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    difference_material: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
  }, {
    timestamps: false,
    indexes: [{ unique: true, fields: ['lecture_item_id', 'keyword'] }],
  });

  model.associate = (models) => {
    models.lecture_item_keyword.belongsTo(models.lecture_item, { foreignKey: 'lecture_item_id' });
  };

  return model;
};
