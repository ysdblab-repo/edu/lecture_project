module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('coding_keyword', {
    coding_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  model.associate = (models) => {
    models.coding_keyword.belongsTo(models.coding, { onDelete: 'cascade', foreignKey: 'coding_id' });
  };

  return model;
};
