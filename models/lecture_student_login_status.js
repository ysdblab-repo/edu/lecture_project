module.exports = (sequelize, DataTypes) => {
  const lecture_student_login_status = sequelize.define('lecture_student_login_status', {
    lecture_student_login_status_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    count: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    
  }, {
    underscored: true,
  });
  
  return lecture_student_login_status;
};
