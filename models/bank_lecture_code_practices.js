module.exports = (sequelize, DataTypes) => {
  const lectureCodePractice = sequelize.define('bank_lecture_code_practice', {
    lecture_code_practice_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    code: {
      type: DataTypes.TEXT,
    },
	  is_jupyter: {
	    type: DataTypes.INTEGER,
	    allowNull: false,
      defaultValue: 1,
    },
    language_type: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
    },
	  activated_time: {
      type: DataTypes.DATE,
	    allowNull: true,
      defaultValue: null,
    },
	  deactivated_time: {
      type: DataTypes.DATE,
	    allowNull: true,
      defaultValue: null,
	  },
    difficulty: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
      comment: '난이도',
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });

  lectureCodePractice.associate = (models) => {
    models.bank_lecture_code_practice.belongsTo(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.bank_lecture_code_practice.hasMany(models.bank_lecture_code_practice_keyword, { onDelete: 'cascade', foreignKey: 'lecture_code_practice_id' });

  };

  return lectureCodePractice;
};
