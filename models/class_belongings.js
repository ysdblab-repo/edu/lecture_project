module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('class_belonging', {
    class_belonging_id: {
      type: DataTypes.INTEGER,
      autoIncrement:true,
      primaryKey: true,
    },
    belonging_pwd: {
      type: DataTypes.STRING,
    },
    year: {
      type: DataTypes.INTEGER,
    },
    semester: {
      type: DataTypes.INTEGER,
    },
    affiliation_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  }, {
    timestamps: true,
  });

  model.associate = (models) => { 
    models.class_belonging.belongsTo(models.affiliation_description, { onDelete: 'cascade', foreignKey: 'affiliation_id' });
    models.class_belonging.hasMany(models.class_password, { foreignKey: 'class_belonging_id' });
    models.class_belonging.hasMany(models.user_belonging, { foreignKey: 'class_belonging_id' });
  };

  return model;
};