module.exports = (sequelize, DataTypes) => {
  const rlh = sequelize.define('realtime_lecture_heartbeat', {
    realtime_lecture_heartbeat_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
  }, {
    underscored: true,
  });

  rlh.associate = (models) => {
    models.realtime_lecture_heartbeat.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.realtime_lecture_heartbeat.belongsTo(models.user, { foreignKey: 'user_id' });
  };

  return rlh;
};
