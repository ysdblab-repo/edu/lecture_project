module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('board_reply', {
    board_reply_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    content: {
      type: DataTypes.TEXT,
    },
    is_teacher: {
      type: DataTypes.BOOLEAN,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.board_reply.belongsTo(models.board, { foreignKey: 'board_id' });
    models.board_reply.belongsTo(models.user, { foreignKey: 'writer_id' });
  };

  return model;
};
