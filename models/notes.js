module.exports = (sequelize, DataTypes) => {
  const note = sequelize.define('note', {
    note_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    note_type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0객관식, 1단답형, 2장문, 3파일 제출',
    },
    url: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    youtube_interval: {
      type: DataTypes.TEXT,
      defaultValue: '',
      comment: '답이 여러개일경우 구분자로 구분',
    },
    lecture_item_id: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    note_file: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
  }, {
    underscored: true,
  });

  note.associate = (models) => {
    models.note.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.note.belongsTo(models.user, { foreignKey: 'creator_id' });
    models.note.hasMany(models.file, { foreignKey: 'note_id' });
    models.note.hasMany(models.note_keyword, { foreignKey: 'note_id' });

  };

  return note;
};