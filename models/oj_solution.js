module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_solution', {
    solution_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    student_answer_log_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    problem_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    user_id: {
      type: DataTypes.STRING,
    },
    time: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    memory: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    errorMsg: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: '',
    },
    in_date: {
      type: DataTypes.DATE,
      defaultValue: '2016-05-13 19:24:00',
    },
    result: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    language: {
      type: DataTypes.INTEGER.UNSIGNED,
      defaultValue: 0,
    },
    ip: {
      type: DataTypes.STRING,
    },
    contest_id: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    valid: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    num: {
      type: DataTypes.INTEGER,
      defaultValue: -1,
    },
    code_length: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    judgetime: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    pass_rate: {
      type: DataTypes.DECIMAL(3, 2).UNSIGNED,
      defaultValue: 0,
    },
    lint_error: {
      type: DataTypes.INTEGER.UNSIGNED,
      defaultValue: 0,
    },
    judger: {
      type: DataTypes.STRING,
      defaultValue: 'LOCAL',
    },
    judged: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    }
  }, {
    timestamps: false,
    tableName: 'oj_solution',
    indexes: [
      { name: 'pid', fields: ['problem_id'] },
      { name: 'uid', fields: ['user_id'] },
      { name: 'res', fields: ['result'] },
      { name: 'cid', fields: ['contest_id'] },
    ],
  });

  model.associate = (models) => {
    models.oj_solution.belongsTo(models.student_answer_log, { onDelete: 'cascade',  foreignKey: 'student_answer_log_id' });
    models.oj_solution.hasMany(models.oj_analysis, { foreignKey: 'solution_id' });
    models.oj_solution.hasMany(models.problem_judge_log, { foreignKey: 'solution_id' });
  };

  return model;
};
