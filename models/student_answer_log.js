module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_answer_log', {
    student_answer_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    order: {
      type: DataTypes.INTEGER,
    },
    answer: {
      type: DataTypes.TEXT,
      comment: '학생이 제출한 답 / 답이 여러개일경우 구분자로 구분',
    },
    score: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    ratio: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    interval: {
      type: DataTypes.INTEGER,
      comment: '학생이 푸는대 걸린 시간 sec',
    },
    path: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    valid: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false,
    },
    count: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: `학생이 제출한 횟수`
    },
    student_local_time: {
      type: DataTypes.DATE,
      defaultValue: null,
    },
    concentration: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    transScore: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    finalScore: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    },
    absolute_reactivity: {
      type: DataTypes.FLOAT,
      defaultValue: null,
    }
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.student_answer_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'student_id' });
    models.student_answer_log.belongsTo(models.class, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.student_answer_log.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.student_answer_log.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'item_id' });
    models.student_answer_log.belongsTo(models.question, { onDelete: 'cascade', foreignKey: 'question_id' });
    models.student_answer_log.hasMany(models.feedback, { onDelete: 'cascade', foreignKey: 'student_answer_log_id' });
    models.student_answer_log.hasOne(models.oj_solution, { onDelete: 'cascade', foreignKey: 'student_answer_log_id' });
    models.student_answer_log.hasOne(models.oj_source_code, { onDelete: 'cascade', foreignKey: 'solution_id' });
    models.student_answer_log.hasOne(models.oj_runtimeinfo, { onDelete: 'cascade', foreignKey: 'solution_id' });
    models.student_answer_log.hasOne(models.oj_compileinfo, { onDelete: 'cascade', foreignKey: 'solution_id' });
    models.student_answer_log.hasMany(models.file, { onDelete:'cascade',  foreignKey: 'student_answer_log_id' });
  };

  return model;
};
