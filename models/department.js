module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('department', {
    name: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    code: {
      type: DataTypes.STRING,
    },
    part: {
      type: DataTypes.STRING,
    },
    manager_name: {
      type: DataTypes.STRING,
    },
    manager_email: {
      type: DataTypes.STRING,
    },
    manager_phone_number: {
      type: DataTypes.STRING,
    },
    university_name: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
  }, {
    underscored: true,
  });

  model.associate = (models) => {
    models.department.belongsTo(models.university, { targetKey: 'name', onDelete: 'cascade', foreignKey: 'university_name' });
  };

  model.selects = {
    set1: ['code', 'name', 'part', 'manager_name', 'manager_email', 'manager_phone_number'],
  };

  return model;
};
