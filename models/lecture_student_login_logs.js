module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_student_login_log', {
    log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    type: {
      type: DataTypes.INTEGER,
    }  // 0 - in , 1 - out , 2 - 입장허용을 위한 value
  });
  model.associate = (models) => {
    models.lecture_student_login_log.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.lecture_student_login_log.belongsTo(models.user, { onDelete : 'cascade', foreignKey: 'student_id'});
  }
  return model;
}