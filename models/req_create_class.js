module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('req_create_class', {
    req_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      required: true,
    },
    affiliation_id: {
      type: DataTypes.INTEGER,
      required: true,
    },
    type: {
      type: DataTypes.INTEGER, // 1: 일반, 2: 실시간 스트리밍, 3: 도커 사용, 4: 맛보기
      required: true,
    },
    class_id: {
      type: DataTypes.INTEGER,
      required: true,
    },
    class_name: {
      type: DataTypes.TEXT,
      defaultValue: null,
    },
    status: {
      type: DataTypes.INTEGER, // 0: 접수, 1: 수락, 강사는 아직 과목을 생성하지 않음, 2: 거절, 3: 수락, 강사는 과목을 생성함
      required: true,
      defaultValue: 0,
    },
    subject_type: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: '정규과목(폐쇄 과목: 1), 맛보기(시연, 개방 과목: 2)',
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: null,
    },
  }, {
  });
  model.associate = (models) => {
    models.req_create_class.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.req_create_class.belongsTo(models.affiliation_description, { onDelete: 'cascade', foreignKey: 'affiliation_id' });
    models.req_create_class.belongsTo(models.class, { onDelete: 'cascade', foreignKey: 'class_id' });
    models.req_create_class.hasMany(models.class_passwords_by_admin, { foreignKey: 'req_id' });
    models.req_create_class.belongsTo(models.open_space_2nd, { onDelete: 'cascade', foreignKey: 'id_2nd' });
  };
  return model;
};
