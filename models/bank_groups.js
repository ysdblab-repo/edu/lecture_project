module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('bank_group', {
    group_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    university_name: {
      type: DataTypes.STRING,
    },
    department_name: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
    timestamps: false,
  });
 
  model.associate = (models) => {
    models.bank_group.hasMany(models.bank_class, { foreignKey: 'group_id' });
    models.bank_group.hasMany(models.bank_lecture, { foreignKey: 'group_id' });
    models.bank_group.hasMany(models.bank_lecture_item, { foreignKey: 'group_id' });
    models.bank_group.hasMany(models.teacher_group, { foreignKey: 'group_id' });
  };

  model.selects = {
    groupInfo: ['group_id', 'name'],
    nothing: [],
  };

  return model;
};
