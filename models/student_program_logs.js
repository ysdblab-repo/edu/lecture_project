module.exports = (sequelize, DataTypes) => {
  const tsk = sequelize.define('student_program_log', {
    student_program_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    time: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    opt: {
      type: DataTypes.INTEGER,
      defaultValue: DataTypes.NOW,
    },
  }, {
    underscored: true,
  });

  tsk.associate = (models) => {
    models.student_program_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });

  };


  return tsk;
};
