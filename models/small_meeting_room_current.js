module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('small_meeting_room_current',{
    room_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      unique: 'actions_unique',
    },
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    socket_id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    connection_order: { // 강의방 전용, 연결 순서
      type: DataTypes.INTEGER,
      unique: 'actions_unique',
    },
  }, {
    uniqueKeys: {
        actions_unique: {
          fields: ['room_id', 'connection_order']
        }
    },
    timestamps: true,
  });

  model.associate = (models) => {
    models.small_meeting_room_log.belongsTo(models.user, { foreignKey: 'user_id' });
    models.small_meeting_room_log.belongsTo(models.small_meeting_room, { onDelete: 'cascade', foreignKey: 'room_id'});
  }

  return model;
}