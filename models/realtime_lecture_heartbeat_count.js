module.exports = (sequelize, DataTypes) => {
  const rlh = sequelize.define('heartbeat_count', {
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    count: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
  }, {
    underscored: true,
  });

  rlh.associate = (models) => {
    models.heartbeat_count.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.heartbeat_count.belongsTo(models.user, { foreignKey: 'user_id' });
  };

  return rlh;
};
