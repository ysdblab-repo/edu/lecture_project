module.exports = (sequelize, DataTypes) => {
  const student_selfstudy_log = sequelize.define('student_selfstudy_log', {
    student_selfstudy_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    start_time: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    end_time: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    participation_score: {
      type: DataTypes.FLOAT,
      allowNull: true,
	  defaultValue: 0,
    },
    concentration_score_time: {
      type: DataTypes.FLOAT,
      allowNull: true,
	  defaultValue: 0,
    },
    concentration_score_type: {
      type: DataTypes.FLOAT,
      allowNull: true,
	  defaultValue: 0,
    },
	concentration_score: {
      type: DataTypes.FLOAT,
      allowNull: true,
	  defaultValue: 0,
    },
	latest_pic_path: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  }, {
    underscored: true,
  });

  student_selfstudy_log.associate = (models) => {
    models.student_selfstudy_log.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.student_selfstudy_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return student_selfstudy_log;
};
