module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_analysis', {
    analysis_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    solution_id: {
      type: DataTypes.INTEGER,
    },
    num_loop_total: {
      type: DataTypes.INTEGER,
    },
    // num_loop_upper: {
    //   type: DataTypes.INTEGER,
    // },
    // num_loop_lower: {
    //   type: DataTypes.INTEGER,
    // },
    num_branch_total: {
      type: DataTypes.INTEGER,
    },
    // num_branch_upper: {
    //   type: DataTypes.INTEGER,
    // },
    // num_branch_lower: {
    //   type: DataTypes.INTEGER,
    // },
    num_function_declared: {
      type: DataTypes.INTEGER,
    },
    num_function_used: {
      type: DataTypes.INTEGER,
    },
    num_var_declared: {
      type: DataTypes.INTEGER,
    },
    num_var_used: {
      type: DataTypes.INTEGER,
    },
    // num_array_declared: {
    //   type: DataTypes.INTEGER,
    // },
    // num_int_declared: {
    //   type: DataTypes.INTEGER,
    // },
    // num_int_used: {
    //   type: DataTypes.INTEGER,
    // },
    // num_double_declared: {
    //   type: DataTypes.INTEGER,
    // },
    // num_double_used: {
    //   type: DataTypes.INTEGER,
    // },
    // num_string_declared: {
    //   type: DataTypes.INTEGER,
    // },
    // num_string_used: {
    //   type: DataTypes.INTEGER,
    // },
    // line_per_loop_avg: {
    //   type: DataTypes.INTEGER,
    // },
    // line_per_branch_avg: {
    //   type: DataTypes.INTEGER,
    // },
    // line_per_function_avg: {
    //   type: DataTypes.INTEGER,
    // },
    num_operator: {
      type: DataTypes.INTEGER,
    },
    num_assignment: {
      type: DataTypes.INTEGER,
    },
    num_comparision_operator: {
      type: DataTypes.INTEGER,
    },
    num_class: {
      type: DataTypes.INTEGER,
    },
    ast_node_number: {
      type: DataTypes.FLOAT,
    },
    // ast_fanout_min: {
    //   type: DataTypes.FLOAT,
    // },
    // ast_fanout_max: {
    //   type: DataTypes.FLOAT,
    // },
    // ast_fanout_avg: {
    //   type: DataTypes.FLOAT,
    // },
    // ast_field_min: {
    //   type: DataTypes.FLOAT,
    // },
    // ast_field_max: {
    //   type: DataTypes.FLOAT,
    // },
    // ast_field_avg: {
    //   type: DataTypes.FLOAT,
    // },
    ast_depth: {
      type: DataTypes.FLOAT,
    },
  }, {
    timestamps: true,
    underscored: true,
  });

  model.associate = (models) => {
    models.oj_analysis.belongsTo(models.oj_solution, { onDelete: 'cascade', foreignKey: 'solution_id' });
  };

  return model;
};
