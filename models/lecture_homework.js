module.exports = (sequelize, DataTypes) => {
  
  const model = sequelize.define('lecture_homework',{
    id: {
      type : DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    lecture_id: {
      type : DataTypes.INTEGER,
    },
    evaluated_cnt: {
      type: DataTypes.INTEGER,
    },
    homeworkStartAt: {
      type: DataTypes.DATE,
    },
    homeworkEndAt: {
      type: DataTypes.DATE,
    },
    scoringStartAt: {
      type: DataTypes.DATE,
    },
    scoringEndAt: {
      type: DataTypes.DATE,
    },
    eval_select: {
      type: DataTypes.INTEGER,
      comment: '교차 평가 문제 수 설정 옵션',
    },
    max_score: {
      type: DataTypes.INTEGER,
      comment: '교차 평가 문제 최대 점수',
    },
    comment: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });

  return model;
}