module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('class_password', {
    class_password_id: {
      type: DataTypes.INTEGER,
      autoIncrement:true,
      primaryKey: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    create_check: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: '0',
    },
    class_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    class_belonging_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  });

  model.associate = (models) => {
    models.class_password.belongsTo(models.class, {onDelete: 'cascade', foreignKey: 'class_id'});
    models.class_password.belongsTo(models.user, {onDelete: 'cascade', foreignKey:'user_id'});
    models.class_password.belongsTo(models.class_belonging, {onDelete: 'cascade', foreignKey:'class_belonging_id'});
  };

  return model;
};