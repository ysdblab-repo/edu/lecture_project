module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_lecture_keyword_coverage', {
    pk_id:{
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    order: {
      type: DataTypes.INTEGER,
      defaultDataValue: 0,
      comment: '0 - 본강, 1 - 복습, 2 - 예습'
    },
    keyword: {
      type: DataTypes.STRING,
    },
    keyword_ratio: {
      type: DataTypes.FLOAT,
    },
    lecture_item_keyword: {
      type: DataTypes.STRING,
    },
    item_keyword_understanding_ratio: {
      type: DataTypes.FLOAT,
    }
  }, {
    timestamps: false,
  });
  model.associate = (models) => {
    models.student_lecture_keyword_coverage.belongsTo(models.lecture_start, { onDelete: 'cascade', foreignKey: 'lecture_start_id' });

  }
  return model;
};
