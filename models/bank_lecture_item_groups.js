module.exports = (sequelize, DataTypes) => {

  const model = sequelize.define('bank_lecture_item_group',{

    lecture_item_group_id: {
      type : DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    opened: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    group_list: {
      type: DataTypes.STRING,
    },
    start: {
      type: DataTypes.INTEGER,
    },
    end: {
      type: DataTypes.INTEGER
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  });

  return model;
}
