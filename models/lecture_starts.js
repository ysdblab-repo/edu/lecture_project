module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_start', {
    lecture_start_id:{
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    teacher_id:{
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
  });

  model.associate = (models) => {
    models.lecture_start.hasMany(models.student_lecture_item_understanding_log, { onDelete: 'cascade', foreignKey: 'lecture_start_id'});
    models.lecture_start.hasMany(models.student_lecture_understanding_log, { onDelete: 'cascade', foreignKey: 'lecture_start_id'});
    models.lecture_start.hasMany(models.student_lecture_keyword_coverage, { onDelete: 'cascade', foreignKey: 'lecture_start_id'} )

    // models.lecture_start.hasMany()
    // models.lecture_start.hasMany()
    // models.lecture_start.hasMany()

    // models.lecture_start.hasMany()
    // models.lecture_start.hasMany()
    // models.lecture_start.hasMany()
  }


  return model;
}