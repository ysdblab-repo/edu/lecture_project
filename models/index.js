const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const config = require('../config');

const sequelize = new Sequelize(config.db.database, config.db.user, config.db.password, config.sequelize);
const db = {};

fs.readdirSync(__dirname)
  .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
   // console.log(` * ${model.name} *`);
  });

Object.keys(db).forEach((modelName) => {
 // console.log(` ** ${modelName} ** `);
  if (db[modelName].associate) {
    //console.log(`-- associate ${db[modelName].name} -- `);

    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
