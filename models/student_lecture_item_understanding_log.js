module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('student_lecture_item_understanding_log', {
    pk_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    item_id: {
      type: DataTypes.INTEGER,
    },
    item_score: {
      type: DataTypes.INTEGER,
    },
    type: {
      type: DataTypes.INTEGER,
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '0 - 본강, 1 - 복습, 2 - 예습'
    },
    student_score: {
      type: DataTypes.INTEGER,
    },
    score_portion: {
      type: DataTypes.FLOAT,
    },
    item_understanding: {
      type: DataTypes.FLOAT,
    },
    keyword: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    item_keyword_score_portion_ratio: {
      type: DataTypes.FLOAT,
    },
    item_keyword_student_score: {
      type: DataTypes.FLOAT,
    },
  });

  model.associate = (models) => {
    models.student_lecture_item_understanding_log.belongsTo(models.lecture_start, { onDelete: 'cascade', foreignKey: 'lecture_start_id' });

  }

  return model;
};
