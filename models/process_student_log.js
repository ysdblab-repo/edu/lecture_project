module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('process_student_log', {
    process_student_log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    student_id: {
      type: DataTypes.INTEGER,
    },
    lecture_id: {
      type: DataTypes.INTEGER,
    },
    process_name: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

  return model;
};
