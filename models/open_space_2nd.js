module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('open_space_2nd',{
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
  },{
    timestamps: false,
  });

  model.associate = (models) => {
    models.open_space_2nd.belongsTo(models.open_space_1st, { foreignKey: 'id_1st' });
    models.open_space_2nd.belongsTo(models.affiliation_description, { foreignKey: 'affiliation_id' });
    models.open_space_2nd.hasMany(models.req_create_class, { foreignKey: 'req_id' });
  }

  return model;
}
