
module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('user_class_delete_log', {
    role: {
      type: DataTypes.STRING,
    },
    class_id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
  }, {
    timestamps: true,
  });
  model.associate = (models) => {
    models.user_class.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user_class.belongsTo(models.class, { onDelete: 'cascade', foreignKey: 'class_id' });
  }
  return model;
};
