module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('user_belonging', {
    user_belonging_id: {
      type: DataTypes.INTEGER,
      autoIncrement:true,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    affiliation_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    class_belonging_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    main_belonging: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
  }, {
    timestamps: true,
  });

  model.associate = (models) => { 
    models.user_belonging.belongsTo(models.affiliation_description, { onDelete: 'cascade', foreignKey: 'affiliation_id' });
    models.user_belonging.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
    models.user_belonging.belongsTo(models.class_belonging, { onDelete: 'cascade', foreignKey: 'class_belonging_id' });
  };

  return model;
};