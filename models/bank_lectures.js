module.exports = (sequelize, DataTypes) => {
  const lectures = sequelize.define('bank_lecture', {
    lecture_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    lecture_type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    location: {
      type: DataTypes.STRING(2000),
      defaultValue: null,
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    start_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    end_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    is_auto: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    video_link: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
    keyword_state: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      // 0: 실행전, 1: 실행중, 2: 실행끝, 3: 에러
    },
    bank_history_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    underscored: true,
  });
 
  lectures.associate = (models) => {
    models.bank_lecture.belongsTo(models.bank_class, { foreignKey: 'class_id' });
    models.bank_lecture.belongsTo(models.user, { foreignKey: 'latest_store_teacher_id' });
    models.bank_lecture.hasMany(models.bank_lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.bank_lecture.hasMany(models.bank_lecture_keyword, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.bank_lecture.hasMany(models.bank_lecture_keyword_relation, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.bank_lecture.hasOne(models.bank_lecture_coverage, { onDelete: 'cascade', foreignKey: 'lecture_id' });
    models.bank_lecture.hasMany(models.bank_material, { as: 'bank_material', foreignKey: 'lecture_id' });
    models.bank_lecture.belongsTo(models.serial_group, { foreignKey: 'lecture_serial_no' });
    models.bank_lecture.belongsTo(models.bank_group, { foreignKey: 'group_id' });
    models.bank_lecture.belongsTo(models.bank_history, { onDelete: 'cascade', foreignKey: 'bank_history_id' });
  };

  lectures.selects = {
    lectureInfo: ['lecture_id', 'name', 'location', 'type', 'start_time', 'end_time', 'latest_store_teacher_id', 'is_auto',
      'video_link', 'keyword_state', 'lecture_serial_no',
      [sequelize.fn('date_format', sequelize.col('created_at'), '%Y-%m-%d %H:%m:%S'), 'created_at'],
      [sequelize.fn('date_format', sequelize.col('updated_at'), '%Y-%m-%d %H:%m:%S'), 'updated_at']],
    name: ['lecture_id', 'name'],
  };
  return lectures;
};
