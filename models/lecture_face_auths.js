module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('lecture_face_auth',{
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    lecture_id: {
      type: DataTypes.INTEGER
    },
    type: {
      type: DataTypes.INTEGER
    },
    class_id: {
      type: DataTypes.INTEGER
    },
    mode: {
      type: DataTypes.INTEGER
    },
    count: {
      type: DataTypes.INTEGER,
    }
    
  });
  return model;
}