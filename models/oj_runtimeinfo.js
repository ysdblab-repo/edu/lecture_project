module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('oj_runtimeinfo', {
    solution_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    error: {
      type: DataTypes.TEXT,
    },
  }, {
    timestamps: false,
    tableName: 'oj_runtimeinfo',
  });

  model.associate = (models) => {
    models.oj_runtimeinfo.belongsTo(models.student_answer_log, { onDelete: 'cascade',  foreignKey: 'solution_id' });
  };

  return model;
};
