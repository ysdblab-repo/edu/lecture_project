module.exports = (sequelize, DataTypes) => {
  const face_log = sequelize.define('face_log', {
    log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    lecture_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    concentration: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()'),
    },
    updated_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()'),
    }
  }, {
		underscored: true,
		timestamps: false,
  });

  face_log.associate = (models) => {
    models.face_log.belongsTo(models.lecture, { onDelete: 'cascade', foreignKey : 'lecture_id' });
    models.face_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });
  };

  return face_log;
}