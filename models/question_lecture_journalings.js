module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('question_lecture_journaling', {
    question_lecture_journaling_id: {
      type: DataTypes.INTEGER,
      autoIncrement:true,
      primaryKey: true,
    },
    absolute_participation: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    relative_participation: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    absolute_understanding: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    relative_understanding: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    absolute_concentration: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    relative_concentration: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    journaling_type:{
      type:DataTypes.INTEGER,
      allowNull:true,
      comment: '모든 문항 기준(1: 모든 문항에 대한 통계, 2: 모든 문항에 대한 학생별 통계)',
    }
  }, {
    timestamps: true,
  });

  model.associate = (models) => {
    models.question_lecture_journaling.belongsTo(models.class, {onDelete: 'cascade',foreignKey: 'class_id'});
    models.question_lecture_journaling.belongsTo(models.lecture, {onDelete: 'cascade',foreignKey: 'lecture_id'});
    models.question_lecture_journaling.belongsTo(models.lecture_item, {onDelete: 'cascade',foreignKey: 'lecture_item_id'});
    models.question_lecture_journaling.belongsTo(models.question, {onDelete: 'cascade',foreignKey: 'question_id'});
    models.question_lecture_journaling.belongsTo(models.user, {onDelete: 'cascade', foreignKey:'student_id'});
      };

  return model;
};
