module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('connect_session', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    condition_lower: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '이해도 최소',
    },
    condition_upper: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '이해도 최대',
    },
    isActive: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '활성화 여부(0: 미활성, 1: 활성)',
    },
  }, {
    timestamps: true,
  });

  model.associate = (models) => {
    models.connect_session.belongsTo(models.lecture, { foreignKey: 'source' });
    models.connect_session.belongsTo(models.lecture, { foreignKey: 'target' });
    models.connect_session.belongsTo(models.class, { foreignKey: 'class_id' });
    models.connect_session.hasMany(models.connect_session_permission, { onDelete: 'cascade', foreignKey: 'connect_session_id' });
  };

  return model;
};
