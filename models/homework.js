module.exports = (sequelize, DataTypes) => {
  const homework = sequelize.define('homework', {
    homework_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    comment: {
      type: DataTypes.TEXT,
      defaultValue: '',
    },
    score: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  }, {
    underscored: true,
  });

  homework.associate = (models) => {
    models.homework.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.homework.belongsTo(models.user, { foreignKey: 'creator_id' });
    models.homework.hasMany(models.file, { foreignKey: 'homework_id' });
    models.homework.hasMany(models.student_homework, { foreignKey: 'homework_id' });
    models.homework.hasMany(models.homework_keyword, { foreignKey: 'homework_id' });
  };

  return homework;
};
