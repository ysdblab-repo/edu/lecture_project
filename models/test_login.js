module.exports = (sequelize, DataTypes ) => {
  const model = sequelize.define('test_login', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    ip: {
      type: DataTypes.STRING
    },
    token: {
      type: DataTypes.STRING(1000)
    },
    expired: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    type: {
      type: DataTypes.INTEGER
    }
  });
  return model;
}