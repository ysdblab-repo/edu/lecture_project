module.exports = (sequelize, DataTypes) => {
  const tsk = sequelize.define('student_program_detail_log', {
    student_program_detail_log_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    time: {
      type: DataTypes.DATE,
      //defaultValue: DataTypes.NOW
    },
    active_program: {
      type: DataTypes.STRING,
    },
    num_keyboard_type: {
      type: DataTypes.INTEGER,
    },
    string_keyboard_type: {
      type: DataTypes.STRING,
    },
    activated_time: {
      type: DataTypes.INTEGER,
    },
    mac_address: {
      type: DataTypes.STRING,
    },
    ip_address: {
      type: DataTypes.STRING,
    },
  }, {
    underscored: true,
  });

  tsk.associate = (models) => {
    models.student_program_detail_log.belongsTo(models.user, { onDelete: 'cascade', foreignKey: 'user_id' });

  };


  return tsk;
};
