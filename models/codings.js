module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('coding', {
      coding_id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true,
      },
      description: {
          type: DataTypes.STRING,
      },
      score: {
          type: DataTypes.INTEGER,
      },
      creator_id: {
        type: DataTypes.INTEGER,
      },
  });
  model.associate = (models) => {
    models.coding.belongsTo(models.lecture_item, { onDelete: 'cascade', foreignKey: 'lecture_item_id' });
    models.coding.hasMany(models.coding_keyword, { onDelete: 'cascade', foreignKey: 'coding_id' });
  };

  return model;
}
