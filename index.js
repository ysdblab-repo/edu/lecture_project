module.exports = () => {
  const cluster = require('cluster');
  const express = require('express');
  const bodyParser = require('body-parser');
  const morgan = require('morgan');
  const cors = require('cors');
  const http = require('http');
  const https = require('https');
  const path = require('path');
  const fs = require('fs');
  const config = require('./config');
  const errorBuilder = require('./modules/error-builder');
  const errorHandler = require('./modules/error-handler');
  const models = require('./models');
  const dbIni = require('./modules/db-ini');
  const app = express();
  const modules = require('./modules/frequent-modules');
  const { workerHandler } = require('./scripts/node/childMessageHandler');
  
  if (cluster.isMaster) {
    const { scoreingQueue, doneQueue } = require('./modules/score-update');
    const { oj_solution_judge } = require('./modules/scoreing');

    const oj_solution_scoreing = async () => {
      const solution_id = scoreingQueue.dequeue();
      if (solution_id) {
        let error;
        try {
          await oj_solution_judge(solution_id);
        } catch (err) {
          error = err;
        }
        if (error) {
          await modules.models.oj_solution.update({
            errorMsg: error.toString(),
            judged: 1,
          }, {
            where: {
              solution_id
            }
          });
        }
        doneQueue.enqueue(solution_id);
        setTimeout(() => {
          oj_solution_scoreing();
        }, 100);
      } else {
        setTimeout(() => {
          oj_solution_scoreing();
        }, 500);
      }
    }

    oj_solution_scoreing();
  } else {
    process.on('message', async message => {
      workerHandler(message);
    });
  }
  // define rootPath
  modules.rootPath = `${__dirname}`;

  app.use(express.static('public'));
  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: false, limit: '1024mb' }));
  app.use(bodyParser.json({ limit: '1024mb' }));
  app.use(morgan('common'));
  app.use('/', require('./router'));
  app.set('views', path.join(__dirname, '/views'));
  app.set('view engine', 'ejs');
  app.engine('html', require('ejs').renderFile);

  // no route match
  app.use((req, res, next) => {
    // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    // console.log(ip);
    next(errorBuilder.default('Not Found', 404, true));
  });

  app.use(errorHandler);

  let options;
  if (config.protocol === 'HTTPS' || config.protocol === 'https') {
    options = {
      key: fs.readFileSync('credential/wiseswot.private.key'),
      cert: fs.readFileSync('credential/www.wiseswot.com.crt'),
    };
  }


  models.sequelize.sync()
    .then(() => {
      if (!cluster.isMaster) {
        process.send && process.send({
          _done: true
        });
      }
    })
    .then(() => dbIni())
    .then(() => {
      if (config.protocol === 'HTTPS' || config.protocol === 'https') {
        return https.createServer(options, app).listen(config.port);
      }
      return http.createServer(app).listen(config.port);
    })
    .then(() => {
      if (cluster.isMaster) {
        console.log(`Express is running on port ${config.port}`);
      }
    });
}