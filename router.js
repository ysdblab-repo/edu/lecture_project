
const express = require('express');
const router = express.Router();

router.use('/users', require('./routers/users'));
router.use('/auth', require('./routers/auth'));
router.use('/bank', require('./routers/banks'));
router.use('/classes', require('./routers/classes'));
router.use('/lectures', require('./routers/lectures'));
router.use('/user_classes', require('./routers/user_classes'));
router.use('/student_lecture_logs', require('./routers/student_lecture_logs'));
router.use('/lecture_items', require('./routers/lecture_items'));
router.use('/questions', require('./routers/questions'));
router.use('/materials', require('./routers/materials'));
router.use('/surveys', require('./routers/surveys'));
router.use('/homework', require('./routers/homework'));
router.use('/files', require('./routers/files'));
router.use('/student', require('./routers/student'));
router.use('/test_page', require('./routers/test_page'));
router.use('/student_program_detail_logs', require('./routers/student_program_detail_logs'));
router.use('/student_program_logs', require('./routers/student_program_logs'));
router.use('/teacher_streaming_keys', require('./routers/teacher_streaming_keys'));
router.use('/student_lecture_faces', require('./routers/student_lecture_faces'));
router.use('/student_selfstudy_logs', require('./routers/student_selfstudy_logs'));
router.use('/journalings', require('./routers/journalings'));
router.use('/realtime_lecture_heartbeats', require('./routers/realtime_lecture_heartbeats'));
router.use('/lecture_code_practices', require('./routers/lecture_code_practices'));
router.use('/discussions', require('./routers/discussions'));
router.use('/lecture_chatings', require('./routers/lecture_chatings'));
router.use('/small_group_chattings', require('./routers/small_group_chattings'));
router.use('/downloads', require('./routers/downloads'));
router.use('/report', require('./routers/report'));
router.use('/reports', require('./routers/reports'));
router.use('/main_classes', require('./routers/main_classes'));
router.use('/boards', require('./routers/boards'));
router.use('/dump', require('./routers/dump'));
// added at preswot2
router.use('/ongoing', require('./routers/ongoing'));
router.use('/notes', require('./routers/notes'));
router.use('/automatic_lectures',require('./routers/automatic_lectures'));
// test
router.use('/test', require('./routers/test'));
router.use('/tests', require('./routers/tests'));
router.use('/face-model',require('./routers/face-models'));
// admin
// router.use('/admin',require('./routers/admin'));
router.use('/journalings2', require('./routers/journalings2'));
router.use('/journalings3', require('./routers/journalings3'));
router.use('/attitude', require('./routers/attitude'));
router.use('/cheating_small', require('./routers/cheating_small'));
router.use('/cheating_large', require('./routers/cheating_large'));
router.use('/university', require('./routers/university'));
router.use('/department', require('./routers/department'));
router.use('/admin_user', require('./routers/admin_user'));
router.use('/admin_bank', require('./routers/admin_bank'));
router.use('/admin_class', require('./routers/admin_class'));
router.use('/kdata', require('./routers/kdata'));
router.use('/process', require('./routers/process'));
router.use('/individuals', require('./routers/individuals'));
// similarity_score and coding_assignments
router.use('/similarity_score', require('./routers/similarity_score'));
router.use('/coding_assignment', require('./routers/coding_assignment'));
// docker container automation : depricated
router.use('/container_automation', require('./routers/container_automation'));
// docker container automation : new
router.use('/docker', require('./routers/docker'));
router.use('/journaling4', require('./routers/journaling4'));
router.use('/auth_rtmp', require('./routers/auth_rtmp'));

// enrollments
router.use('/enrollments', require('./routers/enrollments'));

router.use('/req_create_class', require('./routers/req_create_class'));
router.use('/user_belonging', require('./routers/user_belongings'));
router.use('/user_affiliation', require('./routers/user_affiliation'));

router.use('/lecture_item_lists', require('./routers/lecture_item_lists'));
router.use('/connect_session', require('./routers/connect_session'));
router.use('/small_meeting_room', require('./routers/small_meeting_room'));
router.use('/realtime_lecture_room', require('./routers/realtime_lecture_room'));
router.use('/attend', require('./routers/attend'));
router.use('/open_space', require('./routers/open_space'));

router.use('/affiliation', require('./routers/affiliation'));
// record
router.use('/record', require('./routers/record'));
router.use('/user_class_delete_logs', require('./routers/user_class_delete_logs'));

router.use('/bdi', require('./routers/bdi'));

module.exports = router;
