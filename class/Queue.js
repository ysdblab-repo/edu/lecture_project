const { Stack } = require('./Stack');
exports.Queue = class Queue {
  constructor() {
    this.inbox = new Stack();
    this.outbox = new Stack();
  }
  enqueue(value) {
    this.inbox.push(value);
  }
  dequeue(value) {
    const size = this.inbox.size();
    let temp_size = size;
    while (temp_size > 0) {
      this.outbox.push(this.inbox.pop());
      temp_size -= 1;
    }
    let dequeued;
    if (this.outbox.contains(value)) {
      let temp_value;
      temp_size = size;
      while (temp_size > 0) {
        temp_value = this.outbox.pop();
        if (dequeued || temp_value !== value) {
          this.inbox.push(temp_value);
        } else {
          dequeued = temp_value;
        }
      }
    } else {
      dequeued = this.outbox.pop();
      temp_size = size - 1;
      while (temp_size > 0) {
        this.inbox.push(this.outbox.pop());
        temp_size -= 1;
      }
    }
    return dequeued;
  }
  purge() {
    const inSize = this.inbox.size();
    const outSize = this.outbox.size();

    for (let i = 0; i < inSize; i++) {
      this.inbox.pop();
    }
    for (let o = 0; o < outSize; o++) {
      this.outbox.pop();
    }
  }
  contains(value) {
    return this.inbox.contains(value);
  }
  size() {
    return this.inbox.size();
  }
}